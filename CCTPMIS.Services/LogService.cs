﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Web;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;

namespace CCTPMIS.Services
{


    public interface ILogService
    {
        void DBLog(string type, string exc, string category, string item, string UserId);


        void FileLog(string dataType, string data, string userId = null);

        void LogWrite(string type, string exc, string category, string item, string UserId);

        void AuditTrail(AuditTrailVm model);

        void FPLog(string data, string enrolmentNo);
    }

    public class LogService : ILogService
    {
        private string path = HttpContext.Current.Server.MapPath("~/logs/");
        private string dBPath = HttpContext.Current.Server.MapPath("~/DATABASE/");

        private readonly IGenericService GenericService;
        public LogService(GenericService genericService)
        {
            GenericService = genericService;
        }

        public void DBLog(string type, string exc, string category, string item, string UserId)
        {

        }
        public void FPLog( string data,string enrolmentNo)
        {

            var fs = new FileStream($"{dBPath}{enrolmentNo}.json",FileMode.OpenOrCreate,FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine(data);
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void FileLog(string dataType, string data, string userId = null)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path}{dataType}-{todaydate}-{hour}-00.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine("||            Date: {0}   Time : {1}", todaydate, t);
                if (userId != null)
                {
                    mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                    mStreamWriter.WriteLine(" USER ID  " + userId);
                }
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.WriteLine(data);
                mStreamWriter.WriteLine(" ****************************************************************************************************************************");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void LogWrite(string type, string exc, string category, string item, string UserId)
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            var minute = dt.Date.ToString("mm");
            hour = dt.TimeOfDay.Hours.ToString();
            if (!Equals(seconds, dt.Second))
            {
                seconds = dt.Second;
            }

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{this.path}-{todaydate}-{hour}-00.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.WriteLine("");
                mStreamWriter.WriteLine(exc);

                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }

        public void MailLog(string type, string exc, string category, string item, string UserId)
        {
        }      
        public void AuditTrail(AuditTrailVm model)
        {
            HttpContext context = HttpContext.Current;
            var UserAgent = "";
            if (context != null && context.Request.Browser != null)
            {
                var objBrwInfo = HttpContext.Current.Request.Browser;
                UserAgent = objBrwInfo.Browser + " " + objBrwInfo.Version + " " + objBrwInfo.Platform;
            }
            var ip = GetExternalIP();
            var spName = "AddAuditTrail";
            var parameterNames = "@IPAddress,@MACAddress,@IMEI,@ModuleRightCode,@Description,@TableName,@UserAgent,@Key1,@Key2,@Key3,@Key4,@Key5,@Record,@WasSuccessful,@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("IPAddress",ip?? "")},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("MACAddress", GetMacAddress(ip)??""), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("IMEI", model.IMEI ?? (object)DBNull.Value), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("ModuleRightCode", model.ModuleRightCode), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Description", model.Description), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("TableName", model.TableName), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserAgent", UserAgent), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Key1", model.Key1 ?? (object)DBNull.Value) },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Key2", model.Key2 ?? (object)DBNull.Value), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Key3", model.Key3 ?? (object)DBNull.Value), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Key4",model.Key4 ?? (object)DBNull.Value), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Key5", model.Key5 ?? (object)DBNull.Value), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("Record", model.Record), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("WasSuccessful", model.WasSuccessful), },
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", model.UserId), },

            };

            try
            {
                GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);

            }
            catch (Exception e)
            {
               
            }
          
        }

        public string GetMacAddress(string ipAddress)
        {
            string macAddress;
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process {
                StartInfo =
                {
                    FileName = "arp",
                    Arguments = "-a " + ipAddress,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            pProcess.Start();
            string strOutput = pProcess.StandardOutput.ReadToEnd();
            string[] substrings = strOutput.Split('-');
            if (substrings.Length >= 8)
            {
                macAddress = substrings[3].Substring(Math.Max(0, substrings[3].Length - 2))
                             + "-" + substrings[4] + "-" + substrings[5] + "-" + substrings[6]
                             + "-" + substrings[7] + "-"
                             + substrings[8].Substring(0, 2);
                return macAddress;
            }
            else
            {
                return "";
            }
        }

        private string GetMacAddress()
        {

            string MacAddress = string.Empty;

            try
            {
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

                MacAddress = Convert.ToString(nics[0].GetPhysicalAddress());

                return MacAddress;
            }
            catch (ArgumentNullException)
            {
                //  lblErrorMsg.Text = "Application Error : " + Exc.Message;
                return MacAddress;
            }
            catch (InvalidCastException)
            {

                return MacAddress;
            }
            catch (InvalidOperationException)
            {

                return MacAddress;
            }
            catch (NullReferenceException)
            {
                return MacAddress;
            }
            catch (Exception)
            {
                return MacAddress;
            }

        }

        private string GetComputerName(string clientIP)
        {
            try
            {
                var hostEntry = Dns.GetHostEntry(clientIP);
                return hostEntry.HostName;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private static string GetExternalIP()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                    .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }
    }
}