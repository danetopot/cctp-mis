﻿namespace CCTPMIS.Services
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading.Tasks;

    using Business.Context;
    using Business.Model;

    public interface ISystemCodeService
    {
        Task<int> AddSystemCodeAsync(SystemCode systemCode);

        Task<int> AddSystemCodeDetailAsync(SystemCodeDetail systemCodeDetail);

        SystemCode FindSystemCode(int id);

        Task<SystemCode> FindSystemCodeAsync(int id);

        SystemCodeDetail FindSystemCodeDetail(int id);

        Task<SystemCodeDetail> FindSystemCodeDetailAsync(int id);

        SystemCode GetSystemCode(int id);

        Task<SystemCode> GetSystemCodeAsync(int id);

        SystemCodeDetail GetSystemCodeDetail(int id);

        Task<SystemCodeDetail> GetSystemCodeDetailAsync(int id);

        IQueryable<SystemCodeDetail> GetSystemCodeDetails(int systemCodeId, int page, int pageSize);

        Task<IEnumerable<SystemCodeDetail>> GetSystemCodeDetailsAsync(int systemCodeId, int pageId, int pageSize);

        IQueryable<SystemCode> GetSystemCodes();

        IQueryable<SystemCode> GetSystemCodes(int pageId, int pagesize);

        Task<IEnumerable<SystemCode>> GetSystemCodesAsync();

        Task<IEnumerable<SystemCode>> GetSystemCodesAsync(int pageId, int pagesize);

        Task UpdateSystemCodeAsync(SystemCode systemCode);

        Task UpdateSystemCodeDetailAsync(SystemCodeDetail systemCodeDetail);
    }

    public class SystemCodeService : ISystemCodeService
    {
        private readonly ApplicationDbContext db;

        public SystemCodeService(ApplicationDbContext dbContext)
        {
            db = dbContext;
        }

        public async Task<int> AddSystemCodeAsync(SystemCode systemCode)
        {
            db.SystemCodes.Add(systemCode);
            await db.SaveChangesAsync();
            return systemCode.Id;
        }

        public async Task<int> AddSystemCodeDetailAsync(SystemCodeDetail systemCodeDetail)
        {
            db.SystemCodeDetails.Add(systemCodeDetail);
            await db.SaveChangesAsync();
            return systemCodeDetail.Id;
        }

        public SystemCode FindSystemCode(int id)
        {
            return db.SystemCodes.Find(id);
        }

        public async Task<SystemCode> FindSystemCodeAsync(int id)
        {
            return await db.SystemCodes.FindAsync(id);
        }

        public SystemCodeDetail FindSystemCodeDetail(int id)
        {
            return db.SystemCodeDetails.Find(id);
        }

        public async Task<SystemCodeDetail> FindSystemCodeDetailAsync(int id)
        {
            return await db.SystemCodeDetails.FindAsync(id);
        }

        public SystemCode GetSystemCode(int id)
        {
            return db.SystemCodes.Include(x => x.SystemCodeDetails).Single(x => x.Id == id);
        }

        public async Task<SystemCode> GetSystemCodeAsync(int id)
        {
            return await db.SystemCodes.Include(x => x.SystemCodeDetails).SingleAsync(x => x.Id == id);
        }

        public SystemCodeDetail GetSystemCodeDetail(int id)
        {
            return db.SystemCodeDetails.Single(x => x.Id == id);
        }

        public async Task<SystemCodeDetail> GetSystemCodeDetailAsync(int id)
        {
            return await db.SystemCodeDetails.SingleOrDefaultAsync(x => x.Id == id);
        }

        public IQueryable<SystemCodeDetail> GetSystemCodeDetails(int systemCodeId, int page, int pageSize)
        {
            var codeDetails = db.SystemCodeDetails.Where(x => x.SystemCodeId == systemCodeId)
                .OrderBy(x => x.Description).Skip(pageSize * page).Take(pageSize);
            return codeDetails;
        }

        public async Task<IEnumerable<SystemCodeDetail>> GetSystemCodeDetailsAsync(
            int systemCodeId,
            int page,
            int pageSize)
        {
            var codeDetails = await db.SystemCodeDetails.Where(x => x.SystemCodeId == systemCodeId)
                                  .OrderBy(x => x.Description).Skip(pageSize * page).Take(pageSize).ToListAsync();
            return codeDetails;
        }

        public IQueryable<SystemCode> GetSystemCodes()
        {
            return db.SystemCodes;
        }

        public IQueryable<SystemCode> GetSystemCodes(int pageId, int pagesize)
        {
            return db.SystemCodes.Skip(pagesize * pageId).Take(pagesize);
        }

        public async Task<IEnumerable<SystemCode>> GetSystemCodesAsync()
        {
            return await db.SystemCodes.ToListAsync();
        }

        public async Task<IEnumerable<SystemCode>> GetSystemCodesAsync(int pageId, int pagesize)
        {
            return await db.SystemCodes.OrderBy(x => x.Code).Skip(pagesize * pageId).Take(pagesize).ToListAsync();
        }

        public async Task UpdateSystemCodeAsync(SystemCode systemCode)
        {
            db.SystemCodes.AddOrUpdate(systemCode);
            await db.SaveChangesAsync();
        }

        public async Task UpdateSystemCodeDetailAsync(SystemCodeDetail systemCodeDetail)
        {
            db.SystemCodeDetails.AddOrUpdate(systemCodeDetail);
            await db.SaveChangesAsync();
        }
    }
}