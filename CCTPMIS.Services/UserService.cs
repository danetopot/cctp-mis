﻿using System.Collections.Generic;
using CCTPMIS.Business.Model;


namespace CCTPMIS.Services
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    using Business.Context;

    public interface IUserService
    {
        int ActivateUser(int id);

        int DeactivateUser(int id);

        string GeneratePassword(int maxSize);

        int LockUser(int id);

        string Random(string type);

        int UnLockUser(int id);

        ApplicationUser Find(int id);
    }

    public class UserService : IUserService
    {
        private readonly ApplicationDbContext db;

        public UserService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public ApplicationUser Find(int id)
        {
            return db.Users.Single(a => a.Id == id);
        }
        public void Update(ApplicationUser user)
        {
            db.Users.AddOrUpdate(user);
            db.SaveChanges();
            return;
        }
        public int ActivateUser(int id)
        {
            var user = db.Users.Single(a => a.Id == id);
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.IsActive = true;
                user.AccessFailedCount = 0;
                user.LockoutEndDateUtc = null;
                user.IsLocked = false;
                db.Users.AddOrUpdate(user);
                db.SaveChanges();
            }
            else
            {
                id = 0;
            }

            return id;
        }

        public int DeactivateUser(int id)
        {
            var user = db.Users.Single(a => a.Id == id);
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.AccessFailedCount = 6;
                user.IsActive = false;
                user.LockoutEndDateUtc = DateTime.Now.AddYears(100);
                db.Users.AddOrUpdate(user);
                db.SaveChanges();
            }
            else
            {
                id = 0;
            }

            return id;
        }

        public string GeneratePassword(int maxSize)
        {
            var passwords = string.Empty;
            var chArray1 = new char[52];
            var chArray2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^*()_+".ToCharArray();
            var data1 = new byte[1];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetNonZeroBytes(data1);
                var data2 = new byte[maxSize];
                cryptoServiceProvider.GetNonZeroBytes(data2);
                var stringBuilder = new StringBuilder(maxSize);
                foreach (var num in data2)
                {
                    stringBuilder.Append(chArray2[num % chArray2.Length]);
                }

                passwords = stringBuilder.ToString();
                return Shuffle(passwords + Random("N") + Random("S") + Random("l"));
            }
        }

        public int LockUser(int id)
        {
            var user = db.Users.Single(a => a.Id == id);
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.AccessFailedCount = 5;
                user.LockoutEndDateUtc = DateTime.Now.AddYears(100);
                db.Users.AddOrUpdate(user);
                db.SaveChanges();
            }
            else
            {
                id = 0;
            }

            return id;
        }

        public string Shuffle(string list)
        {
            Random R = new Random();
            int index;
            List<char> chars = new List<char>(list);
            StringBuilder sb = new StringBuilder();
            while (chars.Count > 0)
            {
                index = R.Next(chars.Count);
                sb.Append(chars[index]);
                chars.RemoveAt(index);
            }
            return sb.ToString();
        }
        public string Random(string type)
        {
            var data2 = new byte[2];
            var passwords = string.Empty;
            switch (type)
            {
                case "N":
                    {
                        var charArray = "0123456789";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                        {
                            stringBuilder.Append(charArray[num % charArray.Length]);
                        }

                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "l":
                    {
                        var charArray = "abcdefghijklmnopqrstuvwxyz";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                        {
                            stringBuilder.Append(charArray[num % charArray.Length]);
                        }

                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "C":
                    {
                        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                        {
                            stringBuilder.Append(charArray[num % charArray.Length]);
                        }

                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "S":
                    {
                        var charArray = "!@#$%^&*()_+-={}|[]:;<>?,./";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                        {
                            stringBuilder.Append(charArray[num % charArray.Length]);
                        }

                        passwords = stringBuilder.ToString();
                        return passwords;
                    }
            }

            return string.Empty;
        }

        public int UnLockUser(int id)
        {
            var user = db.Users.Single(a => a.Id == id);
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.AccessFailedCount = 0;
                user.LockoutEndDateUtc = DateTime.Now.AddDays(-1);
                user.PasswordChangeDate = DateTime.Now.AddDays(-90);
                db.Users.AddOrUpdate(user);
                db.SaveChanges();
            }
            else
            {
                id = 0;
            }

            return id;
        }
    }
}