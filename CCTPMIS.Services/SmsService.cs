﻿namespace CCTPMIS.Services
{
    using System;
    using System.Net.Mail;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;

    using Postal;

    public interface ISmsService : IIdentityMessageService, IEmailService
    {
    }

    public class SmsService : ISmsService
    {
        public MailMessage CreateMailMessage(Email email)
        {
            throw new NotImplementedException();
        }

        public string CreateRawMessage(Email email)
        {
            throw new NotImplementedException();
        }

        public void Send(Email email)
        {
            throw new NotImplementedException();
        }

        public Task SendAsync(IdentityMessage message)
        {
            throw new NotImplementedException();
        }

        public Task SendAsync(Email email)
        {
            throw new NotImplementedException();
        }
    }
}