﻿namespace CCTPMIS.Services
{
    using Business.Context;

    public interface ISettingService
    {
    }

    public class SettingService : ISettingService
    {
        public ApplicationDbContext db;

        public SettingService(ApplicationDbContext _db)
        {
            db = _db;
        }
    }
}