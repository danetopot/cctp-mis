﻿namespace CCTPMIS.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Business.Model;

    public interface IGeoMasterService
    {
        /*        Task<GeoMaster> GetGeoMastersAsync(int id);
                Task<IEnumerable<GeoMaster>> GetGeoMastersAsync();
                Task<int> UpdateGeoMasterAsync(GeoMaster master);
                Task<IEnumerable<GeoMaster>> GetGeoMasterAsync(int pageSize, int pageCount);
                Task AddGeoMasterAsync(GeoMaster geoMaster);
                Task<County> GetCountyByIdAsync(int id);
                Task<IEnumerable<County>> GetCountiesByGeoMasterIdAsync(int masterId);
                Task<int> UpdateCountyAsync(County master);
                Task<IEnumerable<County>> GetCountyAsync(int masterId, int pageSize, int pageCount);
                Task AddCountyAsync(County County);

                */

        void AddCounty(County county);

        void AddGeoMaster(GeoMaster geoMaster);

        Task<IEnumerable<County>> GetCountiesByGeoMasterId(int masterId);

        Task<IEnumerable<County>> GetCountiesByMasterIdAsync(int masterId, int pageSize, int pageCount);

        County GetCountyById(int id);

        Task<IEnumerable<GeoMaster>> GetGeoMasterAsync(int pageSize, int pageCount);

        GeoMaster GetGeoMasterAsync(int id);

        Task<IEnumerable<GeoMaster>> GetGeoMastersAsync();

        void UpdateCounty(County county);

        void UpdateGeoMaster(GeoMaster master);
    }

    public class GeoMasterService : IGeoMasterService
    {
        private readonly IGenericService GenericService;

        public GeoMasterService(GenericService genericService)
        {
            GenericService = genericService;
        }

        public void AddCounty(County county)
        {
            GenericService.Create<County>(county);
        }

        public void AddGeoMaster(GeoMaster geoMaster)
        {
            GenericService.Create<GeoMaster>(geoMaster);
        }

        public async Task<IEnumerable<County>> GetCountiesByGeoMasterId(int masterId)
        {
            return await GenericService.GetAsync<County>(x => x.GeoMasterId == masterId, null, null, null, null);

            // return await  UoW.GetRepository<County>().GetAllAsync(d => d.GeoMasterId == masterId,null,null,null,null,null);
        }

        public async Task<IEnumerable<County>> GetCountiesByMasterIdAsync(int masterId, int pageSize, int pageCount)
        {
            return await GenericService.GetAsync<County>(
                       d => d.GeoMasterId == masterId,
                       d => d.OrderBy(x => x.Name),
                       null,
                       pageSize * pageCount,
                       pageSize);
        }

        public County GetCountyById(int id)
        {
            return GenericService.GetOne<County>(x => x.Id == id);

            // return  UoW.GetRepository<County>().Find(id);
        }

        public async Task<IEnumerable<GeoMaster>> GetGeoMasterAsync(int pageSize, int pageCount)
        {
            return await GenericService.GetAsync<GeoMaster>(null, null, null, pageCount * pageSize, pageSize);

            // return await UoW.GetRepository<GeoMaster>().GetAllAsync(null, null, null, null, page: pageCount, pageSize: pageSize);
        }

        public GeoMaster GetGeoMasterAsync(int id)
        {
            return GenericService.GetOne<GeoMaster>(d => d.Id == id);
        }

        public async Task<IEnumerable<GeoMaster>> GetGeoMastersAsync()
        {
            return await GenericService.GetAllAsync<GeoMaster>(null, null, null, null);

            // return await UoW.GetRepository<GeoMaster>().GetAllAsync(null, null, null, null, null,null);
        }

        public void UpdateCounty(County county)
        {
            GenericService.Update<County>(county);
            GenericService.Save();

            // UoW.GetRepository<County>().Update(county);
            // UoW.Save();
        }

        public void UpdateGeoMaster(GeoMaster master)
        {
            // UoW.GetRepository<GeoMaster>().Update(master);
            // UoW.Save();
            GenericService.Update<GeoMaster>(master);
            GenericService.Save();
        }
    }
}