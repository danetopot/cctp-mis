﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using Microsoft.AspNet.Identity;

namespace CCTPMIS.Services
{

    public interface IGroupService
    {
        ApplicationGroup GetGroup(int id);
        ApplicationGroup Find(int id);
        IQueryable<ApplicationGroup> GetGroups();

        IEnumerable<ApplicationRole> GetGroupRoles(int groupId);
        int CreateGroup(ApplicationGroup group);

        IEnumerable<ApplicationGroupRole> SetApplicationGroupRoles(int id, int[] selectedRoles);
        bool RemoveApplicationGroupRoles(int groupId);
        int UpdateGroup(ApplicationGroup group);
        IEnumerable<ApplicationUser> GetApplicationGroupUsers(int id);

        Task DeleteGroup(int id);

    }

    public class GroupService : IGroupService
    {
        private ApplicationDbContext db;
        public GroupService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public ApplicationGroup GetGroup(int id)
        {
            return db.ApplicationGroups.Include(x => x.ApplicationGroupRoles).Single(x => x.Id == id);
        }
        public IQueryable<ApplicationGroup> GetGroups()
        {
            return db.ApplicationGroups.Include(x => x.ApplicationGroupRoles);
        }

        public IEnumerable<ApplicationRole> GetGroupRoles(int groupId)
        {
            var grp = db.ApplicationGroups.Include(x => x.ApplicationGroupRoles).FirstOrDefault(g => g.Id == groupId);
            var roles = db.Roles.ToList();

            var groupRoles = from r in roles
                             where grp.ApplicationGroupRoles.Any(ap => ap.ApplicationRoleId == r.Id)
                             select r;
            return groupRoles;
        }

        public int CreateGroup(ApplicationGroup @group)
        {
            db.ApplicationGroups.Add(group);
            db.SaveChanges();
            return group.Id;
        }

        public ApplicationGroup Find(int id)
        {
            return db.ApplicationGroups.Find(id);
        }

        public IEnumerable<ApplicationGroupRole> SetApplicationGroupRoles(int id, int[] selectedRoles)
        {
            foreach (var role in selectedRoles)
                db.ApplicationGroupRoles.Add(new ApplicationGroupRole { ApplicationGroupId = id, ApplicationRoleId = role });
            db.SaveChanges();
            return db.ApplicationGroupRoles.Where(x => x.ApplicationGroupId == id);
        }

        public int UpdateGroup(ApplicationGroup @group)
        {
            db.ApplicationGroups.AddOrUpdate(group);
            db.SaveChanges();
            return group.Id;
        }

        public IEnumerable<ApplicationUser> GetApplicationGroupUsers(int id)
        {
            var userGroups = db.ApplicationUserGroups.Where(x => x.ApplicationGroupId == id);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in userGroups)
            {
                var user = db.Users.Find(groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }

        public async Task DeleteGroup(int id)
        {
            var grp = await db.ApplicationGroups.FindAsync(id);
            db.ApplicationGroups.Remove(grp ?? throw new InvalidOperationException());
            await db.SaveChangesAsync();
        }

        public bool RemoveApplicationGroupRoles(int groupId)
        {
            var itemstoPurge = db.ApplicationGroupRoles.Where(x => x.ApplicationGroupId == groupId);
            if (itemstoPurge.Any())
                db.ApplicationGroupRoles.RemoveRange(itemstoPurge);
            db.SaveChanges();
            return true;
        }


    }

}
