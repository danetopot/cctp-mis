﻿namespace CCTPMIS.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Business.Context;

    public interface IExportService
    {
        void ExportToExcel(object list, string fileName);

        void ExportToPDF(string url, string reportTitle, string paperKind = "A3");
    }

    public class ExportService : IExportService
    {
        private ApplicationDbContext db;

        public ExportService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public void ExportToExcel(object list, string fileName)
        {
            var gv = new GridView { DataSource = list, GridLines = GridLines.None };
            gv.DataBind();

            var headerRow = gv.HeaderRow;
            foreach (TableCell cel in headerRow.Cells)
            {
                cel.Text = cel.Text.Replace("_", " ");
            }

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Charset = string.Empty;

            var strwritter = new StringWriter();
            var htmltextwrtter = new HtmlTextWriter(strwritter);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

            HttpContext.Current.Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
            HttpContext.Current.Response.Write("<head>");
            HttpContext.Current.Response.Write(
                "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\">");
            HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
            HttpContext.Current.Response.Write("<xml>");
            HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
            HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
            HttpContext.Current.Response.Write("<x:ExcelWorksheet>");

            // this line names the worksheet
            HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
            HttpContext.Current.Response.Write("<x:WorksheetOptions>");

            // these 2 lines are what works the magic
            HttpContext.Current.Response.Write("<x:Panes>");
            HttpContext.Current.Response.Write("</x:Panes>");

            HttpContext.Current.Response.Write("</x:WorksheetOptions>");
            HttpContext.Current.Response.Write("</x:ExcelWorksheet>");

            HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
            HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
            HttpContext.Current.Response.Write("</xml>");
            HttpContext.Current.Response.Write("<![endif]-->");
            HttpContext.Current.Response.Write("</head>");
            HttpContext.Current.Response.Write("<body>");

            // Grid view
            gv.HeaderStyle.Font.Bold = true;
            gv.RenderControl(htmltextwrtter);
            HttpContext.Current.Response.Write(strwritter.ToString());

            HttpContext.Current.Response.Write("</body>");
            HttpContext.Current.Response.Write("</html>");

            HttpContext.Current.Response.End();
        }

        public void ExportToPDF(string url, string reportTitle, string paperKind = "A3")
        {
            string html;
            var originalPath = new Uri(HttpContext.Current.Request.Url.AbsoluteUri).OriginalString;
            var parentDirectory = originalPath.Substring(0, originalPath.LastIndexOf("/"));
            var rootUrl = originalPath + parentDirectory;

            // var rootUrl = WebRequest.ur // Statics.GetSystemSettings().Single(s => s.Key == "ROOT_URL").Value;
            url = rootUrl + url;
            using (var client = new WebClient())
            {
                html = client.DownloadString(url);
            }

            var title = reportTitle + ".pdf";

            /*
                         var pechkin = Factory.Create(new GlobalConfig().SetPaperSize(PaperKind.A3));
                          if (paperKind.Equals("A4"))
                          {
                              pechkin = Factory.Create(new GlobalConfig().SetPaperSize(PaperKind.A4));
                          }

                          var pdf = pechkin.Convert(new ObjectConfig()
                              .SetLoadImages(true).SetZoomFactor(1.5)

                              .SetPrintBackground(true)
                              .SetScreenMediaType(true)
                              .SetCreateExternalLinks(true), html);

                          System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/Content/Downloads/" + title), pdf);
                          HttpContext.Current.Response.Clear();
                          HttpContext.Current.Response.ClearContent();
                          HttpContext.Current.Response.ClearHeaders();
                          HttpContext.Current.Response.ContentType = "application/pdf";
                          HttpContext.Current.Response.AddHeader("Content-Disposition",
                              value: string.Format("attachment;filename={1}; size={0}", pdf.Length, title));
                          HttpContext.Current.Response.BinaryWrite(pdf);
                           */
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
    }
}