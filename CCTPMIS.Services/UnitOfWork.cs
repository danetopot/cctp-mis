﻿using CCTPMIS.Business.Context;
using CCTPMIS.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using CCTPMIS.Business.Interfaces;


namespace CCTPMIS.Services
{
    public interface IUnitOfWork : IDisposable
    {
        int Save();

        Task<int> SaveAsync();


        // IRepository<TEntity> GetRepository<TEntity>() where TEntity : class,IEntity
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private Dictionary<Type, object> _repositories;

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<ApplicationDbContext> GetRepository<ApplicationDbContext>() where TEntity : class, IEntity
        {
            // Checks if the Dictionary Key contains the Model class
            if (_repositories.Keys.Contains(typeof(TEntity)))
            {
                // Return the repository for that Model class
                return _repositories[typeof(TEntity)] as IRepository<TEntity>;
            }
            // If the repository for that Model class doesn't exist, create it
            var repository = new Repository<TEntity>(_context);

            // Add it to the dictionary
            _repositories.Add(typeof(TEntity), repository);

            return repository;
        }
        /*     */

        public int Save()
        {
            return _context.SaveChanges();
        }
        public Task<int> SaveAsync()
        {
            try
            {
                return _context.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                return Task.FromResult(0);
            }


        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}