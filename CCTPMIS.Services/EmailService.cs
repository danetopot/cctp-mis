﻿using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models;

namespace CCTPMIS.Services
{
    using System;
    using System.Net;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Postal;

    public interface IEmailService : Postal.IEmailService
    {
        string CreateRawMessage(Email email);
    }

    public class EmailService : IEmailService
    {
        private readonly Func<SmtpClient> createSmtpClient;

        private readonly IEmailParser emailParser;

        private readonly IEmailViewRenderer emailViewRenderer;

        /// <summary>
        ///     Creates a new cref="EmailService" , using the default view engines.
        /// </summary>
        public EmailService()
            : this(ViewEngines.Engines)
        {
        }

        /// <summary>Creates a new <see cref="EmailService" />, using the given view engines.</summary>
        /// <param name="viewEngines">The view engines to use when creating email views.</param>
        /// <param name="createSmtpClient">
        ///     A function that creates a <see cref="SmtpClient" />. If null, a default creation
        ///     function is used.
        /// </param>
        public EmailService(ViewEngineCollection viewEngines, Func<SmtpClient> createSmtpClient = null)
        {
            emailViewRenderer = new EmailViewRenderer(viewEngines);
            emailParser = new EmailParser(emailViewRenderer);
            this.createSmtpClient = createSmtpClient ?? (() => new SmtpClient());
        }

        /// <summary>
        ///     Creates a new <see cref="EmailService" />.
        /// </summary>
        public EmailService(
            IEmailViewRenderer emailViewRenderer,
            IEmailParser emailParser,
            Func<SmtpClient> createSmtpClient)
        {
            this.emailViewRenderer = emailViewRenderer;
            this.emailParser = emailParser;
            this.createSmtpClient = createSmtpClient;
        }

        /// <summary>
        ///     Renders the email view and builds a <see cref="MailMessage" />. Does not send the email.
        /// </summary>
        /// <param name="email">The email to render.</param>
        /// <returns>A <see cref="MailMessage" /> containing the rendered email.</returns>
        public MailMessage CreateMailMessage(Email email)
        {
            var rawEmailString = emailViewRenderer.Render(email);
            var header = emailViewRenderer.Render(email, "Email_Header");
            var footer = emailViewRenderer.Render(email, "Email_Footer");

            var finalEmailstring = rawEmailString.Replace("_#HEADER#_", header).Replace("_#FOOTER#_", footer);
            var mailMessage = emailParser.Parse(finalEmailstring, email);
            mailMessage.IsBodyHtml = true;

            return mailMessage;
        }

        public string CreateRawMessage(Email email)
        {
            var rawEmailString = emailViewRenderer.Render(email);
            var mailMessage = emailParser.Parse(rawEmailString, email);
            return mailMessage.Body.Replace("_#HEADER#_", string.Empty).Replace("_#FOOTER#_", string.Empty)
                .Replace(Environment.NewLine, string.Empty);
        }

        public SmtpClient CreateSmtpClient()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);

            var smtpClient = new SmtpClient
                                 {
                                     DeliveryMethod = SmtpDeliveryMethod.Network,
                                     EnableSsl = false,
                                     Host = WebConfigurationManager.AppSettings["EMAIL_HOST"], 
                                     Port = int.Parse(WebConfigurationManager.AppSettings["EMAIL_PORT"]),
                                     UseDefaultCredentials = false, 
                                     Credentials = new NetworkCredential(WebConfigurationManager.AppSettings["EMAIL_USER"],
                                         WebConfigurationManager.AppSettings["EMAIL_PASS"])
                                 };
            return smtpClient;
        }

        /// <summary>
        ///     Sends an email using an <see cref="SmtpClient" />.
        /// </summary>
        /// <param name="email">The email to send.</param>
        public void Send(Email email)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            using (var mailMessage = CreateMailMessage(email))
            using (var smtp = CreateSmtpClient())
            {
                smtp.Send(mailMessage);
            }
        }


        /// <summary>
        ///     Send an email asynchronously, using an <see cref="SmtpClient" />.
        /// </summary>
        /// <param name="email">The email to send.</param>
        /// <returns>A <see cref="Task" /> that completes once the email has been sent.</returns>
        public   Task SendAsync(Email email)
        {
            var mailMessage = CreateMailMessage(email);
            mailMessage.IsBodyHtml = true;

            // --- To re-look into this before deployment 
            //var logService = new LogService(new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext())));
            //logService.FileLog(MisKeys.EMAIL, mailMessage.Body, mailMessage.Subject);

            //if(WebConfigurationManager.AppSettings["SEND_EMAIL"] == "False")
            // return Task.FromResult(0);
            // else
            try
            {
                var smtp = CreateSmtpClient();
                try
                {
                    var taskCompletionSource = new TaskCompletionSource<object>();
                    smtp.SendCompleted += (o, e) =>
                        {
                            smtp?.Dispose();
                            mailMessage.Dispose();

                            if (e.Error != null)
                            {
                                taskCompletionSource.TrySetException(e.Error);
                            }
                            else if (e.Cancelled)
                            {
                                taskCompletionSource.TrySetCanceled();
                            }
                            else
                            {
                                // Success
                                taskCompletionSource.TrySetResult(null);
                            }
                        };
                    smtp.SendAsync(mailMessage, null);
                    return taskCompletionSource.Task;
                }
                catch
                {
                    smtp.Dispose();
                    throw;
                }
            }
            catch
            {
                mailMessage.Dispose();
                throw;
            }
        }


        public bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            // replace with proper validation
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            else
                return false;
        }
    }
}