﻿namespace CCTPMIS.Services
{
    using System;
    using System.Data.Entity;
    using System.IO;

    using Business.Context;

    using Ionic.Zip;

    public interface IBackupService
    {
        bool BackupDataBase(int workProcessID);

        void ExportFiles(string path, string storagePath, int workProcessID);

        void ExportPhotos(string path, string storagePath, int workProcessID);

        void ExportUploadedFiles(string path, string storagePath, int workProcessID);
    }

    public class BackupService : IBackupService
    {
        private readonly ApplicationDbContext db;

        public BackupService()
        {
            db = ApplicationDbContext.Create();
        }

        public bool BackupDataBase(int workProcessID)
        {
            var isSuccessful = false;
            try
            {
                // _workProcessService.Update(workProcessID, "Database Backup started", 0);
                var dbname = db.Database.Connection.Database;
                var dbBackUp = dbname + "_" + DateTime.Now.ToString("yyyyMMddHHmm");
                const string sqlCommand =
                    @"BACKUP DATABASE [{0}] TO  DISK = N'{1}' WITH NOFORMAT, NOINIT,  NAME = N'{2}', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";
                var dbBackupName = $"{dbname}-Full Database Backup";
                var path = db.Database.ExecuteSqlCommand(
                    TransactionalBehavior.DoNotEnsureTransaction,
                    string.Format(sqlCommand, dbname, dbBackUp, dbBackupName));
                isSuccessful = true;

                // _workProcessService.Update(workProcessID, "Database Backup completed", 100, isComplete: true);
            }
            catch (Exception err)
            {
                try
                {
                    // _workProcessService.Update(workProcessID, "Database Backup  failed", 100, isComplete: true, error: err.Message);
                }
                catch
                {
                }
            }

            return isSuccessful;
        }

        public void ExportFiles(string path, string storagePath, int workProcessID)
        {
            using (var zip = new ZipFile())
            {
                zip.AddDirectory(path, storagePath);
                var fileName = "Backup_" + DateTime.Now.ToString("yyyyMMddTHHmmss");
                zip.Save(fileName);
            }
        }

        public void ExportPhotos(string path, string storagePath, int workProcessID)
        {
            try
            {
                // _workProcessService.Update(workProcessID, "Export started", 0);
                var fileName = "Photos_" + DateTime.Now.ToString("yyyyMMddTHHmmss");
                using (var zip = new ZipFile(Path.Combine(path, fileName + ".zip")))
                {
                    // foreach (var upload in db.Uploads.Where(u => u.Type == UploadType.ProductImage).OrderBy(u => u.Id).InChunksOf(100))
                    // {
                    // zip.AddFile(Path.Combine(storagePath, upload.Id.ToString()), "")
                    // .FileName = upload.Id + ".jpg";
                    // }
                    zip.SaveProgress += delegate(object sender, SaveProgressEventArgs args)
                        {
                            if (args.EntriesSaved == 0 || args.EntriesSaved % 100 != 0) return;
                            bool cancelRequested;

                            // _workProcessService.Update(workProcessID, $"Archiving photos ({args.EntriesSaved} of {args.EntriesTotal})",
                            // ((double)args.EntriesSaved / args.EntriesTotal) * 100.0,
                            // out cancelRequested);
                            // if (cancelRequested) args.Cancel = true;
                        };
                    zip.Save();
                }

                // _workProcessService.Update(workProcessID, "Export completed", 100, isComplete: true);
            }
            catch (Exception err)
            {
                try
                {
                    // _workProcessService.Update(workProcessID, "Export failed", 100, isComplete: true, error: err.Message);
                }
                catch
                {
                }

                try
                {
                    // ErrorLog.GetDefault(HttpContext.Current).Log(new Error(err));
                }
                catch
                {
                }
            }
        }

        public void ExportUploadedFiles(string path, string storagePath, int workProcessID)
        {
        }
    }
}