﻿using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CCTPMIS.Services
{
    public interface IGroupService
    {
        IdentityResult ClearUserGroups(int userId);

        Task<IdentityResult> ClearUserGroupsAsync(int userId);

        IdentityResult CreateGroup(ApplicationGroup group);

        Task<IdentityResult> CreateGroupAsync(ApplicationGroup group);

        ApplicationGroup FindById(int id);

        Task<ApplicationGroup> FindByIdAsync(int id);

        Task<ApplicationGroup> GetApplicationGroupAsync(int id);

        ApplicationGroup GetApplicationGroup(int groupId);

        IQueryable<ApplicationGroup> GetApplicationGroups();

        IEnumerable<ApplicationRole> GetGroupRoles(int groupId);

        Task<IEnumerable<ApplicationRole>> GetGroupRolesAsync(int groupId);

        IQueryable<ApplicationGroup> GetGroups();

        IEnumerable<ApplicationUser> GetGroupUsers(int groupId);

        Task<IEnumerable<ApplicationUser>> GetGroupUsersAsync(int groupId);

        IEnumerable<ApplicationGroupRole> GetUserGroupProfiles(int userId);

        Task<IEnumerable<ApplicationGroupRole>> GetUserGroupProfilesAsync(int userId);

        IEnumerable<ApplicationGroup> GetUserGroups(int userId);

        Task<IEnumerable<ApplicationGroup>> GetUserGroupsAsync(int userId);

        IdentityResult RefreshUserGroupProfiles(int userId);

        Task<IdentityResult> RefreshUserGroupProfilesAsync(int userId);

        IdentityResult SetGroupRoles(int groupId, params string[] roleNames);

        Task<IdentityResult> SetGroupRolesAsync(int groupId, params string[] roleNames);

        IdentityResult SetUserGroups(int userId, params int[] groupIds);

        Task<IdentityResult> SetUserGroupsAsync(int userId, params int[] groupIds);

        IdentityResult UpdateGroup(ApplicationGroup group);

        Task<IdentityResult> UpdateGroupAsync(ApplicationGroup group);
    }

    public class GroupService : IGroupService
    {
        private readonly ApplicationDbContext db;
        private ApplicationRoleManager _roleManager;
        private ApplicationUserManager _userManager;

        public GroupService(ApplicationDbContext db)
        {
            this.db = db;
            _roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            _userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
        }

        public IdentityResult ClearUserGroups(int userId)
        {
            return SetUserGroups(userId, new int[] { });
        }

        public async Task<IdentityResult> ClearUserGroupsAsync(int userId)
        {
            return await SetUserGroupsAsync(userId, new int[] { });
        }

        public IdentityResult CreateGroup(ApplicationGroup group)
        {
            db.ApplicationGroups.Add(group);
            db.SaveChanges();
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> CreateGroupAsync(ApplicationGroup group)
        {
            db.ApplicationGroups.Add(group);
            await db.SaveChangesAsync();
            return IdentityResult.Success;
        }

        public ApplicationGroup FindById(int id)
        {
            return db.ApplicationGroups.Find(id);
        }

        public async Task<ApplicationGroup> FindByIdAsync(int id)
        {
            return await db.ApplicationGroups.SingleAsync(x => x.Id == id);
        }

        public ApplicationGroup GetApplicationGroup(int groupId)
        {
            return db.ApplicationGroups.Include(x => x.ApplicationGroupRoles).SingleOrDefault(x => x.Id == groupId);
        }

        public async Task<ApplicationGroup> GetApplicationGroupAsync(int id)
        {
            return await db.ApplicationGroups.Include(x => x.ApplicationGroupRoles).Where(x => x.Id == id).SingleAsync();
        }

        public IQueryable<ApplicationGroup> GetApplicationGroups()
        {
            return db.ApplicationGroups;
        }

        public IEnumerable<ApplicationRole> GetGroupRoles(int groupId)
        {
            var grp = db.ApplicationGroups.FirstOrDefault(g => g.Id == groupId);
            var roles = _roleManager.Roles.ToList();
            var groupRoles = from r in roles
                             where grp.ApplicationGroupRoles.Any(ap => ap.ApplicationRoleId == r.Id)
                             select r;
            return groupRoles;
        }

        public async Task<IEnumerable<ApplicationRole>> GetGroupRolesAsync(int groupId)
        {
            var grp = await db.ApplicationGroups.FirstOrDefaultAsync(g => g.Id == groupId);
            var roles = await _roleManager.Roles.ToListAsync();
            var groupRoles = (from r in roles
                              where grp.ApplicationGroupRoles.Any(ap => ap.ApplicationRoleId == r.Id)
                              select r).ToList();
            return groupRoles;
        }

        public IQueryable<ApplicationGroup> GetGroups()
        {
            return db.ApplicationGroups;
        }

        public IEnumerable<ApplicationUser> GetGroupUsers(int groupId)
        {
            var group = db.ApplicationGroups.Include(x => x.ApplicationGroupUsers).Single(x => x.Id == groupId);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in group.ApplicationGroupUsers)
            {
                var user = db.Users.Find(groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }

        public async Task<IEnumerable<ApplicationUser>> GetGroupUsersAsync(int groupId)
        {
            var group = await db.ApplicationGroups.Include(x => x.ApplicationGroupUsers).SingleAsync(x => x.Id == groupId);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in group.ApplicationGroupUsers)
            {
                var user = await db.Users.FirstOrDefaultAsync(u => u.Id == groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }

        public IEnumerable<ApplicationGroupRole> GetUserGroupProfiles(int userId)
        {
            var userGroups = GetUserGroups(userId);
            var UserGroupProfiles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                UserGroupProfiles.AddRange(group.ApplicationGroupRoles.ToArray());
            }
            return UserGroupProfiles;
        }

        public async Task<IEnumerable<ApplicationGroupRole>> GetUserGroupProfilesAsync(int userId)
        {
            var userGroups = await GetUserGroupsAsync(userId);
            var UserGroupProfiles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                UserGroupProfiles.AddRange(group.ApplicationGroupRoles.ToArray());
            }
            return UserGroupProfiles;
        }

        public IEnumerable<ApplicationGroup> GetUserGroups(int userId)
        {
            var result = new List<ApplicationGroup>();
            var userGroups = (from g in db.ApplicationGroups
                              where g.ApplicationGroupUsers.Any(u => u.ApplicationUserId == userId)
                              select g).ToList();
            return userGroups;
        }

        public async Task<IEnumerable<ApplicationGroup>> GetUserGroupsAsync(int userId)
        {
            var result = new List<ApplicationGroup>();
            var userGroups = (from g in db.ApplicationGroups
                              where g.ApplicationGroupUsers.Any(u => u.ApplicationUserId == userId)
                              select g).ToListAsync();
            return await userGroups;
        }

        public IdentityResult RefreshUserGroupProfiles(int userId)
        {
            var user = _userManager.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            // Remove user from previous roles:
            var oldUserRoles = _userManager.GetRoles(userId);
            if (oldUserRoles.Count > 0)
            {
                _userManager.RemoveFromRoles(userId, oldUserRoles.ToArray());
            }

            // Find teh roles this user is entitled to from group membership:
            var newGroupRoles = GetUserGroupProfiles(userId);

            // Get the damn role names:
            var allRoles = _roleManager.Roles.ToList();
            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            _userManager.AddToRoles(userId, roleNames);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> RefreshUserGroupProfilesAsync(int userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            // Remove user from previous roles:
            var oldUserRoles = await _userManager.GetRolesAsync(userId);
            if (oldUserRoles.Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(userId, oldUserRoles.ToArray());
            }

            // Find the roles this user is entitled to from group membership:
            var newGroupRoles = await GetUserGroupProfilesAsync(userId);

            // Get the damn role names:
            var allRoles = await _roleManager.Roles.ToListAsync();
            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            await _userManager.AddToRolesAsync(userId, roleNames);

            return IdentityResult.Success;
        }

        public IdentityResult SetGroupRoles(int groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            var thisGroup = db.ApplicationGroups.Include(x => x.ApplicationGroupRoles).Single(x => x.Id == groupId);
            db.ApplicationGroupRoles.RemoveRange(thisGroup.ApplicationGroupRoles);
            db.SaveChanges();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                db.ApplicationGroupRoles.Add(new ApplicationGroupRole { ApplicationGroupId = groupId, ApplicationRoleId = role.Id });
            }
            db.SaveChanges();

            var affectedUsers = db.ApplicationGroupUsers.Where(x => x.ApplicationGroupId == groupId);

            // Reset the roles for all affected users:
            foreach (var groupUser in affectedUsers)
            {
                RefreshUserGroupProfiles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> SetGroupRolesAsync(int groupId, params string[] roleNames)
        {
            var currentGroups = db.ApplicationGroupRoles.Where(x => x.ApplicationGroupId == groupId);
            db.ApplicationGroupRoles.RemoveRange(currentGroups);
            await db.SaveChangesAsync();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));

            foreach (var role in newRoles)
                db.ApplicationGroupRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id
                });
            await db.SaveChangesAsync();

            var groupUsers = db.ApplicationGroupUsers.Where(x => x.ApplicationGroupId == groupId);
            // Reset the roles for all affected users:
            foreach (var groupUser in groupUsers)
            {
                await RefreshUserGroupProfilesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }

        public IdentityResult SetUserGroups(int userId, params int[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = db.ApplicationGroupUsers.Where(x => x.ApplicationUserId == userId);
            db.ApplicationGroupUsers.RemoveRange(currentGroups);
            db.SaveChanges();

            foreach (var groupId in groupIds)
                db.ApplicationGroupUsers.Add(new ApplicationGroupUser
                {
                    ApplicationUserId = userId,
                    ApplicationGroupId = groupId
                });
            db.SaveChanges();

            RefreshUserGroupProfiles(userId);
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> SetUserGroupsAsync(int userId, params int[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = db.ApplicationGroupUsers.Where(x => x.ApplicationUserId == userId);
            db.ApplicationGroupUsers.RemoveRange(currentGroups);

            await db.SaveChangesAsync();

            // Add the user to the new groups:
            foreach (var groupId in groupIds)
            {
                db.ApplicationGroupUsers.Add(new ApplicationGroupUser { ApplicationUserId = userId, ApplicationGroupId = groupId });
            }
            await db.SaveChangesAsync();

            await RefreshUserGroupProfilesAsync(userId);
            return IdentityResult.Success;
        }

        public IdentityResult UpdateGroup(ApplicationGroup group)
        {
            db.ApplicationGroups.AddOrUpdate(group);
            db.SaveChanges();
            var userGroups = db.ApplicationGroupUsers.Where(x => x.ApplicationGroupId == group.Id);
            foreach (var groupUser in userGroups)
            {
                RefreshUserGroupProfiles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateGroupAsync(ApplicationGroup group)
        {
            db.ApplicationGroups.AddOrUpdate(group);
            await db.SaveChangesAsync();
            var userGroups = db.ApplicationGroupUsers.Where(x => x.ApplicationGroupId == group.Id);
            foreach (var groupUser in userGroups)
            {
                await RefreshUserGroupProfilesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }
    }
}