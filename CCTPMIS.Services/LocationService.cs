﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;

namespace CCTPMIS.Services
{
    using System;
    using Microsoft.AspNet.Identity;

    public interface ILocationService
    {
        double GetDistance(double lat1, double lon1, double lat2, double lon2, char unit);
    }

    public class LocationService : ILocationService
    {
        // ::: This routine calculates the distance between two points(given the     :::
        // :::  latitude/longitude of those points). It is being used to calculate     :::
        // :::  the distance between two locations using GeoDataSource(TM) products    :::
        // :::                                                                         :::
        // :::  Definitions:                                                           :::
        // :::    South latitudes are negative, east longitudes are positive           :::
        // :::                                                                         :::
        // :::  Passed to function:                                                    :::
        // :::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
        // :::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
        // :::    unit = the unit you desire for results                               :::
        // :::           where: 'M' is statute miles                                   :::
        // :::                  'K' is kilometers (default)                            :::
        // :::                  'N' is nautical miles                                  :::
        // :::                                                                         :::
        public double GetDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            var theta = lon1 - lon2;
            var dist = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2))
                       + Math.Cos(Deg2rad(lat1)) * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
            dist = Math.Acos(dist);
            dist = Rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }

            return dist;
        }

        // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // ::  This function converts decimal degrees to radians             :::
        // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Deg2rad(double deg) => deg * Math.PI / 180.0;

        // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // ::  This function converts radians to decimal degrees             :::
        // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double Rad2deg(double rad) => rad / Math.PI * 180.0;
         
      
    }
}