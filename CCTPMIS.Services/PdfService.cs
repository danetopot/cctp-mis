﻿using System.Linq;
using System.Web.Configuration;

namespace CCTPMIS.Services
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;

    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using iTextSharp.tool.xml;
    using iTextSharp.tool.xml.html;
    using iTextSharp.tool.xml.parser;
    using iTextSharp.tool.xml.pipeline.css;
    using iTextSharp.tool.xml.pipeline.end;
    using iTextSharp.tool.xml.pipeline.html;

    using Image = iTextSharp.text.Image;

    public interface IPdfService
    {
        void ExportToPDF(string url, string reportTitle, string paperKind = "A3");

        string SaveToPDF(string url, string reportTitle, string filePath, string paperKind = "A3");

        PdfPTable SetHeader(string title);

        string GetQueryString(object obj);
    }

    public class PdfService : IPdfService
    {
        public void ExportToPDF(string url, string reportTitle, string paperKind = "A3")
        {
            var request = HttpContext.Current.Request;

            var helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
          //  var rootUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
          var  rootUrl = WebConfigurationManager.AppSettings["SYSTEM_LINK"] + "/";
            // rootUrl += helper.Content("~/");

            url = rootUrl + url;
            url = url.Replace(" ", "-");
            string HTMLContent = string.Empty;
            using (var client = new WebClient())
            {
                HTMLContent = client.DownloadString(url);
            }

            var title = reportTitle + ".pdf";
            HTMLContent = HTMLContent.Replace("<br>", "<br />");
            var pdf = GeneratePdf(HTMLContent, paperKind);

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ContentType = "application/pdf";

            // HttpContext.Current.Response.AddHeader("Content-Disposition",string.Format("attachmentfilename=\"" + title + "\"; size={0}", pdf.Length));
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + title + "\"");
            HttpContext.Current.Response.BinaryWrite(pdf);

            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public string SaveToPDF(string url, string reportTitle, string filePath, string paperKind = "A4")
        {
            var request = HttpContext.Current.Request;

            var helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var rootUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            rootUrl += helper.Content("~/");
            url = rootUrl + url;
            url = url.Replace(" ", "-");
            string HTMLContent = string.Empty;
            using (var client = new WebClient())
            {
                HTMLContent = client.DownloadString(url);
            }

            var title = reportTitle + ".pdf";
            HTMLContent = HTMLContent.Replace("<br>", "<br />");
            var pdf = GeneratePdf(HTMLContent, paperKind);
            File.WriteAllBytes(HttpContext.Current.Server.MapPath(filePath + title), pdf);
            return title;
        }

        public PdfPTable SetHeader(string title)
        {
            var pdfPTable = new PdfPTable(numColumns: 4)
            {
                WidthPercentage = 100,
                HorizontalAlignment = 0,
                SpacingBefore = 0,
                SpacingAfter = 0
            };

            var cellLogo = new PdfPCell { Border = 0, };
            var logo = Image.GetInstance(System.Web.Hosting.HostingEnvironment.MapPath("~/uploads/images/logo.png"));
            logo.Alignment = Element.ALIGN_CENTER;
            logo.ScaleAbsolute(90, 90);
            cellLogo.AddElement(logo);
            pdfPTable.AddCell(cellLogo);
            var pdfTitleCell = new PdfPCell { Border = 0, Colspan = 3 };
            var pdfTitleParagraph = new Paragraph { Alignment = Element.ALIGN_CENTER };
            title = "\n" + title;
            var chunkTitle = new Chunk(title, FontFactory.GetFont(FontFactory.COURIER, 16, Font.BOLD, BaseColor.BLACK));
            pdfTitleParagraph.Add(chunkTitle);
            pdfTitleCell.AddElement(pdfTitleParagraph);
            pdfPTable.AddCell(pdfTitleCell);
            return pdfPTable;
        }

        private byte[] GeneratePdf(string html, string paperKind)
        {
            byte[] bytesArray = null;
            using (var ms = new MemoryStream())
            {
                var document = new Document(PageSize.A3, 30, 30, 30, 30);
                if (paperKind.Equals("A4"))
                {
                    document = new Document(PageSize.A4, 30, 30, 30, 30);
                }

                using (PdfWriter writer = PdfWriter.GetInstance(document, ms))
                {
                    writer.PageEvent = new ITextEvents();
                    document.Open();
                    using (var strReader = new StringReader(html))
                    {
                        // Set factories
                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                        // Set css
                        ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);
                        cssResolver.AddCssFile(HttpContext.Current.Server.MapPath(@"~/Content/export.css"), true);

                        // Export
                        IPipeline pipeline = new CssResolverPipeline(
                            cssResolver,
                            new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
                        var worker = new XMLWorker(pipeline, true);
                        var xmlParse = new XMLParser(true, worker);
                        xmlParse.Parse(strReader);
                        xmlParse.Flush();
                    }

                    document.Close();
                }

                bytesArray = ms.ToArray();
            }

            return bytesArray;
        }

        public string GetQueryString(object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }

    public class ITextEvents : PdfPageEventHelper
    {
        private string _header;

        // this is the BaseFont we are going to use for the header / footer
        private BaseFont bf = null;

        // This is the contentbyte object of the writer
        private PdfContentByte cb;

        private PdfTemplate footerTemplate;

        // we will put the final number of pages in a template
        private PdfTemplate headerTemplate;

        // This keeps track of the creation time
        private DateTime PrintTime = DateTime.Now;

        public string Header
        {
            get
            {
                return _header;
            }

            set
            {
                _header = value;
            }
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            footerTemplate.BeginText();
            footerTemplate.SetFontAndSize(bf, 6);
            footerTemplate.SetTextMatrix(0, 0);
            footerTemplate.ShowText((writer.CurrentPageNumber - 1).ToString());
            footerTemplate.EndText();
        }

        //public override void OnEndPage(PdfWriter writer, Document document)
        //{
        //    base.OnEndPage(writer, document);

        //    // Create PdfTable object
        //    PdfPTable pdfTab = new PdfPTable(3);

        //    // We will have to create separate cells to include image logo and 2 separate strings
        //    // Row 1
        //    string text = "Page " + writer.PageNumber + " of ";
        //    {
        //        // Add paging to footer
        //        cb.BeginText();
        //        cb.SetColorFill(BaseColor.GRAY);
        //        cb.SetFontAndSize(bf, 6);
        //        var half = document.PageSize.Width / 2;
        //        cb.SetTextMatrix(document.PageSize.GetRight(half), document.PageSize.GetBottom(15));
        //        cb.ShowText(text);
        //        cb.EndText();
        //        float len = bf.GetWidthPoint(text, 6);
        //        cb.AddTemplate(footerTemplate, document.PageSize.GetRight(half) + len, document.PageSize.GetBottom(15));

        //        ////Move the pointer and draw line to separate footer section from rest of page
        //        // cb.MoveTo(40, document.PageSize.GetBottom(50) );
        //        // cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50));
        //        // cb.Stroke();
        //    }
        //}

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
           // var logService = new LogService();
            try
            {
                PrintTime = DateTime.Now;
                bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                headerTemplate = cb.CreateTemplate(100, 100);
                footerTemplate = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
                // logService.FileLog
            }
            catch (IOException ioe)
            {
            }
        }
    }
}