﻿using System.Drawing;

namespace CCTPMIS.Services
{
    public class ImagesService
    {
        public Image ResizeByWidth(Image Img, int NewWidth)
        {
            float PercentW = ((float)Img.Width / (float)NewWidth);

            Bitmap bmp = new Bitmap(NewWidth, (int)(Img.Height / PercentW));
            Graphics g = Graphics.FromImage(bmp);

            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(Img, 0, 0, bmp.Width, bmp.Height);
            g.Dispose();

            return bmp;
        }
        public void GetThumb(string destination, string destination_thumb, int width, bool delete)
        {
            System.IO.FileStream fs;
            fs = new System.IO.FileStream(destination, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            Image Img = Image.FromStream(fs);
            Img = ResizeByWidth(Img, width);
            Img.Save(destination_thumb);
            fs.Close();
            if (delete == true)
            {
                System.IO.File.Delete(destination);
            }

        }
        public Image ResizeByHeight(Image Img, int NewHeight)
        {
            float PercentH = ((float)Img.Height / (float)NewHeight);

            Bitmap bmp = new Bitmap((int)(Img.Width / PercentH), NewHeight);
            Graphics g = Graphics.FromImage(bmp);

            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(Img, 0, 0, bmp.Width, bmp.Height);
            g.Dispose();

            return bmp;
        }
    }
}