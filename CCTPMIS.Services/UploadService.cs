﻿namespace CCTPMIS.Services
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;

    using Business.Context;

    public interface IUploadService
    {
        bool DeleteDirectoryFiles(string targetDir, string extTodelete);

        bool isImage(HttpPostedFileBase file);
    }

    public class UploadService : IUploadService
    {
        private readonly ApplicationDbContext db;

        public UploadService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public bool DeleteDirectoryFiles(string targetDir, string extTodelete)
        {
            const bool result = false;

            var files = Directory.GetFiles(targetDir);
            var dirs = Directory.GetDirectories(targetDir);
            var extArrTodelete = extTodelete.Split(',');
            foreach (var file in from file in files
                                 from deleteFile in extArrTodelete.Where(
                                     deleteFile => deleteFile.Contains(Path.GetExtension(file)))
                                 select file)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            return result;
        }

        public bool isImage(HttpPostedFileBase file)
        {
            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
    }
}