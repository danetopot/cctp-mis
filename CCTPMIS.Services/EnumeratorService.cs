﻿using System.Collections.Generic;

namespace CCTPMIS.Services
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using Microsoft.AspNet.Identity;
    using Business.Context;


    public interface IEnumeratorService
    {
        int ActivateEnumerator(int id);

        int DeactivateEnumerator(int id, int userId);

        string GeneratePassword(int maxSize);

        int LockEnumerator(int id);

        string Random(string type);

        int UnLockEnumerator(int id);
    }

    public class EnumeratorService : IEnumeratorService
    {
        private readonly ApplicationDbContext db;

        public EnumeratorService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public int ActivateEnumerator(int id)
        {
            var enumerator = db.Enumerators.Single(a => a.Id == id);
            if (enumerator != null)
            {
                enumerator.LockoutEnabled = true;
                enumerator.IsActive = true;
                enumerator.AccessFailedCount = 0;
                enumerator.LockoutEndDateUtc = DateTime.Now;
                db.Enumerators.AddOrUpdate(enumerator);
                db.SaveChanges();
            }
            else
                id = 0;

            return id;
        }

        public int DeactivateEnumerator(int id, int userId)
        {
            var enumerator = db.Enumerators.Single(a => a.Id == id);
            if (enumerator != null)
            {
                enumerator.LockoutEnabled = true;
                enumerator.AccessFailedCount = 6;
                enumerator.IsActive = false;
                enumerator.LockoutEndDateUtc = DateTime.Now.AddYears(100);
                enumerator.DeactivatedBy = userId;
                enumerator.DeactivatedOn = DateTime.Now;
                db.Enumerators.AddOrUpdate(enumerator);
                db.SaveChanges();
            }
            else
                id = 0;

            return id;
        }

        public string GeneratePin(int maxSize)
        {
            var passwords = string.Empty;
            var chArray1 = new char[52];
            var chArray2 = "1234567890".ToCharArray();
            var data1 = new byte[1];

            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetNonZeroBytes(data1);
                var data2 = new byte[maxSize];
                cryptoServiceProvider.GetNonZeroBytes(data2);
                var stringBuilder = new StringBuilder(maxSize);
                foreach (var num in data2)
                    stringBuilder.Append(chArray2[(int)num % chArray2.Length]);
                passwords = stringBuilder.ToString();
                var randomPass = Shuffle(passwords);
                return randomPass.Substring(0, maxSize);
            }
        }

        public string GeneratePassword(int maxSize)
        {
            var passwords = string.Empty;
            var chArray1 = new char[52];
            var chArray2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^*()_+".ToCharArray();
            var data1 = new byte[1];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetNonZeroBytes(data1);
                var data2 = new byte[maxSize];
                cryptoServiceProvider.GetNonZeroBytes(data2);
                var stringBuilder = new StringBuilder(maxSize);
                foreach (var num in data2)
                    stringBuilder.Append(chArray2[(int)num % chArray2.Length]);
                passwords = stringBuilder.ToString();
                return Shuffle(Random("N") + passwords +  Random("S") + Random("l"));
            }
        }

        public int LockEnumerator(int id)
        {
            var enumerator = db.Enumerators.Single(a => a.Id == id);
            if (enumerator != null)
            {
                enumerator.LockoutEnabled = true;
                enumerator.AccessFailedCount = 6;
                enumerator.LockoutEndDateUtc = DateTime.Now.AddYears(100);
                db.Enumerators.AddOrUpdate(enumerator);
                db.SaveChanges();
            }
            else
                id = 0;

            return id;
        }





        public   string Shuffle(string list)
        {
            Random R = new Random();
            int index;
            List<char> chars = new List<char>(list);
            StringBuilder sb = new StringBuilder();
            while (chars.Count > 0)
            {
                index = R.Next(chars.Count);
                sb.Append(chars[index]);
                chars.RemoveAt(index);
            }
            return sb.ToString();
        }

        public string Random(string type)
        {
            var data2 = new byte[2];
            var passwords = string.Empty;
            switch (type)
            {
                case "N":
                    {
                        var charArray = "0123456789";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "l":
                    {
                        var charArray = "abcdefghijklmnopqrstuvwxyz";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "C":
                    {
                        var charArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }

                case "S":
                    {
                        var charArray = "!@#$%^&*()_+-={}|[]:;<>?,./";
                        var stringBuilder = new StringBuilder(2);
                        foreach (var num in data2)
                            stringBuilder.Append(charArray[(int)num % charArray.Length]);
                        passwords = stringBuilder.ToString();
                        return passwords;
                    }
            }

            return string.Empty;
        }

        public int UnLockEnumerator(int id)
        {
            var enumerator = db.Enumerators.Single(a => a.Id == id);
            if (enumerator != null)
            {
                enumerator.LockoutEnabled = true;
                enumerator.AccessFailedCount = 0;
                enumerator.LockoutEndDateUtc = DateTime.Now.AddDays(-1);
                db.Enumerators.AddOrUpdate(enumerator);
                db.SaveChanges();
            }
            else
                id = 0;

            return id;
        }
    }
}