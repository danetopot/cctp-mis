﻿using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.ViewModels;
using FluentValidation;

namespace CCTPMIS.Mobile.Validators
{

    public class LoginValidator : AbstractValidator<LoginViewModel>
    {
        public LoginValidator()
        {
            RuleFor(x => x.NationalId).NotEmpty().Matches(@"^[0-9]{1,8}$").WithMessage("National ID No. is required.");
            RuleFor(x => x.Pin).NotEmpty().Matches(@"^[0-9]{1,6}$").WithMessage("Pin is required.");
        }
    }

    public class ComValListingValidator : AbstractValidator<ComValListingPlanHH>
    {
        public ComValListingValidator()
        {
            RuleFor(x => x.ProgrammeId).NotNull().WithMessage("Programme is Required");
            RuleFor(x => x.LocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Location is Required");
            RuleFor(x => x.SubLocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Sub Location is Required");
            RuleFor(x => x.Village).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Village Name");
            RuleFor(x => x.Years).NotNull().GreaterThan(-1).LessThanOrEqualTo(100).WithMessage("Select the Years");
            RuleFor(x => x.Months).NotNull().GreaterThan(-1).LessThanOrEqualTo(12).WithMessage("Select the Months");
            RuleFor(x => x.NearestReligiousBuilding).NotEmpty().Matches(@"^[A-Za-z0-9_'--/~!@#$%^&* ]{1,100}$").When(x => x.NearestSchool == null).WithMessage("Check Nearest Religious Building");
            RuleFor(x => x.NearestSchool).NotEmpty().Matches(@"^[A-Za-z0-9_'--/~!@#$%^&* ]{1,100}$").When(x => x.NearestReligiousBuilding == null).WithMessage("Check Nearest School");
            RuleFor(x => x.PhysicalAddress).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Physical Address"); ;
            RuleFor(x => x.HouseholdMembers).NotEmpty().GreaterThan(0).WithMessage("Check Household Members");
            RuleFor(x => x.CgFirstName).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgSurname).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgPhoneNumber).NotEmpty().Matches(@"^[0-9+-- ]{1,10}$").WithMessage("Check Caregiver Phone Number").MaximumLength(10);
            RuleFor(x => x.CgSexId).NotNull().GreaterThan(0).WithMessage("Caregiver Sex is required");
            RuleFor(x => x.CgMiddleName).Length(0, 100).Matches(@"^[A-Za-z_'-- ]{1,100}$").WithMessage("Check Caregiver Middle Name"); //.Unless(x => x.CgMiddleName == null);
            RuleFor(x => x.BeneFirstName).MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneFirstName == null).WithMessage("Check Beneficiary First Name");
            RuleFor(x => x.BeneSurname).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneSurname == null).WithMessage("Check Beneficiary Surname");
            RuleFor(x => x.BenePhoneNumber).Matches(@"^[0-9+-- ]{1,10}$").WithMessage("Check Beneficiary Phone Number").Unless(x => x.BenePhoneNumber == null).MaximumLength(10);
            RuleFor(x => x.BeneSexId).GreaterThan(0).WithMessage("Beneficiary Sex is required").Unless(x => x.BeneSexId == null);
            RuleFor(x => x.BeneMiddleName).Length(0, 100).Matches(@"^[A-Za-z_'-- ]{1,100}$").WithMessage("Check Beneficiary Middle Name"); //.Unless(x => x.BeneMiddleName == null);
            RuleFor(x => x.BeneNationalIdNo).Matches(@"^[0-9]{1,8}$").WithMessage("Check Beneficiary National Id No.").Unless(x => x.BeneNationalIdNo == null);
            RuleFor(x => x.CgNationalIdNo).NotEmpty().Matches(@"^[0-9]{1,8}$").WithMessage("Check Caregiver National Id No.");
        }
    }

    public class ComValListingExitValidator : AbstractValidator<ComValListingPlanHH>
    {
        public ComValListingExitValidator()
        {
            RuleFor(x => x.ExitReason).NotNull().WithMessage("Exit Reason is Required");
        }
    }
}