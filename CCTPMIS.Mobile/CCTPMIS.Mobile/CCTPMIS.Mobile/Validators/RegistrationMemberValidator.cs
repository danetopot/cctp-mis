﻿using CCTPMIS.Mobile.Database;
using FluentValidation;

namespace CCTPMIS.Mobile.Validators
{

    public class RegistrationMemberValidator : AbstractValidator<RegistrationMember>
    {
        public RegistrationMemberValidator()
        {
            RuleFor(x => x.FirstName).MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,100}$").WithMessage("Check  First Name");
            RuleFor(x => x.Surname).Matches(@"^[A-Za-z_'-- ]{1,100}$").WithMessage("Check  Surname");
            RuleFor(x => x.MiddleName).Matches(@"^[A-Za-z_'-- ]{1,100}$").Unless(x => x.MiddleName == null || x.MiddleName.Length == 0).WithMessage("Check  Middle Name"); 
            RuleFor(x => x.PhoneNumber).Matches(@"^[0-9+-- ]{1,10}$").WithMessage("Check 3.01(b)").MaximumLength(10).WithMessage("Check 3.01(b) ").Unless(x => string.IsNullOrEmpty(x.PhoneNumber));
            RuleFor(x => x.IdentificationNumber).Matches(@"^[0-9]{1,8}$").WithMessage("3.02 (b) Response is required.").Unless(x => string.IsNullOrEmpty(x.IdentificationNumber));
            RuleFor(x => x.RelationshipId).NotNull().GreaterThan(0).WithMessage("3.03 Response is required");
            RuleFor(x => x.SexId).NotNull().GreaterThan(0).WithMessage("3.04 Response is required");
            RuleFor(x => x.DateOfBirth).NotEmpty().WithMessage("Check 3.05");
            RuleFor(x => x.MaritalStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.06");
            RuleFor(x => x.FatherAliveStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.08");
            RuleFor(x => x.MotherAliveStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.09");
            RuleFor(x => x.ChronicIllnessStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.10 Response");
            RuleFor(x => x.DisabilityTypeId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.11 Response");
            RuleFor(x => x.DisabilityCareStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.12 Response").Unless(x=>x.DisabilityCareStatusId==null);
            RuleFor(x => x.DisabilityCareStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.12 Response").Unless(x => x.DisabilityCareStatusId == null);
            RuleFor(x => x.CareGiverId).NotNull().WithMessage("Check 3.13 Response").Unless(x=> string.IsNullOrEmpty(x.CareGiverId));
            RuleFor(x => x.LearningStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.14 Response").When(x => x.Age > 2);
            RuleFor(x => x.EducationLevelId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.15 Response").When(x => x.Age > 2 && x.LearningStatusId !=373 && x.LearningStatusId != 374);
            RuleFor(x => x.WorkTypeId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.16 Response").When(x => x.Age > 5);
            RuleFor(x => x.FormalJobNgoId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.17 Response").When(x => x.Age > 5);
        }
    }
}