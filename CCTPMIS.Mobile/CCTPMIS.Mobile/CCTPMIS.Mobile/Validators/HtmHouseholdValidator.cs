﻿using CCTPMIS.Mobile.Database;
using FluentValidation;

namespace CCTPMIS.Mobile.Validators
{
    public class HtmHouseholdValidator : AbstractValidator<Registration>
    {
        public HtmHouseholdValidator()
        {
            RuleFor(x => x.ProgrammeId).NotEmpty().GreaterThan(0).WithMessage("Programme is Required");
            RuleFor(x => x.LocationId).NotEmpty().GreaterThan(0).WithMessage("Location is Required");
            RuleFor(x => x.SubLocationId).NotEmpty().GreaterThan(0).WithMessage("Sub Location is Required");
            RuleFor(x => x.Village).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Village Name");
            RuleFor(x => x.Years).NotNull().GreaterThan(-1).LessThanOrEqualTo(100).WithMessage("Select the Years");
            RuleFor(x => x.Months).NotNull().GreaterThan(-1).LessThanOrEqualTo(12).WithMessage("Select the Months");

            RuleFor(x => x.NearestReligiousBuilding).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest Religious Building");
            RuleFor(x => x.NearestSchool).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest School"); ;
            RuleFor(x => x.PhysicalAddress).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Physical Address"); ;
            RuleFor(x => x.HouseholdMembers).NotEmpty().GreaterThan(0).WithMessage("Check Household Members");
            RuleFor(x => x.HabitableRooms).NotNull().GreaterThan(0).WithMessage("Check Habitable Rooms");

            RuleFor(x => x.TenureStatusId).NotNull().GreaterThan(0).WithMessage("Check Tenure Status");
            RuleFor(x => x.IsOwnedId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Tenure Status");

            RuleFor(x => x.RoofConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.FloorConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.WallConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.DwellingUnitRiskId).GreaterThan(0).NotNull();
            RuleFor(x => x.WaterSourceId).GreaterThan(0).NotNull();
            RuleFor(x => x.WasteDisposalModeId).GreaterThan(0).NotNull();
            RuleFor(x => x.CookingFuelTypeId).GreaterThan(0).NotNull();
            RuleFor(x => x.LightingFuelTypeId).GreaterThan(0).NotNull();

            RuleFor(x => x.IsTelevisionId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Television");
            RuleFor(x => x.IsMotorcycleId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Motor cycle");
            RuleFor(x => x.IsTukTukId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check TukTuk");
            RuleFor(x => x.IsRefrigeratorId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Refrigerator");
            RuleFor(x => x.IsCarId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Car");
            RuleFor(x => x.IsMobilePhoneId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check  Mobile Phone");
            RuleFor(x => x.IsBicycleId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Bicycle");

            RuleFor(x => x.ExoticCattle).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Exotic Cattle");
            RuleFor(x => x.IndigenousCattle).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Indigenous Cattle");
            RuleFor(x => x.Sheep).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Sheep");
            RuleFor(x => x.Goats).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Goats");
            RuleFor(x => x.Camels).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Camels");
            RuleFor(x => x.Donkeys).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check  Donkeys");
            RuleFor(x => x.Pigs).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Pigs");
            RuleFor(x => x.Chicken).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Chicken");

            RuleFor(x => x.LiveBirths).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Live births");
            RuleFor(x => x.Deaths).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Deaths");
            RuleFor(x => x.HouseHoldConditionId).GreaterThan(0).NotNull();

            RuleFor(x => x.IsSkippedMealId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Is Skipped Meal");
            RuleFor(x => x.NsnpProgrammesId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check  N.S.N.P. Programmes");
            RuleFor(x => x.IsReceivingSocialId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Is Receiving Social");
            RuleFor(x => x.Programmes).NotEmpty().WithMessage("Check Social Programmes External Support Programmes List").When(x => x.IsReceivingSocialId > 0);
            RuleFor(x => x.InKindBenefit).NotNull().WithMessage("Check In Kind Benefit").When(x => x.BenefitTypeId == 2 || x.BenefitTypeId == 3);
            RuleFor(x => x.LastReceiptAmount).NotNull().GreaterThanOrEqualTo(0).When(x => x.BenefitTypeId == 1).WithMessage("Check Last Receipt Amount");
        }
    }
}