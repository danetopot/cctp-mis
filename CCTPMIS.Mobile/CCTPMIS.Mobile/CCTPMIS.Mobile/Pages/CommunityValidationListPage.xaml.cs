﻿using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommunityValidationListPage : TabbedPage
    {
        private ComValListViewModel ViewModel => vm ?? (vm = BindingContext as ComValListViewModel);
        private ComValListViewModel vm;
        //private bool showFavs, showPast, showAllCategories;
        //private string filteredCategories;
        private ToolbarItem filterItem;
        private string loggedIn;

        public CommunityValidationListPage()
        {
            InitializeComponent();
            loggedIn = Settings.Current.Email;

            BindingContext = vm = new ComValListViewModel(Navigation);

            ComValCleanHHs.VerticalOptions = LayoutOptions.FillAndExpand;
            ComValExceptionHHs.VerticalOptions = LayoutOptions.FillAndExpand;
            UploadReady.VerticalOptions = LayoutOptions.FillAndExpand;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is ComValListViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                vm.ComValCleanHHs.ReplaceRange(bindingContext.GetComValCleanHHs());
                vm.ComValExceptionHHs.ReplaceRange(bindingContext.GetComValExceptionHHs());
                vm.ComValReadyHHs.ReplaceRange(bindingContext.GetComValReadyHHs());
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.ComValCleanHHs.Clear();
            vm.ComValExceptionHHs.Clear();
            vm.ComValReadyHHs.Clear();

        }
    }
}