﻿using System;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationMemberPage : ContentPage
    {
        RegistrationMemberViewModel ViewModel => vm ?? (vm = BindingContext as RegistrationMemberViewModel);
        private RegistrationMemberViewModel vm;

        public RegistrationMemberPage(int hhId, string memberId)
        {
            InitializeComponent();
            BindingContext = new RegistrationMemberViewModel(Navigation, hhId, memberId);


            //var Year = DateTime.Now.Year + 1;
            //var minDate = Year - 1900;
            //var mincgOPCTyr = new DateTime(Year - minDate, 1, 1);
            //DateOfBirth.SetValue(DatePicker.MaximumDateProperty, DateTime.Now);
            //DateOfBirth.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
            //DateOfBirth.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);

        }

        private void DateOfBirth_OnDateSelected(object sender, DateChangedEventArgs e)
        {
            var date = e.NewDate;
            var today = DateTime.Now;
            var threeYearsAgo = new DateTime(today.Year - 3, today.Month, today.Day);
            var fiveYearsAgo  = new DateTime(today.Year - 3, today.Month, today.Day);
            if (!(BindingContext is RegistrationListViewModel bindingContext)) return;
            if (vm == null) return;
            vm.IsOver5 = (date < fiveYearsAgo);
            vm.IsOver3 = (date < threeYearsAgo);
        }
    }
}