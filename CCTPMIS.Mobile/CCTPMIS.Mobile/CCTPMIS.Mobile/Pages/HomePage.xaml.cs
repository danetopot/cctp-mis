﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
	    private HomeViewModel vm;

	    public HomePage()
	    {
	        InitializeComponent();

	        BindingContext = vm = new HomeViewModel(Navigation);
	    }
    }
}