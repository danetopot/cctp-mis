﻿using System;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HouseholdListingEditPage : ContentPage
	{
		public HouseholdListingEditPage (int? id = null)
		{
		    Title = !id.HasValue ? $"New Household Listing Data" : $"Edit Household #{id}";
            InitializeComponent ();

		    InitializeComponent();
		    BindingContext = new HouseholdListingAddEditViewModel(Navigation);

		    var Year = DateTime.Now.Year + 1;
		    var minOPCTyr = new DateTime(Year - 70, 1, 1);
		    BeneDoB.SetValue(DatePicker.MaximumDateProperty, minOPCTyr);
		    var minDate = Year - 1900;
		    var mincgOPCTyr = new DateTime(Year - minDate, 1, 1);

		    CgDoB.SetValue(DatePicker.MaximumDateProperty, DateTime.Now.AddYears(-18));

		    CgDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
		    BeneDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
        }
	}
}