﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationDetailPage : TabbedPage
    {
        public RegistrationDetailPage (int id)
        {
            InitializeComponent();
            BindingContext = new RegistrationDetailViewModel(Navigation, id);
        }
    }
}