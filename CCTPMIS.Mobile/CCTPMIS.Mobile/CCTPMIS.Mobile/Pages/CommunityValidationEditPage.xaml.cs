﻿using System;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommunityValidationEditPage : ContentPage
	{
		public CommunityValidationEditPage(int id)
		{
			InitializeComponent();
			BindingContext = new ComValEditViewModel(Navigation, id);


			var Year = DateTime.Now.Year + 1;
			var minOPCTyr = new DateTime(Year - 70, 1, 1);
		 
			var minDate = Year - 1900;
			var mincgOPCTyr = new DateTime(Year - minDate, 1, 1);

			CgDoB.SetValue(DatePicker.MaximumDateProperty, DateTime.Now.AddYears(-18));
			CgDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);

			BeneDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
			BeneDoB.SetValue(DatePicker.MaximumDateProperty, minOPCTyr);

		}
	}
}