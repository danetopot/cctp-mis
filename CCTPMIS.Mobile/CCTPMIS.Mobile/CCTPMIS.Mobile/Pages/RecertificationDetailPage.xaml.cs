﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecertificationDetailPage : TabbedPage
    {
        public RecertificationDetailPage(int id)
        {
            InitializeComponent();
            BindingContext = new RecertificationDetailViewModel(Navigation, id);
        }
    }
}