﻿using CCTPMIS.Mobile.Models;
using CCTPMIS.Mobile.Pages;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.HomePage, Title="Home" },
                new HomeMenuItem {Id = MenuItemType.HouseHoldListingPage, Title="Household Listing" },
                new HomeMenuItem {Id = MenuItemType.CommunityValidationPage, Title="Community Validation " },
                new HomeMenuItem {Id = MenuItemType.RegistrationPage, Title="Household Registration " },
                new HomeMenuItem {Id = MenuItemType.RecertificationPage, Title="Household Re-certification " },
                new HomeMenuItem {Id = MenuItemType.SyncPage, Title="Data Transfer" },
                new HomeMenuItem {Id = MenuItemType.LogoutPage, Title="Logout" },
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}