﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationEditPage : ContentPage
    {
        public RegistrationEditPage (int id)
        {
            InitializeComponent();
            BindingContext = new RegistrationEditViewModel(Navigation, id);

          //  NsNpProgs.SetBinding(ListView.ItemsSourceProperty, "Pokemons");
        }
    }
}