﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SyncPage : TabbedPage
	{
	    private SyncViewModel vm;

	    public SyncPage()
	    {
	        InitializeComponent();
	        BindingContext = vm = new SyncViewModel(Navigation);
	    }
    }
}