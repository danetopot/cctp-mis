﻿using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecertificationListPage : TabbedPage
    {
        RecertificationListViewModel ViewModel => vm ?? (vm = BindingContext as RecertificationListViewModel);
        private RecertificationListViewModel vm;
        //private bool showFavs, showPast, showAllCategories;
        //private string filteredCategories;
        private ToolbarItem filterItem;
        private string loggedIn;

        public RecertificationListPage()
        {
            InitializeComponent();
            loggedIn = Settings.Current.Email;

            BindingContext = vm = new RecertificationListViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is RecertificationListViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                vm.DownloadedRecertifications.ReplaceRange(bindingContext.GetDownloadedRecertifications());
                vm.OngoingRecertifications.ReplaceRange(bindingContext.GetOngoingRecertifications());
                vm.CompleteRecertifications.ReplaceRange(bindingContext.GetCompleteRecertifications());
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.DownloadedRecertifications.Clear();
            vm.OngoingRecertifications.Clear();
            vm.CompleteRecertifications.Clear();

        }
    }
}