﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecertificationEditPage : ContentPage
    {
        public RecertificationEditPage(int id)
        {
            InitializeComponent();
            BindingContext = new RecertificationEditViewModel(Navigation, id);

            //  NsNpProgs.SetBinding(ListView.ItemsSourceProperty, "Pokemons");
        }
    }
}