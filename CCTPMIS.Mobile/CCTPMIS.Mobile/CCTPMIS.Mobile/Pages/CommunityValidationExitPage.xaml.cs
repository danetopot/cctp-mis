﻿using System;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommunityValidationExitPage : ContentPage
	{
		public CommunityValidationExitPage (int id)
		{
            InitializeComponent();
            BindingContext = new ComValExitViewModel(Navigation, id);
        }
	}
}