﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HouseholdListingDetailPage : ContentPage
	{
		public HouseholdListingDetailPage (int id)
		{
			InitializeComponent ();
		    BindingContext = new HouseholdListingDetailViewModel(Navigation, id);
        }
	}
}