﻿using System;
using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecertificationMemberPage : ContentPage
    {
        RecertificationMemberViewModel ViewModel => vm ?? (vm = BindingContext as RecertificationMemberViewModel);
        private RecertificationMemberViewModel vm;

        public RecertificationMemberPage(int hhId, string memberId)
        {
            InitializeComponent();
            BindingContext = new RecertificationMemberViewModel(Navigation, hhId, memberId);
            
        }

        private void DateOfBirth_OnDateSelected(object sender, DateChangedEventArgs e)
        {
            var date = e.NewDate;
            var today = DateTime.Now;
            var threeYearsAgo = new DateTime(today.Year - 3, today.Month, today.Day);
            var fiveYearsAgo = new DateTime(today.Year - 3, today.Month, today.Day);
            if (!(BindingContext is RecertificationListViewModel bindingContext)) return;
            if (vm == null) return;
            vm.IsOver5 = (date < fiveYearsAgo);
            vm.IsOver3 = (date < threeYearsAgo);
        }
    }
}