﻿using CCTPMIS.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommunityValidationDetailPage : ContentPage
	{
	    public CommunityValidationDetailPage(int id)
	    {
	        InitializeComponent();
	        BindingContext = new ComValDetailsViewModel(Navigation, id);
	    }
	}
}