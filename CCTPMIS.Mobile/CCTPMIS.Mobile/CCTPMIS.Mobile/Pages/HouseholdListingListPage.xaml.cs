﻿using System;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.ViewModels;
using FormsToolkit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HouseHoldListingListPage : ContentPage
    {

        HouseholdListingViewModel ViewModel => vm ?? (vm = BindingContext as HouseholdListingViewModel);
        private HouseholdListingViewModel vm;
        private bool showFavs, showPast, showAllCategories;
        private string filteredCategories;
        private ToolbarItem filterItem;
        private string loggedIn;

        public HouseHoldListingListPage()
        {
            InitializeComponent();

            loggedIn = Settings.Current.Email;
            BindingContext = vm = new HouseholdListingViewModel(Navigation);
            vm.Listings.Clear();
            vm.Listings.AddRange(vm.GetListings());

            ListingsGrid.VerticalOptions = LayoutOptions.FillAndExpand;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            vm.Listings.Clear();
            vm.Listings.AddRange(vm.GetListings());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.Listings.Clear();
        }

        private async void NewHouseHold_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new NavigationPage(new HouseholdListingEditPage()));
                vm.Listings.Clear();
            }
            catch (Exception exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Load New Household Listing Page",
                    Message = exception.Message,
                    Cancel = "OK"
                });
            }
        }
    }
}
