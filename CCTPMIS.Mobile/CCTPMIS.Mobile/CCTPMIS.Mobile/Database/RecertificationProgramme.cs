﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class RecertificationProgramme
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int Id { get; set; }
        public int RecertificationId { get; set; }
        public int ProgrammeId { get; set; }

    }
}