﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class Programme
    {
        [PrimaryKey, Unique]
        public int Id { get; set; }

        public string Name { get; set; }
        public int BeneficiaryTypeId { get; set; }
        public string Code { get; set; }
        public int PrimaryRecipientId { get; set; }
        public int? SecondaryRecipientId { get; set; }
        public bool SecondaryRecipientMandatory { get; set; }
        public bool IsActive { get; set; }
    }
}