﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class SubLocation
    {
        public string Code { get; set; }

        [PrimaryKey, Unique]
        public int Id { get; set; }

        public int LocationId { get; set; }

        public string Name { get; set; }
    }
}