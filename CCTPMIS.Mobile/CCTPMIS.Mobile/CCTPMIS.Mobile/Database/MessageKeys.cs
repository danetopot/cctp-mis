﻿namespace CCTPMIS.Mobile.Database
{
    public static class MessageKeys
    {
        public const string Choice = "choice";

        public const string Connection = "connection";

        public const string Error = "error";

        public const string LoggedIn = "loggedin";

        public const string Message = "message";

        public const string NavigateLogin = "navigate_login";

        public const string NavigateToEvent = "navigate_event";

        public const string NavigateToImage = "navigate_image";

        public const string NavigateToRegistration = "navigate_registration";
        public const string NavigateToCvRegistration = "navigate_cv_registration";

        public const string NavigateToSpeaker = "navigate_speaker";

        public const string NavigateToSponsor = "navigate_sponsor";

        public const string Question = "question";
    }
}