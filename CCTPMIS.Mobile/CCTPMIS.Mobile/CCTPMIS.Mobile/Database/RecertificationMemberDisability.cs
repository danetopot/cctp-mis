﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class RecertificationMemberDisability
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int Id { get; set; }
        public string RecertificationMemberId { get; set; }
        public int DisabilityId { get; set; }

        public int RecertificationId { get; set; }
    }
}