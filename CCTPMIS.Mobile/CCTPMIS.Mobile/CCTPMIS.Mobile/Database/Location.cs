﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class Location
    {
        public string Code { get; set; }
         public string Name { get; set; }

        [PrimaryKey,Unique]
        public int Id { get; set; }
    }
}
