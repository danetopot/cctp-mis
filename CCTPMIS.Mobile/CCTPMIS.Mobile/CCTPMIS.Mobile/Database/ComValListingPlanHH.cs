﻿using System.Text;
using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class ComValListingPlanHH
    {
        [PrimaryKey,Unique]
        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public byte ProgrammeId { get; set; }
        public string RegDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneNationalIdNo { get; set; }
        public string BenePhoneNumber { get; set; }
        public int? BeneSexId { get; set; }
        public string BeneDoB { get; set; }
        public string BeneDoBDate { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public string CgPhoneNumber { get; set; }
        public int CgSexId { get; set; }
        public string CgDoB { get; set; }
        public string CgDoBDate { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int SyncEnumeratorId { get; set; }
        public int DownEnumeratorId { get; set; }

        public string CgFullName => $"{CgFirstName}  {CgMiddleName} {CgSurname}";

        public string BeneFullName => $"{BeneFirstName}  {BeneMiddleName} {BeneSurname}";

        public string DownloadDate { get; set; }

        public string GeoPosition => $"[ {Longitude}, {Latitude}]";
        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        public string ReferenceNo => $" Targeting ID.: #{Id}".ToUpper();

        [Ignore]
        public Location Location { get; set; }

        [Ignore]
        public Programme Programme { get; set; }

        public string LocationName => $"{Location?.Name}";
        public string ProgrammeName => $"{Programme?.Name}";

        [Ignore]
        public SubLocation SubLocation { get; set; }

        public string SubLocationName => $"{SubLocation?.Name}";

        public string DurationDisplay => $"{Years} Years, {Months} Months";

        [Ignore]
        public SystemCodeDetail CgSex { get; set; }

        [Ignore]
        public SystemCodeDetail BeneSex { get; set; }

        public string CgSexName => $"{CgSex?.Description}";
        public string BeneSexName => $"{BeneSex?.Description}";
        public string ComValDate { get; set; }
        public int CgMatches { get; set; }
        public int BeneMatches { get; set; }
        public int HasBene { get; set; }

        public string ExitReason { get; set; }

        private const string delimiter = " ";
        private string haystack;
        [Newtonsoft.Json.JsonIgnore]
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;

                var builder = new StringBuilder();
                builder.Append(delimiter);
                builder.Append(Village);
                builder.Append(delimiter);
                builder.Append(CgNationalIdNo);
                builder.Append(delimiter);
                builder.Append(BeneNationalIdNo);
                builder.Append(delimiter);
                builder.Append(Location?.Name);
                builder.Append(delimiter);
                builder.Append(SubLocation?.Name);
                builder.Append(delimiter);
                builder.Append(Programme?.Name);
                builder.Append($"{CgFirstName}{delimiter}{CgMiddleName}{delimiter}{CgSurname}");
                builder.Append(delimiter);
                builder.Append($"{BeneFirstName}{delimiter}{BeneMiddleName}{delimiter}{BeneSurname}");
                haystack = builder.ToString();
                return haystack;
            }
        }
    }
}