﻿using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class SystemCodeDetail
    {
        public string Code { get; set; }

        public string Description { get; set; }

        [PrimaryKey, Unique]
        public int Id { get; set; }

        public byte OrderNo { get; set; }

        public int SystemCodeId { get; set; }
    }
}