﻿using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SQLite;

namespace CCTPMIS.Mobile.Database
{
    public class Registration
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        [Ignore]
        public Programme Programme { get; set; }

        public int ProgrammeId { get; set; }
        public string RegistrationDate { get; set; }
        public int LocationId { get; set; }

        [Ignore]
        public Location Location { get; set; }

        public int SubLocationId { get; set; }

        [Ignore]
        public SubLocation SubLocation { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }

        [Ignore]
        public Enumerator Enumerator { get; set; }

        public string HHdFullName { get; set; }
        public string CgFullName { get; set; }

        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }

        [Ignore]
        public SystemCodeDetail Status { get; set; }

        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        [Ignore]
        public Enumerator SyncEnumerator { get; set; }
        [Ignore,JsonIgnore]
        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        [Ignore, JsonIgnore]
        public string ReferenceNo => $"Reference No: #{Id}".ToUpper();
        [Ignore, JsonIgnore]
        public string LocationName => $"{Location?.Name}";
        [Ignore, JsonIgnore]
        public string ProgrammeName => $"{Programme?.Name}";
        [Ignore, JsonIgnore]
        public string SubLocationName => $"{SubLocation?.Name}";
        [Ignore, JsonIgnore]
        public string DurationDisplay => $"{Years} Years, {Months} Months";

        public int IsTelevisionId { get; set; }
        
        public int IsMotorcycleId { get; set; }
        public int IsTukTukId { get; set; }
        public int IsRefrigeratorId { get; set; }
        public int IsCarId { get; set; }
        public int IsMobilePhoneId { get; set; }
        public int IsBicycleId { get; set; }



        public int ExoticCattle { get; set; }
        public int IndigenousCattle { get; set; }
        public int Sheep { get; set; }
        public int Goats { get; set; }
        public int Camels { get; set; }
        public int Donkeys { get; set; }
        public int Pigs { get; set; }
        public int Chicken { get; set; }
        public int LiveBirths { get; set; }
        public int Deaths { get; set; }


        public int IsSkippedMealId { get; set; }
        public int IsReceivingSocialId { get; set; }
        public string Programmes { get; set; }
        public decimal LastReceiptAmount { get; set; }
        public string InKindBenefitId { get; set; }
        public int WasteDisposalModeId { get; set; }
        public int WaterSourceId { get; set; }
        public int WallConstructionMaterialId { get; set; }
        public int IsOwnedId { get; set; }
        public int TenureStatusId { get; set; }
        public int RoofConstructionMaterialId { get; set; }
        public string OtherProgrammeNames { get; set; }
        public int NsnpProgrammesId { get; set; }
        public int OtherProgrammesId { get; set; }
        public int LightingFuelTypeId { get; set; }
        public string IsMonetary { get; set; }
        public int HabitableRooms { get; set; }
        public int HouseHoldConditionId { get; set; }
        public int FloorConstructionMaterialId { get; set; }
        public int DwellingUnitRiskId { get; set; }
        public int CookingFuelTypeId { get; set; }
        public int BenefitTypeId { get; set; }
        public int? InterviewStatusId { get; set; }
        public int? InterviewResultId { get; set; }



        
        public string DownloadDate { get; set; }
        [Ignore]
        public ICollection<RegistrationMember> RegistrationMembers { get; set; }


        [Ignore]
        public ICollection<RegistrationMemberDisability> RegistrationMemberDisabilities { get; set; }

        [Ignore]
        public ICollection<RegistrationProgramme> RegistrationProgrammes { get; set; }


        public string RegDate1 { get; set; }
        public string RegDate2 { get; set; }
        public string RegDate3 { get; set; }
 
        private const string delimiter = " ";
        private string haystack;
        [Newtonsoft.Json.JsonIgnore] 
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;
                var builder = new StringBuilder();
                builder.Append(delimiter);
                builder.Append(Id);
                builder.Append(delimiter);
                builder.Append(Village);
                builder.Append(delimiter);
                builder.Append(PhysicalAddress);
                builder.Append(delimiter);
                builder.Append(NearestReligiousBuilding);
                builder.Append(delimiter);
                builder.Append(NearestSchool);
                haystack = builder.ToString();
                return haystack;
            }
        }


        [Ignore, JsonIgnore]
        public SystemCodeDetail IsTelevision { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsMotorcycle { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsTukTuk { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsRefrigerator { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsCar { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsMobilePhone { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsBicycle { get; set; }
        [Ignore, JsonIgnore]
        public SystemCodeDetail IsSkippedMeal { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsReceivingSocial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail InKindBenefit { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail WasteDisposalMode { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail WaterSource { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail WallConstructionMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail IsOwned { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail TenureStatus { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail RoofConstructionMaterial { get; set; }


        [Ignore, JsonIgnore]
        public SystemCodeDetail NsnpProgrammes { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail OtherProgrammes { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail LightingFuelType { get; set; }



        [Ignore, JsonIgnore]
        public SystemCodeDetail HouseHoldCondition { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail FloorConstructionMaterial { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail DwellingUnitRisk { get; set; }



        [Ignore, JsonIgnore]
        public SystemCodeDetail CookingFuelType { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail BenefitType { get; set; }

        [Ignore, JsonIgnore]
        public SystemCodeDetail InterviewStatus { get; set; }
         
        [Ignore]
        public SystemCodeDetail InterviewResult { get; set; }
    }
}