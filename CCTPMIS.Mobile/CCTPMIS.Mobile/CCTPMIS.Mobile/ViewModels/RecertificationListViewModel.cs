﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Pages;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    public class RecertificationListViewModel : LocalBaseViewModel
    {
        public INavigation Navigation;

        public RecertificationListViewModel(INavigation navigation) : base(navigation)
        {
            DownloadedRecertifications.ReplaceRange(GetDownloadedRecertifications());
            OngoingRecertifications.ReplaceRange(GetOngoingRecertifications());
            CompleteRecertifications.ReplaceRange(GetCompleteRecertifications());
            Navigation = navigation;

            PendingInterview = App.Database.SystemCodeDetailGetByCode("Interview Status", "00").Id;
            OngoingInterview = App.Database.SystemCodeDetailGetByCode("Interview Status", "01").Id;
            CompleteInterview = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
        }

        public int PendingInterview;
        public int OngoingInterview;
        public int CompleteInterview;

        public ObservableCollection<Recertification> GetCompleteRecertifications()
        {
            var items = App.Database.GetTable("Recertification");
            var hh = new ObservableCollection<Recertification>();
            foreach (var item in items)
            {
                var hhitem = (Recertification)item;
                if (hhitem.InterviewStatusId == CompleteInterview)
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableCollection<Recertification> GetOngoingRecertifications()
        {
            var items = App.Database.GetTable("Recertification");
            var hh = new ObservableCollection<Recertification>();
            foreach (var item in items)
            {
                var hhitem = (Recertification)item;
                if (hhitem.InterviewStatusId == OngoingInterview)
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableCollection<Recertification> GetDownloadedRecertifications()
        {
            var items = App.Database.GetTable("Recertification");
            var hh = new ObservableCollection<Recertification>();
            foreach (var item in items)
            {
                var hhitem = (Recertification)item;
                if (hhitem.InterviewStatusId == PendingInterview)
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableRangeCollection<Recertification> CompleteRecertifications { get; } = new ObservableRangeCollection<Recertification>();
        public ObservableRangeCollection<Recertification> OngoingRecertifications { get; } = new ObservableRangeCollection<Recertification>();
        public ObservableRangeCollection<Recertification> DownloadedRecertifications { get; } = new ObservableRangeCollection<Recertification>();











        private ICommand forceRefreshDownloadedCommand;

        public ICommand ForceRefreshDownloadedCommand => forceRefreshDownloadedCommand ?? (forceRefreshDownloadedCommand = new Command(async () => await ExecuteForceRefreshDownloadedCommandAsync()));

        private async Task ExecuteForceRefreshDownloadedCommandAsync()
        {
            ExecuteCompleteRecertifications();
        }
        private void ExecuteCompleteRecertifications()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetCompleteRecertifications().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                CompleteRecertifications.ReplaceRange(loaded);
            }
            else
            {
                CompleteRecertifications.ReplaceRange(GetCompleteRecertifications());
            }

            IsBusy = false;
        }


        private ICommand forceRefreshOngoingCommand;

        public ICommand ForceRefreshOngoingCommand => forceRefreshOngoingCommand ?? (forceRefreshOngoingCommand = new Command(async () => await ExecuteForceRefreshOngoingCommandAsync()));

        private async Task ExecuteForceRefreshOngoingCommandAsync()
        {
            ExecuteOngoingRecertifications();
        }
        private void ExecuteOngoingRecertifications()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetOngoingRecertifications().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                OngoingRecertifications.ReplaceRange(loaded);
            }
            else
            {
                OngoingRecertifications.ReplaceRange(GetOngoingRecertifications());
            }

            IsBusy = false;
        }

        private ICommand forceRefreshCompleteCommand;

        public ICommand ForceRefreshCompleteCommand => forceRefreshCompleteCommand ?? (forceRefreshCompleteCommand = new Command(async () => await ExecuteForceRefreshCompleteCommandAsync()));

        private async Task ExecuteForceRefreshCompleteCommandAsync()
        {
            ExecuteDownloadedRecertifications();
        }




        private void ExecuteDownloadedRecertifications()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetDownloadedRecertifications().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                DownloadedRecertifications.ReplaceRange(loaded);
            }
            else
            {
                DownloadedRecertifications.ReplaceRange(GetDownloadedRecertifications());
            }

            IsBusy = false;
        }


        #region Properties

        private Recertification selectedRecertification;

        public Recertification SelectedRecertification
        {
            get { return selectedRecertification; }
            set
            {
                selectedRecertification = value;
                OnPropertyChanged();
                if (selectedRecertification == null)
                    return;
                Navigation.PushAsync(new RecertificationDetailPage(selectedRecertification.Id));

                SelectedRecertification = null;
            }
        }



        #endregion Properties

        #region Filtering and Sorting



        private bool noRecertificationsFound;

        public bool NoRecertificationsFound
        {
            get { return noRecertificationsFound; }
            set { SetProperty(ref noRecertificationsFound, value); }
        }

        private string noRecertificationsFoundMessage;

        public string NoRecertificationsFoundMessage
        {
            get { return noRecertificationsFoundMessage; }
            set { SetProperty(ref noRecertificationsFoundMessage, value); }
        }

        #endregion Filtering and Sorting

        #region Commands








        #endregion Commands

        private readonly INavigation navigation;

        #region Properties

        private Recertification selectedDownloaded;

        public Recertification SelectedDownloaded
        {
            get { return selectedDownloaded; }
            set
            {
                selectedDownloaded = value;
                OnPropertyChanged();
                if (selectedDownloaded != null)
                {
                    Navigation.PushAsync(new RecertificationDetailPage(selectedDownloaded.Id));
                    return;
                }
            }
        }

        private Recertification selectedOngoing;

        public Recertification SelectedOngoing
        {
            get { return selectedOngoing; }
            set
            {
                selectedOngoing = value;
                OnPropertyChanged();
                if (selectedOngoing != null)
                {
                    Navigation.PushAsync(new RecertificationDetailPage(selectedOngoing.Id));
                    return;
                }
            }
        }

        private Recertification selectedComplete;

        public Recertification SelectedComplete
        {
            get { return selectedComplete; }
            set
            {
                selectedComplete = value;
                OnPropertyChanged();
                if (selectedComplete != null)
                {
                    Navigation.PushAsync(new RecertificationDetailPage(selectedOngoing.Id));
                    return;
                }
            }
        }

        #endregion Properties
    }
}
