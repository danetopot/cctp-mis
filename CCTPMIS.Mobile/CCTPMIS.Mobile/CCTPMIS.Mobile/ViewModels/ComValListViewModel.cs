﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Pages;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    public class ComValListViewModel : LocalBaseViewModel
    {
        public INavigation Navigation;

        public ComValListViewModel(INavigation navigation) : base(navigation)
        {

            ExecuteComValCleanHHs();
            ExecuteComValExceptionHHs();            
            ExecuteComValReadyHHs();
            Navigation = navigation;
        }

        public ObservableCollection<ComValListingPlanHH> GetComValCleanHHs()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH)item;
                if (string.IsNullOrEmpty(hhitem.ComValDate) && hhitem.CgMatches == 1 && (hhitem.BeneMatches == 1 || hhitem.HasBene == 0))
                    hh.Add(hhitem);
            }
            return hh;
        }
        public ObservableCollection<ComValListingPlanHH> GetComValExceptionHHs()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH)item;
                if (string.IsNullOrEmpty(hhitem.ComValDate) && (hhitem.CgMatches == 0 || (hhitem.BeneMatches == 0 && hhitem.HasBene == 1)))
                    hh.Add(hhitem);
            }
            return hh;
        }
        public ObservableCollection<ComValListingPlanHH> GetComValReadyHHs()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH)item;
                if (!string.IsNullOrEmpty(hhitem.ComValDate))
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableRangeCollection<ComValListingPlanHH> ComValCleanHHs { get; } = new ObservableRangeCollection<ComValListingPlanHH>();
        public ObservableRangeCollection<ComValListingPlanHH> ComValExceptionHHs { get; } = new ObservableRangeCollection<ComValListingPlanHH>();
        public ObservableRangeCollection<ComValListingPlanHH> ComValReadyHHs { get; } = new ObservableRangeCollection<ComValListingPlanHH>();


        //private string filter = string.Empty;

        //public string Filter
        //{
        //    get { return filter; }
        //    set
        //    {
        //        if (SetProperty(ref filter, value))
        //        {

        //        }
                    
        //    }
        //}
        private ICommand forceRefreshCleanCommand;

        public ICommand ForceRefreshCleanCommand => forceRefreshCleanCommand ?? (forceRefreshCleanCommand = new Command(async () => await ExecuteForceRefreshCleanCommandAsync()));

        private async Task ExecuteForceRefreshCleanCommandAsync()
        {
           ExecuteComValCleanHHs();
        }
        private void ExecuteComValCleanHHs()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetComValCleanHHs().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                ComValCleanHHs.ReplaceRange(loaded);
            }
            else
            {
                ComValCleanHHs.ReplaceRange(GetComValCleanHHs());
            }

            IsBusy = false;
        }


        private ICommand forceRefreshExceptionCommand;

        public ICommand ForceRefreshExceptionCommand => forceRefreshExceptionCommand ?? (forceRefreshExceptionCommand = new Command(async () => await ExecuteForceRefreshExceptionCommandAsync()));

        private async Task ExecuteForceRefreshExceptionCommandAsync()
        {
              ExecuteComValExceptionHHs();
        }
        private void ExecuteComValExceptionHHs()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetComValExceptionHHs().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                ComValExceptionHHs.ReplaceRange(loaded);
            }
            else
            {
                ComValExceptionHHs.ReplaceRange(GetComValExceptionHHs());
            }

            IsBusy = false;
        }

        private ICommand forceRefreshReadyCommand;

        public ICommand ForceRefreshReadyCommand => forceRefreshReadyCommand ?? (forceRefreshReadyCommand = new Command(async () => await ExecuteForceRefreshReadyCommandAsync()));

        private async Task ExecuteForceRefreshReadyCommandAsync()
        {
            ExecuteComValReadyHHs( );
        }




        private void ExecuteComValReadyHHs()
        {
            IsBusy = true;
            if (!string.IsNullOrEmpty(Filter))
            {
                var loaded = GetComValReadyHHs().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                ComValReadyHHs.ReplaceRange(loaded);
            }
            else
            {
                ComValReadyHHs.ReplaceRange(GetComValReadyHHs());
            }

            IsBusy = false;
        }


        #region Properties

        private ComValListingPlanHH selectedRegistration;

        public ComValListingPlanHH SelectedRegistration
        {
            get { return selectedRegistration; }
            set
            {
                selectedRegistration = value;
                OnPropertyChanged();
                if (selectedRegistration == null)
                    return;

                MessagingService.Current.SendMessage(MessageKeys.NavigateToRegistration, selectedRegistration);

                SelectedRegistration = null;
            }
        }

       

        #endregion Properties

        #region Filtering and Sorting

       

        private bool noComValListingPlanHHsFound;

        public bool NoComValListingPlanHHsFound
        {
            get { return noComValListingPlanHHsFound; }
            set { SetProperty(ref noComValListingPlanHHsFound, value); }
        }

        private string noComValListingPlanHHsFoundMessage;

        public string NoComValListingPlanHHsFoundMessage
        {
            get { return noComValListingPlanHHsFoundMessage; }
            set { SetProperty(ref noComValListingPlanHHsFoundMessage, value); }
        }

        #endregion Filtering and Sorting

        #region Commands

         

       




        #endregion Commands

        private readonly INavigation navigation;

        #region Properties

        private ComValListingPlanHH selectedClean;

        public ComValListingPlanHH SelectedClean
        {
            get { return selectedClean; }
            set
            {
                selectedClean = value;
                OnPropertyChanged();
                if (selectedClean != null)
                {
                    Navigation.PushAsync(new CommunityValidationDetailPage(selectedClean.Id));
                    return;
                }
            }
        }

        private ComValListingPlanHH selectedException;

        public ComValListingPlanHH SelectedException
        {
            get { return selectedException; }
            set
            {
                selectedException = value;
                OnPropertyChanged();
                if (selectedException != null)
                {
                    Navigation.PushAsync(new CommunityValidationDetailPage(selectedException.Id));
                    return;
                }
            }
        }

        private ComValListingPlanHH selectedReady;

        public ComValListingPlanHH SelectedReady
        {
            get { return selectedReady; }
            set
            {
                selectedReady = value;
                OnPropertyChanged();
                if (selectedReady != null)
                {
                    Navigation.PushAsync(new CommunityValidationDetailPage(selectedReady.Id));
                    return;
                }
            }
        }

        #endregion Properties
    }
}
