﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Converters;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.Pages;
using FormsToolkit;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
   public class ComValDetailsViewModel:  LocalBaseViewModel
    {
        public ComValDetailsViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("ComValListingPlanHH", "Id", id.ToString());
            Registration = (ComValListingPlanHH)data;
            Registration.Programme = (Programme)App.Database.GetTableRow("Programme", "id", Registration.ProgrammeId.ToString());
            Registration.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", Registration.SubLocationId.ToString());
            Registration.Location = (Location)App.Database.GetTableRow("Location", "id", Registration.LocationId.ToString());
            Registration.CgSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Registration.CgSexId.ToString());
            if (Registration.BeneSexId != null)
                Registration.BeneSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Registration.BeneSexId.ToString());
            Navigation = navigation;

            IsShowBene = Registration.Programme.Code == "OPCT";

            IsClean = ( Registration.CgMatches == 1  && (Registration.BeneMatches == 1 || Registration.HasBene == 0));

            if(Registration.ComValDate == null)
            {
                IsProcessed = false;
                IsNotProcessed = true;
            }
            else
            {
                IsProcessed = true;
                IsNotProcessed = false;
            }
            //IsProcessed = !string.IsNullOrEmpty(Registration.ComValDate.ToString());
            //IsNotProcessed = string.IsNullOrEmpty(Registration.ComValDate.ToString());
            IsNotClean = !IsClean;
        }

        private bool isShowBene;
        public bool IsShowBene
        {
            get { return isShowBene; }
            set { SetProperty(ref isShowBene, value); }
        }


        public bool IsClean { get; set; }
        public bool IsNotClean { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsNotProcessed { get; set; }

        public ComValListingPlanHH Registration { get; set; }
        public int Id { get; set; }

        private ICommand _correctRegistrationCommand;
        private ICommand _confirmRegistrationCommand;
        private ICommand _exitRegistrationCommand;
        public INavigation Navigation;

        

        public ICommand CorrectRegistrationCommand => _correctRegistrationCommand ??
                                                      (_correctRegistrationCommand = new Command(async () =>
                                                          await ExecuteCorrectRegistration()));

        public ICommand ConfirmRegistrationCommand => _confirmRegistrationCommand ?? (_confirmRegistrationCommand = new Command(async () => await ExecuteConfirmRegistration()));
        public ICommand ExitRegistrationCommand => _exitRegistrationCommand ?? (_exitRegistrationCommand = new Command(async () => await ExecuteExitRegistration()));

        private async Task ExecuteCorrectRegistration()
        {
            try
            {
                var reg = this.Registration;
                App.Database.AddOrUpdate(reg);
                await Navigation.PushAsync(new CommunityValidationEditPage(reg.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        private async Task ExecuteConfirmRegistration()
        {
            try
            {
                var reg = this.Registration;
                reg.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REGCONFIRM").Id;
                reg.EnumeratorId = Settings.Current.EnumeratorId;
                reg.ComValDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                App.Database.AddOrUpdate(reg);
                await Navigation.PopToRootAsync(true);
                await Navigation.PushAsync(new CommunityValidationListPage());
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        private async Task ExecuteExitRegistration()
        {
            try
            {
                var reg = this.Registration;
                App.Database.AddOrUpdate(reg);
                await Navigation.PushAsync(new CommunityValidationExitPage(reg.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            /*
            try
            {
                var reg = this.Registration;
                reg.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REGEXIT").Id;
                reg.EnumeratorId = Settings.Current.EnumeratorId;
                reg.ComValDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                App.Database.AddOrUpdate(reg);
                await Navigation.PopToRootAsync(true);
                await Navigation.PushAsync(new CommunityValidationListPage());
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            */
        }
    }
}
