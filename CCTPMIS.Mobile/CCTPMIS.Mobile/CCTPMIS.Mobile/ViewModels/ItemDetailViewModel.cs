﻿using CCTPMIS.Mobile.Models;

namespace CCTPMIS.Mobile.ViewModels
{
    public class ItemDetailViewModel : LocalBaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
    }
}
