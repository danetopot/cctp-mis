﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Converters;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.Interface;
using CCTPMIS.Mobile.Models;
using CCTPMIS.Mobile.Pages;
using FormsToolkit;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    public class SyncViewModel : LocalBaseViewModel
    {
        #region Properties

        private IAppClient client;
        public INavigation Navigation;

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        public SyncViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>();
            email = Settings.Current.Email;
            password = Settings.Current.Password;

            SyncedDownloadListingTime = Settings.Current.LastSyncDown;
            SyncedUploadListingTime = Settings.Current.LastSync;
            IsBusyListing = false;

            SyncDownListingText = "Download Household Listing Data From the Server";
            SyncUpListingText = "Upload Captured Household Listing Data";

            SyncDownComValText = "Download Community Validation Data from Server";
            SyncUpComValText = "Upload Community Validation Data To Server";

            SyncDownRegistrationText = "Download Registration Data from Server";
            SyncUpRegistrationText = "Upload Registration Data To Server";


            SyncDownRecertificationText = "Download Re-certification Data from Server";
            SyncUpRecertificationText = "Upload Re-certification Data To Server";

            Navigation = navigation;
        }

        private int localListing;

        public int LocalListing
        {
            get { return localListing; }
            set { SetProperty(ref localListing, value); }
        }

        private DateTime syncedUploadListingTime;

        public DateTime SyncedUploadListingTime
        {
            get { return syncedUploadListingTime; }
            set { SetProperty(ref syncedUploadListingTime, value); }
        }

        private DateTime syncedDownloadListingTime;

        public DateTime SyncedDownloadListingTime
        {
            get { return syncedDownloadListingTime; }
            set { SetProperty(ref syncedDownloadListingTime, value); }
        }

        #endregion Properties

        #region Household Listing Sync

        private string syncUpListingText;

        public string SyncUpListingText
        {
            get { return syncUpListingText; }
            set { SetProperty(ref syncUpListingText, value); }
        }

        private string syncDownListingText;

        public string SyncDownListingText
        {
            get { return syncDownListingText; }
            set { SetProperty(ref syncDownListingText, value); }
        }

        private bool isBusyListing = false;

        public bool IsBusyListing
        {
            get
            {
                return isBusyListing;
            }
            set
            {
                SetProperty(ref isBusyListing, value);
            }
        }

        private string logListing;

        public string LogListing
        {
            get { return logListing; }
            set { SetProperty(ref logListing, value); }
        }

        private string messageListing;

        public string MessageListing
        {
            get { return messageListing; }
            set { SetProperty(ref messageListing, value); }
        }

        private ICommand syncUpListingCommand;
        public ICommand SyncUpListingCommand => syncUpListingCommand ?? (syncUpListingCommand = new Command(async () => await ExecuteSyncUpListingAsync()));

        private async Task ExecuteSyncUpListingAsync()
        {
            if (IsBusyListing)
                return;
            MessageListing = "Establishing Secure Connection... ...";
            SyncUpListingText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your Internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                IsBusyListing = true;
                var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());
                MessageListing = "Uploading Sync Data  ...";
                SyncUpListingText = "Uploading Data";
                await Task.Delay(1000);
                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var localdata = App.Database.GetTable("Listing");
                if (localdata.Any())
                {
                    var toSync = localdata.Count();
                    var syncedRecords = 0;
                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    foreach (var item in localdata)
                    {
                        var Listing = (Listing)item;
                        Listing.SyncEnumeratorId = Settings.Current.EnumeratorId;
                        MessageListing = $"Processing Record Ref.#{Listing.Id} ... ";
                        await Task.Delay(1000);
                        var feedback = await client.PostListing(Listing, model);

                        if (feedback.StatusId == null)
                        {
                            Settings.Current.FirstRun = true;
                            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                new MessagingServiceAlert
                                {
                                    Title = "Sync Failed",
                                    Message = "Kindly Try Again!",
                                    Cancel = "OK"
                                });
                            await Navigation.PushModalAsync(new LoginPage());
                            return;
                        }
                        else
                        {
                            if (feedback.StatusId == 0)
                            {
                                syncedRecords += 1;
                                LocalListing = LocalListing - 1;
                                SyncedUploadListingTime = DateTime.Now;

                                if (deleteonSync == "1")
                                {
                                    App.Database.Delete<Listing>(Listing.Id);
                                }
                                else if (deleteonSync == "0")
                                {
                                    if (feedback.Id != null)
                                    {
                                        Listing.Id = feedback.Id.Value;
                                        App.Database.Update<Listing>(Listing);
                                    }
                                }
                            }
                            else if (feedback.StatusId == -1)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Sync!",
                                    Message = $"An Error Occurred.  Reason : {feedback.Description} \n Upload has been Stopped." + $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is Uploaded.",
                                    Cancel = "OK"
                                });
                                return;
                            }
                        }
                    }

                    var response = "";
                    if (toSync == syncedRecords)
                        response = $"Upload was Successful.  {syncedRecords} of {toSync} Records  were Uploaded";
                    if (toSync != syncedRecords)
                        response = $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is Uploaded.";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync!",
                        Message = response,
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Upload not Initiated",
                        Message = "No Data that is ready to Upload.",
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Upload",
                    Message = $"The Upload failed. \n {ex.Message}",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageListing = string.Empty;
                SyncUpListingText = "Upload Recorded Data";
                IsBusy = false;
                IsBusyListing = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownListingCommand;

        public ICommand SyncDownListingCommand =>
            syncDownListingCommand ?? (syncDownListingCommand = new Command(async () => await ExecuteSyncDownListingAsync()));

        private async Task ExecuteSyncDownListingAsync()
        {
            if (IsBusy)
                return;
            MessageListing = "Establishing Secure Connection... ...";
            SyncDownListingText = "Downloading ...";
            try
            {
                IsBusyListing = true;
                IsBusy = true;
                ListingOptionsResponse enumeratorResult = null;
                try
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }
                    var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                    MessageListing = "Pulling Sync Data... ...";
                    SyncDownListingText = "Downloading Data";
                    enumeratorResult = await client.GetListingSettings("", "", enumerator.Id.ToString());

                    if (enumeratorResult != null)
                    {
                        foreach (var item in enumeratorResult.Programmes)
                            App.Database.AddOrUpdate(item);

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;
                        MessageListing = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully.",
                            Cancel = "OK"
                        });
                        SyncedDownloadListingTime = DateTime.Now;
                    }
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageListing = string.Empty;
                SyncDownListingText = "Download Latest Programmes and Household Listing Plan Data and Settings";
                IsBusyListing = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Household Listing Sync

        #region Community Validation Registration Sync

        private string syncUpComValText;

        public string SyncUpComValText
        {
            get { return syncUpComValText; }
            set { SetProperty(ref syncUpComValText, value); }
        }

        private string syncDownComValText;

        public string SyncDownComValText
        {
            get { return syncDownComValText; }
            set { SetProperty(ref syncDownComValText, value); }
        }

        private bool isBusyComVal = false;

        public bool IsBusyComVal
        {
            get
            {
                return isBusyComVal;
            }
            set
            {
                SetProperty(ref isBusyComVal, value);
            }
        }

        private string messageComVal;

        public string MessageComVal
        {
            get { return messageComVal; }
            set { SetProperty(ref messageComVal, value); }
        }

        private ICommand syncUpComValCommand;
        public ICommand SyncUpComValCommand => syncUpComValCommand ?? (syncUpComValCommand = new Command(async () => await ExecuteSyncUpComValAsync()));

        private async Task ExecuteSyncUpComValAsync()
        {
            if (IsBusyComVal)
                return;
            MessageComVal = "Establishing Secure Connection... ...";
            SyncUpComValText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone Offline. \n" +
                                  "Please check your Internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                IsBusyComVal = true;
                var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                MessageComVal = "Uploading Sync Data  ...";
                SyncUpComValText = "Uploading Data";

                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };
                var localdata = App.Database.GetTable("ComValListingPlanHH");
                if (localdata.Any())
                {
                    var toSync = localdata.Count();
                    var syncedRecords = 0;

                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    foreach (var item in localdata)
                    {
                        var ComVal = (ComValListingPlanHH)item;
                        if (ComVal.ComValDate != null)
                        {
                            ComVal.SyncEnumeratorId = Settings.Current.EnumeratorId;
                            MessageComVal = $"Processing Record Ref.#{ComVal.Id} ... ";
                            var feedback = await client.PostListingCommVal(ComVal, model);
                            if (feedback.StatusId == null)
                            {
                                Settings.Current.FirstRun = true;
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                    new MessagingServiceAlert
                                    {
                                        Title = "Sync Failed",
                                        Message = "Most probably, your session timed out ",
                                        Cancel = "OK"
                                    });
                                return;
                            }
                            else
                            {
                                if (feedback.StatusId == 0)
                                {
                                    syncedRecords++;
                                    if (deleteonSync == "1")
                                    {
                                        App.Database.Delete<ComValListingPlanHH>(ComVal.Id);
                                    }
                                    else if (deleteonSync == "0")
                                    {
                                        if (feedback.Id != null)
                                        {
                                            ComVal.Id = feedback.Id.Value;
                                            App.Database.Update(ComVal);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var response = "";
                    if (toSync == syncedRecords)
                        response = $"Upload was Successful.  {syncedRecords} of {toSync} Records  were Uploaded";
                    if (toSync != syncedRecords)
                        response = $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is is Processed before Uploading.";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync!",
                            Message = response,
                            Cancel = "OK"
                        });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Check Logs for More Information.",
                            Cancel = "OK"
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageComVal = string.Empty;
                SyncUpComValText = "Upload Recorded Data";
                IsBusy = false;
                IsBusyComVal = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownComValCommand;

        public ICommand SyncDownComValCommand =>
            syncDownComValCommand ?? (syncDownComValCommand = new Command(async () => await ExecuteSyncDownComValAsync()));

        private async Task ExecuteSyncDownComValAsync()
        {
            if (IsBusy)
                return;
            MessageComVal = "Establishing Secure Connection... ...";
            SyncDownComValText = "Starting ...";
            try
            {
                IsBusyComVal = true;
                IsBusy = true;
                EnumeratorCVResponse enumeratorResult = null;
                try
                {
                    var households = App.Database.GetTableRows<ComValListingPlanHH>("ComValListingPlanHH");

                    if (households.Any())
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have already downloaded data." +
                                      "Please Fill and submit the downloaded data before downloading again",
                            Cancel = "OK"
                        });
                        return;
                    }
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }

                    var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                    MessageComVal = "Pulling Sync Data... ...";
                    SyncDownComValText = "Downloading Data";

                    enumeratorResult = await client.ValidationListByEnumerator("", "", enumerator.Id.ToString());
                    IsBusyComVal = false;
                    //   await Task.Delay(500);
                    IsBusyComVal = true;
                    if (enumeratorResult != null)
                    {

                        MessageComVal = "Updating Local Database ... ...";
                        if (enumeratorResult.ComValListingPlanHHs.Any())
                            foreach (var item in enumeratorResult.ComValListingPlanHHs)
                            {
                                item.DownEnumeratorId = Settings.Current.EnumeratorId;
                                item.DownloadDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                                App.Database.AddOrUpdate(item);
                            }

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;

                        MessageComVal = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully",
                            Cancel = "OK"
                        });
                    }
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Complete",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                // Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageComVal = string.Empty;
                SyncDownComValText = "Download Recorded Data";
                IsBusyComVal = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Community Validation Registration Sync

        #region Registration Sync

        private string syncUpRegistrationText;

        public string SyncUpRegistrationText
        {
            get { return syncUpRegistrationText; }
            set { SetProperty(ref syncUpRegistrationText, value); }
        }

        private string syncDownRegistrationText;

        public string SyncDownRegistrationText
        {
            get { return syncDownRegistrationText; }
            set { SetProperty(ref syncDownRegistrationText, value); }
        }

        private string logRegistration;

        public string LogRegistration
        {
            get { return logRegistration; }
            set { SetProperty(ref logRegistration, value); }
        }


        private string messageRegistration;

        public string MessageRegistration
        {
            get { return messageRegistration; }
            set { SetProperty(ref messageRegistration, value); }
        }

        private ICommand syncUpRegistrationCommand;
        public ICommand SyncUpRegistrationCommand => syncUpRegistrationCommand ?? (syncUpRegistrationCommand = new Command(async () => await ExecuteSyncUpRegistrationAsync()));

        private async Task ExecuteSyncUpRegistrationAsync()
        {
            if (IsBusy)
                return;
            MessageRegistration = "Establishing Secure Connection... ...";
            SyncUpRegistrationText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                MessageRegistration = "Uploading Sync Data  ...";

                SyncUpRegistrationText = "Uploading Data";

                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var localdata = App.Database.GetTableRows<Registration>("Registration");

                if (localdata.Any())
                {
                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    //  int PendingInterview = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                    //var limitedSync = App.Database.SystemCodeDetailGetByCode("STATUS", "EDITING").Id;
                    foreach (var item in localdata)
                    {
                        var Tar = item;

                        int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                        if (Tar.InterviewStatusId == InterviewStatusId)
                        {
                            var members = App.Database.GetTableRows<RegistrationMember>("RegistrationMember", "RegistrationId", Tar.Id.ToString());
                            var programmes = App.Database.GetTableRows<RegistrationProgramme>("RegistrationProgramme", "RegistrationId", Tar.Id.ToString());
                            var disabilities = App.Database.GetTableRows<RegistrationMemberDisability>("RegistrationMemberDisability", "RegistrationId", Tar.Id.ToString());

                            Tar.SyncEnumeratorId = Settings.Current.EnumeratorId;
                            Tar.RegistrationMembers = members;
                            Tar.RegistrationMemberDisabilities = disabilities;
                            Tar.RegistrationProgrammes = programmes;

                            MessageRegistration = $"Processing Record Ref.#{Tar.Id} ... ";

                            var feedback = await client.PostRegistration(Tar, model);


                            if (feedback.StatusId == null)
                            {
                                Settings.Current.FirstRun = true;
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                    new MessagingServiceAlert
                                    {
                                        Title = "Sync Failed",
                                        Message = "Most probably, your session timed out ",
                                        Cancel = "OK"
                                    });
                                return;
                            }
                            else
                            {
                                if (feedback.StatusId == 0)
                                {
                                    if (deleteonSync == "1")
                                    {
                                        App.Database.Delete<Registration>(Tar.Id);
                                        //    LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Deleted  Record  #{Tar.Id} from Local DB after successful Sync! \n";
                                    }
                                    else if (deleteonSync == "0")
                                    {
                                        if (feedback.Id != null)
                                        {
                                            Tar.Id = feedback.Id.Value;
                                            App.Database.Update(Tar);
                                            //  LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Updated  Record  #{Tar.Id} on Local DB after successful Sync! \n";
                                        }
                                        else
                                        {
                                            //LogRegistration +=
                                            //    $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : We could not Update the Local Record. Your Data may be duplicated on sync. #{Tar.Id} on Local DB after successful Sync! \n";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //  LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Household {Tar.Id} not Ready for Sync.  \n";
                        }
                    }

                    var response = "Sync was Successful. Check the Log for more information";
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync!",
                            Message = response,
                            Cancel = "OK"
                        });
                }
                else
                {
                    // LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : No Cv Registered Households to Sync.\n \n";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Check Logs for More Information.",
                            Cancel = "OK"
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRegistration = string.Empty;
                SyncUpRegistrationText = "Upload Recorded Data";
                //     LogRegistration += $" {DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Completed \n \n";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownRegistrationCommand;

        public ICommand SyncDownRegistrationCommand => syncDownRegistrationCommand ?? (syncDownRegistrationCommand = new Command(async () => await ExecuteSyncDownRegistrationAsync()));

        private async Task ExecuteSyncDownRegistrationAsync()
        {
            if (IsBusy)
                return;
            MessageRegistration = "Establishing Secure Connection... ...";
            SyncDownRegistrationText = "Starting ...";
            // LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}: {MessageRegistration} \n";
            try
            {
                //IsBusyRegistration = true;
                IsBusy = true;
                EnumeratorCVResponse enumeratorResult = null;
                try
                {
                    var households = App.Database.GetTableRows<Registration>("Registration");

                    if (households.Any())
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have already downloaded data." +
                                      "Please Fill and submit the downloaded data before downloading again",
                            Cancel = "OK"
                        });
                        return;
                    }


                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }

                    var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                    MessageRegistration = "Pulling Sync Data... ...";
                    SyncDownRegistrationText = "Downloading Data";




                    enumeratorResult = await client.RegistrationListByEnumerator("", "", enumerator.Id.ToString());
                    IsBusyComVal = false;
                    //   await Task.Delay(500);
                    IsBusyComVal = true;
                    if (enumeratorResult != null)
                    {
                        MessageComVal = "Updating Local Database ... ...";
                        int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "00").Id;
                        int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "00").Id;
                        int IdentificationDocumentTypeId = App.Database.SystemCodeDetailGetByCode("ID Document Type", "01").Id;

                        if (enumeratorResult.Registrations.Any())
                            foreach (var item in enumeratorResult.Registrations)
                            {
                                item.InterviewStatusId = InterviewStatusId;
                                item.InterviewResultId = InterviewResultId;
                                item.DownloadDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                                App.Database.AddOrUpdate(item);
                                
                            }
                         
                        if (enumeratorResult.RegistrationMembers.Any())
                        {
                            
                            foreach (var member in enumeratorResult.RegistrationMembers)
                            {
                                member.IPRSed = true;
                                member.DateOfBirth = DateTime.Parse(member.DateOfBirth).ToString("O");
                                member.IdentificationDocumentTypeId = IdentificationDocumentTypeId;

                                App.Database.AddOrUpdate(member);

                            }
                        }

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;
                        MessageComVal = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully",
                            Cancel = "OK"
                        });
                    }




                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Complete",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                // Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRegistration = string.Empty;
                SyncDownRegistrationText = "Download Recorded Data";
                //IsBusyRegistration = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Targeting Sync


        #region Targeting Sync

        private string syncUpRecertificationText;

        public string SyncUpRecertificationText
        {
            get { return syncUpRecertificationText; }
            set { SetProperty(ref syncUpRecertificationText, value); }
        }

        private string syncDownRecertificationText;

        public string SyncDownRecertificationText
        {
            get { return syncDownRecertificationText; }
            set { SetProperty(ref syncDownRecertificationText, value); }
        }

        private string logRecertification;

        public string LogRecertification
        {
            get { return logRecertification; }
            set { SetProperty(ref logRecertification, value); }
        }


        private string messageRecertification;

        public string MessageRecertification
        {
            get { return messageRecertification; }
            set { SetProperty(ref messageRecertification, value); }
        }

        private ICommand syncUpRecertificationCommand;
        public ICommand SyncUpRecertificationCommand => syncUpRecertificationCommand ?? (syncUpRecertificationCommand = new Command(async () => await ExecuteSyncUpRecertificationAsync()));

        private async Task ExecuteSyncUpRecertificationAsync()
        {
            if (IsBusy)
                return;
            MessageRecertification = "Establishing Secure Connection... ...";
            SyncUpRecertificationText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                MessageRecertification = "Uploading Sync Data  ...";

                SyncUpRecertificationText = "Uploading Data";

                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var localdata = App.Database.GetTableRows<Recertification>("Recertification");

                if (localdata.Any())
                {
                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;

                    var limitedSync = App.Database.SystemCodeDetailGetByCode("STATUS", "EDITING").Id;
                    foreach (var item in localdata)
                    {
                        var Tar = item;


                        int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                        if (Tar.InterviewStatusId == InterviewStatusId)
                        {

                            var members = App.Database.GetTableRows<RecertificationMember>("RecertificationMember", "RecertificationId", Tar.Id.ToString());
                            var programmes = App.Database.GetTableRows<RecertificationProgramme>("RecertificationProgramme", "RecertificationId", Tar.Id.ToString());
                            var disabilities = App.Database.GetTableRows<RecertificationMemberDisability>("RecertificationMemberDisability", "RecertificationId", Tar.Id.ToString());

                            Tar.SyncEnumeratorId = Settings.Current.EnumeratorId;
                            Tar.RecertificationMembers = members;
                            Tar.RecertificationMemberDisabilities = disabilities;
                            Tar.RecertificationProgrammes = programmes;
                            Tar.SyncEnumeratorId = Settings.Current.EnumeratorId;

                            MessageRecertification = $"Processing Record Ref.#{Tar.Id} ... ";
                            await Task.Delay(1000);
                            var feedback = await client.PostRecertification(Tar, model);


                            if (feedback.StatusId == null)
                            {
                                Settings.Current.FirstRun = true;
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                    new MessagingServiceAlert
                                    {
                                        Title = "Sync Failed",
                                        Message = "Most probably, your session timed out ",
                                        Cancel = "OK"
                                    });
                                return;
                            }
                            else
                            {
                                if (feedback.StatusId == 0)
                                {
                                    if (deleteonSync == "1")
                                    {
                                        App.Database.Delete<Recertification>(Tar.Id);
                                    }
                                    else if (deleteonSync == "0")
                                    {
                                        if (feedback.Id != null)
                                        {
                                            Tar.Id = feedback.Id.Value;
                                            App.Database.Update(Tar);
                                        }
                                        else
                                        {
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                        }
                    }

                    var response = "Sync was Successful. Check the Log for more information";
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync!",
                            Message = response,
                            Cancel = "OK"
                        });
                }
                else
                {

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Check Logs for More Information.",
                            Cancel = "OK"
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRecertification = string.Empty;
                SyncUpRecertificationText = "Upload Recorded Data";
                //     LogRecertification += $" {DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Completed \n \n";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownRecertificationCommand;

        public ICommand SyncDownRecertificationCommand => syncDownRecertificationCommand ?? (syncDownRecertificationCommand = new Command(async () => await ExecuteSyncDownRecertificationAsync()));

        private async Task ExecuteSyncDownRecertificationAsync()
        {
            if (IsBusy)
                return;
            MessageRecertification = "Establishing Secure Connection... ...";
            SyncDownRecertificationText = "Starting ...";
            // LogRecertification += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}: {MessageRecertification} \n";
            try
            {
                //IsBusyRecertification = true;
                IsBusy = true;
                EnumeratorCVResponse enumeratorResult = null;
                try
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }

                    var enumerator = (Enumerator)App.Database.GetTableRow("Enumerator", "Id", Settings.EnumeratorId.ToString());

                    MessageRecertification = "Pulling Sync Data... ...";
                    SyncDownRecertificationText = "Downloading Data";



                    enumeratorResult = await client.RecertificationListByEnumerator("", "", enumerator.Id.ToString());
                    IsBusyComVal = false;
                    //   await Task.Delay(500);
                    IsBusyComVal = true;
                    if (enumeratorResult != null)
                    {
                        MessageComVal = "Updating Local Database ... ...";
                        int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "00").Id;
                        int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "00").Id;

                        if (enumeratorResult.Recertifications.Any())
                            foreach (var item in enumeratorResult.Recertifications)
                            {
                                item.InterviewStatusId = InterviewStatusId;
                                item.InterviewResultId = InterviewResultId;
                                item.DownloadDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                                App.Database.AddOrUpdate(item);
                            }

                        if (enumeratorResult.RecertificationMembers.Any())
                        {
                            foreach (var member in enumeratorResult.RecertificationMembers)
                            {
                                member.IPRSed = true;
                                App.Database.AddOrUpdate(member);

                            }
                        }


                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;

                        MessageComVal = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully",
                            Cancel = "OK"
                        });
                    }




                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Complete",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRecertification = string.Empty;
                SyncDownRecertificationText = "Download Recorded Data";

                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Targeting Sync
    }
}
