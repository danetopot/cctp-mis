﻿using CCTPMIS.Mobile.Interface;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    
    public class HomeViewModel : LocalBaseViewModel
    {
        private IAppClient client;

        public HomeViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<IAppClient>(); 
        }

    }
}
