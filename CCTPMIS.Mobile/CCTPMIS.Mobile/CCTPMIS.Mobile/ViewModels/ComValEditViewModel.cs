﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Converters;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.Pages;
using CCTPMIS.Mobile.Validators;
using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;

namespace CCTPMIS.Mobile.ViewModels
{
    public class ComValEditViewModel : LocalBaseViewModel
    {
        public ObservableRangeCollection<SystemCodeDetail> CgSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> BeneSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<Location> Locations = new ObservableRangeCollection<Location>();
        public ObservableRangeCollection<Programme> Programmes = new ObservableRangeCollection<Programme>();
        public ObservableRangeCollection<SubLocation> SubLocations = new ObservableRangeCollection<SubLocation>();
        private ICommand _saveHouseholdCommand;
        private Programme _selectedProgramme;
        private Location _selectedLocation;
        private SubLocation _selectedSubLocation;
        private SystemCodeDetail _selectedBeneSex;
        private SystemCodeDetail _selectedCgSex;
        public INavigation Navigation;

        public ComValEditViewModel(INavigation navigation, int id) : base(navigation)
        {
            Registration = (ComValListingPlanHH)App.Database.GetTableRow("ComValListingPlanHH", "Id", id.ToString());

            IsClean = (string.IsNullOrEmpty(Registration.ComValDate)
                       && Registration.CgMatches == 1
                       && (Registration.BeneMatches == 1 || Registration.HasBene == 0));

            IsProcessed = !string.IsNullOrEmpty(Registration.ComValDate);

          //  IsShowBene = Registration.Programme.Code == "OPCT";

            if (IsClean)
            {
                Navigation.PopToRootAsync(true);
                Navigation.PushAsync(new CommunityValidationListPage());

                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Editing Household limitation",
                        Message = "This Household cannot be Edited.",
                        Cancel = "OK"
                    });
            }

            var codes = App.Database.SystemCodeDetailsGetByCode("Sex");
            LoadedCgSex.AddRange(codes);
            LoadedBeneSex.AddRange(codes);

            LoadedLocations.AddRange(App.Database.LocationsGetByEnumerator(Settings.Current.EnumeratorId));
            LoadedSubLocations.AddRange(App.Database.SubLocationGetByLocationId(Registration.LocationId));

            Registration.CgDoBDate = DateFormatter.ToEnDateTimeString(Registration.CgDoB);

            if (!string.IsNullOrEmpty(Registration.BeneDoB))
                Registration.BeneDoBDate = DateFormatter.ToEnDateTimeString(Registration.BeneDoB);

            var codesProgrammes = App.Database.GetTable("Programme");

            foreach (var item in codesProgrammes)
                LoadedProgrammes.Add((Programme)item);

            SelectedProgramme = LoadedProgrammes.Single(x => x.Id == Registration.ProgrammeId);

            IsShowBene = SelectedProgramme.Code == "OPCT";

            SelectedLocation = LoadedLocations.Single(x => x.Id == Registration.LocationId);
            SelectedSubLocation = LoadedSubLocations.Single(x => x.Id == Registration.SubLocationId);
            SelectedCgSex = LoadedCgSex.Single(x => x.Id == Registration.CgSexId);
            if(Registration.BeneSexId!=null)
            SelectedBeneSex = LoadedBeneSex.Single(x => x.Id == Registration.BeneSexId);

            if (Registration.BeneDoBDate == null)
            {
                Registration.BeneDoBDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now.AddYears(-20));
            }


            Navigation = navigation;

            _validator = new ComValListingValidator();
            IsNotProcessed = !IsProcessed;
            IsNotClean = !IsClean;

        }
        public bool IsClean { get; set; }
        public bool IsNotClean { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsNotProcessed { get; set; }
        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private bool isShowBene;
        private readonly IValidator _validator;
        public bool IsShowBene
        {
            get { return isShowBene; }
            set { SetProperty(ref isShowBene, value); }
        }

        public ComValListingPlanHH Registration { get; set; }

        public int Id { get; set; }

        public DateTime NextForceSync { get; set; }

        public Location SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (_selectedLocation == value) return;
                _selectedLocation = value;
                SubLocations.Clear();
                SubLocations.AddRange(App.Database.SubLocationGetByLocationId(_selectedLocation.Id));
            }
        }

        public Programme SelectedProgramme
        {
            get => _selectedProgramme;
            set
            {
                if (this._selectedProgramme == value) return;
                this._selectedProgramme = value;
                IsShowBene = _selectedProgramme.Code == "OPCT";
                this.OnPropertyChanged();
            }
        }

        public SubLocation SelectedSubLocation
        {
            get => _selectedSubLocation;
            set
            {
                if (_selectedSubLocation == value) return;
                _selectedSubLocation = value;

                // OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedCgSex
        {
            get => _selectedCgSex;
            set
            {
                if (this._selectedCgSex == value) return;
                this._selectedCgSex = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedBeneSex
        {
            get => _selectedBeneSex;
            set
            {
                if (this._selectedBeneSex == value) return;
                this._selectedBeneSex = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand SaveHouseholdCommand => _saveHouseholdCommand ?? (_saveHouseholdCommand = new Command(async () => await ExecuteSaveHouseHold()));

        public ObservableRangeCollection<Location> LoadedLocations
        {
            get => Locations;
            set => SetProperty(ref Locations, value);
        }

        public ObservableRangeCollection<SubLocation> LoadedSubLocations
        {
            get => SubLocations;
            set => SetProperty(ref SubLocations, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedCgSex
        {
            get => CgSex;
            set => SetProperty(ref CgSex, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedBeneSex
        {
            get => BeneSex;
            set => SetProperty(ref BeneSex, value);
        }

        public ObservableRangeCollection<Programme> LoadedProgrammes
        {
            get => Programmes;
            set => SetProperty(ref Programmes, value);
        }
        private async Task ExecuteSaveHouseHold()
        {
            try
            {
                var reg = this.Registration;
                reg.ComValDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                reg.CgDoBDate = DateFormatter.ToEnDateTimeString(reg.CgDoB.ToString());
                reg.BeneDoBDate = DateFormatter.ToEnDateTimeString(reg.BeneDoB.ToString());
                Message = "Validating .. ";

                var errorMessage = "";
                IsBusy = true;
                if (IsShowBene)
                {
                    if (string.IsNullOrEmpty(reg.BeneFirstName))
                        errorMessage += "Beneficiary FirstName is required \n";
                    if (string.IsNullOrEmpty(reg.BeneSurname))
                        errorMessage += "Beneficiary Surname is required \n";
                    if (string.IsNullOrEmpty(reg.BeneNationalIdNo))
                        errorMessage += "Beneficiary NationalId No is required \n";
                    if (SelectedBeneSex == null)
                        errorMessage += "Beneficiary Sex is required \n";
                }
                
                if (SelectedLocation != null)
                    reg.LocationId = SelectedLocation.Id;
                if (SelectedSubLocation != null)
                    reg.SubLocationId = SelectedSubLocation.Id;
                if (SelectedBeneSex != null)
                    reg.BeneSexId = SelectedBeneSex.Id;
                if (SelectedCgSex != null)
                    reg.CgSexId = SelectedCgSex.Id;

                if (IsShowBene)
                    reg.BeneDoB = DateFormatter.ToSQLiteDateTimeString(DateTime.Parse(reg.BeneDoBDate));
                reg.CgDoB = DateFormatter.ToSQLiteDateTimeString(DateTime.Parse(reg.CgDoBDate));

                var validationResult = _validator.Validate(reg);
                if (validationResult.IsValid && errorMessage == "")
                {
                    reg.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REGCORRECT").Id;
                    reg.EnumeratorId = Settings.Current.EnumeratorId;

                    App.Database.AddOrUpdate(reg);
                    await Application.Current.MainPage.DisplayAlert("Info", "Save Successful!", "OK");
                    await Navigation.PopToRootAsync(true);
                    await Navigation.PushAsync(new CommunityValidationListPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult);


                    if (errorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Correct the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });

                       
                        IsBusy = false;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }
    }
}