﻿using CCTPMIS.Mobile.Database;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    public class HouseholdListingDetailViewModel :LocalBaseViewModel
    {

        public HouseholdListingDetailViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("Listing", "Id", id.ToString());
            Listing = (Listing)data;
            Listing.Programme = (Programme)App.Database.GetTableRow("Programme", "id", Listing.ProgrammeId.ToString());
            Listing.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", Listing.SubLocationId.ToString());
            Listing.Location = (Location)App.Database.GetTableRow("Location", "id", Listing.LocationId.ToString());
            Listing.CgSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Listing.CgSexId.ToString());
            if (Listing.BeneSexId.HasValue)
                Listing.BeneSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Listing.BeneSexId.ToString());
            IsShowBene = Listing.Programme.Code == "OPCT";
        }

        public Listing Listing { get; set; }
        public int Id { get; set; }
        public bool IsShowBene { get; set; }

    }
}
