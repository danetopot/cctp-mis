﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Converters;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Helpers;
using CCTPMIS.Mobile.Pages;
using CCTPMIS.Mobile.Validators;
using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    class ComValExitViewModel : LocalBaseViewModel
    {
        private ICommand _exitHouseholdCommand;
        public INavigation Navigation;
        public ComValListingPlanHH Registration { get; set; }

        public ComValExitViewModel(INavigation navigation, int id) : base(navigation)
        {
            Registration = (ComValListingPlanHH)App.Database.GetTableRow("ComValListingPlanHH", "Id", id.ToString());

            IsClean = (string.IsNullOrEmpty(Registration.ComValDate)
                       && Registration.CgMatches == 1
                       && (Registration.BeneMatches == 1 || Registration.HasBene == 0));

            IsProcessed = !string.IsNullOrEmpty(Registration.ComValDate);

            Navigation = navigation;

            _validator = new ComValListingExitValidator();
        }

        public ICommand ExitHouseholdCommand => _exitHouseholdCommand ?? (_exitHouseholdCommand = new Command(async () => await ExecuteExitHouseHold()));

        private async Task ExecuteExitHouseHold()
        {
            try
            {

                var reg = this.Registration;
                var errorMessage = "";
                IsBusy = true;

                var validationResult = _validator.Validate(reg);
                if (validationResult.IsValid && errorMessage == "")
                {
                    Message = "Validating .. ";
                    reg.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REGEXIT").Id;
                    reg.EnumeratorId = Settings.Current.EnumeratorId;
                    reg.ComValDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                    App.Database.AddOrUpdate(reg);
                    await Application.Current.MainPage.DisplayAlert("Info", "Save Successful!", "OK");
                    await Navigation.PopToRootAsync(true);
                    await Navigation.PushAsync(new CommunityValidationListPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult);


                    if (errorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert {
                                Title = "Please Correct the Data and Try Again!!",
                                Message = ValidateMessage,
                                Cancel = "OK"
                            });


                        IsBusy = false;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert {
                        Title = "An Error has Occured, Please Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }

        private readonly IValidator _validator;
        public bool IsClean { get; set; }
        public bool IsProcessed { get; set; }
        private string message;
        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

    }
}
