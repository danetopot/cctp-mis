﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Converters;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Models;
using CCTPMIS.Mobile.Pages;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{
    public class RecertificationDetailViewModel : LocalBaseViewModel
    {
        public RecertificationDetailViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("Recertification", "Id", id.ToString());
            Recertification = (Recertification)data;
            Recertification.Programme = (Programme)App.Database.GetTableRow("Programme", "id", Recertification.ProgrammeId.ToString());
            Recertification.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", Recertification.SubLocationId.ToString());
            Recertification.Location = (Location)App.Database.GetTableRow("Location", "id", Recertification.LocationId.ToString());
            Navigation = navigation;

            var systemCodeDetails = App.Database.GetTableRows<SystemCodeDetail>("SystemCodeDetail");

            var otherP = App.Database.SystemCodeDetailsGetByCode("Other SP Programme").ToList();
            var progs = App.Database.GetTableRows<RecertificationProgramme>("RecertificationProgramme", "RecertificationId", Recertification.Id.ToString());

            var list = new List<SelectableItemWrapper<SystemCodeDetail>>();

            if (!progs.Any())
                foreach (var item in otherP)
                    list.Add(new SelectableItemWrapper<SystemCodeDetail> { Item = item, IsSelected = false });
            else
                foreach (var item in otherP)
                {
                    list.Add(new SelectableItemWrapper<SystemCodeDetail>
                        { Item = item, IsSelected = progs.Any(x => x.ProgrammeId == item.Id) });
                }

            LoadedProgrammes.AddRange(list.Where(x => x.IsSelected));


            if (Recertification.IsOwnedId > 0)
                Recertification.IsOwned = systemCodeDetails.Single(x => x.Id == Recertification.IsOwnedId);

            if (Recertification.TenureStatusId > 0)
                Recertification.TenureStatus = systemCodeDetails.Single(x => x.Id == Recertification.TenureStatusId);

            if (Recertification.RoofConstructionMaterialId > 0)
                Recertification.RoofConstructionMaterial = systemCodeDetails.Single(x => x.Id == Recertification.RoofConstructionMaterialId);

            if (Recertification.WallConstructionMaterialId > 0)
                Recertification.WallConstructionMaterial = systemCodeDetails.Single(x => x.Id == Recertification.WallConstructionMaterialId);

            if (Recertification.FloorConstructionMaterialId > 0)
                Recertification.FloorConstructionMaterial = systemCodeDetails.Single(x => x.Id == Recertification.FloorConstructionMaterialId);

            if (Recertification.DwellingUnitRiskId > 0)
                Recertification.DwellingUnitRisk = systemCodeDetails.Single(x => x.Id == Recertification.DwellingUnitRiskId);

            if (Recertification.WaterSourceId > 0)
                Recertification.WaterSource = systemCodeDetails.Single(x => x.Id == Recertification.WaterSourceId);

            if (Recertification.WasteDisposalModeId > 0)
                Recertification.WasteDisposalMode = systemCodeDetails.Single(x => x.Id == Recertification.WasteDisposalModeId);


            if (Recertification.CookingFuelTypeId > 0)
                Recertification.CookingFuelType = systemCodeDetails.Single(x => x.Id == Recertification.CookingFuelTypeId);

            if (Recertification.LightingFuelTypeId > 0)
                Recertification.LightingFuelType = systemCodeDetails.Single(x => x.Id == Recertification.LightingFuelTypeId);


            if (Recertification.IsTelevisionId > 0)
                Recertification.IsTelevision = systemCodeDetails.Single(x => x.Id == Recertification.IsTelevisionId);

            if (Recertification.IsTelevisionId > 0)
                Recertification.IsTelevision = systemCodeDetails.Single(x => x.Id == Recertification.IsTelevisionId);

            if (Recertification.IsMotorcycleId > 0)
                Recertification.IsMotorcycle = systemCodeDetails.Single(x => x.Id == Recertification.IsMotorcycleId);

            if (Recertification.IsTukTukId > 0)
                Recertification.IsTukTuk = systemCodeDetails.Single(x => x.Id == Recertification.IsTukTukId);

            if (Recertification.IsRefrigeratorId > 0)
                Recertification.IsRefrigerator = systemCodeDetails.Single(x => x.Id == Recertification.IsRefrigeratorId);

            if (Recertification.IsCarId > 0)
                Recertification.IsCar = systemCodeDetails.Single(x => x.Id == Recertification.IsCarId);

            if (Recertification.IsMobilePhoneId > 0)
                Recertification.IsMobilePhone = systemCodeDetails.Single(x => x.Id == Recertification.IsMobilePhoneId);

            if (Recertification.IsBicycleId > 0)
                Recertification.IsBicycle = systemCodeDetails.Single(x => x.Id == Recertification.IsBicycleId);


            if (Recertification.HouseHoldConditionId > 0)
                Recertification.HouseHoldCondition = systemCodeDetails.Single(x => x.Id == Recertification.HouseHoldConditionId);


            if (Recertification.IsSkippedMealId > 0)
                Recertification.IsSkippedMeal = systemCodeDetails.Single(x => x.Id == Recertification.IsSkippedMealId);

            if (Recertification.NsnpProgrammesId > 0)
                Recertification.NsnpProgrammes = systemCodeDetails.Single(x => x.Id == Recertification.NsnpProgrammesId);

            if (Recertification.IsReceivingSocialId > 0)
                Recertification.IsReceivingSocial = systemCodeDetails.Single(x => x.Id == Recertification.IsReceivingSocialId);


            if (Recertification.OtherProgrammesId > 0)
                Recertification.OtherProgrammes = systemCodeDetails.Single(x => x.Id == Recertification.OtherProgrammesId);

            if (Recertification.BenefitTypeId > 0)
                Recertification.BenefitType = systemCodeDetails.Single(x => x.Id == Recertification.BenefitTypeId);

            HouseholdMembers.AddRange(GetHouseholdMembers(Recertification.Id));


            Id = id;
        }


        public INavigation Navigation;
        public Recertification Recertification { get; set; }
        public int Id { get; set; }

        public ObservableRangeCollection<RecertificationMember> HouseholdMembers { get; } = new ObservableRangeCollection<RecertificationMember>();

        public ObservableCollection<RecertificationMember> GetHouseholdMembers(int id)
        {
            var items = App.Database.GetTableRows("RecertificationMember", "RecertificationId", id.ToString());
            var hh = new ObservableCollection<RecertificationMember>();
            foreach (var item in items)
            {
                hh.Add((RecertificationMember)item);
            }
            return hh;
        }


        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetail>> _loadedProgrammes =
            new ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetail>>();

        public ObservableRangeCollection<SelectableItemWrapper<SystemCodeDetail>> LoadedProgrammes
        {
            get { return _loadedProgrammes; }
            set => SetProperty(ref _loadedProgrammes, value);
        }


        private ObservableCollection<SystemCodeDetail> _selectedNsnpProgrammes;
        public ObservableCollection<SystemCodeDetail> SelectedNsnpProgrammes
        {
            get { return _selectedNsnpProgrammes; }
            set => SetProperty(ref _selectedNsnpProgrammes, value);
        }


        ObservableCollection<SystemCodeDetail> GetSelectedNsnpProgrammes()
        {
            var selected = LoadedProgrammes
                .Where(p => p.IsSelected)
                .Select(p => p.Item)
                .ToList();
            return new ObservableCollection<SystemCodeDetail>(selected);
        }
        private RecertificationMember selectedMember;

        public RecertificationMember SelectedMember
        {
            get { return selectedMember; }
            set
            {
                selectedMember = value;
                OnPropertyChanged();
                if (selectedMember == null)
                    return;
                Navigation.PushAsync(new RecertificationMemberPage(selectedMember.RecertificationId, selectedMember.Id.ToString()));
                return;
                selectedMember = null;
            }
        }

        private ICommand _editHouseholdCommand;

        public ICommand EditHouseholdCommand => _editHouseholdCommand ?? (_editHouseholdCommand = new Command(async () => await ExecuteEditHousehold()));

        private async Task ExecuteEditHousehold()
        {
            try
            {
                await Navigation.PushAsync(new RecertificationEditPage(Recertification.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        private ICommand _newHouseholdMemberCommand;

        public ICommand NewHouseholdMemberCommand => _newHouseholdMemberCommand ??
                                                     (_newHouseholdMemberCommand = new Command(async () => await ExecuteNewHouseholdMember()));

        private async Task ExecuteNewHouseholdMember()
        {
            try
            {
                await Navigation.PushAsync(new RecertificationMemberPage(Recertification.Id, null));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        private ICommand _editRecertificationCommand;
        public ICommand EditRecertificationCommand => _editRecertificationCommand ??
                                                      (_editRecertificationCommand = new Command(async () => await ExecuteEditRecertification()));

        private async Task ExecuteEditRecertification()
        {
            try
            {
                var reg = this.Recertification;
                await Navigation.PushAsync(new RecertificationEditPage(reg.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }



        private ICommand _completeHouseholdCommand;

        public ICommand CompleteHouseholdCommand => _completeHouseholdCommand ?? (_completeHouseholdCommand = new Command(async () => await ExecuteCompleteHousehold()));

        private async Task ExecuteCompleteHousehold()
        {
            try
            {
                var date = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "01").Id;

                Recertification.InterviewStatusId = InterviewStatusId;
                Recertification.InterviewResultId = InterviewResultId;
                Recertification.RecertificationDate = date;
                App.Database.AddOrUpdate(Recertification);


                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = " The House has been Marked as Complete. This is Now Ready to Sync",
                        Cancel = "OK"
                    });


            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }


        private ICommand _emptyHouseholdCommand;

        public ICommand EmptyHouseholdCommand => _emptyHouseholdCommand ?? (_emptyHouseholdCommand = new Command(async () => await ExecuteEmptyHousehold()));

        private async Task ExecuteEmptyHousehold()
        {
            try
            {
                var date = DateFormatter.ToSQLiteDateTimeString(DateTime.Now.Date);


                if (string.IsNullOrEmpty(Recertification.RegDate1))
                {
                    int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "01").Id;
                    int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "04").Id;
                    ;
                    Recertification.InterviewStatusId = InterviewStatusId;
                    Recertification.InterviewResultId = InterviewResultId;
                    Recertification.RegDate1 = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                    App.Database.AddOrUpdate(Recertification);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Updated Successfully",
                            Message = " The House has been Marked as No One at Home. Please Ensure you visit the Household another day.",
                            Cancel = "OK"
                        });
                    return;

                }

                else if (string.IsNullOrEmpty(Recertification.RegDate2) && Recertification.RegDate1 != date)
                {
                    int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "01").Id;
                    int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "04").Id;

                    Recertification.InterviewStatusId = InterviewStatusId;
                    Recertification.InterviewResultId = InterviewResultId;
                    Recertification.RegDate2 = date;
                    App.Database.AddOrUpdate(Recertification);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Updated Successfully",
                            Message = " The House has been Marked as No One at Home for the Second Time. Please Ensure you visit the Household another day.",
                            Cancel = "OK"
                        });
                }

                else if (string.IsNullOrEmpty(Recertification.RegDate3) && Recertification.RegDate2 != date || string.IsNullOrEmpty(Recertification.RegDate2))
                {
                    int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                    int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "04").Id;

                    Recertification.InterviewStatusId = InterviewStatusId;
                    Recertification.InterviewResultId = InterviewResultId;
                    Recertification.RegDate3 = date;
                    Recertification.RecertificationDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                    App.Database.AddOrUpdate(Recertification);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Updated Successfully",
                            Message = " The House has been Marked as No One at Home.  This House is ready to Upload.",
                            Cancel = "OK"
                        });
                }

                if (string.IsNullOrEmpty(Recertification.RegDate3) && Recertification.RegDate2 != date)
                {
                    int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "01").Id;
                    int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "04").Id;

                    Recertification.InterviewStatusId = InterviewStatusId;
                    Recertification.InterviewResultId = InterviewResultId;
                    Recertification.RegDate2 = date;
                    App.Database.AddOrUpdate(Recertification);

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Updated Successfully",
                            Message = " The House has been Marked as No One at Home. Please Ensure you visit the Household another day.",
                            Cancel = "OK"
                        });
                }


            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }





        private ICommand _locateHouseholdCommand;

        public ICommand LocateHouseholdCommand => _locateHouseholdCommand ?? (_locateHouseholdCommand = new Command(async () => await ExecuteLocateHousehold()));

        private async Task ExecuteLocateHousehold()
        {
            try
            {
                var date = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);

                int InterviewStatusId = App.Database.SystemCodeDetailGetByCode("Interview Status", "02").Id;
                int InterviewResultId = App.Database.SystemCodeDetailGetByCode("Interview Result", "05").Id;

                Recertification.InterviewStatusId = InterviewStatusId;
                Recertification.InterviewResultId = InterviewResultId;
                Recertification.RecertificationDate = date;
                App.Database.AddOrUpdate(Recertification);


                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = "The House has been Marked as one that could not be Located. This is Now Ready to Sync",
                        Cancel = "OK"
                    });
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }
        
    }
}