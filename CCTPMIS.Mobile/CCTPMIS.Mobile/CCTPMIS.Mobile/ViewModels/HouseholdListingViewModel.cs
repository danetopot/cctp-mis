﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.Mobile.Database;
using CCTPMIS.Mobile.Pages;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.Mobile.ViewModels
{


    public class HouseholdListingViewModel : LocalBaseViewModel
    {
        public HouseholdListingViewModel(INavigation navigation) : base(navigation)
        {
         //   NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
            ExecuteFilterListingsAsync().ConfigureAwait(false);
           
        }

        public ObservableCollection<Listing> GetListings(bool clear = false)
        {
            var hh = new ObservableCollection<Listing>();
            if (!clear)
            {
                var items = App.Database.GetTable("Listing");
                foreach (var item in items)
                    hh.Add((Listing)item);
                return hh;
            }
            return hh;
        }

        public ObservableRangeCollection<Listing> Listings { get; } = new ObservableRangeCollection<Listing>();
        //   public ObservableRangeCollection<Listing> ListingsFiltered { get; } = new ObservableRangeCollection<Listing>();
        //     public ObservableRangeCollection<Grouping<string, Listing>> ListingsGrouped { get; } = new ObservableRangeCollection<Grouping<string, Listing>>();
        public DateTime NextForceRefresh { get; set; }

        #region Properties

        private string filter = string.Empty;

        public string Filter
        {
            get { return filter; }
            set
            {
                if (SetProperty(ref filter, value))
                    ExecuteFilterListingsAsync();
            }
        }

        #endregion Properties

        #region Filtering and Sorting

        private void SortListings()
        {
            // ListingsGrouped.ReplaceRange(ListingsFiltered.ReplaceRange(GetListings()));

        }

        private bool noListingsFound;

        public bool NoListingsFound
        {
            get { return noListingsFound; }
            set { SetProperty(ref noListingsFound, value); }
        }

        private string noListingsFoundMessage;

        public string NoListingsFoundMessage
        {
            get { return noListingsFoundMessage; }
            set { SetProperty(ref noListingsFoundMessage, value); }
        }

        #endregion Filtering and Sorting

        #region Properties

        private Listing selectedListing;

        public Listing SelectedListing
        {
            get { return selectedListing; }
            set
            {
                selectedListing = value;
                OnPropertyChanged();
                if (selectedListing != null)
                {
                    Navigation.PushAsync(new HouseholdListingDetailPage(selectedListing.Id));
                    return;
                }
            }
        }

        #endregion Properties

        #region Commands

        private ICommand forceRefreshCommand;

        public ICommand ForceRefreshCommand =>
            forceRefreshCommand ?? (forceRefreshCommand = new Command(async () => await ExecuteForceRefreshCommandAsync()));

        private async Task ExecuteForceRefreshCommandAsync()
        {
            await ExecuteLoadListingsAsync(true);
        }

        private ICommand filterListingsCommand;

        public ICommand FilterListingsCommand =>
            filterListingsCommand ?? (filterListingsCommand = new Command(async () => await ExecuteFilterListingsAsync()));

        private async Task ExecuteFilterListingsAsync()
        {
            Listings.Clear();
            if (string.IsNullOrEmpty(Filter))
            {
                Listings.AddRange(GetListings());
                return;
            }
            else
            {
                var loaded = GetListings().Where(x => x.Haystack.ToLower().Contains(filter.ToLower()));
                Listings.AddRange(loaded);

                IsBusy = true;
                NoListingsFound = false;

                IsBusy = false;
            }
        }

        private ICommand loadNewHouseHoldCommand;
        public ICommand LoadNewHouseHoldCommand => loadNewHouseHoldCommand ?? (loadNewHouseHoldCommand = new Command(async () => await ExecuteLoadNewListingPageAsync()));

        private async Task ExecuteLoadNewListingPageAsync()
        {
            try
            {
                await Navigation.PushAsync(new HouseholdListingEditPage());
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Try Again!!",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
            }
        }

        private ICommand loadListingsCommand;
        public ICommand LoadListingsCommand => loadListingsCommand ?? (loadListingsCommand = new Command<bool>(async (s) => await ExecuteLoadListingsAsync(s)));

        private async Task<bool> ExecuteLoadListingsAsync(bool force = false)
        {
            try
            {
                IsBusy = true;
                NoListingsFound = false;
                Filter = string.Empty;
                Listings.ReplaceRange(GetListings());
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage(MessageKeys.Error, ex);
            }
            finally
            {
                IsBusy = false;
            }
            return true;
        }
        #endregion Commands
    }
}
