﻿namespace CCTPMIS.Mobile.Interface
{
    public interface IToast
    {
        void SendToast(string message);
    }

}
