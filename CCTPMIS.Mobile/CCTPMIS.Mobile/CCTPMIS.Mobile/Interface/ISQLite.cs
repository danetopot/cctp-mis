﻿using SQLite;

namespace CCTPMIS.Mobile.Interface
{

    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
