﻿using System.Threading.Tasks;

namespace CCTPMIS.Mobile.Interface
{
    public interface IPushNotifications
    {
        bool IsRegistered { get; }

        void OpenSettings();

        Task<bool> RegisterForNotifications();
    }
}