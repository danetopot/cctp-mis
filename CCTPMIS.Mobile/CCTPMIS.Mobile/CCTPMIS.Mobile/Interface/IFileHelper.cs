﻿namespace CCTPMIS.Mobile.Interface
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
