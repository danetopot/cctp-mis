﻿using System.Collections.Generic;
using System.Linq;
using CCTPMIS.Mobile.Database;

namespace CCTPMIS.Mobile.Extensions
{
    public static class ListingExtensions
    {
        public static IEnumerable<Listing> Search(this IEnumerable<Listing> Listings, string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
                return Listings;
            return Listings.Where(x => x.Haystack.Contains(searchText));
        }
    }
}
