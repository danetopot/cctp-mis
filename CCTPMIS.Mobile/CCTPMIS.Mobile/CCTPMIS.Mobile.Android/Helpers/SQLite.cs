﻿using System;
using Xamarin.Forms;
using CCTPMIS.Mobile.Interface;
using global::SQLite;
using System.IO;

[assembly: Dependency(typeof(CCTPMIS.Mobile.Droid.Helpers.SQLite))]
namespace CCTPMIS.Mobile.Droid.Helpers
{
  


    public class SQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string sqliteFilename = "CCTPMISv2018.db3";
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, sqliteFilename);
            return new SQLiteConnection(path);
        }
    }
}