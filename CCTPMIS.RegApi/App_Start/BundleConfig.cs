﻿using System.Web.Optimization;

namespace CCTPMIS.RegApi
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                "~/Scripts/angular.js",
                "~/Scripts/css3-animate-it.js",
                "~/Scripts/jquery-cookie-{version}.js",
                "~/bower_components/iCheck/icheck.js",
                "~/bower_components/sweet-modal/dist/min/jquery.sweet-modal.min.js",
                "~/bower_components/metisMenu/dist/metisMenu.js",
                "~/Scripts/sweetalert.min.js",

                "~/bower_components/metisMenuAutoOpen/jquery.metismenuAutoOpen.js"));
            bundles.Add(
                new ScriptBundle("~/bundles/cctpmis").Include("~/Scripts/jquery-cctpmis.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/font-awesome.css",
                "~/Content/entypo.css",
                "~/bower_components/sweet-modal/dist/min/jquery.sweet-modal.min.css",
                "~/bower_components/iCheck/skins/all.css",
                "~/Content/css3-animate-it.css",
                "~/bower_components/metisMenu/dist/metisMenu.css",
                "~/Content/theme_styles.css",
                "~/Content/sweetalert.css",
                "~/Content/site.css"));
        }
    }
}