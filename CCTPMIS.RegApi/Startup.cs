﻿using System;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Cors;
using System.Web.Http;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Services;
using Microsoft.Owin.Cors;




[assembly: OwinStartup(typeof(CCTPMIS.RegApi.Startup))]

namespace CCTPMIS.RegApi
{


    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(new CorsOptions()
            {
                PolicyProvider = new CorsPolicyProvider()
                {
                    PolicyResolver = request => request.Path.StartsWithSegments(new PathString("/Token")) ?
                                                    Task.FromResult(new CorsPolicy
                                                    {
                                                        AllowAnyOrigin = true
                                                    }) :
                                                    Task.FromResult<CorsPolicy>(null)
                }
            });
            //var webApiConfiguration = new HttpConfiguration();
            //webApiConfiguration.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional, controller = "values" });
            //app.UseNinjectMiddleware(CreateKernel);
                //.UseNinjectWebApi(webApiConfiguration);
            ConfigureAuth(app);
        }

         
    }


}