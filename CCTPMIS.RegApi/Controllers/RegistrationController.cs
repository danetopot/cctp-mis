﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.RegApi;
using CCTPMIS.Models.Registration;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.RegApi.Controllers
{
    /// <summary>
    /// Registration Module
    /// </summary>
    [Authorize]
    public class RegistrationController : GenericApiController
    {

        private readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public RegistrationController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public IHttpActionResult Index()
        {

            return Ok();
        }

        /// <summary>
        /// End point Accepting  Registration  Data after Community Validation. 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Registration/ComVal/")]
        public IHttpActionResult PostRegistrationComVal(RegistrationHhComVm model)
        {
            // Log
            var userId = int.Parse(User.Identity.GetUserId());
            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATACV + "_POSTED", data, userId.ToString());
            var toModel = new RegistrationHhCv();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(model, toModel);
            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus{StatusId = -1,Description = description};
                return Ok(apiFeedback);
            }
            if (!User.Identity.IsAuthenticated)
            {
                apiFeedback = new ApiStatus{StatusId = -1,Description = "The session is not Authenticated"};
                return Ok(apiFeedback);
            }
            try
            {
                toModel.CvSyncUpDate = DateTime.Now;
                toModel.RegAcceptCvId = null;
                toModel.CvSyncDownDate = toModel.CvUpdateDate;
                toModel.RegistrationHhId = model.Id;

                var toDbModel = new RegistrationHHCvVm();
                new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(toModel, toDbModel);

                data = JsonConvert.SerializeObject(toModel);
                LogService.FileLog(MisKeys.REGISTRATIONDATACV + "_DATABASE", data, userId.ToString());

                var serializer = new XmlSerializer(typeof(List<RegistrationHHCvVm>),new XmlRootAttribute("RegistrationHHCvs"));
                var settings = new XmlWriterSettings{Indent = false,OmitXmlDeclaration = true,};
                var xml = string.Empty;

                var listViewModel = new List<RegistrationHHCvVm> {toDbModel};

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_xml", xml, userId.ToString());

                var spName = "AddEditRegistrationHHCv";
                var parameterNames = "@Id,@RegistrationHHCvXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("Id",toModel.Id)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("RegistrationHHCvXml",xml)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }

        /// <summary>
        /// This Endpoint Accepts the Registration  of Households 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Registration/")]
        public IHttpActionResult PostRegistration(HHListingVm model)
        {
            // Log
            var userId = int.Parse(User.Identity.GetUserId());

            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", data, userId.ToString());

            var toModel = new RegistrationHh();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false)
                .CreateMapper()
                .Map(model, toModel);

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_TO_DATABASE", JsonConvert.SerializeObject(toModel));

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            if (!User.Identity.IsAuthenticated)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "The session is not Authenticated"
                };
                return Ok(apiFeedback);
            }
            try
            {
                GenericService.Create<RegistrationHh>(toModel);

                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "The HouseHold Registration details were Successfully Submitted"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }
 
    }
}