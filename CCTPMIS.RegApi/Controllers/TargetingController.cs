﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.RegApi;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.RegApi.Controllers
{
    /// <summary>
    /// Targeting Module
    /// </summary>
    [Authorize]
    public class TargetingController : GenericApiController
    {

        private readonly IGenericService GenericService;

        private readonly ILogService LogService;

        
        public TargetingController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Index()
        {

            return Ok();
        }


       




       /// <summary>
       /// Accept The Household Listing Data on initial Capture by Enumerators
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>

        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Targeting/Listing")]
        public IHttpActionResult PostListing(HHListingVm model)
        {
            // Log
            var userId = int.Parse(User.Identity.GetUserId());

            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", data, userId.ToString());

            var toModel = new RegistrationHh();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false)
                .CreateMapper()
                .Map(model, toModel);


            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
             
            try
            {
                var serializer = new XmlSerializer(typeof(List<HHListingVm>), new XmlRootAttribute("HHListings"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<HHListingVm> { model };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_xml", xml, userId.ToString());

                var spName = "AddEditHHListing";
                var parameterNames = "@ProgrammeId,@HouseHoldXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProgrammeId",toModel.ProgrammeId)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldXml",xml)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;
                 
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }


        /// <summary>
        /// Household Listing ComVal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Targeting/CommVal/")]
        public IHttpActionResult PostListingComVal(ComHHListingVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            model.SyncUpDate = DateTime.Now.ToString("s");
            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", data, userId.ToString());

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
            try
            {
                var serializer = new XmlSerializer(typeof(List<ComHHListingVm>), new XmlRootAttribute("ComHHListings"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<ComHHListingVm> { model };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_xml", xml, userId.ToString());

                var spName = "AddEditComValHHListing";
                var parameterNames = "@ProgrammeId,@HouseHoldXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProgrammeId",model.ProgrammeId)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldXml",xml)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;

            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }



        /// <summary>
        /// This Endpoint Accepts the Potential Beneficiary Households Data Collected from the HTM Pre-populated on the tablets that was Synced after Registration.  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns an ApiStatus Model</returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Targeting/Registration")]
        public IHttpActionResult PostRegistration(TabletHouseholdViewModel model)
        {
            ApiStatus apiFeedback;
            LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(model), " ");
            var userId = int.Parse(User.Identity.GetUserId());
 
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", description, userId.ToString());
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            } 
            try
            {
                var householdInfo = JsonConvert.DeserializeObject<RegistrationHHVm>(model.HouseholdInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(householdInfo), "Household");

                var deviceInfo = JsonConvert.DeserializeObject<TabEnvironment>(model.DeviceInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(deviceInfo), "Household");

                var serializer = new XmlSerializer(typeof(List<RegistrationHHVm>), new XmlRootAttribute("Registrations"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<RegistrationHHVm> { householdInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }

                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xml, userId.ToString());

                var serializerx = new XmlSerializer(typeof(List<TabEnvironment>), new XmlRootAttribute("Registrations"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xmlx = string.Empty;
                var listViewModelx = new List<TabEnvironment> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    xmlx += sw.ToString();
                }
                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xmlx, userId.ToString());



                var spName = "AddEditRegistrationHH";
                var parameterNames = "@HouseHoldInfoXml,@DeviceInfoXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldInfoXml",xml)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("DeviceInfoXml",xmlx)},
                };

                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);

                apiFeedback = apiDbFeedback;




                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "The HouseHold Registration details were Successfully Submitted"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }
        
        /// <summary>
        /// This Endpoint Accepts the Potential Beneficiary Households Data Collected from the HTM Pre-populated on the tablets that was Synced after Recertification.  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns an ApiStatus Model</returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Targeting/Recertification")]
        public IHttpActionResult PostRecertification(TabletHouseholdViewModel model)
        {
            ApiStatus apiFeedback;
            LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(model), " ");
            var userId = int.Parse(User.Identity.GetUserId());

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", description, userId.ToString());
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
            try
            {
                var householdInfo = JsonConvert.DeserializeObject<RecertificationHHVm>(model.HouseholdInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(householdInfo), "Household");

                var deviceInfo = JsonConvert.DeserializeObject<TabEnvironment>(model.DeviceInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(deviceInfo), "Household");

                var serializer = new XmlSerializer(typeof(List<RegistrationHHVm>), new XmlRootAttribute("Recertifications"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<RecertificationHHVm> { householdInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }

                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xml, userId.ToString());

                var serializerx = new XmlSerializer(typeof(List<TabEnvironment>), new XmlRootAttribute("Recertifications"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xmlx = string.Empty;
                var listViewModelx = new List<TabEnvironment> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    xmlx += sw.ToString();
                }
                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xmlx, userId.ToString());



                var spName = "AddEditRecertificationHH";
                var parameterNames = "@HouseHoldInfoXml,@DeviceInfoXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldInfoXml",xml)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("DeviceInfoXml",xmlx)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;


                apiFeedback = new ApiStatus
                {
                    StatusId = 0,
                    Description = "The HouseHold Re-certification details were Successfully Submitted"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return this.Ok(apiFeedback);
        }





    }
}
