﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CCTPMIS.Business.Context;
using CCTPMIS.Services;

namespace CCTPMIS.RegApi.Controllers
{
    /// <summary>
    /// Synchronization Module
    /// </summary>
    [Authorize]
    public class SyncController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IGenericService GenericService;
        public readonly LogService LogService;

        public SyncController(IGenericService genericService, LogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }
    }
}
