﻿using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.RegApi;
using CCTPMIS.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace CCTPMIS.RegApi.Controllers
{
    /// <summary>
    /// Downward Pull Module
    /// </summary>
    public class OptionsController : GenericApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IGenericService GenericService;
        public readonly LogService LogService;

        public OptionsController(IGenericService genericService, LogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        /// <summary>
        /// Community Validation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/Options/GetCommunityVal/{id}/")]
        [HttpGet]
        public IHttpActionResult GetCommunityVal(int id)
        {
            var returnModel = new CvSetupVm();
            returnModel = GetComminityValidationDownloadData(id);
            return Ok(returnModel);

        }

        /// <summary>
        /// Registration
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/Options/Registration/{id}/")]
        [HttpGet]
        public IHttpActionResult GetRegistration(int id)
        {
            var returnModel = new CvSetupVm();
            returnModel = GetRegistrationDownloadData(id);
            return Ok(returnModel);

        }

        [Route("api/Options/CommunityVal")]
        [HttpPost]
        public IHttpActionResult GetCommunityValOptions(LoginVM model)
        {

            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetComminityValidationDownloadData(enumerator.Id);
            return Ok(returnModel);



        }



        [Route("api/Options/")]
        [HttpPost]
        public IHttpActionResult GetOptions(LoginVM model)
        {
            string hashPin;
            var returnModel = new InitialSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetEnumeratorLoginDownloadData(enumerator.Id);
            return Ok(returnModel);
        }



        /// <summary>
        /// This is the Endpoint of getting all the beneficiary Households that are in Active Targeting Registration Plans.
        /// </summary>
        /// <param name="model"> The User View Model </param>
        /// <returns> Repurposed Registration Households into H.T.M. Tool </returns>
        [Route("api/Options/TargetingData")]
        [HttpPost]
        public IHttpActionResult GetTargetingData(LoginVM model)
        {
            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetTargetingDownloadData(enumerator.Id);
            return Ok(returnModel);
        }

        /// <summary>
        /// Get Programmes and Enumerator Locations
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Options/Listing")]
        [HttpPost]
        public IHttpActionResult GetListingOptions(LoginVM model)
        {
            string hashPin;
            var returnModel = new InitialSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetEnumeratorListingData(enumerator.Id);

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(model), "");

            return Ok(returnModel);
        }



        /// <summary>
        /// This is the Endpoint of getting all the beneficiary Households that are in Active Targeting Registration Plans.
        /// </summary>
        /// <param name="model"> The User View Model </param>
        /// <returns> Repurposed Registration Households into H.T.M. Tool </returns>
        [Route("api/Options/Registration")]
        [HttpPost]
        public IHttpActionResult GetRegistrationData(LoginVM model)
        {

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(model), "");

            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(enumerator), "");


            returnModel = GetRegistrationDownloadData(enumerator.Id);
            return Ok(returnModel);
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        private InitialSetupVm GetEnumeratorListingData(int enumeratorId)
        {
            var returnModel = new InitialSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);
            returnModel.Programmes = (from c in this.db.Programmes
                                      select new ProgrammeVm()
                                      {
                                          Id = c.Id,
                                          Code = c.Code,
                                          Name = c.Name,
                                          BeneficiaryTypeId = c.BeneficiaryTypeId,
                                          IsActive = c.IsActive,
                                          PrimaryRecipientId = c.PrimaryRecipientId,
                                          SecondaryRecipientId = c.SecondaryRecipientId,
                                          SecondaryRecipientMandatory = c.SecondaryRecipientMandatory,
                                      }).ToList();

            returnModel.TargetPlans = (from c in this.db.TargetPlans
                                       select new TargetPlanVm
                                       {
                                           Id = c.Id,
                                           Start = c.Start,
                                           End = c.End,
                                           CategoryId = c.CategoryId,
                                           Name = c.Name,
                                           StatusId = c.StatusId
                                       }).ToList();


            return returnModel;
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        private InitialSetupVm GetEnumeratorLoginDownloadData(int enumeratorId)
        {
            var returnModel = new InitialSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);
            var mobileDownloadCodes = "System Settings,Beneficiary Type,Sex,Registration Group,Member Role,Relationship,Other SP Programme,SP Benefit Type,REG_CATEGORIES,REG_STATUS,Registration Status,Tenure Status,Wall Material,Floor Material,Roof Material,Toilet Type,Water Source,Lighting Source,Cooking Fuel,Dwelling Unit Risk,Household Assets,Household Asset Type,Boolean Options,Household Conditions,Household Option,Marital Status,Disability,Education Attendance,Education Level,Work Type,Interview Type,Interview Result,Interview Status,ID Document Type";

            returnModel.Enumerator = enumeratorVm;
            returnModel.EnumeratorLocations = (from c in this.db.EnumeratorLocations
                                               where c.EnumeratorId == enumerator.Id && c.IsActive
                                               select new EnumeratorLocationVm()
                                               {
                                                   Id = c.Id,
                                                   LocationId = c.LocationId,
                                                   EnumeratorId = c.EnumeratorId,
                                                   IsActive = c.IsActive
                                               }).Distinct().ToList();

            returnModel.Locations = (from c in this.db.EnumeratorLocations
                                     join l in db.Locations on c.LocationId equals l.Id
                                     where c.EnumeratorId == enumerator.Id && c.IsActive
                                     select new LocationVm()
                                     {
                                         Id = l.Id,
                                         Name = l.Name,
                                         Code = l.Code
                                     }).Distinct().ToList();

            returnModel.SubLocations = (from c in this.db.EnumeratorLocations
                                        join l in db.Locations on c.LocationId equals l.Id
                                        join s in db.SubLocations on l.Id equals s.LocationId
                                        where c.EnumeratorId == enumerator.Id && c.IsActive
                                        select new SubLocationVm()
                                        {
                                            LocationId = s.LocationId,
                                            Id = s.Id,
                                            Name = s.Name,
                                        }).Distinct().ToList();

            returnModel.TargetPlans = (from c in this.db.TargetPlans

                                       select new TargetPlanVm
                                       {
                                           Id = c.Id,
                                           Start = c.Start,
                                           End = c.End,
                                           CategoryId = c.CategoryId,
                                           Name = c.Name,
                                           StatusId = c.StatusId
                                       }).Distinct().ToList();

            returnModel.SystemCodes = (from c in this.db.SystemCodes
                                       where mobileDownloadCodes.Contains(c.Code)
                                       select new SystemCodeVm()
                                       {
                                           Id = c.Id,
                                           Code = c.Code,
                                           Description = c.Description
                                       }).Distinct().ToList();

            returnModel.SystemCodeDetails =
                (from c in this.db.SystemCodeDetails
                 where mobileDownloadCodes.Contains(c.SystemCode.Code)
                 select new SystemCodeDetailVm()
                 {
                     Id = c.Id,
                     Code = c.Code,
                     Description = c.Description,
                     SystemCodeId = c.SystemCodeId
                 }).Distinct().ToList();

            returnModel.Programmes = (from c in this.db.Programmes

                                      select new ProgrammeVm()
                                      {
                                          Id = c.Id,
                                          Code = c.Code,
                                          Name = c.Name,
                                          BeneficiaryTypeId = c.BeneficiaryTypeId,
                                          IsActive = c.IsActive,
                                          PrimaryRecipientId = c.PrimaryRecipientId,
                                          SecondaryRecipientId = c.SecondaryRecipientId,
                                          SecondaryRecipientMandatory = c.SecondaryRecipientMandatory,
                                      }).Distinct().ToList();



            return returnModel;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetComminityValidationDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);

            var spName = "GetHouseholdsReadyForComValDownload";
            var parameterNames = "@EnumeratorId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("EnumeratorId",enumeratorId)},
             };
            returnModel.ComValListingPlanHHs = GenericService.GetManyBySp<ComValDownloadListingPlanHH>(spName, parameterNames, parameterList).ToList();

            return returnModel;
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetRegistrationDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var spName = "GetHouseholdsReadyForRegistration";
            var parameterNames = "@EnumeratorId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("EnumeratorId",enumeratorId)},
            };
            try
            {
                var reg = GenericService.GetManyBySp<ComValListingPlanHH>(spName, parameterNames, parameterList).ToList();

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(reg), "");

                returnModel.Registrations = reg;
                var members = new List<CvMember>();
                if (!reg.Any())
                {
                    return returnModel;
                }

                foreach (var household in reg)
                {
                    var cgId = Guid.NewGuid().ToString();
                    var beneId = Guid.NewGuid().ToString();
                    var cg = new CvMember
                    {
                        FirstName = household.CgFirstName,
                        MiddleName = household.CgMiddleName,
                        Surname = household.CgSurname,
                        RegistrationId = household.Id,
                        SexId = household.CgSexId,
                        DateOfBirth = household.CgDoB.ToString("O"),
                        IdentificationNumber = household.CgNationalIdNo,
                        PhoneNumber = household.CgPhoneNumber,
                        MemberId = cgId,

                    };
                    members.Add(cg);

                    if (string.IsNullOrEmpty(household.BeneNationalIdNo) || !household.BeneSexId.HasValue || !household.BeneDoB.HasValue)
                    {
                        continue;
                    }
                    else
                    {

                        var bene = new CvMember
                        {
                            FirstName = household.BeneFirstName,
                            MiddleName = household.BeneMiddleName,
                            Surname = household.BeneSurname,
                            RegistrationId = household.Id,
                            SexId = household.BeneSexId.Value,
                            DateOfBirth = household.BeneDoB.Value.ToString("O"),
                            IdentificationNumber = household.BeneNationalIdNo,
                            PhoneNumber = household.BenePhoneNumber,
                            MemberId = beneId,
                            CareGiverId = cgId
                        };
                        members.Add(bene);
                    }
                
                }

                returnModel.RegistrationMembers = members;
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(members), "");
            }
            catch (Exception e)
            {
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", e.Message, "");
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(e), "");
            }
            return returnModel;


        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetTargetingDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);



            //returnModel.ValRegistrationHhVms =
            //    (from r in db.RegistrationHhs
            //     join v in db.RegExceptions on r.Id equals v.Id
            //     join s in db.SubLocations on r.SubLocationId equals s.Id
            //     join c in db.TargetPlans on r.RegPlanId equals c.Id
            //     join l in db.Locations on s.LocationId equals l.Id
            //     join el in db.EnumeratorLocations on l.Id equals el.LocationId
            //     join tpc in db.TarPlanProgrammes on r.RegPlanId equals tpc.TargetPlanId
            //     join p in db.Programmes on tpc.ProgrammeId equals p.Id
            //     join t in db.TargetPlans on tpc.TargetPlanId equals t.Id
            //     where el.EnumeratorId == enumeratorId
            //           && el.IsActive
            //           && t.Status.Code == "ACTIVE"
            //           && c.Status.Code == "CLOSED"
            //     select r
            //    ).GroupBy(r => r.Id)
            //    .Select(grp => grp.FirstOrDefault()).ToList();
            return returnModel;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}