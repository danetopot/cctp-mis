using Microsoft.Owin;

[assembly: OwinStartup(typeof(CCTPMIS.RegMobileApp.MobileAppService.Startup))]

namespace CCTPMIS.RegMobileApp.MobileAppService
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}