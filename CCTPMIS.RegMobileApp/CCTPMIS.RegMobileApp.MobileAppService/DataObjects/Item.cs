﻿namespace CCTPMIS.RegMobileApp.MobileAppService.DataObjects
{
    using Microsoft.Azure.Mobile.Server;

    public class Item : EntityData
    {
        public string Description { get; set; }

        public string Text { get; set; }
    }
}