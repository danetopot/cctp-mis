﻿using Android.Widget;
using CCTPMIS.RegMobileApp.Helpers;
using Plugin.CurrentActivity;
using Plugin.Media;
using Plugin.Permissions;
using Android.Gms.Common;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace CCTPMIS.RegMobileApp.Droid
{
    using Android.App;
    using Android.Content.PM;
    using Android.OS;

    using FormsToolkit.Droid;

    [Activity(
        Label = "CCTP MIS Data Collection Tool",
        Icon = "@drawable/logo",
    
        Theme = "@style/MainTheme",
     
        MainLauncher = true,

        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override async void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            await CrossMedia.Current.Initialize();
            Forms.Init(this, bundle);
            Xamarin.Forms.DataGrid.DataGridComponent.Init();
             Toolkit.Init();
            Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();

            CrossCurrentActivity.Current.Init(this, bundle);

            CrossCurrentActivity.Current.Activity = this;

            LoadApplication(new App());

            var gpsAvailable = IsPlayServicesAvailable();
            Settings.Current.PushNotificationsEnabled = gpsAvailable;
        }
        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    if (Settings.Current.GooglePlayChecked)
                        return false;

                    Settings.Current.GooglePlayChecked = true;
                    Toast.MakeText(this, "Google Play services is not installed, push notifications have been disabled.", ToastLength.Long).Show();
                }
                else
                {
                    Settings.Current.PushNotificationsEnabled = false;
                }
                return false;
            }
            else
            {
                Settings.Current.PushNotificationsEnabled = true;
                return true;
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}