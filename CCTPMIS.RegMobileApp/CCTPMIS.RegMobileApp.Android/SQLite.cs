﻿using Xamarin.Forms;

[assembly: Dependency(typeof(CCTPMIS.RegMobileApp.Droid.SQLite))]

namespace CCTPMIS.RegMobileApp.Droid
{
    using System;
    using System.IO;

    using global::SQLite;

    public class SQLite : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            string sqliteFilename = "ccptmisv2.db3";
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string path = Path.Combine(documentsPath, sqliteFilename);

            return new SQLiteConnection(path);
        }
    }
}