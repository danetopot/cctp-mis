﻿namespace CCTPMIS.RegMobileApp
{
    using SQLite;

    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }

    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}