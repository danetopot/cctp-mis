﻿namespace CCTPMIS.RegMobileApp.Interfaces
{
    using System;
    using System.Collections;
    using System.Threading.Tasks;

    using CCTPMIS.RegMobileApp.Models;

    public interface ILogger
    {
        void Report(Exception exception = null, Severity warningLevel = Severity.Warning);

        void Report(Exception exception, IDictionary extraData, Severity warningLevel = Severity.Warning);

        void Report(Exception exception, string key, string value, Severity warningLevel = Severity.Warning);

        void Track(string trackIdentifier);

        void Track(string trackIdentifier, string key, string value);

        void TrackPage(string page, string id = null);
    }

    public enum Severity
    {
        /// <summary>
        /// Warning Severity
        /// </summary>
        Warning,

        /// <summary>
        /// Error Severity, you are not expected to call this from client side code unless you have disabled unhandled exception handling.
        /// </summary>
        Error,

        /// <summary>
        /// Critical Severity
        /// </summary>
        Critical
    }

    public interface IToast
    {
        void SendToast(string message);
    }

    public interface ISSOClient
    {
        Task<AccountResponse> ForgotPasswordAsync(string username);

        Task<AccountResponse> LoginAsync(string username, string password);

        Task<EnumeratorLoginResponse> LoginEnumerator(string nationalIdNo, string pin, string id);

        Task<EnumeratorCVResponse> ValidationListByEnumerator(string nationalIdNo, string pin, string id);

        Task<EnumeratorCVResponse> GetRegistrationData(string nationalIdNo, string pin, string id);

        Task<ApiStatus> PostListing(Listing reg, LocalDeviceInfo deviceInfo);

        Task<ApiStatus> PostListingCommVal(ComValListingPlanHH reg, LocalDeviceInfo deviceInfo);

        Task<ApiStatus> PostTargeting(TargetingHh reg, LocalDeviceInfo deviceInfo);

        Task<ListingOptionsResponse> GetListingSettings(string nationalIdNo, string pin, string id);

        Task<ApiStatus> ForgotPin(string nationalIdNo, string emailAddress, string id);

        Task<ApiStatus> ChangePin(string currentPin, string newPin, string id);

        Task LogoutAsync();
    }

    public interface IPushNotifications
    {
        bool IsRegistered { get; }

        void OpenSettings();

        Task<bool> RegisterForNotifications();
    }

    public interface ITextToSpeech
    {
        void Speak(string text);
    }

    public interface ICloseApplication
    {
        void CloseApplication();
    }

    public static class SauLoggerKeys
    {
        public const string CallHotel = "CallHotel";

        public const string CopyPassword = "CopyPassword";

        public const string FavoriteAdded = "FavoriteAdded";

        public const string FavoriteRemoved = "FavoriteRemoved";

        public const string LaunchedBrowser = "LaunchedBrowser";

        public const string LeaveFeedback = "LeaveFeedback";

        public const string LoginCancel = "LoginCancel";

        public const string LoginFailure = "LoginFailure";

        public const string LoginSuccess = "LoginSuccess";

        public const string LoginTime = "LoginTime";

        public const string Logout = "Logout";

        public const string ManualSync = "ManualSync";

        public const string NavigateToEvolve = "NavigateToEvolve";

        public const string ReminderAdded = "ReminderAdded";

        public const string ReminderRemoved = "ReminderRemoved";

        public const string Share = "Share";

        public const string Signup = "Signup";

        public const string WiFiConfig = "WiFiConfigured";
    }
}