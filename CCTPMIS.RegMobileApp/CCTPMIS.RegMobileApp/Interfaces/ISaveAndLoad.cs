﻿namespace CCTPMIS.RegMobileApp.Interfaces
{
    using System.Threading.Tasks;

    public interface ISaveAndLoad
    {
        bool FileExists(string filename);

        Task<string> LoadTextAsync(string filename);

        Task SaveTextAsync(string filename, string text);
    }
}