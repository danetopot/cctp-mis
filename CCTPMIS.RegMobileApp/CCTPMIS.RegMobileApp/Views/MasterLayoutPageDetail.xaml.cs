﻿namespace CCTPMIS.RegMobileApp.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterLayoutPageDetail : ContentPage
    {
        public MasterLayoutPageDetail()
        {
            InitializeComponent();
        }
    }
}