﻿using System;
using System.Text.RegularExpressions;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.ViewModels;
using FormsToolkit;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePasswordPage : ContentPage
    {
        private ForgotViewModel vm;

        public ChangePasswordPage()
        {
            InitializeComponent();
            BindingContext = vm = new ForgotViewModel(Navigation);
        }

        private async void OnToLoginButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage()).ConfigureAwait(false);
        }

        private async void OnChangePasswordButtonClicked(object sender, EventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                    {
                        Title = "Sign in Information",
                        Message = "You are offline !",
                        Cancel = "OK"
                    });
                return;
            }

            if (string.IsNullOrWhiteSpace(CurrentPin.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                    {
                        Title = "Sign in Information",
                        Message = "We do need your  Current PIN :-)",
                        Cancel = "OK"
                    });
                return;
            }
            if (string.IsNullOrWhiteSpace(NewPin.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                    {
                        Title = "Sign in Information",
                        Message = "We do need your  New PIN received on Email :-)",
                        Cancel = "OK"
                    });
                return;
            }

            if (string.IsNullOrWhiteSpace(ConfirmNewPin.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                    {
                        Title = "Sign in Information",
                        Message = "We do need you confirm New PIN :-)",
                        Cancel = "OK"
                    });
                return;
            }
            if (ConfirmNewPin.Text!= NewPin.Text)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                    {
                        Title = "Sign in Information",
                        Message = "Your New PIN and the COnfirmation Does not Match :-)",
                        Cancel = "OK"
                    });
                return;
            }

            // Send the API Call

            // Listen to the Feedback

            // call the Api and then, Notify the User
            MessagingService.Current.SendMessage<MessagingServiceAlert>(
                MessageKeys.Message,
                new MessagingServiceAlert
                {
                    Title = "Check your Email",
                    Message =
                            "The Password has been reset successfully. Kindly proceed to Login",
                    Cancel = "OK"
                });

            await Navigation.PushAsync(new LoginPage()).ConfigureAwait(false);
            return;
        }
    }
}