﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Account
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LogoutPage : ContentPage
    {
        private ForgotViewModel vm;

        public LogoutPage()
	    {
	        InitializeComponent();
	        BindingContext = vm = new ForgotViewModel(Navigation);
	    }
    }
}