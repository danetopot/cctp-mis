﻿using CCTPMIS.RegMobileApp.ViewModels;

namespace CCTPMIS.RegMobileApp.Views
{
    using System;
    using System.Text.RegularExpressions;

    using CCTPMIS.RegMobileApp.Data;

    using FormsToolkit;

    using Plugin.Connectivity;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPage : ContentPage
    {
        private ForgotViewModel vm;

        public ForgotPasswordPage()
        {
            InitializeComponent();
            BindingContext = vm = new ForgotViewModel(Navigation);
        }

        private async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage()).ConfigureAwait(false);
        }

        private async void OnResetPasswordButtonClicked(object sender, EventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                        {
                            Title = "Sign in Information",
                            Message = "You are offline!",
                            Cancel = "OK"
                        });
                return;
            }

            if (string.IsNullOrWhiteSpace(Email.Text))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Message,
                    new MessagingServiceAlert
                        {
                            Title = "Sign in Information",
                            Message = "We do need your email address :-)",
                            Cancel = "OK"
                        });
                return;
            }

            var emailRegex =
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))"
                + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
            var isEmailValid = Regex.IsMatch(
                Email.Text,
                emailRegex,
                RegexOptions.IgnoreCase,
                TimeSpan.FromMilliseconds(250));

            if (!isEmailValid)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                        {
                            Title = "Sign in Information",
                            Message = "You are using an Invalid Email. Correct and try again.",
                            Cancel = "OK"
                        });
                return;
            }

            // call the Api and then, Notify the User
            MessagingService.Current.SendMessage<MessagingServiceAlert>(
                MessageKeys.Message,
                new MessagingServiceAlert
                    {
                        Title = "Check your Email",
                        Message =
                            " A message has been sent to you by email with instructions on how to reset your password. (if your Email exists)",
                        Cancel = "OK"
                    });

            await Navigation.PushAsync(new ChangePasswordPage()).ConfigureAwait(false);
            return;
        }
    }
}