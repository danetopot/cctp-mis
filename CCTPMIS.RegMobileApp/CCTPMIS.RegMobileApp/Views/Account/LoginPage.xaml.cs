﻿using CCTPMIS.RegMobileApp.ViewModels;

namespace CCTPMIS.RegMobileApp.Views
{
    using System;
    using System.Text.RegularExpressions;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Helpers;
    using CCTPMIS.RegMobileApp.Models;

    using FormsToolkit;

    using Plugin.Connectivity;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private LoginViewModel vm;

        public LoginPage()
        {
            InitializeComponent();

            BindingContext = vm = new LoginViewModel(Navigation);
        }

        protected override bool OnBackButtonPressed()
        {
            if (Settings.Current.FirstRun)
                return true;

            return base.OnBackButtonPressed();
        }

        //private bool AreCredentialsCorrect(UserLoginViewModel user)
        //{
        //    return user.Email == Constants.Email && user.Password == Constants.Password;
        //}

        //private async void OnForgotPasswordButtonClicked(object sender, EventArgs e)
        //{
        //    await Navigation.PushAsync(new ForgotPasswordPage()).ConfigureAwait(false);
        //}

        //private async void OnLoginButtonClicked(object sender, EventArgs e)
        //{
        //    if (!CrossConnectivity.Current.IsConnected)
        //    {
        //        MessagingService.Current.SendMessage<MessagingServiceAlert>(
        //            MessageKeys.Error,
        //            new MessagingServiceAlert
        //                {
        //                    Title = "Sign in Information",
        //                    Message = "You are offline !",
        //                    Cancel = "OK"
        //                });
        //        return;
        //    }

        //    if (string.IsNullOrWhiteSpace(Email.Text))
        //    {
        //        MessagingService.Current.SendMessage<MessagingServiceAlert>(
        //            MessageKeys.Error,
        //            new MessagingServiceAlert
        //                {
        //                    Title = "Sign in Information",
        //                    Message = "We do need your email address :-)",
        //                    Cancel = "OK"
        //                });
        //        return;
        //    }

        //    var emailRegex =
        //        @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))"
        //        + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
        //    var isEmailValid = Regex.IsMatch(
        //                               Email.Text,
        //                               emailRegex,
        //                               RegexOptions.IgnoreCase,
        //                               TimeSpan.FromMilliseconds(250));

        //    if (!isEmailValid)
        //    {
        //        MessagingService.Current.SendMessage<MessagingServiceAlert>(
        //            MessageKeys.Error,
        //            new MessagingServiceAlert
        //                {
        //                    Title = "Sign in Information",
        //                    Message = "You are using an Invalid Email. Correct and try again.",
        //                    Cancel = "OK"
        //                });
        //        return;
        //    }

        //    if (string.IsNullOrWhiteSpace(Password.Text))
        //    {
        //        MessagingService.Current.SendMessage<MessagingServiceAlert>(
        //            MessageKeys.Error,
        //            new MessagingServiceAlert
        //                {
        //                    Title = "Sign in Information",
        //                    Message = "We do need your Password :-)",
        //                    Cancel = "OK"
        //                });
        //        return;
        //    }

        //    var user = new UserLoginViewModel { Email = Email.Text, Password = Password.Text };

        //    var isValid = AreCredentialsCorrect(user);
        //    if (isValid)
        //    {
        //        Settings.Current.LoggedIn = true;
        //        Settings.Current.FirstRun = false;
        //        var homepage = new MasterLayoutPageDetail();
        //        await Navigation.PopModalAsync();
        //    }
        //    else
        //    {
        //        MessageLabel.Text = "Login failed";
        //        Password.Text = string.Empty;
        //    }
        //}

        private async void OnTermsClicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(Constants.BaseSiteAddress + "/terms/"));
        }
    }
}