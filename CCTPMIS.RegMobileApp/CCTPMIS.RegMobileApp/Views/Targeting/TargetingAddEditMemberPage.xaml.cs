﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Targeting
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TargetingAddEditMemberPage : ContentPage
    {
        TargetingAddEditMemberViewModel ViewModel => vm ?? (vm = BindingContext as TargetingAddEditMemberViewModel);
        private TargetingAddEditMemberViewModel vm;

        public TargetingAddEditMemberPage(int? id, int hhid)
        {
            InitializeComponent();

            BindingContext = new TargetingAddEditMemberViewModel(Navigation, id, hhid);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is TargetingAddEditMemberViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                //    vm.HouseholdMembers.ReplaceRange(bindingContext.GetHouseholdMembers(bindingContext.Id));
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (BindingContext is TargetingAddEditMemberViewModel bindingContext)
                bindingContext.OnDisappearing();
        }
    }
}