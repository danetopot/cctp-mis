﻿namespace CCTPMIS.RegMobileApp.Views
{
    using CCTPMIS.RegMobileApp.Models;
    using CCTPMIS.RegMobileApp.ViewModels;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationDetailPage : ContentPage
    {
        private RegistrationDetailViewModel viewModel;

        public RegistrationDetailPage(RegistrationDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public RegistrationDetailPage()
        {
            InitializeComponent();

            var item = new HouseHoldRegistration { };

            viewModel = new RegistrationDetailViewModel(item);
            BindingContext = viewModel;
        }
    }
}