﻿namespace CCTPMIS.RegMobileApp.Views
{
    using CCTPMIS.RegMobileApp.ViewModels;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHouseholdMemberPage : ContentPage
    {
        public NewHouseholdMemberPage()
        {
            InitializeComponent();
            BindingContext = new NewHouseholdMemberViewModel(Navigation);
        }
    }
}