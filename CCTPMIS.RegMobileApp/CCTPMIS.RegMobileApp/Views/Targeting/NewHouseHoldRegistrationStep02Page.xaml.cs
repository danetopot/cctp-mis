﻿namespace CCTPMIS.RegMobileApp.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHouseHoldRegistrationStep02 : ContentPage
    {
        public NewHouseHoldRegistrationStep02()
        {
            InitializeComponent();
        }
    }
}