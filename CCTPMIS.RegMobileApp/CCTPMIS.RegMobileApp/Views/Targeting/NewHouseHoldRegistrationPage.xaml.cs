﻿namespace CCTPMIS.RegMobileApp.Views
{
    using CCTPMIS.RegMobileApp.ViewModels;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHouseHoldRegistrationPage : ContentPage
    {
        public NewHouseHoldRegistrationPage()
        {
            InitializeComponent();
            BindingContext = new NewRegistrationViewModel(Navigation);
        }
    }
}