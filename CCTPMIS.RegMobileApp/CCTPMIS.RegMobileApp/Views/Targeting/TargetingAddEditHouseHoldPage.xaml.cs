﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Targeting
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TargetingAddEditHouseHoldPage : ContentPage
	{
	    TargetingEditViewModel ViewModel => vm ?? (vm = BindingContext as TargetingEditViewModel);
        private TargetingEditViewModel vm;

        public TargetingAddEditHouseHoldPage (int id)
		{
			InitializeComponent ();

		    BindingContext = new TargetingEditViewModel(Navigation, id);
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

            if (BindingContext is TargetingEditViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                //    vm.HouseholdMembers.ReplaceRange(bindingContext.GetHouseholdMembers(bindingContext.Id));
            }
        }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();

            if (BindingContext is TargetingEditViewModel bindingContext)
                bindingContext.OnDisappearing();
        }
    }
}