﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Targeting
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TargetingDetailsPage : TabbedPage
	{
		public TargetingDetailsPage (int id)
	    {
	        InitializeComponent();
	        BindingContext = new TargetingDetailsViewModel(Navigation, id);
		}
	}
}