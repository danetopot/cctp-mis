﻿namespace CCTPMIS.RegMobileApp.Views
{
    using CCTPMIS.RegMobileApp.ViewModels;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHouseHoldRegistrationStep01Page : ContentPage
    {
        public NewHouseHoldRegistrationStep01Page()
        {
            InitializeComponent();

            BindingContext = new NewHouseHoldRegistrationStep01ViewModel(Navigation);
        }
    }
}