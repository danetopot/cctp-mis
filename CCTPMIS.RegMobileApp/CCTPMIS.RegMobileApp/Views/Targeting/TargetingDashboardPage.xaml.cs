﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Targeting
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TargetingDashboardPage : ContentPage
	{
	    TargetingViewModel ViewModel => vm ?? (vm = BindingContext as TargetingViewModel);
        private TargetingViewModel vm;

        public TargetingDashboardPage ()
		{
			InitializeComponent ();
		    BindingContext = vm = new TargetingViewModel(Navigation);
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();
	        switch (BindingContext)
	        {
	            case TargetingViewModel bindingContext:
	                bindingContext.OnAppearing();
	                vm.Households.ReplaceRange(bindingContext.GetHouseholds());
	                break;
	        }
	    }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();

	        ComValDashboardViewModel bindingContext;
	        bindingContext = BindingContext as ComValDashboardViewModel;

	        if (bindingContext != null)
	            bindingContext.OnDisappearing();
	    }
    }
}