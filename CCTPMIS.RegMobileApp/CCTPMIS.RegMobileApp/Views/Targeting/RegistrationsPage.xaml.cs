﻿namespace CCTPMIS.RegMobileApp.Views
{
    using System;

    using CCTPMIS.RegMobileApp.Models;
    using CCTPMIS.RegMobileApp.ViewModels;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HouseHoldRegistrationsPage : ContentPage
    {
        private RegistrationsViewModel viewModel;

        public HouseHoldRegistrationsPage()
        {
            InitializeComponent();
            Regs.ItemsSource = App.Database.GetHouseHolds();
            BindingContext = viewModel=  new RegistrationsOldsViewModel();
         
        }

        protected override void OnAppearing()
        {
        
            base.OnAppearing();

           Regs.ItemsSource =   App.Database.GetHouseHolds();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Regs.ItemsSource = null; 


        }

        private async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewHouseHoldRegistrationStep01Page()));
           
        }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as HouseHoldRegistration;
            if (item == null)
                return;

            await Navigation.PushAsync(new RegistrationDetailPage(new RegistrationDetailViewModel(item)));

            
        }
    }
}