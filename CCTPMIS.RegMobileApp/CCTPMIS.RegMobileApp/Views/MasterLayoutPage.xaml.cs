﻿using CCTPMIS.RegMobileApp.Views.Registration;
using CCTPMIS.RegMobileApp.Views;
using CCTPMIS.RegMobileApp.Views.Account;
using CCTPMIS.RegMobileApp.Views.Sync;
using CCTPMIS.RegMobileApp.Views.Targeting;

namespace CCTPMIS.RegMobileApp.Views
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Helpers;
    using CCTPMIS.RegMobileApp.Models;
    using CCTPMIS.RegMobileApp.Pages;

    using FormsToolkit;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterLayoutPage : MasterDetailPage
    {
        private bool isRunning = false;

        private DeepLinkPage page;

        private Dictionary<int, CctpNavigationPage> pages;

        public MasterLayoutPage()
        {
            pages = new Dictionary<int, CctpNavigationPage>
                        {
                            { 0, new CctpNavigationPage( new MasterLayoutPageDetail())},
                            { 1, new CctpNavigationPage( new HHListingsDashboardPage())},
                            { 2, new CctpNavigationPage( new ComValDashboardPage()) },
                            { 3, new CctpNavigationPage( new TargetingDashboardPage()) },
                         //   { 4, new CctpNavigationPage( new SyncdDashboardPage()) },
                            { 4, new CctpNavigationPage( new SyncTabbedPage()) },
                            { 5, new CctpNavigationPage( new AboutPage())},
                            { 6, new CctpNavigationPage( new ProfileDashboardPage())},
                            { 7, new CctpNavigationPage( new SettingsDashboardPage()) },
                            { 8, new CctpNavigationPage( new LogoutPage()) },
                        };

            Master = new MasterLayoutPageMaster();

            Detail = pages[0];

            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;

            MessagingService.Current.Subscribe<DeepLinkPage>(
                "DeepLinkPage",
                async (m, p) =>
                    {
                        page = p;

                        if (isRunning)
                            await GoToDeepLink();
                    });
        }

        public void NavigateAsync(int menuId)
        {
            CctpNavigationPage newPage = null;
            if (!pages.ContainsKey(menuId))
            {
                // only cache specific pages
                switch (menuId)
                {
                    case (int)AppPage.HomePage: // Main Page
                        pages.Add(menuId, new CctpNavigationPage(new MasterLayoutPageDetail()));
                        break;

                    case (int)AppPage.Registration: // Registration
                        pages.Add(menuId, new CctpNavigationPage(new HHListingsDashboardPage()));
                        break;

                    case (int)AppPage.CommunityVal: // Registration
                        pages.Add(menuId, new CctpNavigationPage(new ComValDashboardPage()));
                        break;

                    case (int)AppPage.Targeting: // Targeting
                        pages.Add(menuId, new CctpNavigationPage(new TargetingDashboardPage()));
                        break;

                    case (int)AppPage.Sync:  //sync
                        newPage = new CctpNavigationPage(new SyncTabbedPage());
                        break;

                    case (int)AppPage.Settings:
                        newPage = new CctpNavigationPage(new ProfileDashboardPage());
                        break;

                    case (int)AppPage.About:
                        newPage = new CctpNavigationPage(new SettingsDashboardPage());
                        break;

                    case (int)AppPage.MyProfile:
                        newPage = new CctpNavigationPage(new ProfileDashboardPage());
                        break;

                    case (int)AppPage.Logout:
                        newPage = new CctpNavigationPage(new LogoutPage());
                        break;
                }
            }

            if (newPage == null)
                newPage = pages[menuId];

            if (newPage == null)
                return;

            // if we are on the same tab and pressed it again.
            if (Detail == newPage)
            {
                newPage.Navigation.PopToRootAsync();
            }

         //   newPage.Navigation.PushAsync(Detail);

            Detail = newPage;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (Settings.Current.FirstRun)
            {
                MessagingService.Current.SendMessage(MessageKeys.NavigateLogin);
            }

            isRunning = true;

            await GoToDeepLink();
        }

        private async Task GoToDeepLink()
        {
            if (page == null)
                return;
            var p = page.Page;
            var id = page.Id;
            page = null;
            switch (p)
            {
                case AppPage.HomePage:
                    NavigateAsync((int)AppPage.HomePage);
                    await Detail.Navigation.PushAsync(new MasterDetailPage());
                    break;
            }
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is MasterLayoutPageMenuItem item))
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            // Detail = new NavigationPage(page);
            IsPresented = false;
            NavigateAsync(item.Id);

            MasterPage.ListView.SelectedItem = null;
        }
    }
}