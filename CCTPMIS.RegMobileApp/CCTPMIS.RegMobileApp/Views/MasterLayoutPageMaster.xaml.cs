﻿using Plugin.DeviceInfo;

namespace CCTPMIS.RegMobileApp.Views
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterLayoutPageMaster : ContentPage
    {
        public ListView ListView;

        public MasterLayoutPageMaster()
        {
            InitializeComponent();

            BindingContext = new MasterLayoutPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        private class MasterLayoutPageMasterViewModel : INotifyPropertyChanged
        {
            public MasterLayoutPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterLayoutPageMenuItem>(
                    new[]
                        {
                            new MasterLayoutPageMenuItem { Id = 0, Title = "Dashboard", Icon = "menu_venue.png" },
                            new MasterLayoutPageMenuItem { Id = 1, Title = "Household Listing", Icon = "menu_venue.png" },
                            new MasterLayoutPageMenuItem { Id = 2, Title = "Community Validation", Icon = "menu_info.png" },
                            new MasterLayoutPageMenuItem { Id = 3, Title = "Household Registration", Icon = "menu_info.png" },
                            new MasterLayoutPageMenuItem { Id = 4, Title = "Up-Down Data Transfer", Icon = "menu_settings.png" },
                            new MasterLayoutPageMenuItem { Id = 5, Title = "Household Re-certification", Icon = "menu_sponsors.png" },
                            new MasterLayoutPageMenuItem { Id = 6, Title = "My Profile", Icon = "menu_feed.png" },
                            new MasterLayoutPageMenuItem { Id = 7, Title = "Settings", Icon = "menu_feed.png" },
                            new MasterLayoutPageMenuItem { Id = 8, Title = "Logout",  Icon = "menu_venue.png" },
                        });
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public ObservableCollection<MasterLayoutPageMenuItem> MenuItems { get; set; }

            private void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}