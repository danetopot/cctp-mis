﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Sync
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SyncTabbedPage : TabbedPage
    {
        private SyncViewModel vm;

        public SyncTabbedPage ()
        {
            InitializeComponent();
            BindingContext = vm = new SyncViewModel(Navigation);
        }
    }
}