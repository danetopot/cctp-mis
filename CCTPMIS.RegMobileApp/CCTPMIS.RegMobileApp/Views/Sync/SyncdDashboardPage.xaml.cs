﻿using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Sync
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SyncdDashboardPage : ContentPage
	{
	    SyncViewModel vm;
	    public SyncdDashboardPage()
	    {
	        InitializeComponent();

	        BindingContext = vm = new SyncViewModel(Navigation);
        }

	}
}