﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.ViewModels;
using CCTPMIS.RegMobileApp.ViewModels.Accounts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ProfileDashboardPage : ContentPage
    {
        private ProfileViewModel vm;

        public ProfileDashboardPage()
	    {
	        InitializeComponent();

	        BindingContext = vm = new ProfileViewModel(Navigation);
	    }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

            if (BindingContext is ProfileViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                if (Helpers.Settings.Current.EnumeratorId > 0)
                {
                    var data = App.Database.GetTableRow("Enumerator", "Id", Helpers.Settings.Current.EnumeratorId.ToString());
                    vm.Enumerator = (Enumerator)data;
                }
            }
        }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();

            if (BindingContext is ProfileViewModel bindingContext)
                bindingContext.OnDisappearing();
        }
    }
}