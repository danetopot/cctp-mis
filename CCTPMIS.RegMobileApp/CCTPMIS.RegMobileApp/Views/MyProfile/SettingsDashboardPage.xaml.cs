﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using MvvmHelpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsDashboardPage : TabbedPage
	{
		public SettingsDashboardPage ()
		{
			InitializeComponent ();
		    BindingContext = new ViewModelBase();
        }
	}
}