﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Registration
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHHListingPage : ContentPage
    {
        public NewHHListingPage()
        {
            InitializeComponent();
            BindingContext = new HHListingNewViewModel(Navigation);

            var Year = DateTime.Now.Year+1;
            var minOPCTyr = new DateTime(Year-70,1,1);
            BeneDoB.SetValue(DatePicker.MaximumDateProperty, minOPCTyr);
            var minDate = Year - 1900;
            var mincgOPCTyr = new DateTime(Year - minDate, 1, 1);

            CgDoB.SetValue(DatePicker.MaximumDateProperty, DateTime.Now.AddYears(-18));

            CgDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
            BeneDoB.SetValue(DatePicker.MinimumDateProperty, mincgOPCTyr);
        }
    }
}