﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CCTPMIS.RegMobileApp.Helpers;
using FormsToolkit;

namespace CCTPMIS.RegMobileApp.Views.Registration
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HHListingsDashboardPage : ContentPage
    {
        HHListingsViewModel ViewModel => vm ?? (vm = BindingContext as HHListingsViewModel);
        private HHListingsViewModel vm;
        private bool showFavs, showPast, showAllCategories;
        private string filteredCategories;
        private ToolbarItem filterItem;
        private string loggedIn;

        public HHListingsDashboardPage()
        {
            InitializeComponent();

            loggedIn = Settings.Current.Email;
            showFavs = Settings.Current.FavoritesOnly;
            showPast = Settings.Current.ShowPastRegistrations;
            showAllCategories = Settings.Current.ShowAllCategories;
            filteredCategories = Settings.Current.FilteredCategories;
            BindingContext = vm = new HHListingsViewModel(Navigation);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext is HHListingsViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                vm.Registrations.ReplaceRange(bindingContext.GetRegistrations());
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            var bindingContext = BindingContext as HHListingsViewModel;

            if (bindingContext != null)
                bindingContext.OnDisappearing();
        }

        private async void NewHouseHold_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new NavigationPage(new NewHHListingPage()));
            }
            catch (Exception exception)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Load Registration Page",
                    Message = exception.Message,
                    Cancel = "OK"
                });
            }
        }
    }
}