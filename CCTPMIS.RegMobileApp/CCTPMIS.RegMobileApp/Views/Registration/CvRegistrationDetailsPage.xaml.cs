﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Registration
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CvRegistrationDetailsPage : ContentPage
	{
        public CvRegistrationDetailsPage(int id)
        {
            InitializeComponent();
            BindingContext = new ComValDetailsViewModel(Navigation,id);
        }
    }
}