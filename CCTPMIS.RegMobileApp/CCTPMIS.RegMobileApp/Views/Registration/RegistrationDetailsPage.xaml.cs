﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Views.Registration
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationDetailsPage : ContentPage
	{
		public RegistrationDetailsPage (int id)
		{
			InitializeComponent ();

		    BindingContext = new HHListingDetailViewModel(Navigation, id);
        }
	}
}