﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CCTPMIS.RegMobileApp.Helpers;
using FormsToolkit;

namespace CCTPMIS.RegMobileApp.Views.Registration
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ComValDashboardPage : TabbedPage
	{
	    ComValDashboardViewModel ViewModel => vm ?? (vm = BindingContext as ComValDashboardViewModel);
        private ComValDashboardViewModel vm;
        private bool showFavs, showPast, showAllCategories;
        private string filteredCategories;
        private ToolbarItem filterItem;
        private string loggedIn;

        public ComValDashboardPage ()
		{
			InitializeComponent ();
		    loggedIn = Settings.Current.Email;
		    showFavs = Settings.Current.FavoritesOnly;
		    showPast = Settings.Current.ShowPastRegistrations;
		    showAllCategories = Settings.Current.ShowAllCategories;
		    filteredCategories = Settings.Current.FilteredCategories;
		    BindingContext = vm = new ComValDashboardViewModel(Navigation);
        }

	    protected override void OnAppearing()
	    {
	        base.OnAppearing();

            if (BindingContext is ComValDashboardViewModel bindingContext)
            {
                bindingContext.OnAppearing();
                vm.ComValListingPlanHHs.ReplaceRange(bindingContext.GetComValListingPlanHHs());
                vm.ComValListingPlanHHExceptions.ReplaceRange(bindingContext.GetComValListingPlanHHsExceptions());
                vm.ComValListingPlanHHReady.ReplaceRange(bindingContext.GetComValListingPlanHHReady());
            }
        }

	    protected override void OnDisappearing()
	    {
	        base.OnDisappearing();

	        if (BindingContext is ComValDashboardViewModel bindingContext)
	            bindingContext.OnDisappearing();
	    }
    }
}