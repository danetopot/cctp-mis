﻿using MvvmHelpers;

namespace CCTPMIS.RegMobileApp.Views
{
    using System.IO;
    using System.Reflection;

    using CCTPMIS.RegMobileApp.Models;

    using Newtonsoft.Json;

    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();

            BindingContext = new BaseViewModel();
        }
    }
}