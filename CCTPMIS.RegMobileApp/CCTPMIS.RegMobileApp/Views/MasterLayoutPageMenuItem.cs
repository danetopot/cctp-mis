﻿namespace CCTPMIS.RegMobileApp.Views
{
    using System;

    public class MasterLayoutPageMenuItem
    {
        public MasterLayoutPageMenuItem()
        {
            TargetType = typeof(MasterLayoutPageDetail);
        }

        public string Icon { get; set; }

        public int Id { get; set; }

        public Type TargetType { get; set; }

        public string Title { get; set; }
    }
}