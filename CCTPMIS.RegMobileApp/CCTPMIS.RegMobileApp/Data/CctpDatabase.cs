﻿using System;
using System.Diagnostics;

namespace CCTPMIS.RegMobileApp.Data
{
    using System.Collections.Generic;
    using System.Linq;

    using CCTPMIS.RegMobileApp.Models;

    using SQLite;

    using Xamarin.Forms;

    // using Location = Plugin.Media.Abstractions.Location;
    public class CctpDatabase
    {
        private readonly SQLiteConnection _database;
        private readonly string nameSpace = "CCTPMIS.RegMobileApp.Models.";

        public CctpDatabase()
        {
            _database = DependencyService.Get<ISQLite>().GetConnection();
            this._database.CreateTable<SystemCode>();
            this._database.CreateTable<SystemCodeDetail>();
            this._database.CreateTable<Location>();
            this._database.CreateTable<SubLocation>();
            this._database.CreateTable<Programme>();
             this._database.CreateTable<County>();
            this._database.CreateTable<Constituency>();
            this._database.CreateTable<Enumerator>();
            this._database.CreateTable<Listing>();
            this._database.CreateTable<ComValListingPlanHH>();
            this._database.CreateTable<TargetingHh>();
            this._database.CreateTable<TargetingHhMember>();
            this._database.CreateTable<EnumeratorLocation>();
        }

        public virtual void AddOrUpdate<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.InsertOrReplace(entity);
        }

        public virtual void Create<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Insert(entity);
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Update(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            this._database.Delete(entity);
        }

        public virtual void Delete<TEntity>(int primarykey) where TEntity : class
        {
            this._database.Delete<TEntity>(primarykey);
        }

        public virtual void Manage<TEntity>(TEntity entity) where TEntity : class
        {
        }

        public int DeleteHouseHold(HouseHoldRegistration houseHoldRegistration)
        {
            return this._database.Delete(houseHoldRegistration);
        }

        public HouseHoldRegistration GetHouseHold(int id)
        {
            return this._database.Table<HouseHoldRegistration>().Where(i => i.Id == id).FirstOrDefault();
        }

        public IEnumerable<HouseHoldRegistration> GetHouseHolds()
        {
            return this._database.Table<HouseHoldRegistration>().ToList();
        }

        public List<HouseHoldRegistration> GetHouseHoldsNotDone()
        {
            return this._database.Query<HouseHoldRegistration>(
                "SELECT * FROM [HouseHoldRegistration] WHERE [Status] = 0");
        }

        public int SaveHouseHold(HouseHoldRegistration houseHoldRegistration)
        {
            if (houseHoldRegistration.Id != 0)
            {
                return this._database.Update(houseHoldRegistration);
            }
            else
            {
                return this._database.Insert(houseHoldRegistration);
            }
        }

        public int SaveHouseHoldMember(TargetingHhMember member)
        {
            if (member.Id != 0)
            {
                return this._database.Update(member);
            }
            else
            {
                return this._database.Insert(member);
            }
        }

        public int SaveRegistration(Listing registration)
        {
            if (registration.LocalId != 0)
            {
                return this._database.Update(registration);
            }
            else
            {
                return this._database.Insert(registration);
            }
        }

        #region Registrations

        public List<Listing> RegistrationGetAll()
        {
            return this._database.Query<Listing>(
                "SELECT * FROM [Registration]");
        }

        public SystemCodeDetail RegistrationGetById(int id)
        {
            return this._database.Query<SystemCodeDetail>($"SELECT * FROM [Registration] WHERE [Id] = {id}").Single();
        }

        public SystemCodeDetail RegistrationGetById(bool id)
        {
            return this._database.Query<SystemCodeDetail>($"SELECT * FROM [Registration] WHERE [IsFavourite] = {id}").Single();
        }

        #endregion Registrations

        #region SystemCodeDetail

        public List<SystemCodeDetail> SystemCodeDetailGetAll()
        {
            return this._database.Query<SystemCodeDetail>(
                "SELECT * FROM [SystemCodeDetail]");
        }

        public List<SystemCodeDetail> SystemCodeDetailsGetByCode(string code)
        {
            var systemCode = this._database.Query<SystemCode>($"select * from systemCode where code = '{code}'").Single();

            var systemcodetails = this._database.Query<SystemCodeDetail>(
                $"SELECT * FROM [SystemCodeDetail] WHERE SystemCodeId ={systemCode.Id} ");
            return systemcodetails;
        }

        public SystemCodeDetail SystemCodeDetailGetById(int id)
        {
            return this._database.Query<SystemCodeDetail>($"SELECT * FROM [SystemCodeDetail] WHERE [Id] = {id}").Single();
        }

        public SystemCodeDetail SystemCodeDetailGetByCode(string parentCode, string childCode)
        {
            var sql1 = $"SELECT * FROM [SystemCode]  WHERE [Code] = '{parentCode}'";
            Debug.WriteLine(sql1);
            var systemCode = this._database.Query<SystemCode>(sql1).Single();

            var sql2 =
                $"SELECT * FROM [SystemCodeDetail] WHERE [Code] = '{childCode}' AND [SystemCodeId] ={systemCode.Id} ";
            Debug.WriteLine(sql2);
            return this._database.Query<SystemCodeDetail>(sql2).Single();
        }

        #endregion SystemCodeDetail

        #region Location

        public List<Location> LocationsGetAll()
        {
            return this._database.Query<Location>(
                "SELECT * FROM [Location]");
        }

        public List<Location> LocationsGetByEnumerator(int id)
        {
            var q = _database.Query<Location>("SELECT L.Id,L.Name FROM [Location] L inner JOIN [EnumeratorLocation] EL on L.Id = EL.LocationId and EL.IsActive=1 where EL.EnumeratorId = ?", id).ToList();
            return q.Select(x => new Location { Id = x.Id, Name = x.Name }).ToList();
        }

        public Location LocationGetById(int id)
        {
            return this._database.Query<Location>($"SELECT * FROM [Location] WHERE [Id] = {id}").Single();
        }

        #endregion Location

        #region SubLocation

        public List<SubLocation> SubLocationGetAll()
        {
            return this._database.Query<SubLocation>(
                "SELECT * FROM [SubLocation] WHERE [SystemCodeId] = 1");
        }

        public SubLocation SubLocationGetById(int id)
        {
            return this._database.Query<SubLocation>($"SELECT * FROM [SubLocation] WHERE [Id] = {id}").Single();
        }

        public List<SubLocation> SubLocationGetByLocationId(int id)
        {
            return this._database.Query<SubLocation>($"SELECT * FROM [SubLocation] WHERE [LocationId] = {id}");
        }

        #endregion SubLocation

        #region Enumaretors

        public List<Enumerator> EnumaretorsGetAll()
        {
            return this._database.Query<Enumerator>(
                "SELECT * FROM [Enumerator]");
        }

        public Enumerator EnumeratorGetById(int id)
        {
            return this._database.Query<Enumerator>($"SELECT * FROM [Enumerator] WHERE [Id] = {id}").Single();
        }

        #endregion Enumaretors

        #region Abstract Get

        public List<object> GetTable(string tableName)
        {
            // tableName =  + tableName;
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM [" + tableName + "]";
            return _database.Query(map, query, obj).ToList();
        }

        public List<object> GetTableRows(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).ToList();
        }

        public object GetTableRow(string tableName, string column, string value)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column + " = '" + value + "'";
            return _database.Query(map, query, obj).Single();
        }

        public object GetTableRow(string tableName, string column1, string value1, string column2, string value2)
        {
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + tableName));
            string query = "SELECT * FROM " + tableName + " WHERE " + column1 + " = '" + value1 + "' AND " + column2 + " = '" + value2 + "' LIMIT 1";
            return _database.Query(map, query, obj).Single();
        }

        public object GetCvRegData(int id)
        {
            var query =
                $"SELECT * FROM [CvRegistration] R INNER JOIN [SubLocation] A ON R.SubLocationId = A.Id and R.Id = {id}";
            object[] obj = new object[] { };
            TableMapping map = new TableMapping(Type.GetType(nameSpace + "CvRegistration"));
            return _database.Query(map, query, obj).Single();
        }

        #endregion Abstract Get
    }
}