﻿namespace CCTPMIS.RegMobileApp.Data
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;

    using CCTPMIS.RegMobileApp.Models;

    using Newtonsoft.Json;

    /*
    public static class HouseHoldMemberOptionsData
    {
        static HouseHoldMemberOptionsData()
        {
            Assembly assembly = typeof(RegistrationData).Assembly;
            var stream = assembly.GetManifestResourceStream("CCTPMIS.RegMobileApp.LocalData.HHMD.json");
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                var localData = JsonConvert.DeserializeObject<HouseHoldMemberDataOption>(json);

                MemberIdentificationDocumentTypes = localData.MemberIdentificationDocumentTypes;
                MemberMaritalStatuses = localData.MemberMaritalStatuses;
                MemberSexs = localData.MemberSexs;
                MemberRelationships = localData.MemberRelationships;
                MemberStatusOptions = localData.MemberStatusOptions;
                MemberDisabilityTypes = localData.MemberDisabilityTypes;
                MemberDisabilityCareStatuses = localData.MemberDisabilityCareStatuses;
                MemberLearningStatuses = localData.MemberLearningStatuses;
                MemberEducationLevels = localData.MemberEducationLevels;
                MemberWorkTypes = localData.MemberWorkTypes;
                MemberMotherAliveOptions = localData.MemberMotherAliveOptions;
                MemberFatherAliveOptions = localData.MemberFatherAliveOptions;
                MemberJobOptions = localData.MemberJobOptions;
                MemberChronicIllnessOptions = localData.MemberChronicIllnessOptions;
            }
        }

        public static IList<SystemCodeDetail> MemberChronicIllnessOptions { get; set; }
        public static IList<SystemCodeDetail> MemberDisabilityCareStatuses { get; set; }
        public static IList<SystemCodeDetail> MemberDisabilityTypes { get; set; }
        public static IList<SystemCodeDetail> MemberEducationLevels { get; set; }
        public static IList<SystemCodeDetail> MemberFatherAliveOptions { get; set; }
        public static IList<SystemCodeDetail> MemberIdentificationDocumentTypes { get; set; }
        public static IList<SystemCodeDetail> MemberJobOptions { get; set; }
        public static IList<SystemCodeDetail> MemberLearningStatuses { get; set; }
        public static IList<SystemCodeDetail> MemberMaritalStatuses { get; set; }
        public static IList<SystemCodeDetail> MemberMotherAliveOptions { get; set; }
        public static IList<SystemCodeDetail> MemberRelationships { get; set; }
        public static IList<SystemCodeDetail> MemberSexs { get; set; }
        public static IList<SystemCodeDetail> MemberStatusOptions { get; set; }
        public static IList<SystemCodeDetail> MemberWorkTypes { get; set; }
    }

    public static class RegistrationData
    {
        static RegistrationData()
        {
            Assembly assembly = typeof(RegistrationData).Assembly;
            var stream = assembly.GetManifestResourceStream("CCTPMIS.RegMobileApp.LocalData.LocalData.json");
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                var localData = JsonConvert.DeserializeObject<StaticData>(json);
                Programmes = localData.Programmes;
                Counties = localData.Counties;
                Divisions = localData.Divisions;
                Constituencies = localData.Constituencies;
                Locations = localData.Locations;
                SubLocations = localData.SubLocations;
                Localities = localData.Localities;
                TenureStatuses = localData.TenureStatuses;
                RoofConstructionMaterials = localData.RoofConstructionMaterials;
                WallConstructionMaterials = localData.WallConstructionMaterials;
                FloorConstructionMaterials = localData.FloorConstructionMaterials;
                DwellingRisks = localData.DwellingRisks;
                WaterSources = localData.WaterSources;
                WasteDisposals = localData.WasteDisposals;
                CookingFuels = localData.CookingFuels;
                LightingFuels = localData.LightingFuels;
                HouseholdConditions = localData.HouseholdConditions;
                NsnpBenefits = localData.NsnpBenefits;
                BenefitTypes = localData.BenefitTypes;
            }
        }

        public static IList<OptionsModel> BenefitTypes { get; set; }

        public static IList<Constituency> Constituencies { get; set; }

        public static IList<OptionsModel> CookingFuels { get; set; }

        public static IList<County> Counties { get; set; }

        public static IList<Division> Divisions { get; set; }

        public static IList<OptionsModel> DwellingRisks { get; set; }

        public static IList<OptionsModel> FloorConstructionMaterials { get; set; }

        public static IList<OptionsModel> HouseholdConditions { get; set; }

        public static IList<OptionsModel> LightingFuels { get; set; }

        public static IList<Locality> Localities { get; set; }

        public static IList<Location> Locations { get; set; }

        public static IList<OptionsModel> NsnpBenefits { get; set; }

        public static IList<Programme> Programmes { get; set; }

        public static IList<OptionsModel> RoofConstructionMaterials { get; set; }

        public static IList<SubLocation> SubLocations { get; set; }

        public static IList<OptionsModel> TenureStatuses { get; set; }

        public static IList<OptionsModel> WallConstructionMaterials { get; set; }

        public static IList<OptionsModel> WasteDisposals { get; set; }

        public static IList<OptionsModel> WaterSources { get; set; }
    }

    */
}