﻿namespace CCTPMIS.RegMobileApp.Data
{
    public static class ApiKeys
    {
        public const string AzureHubName = "AzureHubName";

        public const string AzureKey = "AzureKey";

        public const string AzureListenConneciton = "AzureListenConneciton";

        public const string AzureServiceBusName = "AzureServiceBusName";

        public const string AzureServiceBusUrl = "AzureServiceBusUrl";

        public const string GoogleSenderId = "GoogleSenderId";

        public const string HockeyAppAndroid = "HockeyAppAndroid";

        public const string HockeyAppiOS = "HockeyAppiOS";

        public const string HockeyAppUWP = "HockeyAppUWP";
    }
}