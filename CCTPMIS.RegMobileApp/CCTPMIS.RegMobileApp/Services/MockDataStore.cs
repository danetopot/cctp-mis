﻿namespace CCTPMIS.RegMobileApp.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using CCTPMIS.RegMobileApp.Models;

    public class MockDataStore : IDataStore<HouseHoldRegistration>
    {
        private List<HouseHoldRegistration> items;

        public MockDataStore()
        {
            items = new List<HouseHoldRegistration>();
            var mockItems = new List<HouseHoldRegistration>
                                {
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "First houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "Second houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "Third houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "Fourth houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "Fifth houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                    // new HouseHoldRegistration { Id = Guid.NewGuid().ToString(), Text = "Sixth houseHoldRegistration", Description="This is an houseHoldRegistration description." },
                                };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            items.Add(houseHoldRegistration);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            var _item = items.Where((HouseHoldRegistration arg) => arg.Id == houseHoldRegistration.Id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<HouseHoldRegistration> GetItemAsync(int id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<HouseHoldRegistration>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }

        public async Task<bool> UpdateItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            var _item = items.Where((HouseHoldRegistration arg) => arg.Id == houseHoldRegistration.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(houseHoldRegistration);

            return await Task.FromResult(true);
        }
    }
}