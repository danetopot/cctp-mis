﻿namespace CCTPMIS.RegMobileApp.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDataStore<T>
    {
        Task<bool> AddItemAsync(T item);

        Task<bool> DeleteItemAsync(T item);

        Task<T> GetItemAsync(int id);

        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);

        Task<bool> UpdateItemAsync(T item);
    }
}