﻿namespace CCTPMIS.RegMobileApp.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading.Tasks;

    using CCTPMIS.RegMobileApp.Models;

    using Microsoft.WindowsAzure.MobileServices;
    using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
    using Microsoft.WindowsAzure.MobileServices.Sync;

    public class AzureDataStore : IDataStore<HouseHoldRegistration>
    {
        private bool isInitialized;

        private IMobileServiceSyncTable<HouseHoldRegistration> itemsTable;

        public MobileServiceClient MobileService { get; set; }

        public async Task<bool> AddItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            await InitializeAsync();
            await PullLatestAsync();
            await itemsTable.InsertAsync(houseHoldRegistration);
            await SyncAsync();

            return true;
        }

        public async Task<bool> DeleteItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            await InitializeAsync();
            await PullLatestAsync();
            await itemsTable.DeleteAsync(houseHoldRegistration);
            await SyncAsync();

            return true;
        }

        public async Task<HouseHoldRegistration> GetItemAsync(int id)
        {
            await InitializeAsync();
            await PullLatestAsync();
            var items = await itemsTable.Where(s => s.Id == id).ToListAsync();

            if (items == null || items.Count == 0)
                return null;

            return items[0];
        }

        public async Task<IEnumerable<HouseHoldRegistration>> GetItemsAsync(bool forceRefresh = false)
        {
            await InitializeAsync();
            if (forceRefresh)
                await PullLatestAsync();

            return await itemsTable.ToEnumerableAsync();
        }

        public async Task InitializeAsync()
        {
            if (isInitialized)
                return;

            MobileService = new MobileServiceClient(App.AzureMobileAppUrl)
                                {
                                    SerializerSettings =
                                        new
                                        MobileServiceJsonSerializerSettings
                                            {
                                                CamelCasePropertyNames
                                                    = true
                                            }
                                };

            var path = "syncstore.db";
            path = Path.Combine(MobileServiceClient.DefaultDatabasePath, path);
            var store = new MobileServiceSQLiteStore(path);

            store.DefineTable<HouseHoldRegistration>();
            await MobileService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
            itemsTable = MobileService.GetSyncTable<HouseHoldRegistration>();

            isInitialized = true;
        }

        public async Task<bool> PullLatestAsync()
        {
            try
            {
                await itemsTable.PullAsync($"all{typeof(HouseHoldRegistration).Name}", itemsTable.CreateQuery());
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Unable to pull items: {ex.Message}");

                return false;
            }

            return true;
        }

        public async Task<bool> SyncAsync()
        {
            try
            {
                await MobileService.SyncContext.PushAsync();
                if (!(await PullLatestAsync().ConfigureAwait(false)))
                    return false;
            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult == null)
                {
                    Debug.WriteLine($"Unable to sync, that is alright as we have offline capabilities: {exc.Message}");

                    return false;
                }

                foreach (var error in exc.PushResult.Errors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        // Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(
                        $"Error executing sync operation. HouseHoldRegistration: {error.TableName} ({error.Item["id"]}). Operation discarded.");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Unable to sync items: {ex.Message}");
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateItemAsync(HouseHoldRegistration houseHoldRegistration)
        {
            await InitializeAsync();
            await itemsTable.UpdateAsync(houseHoldRegistration);
            await SyncAsync();

            return true;
        }
    }
}