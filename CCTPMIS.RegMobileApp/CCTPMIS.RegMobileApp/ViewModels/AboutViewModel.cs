﻿using MvvmHelpers;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System;
    using System.Windows.Input;

    using Xamarin.Forms;

    public class AboutViewModel : ViewModelBase
    {
        public AboutViewModel()
        {
            Title = "About SAU CCTPMIS";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://inuajamii.go.ke/")));
        }

        public ICommand OpenWebCommand { get; }
    }
}