﻿namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Models;

    using MvvmHelpers;

    using Xamarin.Forms;

    public class NewTargetingViewModel : ViewModelBase
    {
        public ObservableRangeCollection<OptionsModel> BenefitTypes = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<Constituency> Constituencies = new ObservableRangeCollection<Constituency>();

        public ObservableRangeCollection<OptionsModel> CookingFuels = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<County> Counties = new ObservableRangeCollection<County>();

        public ObservableRangeCollection<Division> Divisions = new ObservableRangeCollection<Division>();

        public ObservableRangeCollection<OptionsModel> DwellingRisks = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> FloorConstructionMaterials =
            new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> HouseholdConditions =
            new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> LightingFuels = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<Locality> Localities = new ObservableRangeCollection<Locality>();

        public ObservableRangeCollection<Location> Locations = new ObservableRangeCollection<Location>();

        public ObservableRangeCollection<OptionsModel> NsnpBenefits = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<Programme> Programmes = new ObservableRangeCollection<Programme>();

        public ObservableRangeCollection<OptionsModel> RoofConstructionMaterials =
            new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<SubLocation> SubLocations = new ObservableRangeCollection<SubLocation>();

        public ObservableRangeCollection<OptionsModel> TenureStatuses = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> WallConstructionMaterials =
            new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> WasteDisposals = new ObservableRangeCollection<OptionsModel>();

        public ObservableRangeCollection<OptionsModel> WaterSources = new ObservableRangeCollection<OptionsModel>();

        private ICommand saveHouseHoldCommand;

        private Constituency selectedConstituency;

        private County selectedCounty;

        private SubLocation selectedDivision;

        private SubLocation selectedLocality;

        private SubLocation selectedLocation;

        private Programme selectedProgramme;

        private SubLocation selectedSubLocation;

        public NewTargetingViewModel(INavigation navigation, HouseHoldRegistration houseHoldRegistration = null)
            : base(navigation)
        {
            LoadedCounties.AddRange(RegistrationData.Counties);
            LoadedConstituencies.AddRange(RegistrationData.Constituencies);
            LoadedProgrammes.AddRange(RegistrationData.Programmes);
            LoadedLocations.AddRange(RegistrationData.Locations);
            LoadedLocalities.AddRange(RegistrationData.Localities);
            LoadedDivisions.AddRange(RegistrationData.Divisions);
            LoadedSubLocations.AddRange(RegistrationData.SubLocations);
            LoadedTenureStatuses.AddRange(RegistrationData.TenureStatuses);
            LoadedRoofConstructionMaterials.AddRange(RegistrationData.RoofConstructionMaterials);
            LoadedWallConstructionMaterials.AddRange(RegistrationData.WallConstructionMaterials);
            LoadedFloorConstructionMaterials.AddRange(RegistrationData.FloorConstructionMaterials);
            LoadedDwellingRisks.AddRange(RegistrationData.DwellingRisks);
            LoadedWaterSources.AddRange(RegistrationData.WaterSources);
            LoadedWasteDisposals.AddRange(RegistrationData.WasteDisposals);
            LoadedCookingFuels.AddRange(RegistrationData.CookingFuels);
            LoadedLightingFuels.AddRange(RegistrationData.LightingFuels);
            LoadedHouseholdConditions.AddRange(RegistrationData.HouseholdConditions);
            LoadedNsnpBenefits.AddRange(RegistrationData.NsnpBenefits);
            LoadedBenefitTypes.AddRange(RegistrationData.BenefitTypes);
            HouseHoldRegistration = new HouseHoldRegistration();
            NextForceSync = DateTime.UtcNow.AddMinutes(45);
        }
        public DateTime NextForceSync { get; set; }

        public HouseHoldRegistration HouseHoldRegistration { get; set; }

        public ObservableRangeCollection<OptionsModel> LoadedBenefitTypes
        {
            get => BenefitTypes;
            set => SetProperty(ref BenefitTypes, value);
        }

        public ObservableRangeCollection<Constituency> LoadedConstituencies
        {
            get => Constituencies;
            set => SetProperty(ref Constituencies, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedCookingFuels
        {
            get => CookingFuels;
            set => SetProperty(ref CookingFuels, value);
        }

        public ObservableRangeCollection<County> LoadedCounties
        {
            get => Counties;
            set => SetProperty(ref Counties, value);
        }

        public ObservableRangeCollection<Division> LoadedDivisions
        {
            get => Divisions;
            set => SetProperty(ref Divisions, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedDwellingRisks
        {
            get => DwellingRisks;
            set => SetProperty(ref DwellingRisks, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedFloorConstructionMaterials
        {
            get => FloorConstructionMaterials;
            set => SetProperty(ref FloorConstructionMaterials, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedHouseholdConditions
        {
            get => HouseholdConditions;
            set => SetProperty(ref HouseholdConditions, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedLightingFuels
        {
            get => LightingFuels;
            set => SetProperty(ref LightingFuels, value);
        }

        public ObservableRangeCollection<Locality> LoadedLocalities
        {
            get => Localities;
            set => SetProperty(ref Localities, value);
        }

        public ObservableRangeCollection<Location> LoadedLocations
        {
            get => Locations;
            set => SetProperty(ref Locations, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedNsnpBenefits
        {
            get => NsnpBenefits;
            set => SetProperty(ref NsnpBenefits, value);
        }

        public ObservableRangeCollection<Programme> LoadedProgrammes
        {
            get => Programmes;
            set => SetProperty(ref Programmes, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedRoofConstructionMaterials
        {
            get => RoofConstructionMaterials;
            set => SetProperty(ref RoofConstructionMaterials, value);
        }

        public ObservableRangeCollection<SubLocation> LoadedSubLocations
        {
            get => SubLocations;
            set => SetProperty(ref SubLocations, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedTenureStatuses
        {
            get => TenureStatuses;
            set => SetProperty(ref TenureStatuses, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedWallConstructionMaterials
        {
            get => WallConstructionMaterials;
            set => SetProperty(ref WallConstructionMaterials, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedWasteDisposals
        {
            get => WasteDisposals;
            set => SetProperty(ref WasteDisposals, value);
        }

        public ObservableRangeCollection<OptionsModel> LoadedWaterSources
        {
            get => WaterSources;
            set => SetProperty(ref WaterSources, value);
        }

        public ICommand SaveHouseHoldCommand =>
            saveHouseHoldCommand ?? (saveHouseHoldCommand = new Command(async () => await ExecuteSaveHouseHold()));

        public Constituency SelectedConstituency
        {
            get => selectedConstituency;
            set
            {
                if (selectedConstituency == value) return;
                selectedConstituency = value;
                OnPropertyChanged();

                // Locations.Clear();
                // Locations.AddRange(RegistrationData.Locations.Where(x => x.DivisionId == this.selectedConstituency.Id));
            }
        }

        public County SelectedCounty
        {
            get => selectedCounty;
            set
            {
                if (selectedCounty == value) return;
                selectedCounty = value;
                OnPropertyChanged();
                this.Constituencies.Clear();
                Constituencies.AddRange(
                    RegistrationData.Constituencies.Where(x => x.CountyId == this.selectedCounty.Id));
            }
        }

        public SubLocation SelectedDivision
        {
            get => selectedDivision;
            set
            {
                if (selectedDivision == value) return;
                selectedDivision = value;
                OnPropertyChanged();

                Locations.Clear();
                Locations.AddRange(RegistrationData.Locations.Where(x => x.DivisionId == this.selectedDivision.Id));
            }
        }

        public SubLocation SelectedLocality
        {
            get => selectedLocality;
            set
            {
                if (selectedLocality == value) return;
                selectedLocality = value;
                OnPropertyChanged();
            }
        }

        public SubLocation SelectedLocation
        {
            get => selectedLocation;
            set
            {
                if (selectedLocation == value) return;
                selectedLocation = value;
                SubLocations.Clear();
                SubLocations.AddRange(
                    RegistrationData.SubLocations.Where(x => x.LocationId == this.selectedLocation.Id));
            }
        }

        public Programme SelectedProgramme
        {
            get => selectedProgramme;
            set
            {
                if (this.selectedProgramme == value) return;
                this.selectedProgramme = value;
                this.OnPropertyChanged();
            }
        }

        public SubLocation SelectedSubLocation
        {
            get => selectedSubLocation;
            set
            {
                if (selectedSubLocation == value) return;
                selectedSubLocation = value;

                // OnPropertyChanged();
            }
        }

        private async Task ExecuteSaveHouseHold()
        {
            var household = this.HouseHoldRegistration;
            household.Status = "Registered, Pending Upload";
            App.Database.SaveHouseHold(household);

            await Navigation.PopModalAsync();
        }
    }
}