﻿namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Models;
    using FormsToolkit;
    using Xamarin.Forms;


    public class RegistrationOldsViewModel : BaseViewModel
    {
        public RegistrationOldsViewModel()
        {
            Title = "House Holds";
          
            LoadHouseHoldsCommand = new Command(async () => await ExecuteLoadHouseHoldsCommand());
            this.HouseHoldRegistrations = GetHouseHoldRegistrations();
            // MessagingCenter.Subscribe<HouseHoldRegistrationsPage, HouseHoldRegistration>(this, "AddItem", async (obj, item) =>
            // {
            // var _item = item as HouseHoldRegistration;
            // this.HouseHoldRegistrations.Add(_item);
            // await DataStore.AddItemAsync(_item);
            // });
        }


        public ObservableCollection<HouseHoldRegistration> GetHouseHoldRegistrations()
        {
            var items = App.Database.GetHouseHolds();
            var hh = new ObservableCollection<HouseHoldRegistration>();
            foreach (var item in items)
            {
                hh.Add(item);
            }
            return hh;
        }

        public ObservableCollection<HouseHoldRegistration> HouseHoldRegistrations { get; set; }

        public Command LoadHouseHoldsCommand { get; set; }

        private async Task ExecuteLoadHouseHoldsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                this.HouseHoldRegistrations.Clear();
                var items = App.Database.GetHouseHolds();

                foreach (var item in items)
                {
                    this.HouseHoldRegistrations.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
            await Task.FromResult(0);
        }

        #region Properties
        HouseHoldRegistration selectedHouseHold;
        public HouseHoldRegistration SelectedHouseHold
        {
            get { return selectedHouseHold; }
            set
            {
                selectedHouseHold = value;
                OnPropertyChanged();
                if (selectedHouseHold == null)
                    return;

               // Navigate 
            }
        }


        #endregion

    }
}