﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Interfaces;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views;
using FormsToolkit;
using Plugin.Connectivity;
using Plugin.DeviceInfo;
using Xamarin.Forms;
using Settings = CCTPMIS.RegMobileApp.Helpers.Settings;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class SyncViewModel : ViewModelBase
    {
        #region Properties

        private ISSOClient client;
      //  public INavigation Navigation;

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        public SyncViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<ISSOClient>();
            email = Settings.Current.Email;
            password = Settings.Current.Password;

            SyncedDownloadListingTime = Settings.Current.LastSyncDown;
            SyncedUploadListingTime = Settings.Current.LastSync;
            IsBusyListing = false;

            syncDownListingText = "Download From the Server";
            syncUpListingText = "Upload Captured Household Listing Data";

            syncDownComValText = "Download Community Validation Data from Server";
            syncUpComValText = "Upload Community Validation Data To Server";

            syncDownRegistrationText = "Download Registration Data from Server";
            syncUpRegistrationText = "Upload Registration Data To Server";

        //    Navigation = navigation;
        }

        private int localListing;

        public int LocalListing
        {
            get { return localListing; }
            set { SetProperty(ref localListing, value); }
        }

        private DateTime syncedUploadListingTime;

        public DateTime SyncedUploadListingTime
        {
            get { return syncedUploadListingTime; }
            set { SetProperty(ref syncedUploadListingTime, value); }
        }

        private DateTime syncedDownloadListingTime;

        public DateTime SyncedDownloadListingTime
        {
            get { return syncedDownloadListingTime; }
            set { SetProperty(ref syncedDownloadListingTime, value); }
        }

        #endregion Properties

        #region Household Listing Sync

        private string syncUpListingText;

        public string SyncUpListingText
        {
            get { return syncUpListingText; }
            set { SetProperty(ref syncUpListingText, value); }
        }

        private string syncDownListingText;

        public string SyncDownListingText
        {
            get { return syncDownListingText; }
            set { SetProperty(ref syncDownListingText, value); }
        }

        private bool isBusyListing = false;

        public bool IsBusyListing
        {
            get
            {
                return isBusyListing;
            }
            set
            {
                SetProperty(ref isBusyListing, value);
            }
        }

        private string logListing;

        public string LogListing
        {
            get { return logListing; }
            set { SetProperty(ref logListing, value); }
        }

        private string messageListing;

        public string MessageListing
        {
            get { return messageListing; }
            set { SetProperty(ref messageListing, value); }
        }

        private ICommand syncUpListingCommand;
        public ICommand SyncUpListingCommand => syncUpListingCommand ?? (syncUpListingCommand = new Command(async () => await ExecuteSyncUpListingAsync()));

        private async Task ExecuteSyncUpListingAsync()
        {
            if (IsBusyListing)
                return;
            MessageListing = "Establishing Secure Connection... ...";
            SyncUpListingText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your Internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                IsBusyListing = true;
                var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);
                MessageListing = "Uploading Sync Data  ...";
                SyncUpListingText = "Uploading Data";
                await Task.Delay(1000);
                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var localdata = App.Database.GetTable("Listing");
                if (localdata.Any())
                {
                    var toSync = localdata.Count();
                    var syncedRecords = 0;
                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    foreach (var item in localdata)
                    {
                        var Listing = (Listing)item;
                        Listing.SyncEnumeratorId = Settings.Current.EnumeratorId;
                        MessageListing = $"Processing Record Ref.#{Listing.LocalId} ... ";
                        await Task.Delay(1000);
                        var feedback = await client.PostListing(Listing, model);

                        if (feedback.StatusId == null)
                        {
                            Settings.Current.FirstRun = true;
                            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                new MessagingServiceAlert
                                {
                                    Title = "Sync Failed",
                                    Message = "Kindly Try Again!",
                                    Cancel = "OK"
                                });
                            await Navigation.PushModalAsync(new LoginPage());
                            return;
                        }
                        else
                        {
                            if (feedback.StatusId == 0)
                            {
                                syncedRecords += 1;
                                LocalListing = LocalListing - 1;
                                SyncedUploadListingTime = DateTime.Now;

                                if (deleteonSync == "1")
                                {
                                    App.Database.Delete<Listing>(Listing.LocalId);
                                }
                                else if (deleteonSync == "0")
                                {
                                    if (feedback.Id != null)
                                    {
                                        Listing.Id = feedback.Id.Value;
                                        App.Database.Update<Listing>(Listing);
                                    }
                                }
                            }
                            else if (feedback.StatusId == -1)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Sync!",
                                    Message = $"An Error Occurred.  Reason : {feedback.Description} \n Upload has been Stopped." + $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is Uploaded.",
                                    Cancel = "OK"
                                });
                                return;
                            }
                        }
                    }

                    var response = "";
                    if (toSync == syncedRecords)
                        response = $"Upload was Successful.  {syncedRecords} of {toSync} Records  were Uploaded";
                    if (toSync != syncedRecords)
                        response = $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is Uploaded.";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync!",
                        Message = response,
                        Cancel = "OK"
                    });
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Upload not Initiated",
                        Message = "No Data that is ready to Upload.",
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Upload",
                    Message = $"The Upload failed. \n {ex.Message}",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageListing = string.Empty;
                SyncUpListingText = "Upload Recorded Data";
                IsBusy = false;
                IsBusyListing = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownListingCommand;

        public ICommand SyncDownListingCommand =>
            syncDownListingCommand ?? (syncDownListingCommand = new Command(async () => await ExecuteSyncDownListingAsync()));

        private async Task ExecuteSyncDownListingAsync()
        {
            if (IsBusy)
                return;
            MessageListing = "Establishing Secure Connection... ...";
            SyncDownListingText = "Downloading ...";
            try
            {
                IsBusyListing = true;
                IsBusy = true;
                ListingOptionsResponse enumeratorResult = null;
                try
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }
                    var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);
                    MessageListing = "Pulling Sync Data... ...";
                    SyncDownListingText = "Downloading Data";
                    enumeratorResult = await client.GetListingSettings("", "", enumerator.Id.ToString());
                    
                    if (enumeratorResult != null)
                    {
                        foreach (var item in enumeratorResult.Programmes)
                            App.Database.AddOrUpdate(item);

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;
                        MessageListing = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully.",
                            Cancel = "OK"
                        });
                        SyncedDownloadListingTime = DateTime.Now;
                    }
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageListing = string.Empty;
                SyncDownListingText = "Download From the Server";
                IsBusyListing = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Household Listing Sync

        #region Community Validation Registration Sync

        private string syncUpComValText;

        public string SyncUpComValText
        {
            get { return syncUpComValText; }
            set { SetProperty(ref syncUpComValText, value); }
        }

        private string syncDownComValText;

        public string SyncDownComValText
        {
            get { return syncDownComValText; }
            set { SetProperty(ref syncDownComValText, value); }
        }

        private bool isBusyComVal = false;

        public bool IsBusyComVal
        {
            get
            {
                return isBusyComVal;
            }
            set
            {
                SetProperty(ref isBusyComVal, value);
            }
        }

        private string messageComVal;

        public string MessageComVal
        {
            get { return messageComVal; }
            set { SetProperty(ref messageComVal, value); }
        }

        private ICommand syncUpComValCommand;
        public ICommand SyncUpComValCommand => syncUpComValCommand ?? (syncUpComValCommand = new Command(async () => await ExecuteSyncUpComValAsync()));

        private async Task ExecuteSyncUpComValAsync()
        {
            if (IsBusyComVal)
                return;
            MessageComVal = "Establishing Secure Connection... ...";
            SyncUpComValText = "Working.";

            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone Offline. \n" +
                                  "Please check your Internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                IsBusyComVal = true;
                var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);
                MessageComVal = "Uploading Sync Data  ...";
                SyncUpComValText = "Uploading Data";

                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };
                var localdata = App.Database.GetTable("ComValListingPlanHH");
                if (localdata.Any())
                {
                    var toSync = localdata.Count();
                    var syncedRecords = 0;

                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    foreach (var item in localdata)
                    {
                        var ComVal = (ComValListingPlanHH)item;
                        if (ComVal.ComValDate != null)
                        {
                            ComVal.SyncEnumeratorId = Settings.Current.EnumeratorId;
                            MessageComVal = $"Processing Record Ref.#{ComVal.Id} ... ";
                            var feedback = await client.PostListingCommVal(ComVal, model);
                            if (feedback.StatusId == null)
                            {
                                Settings.Current.FirstRun = true;
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                    new MessagingServiceAlert
                                    {
                                        Title = "Sync Failed",
                                        Message = "Most probably, your session timed out ",
                                        Cancel = "OK"
                                    });
                                return;
                            }
                            else
                            {
                                if (feedback.StatusId == 0)
                                {
                                    syncedRecords++;
                                    if (deleteonSync == "1")
                                    {
                                        App.Database.Delete<ComValListingPlanHH>(ComVal.Id);
                                    }
                                    else if (deleteonSync == "0")
                                    {
                                        if (feedback.Id != null)
                                        {
                                            ComVal.Id = feedback.Id.Value;
                                            App.Database.Update(ComVal);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var response = "";
                    if (toSync == syncedRecords)
                        response = $"Upload was Successful.  {syncedRecords} of {toSync} Records  were Uploaded";
                    if (toSync != syncedRecords)
                        response = $"Partial Upload Complete.  {syncedRecords} of {toSync} Records  were Uploaded. Kindly Ensure that All Data is is Processed before Uploading.";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync!",
                            Message = response,
                            Cancel = "OK"
                        });

                    Toast.SendToast(response);
                }
                else
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Check Logs for More Information.",
                            Cancel = "OK"
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageComVal = string.Empty;
                SyncUpComValText = "Upload Recorded Data";
                IsBusy = false;
                IsBusyComVal = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownComValCommand;

        public ICommand SyncDownComValCommand =>
            syncDownComValCommand ?? (syncDownComValCommand = new Command(async () => await ExecuteSyncDownComValAsync()));

        private async Task ExecuteSyncDownComValAsync()
        {
            if (IsBusy)
                return;
            MessageComVal = "Establishing Secure Connection... ...";
            SyncDownComValText = "Starting ...";
            try
            {
                IsBusyComVal = true;
                IsBusy = true;
                EnumeratorCVResponse enumeratorResult = null;
                try
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }

                    var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);
                    MessageComVal = "Pulling Sync Data... ...";
                    SyncDownComValText = "Downloading Data";

                    enumeratorResult = await client.ValidationListByEnumerator("", "", enumerator.Id.ToString());
                    IsBusyComVal = false;
                    //   await Task.Delay(500);
                    IsBusyComVal = true;
                    if (enumeratorResult != null)
                    {
                        MessageComVal = "Updating Local Database ... ...";

                        foreach (var item in enumeratorResult.ComValListingPlanHHs)
                        {
                            item.DownloadDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                            App.Database.AddOrUpdate(item);
                        }

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;

                        MessageComVal = "Complete ... ...";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully",
                            Cancel = "OK"
                        });
                    }
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Complete",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                // Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageComVal = string.Empty;
                SyncDownComValText = "Download Recorded Data";
                IsBusyComVal = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Community Validation Registration Sync

        #region Targeting Sync

        private string syncUpRegistrationText;

        public string SyncUpRegistrationText
        {
            get { return syncUpRegistrationText; }
            set { SetProperty(ref syncUpRegistrationText, value); }
        }

        private string syncDownRegistrationText;

        public string SyncDownRegistrationText
        {
            get { return syncDownRegistrationText; }
            set { SetProperty(ref syncDownRegistrationText, value); }
        }

        private string logRegistration;

        public string LogRegistration
        {
            get { return logRegistration; }
            set { SetProperty(ref logRegistration, value); }
        }

       
        private string messageRegistration;

        public string MessageRegistration
        {
            get { return messageRegistration; }
            set { SetProperty(ref messageRegistration, value); }
        }

        private ICommand syncUpRegistrationCommand;
        public ICommand SyncUpRegistrationCommand => syncUpRegistrationCommand ?? (syncUpRegistrationCommand = new Command(async () => await ExecuteSyncUpRegistrationAsync()));

        private async Task ExecuteSyncUpRegistrationAsync()
        {
            if (IsBusy)
                return;
            MessageRegistration = "Establishing Secure Connection... ...";
            SyncUpRegistrationText = "Working.";

            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : {MessageRegistration} \n";
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Failed",
                        Message = "Uh Oh, It looks like you have gone offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }
                IsBusy = true;
                 var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);

                MessageRegistration = "Uploading Sync Data  ...";

                SyncUpRegistrationText = "Uploading Data";
                LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : {MessageRegistration} \n";

                var model = new LocalDeviceInfo()
                {
                    Version = CrossDeviceInfo.Current.Version,
                    AppBuild = CrossDeviceInfo.Current.AppBuild,
                    AppVersion = CrossDeviceInfo.Current.AppVersion,
                    DeviceName = CrossDeviceInfo.Current.DeviceName,
                    DeviceId = CrossDeviceInfo.Current.Id,
                    Idiom = CrossDeviceInfo.Current.Idiom,
                    IsDevice = CrossDeviceInfo.Current.IsDevice,
                    DeviceManufacturer = CrossDeviceInfo.Current.Manufacturer,
                    DeviceModel = CrossDeviceInfo.Current.Model,
                    Platform = CrossDeviceInfo.Current.Platform,
                    VersionNumber = CrossDeviceInfo.Current.VersionNumber.ToString()
                };

                var localdata = App.Database.GetTable("TargetingHh");

                if (localdata.Any())
                {
                    LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Will be Syncing {localdata.Count} Households \n";
                    var deleteonSync = ((SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "Code", "DELETEONSYNC")).Description;
                    LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Delete Data on sync? {(deleteonSync == "1" ? "Yes" : "Nope")}  \n";

                    var limitedSync = App.Database.SystemCodeDetailGetByCode("STATUS", "EDITING").Id;
                    foreach (var item in localdata)
                    {
                        var Tar = (TargetingHh)item;
                        var hhm = new List<TargetingHhMember>();
                        var members = App.Database.GetTableRows("TargetingHhMember", "TargetingHhId", Tar.Id.ToString());
                         
                        if (Tar.StatusId == limitedSync)
                        {
                            Tar.SyncEnumeratorId = Settings.Current.EnumeratorId;
                            //Tar.dat = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);

                            MessageRegistration = $"Processing Record Ref.#{Tar.Id} ... ";
                            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : {MessageRegistration} \n";
                            await Task.Delay(1000);
                            var feedback = await client.PostTargeting(Tar, model);

                            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Record #{Tar.Id} => [{feedback.StatusId}] {feedback.Description} \n";

                            if (feedback.StatusId == null)
                            {
                                Settings.Current.FirstRun = true;
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                                    new MessagingServiceAlert
                                    {
                                        Title = "Sync Failed",
                                        Message = "Most probably, your session timed out ",
                                        Cancel = "OK"
                                    });
                                return;
                            }
                            else
                            {
                                if (feedback.StatusId == 0)
                                {
                                    if (deleteonSync == "1")
                                    {
                                        App.Database.Delete<CvRegistration>(Tar.Id);
                                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Deleted  Record  #{Tar.Id} from Local DB after successful Sync! \n";
                                    }
                                    else if (deleteonSync == "0")
                                    {
                                        if (feedback.Id != null)
                                        {
                                            Tar.Id = feedback.Id.Value;
                                            App.Database.Update(Tar);
                                            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Updated  Record  #{Tar.Id} on Local DB after successful Sync! \n";
                                        }
                                        else
                                        {
                                            LogRegistration +=
                                                $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : We could not Update the Local Record. Your Data may be duplicated on sync. #{Tar.Id} on Local DB after successful Sync! \n";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Household {Tar.Id} not Ready for Sync.  \n";
                        }
                    }

                    var response = "Sync was Successful. Check the Log for more information";
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync!",
                            Message = response,
                            Cancel = "OK"
                        });
                }
                else
                {
                   // LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : No Cv Registered Households to Sync.\n \n";

                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message,
                        new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Check Logs for More Information.",
                            Cancel = "OK"
                        });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.InnerException?.Message);
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRegistration = string.Empty;
                SyncUpRegistrationText = "Upload Recorded Data";
                LogRegistration += $" {DateFormatter.ToSQLiteDateTimeString(DateTime.Now)} : Completed \n \n";
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        private ICommand syncDownRegistrationCommand;

        public ICommand SyncDownRegistrationCommand =>   syncDownRegistrationCommand ?? (syncDownRegistrationCommand = new Command(async () => await ExecuteSyncDownRegistrationAsync()));

        private async Task ExecuteSyncDownRegistrationAsync()
        {
            if (IsBusy)
                return;
            MessageRegistration = "Establishing Secure Connection... ...";
            SyncDownRegistrationText = "Starting ...";
            LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}: {MessageRegistration} \n";
            try
            {
                //IsBusyRegistration = true;
                IsBusy = true;
                EnumeratorCVResponse enumeratorResult = null;
                try
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Failed",
                            Message = "Uh Oh, It looks like you have gone offline. Please check your internet connection to get the latest data and enable syncing data.",
                            Cancel = "OK"
                        });
                        return;
                    }

                    var enumerator = App.Database.EnumeratorGetById(Settings.EnumeratorId);
                    MessageRegistration = "Pulling Sync Data... ...";
                    SyncDownRegistrationText = "Downloading Data";
                    
                    LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:{MessageRegistration} \n";
                    //enumeratorResult = await client.GetListingSettings("", "", enumerator.Id.ToString());
                    enumeratorResult = await client.GetRegistrationData("", "", enumerator.Id.ToString());
                    //IsBusyRegistration = false;
                    //   await Task.Delay(500);
                    //IsBusyRegistration = true;
                    if (enumeratorResult != null)
                    {
                        MessageRegistration = "Updating Local Database ... ...";
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:{MessageRegistration} \n";
                        //now Load into the Tables
                        App.Database.AddOrUpdate(enumeratorResult.Enumerator);
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}: Updated Enumerator Details Locally \n";

                        foreach (var item in enumeratorResult.SystemCodes)
                            App.Database.AddOrUpdate(item);
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.SystemCodes.Count} Codes \n";

                        foreach (var item in enumeratorResult.SystemCodeDetails)
                            App.Database.AddOrUpdate(item);
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.SystemCodeDetails.Count} Code Options \n";

                        foreach (var item in enumeratorResult.Locations)
                            App.Database.AddOrUpdate(item);
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.Locations.Count} Locations \n";

                        foreach (var item in enumeratorResult.SubLocations)
                            App.Database.AddOrUpdate(item);
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.SubLocations.Count} Sub Locations \n";

                        foreach (var item in enumeratorResult.Programmes)
                            App.Database.AddOrUpdate(item);

                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.Programmes.Count} Programmes \n";

                        var enumeratorId = Settings.Current.EnumeratorId;

                        var hhead = (SystemCodeDetail)App.Database.SystemCodeDetailGetByCode("Relationship", "Head");
                        var cGiver = (SystemCodeDetail)App.Database.SystemCodeDetailGetByCode("Relationship", "DK");

                        foreach (var item in enumeratorResult.ComValListingPlanHHs)
                        {
                            var targetingHouseHold = new  TargetingHh
                            {
                                ProgrammeId = item.ProgrammeId,
                                Village = item.Village,
                                EndTime = item.EndTime,
                                EnumeratorId = enumeratorId,
                                HouseholdMembers = item.HouseholdMembers,
                                Years = item.Years,
                                Months = item.Months,
                                Id = item.Id,
                                Latitude = item.Latitude,
                                Longitude = item.Longitude,
                                LocationId = item.LocationId,
                                SubLocationId = item.SubLocationId,
                                NearestReligiousBuilding = item.NearestReligiousBuilding,
                                NearestSchool = item.NearestSchool,
                                PhysicalAddress = item.PhysicalAddress,
                                UniqueId = item.UniqueId,
                                StatusId = item.StatusId,
                                RegistrationDate = item.RegistrationDate,
                                TargetPlanId = item.TargetPlanId,
                                HHdFullName = item.HHdFullName,
                                CgFullName = item.CgFullName
                            };

                            App.Database.AddOrUpdate(targetingHouseHold);
                            var hh = new TargetingHhMember
                            {
                                FirstName = item.HHdFirstName,
                                MiddleName = item.HHdMiddleName,
                                Surname = item.HHdSurname,
                                DateOfBirth = item.HHdDoB,
                                HasIdNumber = true,
                                IdentificationNumber = item.HHdNationalIdNo,
                                MemberSexId = item.HHdSexId,
                                TargetingHhId = item.Id,
                                MemberRelationshipId = hhead.Id
                            };
                            var cg = new TargetingHhMember
                            {
                                FirstName = item.CgFirstName,
                                MiddleName = item.CgMiddleName,
                                Surname = item.CgSurname,
                                DateOfBirth = item.CgDoB,
                                HasIdNumber = true,
                                IdentificationNumber = item.CgNationalIdNo,
                                MemberSexId = (item.CgSexId).ToString(),
                                TargetingHhId = item.Id,
                                MemberRelationshipId = cGiver.Id
                            };

                            App.Database.Create(hh);
                            App.Database.Create(cg);
                        }

                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}:Updated {enumeratorResult.ComValListingPlanHHs.Count} Taristration Plans \n";

                        Settings.Current.LastSyncDown = DateTime.UtcNow;
                        Settings.Current.HasSyncedDataDownwards = true;

                        MessageRegistration = "Complete ... ...";
                        LogRegistration += $"{DateFormatter.ToSQLiteDateTimeString(DateTime.Now)}: Completed \n \n";
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Sync Complete",
                            Message = "Sync has been Completed Successfully. Kindly Restart the Application.",
                            Cancel = "OK"
                        });
                    }
                    
                }
                catch (Exception ex)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Sync Complete",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                // Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sync",
                    Message = "The Sync failed. Please try again later.",
                    Cancel = "OK"
                });
            }
            finally
            {
                MessageRegistration = string.Empty;
                SyncDownRegistrationText = "Download Recorded Data";
                //IsBusyRegistration = false;
                IsBusy = false;
                await Task.FromResult(0);
            }
        }

        #endregion Targeting Sync

    }
}