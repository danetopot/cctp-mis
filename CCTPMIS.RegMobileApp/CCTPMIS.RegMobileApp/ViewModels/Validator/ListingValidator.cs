﻿using System;
using System.Collections.Generic;
using System.Text;
using CCTPMIS.RegMobileApp.Models;
using FluentValidation;

namespace CCTPMIS.RegMobileApp.ViewModels.Validator
{
    public class ListingValidator : AbstractValidator<Listing>
    {
        public ListingValidator()
        {
            RuleFor(x => x.ProgrammeId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Programme is Required");
            RuleFor(x => x.LocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Location is Required");
            RuleFor(x => x.SubLocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Sub Location is Required");

            RuleFor(x => x.Village).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Village Name");
            RuleFor(x => x.Years).NotNull().GreaterThan(-1).LessThanOrEqualTo(100).WithMessage("Select the Years");
            RuleFor(x => x.Months).NotNull().GreaterThan(-1).LessThanOrEqualTo(12).WithMessage("Select the Months");
            RuleFor(x => x.NearestReligiousBuilding).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest Religious Building");
            RuleFor(x => x.NearestSchool).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest School"); ;
            RuleFor(x => x.PhysicalAddress).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Physical Address"); ;
            RuleFor(x => x.HouseholdMembers).NotEmpty().GreaterThan(0).WithMessage("Check Household Members");
            RuleFor(x => x.CgFirstName).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgSurname).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgPhoneNumber).NotEmpty().Matches(@"^[0-9+-- ]{1,15}$").WithMessage("Check Caregiver Phone Number").MaximumLength(15);
            RuleFor(x => x.CgSexId).NotNull().GreaterThan(0).WithMessage("Caregiver Sex is required");
            RuleFor(x => x.CgMiddleName).Length(0, 20).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.CgMiddleName == null).WithMessage("Check Caregiver Middle Name");

            RuleFor(x => x.BeneFirstName).MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneFirstName == null).WithMessage("Check Beneficiary First Name");
            RuleFor(x => x.BeneSurname).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneSurname == null).WithMessage("Check Beneficiary Surname");
            RuleFor(x => x.BenePhoneNumber).Matches(@"^[0-9+-- ]{1,15}$").WithMessage("Check Beneficiary Phone Number").Unless(x => x.BenePhoneNumber == null).MaximumLength(15);
            RuleFor(x => x.BeneSexId).GreaterThan(0).WithMessage("Beneficiary Sex is required").Unless(x => x.BeneSexId == null);
            RuleFor(x => x.BeneMiddleName).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Beneficiary Middle Name").Unless(x => x.BeneMiddleName == null);

            RuleFor(x => x.BeneNationalIdNo).Matches(@"^[0-9]{1,8}$").WithMessage("Check Beneficiary National Id No.").Unless(x => x.BeneNationalIdNo == null);

            RuleFor(x => x.CgNationalIdNo).NotEmpty().Matches(@"^[0-9]{1,8}$").WithMessage("Check Caregiver National Id No.");
        }
    }

    public class HtmHouseholdValidator : AbstractValidator<TargetingHh>
    {
        public HtmHouseholdValidator()
        {
            RuleFor(x => x.ProgrammeId).NotEmpty().GreaterThan(0).WithMessage("Programme is Required");
            RuleFor(x => x.LocationId).NotEmpty().GreaterThan(0).WithMessage("Location is Required");
            RuleFor(x => x.SubLocationId).NotEmpty().GreaterThan(0).WithMessage("Sub Location is Required");
            RuleFor(x => x.Village).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Village Name");
            RuleFor(x => x.Years).NotNull().GreaterThan(-1).LessThanOrEqualTo(100).WithMessage("Select the Years");
            RuleFor(x => x.Months).NotNull().GreaterThan(-1).LessThanOrEqualTo(12).WithMessage("Select the Months");

            RuleFor(x => x.NearestReligiousBuilding).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest Religious Building");
            RuleFor(x => x.NearestSchool).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest School"); ;
            RuleFor(x => x.PhysicalAddress).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Physical Address"); ;
            RuleFor(x => x.HouseholdMembers).NotEmpty().GreaterThan(0).WithMessage("Check Household Members");
            RuleFor(x => x.HabitableRooms).NotNull().GreaterThan(0).WithMessage("Check Habitable Rooms");

            RuleFor(x => x.TenureStatusId).NotNull().GreaterThan(0).WithMessage("Check Tenure Status");
            RuleFor(x => x.IsOwned).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Tenure Status");

            RuleFor(x => x.RoofConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.FloorConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.WallConstructionMaterialId).GreaterThan(0).NotNull();
            RuleFor(x => x.DwellingUnitRiskId).GreaterThan(0).NotNull();
            RuleFor(x => x.WaterSourceId).GreaterThan(0).NotNull();
            RuleFor(x => x.WasteDisposalModeId).GreaterThan(0).NotNull();
            RuleFor(x => x.CookingFuelTypeId).GreaterThan(0).NotNull();
            RuleFor(x => x.LightingFuelTypeId).GreaterThan(0).NotNull();

            RuleFor(x => x.IsTelevision).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Television");
            RuleFor(x => x.IsMotorcycle).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Motor cycle");
            RuleFor(x => x.IsTukTuk).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check TukTuk");
            RuleFor(x => x.IsRefrigerator).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Refrigerator");
            RuleFor(x => x.IsCar).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Car");
            RuleFor(x => x.IsMobilePhone).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check  Mobile Phone");
            RuleFor(x => x.IsBicycle).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Bicycle");

            RuleFor(x => x.ExoticCattle).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Exotic Cattle");
            RuleFor(x => x.IndigenousCattle).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Indigenous Cattle");
            RuleFor(x => x.Sheep).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Sheep");
            RuleFor(x => x.Goats).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Goats");
            RuleFor(x => x.Camels).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Camels");
            RuleFor(x => x.Donkeys).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check  Donkeys");
            RuleFor(x => x.Pigs).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Pigs");
            RuleFor(x => x.Chicken).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Chicken");

            RuleFor(x => x.LiveBirths).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Live births");
            RuleFor(x => x.Deaths).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check Deaths");
            RuleFor(x => x.HouseHoldConditionId).GreaterThan(0).NotNull();

            RuleFor(x => x.IsSkippedMeal).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Is Skipped Meal");
            RuleFor(x => x.NsnpProgrammesId).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check  N.S.N.P. Programmes");
            RuleFor(x => x.IsReceivingSocial).NotNull().GreaterThanOrEqualTo(0).LessThanOrEqualTo(1).WithMessage("Check Is Receiving Social");
            RuleFor(x => x.Programmes).NotEmpty().WithMessage("Check Social Programmes External Support Programmes List").When(x => x.IsReceivingSocial > 0);
            RuleFor(x => x.InKindBenefit).NotNull().WithMessage("Check In Kind Benefit").When(x=>x.BenefitTypeId==2 || x.BenefitTypeId==3);
            RuleFor(x => x.LastReceiptAmount).NotNull().GreaterThanOrEqualTo(0).When(x => x.BenefitTypeId==1).WithMessage("Check Last Receipt Amount");
        }
    }
}