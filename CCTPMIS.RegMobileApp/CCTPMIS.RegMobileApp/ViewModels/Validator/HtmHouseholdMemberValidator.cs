﻿using CCTPMIS.RegMobileApp.Models;
using FluentValidation;

namespace CCTPMIS.RegMobileApp.ViewModels.Validator
{
    public class HtmHouseholdMemberValidator : AbstractValidator<TargetingHhMember>
    {

        public HtmHouseholdMemberValidator()
        {
            RuleFor(x => x.FirstName).MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check  First Name");
            RuleFor(x => x.Surname).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check  Surname");
            RuleFor(x => x.MiddleName).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check  Middle Name").Unless(x => x.MiddleName == null);
            RuleFor(x => x.PhoneNumber).Matches(@"^[0-9+-- ]{1,15}$").WithMessage("Check 3.01(b)")
                .MaximumLength(15).WithMessage("Check 3.01(b) is too long").Unless(x => x.PhoneNumber == null);

            RuleFor(x => x.HasIdNumberId).NotNull().WithMessage("3.02 (b) Response is required.").Unless(x => x.IdentificationNumber == null);
            RuleFor(x => x.IdentificationNumber).Matches(@"^[0-9]{1,8}$").WithMessage("3.02 (b) Response is required.").Unless(x => x.IdentificationNumber == null);

            RuleFor(x => x.RelationshipId).GreaterThan(0).WithMessage("3.03 Response is required").Unless(x => x.SexId == null);
            RuleFor(x => x.SexId).GreaterThan(0).WithMessage("3.04 Response is required").Unless(x => x.SexId == null);
             
            RuleFor(x => x.DateOfBirth).NotEmpty().WithMessage("Check 3.05");

            RuleFor(x => x.MaritalStatusId).NotNull()
                .GreaterThanOrEqualTo(0).WithMessage("Check 3.06");

            RuleFor(x => x.SpouseInHouseholdId)
                .GreaterThanOrEqualTo(0).WithMessage("Check 3.07")
                .When(x => x.MaritalStatus.Code=="2" || x.MaritalStatus.Code == "3");

            // hide this if answer is no
            RuleFor(x => x.SpouseLineNumber).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.07 (b)")
                .Unless(x => x.SpouseInHouseholdId == null);


            RuleFor(x => x.FatherAliveStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.08");
            RuleFor(x => x.MotherAliveStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.09");

            RuleFor(x => x.ChronicIllnessStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.10 Response");
            
            RuleFor(x => x.DisabilityTypeId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.11 Response");


            RuleFor(x => x.DisabilityCareStatusId).NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Check 3.12 Response");

            RuleFor(x => x.CareGiverId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.13 Response")
                .Unless(x => x.DisabilityCareStatusId == null);
            RuleFor(x => x.LearningStatusId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.14 Response")
                .Unless(x => x.DisabilityCareStatusId == null);
            RuleFor(x => x.EducationLevelId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.15 Response");
            RuleFor(x => x.WorkTypeId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.16 Response");
            RuleFor(x => x.FormalJobNgoId).NotNull().GreaterThanOrEqualTo(0).WithMessage("Check 3.17 Response");


        }
    }
}