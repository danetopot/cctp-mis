﻿using CCTPMIS.RegMobileApp.Models;
using FluentValidation;

namespace CCTPMIS.RegMobileApp.ViewModels.Validator
{
    public class ComValListingValidator : AbstractValidator<ComValListingPlanHH>
    {
        public ComValListingValidator()
        {
            RuleFor(x => x.ProgrammeId).NotNull().WithMessage("Programme is Required");
            RuleFor(x => x.LocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Location is Required");
            RuleFor(x => x.SubLocationId).NotEmpty().GreaterThanOrEqualTo(0).WithMessage("Sub Location is Required");
            RuleFor(x => x.Village).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Village Name");
            RuleFor(x => x.Years).NotNull().GreaterThan(-1).LessThanOrEqualTo(100).WithMessage("Select the Years");
            RuleFor(x => x.Months).NotNull().GreaterThan(-1).LessThanOrEqualTo(12).WithMessage("Select the Months");
            RuleFor(x => x.NearestReligiousBuilding).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest Religious Building");
            RuleFor(x => x.NearestSchool).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Nearest School"); ;
            RuleFor(x => x.PhysicalAddress).NotEmpty().Matches(@"^[A-Za-z0-9_'-- ]{1,100}$").WithMessage("Check Physical Address"); ;
            RuleFor(x => x.HouseholdMembers).NotEmpty().GreaterThan(0).WithMessage("Check Household Members");
            RuleFor(x => x.CgFirstName).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgSurname).NotEmpty().MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Caregiver First Name");
            RuleFor(x => x.CgPhoneNumber).NotEmpty().Matches(@"^[0-9+-- ]{1,15}$").WithMessage("Check Caregiver Phone Number").MaximumLength(15);
            RuleFor(x => x.CgSexId).NotNull().GreaterThan(0).WithMessage("Caregiver Sex is required");
            RuleFor(x => x.CgMiddleName).Length(0, 20).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.CgMiddleName == null).WithMessage("Check Caregiver Middle Name");
            RuleFor(x => x.BeneFirstName).MaximumLength(20).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneFirstName == null).WithMessage("Check Beneficiary First Name");
            RuleFor(x => x.BeneSurname).Matches(@"^[A-Za-z_'-- ]{1,20}$").Unless(x => x.BeneSurname == null).WithMessage("Check Beneficiary Surname");
            RuleFor(x => x.BenePhoneNumber).Matches(@"^[0-9+-- ]{1,15}$").WithMessage("Check Beneficiary Phone Number").Unless(x => x.BenePhoneNumber == null).MaximumLength(15);
            RuleFor(x => x.BeneSexId).GreaterThan(0).WithMessage("Beneficiary Sex is required").Unless(x => x.BeneSexId == null);
            RuleFor(x => x.BeneMiddleName).Matches(@"^[A-Za-z_'-- ]{1,20}$").WithMessage("Check Beneficiary Middle Name").Unless(x => x.BeneMiddleName == null);

            RuleFor(x => x.BeneNationalIdNo).Matches(@"^[0-9]{1,8}$").WithMessage("Check Beneficiary National Id No.").Unless(x => x.BeneNationalIdNo == null);
            RuleFor(x => x.CgNationalIdNo).NotEmpty().Matches(@"^[0-9]{1,8}$").WithMessage("Check Caregiver National Id No.");
        }
    }
}