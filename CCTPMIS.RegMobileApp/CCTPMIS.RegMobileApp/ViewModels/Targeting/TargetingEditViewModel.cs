﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views.Registration;
using CCTPMIS.RegMobileApp.Views.Targeting;
using FormsToolkit;
using MvvmHelpers;
using Plugin.Geolocator;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class TargetingEditViewModel : ViewModelBase
    {
        public TargetingEditViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("TargetingHh", "Id", id.ToString());
            HouseHold = (TargetingHh)data;
            HouseHold.Programme = (Programme)App.Database.GetTableRow("Programme", "id", HouseHold.ProgrammeId.ToString());
            HouseHold.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", HouseHold.SubLocationId.ToString());
            HouseHold.Location = (Location)App.Database.GetTableRow("Location", "id", HouseHold.LocationId.ToString());
            HouseholdMembers.AddRange(GetHouseholdMembers(id));
            Navigation = navigation;
            Id = id;

            LoadedTenureStatuses.AddRange(App.Database.SystemCodeDetailsGetByCode("Tenure Status"));
            LoadedRoofConstructionMaterials.AddRange(App.Database.SystemCodeDetailsGetByCode("Roof Material"));
            LoadedWallConstructionMaterials.AddRange(App.Database.SystemCodeDetailsGetByCode("Wall Material"));
            LoadedFloorConstructionMaterials.AddRange(App.Database.SystemCodeDetailsGetByCode("Floor Material"));
            LoadedDwellingRisks.AddRange(App.Database.SystemCodeDetailsGetByCode("Dwelling Unit Risk"));
            LoadedWaterSources.AddRange(App.Database.SystemCodeDetailsGetByCode("Water Source"));
            LoadedWasteDisposals.AddRange(App.Database.SystemCodeDetailsGetByCode("Toilet Type"));
            LoadedCookingFuels.AddRange(App.Database.SystemCodeDetailsGetByCode("Cooking Fuel"));
            LoadedLightingFuels.AddRange(App.Database.SystemCodeDetailsGetByCode("Lighting Source"));
            LoadedHouseholdConditions.AddRange(App.Database.SystemCodeDetailsGetByCode("Household Conditions"));
            LoadedHouseholdOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("Household Option"));
            LoadedNsnpBenefits.AddRange(App.Database.SystemCodeDetailsGetByCode("Other SP Programme"));
            LoadedBenefitTypes.AddRange(App.Database.SystemCodeDetailsGetByCode("SP Benefit Type"));
            LoadedInterviewStatus.AddRange(App.Database.SystemCodeDetailsGetByCode("Interview Result"));

            SelectedBenefitType = LoadedBenefitTypes.FirstOrDefault(x => x.Id == HouseHold.BenefitTypeId) ?? null;
            SelectedTenureStatus = LoadedTenureStatuses.FirstOrDefault(x => x.Id == HouseHold.TenureStatusId) ?? null;
            SelectedRoofConstructionMaterial = LoadedRoofConstructionMaterials.FirstOrDefault(x => x.Id == HouseHold.RoofConstructionMaterialId) ?? null;
            SelectedFloorConstructionMaterial = LoadedFloorConstructionMaterials.FirstOrDefault(x => x.Id == HouseHold.FloorConstructionMaterialId) ?? null;
            SelectedWallConstructionMaterial = LoadedWallConstructionMaterials.FirstOrDefault(x => x.Id == HouseHold.WallConstructionMaterialId) ?? null;
            SelectedDwellingRisk = LoadedDwellingRisks.FirstOrDefault(x => x.Id == HouseHold.DwellingUnitRiskId) ?? null;
            SelectedWaterSource = LoadedWaterSources.FirstOrDefault(x => x.Id == HouseHold.WaterSourceId) ?? null;
            SelectedWasteDisposal = LoadedWasteDisposals.FirstOrDefault(x => x.Id == HouseHold.WasteDisposalModeId) ?? null;
            SelectedCookingFuel = LoadedCookingFuels.FirstOrDefault(x => x.Id == HouseHold.CookingFuelTypeId) ?? null;
            SelectedLightingFuel = LoadedLightingFuels.FirstOrDefault(x => x.Id == HouseHold.LightingFuelTypeId) ?? null;
            SelectedHouseholdConditions = LoadedHouseholdConditions.FirstOrDefault(x => x.Id == HouseHold.HouseHoldConditionId) ?? null;
            SelectedHouseholdOptions = LoadedHouseholdOptions.FirstOrDefault(x => x.Id == HouseHold.OtherProgrammesId) ?? null;
            SelectedNsnpBenefit = LoadedNsnpBenefits.FirstOrDefault(x => x.Id == HouseHold.OtherProgrammesId) ?? null;
            SelectedBenefitType = LoadedBenefitTypes.FirstOrDefault(x => x.Id == HouseHold.BenefitTypeId) ?? null;
            SelectedInterviewStatus = LoadedBenefitTypes.FirstOrDefault(x => x.Id == HouseHold.BenefitTypeId) ?? null;





        }

        public ObservableRangeCollection<SystemCodeDetail> InterviewStatus = new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> BenefitTypes = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> CookingFuels = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> DwellingRisks = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> FloorConstructionMaterials = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> HouseholdConditions = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> LightingFuels = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> NsnpBenefits = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> RoofConstructionMaterials = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> TenureStatuses = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> WallConstructionMaterials = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> WasteDisposals = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> WaterSources = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> HouseholdOptions = new ObservableRangeCollection<SystemCodeDetail>();



        //private Programme _selectedProgramme;
        //private Location _selectedLocation;
        //private SubLocation _selectedSubLocation;
        private SystemCodeDetail _selectedBenefitType;

        private SystemCodeDetail _selectedCookingFuel;

        private SystemCodeDetail _selectedDwellingRisk;

        private SystemCodeDetail _selectedFloorConstructionMaterial;

        private SystemCodeDetail _selectedLightingFuel;

        private SystemCodeDetail _selectedNsnpBenefit;

        private SystemCodeDetail _selectedRoofConstructionMaterial;

        private SystemCodeDetail _selectedTenureStatus;

        private SystemCodeDetail _selectedWallConstructionMaterial;

        private SystemCodeDetail _selectedWasteDisposal;

        private SystemCodeDetail _selectedWaterSource;
        private SystemCodeDetail _selectedHouseholdConditions;
        private SystemCodeDetail _selectedHouseholdOptions;

        private SystemCodeDetail _selectedInterviewStatus;

        public SystemCodeDetail SelectedInterviewStatus
        {
            get => _selectedInterviewStatus;
            set
            {
                if (this._selectedInterviewStatus == value) return;
                this._selectedInterviewStatus = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedBenefitType
        {
            get => _selectedBenefitType;
            set
            {
                if (this._selectedBenefitType == value) return;
                this._selectedBenefitType = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedHouseholdOptions
        {
            get => _selectedHouseholdOptions;
            set
            {
                if (this._selectedHouseholdOptions == value) return;
                this._selectedHouseholdOptions = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedHouseholdConditions
        {
            get => _selectedHouseholdConditions;
            set
            {
                if (this._selectedHouseholdConditions == value) return;
                this._selectedHouseholdConditions = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedCookingFuel
        {
            get => _selectedCookingFuel;
            set
            {
                if (this._selectedCookingFuel == value) return;
                this._selectedCookingFuel = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedDwellingRisk
        {
            get => _selectedDwellingRisk;
            set
            {
                if (this._selectedDwellingRisk == value) return;
                this._selectedDwellingRisk = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedFloorConstructionMaterial
        {
            get => _selectedFloorConstructionMaterial;
            set
            {
                if (this._selectedFloorConstructionMaterial == value) return;
                this._selectedFloorConstructionMaterial = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedLightingFuel
        {
            get => _selectedLightingFuel;
            set
            {
                if (this._selectedLightingFuel == value) return;
                this._selectedLightingFuel = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedNsnpBenefit
        {
            get => _selectedNsnpBenefit;
            set
            {
                if (this._selectedNsnpBenefit == value) return;
                this._selectedNsnpBenefit = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedRoofConstructionMaterial
        {
            get => _selectedRoofConstructionMaterial;
            set
            {
                if (this._selectedRoofConstructionMaterial == value) return;
                this._selectedRoofConstructionMaterial = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedTenureStatus
        {
            get => _selectedTenureStatus;
            set
            {
                if (this._selectedTenureStatus == value) return;
                this._selectedTenureStatus = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedWallConstructionMaterial
        {
            get => _selectedWallConstructionMaterial;
            set
            {
                if (this._selectedWallConstructionMaterial == value) return;
                this._selectedWallConstructionMaterial = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedWasteDisposal
        {
            get => _selectedWasteDisposal;
            set
            {
                if (this._selectedWasteDisposal == value) return;
                this._selectedWasteDisposal = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedWaterSource
        {
            get => _selectedWaterSource;
            set
            {
                if (this._selectedWaterSource == value) return;
                this._selectedWaterSource = value;
                this.OnPropertyChanged();
            }
        }

        public INavigation Navigation;
        public TargetingHh HouseHold { get; set; }
        public int Id { get; set; }

        public ObservableRangeCollection<TargetingHhMember> HouseholdMembers { get; } = new ObservableRangeCollection<TargetingHhMember>();

        public ObservableCollection<TargetingHhMember> GetHouseholdMembers(int id)
        {
            var items = App.Database.GetTableRows("TargetingHhMember", "TargetingHhId", id.ToString());
            var hh = new ObservableCollection<TargetingHhMember>();
            foreach (var item in items)
            {
                hh.Add((TargetingHhMember)item);
            }
            return hh;
        }

        private TargetingHhMember selectedMember;

        public TargetingHhMember SelectedMember
        {
            get { return selectedMember; }
            set
            {
                selectedMember = value;
                OnPropertyChanged();
                if (selectedMember == null)
                    return;
                Navigation.PushAsync(new TargetingAddEditMemberPage(selectedMember.Id, selectedMember.TargetingHhId));
                return;
                selectedMember = null;
            }
        }

        private ICommand _editHouseholdCommand;

        public ICommand EditHouseholdCommand => _editHouseholdCommand ?? (_editHouseholdCommand = new Command(async () => await ExecuteEditHousehold()));

        private async Task ExecuteEditHousehold()
        {
            try
            {
                await Navigation.PushAsync(new TargetingAddEditHouseHoldPage(HouseHold.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        #region Options

        public ObservableRangeCollection<SystemCodeDetail> LoadedBenefitTypes
        {
            get => BenefitTypes;
            set => SetProperty(ref BenefitTypes, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedInterviewStatus
        {
            get => InterviewStatus;
            set => SetProperty(ref InterviewStatus, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedCookingFuels
        {
            get => CookingFuels;
            set => SetProperty(ref CookingFuels, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedDwellingRisks
        {
            get => DwellingRisks;
            set => SetProperty(ref DwellingRisks, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedFloorConstructionMaterials
        {
            get => FloorConstructionMaterials;
            set => SetProperty(ref FloorConstructionMaterials, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedHouseholdConditions
        {
            get => HouseholdConditions;
            set => SetProperty(ref HouseholdConditions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedHouseholdOptions
        {
            get => HouseholdOptions;
            set => SetProperty(ref HouseholdOptions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedLightingFuels
        {
            get => LightingFuels;
            set => SetProperty(ref LightingFuels, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedNsnpBenefits
        {
            get => NsnpBenefits;
            set => SetProperty(ref NsnpBenefits, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedRoofConstructionMaterials
        {
            get => RoofConstructionMaterials;
            set => SetProperty(ref RoofConstructionMaterials, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedTenureStatuses
        {
            get => TenureStatuses;
            set => SetProperty(ref TenureStatuses, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedWallConstructionMaterials
        {
            get => WallConstructionMaterials;
            set => SetProperty(ref WallConstructionMaterials, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedWasteDisposals
        {
            get => WasteDisposals;
            set => SetProperty(ref WasteDisposals, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedWaterSources
        {
            get => WaterSources;
            set => SetProperty(ref WaterSources, value);
        }

        #endregion Options

        private ICommand _saveHouseHoldCommand;
        public ICommand SaveHouseHoldCommand => _saveHouseHoldCommand ?? (_saveHouseHoldCommand = new Command(async () => await ExecuteSaveHouseHold()));

        private async Task ExecuteSaveHouseHold()
        {
            try
            {
                var targetingHh = this.HouseHold;
                targetingHh.EndTime = DateFormatter.ToSQLiteDateTimeString(DateTime.Now); ;

                var errorMessage = "";

                if (SelectedTenureStatus == null)
                    errorMessage += "Tenure Status is required \n";
                else
                    targetingHh.TenureStatusId = SelectedTenureStatus.Id;

                if (SelectedRoofConstructionMaterial == null)
                    errorMessage += "Roof Construction Material is required \n";
                else
                    targetingHh.RoofConstructionMaterialId = SelectedRoofConstructionMaterial.Id;

                if (SelectedWallConstructionMaterial == null)
                    errorMessage += "Wall Construction Material is required \n";
                else
                    targetingHh.WallConstructionMaterialId = SelectedWallConstructionMaterial.Id;

                if (SelectedFloorConstructionMaterial == null)
                    errorMessage += "Floor /Tiling Construction Material is required \n";
                else
                    targetingHh.FloorConstructionMaterialId = SelectedFloorConstructionMaterial.Id;

                if (SelectedDwellingRisk == null)
                    errorMessage += "Dwelling Unit Risk is required \n";
                else
                    targetingHh.DwellingUnitRiskId = SelectedDwellingRisk.Id;

                if (SelectedHouseholdOptions == null)
                    errorMessage += "Dwelling Unit Risk is required \n";
                else
                    targetingHh.OtherProgrammesId = SelectedHouseholdOptions.Id;

                if (SelectedWaterSource == null)
                    errorMessage += "Water source is required \n";
                else
                    targetingHh.WaterSourceId = SelectedWaterSource.Id;

                if (SelectedWasteDisposal == null)
                    errorMessage += "Waste Disposal is required \n";
                else
                    targetingHh.WasteDisposalModeId = SelectedWasteDisposal.Id;

                if (SelectedCookingFuel == null)
                    errorMessage += "Cooking Fuel is required \n";
                else
                    targetingHh.CookingFuelTypeId = SelectedCookingFuel.Id;

                if (SelectedLightingFuel == null)
                    errorMessage += "Lighting Fuel is required \n";
                else
                    targetingHh.LightingFuelTypeId = SelectedLightingFuel.Id;

                if (SelectedHouseholdConditions == null)
                    errorMessage += "Household Conditions is required \n";
                else
                    targetingHh.HouseHoldConditionId = SelectedHouseholdConditions.Id;

                if (string.IsNullOrEmpty(targetingHh.Village))
                    errorMessage += "Village is required \n";
                if (string.IsNullOrEmpty(targetingHh.PhysicalAddress))
                    errorMessage += "Physical Address is required \n";
                if (string.IsNullOrEmpty(targetingHh.NearestReligiousBuilding))
                    errorMessage += "Nearest Church or Mosque is required \n";
                if (string.IsNullOrEmpty(targetingHh.NearestSchool))
                    errorMessage += "Nearest School is required \n";

                if (targetingHh.HabitableRooms < 1)
                    errorMessage += "The Habitable Rooms must be greater than 0 \n";

                if (targetingHh.HouseholdMembers < 2)
                    errorMessage += "The Household Members must be greater than 2 \n";

                if (errorMessage.Length > 0)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(
                        MessageKeys.Error,
                        new MessagingServiceAlert
                        {
                            Title = "Please Correct the Data and Try Again!!",
                            Message = errorMessage,
                            Cancel = "OK"
                        });
                    return;
                }
                var categoryId = App.Database.SystemCodeDetailGetByCode("REG_CATEGORIES", "TARGETING").Id;

                var position = await CrossGeolocator.Current.GetPositionAsync();

                targetingHh.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REG").Id;

                targetingHh.EndTime = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                targetingHh.RegistrationDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                targetingHh.UniqueId = Guid.NewGuid().ToString();

                targetingHh.Latitude = position.Latitude;
                targetingHh.Longitude = position.Longitude;
                targetingHh.EnumeratorId = Settings.Current.EnumeratorId;
                App.Database.AddOrUpdate(targetingHh);
                await Navigation.PopAsync();
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }
    }
}