﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views.Registration;
using CCTPMIS.RegMobileApp.Views.Targeting;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class TargetingViewModel : ViewModelBase
    {
        public TargetingViewModel(INavigation navigation) : base(navigation)
        {
            NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
            Households.AddRange(GetHouseholds());
        }
        public DateTime NextForceRefresh { get; set; }
        public ObservableRangeCollection<TargetingHh> Households { get; } = new ObservableRangeCollection<TargetingHh>();
        public ObservableCollection<TargetingHh> GetHouseholds()
        {
            var items = App.Database.GetTable("TargetingHh");
            var hh = new ObservableCollection<TargetingHh>();
            foreach (var item in items)
            {
                hh.Add((TargetingHh)item);
            }
            return hh;
        }

        private bool noHouseholdsFound;
        public bool NoHouseholdsFound
        {
            get { return noHouseholdsFound; }
            set { SetProperty(ref noHouseholdsFound, value); }
        }

        private string noHouseholdsFoundMessage;
        public string NoHouseholdsFoundMessage
        {
            get { return noHouseholdsFoundMessage; }
            set { SetProperty(ref noHouseholdsFoundMessage, value); }
        }

        private TargetingHh selectedHousehold;
        public TargetingHh SelectedHousehold
        {
            get { return selectedHousehold; }
            set
            {
                selectedHousehold = value;
                OnPropertyChanged();
                if (selectedHousehold == null)
                    return;
                Navigation.PushAsync(new TargetingDetailsPage(selectedHousehold.Id));
                return;
                SelectedHousehold = null;
            }
        }
        private ICommand loadHouseholdsCommand;

        public ICommand LoadHouseholdsCommand =>
            loadHouseholdsCommand ?? (loadHouseholdsCommand = new Command<bool>(async (f) => await ExecuteLoadHouseholdsAsync()));

        private async Task<bool> ExecuteLoadHouseholdsAsync(bool force = false)
        {
            if (IsBusy)
              return false;

            try
            {
                NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
                IsBusy = true;
                NoHouseholdsFound = false;
                
                Households.ReplaceRange(GetHouseholds());                 
            }
            catch (Exception ex)
            {
                Logger.Report(ex, "Method", "ExecuteLoadHouseholdsAsync");
                MessagingService.Current.SendMessage(MessageKeys.Error, ex);
            }
            finally
            {
                IsBusy = false;
            }
            return true;
        }
    }
}