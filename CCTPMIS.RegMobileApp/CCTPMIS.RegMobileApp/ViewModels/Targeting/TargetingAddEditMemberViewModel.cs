﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Models;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class TargetingAddEditMemberViewModel : ViewModelBase
    {
        public TargetingAddEditMemberViewModel(INavigation navigation, int? id, int hhId) : base(navigation)
        {
            LoadedMemberIdentificationDocumentTypes.AddRange(App.Database.SystemCodeDetailsGetByCode("Iden Doc Type"));
            LoadedMemberRelationships.AddRange(App.Database.SystemCodeDetailsGetByCode("Relationship"));
            LoadedMemberSex.AddRange(App.Database.SystemCodeDetailsGetByCode("Sex"));
            LoadedMemberMaritalStatuses.AddRange(App.Database.SystemCodeDetailsGetByCode("Marital Status"));
            LoadedMemberFatherAliveOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("Boolean Options"));
            LoadedMemberMotherAliveOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("Boolean Options"));
            LoadedMemberChronicIllnessOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("Boolean Options"));
            LoadedMemberDisabilityTypes.AddRange(App.Database.SystemCodeDetailsGetByCode("Disability"));
            LoadedMemberDisabilityCareStatuses.AddRange(App.Database.SystemCodeDetailsGetByCode("Boolean Options"));
            LoadedMemberLearningStatuses.AddRange(App.Database.SystemCodeDetailsGetByCode("Education Attendance"));
            LoadedMemberEducationLevels.AddRange(App.Database.SystemCodeDetailsGetByCode("Education Level"));
            LoadedMemberWorkTypes.AddRange(App.Database.SystemCodeDetailsGetByCode("Work Type"));
            LoadedMemberJobOptions.AddRange(App.Database.SystemCodeDetailsGetByCode("Boolean Options"));

            if (id == null)
            {
                HouseHoldMember = new TargetingHhMember
                {
                    TargetingHhId = hhId,
                    DateOfBirth = DateFormatter.ToSQLiteDateTimeString(DateTime.Now)
                };
            }
            else
            {
                var data = App.Database.GetTableRow("TargetingHhMember", "Id", id.ToString());
                HouseHoldMember = (TargetingHhMember) data;

                SelectedIdentificationDocumentType =
                    LoadedMemberIdentificationDocumentTypes.FirstOrDefault(x =>
                        x.Id == HouseHoldMember.IdentificationDocumentTypeId) ?? null;
                SelectedRelationship =
                    LoadedMemberRelationships.FirstOrDefault(x => x.Id == HouseHoldMember.RelationshipId) ?? null;
                SelectedSex = LoadedMemberSex.FirstOrDefault(x => x.Id == HouseHoldMember.SexId) ?? null;
                SelectedMaritalStatus =
                    LoadedMemberMaritalStatuses.FirstOrDefault(x => x.Id == HouseHoldMember.MaritalStatusId) ??
                    null;
                SelectedFatherAliveOption =
                    LoadedMemberFatherAliveOptions.FirstOrDefault(
                        x => x.Id == HouseHoldMember.FatherAliveStatusId) ?? null;
                SelectedMotherAliveOption =
                    LoadedMemberMotherAliveOptions.FirstOrDefault(
                        x => x.Id == HouseHoldMember.MotherAliveStatusId) ?? null;
                SelectedChronicIllnessOption =
                    LoadedMemberChronicIllnessOptions.FirstOrDefault(x =>
                        x.Id == HouseHoldMember.ChronicIllnessStatusId) ?? null;
                SelectedDisabilityType =
                    LoadedMemberDisabilityTypes.FirstOrDefault(x => x.Id == HouseHoldMember.DisabilityTypeId) ??
                    null;
                SelectedDisabilityCareStatus =
                    LoadedMemberDisabilityCareStatuses.FirstOrDefault(x =>
                        x.Id == HouseHoldMember.DisabilityCareStatusId) ?? null;
                SelectedLearningStatus =
                    LoadedMemberLearningStatuses.FirstOrDefault(x => x.Id == HouseHoldMember.LearningStatusId) ??
                    null;
                SelectedEducationLevel =
                    LoadedMemberEducationLevels.FirstOrDefault(x => x.Id == HouseHoldMember.EducationLevelId) ??
                    null;
                SelectedWorkType =
                    LoadedMemberWorkTypes.FirstOrDefault(x => x.Id == HouseHoldMember.WorkLevelId) ?? null;
                SelectedJobOption =
                    LoadedMemberJobOptions.FirstOrDefault(x => x.Id == HouseHoldMember.FormalJobNgoId) ?? null;

                HouseHoldMember.DateOfBirth = DateFormatter.ToEnDateTimeString(HouseHoldMember.DateOfBirth);
            }
        }

        public TargetingHhMember HouseHoldMember { get; set; }

        public ObservableRangeCollection<SystemCodeDetail> MemberJobOptions =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberWorkTypes =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberEducationLevels =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberLearningStatuses =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberDisabilityCareStatuses =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberDisabilityTypes =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberChronicIllnessOptions =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberMotherAliveOptions =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberFatherAliveOptions =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MaritalStatuses =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> MemberSexs =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> Relationships =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> IdentificationDocumentTypes =
            new ObservableRangeCollection<SystemCodeDetail>();

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberIdentificationDocumentTypes
        {
            get => IdentificationDocumentTypes;
            set => SetProperty(ref IdentificationDocumentTypes, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberRelationships
        {
            get => Relationships;
            set => SetProperty(ref Relationships, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberSex
        {
            get => MemberSexs;
            set => SetProperty(ref MemberSexs, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberMaritalStatuses
        {
            get => MaritalStatuses;
            set => SetProperty(ref MaritalStatuses, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberFatherAliveOptions
        {
            get => MemberFatherAliveOptions;
            set => SetProperty(ref MemberFatherAliveOptions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberMotherAliveOptions
        {
            get => MemberMotherAliveOptions;
            set => SetProperty(ref MemberMotherAliveOptions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberChronicIllnessOptions
        {
            get => MemberChronicIllnessOptions;
            set => SetProperty(ref MemberChronicIllnessOptions, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberDisabilityTypes
        {
            get => MemberDisabilityTypes;
            set => SetProperty(ref MemberDisabilityTypes, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberDisabilityCareStatuses
        {
            get => MemberDisabilityCareStatuses;
            set => SetProperty(ref MemberDisabilityCareStatuses, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberLearningStatuses
        {
            get => MemberLearningStatuses;
            set => SetProperty(ref MemberLearningStatuses, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberEducationLevels
        {
            get => MemberEducationLevels;
            set => SetProperty(ref MemberEducationLevels, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberWorkTypes
        {
            get => MemberWorkTypes;
            set => SetProperty(ref MemberWorkTypes, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedMemberJobOptions
        {
            get => MemberJobOptions;
            set => SetProperty(ref MemberJobOptions, value);
        }

        public SystemCodeDetail SelectedIdentificationDocumentType
        {
            get => _SelectedIdentificationDocumentType;
            set
            {
                if (this._SelectedIdentificationDocumentType == value) return;
                this._SelectedIdentificationDocumentType = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedRelationship
        {
            get => _SelectedRelationship;
            set
            {
                if (this._SelectedRelationship == value) return;
                this._SelectedRelationship = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedSex
        {
            get => _SelectedSex;
            set
            {
                if (this._SelectedSex == value) return;
                this._SelectedSex = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedMaritalStatus
        {
            get => _SelectedMaritalStatus;
            set
            {
                if (this._SelectedMaritalStatus == value) return;
                this._SelectedMaritalStatus = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedFatherAliveOption
        {
            get => _SelectedFatherAliveOption;
            set
            {
                if (this._SelectedFatherAliveOption == value) return;
                this._SelectedFatherAliveOption = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedMotherAliveOption
        {
            get => _SelectedMotherAliveOption;
            set
            {
                if (this._SelectedMotherAliveOption == value) return;
                this._SelectedMotherAliveOption = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedChronicIllnessOption
        {
            get => _SelectedChronicIllnessOption;
            set
            {
                if (this._SelectedChronicIllnessOption == value) return;
                this._SelectedChronicIllnessOption = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedDisabilityType
        {
            get => _SelectedDisabilityType;
            set
            {
                if (this._SelectedDisabilityType == value) return;
                this._SelectedDisabilityType = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedDisabilityCareStatus
        {
            get => _SelectedDisabilityCareStatus;
            set
            {
                if (this._SelectedDisabilityCareStatus == value) return;
                this._SelectedDisabilityCareStatus = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedLearningStatus
        {
            get => _SelectedLearningStatus;
            set
            {
                if (this._SelectedLearningStatus == value) return;
                this._SelectedLearningStatus = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedEducationLevel
        {
            get => _SelectedEducationLevel;
            set
            {
                if (this._SelectedEducationLevel == value) return;
                this._SelectedEducationLevel = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedWorkType
        {
            get => _SelectedWorkType;
            set
            {
                if (this._SelectedWorkType == value) return;
                this._SelectedWorkType = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedJobOption
        {
            get => _SelectedJobOption;
            set
            {
                if (this._SelectedJobOption == value) return;
                this._SelectedJobOption = value;
                this.OnPropertyChanged();
            }
        }

        private SystemCodeDetail _SelectedSex;
        private SystemCodeDetail _SelectedMaritalStatus;
        private SystemCodeDetail _SelectedFatherAliveOption;
        private SystemCodeDetail _SelectedMotherAliveOption;
        private SystemCodeDetail _SelectedChronicIllnessOption;
        private SystemCodeDetail _SelectedDisabilityType;
        private SystemCodeDetail _SelectedDisabilityCareStatus;
        private SystemCodeDetail _SelectedLearningStatus;
        private SystemCodeDetail _SelectedEducationLevel;
        private SystemCodeDetail _SelectedWorkType;
        private SystemCodeDetail _SelectedJobOption;
        private SystemCodeDetail _SelectedRelationship;
        private SystemCodeDetail _SelectedIdentificationDocumentType;

        private ICommand _saveMemberCommand;

        public ICommand SaveMemberCommand => _saveMemberCommand ??
                                                (_saveMemberCommand = new Command(async () =>
                                                    await ExecuteSaveHouseHoldMember()));

        private async Task ExecuteSaveHouseHoldMember()
        {
            var targetingHh = this.HouseHoldMember;
            try
            {
              ///  if(SelectedFatherAliveOption!=null)
                    var errorMessage = "";

                if (SelectedFatherAliveOption == null)
                    errorMessage += "Is Father alive required \n";
                else
                    targetingHh.FatherAliveStatusId = SelectedFatherAliveOption.Id;

                if (SelectedMotherAliveOption == null)
                    errorMessage += "Is Mother alive required \n";
                else
                    targetingHh.MotherAliveStatusId = SelectedMotherAliveOption.Id;

                if (SelectedMaritalStatus == null)
                    errorMessage += "Marital Status required \n";
                else
                    targetingHh.MaritalStatusId = SelectedMaritalStatus.Id;

                if (SelectedMaritalStatus == null)
                    errorMessage += "Marital Status required \n";
                else
                    targetingHh.MaritalStatusId = SelectedMaritalStatus.Id;

                if (SelectedIdentificationDocumentType == null)
                    errorMessage += "Identification Document Type required \n";
                else
                    targetingHh.IdentificationDocumentTypeId = SelectedIdentificationDocumentType.Id;

                if (SelectedRelationship == null)
                    errorMessage += "Relationship required \n";
                else
                    targetingHh.RelationshipId = SelectedRelationship.Id;

                if (SelectedSex == null)
                    errorMessage += "Sex required \n";
                else
                    targetingHh.SexId = SelectedSex.Id;

                if (SelectedChronicIllnessOption == null)
                    errorMessage += "Chronic Illness required \n";
                else
                    targetingHh.ChronicIllnessStatusId = SelectedChronicIllnessOption.Id;


                try
                {
                    App.Database.AddOrUpdate(targetingHh);
                    await Navigation.PopAsync();
                }
                catch (Exception)
                {

                    
                }
               
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }
    }
}