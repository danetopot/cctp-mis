﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views.Registration;
using CCTPMIS.RegMobileApp.Views.Targeting;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class TargetingDetailsViewModel : ViewModelBase
    {
        public TargetingDetailsViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("TargetingHh", "Id", id.ToString());
            HouseHold = (TargetingHh)data;
            HouseHold.Programme = (Programme)App.Database.GetTableRow("Programme", "id", HouseHold.ProgrammeId.ToString());
            HouseHold.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", HouseHold.SubLocationId.ToString());
            HouseHold.Location = (Location)App.Database.GetTableRow("Location", "id", HouseHold.LocationId.ToString());

            HouseholdMembers.AddRange(GetHouseholdMembers(id));
            Navigation = navigation;
            Id = id;
        }

        public INavigation Navigation;
        public TargetingHh HouseHold { get; set; }
        public int Id { get; set; }

        public ObservableRangeCollection<TargetingHhMember> HouseholdMembers { get; } = new ObservableRangeCollection<TargetingHhMember>();

        public ObservableCollection<TargetingHhMember> GetHouseholdMembers(int id)
        {
            var items = App.Database.GetTableRows("TargetingHhMember", "TargetingHhId", id.ToString());
            var hh = new ObservableCollection<TargetingHhMember>();
            foreach (var item in items)
            {
                hh.Add((TargetingHhMember)item);
            }
            return hh;
        }

        private TargetingHhMember selectedMember;

        public TargetingHhMember SelectedMember
        {
            get { return selectedMember; }
            set
            {
                selectedMember = value;
                OnPropertyChanged();
                if (selectedMember == null)
                    return;
                Navigation.PushAsync(new TargetingAddEditMemberPage(selectedMember.Id, selectedMember.TargetingHhId));
                return;
                selectedMember = null;
            }
        }

        private ICommand _editHouseholdCommand;

        public ICommand EditHouseholdCommand => _editHouseholdCommand ?? (_editHouseholdCommand = new Command(async () => await ExecuteEditHousehold()));

        private async Task ExecuteEditHousehold()
        {
            try
            {
                await Navigation.PushAsync(new TargetingAddEditHouseHoldPage(HouseHold.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }

        private ICommand _newHouseholdMemberCommand;

        public ICommand NewHouseholdMemberCommand => _newHouseholdMemberCommand ?? (_newHouseholdMemberCommand = new Command(async () => await ExecuteNewHouseholdMember()));

        private async Task ExecuteNewHouseholdMember()
        {
            try
            {
                await Navigation.PushAsync(new TargetingAddEditMemberPage(null,HouseHold.Id));
            }
            catch (Exception e)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please  Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
        }
    }
}