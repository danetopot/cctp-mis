﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using FluentValidation.Results;
using MvvmHelpers;
using Plugin.DeviceInfo;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using CCTPMIS.RegMobileApp.Helpers;
    using CCTPMIS.RegMobileApp.Interfaces;
    using CCTPMIS.RegMobileApp.Models;
    using CCTPMIS.RegMobileApp.Services;

    using Plugin.Share;
    using Plugin.Share.Abstractions;

    using Xamarin.Forms;

    public class ViewModelBase : BaseViewModel
    {
        private ICommand launchBrowserCommand;

        public ViewModelBase(INavigation navigation = null)
        {
            Navigation = navigation;

            AppVersion = $"Software Build {CrossDeviceInfo.Current.AppBuild} - Version. {CrossDeviceInfo.Current.AppVersion}. "; ;
        }

        private string _validateMessage;

        public string ValidateMessage
        {
            get
            {
                return _validateMessage;
            }
            set
            {
                SetProperty(ref this._validateMessage, value, "ValidateMessage");
            }
        }

        private string _appVersion;

        public string AppVersion
        {
            get
            {
                return _appVersion;
            }
            set
            {
                SetProperty(ref this._appVersion, value, "AppVersion");
            }
        }

        protected string GetErrorListFromValidationResult(ValidationResult validationResult)
        {
            var errorList = validationResult.Errors.Select(x=>x.ErrorMessage).ToList();
            return String.Join("\n", errorList.ToArray()); ;
        }

        public virtual void Subscribe()
        {
        }

        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }

        public virtual void OnBackButtonPressed()
        {
        }

        public virtual void OnPopped(Page page)
        {
        }

        public ICommand LaunchBrowserCommand =>
            launchBrowserCommand ?? (launchBrowserCommand =
                                         new Command<string>(async (t) => await ExecuteLaunchBrowserAsync(t)));

        public Settings Settings
        {
            get
            {
                return Settings.Current;
            }
        }

        protected ILogger Logger { get; } = DependencyService.Get<ILogger>();

        protected INavigation Navigation { get; }

        protected IToast Toast { get; } = DependencyService.Get<IToast>();

        public static void Init(bool mock = true)
        {
            DependencyService.Register<ISSOClient,  CctpssoClient>();
        }

        private async Task ExecuteLaunchBrowserAsync(string arg)
        {
            if (IsBusy)
                return;

            if (!arg.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
                && !arg.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
                arg = "http://" + arg;

            // Logger.Track(EvolveLoggerKeys.LaunchedBrowser, "Url", arg);
            var lower = arg.ToLowerInvariant();
            if (Device.RuntimePlatform == Device.iOS && lower.Contains("twitter.com"))
            {
                try
                {
                    var id = arg.Substring(lower.LastIndexOf("/", StringComparison.Ordinal) + 1);
                }
                catch
                {
                }
            }

            try
            {
                await CrossShare.Current.OpenBrowser(
                    arg,
                    new BrowserOptions
                    {
                        ChromeShowTitle = true,
                        ChromeToolbarColor = new ShareColor { A = 255, R = 118, G = 53, B = 235 },
                        UseSafariReaderMode = true,
                        UseSafariWebViewController = true
                    });
            }
            catch
            {
            }
        }
    }

    public abstract class ExtendedBindableObject : BindableObject
    {
        public void RaisePropertyChanged<T>(Expression<Func<T>> property)
        {
            var name = GetMemberInfo(property).Name;
            OnPropertyChanged(name);
        }

        private MemberInfo GetMemberInfo(Expression expression)
        {
            MemberExpression operand;
            LambdaExpression lambdaExpression = (LambdaExpression)expression;
            if (lambdaExpression.Body as UnaryExpression != null)
            {
                UnaryExpression body = (UnaryExpression)lambdaExpression.Body;
                operand = (MemberExpression)body.Operand;
            }
            else
            {
                operand = (MemberExpression)lambdaExpression.Body;
            }
            return operand.Member;
        }
    }
}