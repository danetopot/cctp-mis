﻿using System.Net.Sockets;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System.Threading.Tasks;
    using System.Windows.Input;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Models;

    using FormsToolkit;

    using MvvmHelpers;

    using Xamarin.Forms;

    public class NewHouseholdMemberViewModel : ViewModelBase
    {
        public ObservableRangeCollection<MemberChronicIllnessOption> MemberChronicIllnessOptions =
            new ObservableRangeCollection<MemberChronicIllnessOption>();

        public ObservableRangeCollection<MemberDisabilityCareStatus> MemberDisabilityCareStatuses =
            new ObservableRangeCollection<MemberDisabilityCareStatus>();

        public ObservableRangeCollection<MemberDisabilityType> MemberDisabilityTypes =
            new ObservableRangeCollection<MemberDisabilityType>();

        public ObservableRangeCollection<MemberEducationLevel> MemberEducationLevels =
            new ObservableRangeCollection<MemberEducationLevel>();

        public ObservableRangeCollection<MemberFatherAliveOption> MemberFatherAliveOptions =
            new ObservableRangeCollection<MemberFatherAliveOption>();

        public ObservableRangeCollection<MemberIdentificationDocumentType> MemberIdentificationDocumentTypes =
            new ObservableRangeCollection<MemberIdentificationDocumentType>();

        public ObservableRangeCollection<MemberJobOption> MemberJobOptions =
            new ObservableRangeCollection<MemberJobOption>();

        public ObservableRangeCollection<MemberLearningStatus> MemberLearningStatuses =
            new ObservableRangeCollection<MemberLearningStatus>();

        public ObservableRangeCollection<MemberMaritalStatus> MemberMaritalStatuses =
            new ObservableRangeCollection<MemberMaritalStatus>();

        public ObservableRangeCollection<MemberMotherAliveOption> MemberMotherAliveOptions =
            new ObservableRangeCollection<MemberMotherAliveOption>();

        public ObservableRangeCollection<MemberRelationship> MemberRelationships =
            new ObservableRangeCollection<MemberRelationship>();

        public ObservableRangeCollection<MemberSex> MemberSexs = new ObservableRangeCollection<MemberSex>();

        public ObservableRangeCollection<MemberStatusOption> MemberStatusOptions =
            new ObservableRangeCollection<MemberStatusOption>();

        public ObservableRangeCollection<MemberWorkType> MemberWorkTypes =
            new ObservableRangeCollection<MemberWorkType>();

        private ICommand saveMemberCommand;

        private MemberChronicIllnessOption selectedMemberChronicIllnessOption;

        private MemberDisabilityCareStatus selectedMemberDisabilityCareStatus;

        private MemberDisabilityType selectedMemberDisabilityType;

        private MemberEducationLevel selectedMemberEducationLevel;

        private MemberFatherAliveOption selectedMemberFatherAliveOption;

        private MemberIdentificationDocumentType selectedMemberIdentificationDocumentType;

        private MemberJobOption selectedMemberJobOption;

        private MemberLearningStatus selectedMemberLearningStatus;

        private MemberMaritalStatus selectedMemberMaritalStatus;

        private MemberMotherAliveOption selectedMemberMotherAliveOption;

        private MemberRelationship selectedMemberRelationship;

        private MemberSex selectedMemberSex;

        private MemberStatusOption selectedMemberStatusOption;

        private MemberWorkType selectedMemberWorkType;

        public NewHouseholdMemberViewModel(INavigation navigation, HouseHoldMember householdMember = null)
            : base(navigation)
        {
            LoadedMemberIdentificationDocumentTypes.AddRange(
                HouseHoldMemberOptionsData.MemberIdentificationDocumentTypes);
            LoadedMemberDisabilityCareStatuses.AddRange(HouseHoldMemberOptionsData.MemberDisabilityCareStatuses);
            LoadedMemberDisabilityTypes.AddRange(HouseHoldMemberOptionsData.MemberDisabilityTypes);
            LoadedMemberEducationLevels.AddRange(HouseHoldMemberOptionsData.MemberEducationLevels);
            LoadedMemberLearningStatuses.AddRange(HouseHoldMemberOptionsData.MemberLearningStatuses);
            LoadedMemberMaritalStatuses.AddRange(HouseHoldMemberOptionsData.MemberMaritalStatuses);
            LoadedMemberRelationships.AddRange(HouseHoldMemberOptionsData.MemberRelationships);
            LoadedMemberSexs.AddRange(HouseHoldMemberOptionsData.MemberSexs);
            LoadedMemberStatusOptions.AddRange(HouseHoldMemberOptionsData.MemberStatusOptions);
            LoadedMemberWorkTypes.AddRange(HouseHoldMemberOptionsData.MemberWorkTypes);
            LoadedMemberFatherAliveOptions.AddRange(HouseHoldMemberOptionsData.MemberFatherAliveOptions);
            LoadedMemberMotherAliveOptions.AddRange(HouseHoldMemberOptionsData.MemberMotherAliveOptions);
            LoadedMemberChronicIllnessOptions.AddRange(HouseHoldMemberOptionsData.MemberChronicIllnessOptions);
            LoadedMemberJobOptions.AddRange(HouseHoldMemberOptionsData.MemberJobOptions);
            this.HouseHoldMember = householdMember ?? new HouseHoldMember();
        }

        public HouseHoldMember HouseHoldMember { get; set; }

        public ObservableRangeCollection<MemberChronicIllnessOption> LoadedMemberChronicIllnessOptions
        {
            get => MemberChronicIllnessOptions;
            set => SetProperty(ref MemberChronicIllnessOptions, value);
        }

        public ObservableRangeCollection<MemberDisabilityCareStatus> LoadedMemberDisabilityCareStatuses
        {
            get => MemberDisabilityCareStatuses;
            set => SetProperty(ref MemberDisabilityCareStatuses, value);
        }

        public ObservableRangeCollection<MemberDisabilityType> LoadedMemberDisabilityTypes
        {
            get => MemberDisabilityTypes;
            set => SetProperty(ref MemberDisabilityTypes, value);
        }

        public ObservableRangeCollection<MemberEducationLevel> LoadedMemberEducationLevels
        {
            get => MemberEducationLevels;
            set => SetProperty(ref MemberEducationLevels, value);
        }

        public ObservableRangeCollection<MemberFatherAliveOption> LoadedMemberFatherAliveOptions
        {
            get => MemberFatherAliveOptions;
            set => SetProperty(ref MemberFatherAliveOptions, value);
        }

        public ObservableRangeCollection<MemberIdentificationDocumentType> LoadedMemberIdentificationDocumentTypes
        {
            get => MemberIdentificationDocumentTypes;
            set => SetProperty(ref MemberIdentificationDocumentTypes, value);
        }

        public ObservableRangeCollection<MemberJobOption> LoadedMemberJobOptions
        {
            get => MemberJobOptions;
            set => SetProperty(ref MemberJobOptions, value);
        }

        public ObservableRangeCollection<MemberLearningStatus> LoadedMemberLearningStatuses
        {
            get => MemberLearningStatuses;
            set => SetProperty(ref MemberLearningStatuses, value);
        }

        public ObservableRangeCollection<MemberMaritalStatus> LoadedMemberMaritalStatuses
        {
            get => MemberMaritalStatuses;
            set => SetProperty(ref MemberMaritalStatuses, value);
        }

        public ObservableRangeCollection<MemberMotherAliveOption> LoadedMemberMotherAliveOptions
        {
            get => MemberMotherAliveOptions;
            set => SetProperty(ref MemberMotherAliveOptions, value);
        }

        public ObservableRangeCollection<MemberRelationship> LoadedMemberRelationships
        {
            get => MemberRelationships;
            set => SetProperty(ref MemberRelationships, value);
        }

        public ObservableRangeCollection<MemberSex> LoadedMemberSexs
        {
            get => MemberSexs;
            set => SetProperty(ref MemberSexs, value);
        }

        public ObservableRangeCollection<MemberStatusOption> LoadedMemberStatusOptions
        {
            get => MemberStatusOptions;
            set => SetProperty(ref MemberStatusOptions, value);
        }

        public ObservableRangeCollection<MemberWorkType> LoadedMemberWorkTypes
        {
            get => MemberWorkTypes;
            set => SetProperty(ref MemberWorkTypes, value);
        }

        public ICommand SaveMemberCommand =>
            saveMemberCommand ?? (saveMemberCommand = new Command(async () => await ExecuteSaveMemberAsync()));

        public MemberChronicIllnessOption SelectedMemberChronicIllnessOption
        {
            get => selectedMemberChronicIllnessOption;
            set
            {
                if (this.selectedMemberChronicIllnessOption == value) return;
                this.selectedMemberChronicIllnessOption = value;
                this.OnPropertyChanged();
            }
        }

        public MemberDisabilityCareStatus SelectedMemberDisabilityCareStatus
        {
            get => selectedMemberDisabilityCareStatus;
            set
            {
                if (this.selectedMemberDisabilityCareStatus == value) return;
                this.selectedMemberDisabilityCareStatus = value;
                this.OnPropertyChanged();
            }
        }

        public MemberDisabilityType SelectedMemberDisabilityType
        {
            get => selectedMemberDisabilityType;
            set
            {
                if (this.selectedMemberDisabilityType == value) return;
                this.selectedMemberDisabilityType = value;
                this.OnPropertyChanged();
            }
        }

        public MemberEducationLevel SelectedMemberEducationLevel
        {
            get => selectedMemberEducationLevel;
            set
            {
                if (this.selectedMemberEducationLevel == value) return;
                this.selectedMemberEducationLevel = value;
                this.OnPropertyChanged();
            }
        }

        public MemberFatherAliveOption SelectedMemberFatherAliveOption
        {
            get => selectedMemberFatherAliveOption;
            set
            {
                if (this.selectedMemberFatherAliveOption == value) return;
                this.selectedMemberFatherAliveOption = value;
                this.OnPropertyChanged();
            }
        }

        public MemberIdentificationDocumentType SelectedMemberIdentificationDocumentType
        {
            get => selectedMemberIdentificationDocumentType;
            set
            {
                if (this.selectedMemberIdentificationDocumentType == value) return;
                this.selectedMemberIdentificationDocumentType = value;
                this.OnPropertyChanged();
            }
        }

        public MemberJobOption SelectedMemberJobOption
        {
            get => selectedMemberJobOption;
            set
            {
                if (this.selectedMemberJobOption == value) return;
                this.selectedMemberJobOption = value;
                this.OnPropertyChanged();
            }
        }

        public MemberLearningStatus SelectedMemberLearningStatus
        {
            get => selectedMemberLearningStatus;
            set
            {
                if (this.selectedMemberLearningStatus == value) return;
                this.selectedMemberLearningStatus = value;
                this.OnPropertyChanged();
            }
        }

        public MemberMaritalStatus SelectedMemberMaritalStatus
        {
            get => selectedMemberMaritalStatus;
            set
            {
                if (this.selectedMemberMaritalStatus == value) return;
                this.selectedMemberMaritalStatus = value;
                this.OnPropertyChanged();
            }
        }

        public MemberMotherAliveOption SelectedMemberMotherAliveOption
        {
            get => selectedMemberMotherAliveOption;
            set
            {
                if (this.selectedMemberMotherAliveOption == value) return;
                this.selectedMemberMotherAliveOption = value;
                this.OnPropertyChanged();
            }
        }

        public MemberRelationship SelectedMemberRelationship
        {
            get => selectedMemberRelationship;
            set
            {
                if (this.selectedMemberRelationship == value) return;
                this.selectedMemberRelationship = value;
                this.OnPropertyChanged();
            }
        }

        public MemberSex SelectedMemberSex
        {
            get => selectedMemberSex;
            set
            {
                if (this.selectedMemberSex == value) return;
                this.selectedMemberSex = value;
                this.OnPropertyChanged();
            }
        }

        public MemberStatusOption SelectedMemberStatusOption
        {
            get => selectedMemberStatusOption;
            set
            {
                if (this.selectedMemberStatusOption == value) return;
                this.selectedMemberStatusOption = value;
                this.OnPropertyChanged();
            }
        }

        public MemberWorkType SelectedMemberWorkType
        {
            get => selectedMemberWorkType;
            set
            {
                if (this.selectedMemberWorkType == value) return;
                this.selectedMemberWorkType = value;
                this.OnPropertyChanged();
            }
        }

        private async Task ExecuteSaveMemberAsync()
        {
            var member = this.HouseHoldMember;
            if (SelectedMemberIdentificationDocumentType != null)
                member.MemberIdentificationDocumentTypeId = SelectedMemberIdentificationDocumentType.Id;
            if (SelectedMemberRelationship != null)
                member.MemberRelationshipId = SelectedMemberRelationship.Id;
            if (SelectedMemberSex != null)
                member.MemberSexId = SelectedMemberSex.Id;
            if (SelectedMemberMaritalStatus != null)
                member.MemberMaritalStatusId = SelectedMemberMaritalStatus.Id;
            if (SelectedMemberFatherAliveOption != null)
                member.MemberFatherAliveStatusId = SelectedMemberFatherAliveOption.Id;
            if (SelectedMemberMotherAliveOption != null)
                member.MemberMotherAliveStatusId = SelectedMemberMotherAliveOption.Id;
            if (SelectedMemberChronicIllnessOption != null)
                member.MemberChronicIllnessStatusId = SelectedMemberChronicIllnessOption.Id;
            if (SelectedMemberDisabilityType != null)
                member.MemberDisabilityTypeId = SelectedMemberDisabilityType.Id;
            if (SelectedMemberDisabilityCareStatus != null)
                member.MemberDisabilityCareStatusId = SelectedMemberDisabilityCareStatus.Id;
            if (selectedMemberLearningStatus != null)
                member.MemberLearningStatusId = selectedMemberLearningStatus.Id;
            if (SelectedMemberEducationLevel != null)
                member.MemberEducationLevelId = SelectedMemberEducationLevel.Id;
            if (SelectedMemberWorkType != null)
                member.MemberWorkLevelId = SelectedMemberWorkType.Id;
            if (SelectedMemberJobOption != null)
                member.MemberFormalJobNgoId = SelectedMemberJobOption.Id;

            await Task.FromResult(0);

            App.Database.SaveHouseHoldMember(member);

            MessagingService.Current.SendMessage<MessagingServiceAlert>(
                MessageKeys.Error,
                new MessagingServiceAlert
                    {
                        Title = "Success",
                        Message = "Data Saved Locally Successfully",
                        Cancel = "OK"
                    });
        }
    }
}