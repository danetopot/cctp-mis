﻿namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Models;
    using CCTPMIS.RegMobileApp.Views;
    using FormsToolkit;
    using MvvmHelpers;

    using Xamarin.Forms;

    public class NewHouseHoldRegistrationStep01ViewModel : ViewModelBase
    {
        public ObservableRangeCollection<Constituency> Constituencies = new ObservableRangeCollection<Constituency>();

        public ObservableRangeCollection<County> Counties = new ObservableRangeCollection<County>();

        public ObservableRangeCollection<Division> Divisions = new ObservableRangeCollection<Division>();

        public ObservableRangeCollection<Locality> Localities = new ObservableRangeCollection<Locality>();

        public ObservableRangeCollection<Location> Locations = new ObservableRangeCollection<Location>();

        public ObservableRangeCollection<Programme> Programmes = new ObservableRangeCollection<Programme>();

        public ObservableRangeCollection<SubLocation> SubLocations = new ObservableRangeCollection<SubLocation>();

        private ICommand saveHouseHoldStep01Command;

        private Constituency selectedConstituency;

        private County selectedCounty;

        private Division selectedDivision;

        private Locality selectedLocality;

        private  Location selectedLocation;

        private Programme selectedProgramme;

        private SubLocation selectedSubLocation;

        public NewHouseHoldRegistrationStep01ViewModel(
            INavigation navigation,
            HouseHoldRegistration houseHoldRegistration = null)
            : base(navigation)
        {
            LoadedCounties.AddRange(RegistrationData.Counties);
            LoadedConstituencies.AddRange(RegistrationData.Constituencies);
            LoadedProgrammes.AddRange(RegistrationData.Programmes);
            LoadedLocations.AddRange(RegistrationData.Locations);
            LoadedLocalities.AddRange(RegistrationData.Localities);
            LoadedDivisions.AddRange(RegistrationData.Divisions);
            LoadedSubLocations.AddRange(RegistrationData.SubLocations);
            this.HouseHoldRegistration = houseHoldRegistration ?? new HouseHoldRegistration();
        }

        public HouseHoldRegistration HouseHoldRegistration { get; set; }

        public ObservableRangeCollection<Constituency> LoadedConstituencies
        {
            get => Constituencies;
            set => SetProperty(ref Constituencies, value);
        }

        public ObservableRangeCollection<County> LoadedCounties
        {
            get => Counties;
            set => SetProperty(ref Counties, value);
        }

        public ObservableRangeCollection<Division> LoadedDivisions
        {
            get => Divisions;
            set => SetProperty(ref Divisions, value);
        }

        public ObservableRangeCollection<Locality> LoadedLocalities
        {
            get => Localities;
            set => SetProperty(ref Localities, value);
        }

        public ObservableRangeCollection<Location> LoadedLocations
        {
            get => Locations;
            set => SetProperty(ref Locations, value);
        }

        public ObservableRangeCollection<Programme> LoadedProgrammes
        {
            get => Programmes;
            set => SetProperty(ref Programmes, value);
        }

        public ObservableRangeCollection<SubLocation> LoadedSubLocations
        {
            get => SubLocations;
            set => SetProperty(ref SubLocations, value);
        }

        public ICommand SaveHouseHoldStep01Command =>
            saveHouseHoldStep01Command ?? (saveHouseHoldStep01Command =
                                               new Command(async () => await ExecuteSaveHouseHoldStep01Async()));

        public Constituency SelectedConstituency
        {
            get => selectedConstituency;
            set
            {
                if (selectedConstituency == value) return;
                selectedConstituency = value;
                OnPropertyChanged();

                // Locations.Clear();
                // Locations.AddRange(RegistrationData.Locations.Where(x => x.DivisionId == this.selectedConstituency.Id));
            }
        }

        public County SelectedCounty
        {
            get => selectedCounty;
            set
            {
                if (selectedCounty == value) return;
                selectedCounty = value;
                OnPropertyChanged();
                this.Constituencies.Clear();
                Constituencies.AddRange(
                    RegistrationData.Constituencies.Where(x => x.CountyId == this.selectedCounty.Id));
            }
        }

        public Division SelectedDivision
        {
            get => selectedDivision;
            set
            {
                if (selectedDivision == value) return;
                selectedDivision = value;
                OnPropertyChanged();

                Locations.Clear();
                Locations.AddRange(RegistrationData.Locations.Where(x => x.DivisionId == this.selectedDivision.Id));
            }
        }

        public Locality SelectedLocality
        {
            get => selectedLocality;
            set
            {
                if (selectedLocality == value) return;
                selectedLocality = value;
                OnPropertyChanged();
            }
        }

        public  Location SelectedLocation
        {
            get => selectedLocation;
            set
            {
                if (selectedLocation == value) return;
                selectedLocation = value;
                OnPropertyChanged();
 
                SubLocations.Clear();
                SubLocations.AddRange(
                    RegistrationData.SubLocations.Where(x => x.LocationId == this.selectedLocation.Id));
            }
        }

        public Programme SelectedProgramme
        {
            get => selectedProgramme;
            set
            {
                if (this.selectedProgramme == value) return;
                this.selectedProgramme = value;
                this.OnPropertyChanged();
            }
        }

        public SubLocation SelectedSubLocation
        {
            get => selectedSubLocation;
            set
            {
                if (selectedSubLocation == value) return;
                selectedSubLocation = value;

                 OnPropertyChanged();
            }
        }

        private async Task ExecuteSaveHouseHoldStep01Async()
        {
            var household = this.HouseHoldRegistration;
           var errorMessage = "";
            //if (SelectedProgramme==null)           
            //    errorMessage += "Programme is required \n";       
            //if (SelectedCounty == null)        
            //  errorMessage += "County is required \n";
            //if (SelectedConstituency == null)
            //    errorMessage += "Constituency is required \n";
            ////if (SelectedDivision == null)
            ////    errorMessage += "Division is required \n";

            //if (SelectedLocation == null)
            //    errorMessage += "Location is required \n";

            //if (SelectedSubLocation == null)
            //    errorMessage += "SubLocation is required \n";

            //if (SelectedLocality == null)
            //    errorMessage += "Locality is required \n";

            //if (string.IsNullOrEmpty(household.Village))
            //    errorMessage += "Village is required \n";
            //if (string.IsNullOrEmpty(household.PhysicalAddress))
            //    errorMessage += "Physical Address is required \n";

            //if (household.Years == 0)
            //    errorMessage += "Years is required \n";

            //if (household.Months == 0)
            //    errorMessage += "Months is required \n";

            //if (string.IsNullOrEmpty(household.NearestReligiousBuilding))
            //    errorMessage += "Nearest Church or Mosque is required \n";

            //if (string.IsNullOrEmpty(household.NearestSchool))
            //    errorMessage += "Nearest School is required \n";

            //if (string.IsNullOrEmpty(household.HouseHoldHeadName))
            //    errorMessage += "Household Head's Name is required \n";

            if (errorMessage.Length>0)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = errorMessage,
                        Cancel = "OK"
                    });
                return;
            }

            household.Status = "1";
            App.Database.SaveHouseHold(household);
         
            await Navigation.PopModalAsync();
           await Navigation.PushAsync(new HouseHoldRegistrationsPage());
        }
    }
}