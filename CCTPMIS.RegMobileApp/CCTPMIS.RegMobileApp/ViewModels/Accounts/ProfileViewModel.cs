﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Interfaces;
using CCTPMIS.RegMobileApp.Models;
using Xamarin.Forms;
using CCTPMIS.RegMobileApp.Views;
using CCTPMIS.RegMobileApp.Views.Account;
using FormsToolkit;
using Settings = CCTPMIS.RegMobileApp.Helpers.Settings;

namespace CCTPMIS.RegMobileApp.ViewModels.Accounts
{
    public class ProfileViewModel : ViewModelBase
    {
        public ProfileViewModel(INavigation navigation) : base(navigation)
        {
            id = Settings.Current.EnumeratorId;
            if (Settings.Current.EnumeratorId > 0)
            {
                var data = App.Database.GetTableRow("Enumerator", "Id", id.ToString());
                Enumerator = (Enumerator)data;
            }
        }

        public Enumerator Enumerator { get; set; }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        //LogoutCommand
        private ICommand logoutCommand;

        public ICommand LogOutCommand =>
            logoutCommand ?? (logoutCommand = new Command(async () => await ExecuteLogoutAsync()));

        private async Task ExecuteLogoutAsync()
        {
            await Navigation.PushModalAsync(new LogoutPage());
        }

        private ICommand changePinCommand;

        public ICommand ChangePinCommand =>
            changePinCommand ?? (changePinCommand = new Command(async () => await ExecuteChangePinAsync()));

        private async Task ExecuteChangePinAsync()
        {
            await Navigation.PushModalAsync(new ChangePasswordPage());
        }
    }
}