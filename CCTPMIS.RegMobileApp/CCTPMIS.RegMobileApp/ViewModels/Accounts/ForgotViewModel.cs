﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Interfaces;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views;
using FormsToolkit;
using Xamarin.Forms;
using Settings = CCTPMIS.RegMobileApp.Helpers.Settings;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class ForgotViewModel : ViewModelBase
    {
        private ISSOClient client;

        public ForgotViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<ISSOClient>();
            email = Settings.Current.Email;
            password = Settings.Current.Password;
            //emailAddress = "sys@rmail.com";
            //nationalId = "9";
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string pin;

        public string Pin
        {
            get { return pin; }
            set { SetProperty(ref pin, value); }
        }

        private string currentPin;

        public string CurrentPin
        {
            get { return currentPin; }
            set { SetProperty(ref currentPin, value); }
        }

        private string newPin;

        public string NewPin
        {
            get { return newPin; }
            set { SetProperty(ref newPin, value); }
        }

        private string confirmNewPin;

        public string ConfirmNewPin
        {
            get { return confirmNewPin; }
            set { SetProperty(ref confirmNewPin, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private string nationalId;

        public string NationalId
        {
            get { return nationalId; }
            set { SetProperty(ref nationalId, value); }
        }

        private string emailAddress;

        public string EmailAddress
        {
            get { return emailAddress; }
            set { SetProperty(ref emailAddress, value); }
        }

        private ICommand forgotPinCommand;

        public ICommand ForgotPinCommand =>
            forgotPinCommand ?? (forgotPinCommand = new Command(async () => await ExecuteForgotAsync()));

        private async Task ExecuteForgotAsync()
        {
            if (IsBusy)
                return;

            if (string.IsNullOrWhiteSpace(EmailAddress))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your Email Address :-)",
                    Cancel = "OK"
                });
                return;
            }

            if (string.IsNullOrWhiteSpace(nationalId))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "National ID Number is empty!",
                    Cancel = "OK"
                });
                return;
            }

            try
            {
                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";

                AccountResponse result = null;
                ApiStatus actionResult = null;

                if (result == null)
                    result = await client.LoginAsync(email, password);

                if (result?.AccessToken != null)
                {
                    Settings.Current.AccessToken = result.AccessToken;
                    Settings.Current.AccessTokenExpirationDate = DateTime.Parse(result.ExpiresAt);
                    Settings.Current.AccessTokenIssuedDate = DateTime.Parse(result.IssuedAt);
                    Settings.Current.AccessTokenType = result.TokenType;
                    Message = "Secure Connection Established...";
                    try
                    {
                        // Now the Enumerator Stuff starts here
                        actionResult = await client.ForgotPin(nationalId, emailAddress, "");
                        if (actionResult != null)
                        {
                            if (actionResult.StatusId == -1)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Unable to Sign in.  ",
                                    Message = "We were not able to Reset your Password. Kindly  try Again",
                                    Cancel = "OK"
                                });
                            }
                            else
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Success",
                                    Message = " Kindly  Check your Email for Reset Instructions",
                                    Cancel = "OK"
                                });
                                IsBusy = false;
                                await Navigation.PopModalAsync(true);
                                return;
                            }
                        }
                        else
                        {
                            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                            {
                                Title = "Unable to Sign in.  ",
                                Message = "We were not able to Reset your Password. Kindly Try again",
                                Cancel = "OK"
                            });
                            IsBusy = false;
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Unable to Sign in.  ",
                            Message = ex.Message,
                            Cancel = "OK"
                        });
                        IsBusy = false;
                        return;
                    }
                }
                else
                {
                    // Logger.Track(MessageKeys.Error, "Reason", result.Error);
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sign in.",
                        Message = result.Error,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                //  Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sign in",
                    Message = "The email or password provided is incorrect.",
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;
        }

        //LogoutCommand
        private ICommand logoutCommand;

        public ICommand LogOutCommand =>
            logoutCommand ?? (logoutCommand = new Command(async () => await ExecuteLogoutAsync()));

        private async Task ExecuteLogoutAsync()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";

              //  await client.LogoutAsync();

                Settings.EnumeratorId = 0;
                Settings.AccessToken = null;
                Settings.LoggedIn = false;
                Settings.FirstRun = true;

                //await Navigation.PopToRootAsync();
                await Navigation.PushAsync(new LoginPage());

                await Navigation.PushModalAsync(new LoginPage());
            }
            catch (Exception ex)
            {
                //  Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Log Out" ,
                    Message = " " + ex?.Message,
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;
        }

        private ICommand changePinCommand;

        public ICommand ChangePinCommand =>
            changePinCommand ?? (changePinCommand = new Command(async () => await ExecuteChangePinAsync()));

        private async Task ExecuteChangePinAsync()
        {
            if (IsBusy)
                return;

            if (string.IsNullOrWhiteSpace(CurrentPin))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your Current PIN:-)",
                    Cancel = "OK"
                });
                return;
            }

            if (string.IsNullOrWhiteSpace(NewPin))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your New PIN  !",
                    Cancel = "OK"
                });
                return;
            }

            try
            {
                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";

                AccountResponse result = null;
                ApiStatus actionResult = null;

                if (result == null)
                    result = await client.LoginAsync(email, password);

                if (result?.AccessToken != null)
                {
                    Settings.Current.AccessToken = result.AccessToken;
                    Settings.Current.AccessTokenExpirationDate = DateTime.Parse(result.ExpiresAt);
                    Settings.Current.AccessTokenIssuedDate = DateTime.Parse(result.IssuedAt);
                    Settings.Current.AccessTokenType = result.TokenType;
                    Message = "Secure Connection Established...";
                    try
                    {
                        // Now the Enumerator Stuff starts here
                        actionResult = await client.ChangePin(currentPin, newPin, Settings.Current.EnumeratorId.ToString());
                        if (actionResult != null)
                        {
                            if (actionResult.StatusId == -1)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Unable to Sign in.  ",
                                    Message = "We were not able to Change your PIN. Kindly  try Again",
                                    Cancel = "OK"
                                });
                            }
                            else
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Success",
                                    Message = " Your PIN was Changed Successfully",
                                    Cancel = "OK"
                                });
                                IsBusy = false;
                                await Navigation.PopToRootAsync(true);
                                await Navigation.PushAsync(new LoginPage());
                            }
                        }
                        else
                        {
                            MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                            {
                                Title = "Unable to Sign in.  ",
                                Message = "We were not able to Reset your Password. Kindly  try again",
                                Cancel = "OK"
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Unable to Sign in.  ",
                            Message = ex.Message,
                            Cancel = "OK"
                        });
                        IsBusy = false;
                        return;
                    }
                }
                else
                {
                    // Logger.Track(MessageKeys.Error, "Reason", result.Error);
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sign in. Check your email and Password",
                        Message = result.Error,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                //  Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sign in",
                    Message = "The email or password provided is incorrect.",
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            IsBusy = false;
        }
    }
}