﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Interfaces;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Views;
using FormsToolkit;
using Plugin.Connectivity;
using Plugin.Share;
using Plugin.Share.Abstractions;
using Xamarin.Forms;
using Settings = CCTPMIS.RegMobileApp.Helpers.Settings;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        private ISSOClient client;

        public LoginViewModel(INavigation navigation) : base(navigation)
        {
            client = DependencyService.Get<ISSOClient>();
            email = Settings.Current.Email;
            password = Settings.Current.Password;
            //emailAddress = "sys@rmail.com";
            //nationalId = "9";
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private string pin;

        public string Pin
        {
            get { return pin; }
            set { SetProperty(ref pin, value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }

        private string nationalId;

        public string NationalId
        {
            get { return nationalId; }
            set { SetProperty(ref nationalId, value); }
        }

        private string emailAddress;

        public string EmailAddress
        {
            get { return emailAddress; }
            set { SetProperty(ref emailAddress, value); }
        }

        private ICommand loginCommand;

        public ICommand LoginCommand =>
            loginCommand ?? (loginCommand = new Command(async () => await ExecuteLoginAsync()));

        private async Task ExecuteLoginAsync()
        {
            if (IsBusy)
                return;

            if (string.IsNullOrWhiteSpace(nationalId))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "National ID Number is empty!",
                    Cancel = "OK"
                });
                return;
            }

            if (string.IsNullOrWhiteSpace(pin))
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Sign in Information",
                    Message = "We do need your PIN :-)",
                    Cancel = "OK"
                });
                return;
            }
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Login Failed",
                        Message = "It looks like you are offline. \n" +
                                  "Please check your internet connection and try again.",
                        Cancel = "OK"
                    });
                    return;
                }

                IsBusy = true;
                Message = "Establishing secure connection with the Main server ... ...";

                AccountResponse result = null;
                EnumeratorLoginResponse enumeratorResult = null;

                if (result == null)
                    result = await client.LoginAsync(email, password);

                if (result?.AccessToken != null)
                {
                    Settings.Current.AccessToken = result.AccessToken;
                    Settings.Current.AccessTokenExpirationDate = DateTime.Parse(result.ExpiresAt);
                    Settings.Current.AccessTokenIssuedDate = DateTime.Parse(result.IssuedAt);
                    Settings.Current.AccessTokenType = result.TokenType;

                    Message = "Secure Connection Established...";

                    try
                    {
                        // Now the Enumerator Stuff starts here
                        enumeratorResult = await client.LoginEnumerator(nationalId, pin,"");
                        if (enumeratorResult != null)
                        {
                            if (!string.IsNullOrEmpty(enumeratorResult.Error))
                            {
                                // Logger.Track(MessageKeys.Error, "Reason", enumeratorResult.Error);
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Unable to Sign in. Check your national id number and pin",
                                    Message = enumeratorResult.Error,
                                    Cancel = "OK"
                                });
                                return;
                            }
                            else if (enumeratorResult.Locations == null)
                            {
                                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                                {
                                    Title = "Unable to Sign in.  Even though we were able to Log you In, It seems that your locations have not been assigned.",
                                    Message = enumeratorResult.Error,
                                    Cancel = "OK"
                                });
                                return;
                            }
                            else
                            {
                                //now Load into the Tables
                                App.Database.AddOrUpdate(enumeratorResult.Enumerator);
                                foreach (var item in enumeratorResult.SystemCodes)
                                    App.Database.AddOrUpdate(item);
                                foreach (var item in enumeratorResult.SystemCodeDetails)
                                    App.Database.AddOrUpdate(item);
                                foreach (var item in enumeratorResult.Locations)
                                    App.Database.AddOrUpdate(item);
                                foreach (var item in enumeratorResult.SubLocations)
                                    App.Database.AddOrUpdate(item);
                                foreach (var item in enumeratorResult.Programmes)
                                    App.Database.AddOrUpdate(item);

                                foreach (var item in enumeratorResult.EnumeratorLocations)
                                    App.Database.AddOrUpdate(item);

                                Settings.FirstName = enumeratorResult?.Enumerator.FirstName ?? string.Empty;
                                Settings.LastName = enumeratorResult?.Enumerator.Surname ?? string.Empty;
                                Settings.Email = enumeratorResult?.Enumerator.Email.ToLowerInvariant();
                                Settings.EnumeratorId = enumeratorResult?.Enumerator.Id ?? 0;
                                Settings.Current.LastSyncDown = DateTime.UtcNow;
                                Settings.Current.HasSyncedDataDownwards = true;

                                Settings.FirstRun = false;
                                IsBusy = false;
                                await Finish();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                        {
                            Title = "Unable to Sign in.  ",
                            Message = ex.Message,
                            Cancel = "OK"
                        });
                        IsBusy = false;
                        return;
                    }
                }
                else
                {
                    // Logger.Track(MessageKeys.Error, "Reason", result.Error);
                    MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                    {
                        Title = "Unable to Sign in. Check your national id and pin",
                        Message = result.Error,
                        Cancel = "OK"
                    });
                }
            }
            catch (Exception ex)
            {
                //  Logger.Track(MessageKeys.Error, "Reason", ex?.Message ?? string.Empty);

                MessagingService.Current.SendMessage<MessagingServiceAlert>(MessageKeys.Message, new MessagingServiceAlert
                {
                    Title = "Unable to Sign in",
                    Message = "The national id or pin provided is incorrect.",
                    Cancel = "OK"
                });
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
        }

        private ICommand signupCommand;

        public ICommand SignupCommand =>
            signupCommand ?? (signupCommand = new Command(async () => await ExecuteSignupAsync()));

        private async Task ExecuteSignupAsync()
        {
            //  Logger.Track(ApiKeys.Signup);
            await CrossShare.Current.OpenBrowser("https://www.inuajamii.go.ke:2010/", new BrowserOptions
                {
                    ChromeShowTitle = true,
                    ChromeToolbarColor = new ShareColor
                    {
                        A = 255,
                        R = 118,
                        G = 53,
                        B = 235
                    },
                    UseSafariReaderMode = false,
                    UseSafariWebViewController = true
                });
        }

        private ICommand recoverPinCommand;

        public ICommand RecoverPinCommand =>
            recoverPinCommand ?? (recoverPinCommand = new Command(async () => await ExecuteRecoverPinPageAsync()));

        private async Task ExecuteRecoverPinPageAsync()
        {
            Settings.FirstRun = true;

            await Navigation.PushModalAsync(new ForgotPasswordPage());
        }

        private async Task Finish()
        {
            if (Device.RuntimePlatform == Device.iOS && Settings.FirstRun)
            {
                var push = DependencyService.Get<IPushNotifications>();
                if (push != null)
                    await push.RegisterForNotifications();

                await Navigation.PopModalAsync();
            }
            else
            {
                await Navigation.PopModalAsync();
            }
        }
    }
}