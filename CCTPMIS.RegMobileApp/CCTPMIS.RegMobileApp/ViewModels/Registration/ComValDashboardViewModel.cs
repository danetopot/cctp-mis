﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Extensions;
using CCTPMIS.RegMobileApp.Models;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Views.Registration;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class ComValDashboardViewModel : ViewModelBase
    {
      //  public INavigation Navigation;

        public ComValDashboardViewModel(INavigation navigation) : base(navigation)
        {
            NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
            //   ComValListingPlanHHsFiltered.ReplaceRange(ComValListingPlanHHsFiltered.Get());
            ComValListingPlanHHs.ReplaceRange(GetComValListingPlanHHs());
            ComValListingPlanHHExceptions.ReplaceRange(GetComValListingPlanHHsExceptions());
            ComValListingPlanHHReady.ReplaceRange(GetComValListingPlanHHReady());
         //   Navigation = navigation;
        }

        public ObservableCollection<ComValListingPlanHH> GetComValListingPlanHHs()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH) item;
                if(string.IsNullOrEmpty(hhitem.ComValDate) && hhitem.CgMatches==1 && (hhitem.BeneMatches ==1 || hhitem.HasBene == 0 ))
                hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableCollection<ComValListingPlanHH> GetComValListingPlanHHsExceptions()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH)item;
                if (string.IsNullOrEmpty(hhitem.ComValDate) && (hhitem.CgMatches == 0 || (hhitem.BeneMatches == 0 && hhitem.HasBene == 1)))
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableCollection<ComValListingPlanHH> GetComValListingPlanHHReady()
        {
            var items = App.Database.GetTable("ComValListingPlanHH");
            var hh = new ObservableCollection<ComValListingPlanHH>();
            foreach (var item in items)
            {
                var hhitem = (ComValListingPlanHH)item;
                if (!string.IsNullOrEmpty(hhitem.ComValDate))
                    hh.Add(hhitem);
            }
            return hh;
        }

        public ObservableRangeCollection<ComValListingPlanHH> ComValListingPlanHHs { get; } = new ObservableRangeCollection<ComValListingPlanHH>();
        public ObservableRangeCollection<ComValListingPlanHH> ComValListingPlanHHExceptions { get; } = new ObservableRangeCollection<ComValListingPlanHH>();
        public ObservableRangeCollection<ComValListingPlanHH> ComValListingPlanHHReady { get; } = new ObservableRangeCollection<ComValListingPlanHH>();

        public ObservableRangeCollection<ComValListingPlanHH> ComValListingPlanHHsFiltered { get; } = new ObservableRangeCollection<ComValListingPlanHH>();
        public ObservableRangeCollection<Grouping<string, ComValListingPlanHH>> ComValListingPlanHHsGrouped { get; } = new ObservableRangeCollection<Grouping<string, ComValListingPlanHH>>();
        public DateTime NextForceRefresh { get; set; }

        #region Properties

        private ComValListingPlanHH selectedRegistration;

        public ComValListingPlanHH SelectedRegistration
        {
            get { return selectedRegistration; }
            set
            {
                selectedRegistration = value;
                OnPropertyChanged();
                if (selectedRegistration == null)
                    return;

                MessagingService.Current.SendMessage(MessageKeys.NavigateToRegistration, selectedRegistration);

                SelectedRegistration = null;
            }
        }

        private string filter = string.Empty;

        public string Filter
        {
            get { return filter; }
            set
            {
                if (SetProperty(ref filter, value))
                    ExecuteFilterComValListingPlanHHsAsync();
            }
        }

        #endregion Properties

        #region Filtering and Sorting

        private void SortComValListingPlanHHs()
        {
        }

        private bool noComValListingPlanHHsFound;

        public bool NoComValListingPlanHHsFound
        {
            get { return noComValListingPlanHHsFound; }
            set { SetProperty(ref noComValListingPlanHHsFound, value); }
        }

        private string noComValListingPlanHHsFoundMessage;

        public string NoComValListingPlanHHsFoundMessage
        {
            get { return noComValListingPlanHHsFoundMessage; }
            set { SetProperty(ref noComValListingPlanHHsFoundMessage, value); }
        }

        #endregion Filtering and Sorting

        #region Commands

        private ICommand forceRefreshCommand;

        public ICommand ForceRefreshCommand =>
            forceRefreshCommand ?? (forceRefreshCommand = new Command(async () => await ExecuteForceRefreshCommandAsync()));

        private async Task ExecuteForceRefreshCommandAsync()
        {
            await ExecuteLoadComValListingPlanHHsAsync(true);
        }

        private ICommand filterComValListingPlanHHsCommand;

        public ICommand FilterComValListingPlanHHsCommand =>
            filterComValListingPlanHHsCommand ?? (filterComValListingPlanHHsCommand = new Command(async () => await ExecuteFilterComValListingPlanHHsAsync()));

        private async Task ExecuteFilterComValListingPlanHHsAsync()
        {
            IsBusy = true;
            NoComValListingPlanHHsFound = false;

            // Abort the current command if the query has changed and is not empty
            if (!string.IsNullOrEmpty(Filter))
            {
                var query = Filter;
                await Task.Delay(250);
                if (query != Filter)
                    return;
            }

            // ComValListingPlanHHsFiltered.ReplaceRange(ComValListingPlanHHs.Search(Filter));
            SortComValListingPlanHHs();

            if (ComValListingPlanHHsGrouped.Count == 0)
            {
                //if (Settings.Current.FavoritesOnly)
                //{
                //    if (!Settings.Current.ShowPastComValListingPlanHHs && !string.IsNullOrWhiteSpace(Filter))
                //        NoComValListingPlanHHsFoundMessage = "You haven't favorited\nany ComValListingPlanHHs yet.";
                //    else
                //        NoComValListingPlanHHsFoundMessage = "No ComValListingPlanHHs Found";
                //}
                //else
                NoComValListingPlanHHsFoundMessage = "No ComValListingPlanHHs Found";

                NoComValListingPlanHHsFound = true;
            }
            else
            {
                NoComValListingPlanHHsFound = false;
            }

            IsBusy = false;
        }

        private ICommand loadComValListingPlanHHsCommand;

        public ICommand LoadComValListingPlanHHsCommand =>
            loadComValListingPlanHHsCommand ?? (loadComValListingPlanHHsCommand = new Command<bool>(async (f) => await ExecuteLoadComValListingPlanHHsAsync()));

        private async Task<bool> ExecuteLoadComValListingPlanHHsAsync(bool force = false)
        {
            //if (IsBusy)
            //    return false;

            try
            {
                NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
                IsBusy = true;
                NoComValListingPlanHHsFound = false;
                Filter = string.Empty;
                var reg = GetComValListingPlanHHs();
                ComValListingPlanHHs.ReplaceRange(reg);

#if DEBUG
                await Task.Delay(1000);
#endif

                ComValListingPlanHHs.ReplaceRange(GetComValListingPlanHHs());

                ComValListingPlanHHsFiltered.ReplaceRange(ComValListingPlanHHs);
                SortComValListingPlanHHs();

                if (ComValListingPlanHHsGrouped.Count == 0)
                {
                    if (Settings.Current.FavoritesOnly)
                    {
                        //if (!Settings.Current.ShowPastComValListingPlanHHs)
                        //    NoComValListingPlanHHsFoundMessage = "You haven't favorited\nany ComValListingPlanHHs yet.";
                        //else
                        //    NoComValListingPlanHHsFoundMessage = "No ComValListingPlanHHs Found";
                    }
                    else
                        NoComValListingPlanHHsFoundMessage = "No ComValListingPlanHHs Found";

                    NoComValListingPlanHHsFound = true;
                }
                else
                {
                    NoComValListingPlanHHsFound = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Report(ex, "Method", "ExecuteLoadComValListingPlanHHsAsync");
                MessagingService.Current.SendMessage(MessageKeys.Error, ex);
            }
            finally
            {
                IsBusy = false;
            }

            return true;
        }

        private ICommand favoriteCommand;

        public ICommand FavoriteCommand =>
            favoriteCommand ?? (favoriteCommand = new Command<ComValListingPlanHH>(async (s) => await ExecuteFavoriteCommandAsync(s)));

        private async Task ExecuteFavoriteCommandAsync(ComValListingPlanHH ComValListingPlanHH)
        {
            //var toggled = await FavoriteService.ToggleFavorite(registration);
            //if (toggled && Settings.Current.FavoritesOnly)
            //    SortComValListingPlanHHs();
        }

        #endregion Commands

    //    private readonly INavigation navigation;

        #region Properties

        private ComValListingPlanHH selectedComValListingPlanHH;

        public ComValListingPlanHH SelectedComValListingPlanHH
        {
            get { return selectedComValListingPlanHH; }
            set
            {
                selectedComValListingPlanHH = value;
                OnPropertyChanged();
                if (selectedComValListingPlanHH != null)
                {
                    Navigation.PushAsync(new CvRegistrationDetailsPage(selectedComValListingPlanHH.Id));
                    return;
                }
            }
        }

        private ComValListingPlanHH selectedException;

        public ComValListingPlanHH SelectedException
        {
            get { return selectedException; }
            set
            {
                selectedException = value;
                OnPropertyChanged();
                if (selectedException != null)
                {
                    Navigation.PushAsync(new ComValExceptionDetailsPage(selectedException.Id));
                    return;
                }
            }
        }

        private ComValListingPlanHH selectedReady;

        public ComValListingPlanHH SelectedReady
        {
            get { return selectedReady; }
            set
            {
                selectedReady = value;
                OnPropertyChanged();
                if (selectedReady != null)
                {
                    Navigation.PushAsync(new ComValReadyDetailsPage(selectedException.Id));
                    return;
                }
            }
        }

        #endregion Properties
    }
}