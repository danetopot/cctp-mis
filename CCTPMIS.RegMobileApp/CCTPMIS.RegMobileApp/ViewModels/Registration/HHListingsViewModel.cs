﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Extensions;
using CCTPMIS.RegMobileApp.Models;
using FormsToolkit;
using MvvmHelpers;
using Xamarin.Forms;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Views.Registration;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class HHListingsViewModel : ViewModelBase
    {
        public HHListingsViewModel(INavigation navigation) : base(navigation)
        {
            NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
            Registrations.AddRange(GetRegistrations());
            SortRegistrations();
        }

        public ObservableCollection<Listing> GetRegistrations()
        {
            var items = App.Database.GetTable("Listing");
            var hh = new ObservableCollection<Listing>();
            foreach (var item in items)
                hh.Add((Listing)item);
            return hh;
        }

        public ObservableRangeCollection<Listing> Registrations { get; } = new ObservableRangeCollection<Listing>();
        public ObservableRangeCollection<Listing> RegistrationsFiltered { get; } = new ObservableRangeCollection<Listing>();
        public ObservableRangeCollection<Grouping<string, Listing>> RegistrationsGrouped { get; } = new ObservableRangeCollection<Grouping<string, Listing>>();
        public DateTime NextForceRefresh { get; set; }

        #region Properties

        private string filter = string.Empty;

        public string Filter
        {
            get { return filter; }
            set
            {
                if (SetProperty(ref filter, value))
                    ExecuteFilterRegistrationsAsync();
            }
        }

        #endregion Properties

        #region Filtering and Sorting

        private void SortRegistrations()
        {
        }

        private bool noRegistrationsFound;

        public bool NoRegistrationsFound
        {
            get { return noRegistrationsFound; }
            set { SetProperty(ref noRegistrationsFound, value); }
        }

        private string noRegistrationsFoundMessage;

        public string NoRegistrationsFoundMessage
        {
            get { return noRegistrationsFoundMessage; }
            set { SetProperty(ref noRegistrationsFoundMessage, value); }
        }

        #endregion Filtering and Sorting

        #region Properties

        private Listing selectedRegistration;

        public Listing SelectedRegistration
        {
            get { return selectedRegistration; }
            set
            {
                selectedRegistration = value;
                OnPropertyChanged();
                if (selectedRegistration != null)
                {
                    Navigation.PushAsync(new RegistrationDetailsPage(selectedRegistration.LocalId));
                    return;
                }
            }
        }

        #endregion Properties

        #region Commands

        private ICommand forceRefreshCommand;

        public ICommand ForceRefreshCommand =>
            forceRefreshCommand ?? (forceRefreshCommand = new Command(async () => await ExecuteForceRefreshCommandAsync()));

        private async Task ExecuteForceRefreshCommandAsync()
        {
            await ExecuteLoadRegistrationsAsync(true);
        }

        private ICommand filterRegistrationsCommand;

        public ICommand FilterRegistrationsCommand =>
            filterRegistrationsCommand ?? (filterRegistrationsCommand = new Command(async () => await ExecuteFilterRegistrationsAsync()));

        private async Task ExecuteFilterRegistrationsAsync()
        {
            IsBusy = true;
            NoRegistrationsFound = false;

            // Abort the current command if the query has changed and is not empty
            if (!string.IsNullOrEmpty(Filter))
            {
                var query = Filter;
                await Task.Delay(250);
                if (query != Filter)
                    return;
            }

            // RegistrationsFiltered.ReplaceRange(Registrations.Search(Filter));
            //  RegistrationsFiltered.ReplaceRange(Registrations.Search(Filter));
            SortRegistrations();

            if (RegistrationsGrouped.Count == 0)
            {
                if (Settings.Current.FavoritesOnly)
                {
                    if (!Settings.Current.ShowPastRegistrations && !string.IsNullOrWhiteSpace(Filter))
                        NoRegistrationsFoundMessage = "You haven't favorited\nany Registrations yet.";
                    else
                        NoRegistrationsFoundMessage = "No Registrations Found";
                }
                else
                    NoRegistrationsFoundMessage = "No Registrations Found";
                NoRegistrationsFound = true;
            }
            else
            {
                NoRegistrationsFound = false;
            }

            IsBusy = false;
        }

        private ICommand loadNewHouseHoldCommand;
        public ICommand LoadNewHouseHoldCommand =>   loadNewHouseHoldCommand ?? (loadNewHouseHoldCommand      = new Command(async () =>  await ExecuteLoadNewRegistrationPageAsync()));

        private async Task ExecuteLoadNewRegistrationPageAsync()
        {
            try
            {
                await Navigation.PushAsync(new NewHHListingPage());
            }
            catch (Exception ex)
            {
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Try Again!!",
                        Message = ex.Message,
                        Cancel = "OK"
                    });
            }
        }

        private ICommand loadRegistrationsCommand;
        public ICommand LoadRegistrationsCommand => loadRegistrationsCommand ?? (loadRegistrationsCommand = new Command<bool>(async (s) => await ExecuteLoadRegistrationsAsync(s)));

        private async Task<bool> ExecuteLoadRegistrationsAsync(bool force = false)
        {
            //if (IsBusy)
            //    return false;

            try
            {
                NextForceRefresh = DateTime.UtcNow.AddMinutes(45);
                IsBusy = true;
                NoRegistrationsFound = false;
                Filter = string.Empty;

                Registrations.ReplaceRange(GetRegistrations());

#if DEBUG
                await Task.Delay(1000);
#endif

                Registrations.ReplaceRange(GetRegistrations());

                RegistrationsFiltered.ReplaceRange(Registrations);
                SortRegistrations();

                if (RegistrationsGrouped.Count == 0)
                {
                    if (Settings.Current.FavoritesOnly)
                    {
                        if (!Settings.Current.ShowPastRegistrations)
                            NoRegistrationsFoundMessage = "You haven't favorited\nany Registrations yet.";
                        else
                            NoRegistrationsFoundMessage = "No Registrations Found";
                    }
                    else
                        NoRegistrationsFoundMessage = "No Registrations Found";

                    NoRegistrationsFound = true;
                }
                else
                {
                    NoRegistrationsFound = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Report(ex, "Method", "ExecuteLoadRegistrationsAsync");
                MessagingService.Current.SendMessage(MessageKeys.Error, ex);
            }
            finally
            {
                IsBusy = false;
            }

            return true;
        }

        private ICommand favoriteCommand;

        public ICommand FavoriteCommand =>
            favoriteCommand ?? (favoriteCommand = new Command<Listing>(async (s) => await ExecuteFavoriteCommandAsync(s)));

        private async Task ExecuteFavoriteCommandAsync(Listing registration)
        {
            //var toggled = await FavoriteService.ToggleFavorite(registration);
            //if (toggled && Settings.Current.FavoritesOnly)
            //    SortRegistrations();
        }

        #endregion Commands
    }
}