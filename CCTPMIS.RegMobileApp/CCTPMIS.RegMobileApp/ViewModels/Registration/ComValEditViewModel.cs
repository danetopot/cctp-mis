﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.ViewModels.Validator;
using CCTPMIS.RegMobileApp.Views.Registration;
using FluentValidation;
using FormsToolkit;
using MvvmHelpers;
using Plugin.Geolocator;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class CommunityValEditsViewModel : ViewModelBase
    {
        public ObservableRangeCollection<SystemCodeDetail> CgSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> BeneSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<Location> Locations = new ObservableRangeCollection<Location>();
        public ObservableRangeCollection<Programme> Programmes = new ObservableRangeCollection<Programme>();
        public ObservableRangeCollection<SubLocation> SubLocations = new ObservableRangeCollection<SubLocation>();
        private ICommand _saveRegistrationCommand;
        private Programme _selectedProgramme;
        private Location _selectedLocation;
        private SubLocation _selectedSubLocation;
        private SystemCodeDetail _selectedBeneSex;
        private SystemCodeDetail _selectedCgSex;
        public INavigation Navigation;

        public CommunityValEditsViewModel(INavigation navigation, int id) : base(navigation)
        {
            Registration = (ComValListingPlanHH)App.Database.GetTableRow("ComValListingPlanHH", "Id", id.ToString());
            var codes = App.Database.SystemCodeDetailsGetByCode("Sex");
            LoadedCgSex.AddRange(codes);
            LoadedBeneSex.AddRange(codes);

            LoadedLocations.AddRange(App.Database.LocationsGetAll());
            LoadedSubLocations.AddRange(App.Database.SubLocationGetByLocationId(0));

            Registration.CgDoBDate = DateFormatter.ToEnDateTimeString(Registration.CgDoB);
            if (!string.IsNullOrEmpty(Registration.BeneDoBDate))
                Registration.BeneDoBDate = DateFormatter.ToEnDateTimeString(Registration.BeneDoB);

            var codesProgrammes = App.Database.GetTable("Programme");
            foreach (var item in codesProgrammes)
                LoadedProgrammes.Add((Programme)item);

            SelectedProgramme = LoadedProgrammes.Single(x => x.Id == Registration.ProgrammeId);
            IsShowBene = SelectedProgramme.Code == "OPTC";

            SelectedLocation = LoadedLocations.Single(x => x.Id == Registration.LocationId);
            SelectedSubLocation = LoadedSubLocations.Single(x => x.Id == Registration.SubLocationId);
            SelectedCgSex = LoadedCgSex.Single(x => x.Id == Registration.CgSexId);

            if (Registration.BeneSexId>0)
                SelectedBeneSex = LoadedBeneSex.Single(x => x.Id == Registration.BeneSexId);
            Navigation = navigation;

            _validator = new ComValListingValidator();
        }
        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private bool isShowBene;
        private readonly IValidator _validator;
        public bool IsShowBene
        {
            get { return isShowBene; }
            set { SetProperty(ref isShowBene, value); }
        }

        public ComValListingPlanHH Registration { get; set; }

        public int Id { get; set; }

        public DateTime NextForceSync { get; set; }

        public Location SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (_selectedLocation == value) return;
                _selectedLocation = value;
                SubLocations.Clear();
                SubLocations.AddRange(App.Database.SubLocationGetByLocationId(_selectedLocation.Id));
            }
        }

        public Programme SelectedProgramme
        {
            get => _selectedProgramme;
            set
            {
                if (this._selectedProgramme == value) return;
                this._selectedProgramme = value;
                IsShowBene = _selectedProgramme.Code == "OPCT";
                this.OnPropertyChanged();
            }
        }

        public SubLocation SelectedSubLocation
        {
            get => _selectedSubLocation;
            set
            {
                if (_selectedSubLocation == value) return;
                _selectedSubLocation = value;

                // OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedCgSex
        {
            get => _selectedCgSex;
            set
            {
                if (this._selectedCgSex == value) return;
                this._selectedCgSex = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedBeneSex
        {
            get => _selectedBeneSex;
            set
            {
                if (this._selectedBeneSex == value) return;
                this._selectedBeneSex = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand SaveRegistrationCommand => _saveRegistrationCommand ?? (_saveRegistrationCommand = new Command(async () => await ExecuteSaveHouseHold()));

        public ObservableRangeCollection<Location> LoadedLocations
        {
            get => Locations;
            set => SetProperty(ref Locations, value);
        }

        public ObservableRangeCollection<SubLocation> LoadedSubLocations
        {
            get => SubLocations;
            set => SetProperty(ref SubLocations, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedCgSex
        {
            get => CgSex;
            set => SetProperty(ref CgSex, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedBeneSex
        {
            get => BeneSex;
            set => SetProperty(ref BeneSex, value);
        }

        public ObservableRangeCollection<Programme> LoadedProgrammes
        {
            get => Programmes;
            set => SetProperty(ref Programmes, value);
        }

        private async Task ExecuteSaveHouseHold()
        {
            try
            {
                var reg = this.Registration;
                reg.ComValDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now); ;
                Message = "Validating .. ";

                var errorMessage = "";

                if (IsShowBene)
                {
                    if (string.IsNullOrEmpty(reg.BeneFirstName))
                        errorMessage += "Beneficiary FirstName is required \n";
                    if (string.IsNullOrEmpty(reg.BeneSurname))
                        errorMessage += "Beneficiary Surname is required \n";
                    if (string.IsNullOrEmpty(reg.BeneNationalIdNo))
                        errorMessage += "Beneficiary NationalId No is required \n";
                    if (SelectedBeneSex == null)
                        errorMessage += "Beneficiary Sex is required \n";
                }

                var validationResult = _validator.Validate(reg);
                if (validationResult.IsValid && errorMessage == "")
                {
                    reg.StatusId = App.Database.SystemCodeDetailGetByCode("Registration Status", "REGCORRECT").Id;
                    reg.EnumeratorId = Settings.Current.EnumeratorId;
                    App.Database.AddOrUpdate(reg);
                    await Navigation.PopToRootAsync(true);
                    await Navigation.PushAsync(new ComValDashboardPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult);


                    if (errorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Correct the Data and Try Again!!",
                                Message = errorMessage,
                                Cancel = "OK"
                            });

                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        IsBusy = false;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }
    }
}