﻿using CCTPMIS.RegMobileApp.Views.Registration;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    using System.Collections.Generic;

    using CCTPMIS.RegMobileApp.Data;
    using CCTPMIS.RegMobileApp.Models;

    public class HHListingDetailViewModel : ViewModelBase
    {
        public HHListingDetailViewModel(INavigation navigation, int id) : base(navigation)
        {
            var data = App.Database.GetTableRow("Listing", "LocalId", id.ToString());
            Registration = (Listing)data;
            Registration.Programme = (Programme)App.Database.GetTableRow("Programme", "id", Registration.ProgrammeId.ToString());
            Registration.SubLocation = (SubLocation)App.Database.GetTableRow("SubLocation", "id", Registration.SubLocationId.ToString());
            Registration.Location = (Location)App.Database.GetTableRow("Location", "id", Registration.LocationId.ToString());
            Registration.CgSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Registration.CgSexId.ToString());
            if(Registration.BeneSexId.HasValue)
            Registration.BeneSex = (SystemCodeDetail)App.Database.GetTableRow("SystemCodeDetail", "id", Registration.BeneSexId.ToString());
        }

        public Listing Registration { get; set; }
        public int Id { get; set; }
    }
}