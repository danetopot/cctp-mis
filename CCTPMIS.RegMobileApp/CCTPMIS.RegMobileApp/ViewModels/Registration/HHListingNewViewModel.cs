﻿using FormsToolkit;
using Plugin.Geolocator;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Data;
using CCTPMIS.RegMobileApp.Models;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Validations;
using CCTPMIS.RegMobileApp.ViewModels.Validator;
using CCTPMIS.RegMobileApp.Views.Registration;
using FluentValidation;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.ViewModels
{
    public class HHListingNewViewModel : ViewModelBase
    {
        /* Observable range of collections */
        public ObservableRangeCollection<SystemCodeDetail> CgSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<SystemCodeDetail> BeneSex = new ObservableRangeCollection<SystemCodeDetail>();
        public ObservableRangeCollection<Location> Locations = new ObservableRangeCollection<Location>();
        public ObservableRangeCollection<Programme> Programmes = new ObservableRangeCollection<Programme>();
        public ObservableRangeCollection<SubLocation> SubLocations = new ObservableRangeCollection<SubLocation>();
        private ICommand _saveHouseHoldCommand;
        private Programme _selectedProgramme;
        private Location _selectedLocation;
        private SubLocation _selectedSubLocation;
        private SystemCodeDetail _selectedBeneSex;
        private SystemCodeDetail _selectedCgSex;

        private bool isShowBene;

        public bool IsShowBene
        {
            get { return isShowBene; }
            set { SetProperty(ref isShowBene, value); }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        public HHListingNewViewModel(INavigation navigation, Listing registration = null) : base(navigation)
        {
            Registration = new Listing();
            NextForceSync = DateTime.UtcNow.AddMinutes(45);

            LoadedLocations.AddRange(App.Database.LocationsGetByEnumerator(Settings.EnumeratorId));
            LoadedSubLocations.AddRange(App.Database.SubLocationGetByLocationId(0));

            var codes = App.Database.SystemCodeDetailsGetByCode("Sex");
            LoadedCgSex.AddRange(codes);
            LoadedBeneSex.AddRange(codes);

            var codesProgrammes = App.Database.GetTable("Programme");
            foreach (var item in codesProgrammes)
                LoadedProgrammes.Add((Programme)item);

            Registration.StartTime = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
            Registration.EnumeratorId = Settings.EnumeratorId;
            isShowBene = false;

            _validator = new ListingValidator();
        }

        private readonly IValidator _validator;
        public DateTime NextForceSync { get; set; }
        public Listing Registration { get; set; }

        public Location SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (_selectedLocation == value) return;
                _selectedLocation = value;
                SubLocations.Clear();
                SubLocations.AddRange(App.Database.SubLocationGetByLocationId(_selectedLocation.Id));
            }
        }

        public GridLength Height { get; set; }

        public Programme SelectedProgramme
        {
            get => _selectedProgramme;
            set
            {
                if (this._selectedProgramme == value) return;
                this._selectedProgramme = value;
                IsShowBene = _selectedProgramme.Code == "OPCT";
                this.OnPropertyChanged();
            }
        }

        public SubLocation SelectedSubLocation
        {
            get => _selectedSubLocation;
            set
            {
                if (_selectedSubLocation == value) return;
                _selectedSubLocation = value;

                // OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedCgSex
        {
            get => _selectedCgSex;
            set
            {
                if (this._selectedCgSex == value) return;
                this._selectedCgSex = value;
                this.OnPropertyChanged();
            }
        }

        public SystemCodeDetail SelectedBeneSex
        {
            get => _selectedBeneSex;
            set
            {
                if (this._selectedBeneSex == value) return;
                this._selectedBeneSex = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand SaveHouseHoldCommand => _saveHouseHoldCommand ?? (_saveHouseHoldCommand = new Command(async () => await ExecuteSaveHouseHold()));

        public ObservableRangeCollection<Location> LoadedLocations
        {
            get => Locations;
            set => SetProperty(ref Locations, value);
        }

        public ObservableRangeCollection<SubLocation> LoadedSubLocations
        {
            get => SubLocations;
            set => SetProperty(ref SubLocations, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedCgSex
        {
            get => CgSex;
            set => SetProperty(ref CgSex, value);
        }

        public ObservableRangeCollection<SystemCodeDetail> LoadedBeneSex
        {
            get => BeneSex;
            set => SetProperty(ref BeneSex, value);
        }

        public ObservableRangeCollection<Programme> LoadedProgrammes
        {
            get => Programmes;
            set => SetProperty(ref Programmes, value);
        }

        private async Task ChangeBeneLayout(bool itIs)
        {
            if (IsBusy)
                return;
            try
            {
                IsShowBene = itIs;
            }
            catch (Exception e)
            {
            }
        }

        private async Task ExecuteSaveHouseHold()
        {
            if (IsBusy)
                return;
            try
            {
                var reg = this.Registration;
                reg.EndTime = DateFormatter.ToSQLiteDateTimeString(DateTime.Now); ;

                var errorMessage = "";

                if (reg.Years == 0 && reg.Months == 0)
                    errorMessage += "Check the Duration Years and Months. \n";

                if (SelectedProgramme != null)
                    reg.ProgrammeId = SelectedProgramme.Id;
                if (SelectedLocation != null)
                    reg.LocationId = SelectedLocation.Id;
                if (SelectedSubLocation != null)
                    reg.SubLocationId = SelectedSubLocation.Id;
                if (SelectedBeneSex != null)
                    reg.BeneSexId = SelectedBeneSex.Id;
                if (SelectedCgSex != null)
                    reg.CgSexId = SelectedCgSex.Id;

                if (IsShowBene)
                {
                    if (string.IsNullOrEmpty(reg.BeneFirstName))
                        errorMessage += "Beneficiary FirstName is required \n";
                    if (string.IsNullOrEmpty(reg.BeneSurname))
                        errorMessage += "Beneficiary Surname is required \n";
                    if (string.IsNullOrEmpty(reg.BeneNationalIdNo))
                        errorMessage += "Beneficiary NationalId No is required \n";
                    if (SelectedBeneSex == null)
                        errorMessage += "Beneficiary Sex is required \n";
                }

                var validationResult = _validator.Validate(reg);
                if (validationResult.IsValid && errorMessage == "")
                {
                    IsBusy = true;
                    Message = "Validating .. ";
                    Message = "Getting Location Co-ordinates .. ";

                    var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromMinutes(10));

                    reg.EndTime = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                    reg.RegistrationDate = DateFormatter.ToSQLiteDateTimeString(DateTime.Now);
                    reg.UniqueId = Guid.NewGuid().ToString();

                    reg.Latitude = position.Latitude;
                    reg.Longitude = position.Longitude;

                    if (IsShowBene)
                        reg.BeneDoB = DateFormatter.ToSQLiteDateTimeString(reg.BeneDoBDate);
                    reg.CgDoB = DateFormatter.ToSQLiteDateTimeString(reg.CgDoBDate);
                    reg.EnumeratorId = Settings.Current.EnumeratorId;

                    Message = "Saving to Database Locally .. ";

                    App.Database.AddOrUpdate(reg);
                    IsBusy = false;
                    await Navigation.PopToRootAsync(true);
                    await Navigation.PushAsync(new HHListingsDashboardPage());
                }
                else
                {
                    ValidateMessage = GetErrorListFromValidationResult(validationResult);

                    if (errorMessage.Length > 0 || ValidateMessage.Length > 0)
                    {
                        MessagingService.Current.SendMessage<MessagingServiceAlert>(
                            MessageKeys.Error,
                            new MessagingServiceAlert
                            {
                                Title = "Please Check the Data and Try Again!!",
                                Message = $"{ValidateMessage}\n{errorMessage}",
                                Cancel = "OK"
                            });
                        ValidateMessage = $"{ValidateMessage}\n{errorMessage}";
                        IsBusy = false;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                MessagingService.Current.SendMessage<MessagingServiceAlert>(
                    MessageKeys.Error,
                    new MessagingServiceAlert
                    {
                        Title = "Please Correct the Data and Try Again!!",
                        Message = e.Message,
                        Cancel = "OK"
                    });
                return;
            }
            finally
            {
                Message = string.Empty;
                IsBusy = false;
            }
            return;
        }
    }
}