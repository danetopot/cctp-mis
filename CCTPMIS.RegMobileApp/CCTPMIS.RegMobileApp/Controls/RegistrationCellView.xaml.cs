﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CCTPMIS.RegMobileApp.Controls
{
    public class SessionCell : ViewCell
    {
        private readonly INavigation navigation;

        public SessionCell(INavigation navigation = null)
        {
            Height = 120;
            View = new RegistrationCellView();
            this.navigation = navigation;
        }

        protected override async void OnTapped()
        {
            base.OnTapped();
            if (navigation == null)
                return;
            var session = BindingContext as Listing;
            if (session == null)
                return;
          }
    }

    //[XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class RegistrationCellView : ContentView
    {
        public RegistrationCellView()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty FavoriteCommandProperty =
            BindableProperty.Create(nameof(FavoriteCommand), typeof(ICommand), typeof(RegistrationCellView), default(ICommand));

        public ICommand FavoriteCommand
        {
            get { return GetValue(FavoriteCommandProperty) as Command; }
            set { SetValue(FavoriteCommandProperty, value); }
        }
    }
}