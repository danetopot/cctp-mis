﻿namespace CCTPMIS.RegMobileApp.Controls
{
    using Xamarin.Forms;

    public class CardView : Frame
    {
        public CardView()
        {
            Padding = 10;
            if (Device.RuntimePlatform != Device.iOS && Device.RuntimePlatform != Device.Android) return;
            HasShadow = true;
            BackgroundColor = Color.Transparent;
            BorderColor = Color.Transparent;
        }
    }
}