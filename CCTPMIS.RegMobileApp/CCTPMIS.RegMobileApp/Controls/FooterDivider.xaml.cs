﻿namespace CCTPMIS.RegMobileApp.Controls
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FooterDivider : ContentView
    {
        public FooterDivider()
        {
            InitializeComponent();
        }
    }
}