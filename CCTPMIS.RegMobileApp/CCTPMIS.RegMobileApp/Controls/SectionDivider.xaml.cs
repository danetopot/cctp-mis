﻿namespace CCTPMIS.RegMobileApp.Controls
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SectionDivider : ContentView
    {
        public SectionDivider()
        {
            InitializeComponent();
        }
    }
}