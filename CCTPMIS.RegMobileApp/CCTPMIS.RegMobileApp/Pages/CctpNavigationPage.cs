﻿namespace CCTPMIS.RegMobileApp.Pages
{
    using Xamarin.Forms;

    public class CctpNavigationPage : NavigationPage
    {
        public CctpNavigationPage(Page root)
            : base(root)
        {
            Init();
            Title = root.Title;
            Icon = root.Icon;
        }

        public CctpNavigationPage()
        {
            Init();
        }

        private void Init()
        {
            // if (Device.RuntimePlatform== Device.iOS)
            // {
            // BarBackgroundColor = Color.FromHex("FAFAFA");
            // }
            // else
            // {
            // BarBackgroundColor = (Color)Application.Current.Resources["Primary"];
            // BarTextColor = (Color)Application.Current.Resources["NavigationText"];
            // }
        }
    }
}