﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCTPMIS.RegMobileApp.Helpers;
using CCTPMIS.RegMobileApp.Models;
using MvvmHelpers;
using Xamarin.Forms;

namespace CCTPMIS.RegMobileApp.Extensions
{
    public static class ListingstateExtensions
    {
        public static IEnumerable<Listing> Search(this IEnumerable<Listing> Listings, string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
                return Listings;

            var searchSplit = searchText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            //search title, then category, then speaker name
            return Listings.Where(Registration =>
                searchSplit.Any(search =>
                    Registration.Haystack.IndexOf(search, StringComparison.OrdinalIgnoreCase) >= 0));
        }

        public static IEnumerable<Listing> Get(this IEnumerable<Listing> Listings)
        {
            return Listings;
        }
    }
}