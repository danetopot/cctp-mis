﻿using System.Collections.Generic;

namespace CCTPMIS.RegMobileApp.Models
{
    public class EnumeratorLocation
    {
        public int Id { get; set; }
        public int EnumeratorId { get; set; }
        public int LocationId { get; set; }
        public bool IsActive { get; set; }
    }
}