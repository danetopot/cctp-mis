﻿namespace CCTPMIS.RegMobileApp.Models
{
    using SQLite;

    public class HouseHoldRegistration
    {
        public int BenefitTypeId { get; set; }

        public int Camels { get; set; }

        public int Chicken { get; set; }

        public int ConstituencyId { get; set; }

        public int CookingFuelTypeId { get; set; }

        public int CountyId { get; set; }

        public int Deaths { get; set; }

        public int DivisionId { get; set; }

        public int Donkeys { get; set; }

        public int DwellingUnitRiskId { get; set; }

        public int ExoticCattle { get; set; }

        public int FloorConstructionMaterialId { get; set; }

        public int Goats { get; set; }

        public int HabitableRooms { get; set; }

        public int HouseHoldConditionId { get; set; }

        public string HouseHoldHeadName { get; set; }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int IndigenousCattle { get; set; }

        public string InKindBenefit { get; set; }

        public bool IsBicycle { get; set; }

        public bool IsCar { get; set; }

        public bool IsInKind { get; set; }

        public bool IsMobilePhone { get; set; }

        public bool IsMonetary { get; set; }

        public bool IsMotorcycle { get; set; }

        public bool IsOwnerOccupied { get; set; }

        public bool IsRecievingSocial { get; set; }

        public bool IsRefrigerator { get; set; }

        public bool IsTelevision { get; set; }

        public bool IsTukTuk { get; set; }

        public decimal LastReceiptAmount { get; set; }

        public int LightingFuelTypeId { get; set; }

        public int LiveBirths { get; set; }

        public int LocalityId { get; set; }

        public int LocationId { get; set; }

        public int Months { get; set; }

        public string NearestReligiousBuilding { get; set; }

        public string NearestSchool { get; set; }

        public string OtherProgrammeNames { get; set; }

        public string OtherProgrammesId { get; set; }

        public string PhysicalAddress { get; set; }

        public int Pigs { get; set; }

        public int ProgrammeId { get; set; }

        public int RoofConstructionMaterialId { get; set; }

        public int Sheep { get; set; }

        public string Status { get; set; }

        public int SubLocationId { get; set; }

        public int TenureStatusId { get; set; }

        public string Village { get; set; }
        public int WallConstructionMaterialId { get; set; }

        public int WasteDisposalModeId { get; set; }

        public int WaterSourceId { get; set; }
        public int Years { get; set; }
    }
}