﻿using System;
using Plugin.DeviceInfo.Abstractions;

namespace CCTPMIS.RegMobileApp.Models
{
    public class DeepLinkPage
    {
        public string Id { get; set; }

        public AppPage Page { get; set; }
    }

    public class LocalDeviceInfo
    {
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public Platform Platform { get; set; }
        public Idiom Idiom { get; set; }
        public bool IsDevice { get; set; }
    }

    public class ApiStatus
    {
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public int? Id { get; set; }
    }
}