﻿namespace CCTPMIS.RegMobileApp.Models
{
    public class Constituency
    {
        public string Code { get; set; }

        public int CountyId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}