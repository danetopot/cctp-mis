﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.RegMobileApp.Models
{
    public class CreateModifyFields
    {
        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}