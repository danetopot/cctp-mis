﻿namespace CCTPMIS.RegMobileApp.Models
{
    public class OptionsModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}