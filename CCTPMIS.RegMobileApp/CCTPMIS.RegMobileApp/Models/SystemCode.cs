﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class SystemCode
    {
        public string Code { get; set; }

        public string Description { get; set; }

        [PrimaryKey]
        public int Id { get; set; }

     //   public bool IsUserMaintained { get; set; }

     //   [Ignore]
      //  public ICollection<SystemCodeDetail> SystemCodeDetails { get; set; }
    }
}