﻿using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class SubLocation
    {
        public string Code { get; set; }

        [PrimaryKey]
        public int Id { get; set; }

        public int LocationId { get; set; }

        public string Name { get; set; }
    }
}