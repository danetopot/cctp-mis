﻿using System.Collections.Generic;

namespace CCTPMIS.RegMobileApp.Models
{
    public class ListingOptionsResponse
    {
        public List<Programme> Programmes { get; set; }
         public string Error { get; set; }
    }

    public class EnumeratorLoginResponse : ListingOptionsResponse
    {
        public Enumerator Enumerator { get; set; }
        public List<Location> Locations { get; set; }
        public List<SubLocation> SubLocations { get; set; }
        public List<SystemCode> SystemCodes { get; set; }
        public List<SystemCodeDetail> SystemCodeDetails { get; set; }
        public List<EnumeratorLocation> EnumeratorLocations { get; set; }
    }

    public class EnumeratorCVResponse : EnumeratorLoginResponse
    {
        public List<ComValListingPlanHH> ComValListingPlanHHs { get; set; }
        
    }
}