﻿using System.Collections.Generic;

namespace CCTPMIS.RegMobileApp.Models
{
    public class HouseHoldMemberDataOption
    {
        public List<SystemCodeDetail> MemberChronicIllnessOptions { get; set; }

        public List<SystemCodeDetail> MemberDisabilityCareStatuses { get; set; }

        public List<SystemCodeDetail> MemberDisabilityTypes { get; set; }

        public List<SystemCodeDetail> MemberEducationLevels { get; set; }

        public List<SystemCodeDetail> MemberFatherAliveOptions { get; set; }

        public List<SystemCodeDetail> MemberIdentificationDocumentTypes { get; set; }

        public List<SystemCodeDetail> MemberJobOptions { get; set; }

        public List<SystemCodeDetail> MemberLearningStatuses { get; set; }

        public List<SystemCodeDetail> MemberMaritalStatuses { get; set; }

        public List<SystemCodeDetail> MemberMotherAliveOptions { get; set; }

        public List<SystemCodeDetail> MemberRelationships { get; set; }

        public List<SystemCodeDetail> MemberSexs { get; set; }

        public List<SystemCodeDetail> MemberStatusOptions { get; set; }

        public List<SystemCodeDetail> MemberWorkTypes { get; set; }
    }
}