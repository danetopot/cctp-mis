﻿namespace CCTPMIS.RegMobileApp.Models
{
    public class Division
    {
        public string Code { get; set; }

        public int DistrictId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}