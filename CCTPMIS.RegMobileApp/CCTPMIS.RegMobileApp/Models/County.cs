﻿namespace CCTPMIS.RegMobileApp.Models
{
    public class County
    {
        public string Code { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}