﻿using System.ComponentModel.DataAnnotations;
using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class CvRegistration
    {
        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        [Ignore]
        public Programme Programme { get; set; }

        public int ProgrammeId { get; set; }
        public string RegistrationDate { get; set; }
        public int LocationId { get; set; }

        [Ignore]
        public Location Location { get; set; }

        public int SubLocationId { get; set; }

        [Ignore]
        public SubLocation SubLocation { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }

        [Ignore]
        public Enumerator Enumerator { get; set; }

        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HHdNationalIdNo { get; set; }
        public string HHdFullName => $"{HHdFirstName} {HHdMiddleName} {HHdSurname}";
        public int HHdSexId { get; set; }
        public string HHdSexName => $"{HHdSex?.Description}";

        [Ignore]
        public SystemCodeDetail HHdSex { get; set; }

        public string HHdDoB { get; set; }
        public string HHdDoBDate { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgFullName => $"{CgFirstName} {CgMiddleName} {CgSurname}";
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public string CgSexName => $"{CgSex?.Description}";

        [Ignore]
        public SystemCodeDetail CgSex { get; set; }

        public string CgDoB { get; set; }
        public string CgDoBDate { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }

        [Ignore]
        public SystemCodeDetail Status { get; set; }

        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        [Ignore]
        public Enumerator SyncEnumerator { get; set; }

        [PrimaryKey]
        public int Id { get; set; }

        public int? CvStatusId { get; set; }

        public string CvDownloadDate { get; set; }
        public string CvUpdateDate { get; set; }
        public string CvSyncedDate { get; set; }
        public int? CvEnumeratorId { get; set; }
        public int? CvSyncEnumeratorId { get; set; }

        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        public string ReferenceNo => $"Reference No: #{Id}".ToUpper();
        public string LocationName => $"{Location?.Name}";
        public string ProgrammeName => $"{Programme?.Name}";
        public string SubLocationName => $"{SubLocation?.Name}";
        public string DurationDisplay => $"{Years} Years, {Months} Months";
        public int RegAcceptId { get; set; }

        public string CvStatusName
        {
            get
            {
                if (CvStatusId != null)
                    switch (CvStatusId)
                    {
                        case 1:
                            return "Confirmed";

                        case 2:
                            return "Ammendment";

                        case -1:
                            return "Exited";

                        case 3:
                            return "Ammended";
                    }
                return "Pending";
            }
        }
    }
}