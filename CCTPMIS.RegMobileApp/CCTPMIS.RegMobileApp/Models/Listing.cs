﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class Listing
    {
        public int Id { get; set; }

        [AutoIncrement, PrimaryKey]
        public int LocalId { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string RegistrationDate { get; set; }
        public string CgDoB { get; set; }

        [Ignore]
        public Programme Programme { get; set; }

        public int ProgrammeId { get; set; }
        public int LocationId { get; set; }

        [Ignore]
        public Location Location { get; set; }

        public int SubLocationId { get; set; }

        [Ignore]
        public SubLocation SubLocation { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }

        public int EnumeratorId { get; set; }

        [Ignore]
        public Enumerator Enumerator { get; set; }

        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneNationalIdNo { get; set; }
        public string BeneFullName => $"{BeneFirstName} {BeneMiddleName} {BeneSurname}";
        public int? BeneSexId { get; set; }

        [Ignore]
        public SystemCodeDetail BeneSex { get; set; }

        public string BeneDoB { get; set; }
        public string BenePhoneNumber { get; set; }
        public DateTime BeneDoBDate { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgFullName => $"{CgFirstName} {CgMiddleName} {CgSurname}";
        public string CgNationalIdNo { get; set; }
        public string CgPhoneNumber { get; set; }
        public int CgSexId { get; set; }

        [Ignore]
        public SystemCodeDetail CgSex { get; set; }

        
        public DateTime CgDoBDate { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }

        [Ignore]
        public SystemCodeDetail Status { get; set; }

        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        [Ignore]
        public Enumerator SyncEnumerator { get; set; }

        public string GeoPosition => $"[ {Longitude}, {Latitude}]";

        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        public string ReferenceNo => $"Local Reference No: #{LocalId}".ToUpper();
        public string LocationName => $"{Location?.Name}";
        public string ProgrammeName => $"{Programme?.Name}";
        public string SubLocationName => $"{SubLocation?.Name}";
        public string DurationDisplay => $"{Years} Years, {Months} Months";

        public string CgSexName => $"{CgSex?.Description}";

        public string BeneSexName  => $"{BeneSex?.Description}";

        private const string delimiter = "|";
        private string haystack;

        [Newtonsoft.Json.JsonIgnore]
        public string Haystack
        {
            get
            {
                if (haystack != null)
                    return haystack;

                var builder = new StringBuilder();
                builder.Append(delimiter);
                //  builder.Append(Title);
                builder.Append(delimiter);
                //if (!string.IsNullOrWhiteSpace(MainCategory?.Name))
                //    builder.Append(MainCategory.Name);
                //builder.Append(delimiter);
                //if (Speakers != null)
                //{
                //    foreach (var p in Speakers)
                //        builder.Append($"{p.FirstName} {p.LastName}{delimiter}{p.FirstName}{delimiter}{p.LastName}");
                //}

                haystack = builder.ToString();
                return haystack;
            }
        }
    }
}