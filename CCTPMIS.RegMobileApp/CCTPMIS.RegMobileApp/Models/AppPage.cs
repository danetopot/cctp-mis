﻿namespace CCTPMIS.RegMobileApp.Models
{
    public enum AppPage
    {
        HomePage,
        Registration,
        CommunityVal,
        Targeting,
        Sync,
        About,
        MyProfile,
        Settings,
        Logout,

        /* MiniHacks,
         Sponsors,
         Venue,
         FloorMap,
         ConferenceInfo,
         Settings,
         Session,
         Speaker,
         Sponsor,
         Login,
         ForgotPassword,
         Event,
         Notification,
         TweetImage,
         WiFi,
         CodeOfConduct,
         Filter,
         Information,
         Tweet,
         Evals
         */
    }
}