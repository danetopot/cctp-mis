﻿namespace CCTPMIS.RegMobileApp.Models
{
    public class Locality
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}