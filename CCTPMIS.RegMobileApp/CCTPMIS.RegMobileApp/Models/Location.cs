﻿using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class Location
    {
        public string Code { get; set; }
        public int DivisionId { get; set; }
        public string Name { get; set; }

        [PrimaryKey]
        public int Id { get; set; }
    }
}