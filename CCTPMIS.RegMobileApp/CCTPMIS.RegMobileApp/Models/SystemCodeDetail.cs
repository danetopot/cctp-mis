﻿using System.ComponentModel.DataAnnotations;
using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class SystemCodeDetail : CreateModifyFields
    {
        public string Code { get; set; }

        public string Description { get; set; }

        [PrimaryKey]
        public int Id { get; set; }

        public byte OrderNo { get; set; }

    //    [Ignore]
     //   public SystemCode SystemCode { get; set; }

        public int SystemCodeId { get; set; }
    }
}