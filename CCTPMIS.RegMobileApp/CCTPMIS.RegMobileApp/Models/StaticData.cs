﻿using System.Collections.Generic;

namespace CCTPMIS.RegMobileApp.Models
{
    public class StaticData
    {
        public List<OptionsModel> BenefitTypes { get; set; }

        public List<Constituency> Constituencies { get; set; }

        public List<OptionsModel> CookingFuels { get; set; }

        public List<County> Counties { get; set; }

        public List<Division> Divisions { get; set; }

        public List<OptionsModel> DwellingRisks { get; set; }

        public List<OptionsModel> FloorConstructionMaterials { get; set; }

        public List<OptionsModel> HouseholdConditions { get; set; }

        public List<OptionsModel> LightingFuels { get; set; }

        public List<Locality> Localities { get; set; }

        public List<Location> Locations { get; set; }

        public List<OptionsModel> NsnpBenefits { get; set; }

        public List<Programme> Programmes { get; set; }

        public List<OptionsModel> RoofConstructionMaterials { get; set; }

        public List<SubLocation> SubLocations { get; set; }

        public List<OptionsModel> TenureStatuses { get; set; }

        public List<OptionsModel> WallConstructionMaterials { get; set; }

        public List<OptionsModel> WasteDisposals { get; set; }

        public List<OptionsModel> WaterSources { get; set; }
    }
}