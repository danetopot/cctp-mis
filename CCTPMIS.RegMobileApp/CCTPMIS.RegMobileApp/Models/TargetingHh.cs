﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SQLite;

namespace CCTPMIS.RegMobileApp.Models
{
    public class TargetingHh
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }

        [Ignore]
        public Programme Programme { get; set; }

        public int ProgrammeId { get; set; }
        public string RegistrationDate { get; set; }
        public int LocationId { get; set; }

        [Ignore]
        public Location Location { get; set; }

        public int SubLocationId { get; set; }

        [Ignore]
        public SubLocation SubLocation { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }

        [Ignore]
        public Enumerator Enumerator { get; set; }

        public string HHdFullName { get; set; }
        public string CgFullName { get; set; }

        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }

        [Ignore]
        public SystemCodeDetail Status { get; set; }

        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        [Ignore]
        public Enumerator SyncEnumerator { get; set; }

        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        public string ReferenceNo => $"Reference No: #{Id}".ToUpper();
        public string LocationName => $"{Location?.Name}";
        public string ProgrammeName => $"{Programme?.Name}";
        public string SubLocationName => $"{SubLocation?.Name}";
        public string DurationDisplay => $"{Years} Years, {Months} Months";

        public int IsTelevision { get; set; }
        public int IsMotorcycle { get; set; }
        public int IsTukTuk { get; set; }
        public int IsRefrigerator { get; set; }
        public int IsCar { get; set; }
        public int IsMobilePhone { get; set; }
        public int IsBicycle { get; set; }

        public int ExoticCattle { get; set; }
        public int IndigenousCattle { get; set; }
        public int Sheep { get; set; }
        public int Goats { get; set; }
        public int Camels { get; set; }
        public int Donkeys { get; set; }
        public int Pigs { get; set; }
        public int Chicken { get; set; }
        public int LiveBirths { get; set; }
        public int Deaths { get; set; }
        public int IsSkippedMeal { get; set; }
        public int IsReceivingSocial { get; set; }
        public string Programmes { get; set; }
        public decimal LastReceiptAmount { get; set; }
        public string InKindBenefit { get; set; }
        public int WasteDisposalModeId { get; set; }
        public int WaterSourceId { get; set; }
        public int WallConstructionMaterialId { get; set; }
        public int IsOwned { get; set; }
        public int TenureStatusId { get; set; }
        public int RoofConstructionMaterialId { get; set; }
        public string OtherProgrammeNames { get; set; }
        public int NsnpProgrammesId { get; set; }
        public int OtherProgrammesId { get; set; }
        public int LightingFuelTypeId { get; set; }
        public int IsMonetary { get; set; }
        public int HabitableRooms { get; set; }
        public int HouseHoldConditionId { get; set; }
        public int FloorConstructionMaterialId { get; set; }
        public int DwellingUnitRiskId { get; set; }
        public int CookingFuelTypeId { get; set; }
        public int BenefitTypeId { get; set; }
        public int? InterviewStatusId { get; set; }


        
    }
}