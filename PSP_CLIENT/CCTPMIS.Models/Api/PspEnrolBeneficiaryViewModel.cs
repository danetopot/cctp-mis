﻿namespace CCTPMIS.Models.Api
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class PspEnrolBeneficiaryViewModel
    {
        [Required]
        public string AccountName { get; set; }

        [Required]
        public string AccountNo { get; set; }

        [Required]
        public string AccountOpenedOn { get; set; }

        [Required]
        public string BankCode { get; set; }

        [Required]
        public string BranchCode { get; set; }

        [Required]
        public string TokenId { get; set; }

        [Required]
        public string EnrolmentNo { get; set; }

        public string MobileNo1 { get; set; }

        public string MobileNo2 { get; set; }

        public string PaymentCardNo { get; set; }

        public string PriReciDoB { get; set; }

        public string PriReciFirstName { get; set; }

        [Required]
        public string PriReciLI { get; set; }

        [Required]
        public string PriReciLMF { get; set; }

        [Required]
        public string PriReciLP { get; set; }

        [Required]
        public string PriReciLRF { get; set; }

        [Required]
        public string PriReciLT { get; set; }

        public string PriReciMiddleName { get; set; }

        [Required]
        public string PriReciNationalIdNo { get; set; }

        [Required]
        public string PriReciRI { get; set; }

        [Required]
        public string PriReciRMF { get; set; }

        [Required]
        public string PriReciRP { get; set; }

        [Required]
        public string PriReciRRF { get; set; }

        // [RegularExpression("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$", ErrorMessage = "Invalid Format")]
        [Required]
        public string PriReciRT { get; set; }

        [Required]
        public string PriReciSex { get; set; }

        public string PriReciSurname { get; set; }

        public string SecReciDoB { get; set; }

        public string SecReciFirstName { get; set; }

        public string SecReciLI { get; set; }

        public string SecReciLMF { get; set; }

        public string SecReciLP { get; set; }

        public string SecReciLRF { get; set; }

        public string SecReciLT { get; set; }

        public string SecReciMiddleName { get; set; }

        public string SecReciNationalIdNo { get; set; }

        public string SecReciRI { get; set; }

        public string SecReciRMF { get; set; }

        public string SecReciRP { get; set; }

        public string SecReciRRF { get; set; }

        public string SecReciRT { get; set; }

        public string SecReciSex { get; set; }

        public string SecReciSurname { get; set; }

        public int? UserId { get; set; }

         
    }
}