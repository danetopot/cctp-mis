﻿namespace CCTPMIS.Models.Api
{
    using System.ComponentModel.DataAnnotations;

    public class CreditReportFinalizeViewModel
    {
        [Required]
        public string BankCode { get; set; }

        [Required]
        public int PaymentCycleId { get; set; }
    }
}