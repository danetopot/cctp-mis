﻿namespace CCTPMIS.Models.Api
{
    using System.ComponentModel.DataAnnotations;

    public class AccountActivityReportFinalizeViewModel
    {
        [Required]
        public string BankCode { get; set; }

        [Required]
        public int MonthNo { get; set; }

        [Required]
        public int Year { get; set; }
    }
}