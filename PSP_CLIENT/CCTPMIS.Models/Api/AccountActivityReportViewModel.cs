﻿namespace CCTPMIS.Models.Api
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AccountActivityReportViewModel
    {
        [Required]
        public string AccountName { get; set; }

        [Required]
        public string AccountNo { get; set; }

        [Required]
        public DateTime AccountOpenedOn { get; set; }

        [Required]
        public string BankCode { get; set; }

        [Required]
        public string BranchCode { get; set; }

        // public DateTime? InactiveDate { get; set; }
        public decimal? ClawbackAmount { get; set; }

        public DateTime? DormancyDate { get; set; }

        [Required]
        public int EnrolmentNo { get; set; }

        [Required]
        public bool HadBeneBiosVerified { get; set; } = false;

        [Required]
        public bool HadUniqueWdl { get; set; } = false;

        [Required]
        public bool IsDormant { get; set; } = false;

        // [Required]
        // public bool IsInactive { get; set; } = false;
        [Required]
        public bool IsDueForClawback { get; set; } = false;

        [Required]
        [Range(1, 12, ErrorMessage = "Please enter valid Month Number between a and 12")]
        public int MonthNo { get; set; }

        public DateTime? UniqueWdlDate { get; set; }

        public string UniqueWdlTrxNo { get; set; }

        [Required]
        public int Year { get; set; }
    }
}