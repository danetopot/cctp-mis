﻿namespace CCTPMIS.Models.Api
{
    using System.ComponentModel.DataAnnotations;

    public class PspEnrolCareGiverViewModel
    {
        [Required]
        public string AccountName { get; set; }

        [Required]
        public string AccountNo { get; set; }

        [Required]
        public string BankCode { get; set; }

        [Required]
        public string BranchCode { get; set; }

        [Required]
        public string EnrolmentNo { get; set; }

        [Required]
        public string SecReciDoB { get; set; }

        [Required]
        public string SecReciFirstName { get; set; }

        [Required]
        public string SecReciLI { get; set; }

        [Required]
        public string SecReciLMF { get; set; }

        [Required]
        public string SecReciLP { get; set; }

        [Required]
        public string SecReciLRF { get; set; }

        [Required]
        public string SecReciLT { get; set; }

        public string SecReciMiddleName { get; set; }

        [Required]
        public string SecReciNationalIdNo { get; set; }

        [Required]
        public string SecReciRI { get; set; }

        [Required]
        public string SecReciRMF { get; set; }

        [Required]
        public string SecReciRP { get; set; }

        [Required]
        public string SecReciRRF { get; set; }

        [Required]
        public string SecReciRT { get; set; }

        [Required]
        public string SecReciSex { get; set; }

        [Required]
        public string SecReciSurname { get; set; }
    }
}