﻿namespace CCTPMIS.Models
{
    public static class MisKeys
    {
        public static string ACCOUNTACTIVITY = "ACCOUNTACTIVITY";

        public static string ACTIVITYREPORT = "ACTIVITYREPORT";

        public static string ACTIVITYREPORTFINALIZE = "ACTIVITYREPORTFINALIZE";

        public static string ApiData = "/apiData";

        public static string BENEFICIARIESSEARCH = "BENEFICIARIESSEARCH";

        public static string CREDITREPORT = "CREDITREPORT";

        public static string CREDITREPORTFINALIZE = "CREDITREPORTFINALIZE";

        public static string ENROLSUBMISSION = "ENROLSUBMISSION";

        public static string ERROR = "ERROR";

        public static string EMAIL = "EMAIL";

        public static string MisData = "MisData";

        public static string PAYMENTCARDSUBMISSION = "PAYMENTCARDSUBMISSION";

        public static string PAYROLLTRANSACTION = "PAYROLLTRANSACTION";
    }
}