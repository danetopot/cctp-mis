﻿namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;

    public class GetPayrollSummaryVm
    {
        [Display(Name = "Total Duplicated National IDs ")]
        public int? PayrollDuplicateIdHhs { get; set; }

        [Display(Name = "Total Exceptions")]
        public int? PayrollExceptionsHhs { get; set; }

        [Display(Name = "Total Ineligible Households / Individuals")]
        public int? PayrollIneligibleHhs { get; set; }

        [Display(Name = "Total Invalid National IDs")]
        public int? PayrollInvalidIdHhs { get; set; }

        [Display(Name = "Total Invalid Payment Account Households / Individuals")]
        public int? PayrollInvalidPaymentAccountHhs { get; set; }

        [Display(Name = "Total Invalid Payment Card Households / Individuals")]
        public int? PayrollInvalidPaymentCardHhs { get; set; }

        [Display(Name = "Total Suspicious Households / Individuals")]
        public int? PayrollSuspiciousHhs { get; set; }

        [Display(Name = "Total Payroll Amount")]
        public decimal? TotalPayrollAmount { get; set; }

        [Display(Name = "Total Households / Individuals in Payroll")]
        public int? TotalPayrollHhs { get; set; }
    }
}