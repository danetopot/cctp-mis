﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CCTPMIS.Models.Payment
{
    public class ReconciliationDetailCreateViewModel
    {  
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Upload { get; set; }
        [Display(Name = "Upload Supporting documents")]
        public string UploadFile { get; set; }
        [Display(Name = "Opening Balance")]
        [Required]
        public decimal OpeningBalance { get; set; }

        [Display(Name = "Funds Request")]
        [Required]
        public decimal CrFundsRequests { get; set; }
        [Display(Name = "Funds Request on Bank Statement")]
        [Required]
        public decimal CrFundsRequestsStatement { get; set; }
        [Display(Name = "Funds Request Differences Narration")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string CrFundsRequestsDiffNarration { get; set; }
        [Display(Name = "Clawback Totals")]
        [Required]
        public decimal CrClawBacks { get; set; }
        [Display(Name = "Clawback Totals on Bank Statement")]
        [Required]
        public decimal CrClawBacksStatement { get; set; }
        [Display(Name = "Clawback Totals  Differences Narration")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string CrClawBacksDiffNarration { get; set; }
        [Display(Name = "Total Payments  ")]
        [Required]
        public decimal DrPayments { get; set; }
        [Display(Name = "Total Payments On Bank Statement ")]
        [Required]
        public decimal DrPaymentsStatement { get; set; }
        [Display(Name = "Total Payments Differences Narration")]
        [Required]

        [DataType(DataType.MultilineText)]
        public string DrPaymentsDiffNarration { get; set; }
        [Required]

        [Display(Name = "Total Commissions")]
        public decimal DrCommissions { get; set; }
        [Required]

        [Display(Name = "Total Commissions on Bank Statement")]
        public decimal DrCommissionsStatement { get; set; }
         [Display(Name = "Totals Commissions Differences Narration")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string DrCommissionsDiffNarration { get; set; }
        [Display(Name = "Account Balance")]
        [Required]
        public decimal Balance { get; set; }
        [Display(Name = "Account Balance on Statement")]
        [Required]
        public decimal BalanceStatement { get; set; }
        [Display(Name = "Account Balance Differences Narration")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string BalanceDiffNarration { get; set; }
        [Required]
        [Display(Name = "Payment Service Provider")]
        public byte PspId { get; set; }
        [Required]
        [Display(Name = "Reconciliation No.")]
        public int ReconciliationId { get; set; }
    }
}