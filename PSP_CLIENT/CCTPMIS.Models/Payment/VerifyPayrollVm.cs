﻿namespace CCTPMIS.Models.Payment
{
    public class VerifyPayrollVm
    {
        public GetPayrollSummaryVm GetPayrollSummaryVm { get; set; }

        public GetPayrollSummaryVm VerifyPayrollSummaryVm { get; set; }

        public VerifyPayrollVm1 VerifyPayrollVm1 { get; set; }
    }
}