﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using System.Xml.Serialization;

    [XmlType(TypeName = "Record", Namespace = "")]
    public class ExceptionType
    {
        [Display(Name = "Exception Type")]
        public int? ExceptionTypeId { get; set; }
    }
}