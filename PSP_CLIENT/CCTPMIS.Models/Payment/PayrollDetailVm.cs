﻿namespace CCTPMIS.Models.Payment
{
    using CCTPMIS.Business.Model;

    public class PayrollDetailVm
    {
        public GetPayrollSummaryVm GetPayrollSummaryVm { get; set; }

        public PaymentCycleDetail PaymentCycleDetail { get; set; }
    }
}