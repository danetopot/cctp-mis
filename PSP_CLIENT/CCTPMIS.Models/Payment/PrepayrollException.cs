﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using System.Xml.Serialization;

    [XmlType(TypeName = "Record", Namespace = "")]
    public class PrepayrollException
    {
        [Display(Name = "Household ")]
        public int? HhId { get; set; }
        [Display(Name = "Payment Cycle ")]
        public int? PaymentCycleId { get; set; }
        [Display(Name = "Person ")]
        public int? PersonId { get; set; }
        [Display(Name = "Programme ")]
        public int? ProgrammeId { get; set; }
    }



    [XmlType(TypeName = "Record", Namespace = "")]
    public class PaymentCycleXmlModel
    {
        [Display(Name = "Reference ID")]
        public int? Id { get; set; }
        [Display(Name = "Payment Cycle ID")]
        public int? PaymentCycleId { get; set; }
        [Display(Name = "Cycle Name")]
        public string Name { get; set; }

    }
}