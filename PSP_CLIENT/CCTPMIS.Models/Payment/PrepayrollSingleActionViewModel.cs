﻿namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class PrepayrollSingleActionViewModel
    {
        [Display(Name = "Select Exception Types")]
        public int?[] EnrolmentGroupId { get; set; }

        [Display(Name = "Select Exception Types")]
        public int? ExceptionTypeId { get; set; }

        [Display(Name = "Household ")]
        public int? HhId { get; set; }
        [Display(Name = "ID ")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Notes *")]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [Required]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycle { get; set; }

        [Display(Name = "Person ")]
        public int? PersonId { get; set; }

        [Required]
        [Display(Name = "Programme")]
        public int Programme { get; set; }
        [Display(Name = "Remove Previous Supporting Document ")]
        public bool RemovePreviousSupportingDoc { get; set; }

        [Display(Name = "Replace Previous Actioning")]
        public bool ReplacePreviousActioning { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Upload { get; set; }

        [Display(Name = "Upload Supporting documents")]
        public string UploadFile { get; set; }
    }
}