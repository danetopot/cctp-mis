﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class VerifyPayrollVm1
    {
        [Display(Name = "Status ID")]
        public int StatusId { get; set; }
        [Display(Name = "Is Payroll Verified")]
        public bool Verified { get; set; }
    }
}