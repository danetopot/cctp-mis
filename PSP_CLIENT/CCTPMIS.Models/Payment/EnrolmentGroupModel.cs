﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using System.Xml.Serialization;

    [XmlType(TypeName = "Record", Namespace = "")]
    public class EnrolmentGroupModel
    {
        [Display(Name = "Enrolment Group ID")]
        public int? EnrolmentGroupId { get; set; }
    }
}