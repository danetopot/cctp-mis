﻿namespace CCTPMIS.Models.Payment
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class PaymentCycleDetailCreateViewModel
    {
        [Display(Name = "Entitlement Amount")]
        public decimal? EntitlementAmount { get; set; }

       
        [Required]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        [Required]
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Upload { get; set; }

        [Display(Name = "Upload Supporting documents")]
        public string UploadFile { get; set; }
    }
}