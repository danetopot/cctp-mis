﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class FundsRequestRequirement
    {
        public decimal Amount { get; set; }

        public decimal Commission { get; set; }

        [Display(Name = "Number")]
        public int No { get; set; }
        [Display(Name = "Households")]
        public int NoOfHhs { get; set; }

        public string Programme { get; set; }

        [Display(Name = "PSP")]
        public string Psp { get; set; }

        [Display(Name = "Source of Funds")]
        public string Source { get; set; }

        public decimal Total { get; set; }
    }
}