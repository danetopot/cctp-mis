﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class SelectListA
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }




    public class PostPayrollSummaryViewModel
    {
         [Display(Name="Total Households in Payroll")]
        public int TotalPaymentHhs { get; set; }
        [Display(Name = "Successful Credits")]
        public int SuccessfulPayments { get; set; }
        [Display(Name = "Failed Credits")]
        public int FailedPayments { get; set; }
        [Display(Name = "Total Households in payroll with Exceptions")]
        public int PaymentExceptionsHhs { get; set; }
        [Display(Name = "Total Households with Invalid IDs")]
        public int PaymentInvalidIDHhs { get; set; }
        [Display(Name = "Total Households in payroll with Duplicate IDs")]
        public int PaymentDuplicateIDHhs { get; set; }
        [Display(Name = "Total Households in payroll with Invalid Payment Accounts")]
        public int PaymentInvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Total Households in payroll with Invalid Payment Cards")]
        public int PaymentInvalidPaymentCardHhs { get; set; }
        [Display(Name = "Total Ineligible Households in payroll")]
        public int PaymentIneligibleHhs { get; set; }
        [Display(Name = "Total Households with Suspicious Payments")]
        public int PaymentSuspiciousHhs { get; set; }
        [Display(Name = "Total Amount in Payroll (KES)")]
        public decimal TotalPaymentAmount { get; set; }
    }

}