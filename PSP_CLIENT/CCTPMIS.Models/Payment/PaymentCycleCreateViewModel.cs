﻿using System;
using System.Collections.Generic;

namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;

    

    public class ReconciliationCreateViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Statement Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "Statement End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Associated Payment Cycles")]
        public ICollection<PaymentCycleXmlModel> PaymentCycles { get; set; }

    }
    public class PaymentCycleCreateViewModel
    {
        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(128)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Financial Year")]
        public string FinancialYear { get; set; }

        [Display(Name = "Financial Year")]
        public int? FinancialYearId { get; set; }

        [Display(Name = "Start Month")]
        public string FromMonth { get; set; }

        [Required]
        [Display(Name = "Start Month ")]
        public int? FromMonthId { get; set; }
        [Display(Name = "Reference ID")]
        public int? Id { get; set; }

        [Display(Name = "Payment Stage")]
        public int? StatusId { get; set; }

        [Display(Name = "End Month")]
        public string ToMonth { get; set; }

        [Required]
        [Display(Name = "End Month")]
        public int? ToMonthId { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public string StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
    }
}