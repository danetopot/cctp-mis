﻿namespace CCTPMIS.Models.Payment
{
    using System.Xml.Serialization;

    using CCTPMIS.Business.Model;

    [XmlType(TypeName = "Record", Namespace = "")]
    public class PaymentCycleAddEnrolmentGroupViewModel
    {
        [XmlIgnore]
        public SystemCodeDetail EnrolmentGroup { get; set; }
        public int EnrolmentGroupId { get; set; }
        public int Id { get; set; }
        public decimal PaymentAmount { get; set; }
        public int PaymentCycleId { get; set; }
    }
}