﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class ResendPayrollFileViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Payment Cycle")]
        public string PaymentCycle { get; set; }
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }
        [Display(Name = "User")]
        public int UserId { get; set; }
    }
}