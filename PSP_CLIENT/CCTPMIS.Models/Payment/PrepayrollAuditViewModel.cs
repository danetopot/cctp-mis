﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class PrepayrollAuditViewModel
    {
        [Display(Name = "Actioned Beneficiaries/ Households with Duplicate National ID Numbers")]
        public int? ActionedDuplicateNationalIdHhs { get; set; }
        [Display(Name = "Actioned Ineligible Beneficiaries")]
        public int? ActionedIneligibleHhs { get; set; }
        [Display(Name = "Actioned Beneficiaries with Invalid National ID Numbers")]
        public int? ActionedInvalidNationalIdHhs { get; set; }
        [Display(Name = "Actioned Beneficiaries with Invalid Bank Accounts")]
        public int? ActionedInvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Actioned Beneficiaries with Invalid Payment Cards")]
        public int? ActionedInvalidPaymentCardHhs { get; set; }
        [Display(Name = "Actioned Beneficiaries with Suspicious Payments")]
        public int? ActionedSuspiciousPaymentHhs { get; set; }
        [Display(Name = "Duplicate National ID Numbers Action File")]
        public int? DuplicateIdActionsFileId { get; set; }
        [Display(Name = "Duplicate National ID Numbers e.g. Double Dipping")]
        public int? DuplicateNationalIdHhs { get; set; }
        [Display(Name = "Total Enrolled Beneficiaries")]
        public int? EnroledHhs { get; set; }
        [Display(Name = "Total Beneficiaries in Enrollment Groups")]
        public int? EnrolmentGroupHhs { get; set; }
        [Display(Name = "Exceptions File ID")]
        public int? ExceptionsFileId { get; set; }
        [Display(Name = "Ineligible Beneficiaries Actions File ID")]
        public int? IneligibleBeneficiaryActionsFileId { get; set; }
        [Display(Name = "Ineligible Beneficiaries")]
        public int? IneligibleHhs { get; set; }
        [Display(Name = "Actions File for Beneficiaries with   Invalid National ID Numbers")]
        public int? InvalidIdActionsFileId { get; set; }
        [Display(Name = "Households with Invalid National ID Numbers")]
        public int? InvalidNationalIdHhs { get; set; }
        [Display(Name = "Action File for Beneficiaries with Invalid Payment Accounts")]
        public int? InvalidPaymentAccountActionsFileId { get; set; }
        [Display(Name = "Beneficiaries with Invalid Payment Accounts")]
        public int? InvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Action file for Beneficiaries with Invalid Payment Cards")]
        public int? InvalidPaymentCardActionsFileId { get; set; }
        [Display(Name = "Beneficiaries with Invalid Payment Cards")]
        public int? InvalidPaymentCardHhs { get; set; }
        [Display(Name = "Total Payroll Adjustment Amount")]
        public decimal? PayrollAdjustmentAmount { get; set; }
        [Display(Name = "Payroll Total Amount")]
        public decimal? PayrollAmount { get; set; }
        [Display(Name = "Basic Entitlement Amount")]
        public decimal? PayrollEntitlementAmount { get; set; }
        [Display(Name = "Payroll Has been Actioned")]
        public int? PayrollHasBeenAcioned { get; set; }
        [Display(Name = "Total Households in Payroll")]
        public int? PayrollHhs { get; set; }
        [Display(Name = "Actions File for Households with Suspicious Payments ")]
        public int? SuspiciousPaymentActionsFileId { get; set; }
        [Display(Name = "Households with Suspicious Payments")]
        public int? SuspiciousPaymentHhs { get; set; }
    }
}