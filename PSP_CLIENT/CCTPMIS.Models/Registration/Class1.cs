﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTPMIS.Models.Registration
{
    public class EnrolmentSetup
    {
        public int FinancialYearId { get; set; }
        public int ExpansionStatusId   { get; set; }
        public int SublocationId  { get; set; }
        public int RegistrationGroupId { get; set; }
        public int Quota { get; set; }
    }

    public class RegistrationScreener
    {
        public DateTime RegistrationDate { get; set; }
        public int SublocationId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HhdIdNo { get; set; }
        public string HHdSex { get; set; }
        public DateTime? HhdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgidNo { get; set; }
        public string CgSex { get; set; }
        public DateTime? CgdoB { get; set; }
        public int HouseholdMembers { get; set; }
    }



}
