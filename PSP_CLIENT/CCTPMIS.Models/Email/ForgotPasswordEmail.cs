﻿namespace CCTPMIS.Models.Email
{
    public class ForgotPasswordEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }

        public int ValidityPeriod { get; set; }
    }
}