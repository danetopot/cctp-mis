﻿namespace CCTPMIS.Models.Email
{
    using System;

    public class MisEnrolmentFileEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Description { get; set; }

        public string FileChecksum { get; set; }

        public string FileName { get; set; }

        public string Password { get; set; }

        public string PspName { get; set; }
    }
}