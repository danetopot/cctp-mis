﻿namespace CCTPMIS.Models.Email
{
    using System.Web.Configuration;

    public class EmailGlobal 
    {
        public EmailGlobal()
        {
            FromEmail = WebConfigurationManager.AppSettings["EMAIL_FROM"];
            FromName = WebConfigurationManager.AppSettings["EMAIL_FROM_NAME"];
            PortalLink = WebConfigurationManager.AppSettings["SYSTEM_LINK"];
            PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"];
        }

        public string FirstName { get; set; }

        public string FromEmail { get; set; }

        public string FromName { get; set; }

        public string PortalLink { get; set; }

        public string PortalName { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }

        public string To { get; set; }
    }
}