﻿namespace CCTPMIS.Models.Email
{
    public class ResendCodeEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }
    }
}