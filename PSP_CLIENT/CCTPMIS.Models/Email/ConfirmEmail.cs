﻿namespace CCTPMIS.Models.Email
{
    public class ConfirmEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }

        public string Password { get; set; }
    }
}