﻿namespace CCTPMIS.Models.Email
{
    public class ChangePasswordEmail : EmailGlobal
    {
        public string Password { get; set; }
    }
}