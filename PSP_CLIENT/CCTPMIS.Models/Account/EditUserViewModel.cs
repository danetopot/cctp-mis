﻿namespace CCTPMIS.Models.Account
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
   // using System.Web.Mvc;

    public class EditUserViewModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare(
            "Password",
            ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        // [Required(AllowEmptyStrings = false)]
        // [Display(Name = "Email")]
        // [EmailAddress]
        // public string Email { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

   //     public ICollection<SelectListItem> GroupsList { get; set; }

        public int Id { get; set; }

        public string IsActive { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        [Required]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

       // public ICollection<SelectListItem> RolesList { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Display(Name = "User Group")]
        public int UserGroupId { get; set; }
    }
}