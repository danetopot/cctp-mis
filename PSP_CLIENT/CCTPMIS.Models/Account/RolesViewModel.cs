﻿namespace CCTPMIS.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class RolesViewModel
    {
        public string Description { get; set; }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}