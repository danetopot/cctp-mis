﻿namespace CCTPMIS.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class EditUserProfileViewModel
    {
        [StringLength(50)]
        public string Department { get; set; }

        [Display(Name = "First Name")]
        [StringLength(30)]
        public string FirstName { get; set; }

        public int Id { get; set; }

        [Display(Name = "Middle Name")]
        [StringLength(30)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string Organization { get; set; }

        [StringLength(20)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [StringLength(20)]
        public string Position { get; set; }

        [StringLength(30)]
        public string Surname { get; set; }
    }
}