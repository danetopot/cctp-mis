﻿namespace CCTPMIS.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class AvatarUploadViewModel
    {
        public string Avatar { get; set; }

        public string DisplayAvatar { get; set; }

        public string Message { get; set; }

        [Display(Name = "Profile Image")]
        public string ProfileImage { get; set; }

        [Required]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Upload { get; set; }
    }
}