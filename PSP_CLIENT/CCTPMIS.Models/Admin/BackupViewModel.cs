﻿namespace CCTPMIS.Models.Admin
{
    public class BackupViewModel
    {
        public string FilePath { get; set; }

        public string Message { get; set; }

        public bool Result { get; set; }
    }
}