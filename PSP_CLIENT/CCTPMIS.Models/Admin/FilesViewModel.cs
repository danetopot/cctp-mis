﻿namespace CCTPMIS.Models.Admin
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class FilesViewModel
    {
        [Display(Name = "File Created By ")]
        public int CreatedBy { get; set; }

        [Display(Name = "File Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "File Create Type")]
        public int CreationTypeId { get; set; }

        [Display(Name = "File Downloaded By")]
        public int? DownloadedBy { get; set; }

        [Display(Name = "File Downloaded On")]
        public DateTime? DownloadedOn { get; set; }

        [StringLength(128)]
        [Display(Name = "File Check Sum")]
        public string FileChecksum { get; set; }

        [StringLength(128)]
        [Display(Name = "File Check Sum")]
        public string FileChecksum2 { get; set; }

        [Display(Name = "File")]
        public int? FileCreationId { get; set; }

        public int? FileDownloadId { get; set; }

        [StringLength(128)]
        [Display(Name = "File path")]
        public string FilePath { get; set; }

        [Display(Name = "File")]
        public int Id { get; set; }

        [Display(Name = "File Name")]
        public string Name { get; set; }

        [Display(Name = "File Type")]
        public int TypeId { get; set; }
    }
}