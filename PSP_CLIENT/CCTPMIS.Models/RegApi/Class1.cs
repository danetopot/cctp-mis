﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace CCTPMIS.Models.RegApi
{
     
    public class InitialSetupVm
    {
        public EnumeratorVm Enumerator { get; set; }
        public List<LocationVm> Locations { get; set; }
        public List<SubLocationVm> SubLocations { get; set; }
        public List<SystemCodeVm> SystemCodes { get; set; }
        public List<SystemCodeDetailVm> SystemCodeDetails { get; set; }
        public  string Error { get; set; }
    }


    public class LoginVM
    {
        public string EmailAddress { get; set; }
        public string NationalIdNo { get; set; }
        public string Password { get; set; }
    }
}
