﻿namespace CCTPMIS.Models.RegApi
{
    public class ConstituencyVm : RegionVm
    {
        public int CountyId { get; set; }
    }
}