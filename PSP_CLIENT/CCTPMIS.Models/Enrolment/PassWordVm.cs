﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PassWordVm
    {
        [Display(Name = "File")]
        public int? FileCreationId { get; set; }
        [Display(Name = "File Password ")]
        public string FilePassword { get; set; }
    }
}