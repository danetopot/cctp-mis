﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PaymentCycleSummary
    {
        [Display(Name = "Start Month")]
        public int FromMonthId { get; set; }
        [Display(Name = "Frequency of Payment")]
        public int PaymentFrequency { get; set; }
        [Display(Name = "Frequency of Payment")]
        public int PaymentFrequencyId { get; set; }
        [Display(Name = "End Month")]
        public int ToMonthId { get; set; }
    }
}