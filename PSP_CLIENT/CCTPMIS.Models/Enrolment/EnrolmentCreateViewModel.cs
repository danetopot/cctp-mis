﻿namespace CCTPMIS.Models.Enrolment
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class EnrolmentCreateViewModel
    {
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        public List<EnrolTargetGroupViewModel> EnrolledGroups { get; set; }

        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        [Display(Name = "Total Enrolled Beneficiaries")]
        public int TotalEnrolled { get; set; }
    }
}