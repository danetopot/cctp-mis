﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PayrolApiStatus
    {
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Total Records Affected")]
        public int? NoOfRecs { get; set; }
        [Display(Name = "Status ID")]
        public int StatusId { get; set; }
    }
}