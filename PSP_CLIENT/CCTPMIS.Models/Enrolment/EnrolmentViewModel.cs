﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class EnrolmentViewModel
    {
        [Display(Name = "Total Number of Rows ")]
        public int NoOfRows { get; set; }
    }
}