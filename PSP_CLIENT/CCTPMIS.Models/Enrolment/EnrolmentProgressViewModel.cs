﻿namespace CCTPMIS.Models.Enrolment
{
    using System.Collections.Generic;

    public class EnrolmentProgressViewModel
    {
        public List<BankEnrolmentProgress> BankEnrolmentProgress { get; set; }

        public EnrolmentProgress EnrolmentProgress { get; set; }
    }

    // public class EnrolmentProgressViewModelDataMapper
    // {
    // private EnrolmentProgressViewModel GetEnrolmentProgressViewModelDataMapper(DbDataReader dataReader)
    // {
    // var enrolmentPlanData = new EnrolmentProgressViewModel
    // {
    // EnrolmentProgress = ((IObjectContextAdapter)ApplicationDbContext)
    // .ObjectContext
    // .Translate<EnrolmentProgress>(dataReader)
    // .ToList()
    // };
    // dataReader.NextResult();

    // enrolmentPlanData.BankEnrolmentProgress = ((IObjectContextAdapter)dbContext)
    // .ObjectContext
    // .Translate<Faq>(dataReader)
    // .ToList()
    // .FirstOrDefault();

    // }

    // }
}