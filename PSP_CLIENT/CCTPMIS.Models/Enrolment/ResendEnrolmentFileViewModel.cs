﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class ResendEnrolmentFileViewModel
    {
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = " ID")]
        public int Id { get; set; }
        [Display(Name = "User ID")]
        public int UserId { get; set; }
    }
}