﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class EnrolTargetGroupViewModel
    {
        [Display(Name = " Available Beneficiaries")]
        public int? AvailableNo { get; set; }
        [Display(Name = "Total Beneficiaries ")]
        public int? BeneficiaryNo { get; set; }
        [Display(Name = " Enrolled Beneficiaries")]
        public int? EnrolledNumbers { get; set; }
        [Display(Name = "Expansion Plan Beneficiaries ")]
        public int? ExpansionPlanNo { get; set; }
        [Display(Name = "Target Group ")]
        public int TargetGroupId { get; set; }
    }
}