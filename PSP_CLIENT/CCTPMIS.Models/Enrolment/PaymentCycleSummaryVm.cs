﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PaymentCycleSummaryVm
    {
        [Display(Name = "Entitlement Amount")]
        public decimal EntitlementAmount { get; set; }
    }
}