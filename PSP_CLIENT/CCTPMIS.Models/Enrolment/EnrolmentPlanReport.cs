﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class EnrolmentPlanReport
    {
        [Display(Name = "Programme ")]
        public string Programme { get; set; }
        [Display(Name = "Registered ")]
        public string Reg { get; set; }
    }
}