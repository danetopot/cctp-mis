﻿namespace CCTPMIS.Models.Enrolment
{
    using System.ComponentModel.DataAnnotations;

    public class EnrolmentProgress
    {
        [Display(Name = "Beneficiary Households/Individuals")]
        public int BeneHhs { get; set; }

        [Display(Name = "Enrolment Group")]
        public string EnrolmentGroup { get; set; }

        [Display(Name = "Enrolment Numbers by Programme")]
        public int EnrolmentNumbers { get; set; }

        [Display(Name = "Exp Plan Equal Share")]
        public int ExpPlanEqualShare { get; set; }

        [Display(Name = "Exp Plan Poverty Prioritized")]
        public int ExpPlanPovertyPrioritized { get; set; }

        [Display(Name = "#ID")]
        public int Id { get; set; }

        [Display(Name = "Programme")]
        public string Programme { get; set; }

        [Display(Name = "Registration Group")]
        public string RegGroup { get; set; }

        [Display(Name = "Registration Group Households/Individuals")]
        public int RegGroupHhs { get; set; }

        [Display(Name = "Status ")]
        public string Status { get; set; }
    }
}