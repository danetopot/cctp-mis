﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    using System;

    public class EnroledHhIndividual
    {
        [Display(Name = "Bank")]
        public string Bank { get; set; }

        [Display(Name = "Branch")]
        public string BankBranch { get; set; }

        [Display(Name = "Beneficiary DOB")]
        public DateTime? BeneDoB { get; set; }

        [Display(Name = "Beneficiary First Name")]
        public string BeneFirstName { get; set; }
        [Display(Name = "Beneficiary National ID Number")]
        public string BeneIdNo { get; set; }
        [Display(Name = "Beneficiary Middle Name")]
        public string BeneMiddleName { get; set; }
        [Display(Name = "beneficiary Sex")]
        public string BeneSex { get; set; }
        [Display(Name = "Beneficiary Surname")]
        public string BeneSurname { get; set; }
        [Display(Name = "Caregiver Date Of Birth")]
        public DateTime? CgdoB { get; set; }
        [Display(Name = "Caregiver First Name")]
        public string CgFirstName { get; set; }
        [Display(Name = "Caregiver National Id Number")]
        public string CgidNo { get; set; }
        [Display(Name = "Caregiver Middle Name ")]
        public string CgMiddleName { get; set; }
        [Display(Name = "Caregiver Sex")]
        public string CgSex { get; set; }
        [Display(Name = "Caregiver Surname")]
        public string CgSurname { get; set; }
        [Display(Name = "Caregiver Constituency ")]
        public string Constituency { get; set; }
        [Display(Name = "County ")]
        public string County { get; set; }
        [Display(Name = "District ")]
        public string District { get; set; }
        [Display(Name = "Division ")]
        public string Division { get; set; }
        [Display(Name = "Enrolment Number ")]
        public int EnrolmentNo { get; set; }
        [Display(Name = "Location ")]
        public string Location { get; set; }
        [Display(Name = "Mobile Number #1 ")]
        public string MobileNo1 { get; set; }
        [Display(Name = "Mobile Number #2 ")]
        public string MobileNo2 { get; set; }
        [Display(Name = "Programme No. ")]
        public string ProgrammeNo { get; set; }
        [Display(Name = "Status ")]
        public string Status { get; set; }
        [Display(Name = "Sub Location ")]
        public string SubLocation { get; set; }
    }
}