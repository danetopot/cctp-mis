﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCTPMIS.PspClient.MyAccount
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private async void btnLogin_ClickAsync(object sender, EventArgs e)
        {
            var username = this.txtEmailAddress.Text; var password = this.txtPassword.Text;
            var loginVm = new LoginViewModel { grant_type = "password", username = this.txtEmailAddress.Text, password = this.txtPassword.Text };

            var httpWebApi = new HttpWebApi(StaticInfo.BaseUrl);
            var bearer = await httpWebApi.Login(username, password);

            if (bearer.Contains("access_token"))
            {
                StaticInfo.BearerFeedback = bearer;
                StaticInfo.EmailAddress = username;
                StaticInfo.Password = password;

                MessageBox.Show("Login Successful", "The Login was successful", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            else
            {

                StaticInfo.BearerFeedback = "";
                StaticInfo.EmailAddress = "";
                StaticInfo.Password = "";

                MessageBox.Show("Login Failed", "The Login failed", MessageBoxButtons.RetryCancel,
                    MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtEmailAddress.Text = "";
            txtPassword.Text = "";

        }
    }
}
