﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCTPMIS.PspClient.MyAccount
{
    public partial class LogoutForm : Form
    {
        public LogoutForm()
        {
            InitializeComponent();
        }

        private void LogoutForm_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(StaticInfo.EmailAddress))
            {
                lblMessage.Text = @"You are logged in as "+ StaticInfo.EmailAddress;
                btnLogout.Visible = true;

            }
            else
            {
                lblMessage.Text = @"You are not logged in";
                btnLogout.Visible = false;
            }
           
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            StaticInfo.BearerFeedback = "";
            StaticInfo.EmailAddress = "";
            StaticInfo.Password = "";
            lblMessage.Text = @"You are now logged Out";
            btnLogout.Visible = false;
        }
    }
}
