﻿namespace CCTPMIS.PspClient
{
    public class Rootobject
    {

      
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }



        /// <summary>
        /// The Username.
        /// </summary>
        public string userName { get; set; }



        /// <summary>
        /// Issued.
        /// </summary>
        public string issued { get; set; }



        /// <summary>
        /// Expiry.
        /// </summary>
        public string expires { get; set; }
    }
}