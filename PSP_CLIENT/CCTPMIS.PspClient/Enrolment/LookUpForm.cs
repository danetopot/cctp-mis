﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace CCTPMIS.PspClient.Enrolment
{
    public partial class LookUpForm : Form
    {

        private static HttpClient client = new HttpClient();
        public LookUpForm()
        {
            InitializeComponent();

            client.BaseAddress = new Uri(StaticInfo.BaseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));



        }

        private async  void button1_Click(object sender, EventArgs e)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JsonConvert.DeserializeObject<Rootobject>(StaticInfo.BearerFeedback).access_token);
            var Authentication = JsonConvert.DeserializeObject<Rootobject>(StaticInfo.BearerFeedback);
            var httpWebApi = new HttpWebApi(StaticInfo.BaseUrl, Authentication);

            var dataToPost = new Dictionary<string, string>();
            dataToPost.Add("NationalIdNo", this.txtHHid.Text);

            var val = await httpWebApi.Post("/api/Beneficiaries/", dataToPost);

           
         
            var obj = JsonConvert.DeserializeObject<dynamic>(val);
            string json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            txtFeedback.Text = json;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtFeedback.Text = "";
            txtHHid.Text = "";
        }

        private void LookUpForm_Load(object sender, EventArgs e)
        {

        }
    }
}
