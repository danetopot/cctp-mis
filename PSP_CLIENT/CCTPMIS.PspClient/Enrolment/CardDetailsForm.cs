﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using CCTPMIS.Models.Api;
using Newtonsoft.Json;

namespace CCTPMIS.PspClient.Enrolment
{
    public partial class CardDetailsForm : Form
    {
       
        private static HttpClient client = new HttpClient();
        public CardDetailsForm()
        {
            InitializeComponent();

            client.BaseAddress = new Uri(StaticInfo.BaseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));           
        }



        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Excel Worksheets|*.csv*";
            string FileName = "";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
                txtFile.Text = FileName;
                string[] Lines = File.ReadAllLines(FileName);
                string[] Fields;
                Fields = Lines[0].Split(new char[] { ',' });
                int Cols = Fields.GetLength(0);
                DataTable dt = new DataTable();
                //1st row must be column names; force lower case to ensure matching later on.
                for (int i = 0; i < Cols; i++)
                    dt.Columns.Add(Fields[i].ToLower(), typeof(string));
                DataRow Row;
                for (int i = 1; i < Lines.GetLength(0); i++)
                {
                    Fields = Lines[i].Split(new char[] { ',' });
                    Row = dt.NewRow();
                    for (int f = 0; f < Cols; f++)
                        Row[f] = Fields[f];
                    dt.Rows.Add(Row);
                }
                dataGridView1.DataSource = dt;

            }

        }


        private async void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFile.Text))
            {
                MessageBox.Show("File is Required", "The File is required ", MessageBoxButtons.AbortRetryIgnore,
                    MessageBoxIcon.Error);
            }

            if (dataGridView1.RowCount <= 0)
                return;

            string[] Lines = File.ReadAllLines(txtFile.Text);
            string[] Fields;
            Fields = Lines[0].Split(new char[] { ',' });
            int Cols = Fields.GetLength(0);
            DataTable dt = new DataTable();
            //1st row must be column names; force lower case to ensure matching later on.
            for (int i = 0; i < Cols; i++)
                dt.Columns.Add(Fields[i], typeof(string));
            DataRow Row;
            for (int i = 1; i < Lines.GetLength(0); i++)
            {
                Fields = Lines[i].Split(new char[] { ',' });
                Row = dt.NewRow();
                for (int f = 0; f < Cols; f++)
                    Row[f] = Fields[f];
                dt.Rows.Add(Row);
            }


            var lst = dt.AsEnumerable()
                .Select(r => r.Table.Columns.Cast<DataColumn>()
                    .Select(c => new KeyValuePair<string, object>(c.ColumnName, r[c.Ordinal])
                    ).ToDictionary(z => z.Key, z => z.Value)
                ).ToList();


            var feedback = "";
            foreach (var item in lst)
            {

                Console.WriteLine(JsonConvert.SerializeObject(item));

                var enrolModel = JsonConvert.DeserializeObject<PspUpdatePaymentCardViewModel>(JsonConvert.SerializeObject(item));

                if (!string.IsNullOrEmpty(StaticInfo.BearerFeedback))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JsonConvert.DeserializeObject<Rootobject>(StaticInfo.BearerFeedback).access_token);

                }
                var Authentication = JsonConvert.DeserializeObject<Rootobject>(StaticInfo.BearerFeedback);
                try
                {
                    var httpWebApi = new HttpWebApi(StaticInfo.BaseUrl, Authentication);
                    var val = await httpWebApi.EnrolCardDetails("/api/Enrol/PaymentCard/", enrolModel).ConfigureAwait(false);
                    feedback += "\n" + val;

                    Console.WriteLine($@"{enrolModel.EnrolmentNo} : {val}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($@"{enrolModel.EnrolmentNo} : {ex.Message}");
                    feedback += "\n" + ex.Message;
                }

            }


            MessageBox.Show(feedback, "Response From Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
