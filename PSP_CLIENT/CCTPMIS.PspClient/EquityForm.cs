﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCTPMIS.PspClient
{
    using System.Net.Http;
    using System.Net.Http.Headers;

    using Newtonsoft.Json;

    public partial class EquityForm : Form
    {
      

        private static readonly HttpClient client = new HttpClient();
        public EquityForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var x = Login("bank1@outlook.com", "password");



        }

        public async Task<string> Post(string path, Dictionary<string, string> values)
        {

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            // Encode Values:
            var content = new FormUrlEncodedContent(values);


            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        TokenResponse Authentication { get; set; }
        public string BaseUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }


        public async Task<string> Login(string username, string password)
        {

            // Set Username:
            this.Username = username;

            // Set Password:
            this.Password = password;

            // Conf String to Post:
            var Dic = new Dictionary<string, string>() { { "grant_type", "password" }, { "username", string.Empty }, { "password", string.Empty } };
            Dic["username"] = username;
            Dic["password"] = password;

            // Post to Controller:
            string auth = await this.Post("/Token", Dic);

            // Deserialise Response:
            this.Authentication = JsonConvert.DeserializeObject<TokenResponse>(await this.Post("/Token", Dic));
            var x = this.Authentication.access_token;
            return auth;
        }

    }
    public class TokenResponse
    {


        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }

}
