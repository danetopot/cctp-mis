﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCTPMIS.PspClient.Enrolment;
using CCTPMIS.PspClient.MyAccount;
using CCTPMIS.PspClient.Payments;
using Microsoft.Win32.SafeHandles;

namespace CCTPMIS.PspClient
{
    public partial class CCTP_MDI : Form
    {
        private int childFormNumber = 0;


        public string AccessToken = "";
        public CCTP_MDI()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void enrolmentToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowLoginForm();
        }

        private void CCTP_MDI_Load(object sender, EventArgs e)
        {
          // StaticInfo.BaseUrl = "https://www.inuajamii.go.ke:2011";
           StaticInfo.BaseUrl = "https://www.inuajamii.go.ke:2011";
        //    StaticInfo.BaseUrl = "http://localhost:52577";
            WindowState = FormWindowState.Maximized;
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var logoutForm = new LogoutForm
            {
                MdiParent = this,
                Text = @"Logout to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            logoutForm.Show();
        }

        private void lookUpToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }

            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var logoutForm = new LookUpForm
            {
                MdiParent = this,
                Text = @"Look up Beneficiary",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized
            };
            logoutForm.Show();
        }



        protected void ShowLoginForm()
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new LoginForm
            {
                MdiParent = this,
                Text = @"Login to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }

        private void enrolBeneficiaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new EnrolForm
            {
                MdiParent = this,
                Text = @"Enrol beneficiary into Jamii Programme",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }

        private void enrolCaregiverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new CaregiverForm()
            {
                MdiParent = this,
                Text = @"Enrol Caregiver into  Programme",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }

        private void submitCardDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new CardDetailsForm()
            {
                MdiParent = this,
                Text = @"Submit Card Details into  Programme",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }

        private void creditReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new CreditReportForm()
            {
                MdiParent = this,
                Text = @"Credit Report",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }

        private void monthlyActivityReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(StaticInfo.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new MontlyActivityForm()
            {
                MdiParent = this,
                Text = @" Montly Activity ",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }
    }
}
