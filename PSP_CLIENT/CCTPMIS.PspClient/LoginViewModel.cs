﻿namespace CCTPMIS.PspClient
{
    public class LoginViewModel
    {
        public string grant_type { get; set; }

        public string username { get; set; }
        public string password { get; set; }
    }


    public static class StaticInfo
    {
        public static string EmailAddress;
        public static string BaseUrl;
        public static string Username;
        public static string Password;
        public static string AccessToken;
        //public static string Baseurl;
        public static string BearerFeedback;

         
    }
}