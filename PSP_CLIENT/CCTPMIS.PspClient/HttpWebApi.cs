﻿using System.Diagnostics;

namespace CCTPMIS.PspClient
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    using CCTPMIS.Models.Api;
    using CCTPMIS.Models.Enrolment;

    using Newtonsoft.Json;

    public class HttpWebApi
    {



        #region Fields:



        private static readonly HttpClient client = new HttpClient();



        #endregion



        #region Properties:



        /// <summary>
        /// The basr Uri.
        /// </summary>
        public string BaseUrl { get; set; }



        /// <summary>
        /// Username.
        /// </summary>
        protected internal string Username { get; set; }



        /// <summary>
        /// Password.
        /// </summary>
        protected internal string Password { get; set; }



        /// <summary>
        /// The instance of the Root Object Json Deserialised Class.
        /// </summary>
         Rootobject Authentication { get; set; }



        /// <summary>
        /// The Access Token from the Json Deserialised Login.
        /// </summary>
        public string AccessToken { get; set; }


        

        #endregion

        

        public HttpWebApi(string baseurl)
        {

            // Init Base Url:
            this.BaseUrl = baseurl;
        }
        public HttpWebApi(string baseurl, Rootobject auth)
        {

            // Init Base Url:
            this.BaseUrl = baseurl;
            Authentication = auth;
        }

        public HttpWebApi(  Rootobject auth)
        {

            // Init Base Url:
            
            Authentication = auth;
        }

        public async Task<string> Get(string path)
        {

            if (this.Authentication.access_token == null)
                throw new Exception("Authentication is not completed.");

            // GET
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            var result = await client.GetStringAsync(this.BaseUrl + path);
            return result;
        }



       
        public async Task<string> Login(string username, string password)
        {

            // Set Username:
            this.Username = username;

            // Set Password:
            this.Password = password;

            // Conf String to Post:
            var Dic = new Dictionary<string, string>() { { "grant_type", "password" }, { "username", string.Empty }, { "password", string.Empty } };
            Dic["username"] = username;
            Dic["password"] = password;

            // Post to Controller:
            string auth =   await this.Post("/Token", Dic);

            Debug.WriteLine(auth);
            // Deserialise Response:
             this.Authentication = JsonConvert.DeserializeObject<Rootobject>(auth);
           var x = this.Authentication.access_token;
            return auth;
        }

 
        public async Task<string> Post(string path, Dictionary<string, string> values)
        {

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            // Encode Values:
            var content = new FormUrlEncodedContent(values);
            

            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> EnrolCareGiver(string path, PspEnrolCareGiverViewModel values)
        {

            //   var equity_access_token = "lxq_x6fSkJ-UXQ3Hq7mJJNXbC_1Ymw-EM5VVuUgdPqTa0CyLlfyRJsGq9Q-5zF4VqMtTtYGI9Wc6ifLyq2pBnsXTD_lYvUI1D9Z541isDSO3jOPN4vDCzxibW-qrPAr2d9b-yDxLwmjfAa4MHmRYtT2o1lm9fQV4-acBoXMKD9DGAdm9t1uJTyZlJohggrYIJzvdz8Vl9NOX_cRmHrNqlx5lYZ5gMf-6ZFRkq2aJuubuDeXhXbzN2dhCAKgRlNumOrExI7Ry6l_OIf4kHfPmz2ZBd1wIn9A7OgUZYJ4qxD-4JmsJG7OJ5ERCUg0SZsLwPCgocfGloanYdMJixCByFlyBrMqWZ20L_JNczKjfvp4arbmMI73G6rgMl4kHiuXre_67epcnVe9HB3imxNs5JkbFjYpF5wgyL-iUvoD14lAGoNQBmF2V9YsTOOc-rdsu7zA9CuYYD_VE4RYI6rU6tw";

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", equity_access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }
        public async Task<string> EnrolCardDetails(string path, PspUpdatePaymentCardViewModel values)
        {

            //   var equity_access_token = "lxq_x6fSkJ-UXQ3Hq7mJJNXbC_1Ymw-EM5VVuUgdPqTa0CyLlfyRJsGq9Q-5zF4VqMtTtYGI9Wc6ifLyq2pBnsXTD_lYvUI1D9Z541isDSO3jOPN4vDCzxibW-qrPAr2d9b-yDxLwmjfAa4MHmRYtT2o1lm9fQV4-acBoXMKD9DGAdm9t1uJTyZlJohggrYIJzvdz8Vl9NOX_cRmHrNqlx5lYZ5gMf-6ZFRkq2aJuubuDeXhXbzN2dhCAKgRlNumOrExI7Ry6l_OIf4kHfPmz2ZBd1wIn9A7OgUZYJ4qxD-4JmsJG7OJ5ERCUg0SZsLwPCgocfGloanYdMJixCByFlyBrMqWZ20L_JNczKjfvp4arbmMI73G6rgMl4kHiuXre_67epcnVe9HB3imxNs5JkbFjYpF5wgyL-iUvoD14lAGoNQBmF2V9YsTOOc-rdsu7zA9CuYYD_VE4RYI6rU6tw";

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", equity_access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }
        public async Task<string> Enrol(string path, PspEnrolBeneficiaryViewModel values)
        {

         //   var equity_access_token = "lxq_x6fSkJ-UXQ3Hq7mJJNXbC_1Ymw-EM5VVuUgdPqTa0CyLlfyRJsGq9Q-5zF4VqMtTtYGI9Wc6ifLyq2pBnsXTD_lYvUI1D9Z541isDSO3jOPN4vDCzxibW-qrPAr2d9b-yDxLwmjfAa4MHmRYtT2o1lm9fQV4-acBoXMKD9DGAdm9t1uJTyZlJohggrYIJzvdz8Vl9NOX_cRmHrNqlx5lYZ5gMf-6ZFRkq2aJuubuDeXhXbzN2dhCAKgRlNumOrExI7Ry6l_OIf4kHfPmz2ZBd1wIn9A7OgUZYJ4qxD-4JmsJG7OJ5ERCUg0SZsLwPCgocfGloanYdMJixCByFlyBrMqWZ20L_JNczKjfvp4arbmMI73G6rgMl4kHiuXre_67epcnVe9HB3imxNs5JkbFjYpF5wgyL-iUvoD14lAGoNQBmF2V9YsTOOc-rdsu7zA9CuYYD_VE4RYI6rU6tw";

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                   client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
                   // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", equity_access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

          var  content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
          //  var content = new FormUrlEncodedContent(dic);

          //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
              // Post and get Response:
              var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> CreditReport(string path, CreditReportViewModel values)
        {

 
            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
 
            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> CreditReportFinalize(string path, CreditReportFinalizeViewModel values)
        {

            //   var equity_access_token = "lxq_x6fSkJ-UXQ3Hq7mJJNXbC_1Ymw-EM5VVuUgdPqTa0CyLlfyRJsGq9Q-5zF4VqMtTtYGI9Wc6ifLyq2pBnsXTD_lYvUI1D9Z541isDSO3jOPN4vDCzxibW-qrPAr2d9b-yDxLwmjfAa4MHmRYtT2o1lm9fQV4-acBoXMKD9DGAdm9t1uJTyZlJohggrYIJzvdz8Vl9NOX_cRmHrNqlx5lYZ5gMf-6ZFRkq2aJuubuDeXhXbzN2dhCAKgRlNumOrExI7Ry6l_OIf4kHfPmz2ZBd1wIn9A7OgUZYJ4qxD-4JmsJG7OJ5ERCUg0SZsLwPCgocfGloanYdMJixCByFlyBrMqWZ20L_JNczKjfvp4arbmMI73G6rgMl4kHiuXre_67epcnVe9HB3imxNs5JkbFjYpF5wgyL-iUvoD14lAGoNQBmF2V9YsTOOc-rdsu7zA9CuYYD_VE4RYI6rU6tw";

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", equity_access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> ActivityReport(string path, AccountActivityReportViewModel values)
        {


            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> ActivityReportFinalize(string path, AccountActivityReportFinalizeViewModel values)
        {

            //   var equity_access_token = "lxq_x6fSkJ-UXQ3Hq7mJJNXbC_1Ymw-EM5VVuUgdPqTa0CyLlfyRJsGq9Q-5zF4VqMtTtYGI9Wc6ifLyq2pBnsXTD_lYvUI1D9Z541isDSO3jOPN4vDCzxibW-qrPAr2d9b-yDxLwmjfAa4MHmRYtT2o1lm9fQV4-acBoXMKD9DGAdm9t1uJTyZlJohggrYIJzvdz8Vl9NOX_cRmHrNqlx5lYZ5gMf-6ZFRkq2aJuubuDeXhXbzN2dhCAKgRlNumOrExI7Ry6l_OIf4kHfPmz2ZBd1wIn9A7OgUZYJ4qxD-4JmsJG7OJ5ERCUg0SZsLwPCgocfGloanYdMJixCByFlyBrMqWZ20L_JNczKjfvp4arbmMI73G6rgMl4kHiuXre_67epcnVe9HB3imxNs5JkbFjYpF5wgyL-iUvoD14lAGoNQBmF2V9YsTOOc-rdsu7zA9CuYYD_VE4RYI6rU6tw";

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);
            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", equity_access_token);

            var dic = ObjectToDictionary(values);

            var jsonString = JsonConvert.SerializeObject(values);

            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            // Encode Values:
            //  var content = new FormUrlEncodedContent(dic);

            //  request.Content = new StringContent(html, Encoding.UTF8, "text/plain");
            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }


        public async Task<string> Register(string username, string password)
        {

            // Register: api/Account/Register
            var Dic = new Dictionary<string, string>() { { "Email", string.Empty }, { "Password", string.Empty }, { "ConfirmPassword", string.Empty } };
            Dic["Email"] = username;
            Dic["Password"] = password;
            Dic["ConfirmPassword"] = password;

            return await this.Post("/api/Account/Register", Dic);
        }

        public static Dictionary<string, string> ObjectToDictionary(object obj)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                string propName = prop.Name;
                var val = obj.GetType().GetProperty(propName).GetValue(obj, null);
                if (val != null)
                {
                    ret.Add(propName, val.ToString());
                }
                else
                {
                    ret.Add(propName, null);
                }
            }

            return ret;
        }


    }
}