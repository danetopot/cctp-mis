﻿namespace CCTPMIS.PspClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHHid = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtBearer = new System.Windows.Forms.TextBox();
            this.lblHH = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.txtEnrolment = new System.Windows.Forms.TextBox();
            this.txtResultEnrol = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnTestFingerptint = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnCreditReport = new System.Windows.Forms.Button();
            this.btnCreditReportFinalize = new System.Windows.Forms.Button();
            this.btnCredirReportBulk = new System.Windows.Forms.Button();
            this.btnCreditFinalize = new System.Windows.Forms.Button();
            this.btnActivityReport = new System.Windows.Forms.Button();
            this.btnActivityreportFinalize = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(34, 302);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(536, 45);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(34, 112);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(536, 31);
            this.txtEmail.TabIndex = 1;
            this.txtEmail.Text = "anonymas.logon@gmail.com";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(34, 224);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(536, 31);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.Text = "password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // txtHHid
            // 
            this.txtHHid.Location = new System.Drawing.Point(34, 506);
            this.txtHHid.Name = "txtHHid";
            this.txtHHid.Size = new System.Drawing.Size(549, 31);
            this.txtHHid.TabIndex = 5;
            this.txtHHid.TextChanged += new System.EventHandler(this.txtHHid_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 718);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(549, 49);
            this.button1.TabIndex = 6;
            this.button1.Text = "Filter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtBearer
            // 
            this.txtBearer.Location = new System.Drawing.Point(34, 364);
            this.txtBearer.Multiline = true;
            this.txtBearer.Name = "txtBearer";
            this.txtBearer.Size = new System.Drawing.Size(536, 82);
            this.txtBearer.TabIndex = 7;
            // 
            // lblHH
            // 
            this.lblHH.Location = new System.Drawing.Point(29, 588);
            this.lblHH.MaximumSize = new System.Drawing.Size(450, 0);
            this.lblHH.Name = "lblHH";
            this.lblHH.Size = new System.Drawing.Size(450, 0);
            this.lblHH.TabIndex = 8;
            this.lblHH.Text = "The Search Result will be here";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 467);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(296, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "National Id Number to Lookup";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(777, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(478, 187);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enrolment Test";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Results Come here";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(120, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(265, 36);
            this.button2.TabIndex = 0;
            this.button2.Text = "Enroll Member";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtEnrolment
            // 
            this.txtEnrolment.Location = new System.Drawing.Point(777, 277);
            this.txtEnrolment.Multiline = true;
            this.txtEnrolment.Name = "txtEnrolment";
            this.txtEnrolment.Size = new System.Drawing.Size(478, 202);
            this.txtEnrolment.TabIndex = 1;
            this.txtEnrolment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResultEnrol
            // 
            this.txtResultEnrol.Location = new System.Drawing.Point(777, 525);
            this.txtResultEnrol.Multiline = true;
            this.txtResultEnrol.Name = "txtResultEnrol";
            this.txtResultEnrol.Size = new System.Drawing.Size(478, 210);
            this.txtResultEnrol.TabIndex = 11;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(226, 851);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(357, 50);
            this.button3.TabIndex = 12;
            this.button3.Text = "Test Equity";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnTestFingerptint
            // 
            this.btnTestFingerptint.Location = new System.Drawing.Point(226, 591);
            this.btnTestFingerptint.Name = "btnTestFingerptint";
            this.btnTestFingerptint.Size = new System.Drawing.Size(245, 44);
            this.btnTestFingerptint.TabIndex = 13;
            this.btnTestFingerptint.Text = "Test Finger Print Submission";
            this.btnTestFingerptint.UseVisualStyleBackColor = true;
            this.btnTestFingerptint.Click += new System.EventHandler(this.btnTestFingerptint_ClickAsync);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(763, 839);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(498, 61);
            this.button4.TabIndex = 14;
            this.button4.Text = "Send FromTextFile";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnCreditReport
            // 
            this.btnCreditReport.Location = new System.Drawing.Point(1373, 74);
            this.btnCreditReport.Name = "btnCreditReport";
            this.btnCreditReport.Size = new System.Drawing.Size(186, 69);
            this.btnCreditReport.TabIndex = 15;
            this.btnCreditReport.Text = "Credit Report";
            this.btnCreditReport.UseVisualStyleBackColor = true;
            this.btnCreditReport.Click += new System.EventHandler(this.btnCreditReport_ClickAsync);
            // 
            // btnCreditReportFinalize
            // 
            this.btnCreditReportFinalize.Location = new System.Drawing.Point(1565, 74);
            this.btnCreditReportFinalize.Name = "btnCreditReportFinalize";
            this.btnCreditReportFinalize.Size = new System.Drawing.Size(230, 69);
            this.btnCreditReportFinalize.TabIndex = 16;
            this.btnCreditReportFinalize.Text = "Credit Report Finalize";
            this.btnCreditReportFinalize.UseVisualStyleBackColor = true;
            this.btnCreditReportFinalize.Click += new System.EventHandler(this.btnCreditReportFinalize_Click);
            // 
            // btnCredirReportBulk
            // 
            this.btnCredirReportBulk.Location = new System.Drawing.Point(1373, 172);
            this.btnCredirReportBulk.Name = "btnCredirReportBulk";
            this.btnCredirReportBulk.Size = new System.Drawing.Size(422, 60);
            this.btnCredirReportBulk.TabIndex = 17;
            this.btnCredirReportBulk.Text = "Credit Report Loop Save";
            this.btnCredirReportBulk.UseVisualStyleBackColor = true;
            this.btnCredirReportBulk.Click += new System.EventHandler(this.btnCredirReportBulk_Click);
            // 
            // btnCreditFinalize
            // 
            this.btnCreditFinalize.Location = new System.Drawing.Point(1373, 265);
            this.btnCreditFinalize.Name = "btnCreditFinalize";
            this.btnCreditFinalize.Size = new System.Drawing.Size(422, 60);
            this.btnCreditFinalize.TabIndex = 18;
            this.btnCreditFinalize.Text = "Credit Report Finalize";
            this.btnCreditFinalize.UseVisualStyleBackColor = true;
            this.btnCreditFinalize.Click += new System.EventHandler(this.btnCreditFinalize_Click);
            // 
            // btnActivityReport
            // 
            this.btnActivityReport.Location = new System.Drawing.Point(1373, 364);
            this.btnActivityReport.Name = "btnActivityReport";
            this.btnActivityReport.Size = new System.Drawing.Size(422, 61);
            this.btnActivityReport.TabIndex = 19;
            this.btnActivityReport.Text = "Activity Report Loop";
            this.btnActivityReport.UseVisualStyleBackColor = true;
            this.btnActivityReport.Click += new System.EventHandler(this.btnActivityReport_Click);
            // 
            // btnActivityreportFinalize
            // 
            this.btnActivityreportFinalize.Location = new System.Drawing.Point(1373, 452);
            this.btnActivityreportFinalize.Name = "btnActivityreportFinalize";
            this.btnActivityreportFinalize.Size = new System.Drawing.Size(422, 55);
            this.btnActivityreportFinalize.TabIndex = 20;
            this.btnActivityreportFinalize.Text = " Activity Report Finalize";
            this.btnActivityreportFinalize.UseVisualStyleBackColor = true;
            this.btnActivityreportFinalize.Click += new System.EventHandler(this.btnActivityreportFinalize_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1871, 922);
            this.Controls.Add(this.btnActivityreportFinalize);
            this.Controls.Add(this.btnActivityReport);
            this.Controls.Add(this.btnCreditFinalize);
            this.Controls.Add(this.btnCredirReportBulk);
            this.Controls.Add(this.btnCreditReportFinalize);
            this.Controls.Add(this.btnCreditReport);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnTestFingerptint);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtResultEnrol);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtEnrolment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblHH);
            this.Controls.Add(this.txtBearer);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtHHid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.btnLogin);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHHid;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtBearer;
        private System.Windows.Forms.Label lblHH;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtEnrolment;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtResultEnrol;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnTestFingerptint;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnCreditReport;
        private System.Windows.Forms.Button btnCreditReportFinalize;
        private System.Windows.Forms.Button btnCredirReportBulk;
        private System.Windows.Forms.Button btnCreditFinalize;
        private System.Windows.Forms.Button btnActivityReport;
        private System.Windows.Forms.Button btnActivityreportFinalize;
    }
}

