﻿namespace CCTPMIS.PspClient.Payments
{
    partial class MontlyActivityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFinalize = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 336);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(2252, 894);
            this.dataGridView1.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1884, 101);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(343, 51);
            this.button2.TabIndex = 6;
            this.button2.Text = "Send Monthly Activity Finalize";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtFinalize
            // 
            this.txtFinalize.Location = new System.Drawing.Point(1453, 34);
            this.txtFinalize.Name = "txtFinalize";
            this.txtFinalize.Size = new System.Drawing.Size(425, 31);
            this.txtFinalize.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1884, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(343, 49);
            this.button1.TabIndex = 4;
            this.button1.Text = "Browse Finalize File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(480, 124);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(255, 51);
            this.button4.TabIndex = 11;
            this.button4.Text = "Send Monthly Activity";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(13, 34);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(456, 31);
            this.txtFile.TabIndex = 10;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(480, 34);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(255, 49);
            this.button5.TabIndex = 9;
            this.button5.Text = "Browse Activity File";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // MontlyActivityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2252, 1230);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtFinalize);
            this.Controls.Add(this.button1);
            this.Name = "MontlyActivityForm";
            this.Text = "MontlyActivityForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtFinalize;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button button5;
    }
}