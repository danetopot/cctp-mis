﻿namespace CCTPMIS.PspClient.Payments
{
    partial class CreditReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnFinalizeAll = new System.Windows.Forms.Button();
            this.txtFinalize = new System.Windows.Forms.TextBox();
            this.btnFinalizeFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 115);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(2212, 894);
            this.dataGridView1.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(850, 29);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(236, 51);
            this.button2.TabIndex = 6;
            this.button2.Text = "Send Credit Report";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(65, 41);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(533, 31);
            this.txtFile.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(641, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 49);
            this.button1.TabIndex = 4;
            this.button1.Text = "Browse File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnFinalizeAll
            // 
            this.btnFinalizeAll.Location = new System.Drawing.Point(1945, 31);
            this.btnFinalizeAll.Name = "btnFinalizeAll";
            this.btnFinalizeAll.Size = new System.Drawing.Size(236, 51);
            this.btnFinalizeAll.TabIndex = 8;
            this.btnFinalizeAll.Text = "Credit Report Finalize";
            this.btnFinalizeAll.UseVisualStyleBackColor = true;
            this.btnFinalizeAll.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtFinalize
            // 
            this.txtFinalize.Location = new System.Drawing.Point(1275, 41);
            this.txtFinalize.Name = "txtFinalize";
            this.txtFinalize.Size = new System.Drawing.Size(432, 31);
            this.txtFinalize.TabIndex = 10;
            // 
            // btnFinalizeFile
            // 
            this.btnFinalizeFile.Location = new System.Drawing.Point(1734, 31);
            this.btnFinalizeFile.Name = "btnFinalizeFile";
            this.btnFinalizeFile.Size = new System.Drawing.Size(180, 49);
            this.btnFinalizeFile.TabIndex = 9;
            this.btnFinalizeFile.Text = "Browse File";
            this.btnFinalizeFile.UseVisualStyleBackColor = true;
            this.btnFinalizeFile.Click += new System.EventHandler(this.btnFinalize_Click);
            // 
            // CreditReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2212, 1009);
            this.Controls.Add(this.txtFinalize);
            this.Controls.Add(this.btnFinalizeFile);
            this.Controls.Add(this.btnFinalizeAll);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.button1);
            this.Name = "CreditReportForm";
            this.Text = "CreditReportForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnFinalizeAll;
        private System.Windows.Forms.TextBox txtFinalize;
        private System.Windows.Forms.Button btnFinalizeFile;
    }
}