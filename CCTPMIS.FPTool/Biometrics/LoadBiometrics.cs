﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace CCTPMIS.FPTool.Biometrics
{
    public partial class LoadBiometrics : Form
    {
        public LoadBiometrics()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DirectoryInfo dinfo = new DirectoryInfo(ParametersGetter.BiometricsFolder);
            FileInfo[] Files = dinfo.GetFiles("*.json");
            DataTable dt = new DataTable();
            dt.Columns.Add("File", typeof(string));
            foreach (FileInfo file in Files)
            {
                dt.Rows.Add(file.Name.Replace(".json", ""));
            }
            dataGridView1.DataSource = dt;
        }

        private void btnPostBiometrics_Click(object sender, EventArgs e)
        {

            DirectoryInfo dinfo = new DirectoryInfo(ParametersGetter.BiometricsFolder);
            FileInfo[] Files = dinfo.GetFiles("*.json");
            DataTable dt = new DataTable();
            dt.Columns.Add("File", typeof(string));
            foreach (FileInfo file in Files)
            {
                dt.Rows.Add(file.Name.Replace(".json", ""));
            }
            string fileName;
            string fileContent;

            var apiFeedback = new ApiStatus();

            try
            {

                using (var connection = new SqlConnection(ParametersGetter.defaultConnection))
                {
                    foreach (var file in Files)
                    {
                        connection.Open();
                        fileName = ParametersGetter.BiometricsFolder + file.Name;
                        fileContent = File.ReadAllText(fileName);
                        var model = JsonConvert.DeserializeObject<PspEnrolBeneficiaryViewModel>(fileContent);

                        var cmd = new SqlCommand("dbo.AddEditPaymentCardBiometrics", connection) {

                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add(new SqlParameter("@BenePaymentCardId", file.Name.Replace(".json", "")));
                        if (!string.IsNullOrEmpty(model.SecReciRI))
                        {
                            cmd.Parameters.Add(new SqlParameter("PriReciRT",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRT)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRI",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRI)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRMF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRMF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRRF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRRF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRP",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRP)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLT",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLT)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLI",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLI)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLMF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLMF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLRF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLRF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLP",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLP)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRT",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciRT)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRI",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciRI)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRMF",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciRMF)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRRF",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciRRF)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRP",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciRP)));
                            cmd.Parameters.Add(new SqlParameter("SecReciLT",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciLT)));
                            cmd.Parameters.Add(new SqlParameter("SecReciLI",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciLI)));
                            cmd.Parameters.Add(new SqlParameter("SecReciLMF",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciLMF)));
                            cmd.Parameters.Add(new SqlParameter("SecReciLRF",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciLRF)));
                            cmd.Parameters.Add(new SqlParameter("SecReciLP",
                                System.Text.Encoding.ASCII.GetBytes(model.SecReciLP)));
                            cmd.Parameters.Add(new SqlParameter("UserId", model.UserId));

                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("PriReciRT",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRT)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRI",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRI)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRMF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRMF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRRF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRRF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciRP",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciRP)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLT",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLT)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLI",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLI)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLMF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLMF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLRF",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLRF)));
                            cmd.Parameters.Add(new SqlParameter("PriReciLP",
                                System.Text.Encoding.ASCII.GetBytes(model.PriReciLP)));
                            cmd.Parameters.Add(new SqlParameter("SecReciRT", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciRI", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciRMF", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciRRF", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciRP", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciLT", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciLI", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciLMF", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciLRF", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("SecReciLP", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("UserId", model.UserId));
                        }

                        try
                        {
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                FileLog("SUCCESS_", $@"{file.Name.Replace(".json", "")}", $@"{reader[0]}");
                                //Console.WriteLine();
                            }
                        }
                        catch (Exception exception)
                        {
                            FileLog("ERROR_", $@"{file.Name.Replace(".json", "")} , {exception.Message}","");

                         }

                        connection.Close();


                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                //FileLog("ERROR", ex.Message,"");
            }

        }


        public void FileLog(string dataType, string data, string userId )
        {
            string t;
            int seconds;
            string todaydate, hour;
            var dt = DateTime.Now;
            seconds = dt.Second;
            todaydate = dt.Date.ToString("yyyy-MM-dd");
            hour = dt.TimeOfDay.Hours.ToString();
            

            t = dt.ToString("T");
            var fs = new FileStream(
                $"{AppDomain.CurrentDomain.BaseDirectory}\\{dataType}-{todaydate}-{hour}-00.txt",
                FileMode.OpenOrCreate,
                FileAccess.Write);
            using (var mStreamWriter = new StreamWriter(fs))
            {
                mStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                mStreamWriter.WriteLine($"[{{ {dt:s},{userId??""},{data} }}]");
                mStreamWriter.WriteLine("");
                mStreamWriter.Flush();
                mStreamWriter.Close();
            }
        }
    }

}
