﻿namespace CCTPMIS.FPTool.Biometrics
{
    partial class LoadBiometrics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnPostBiometrics = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1168, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(307, 72);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load Biometrics File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnPostBiometrics
            // 
            this.btnPostBiometrics.Location = new System.Drawing.Point(1510, 106);
            this.btnPostBiometrics.Name = "btnPostBiometrics";
            this.btnPostBiometrics.Size = new System.Drawing.Size(263, 72);
            this.btnPostBiometrics.TabIndex = 1;
            this.btnPostBiometrics.Text = "Post Biometrics";
            this.btnPostBiometrics.UseVisualStyleBackColor = true;
            this.btnPostBiometrics.Click += new System.EventHandler(this.btnPostBiometrics_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(7, 211);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 33;
            this.dataGridView1.Size = new System.Drawing.Size(1766, 610);
            this.dataGridView1.TabIndex = 2;
            // 
            // LoadBiometrics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1863, 866);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnPostBiometrics);
            this.Controls.Add(this.button1);
            this.Name = "LoadBiometrics";
            this.Text = "LoadBiometrics";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnPostBiometrics;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}