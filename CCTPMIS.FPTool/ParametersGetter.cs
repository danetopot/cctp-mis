﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CCTPMIS.FPTool
{
    internal static class ParametersGetter
    {

        public static bool print_for_invoicing = false;
        public static bool print_for_file = false;
        public static SqlConnection SqlConnect = new SqlConnection();
        public static DataTable Rights_formTable = new DataTable();
        public static string curuser = "";
        public static DataTable DataUsers = new DataTable();
        public static string client_prefix = "";
        public static DataSet ClaimsReport = new DataSet();
        public static DataTable Getpoint_id = new DataTable();
        public static DataTable GetClinic = new DataTable();
        public static DataTable GetClientNames = new DataTable();
        public static DataTable GetSchemes = new DataTable();
        public static DataSet st = new DataSet();


        public static string defaultConnection = "";
        public static string biometricsConnection = "";
        public static string legacyConnection = "";
        public static string elmahConnection = "";
        public static string validationApi = "";
        public static string BiometricsFolder = "";

        public static string EmailAddress = "";
        public static string BaseUrl = "";
        public static string Username = "";
        public static string Password = "";
        public static string AccessToken = "";
        public static string BearerFeedback = "";


        public static string GetLocalValues(string value)
        {

            if (st.Tables[0].Rows.Count > 0)
            {

                return st.Tables[0].Rows[0][value].ToString();
            }
            else
            {
                MessageBox.Show("Local Database Access not set");
                return "";

            }
        }

    }
}