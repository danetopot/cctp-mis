﻿using System;
using System.IO;
using System.Windows.Forms;
using CCTPMIS.FPTool.Biometrics;
using CCTPMIS.FPTool.MyAccount;

namespace CCTPMIS.FPTool
{
    public partial class FPTool_MDI : Form
    {
        private int childFormNumber = 0;

        public FPTool_MDI()
        {
            InitializeComponent();
            loadParameters($"{AppDomain.CurrentDomain.BaseDirectory}\\Setup.xml");

        }
        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form {
                MdiParent = this,
                Text = "Window " + childFormNumber++
            };
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
            };
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //  toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //  statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void toolsMenu_Click(object sender, EventArgs e)
        {

        }

        private void fpProcessToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FPTool_MDI_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
            var logoutForm = new LoginForm {
                MdiParent = this,
                Text = @"Login to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            logoutForm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var logoutForm = new LogoutForm {
                MdiParent = this,
                Text = @"Logout to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            logoutForm.Show();
        }

        protected void ShowLoginForm()
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }

            var loginForm = new LoginForm {
                MdiParent = this,
                Text = @"Login to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized

            };
            loginForm.Show();
        }
        private void loadParameters(string str)
        {
            if (File.Exists(str))
            {
                ParametersGetter.st.ReadXml(str);
                ParametersGetter.defaultConnection = ParametersGetter.GetLocalValues("DefaultConnection");
                ParametersGetter.biometricsConnection = ParametersGetter.GetLocalValues("BiometricsConnection");
                ParametersGetter.legacyConnection = ParametersGetter.GetLocalValues("LegacyConnection");
                ParametersGetter.elmahConnection = ParametersGetter.GetLocalValues("ElmahConnection");
                ParametersGetter.validationApi = ParametersGetter.GetLocalValues("ValidationApi");
                ParametersGetter.BiometricsFolder = ParametersGetter.GetLocalValues("BiometricsFolder");

                this.menuStrip.Visible = true;
            }
            else
            {
                MessageBox.Show("Database Configurations are missing. Contact the Administrator");
                this.menuStrip.Visible = false;
            }
        }

        private void myAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void uploadFpsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ParametersGetter.BearerFeedback))
            {
                ShowLoginForm();
                return;
            }
            var loadBiometricsForm = new LoadBiometrics {
                MdiParent = this,
                Text = @"Load FPs to Inua Jamii System",
                AutoScaleMode = AutoScaleMode.Dpi,
                WindowState = FormWindowState.Maximized
            };

            loadBiometricsForm.Show();

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Contact System Administrator");
        }
    }
}
