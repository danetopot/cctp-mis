﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.FPTool
{
    public class ApiStatus
    {
        public string Description { get; set; }
        [Display(Name = "Status ID")]
        public int? StatusId { get; set; }
    }
}