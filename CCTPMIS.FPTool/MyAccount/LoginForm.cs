﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCTPMIS.FPTool.MyAccount
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var username = this.txtEmailAddress.Text; var password = this.txtPassword.Text;
            var loginVm = new LoginViewModel { grant_type = "password", username = this.txtEmailAddress.Text, password = this.txtPassword.Text };

            var httpWebApi = new HttpWebApi(ParametersGetter.validationApi);
            var bearer = await httpWebApi.Login(username, password);

            if (bearer.Contains("access_token"))
            {
                ParametersGetter.BearerFeedback = bearer;
                ParametersGetter.EmailAddress = username;
                ParametersGetter.Password = password;

                MessageBox.Show("Login Successful", "The Login was successful", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            else
            {

                ParametersGetter.BearerFeedback = "";
                ParametersGetter.EmailAddress = "";
                ParametersGetter.Password = "";

                MessageBox.Show("Login Failed", "The Login failed", MessageBoxButtons.RetryCancel,
                    MessageBoxIcon.Error);

                this.ParentForm.Hide();
            }
        }
        }
  
}
