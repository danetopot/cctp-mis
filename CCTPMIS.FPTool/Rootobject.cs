﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;

namespace CCTPMIS.FPTool
{
    public class Rootobject
    {


        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }



        /// <summary>
        /// The Username.
        /// </summary>
        public string userName { get; set; }



        /// <summary>
        /// Issued.
        /// </summary>
        public string issued { get; set; }



        /// <summary>
        /// Expiry.
        /// </summary>
        public string expires { get; set; }
    }

    public class LoginViewModel
    {
        public string grant_type { get; set; }

        public string username { get; set; }
        public string password { get; set; }
    }

    public class HttpWebApi
    {



        #region Fields:



        private static readonly HttpClient client = new HttpClient();



        #endregion



        #region Properties:



        /// <summary>
        /// The basr Uri.
        /// </summary>
        public string BaseUrl { get; set; }



        /// <summary>
        /// Username.
        /// </summary>
        protected internal string Username { get; set; }



        /// <summary>
        /// Password.
        /// </summary>
        protected internal string Password { get; set; }



        /// <summary>
        /// The instance of the Root Object Json Deserialised Class.
        /// </summary>
        Rootobject Authentication { get; set; }



        /// <summary>
        /// The Access Token from the Json Deserialised Login.
        /// </summary>
        public string AccessToken { get; set; }




        #endregion



        public HttpWebApi(string baseurl)
        {

            // Init Base Url:
            this.BaseUrl = baseurl;
        }
        public HttpWebApi(string baseurl, Rootobject auth)
        {

            // Init Base Url:
            this.BaseUrl = baseurl;
            Authentication = auth;
        }

        public HttpWebApi(Rootobject auth)
        {

            // Init Base Url:

            Authentication = auth;
        }

        public async Task<string> Get(string path)
        {

            if (this.Authentication.access_token == null)
                throw new Exception("Authentication is not completed.");

            // GET
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            var result = await client.GetStringAsync(this.BaseUrl + path);
            return result;
        }




        public async Task<string> Login(string username, string password)
        {

            // Set Username:
            this.Username = username;

            // Set Password:
            this.Password = password;

            // Conf String to Post:
            var Dic = new Dictionary<string, string>() { { "grant_type", "password" }, { "username", string.Empty }, { "password", string.Empty } };
            Dic["username"] = username;
            Dic["password"] = password;

            // Post to Controller:
            string auth = await this.Post("/Token", Dic);

            Debug.WriteLine(auth);
            // Deserialise Response:
            this.Authentication = JsonConvert.DeserializeObject<Rootobject>(auth);
            var x = this.Authentication.access_token;
            return auth;
        }


        public async Task<string> Post(string path, Dictionary<string, string> values)
        {

            // Add Access Token to the Headder:
            if (this.Authentication != null)
                if (this.Authentication.access_token != string.Empty)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Authentication.access_token);

            // Encode Values:
            var content = new FormUrlEncodedContent(values);


            // Post and get Response:
            var response = await client.PostAsync(this.BaseUrl + path, content);

            // Return Response:
            return await response.Content.ReadAsStringAsync();
        }

    
        public async Task<string> Register(string username, string password)
        {

            // Register: api/Account/Register
            var Dic = new Dictionary<string, string>() { { "Email", string.Empty }, { "Password", string.Empty }, { "ConfirmPassword", string.Empty } };
            Dic["Email"] = username;
            Dic["Password"] = password;
            Dic["ConfirmPassword"] = password;

            return await this.Post("/api/Account/Register", Dic);
        }

        public static Dictionary<string, string> ObjectToDictionary(object obj)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                string propName = prop.Name;
                var val = obj.GetType().GetProperty(propName).GetValue(obj, null);
                if (val != null)
                {
                    ret.Add(propName, val.ToString());
                }
                else
                {
                    ret.Add(propName, null);
                }
            }

            return ret;
        }


    }
}