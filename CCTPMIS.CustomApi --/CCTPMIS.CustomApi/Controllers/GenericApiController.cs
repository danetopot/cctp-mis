﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;


namespace CCTPMIS.CustomApi.Controllers
{
    ////[Authorize(Users = "mis.inuajamii@socialprotection.or.ke")]
    public class GenericApiController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        protected string GetErrorListFromModelState(ModelStateDictionary modelState)
        {

            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            return message;

            var query = from state in modelState.Values
                from error in state.Errors
                select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
        

    }
}