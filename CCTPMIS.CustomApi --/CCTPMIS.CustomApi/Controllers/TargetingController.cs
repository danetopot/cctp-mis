﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.RegApi;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.CustomApi.Controllers
{
    /// <summary>
    /// This is the Targeting Module. This module processes the following field Data (i) Household Listing initial Data, (ii) Household Listing Community Validation,   (iii) Household Registration Data and (iv) Successful re-certification of households.
    /// </summary>
    // [Authorize(Users = "mis.inuajamii@socialprotection.or.ke")]
    //[Authorize]
    public class TargetingController : GenericApiController
    {
        private readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public TargetingController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }
        
        /// <summary>
        /// This Endpoint Accepts The Household Listing Data after the initial Capture by Field Enumerators
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
       //[Authorize]
        [Route("api/Targeting/Listing")]
        public IHttpActionResult PostListing(HHListingVm model)
        {
            // Log
            var userId = int.Parse(User.Identity.GetUserId());

            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", data, userId.ToString());

            var toModel = new RegistrationHh();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false)
                .CreateMapper()
                .Map(model, toModel);

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            try
            {
                var serializer = new XmlSerializer(typeof(List<HHListingVm>), new XmlRootAttribute("HHListings"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<HHListingVm> { model };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_xml", xml, userId.ToString());

                var spName = "AddEditHHListing";
                var parameterNames = "@ProgrammeId,@HouseHoldXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProgrammeId",toModel.ProgrammeId)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldXml",xml)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return Ok(apiFeedback);
        }

        /// <summary>
        /// This Endpoint accepts the submitted data after successful Household Listing Community Validation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
       //[Authorize]
        [Route("api/Targeting/CommVal/")]
        public IHttpActionResult PostListingComVal(ComHHListingVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            model.SyncUpDate = DateTime.Now.ToString("s");
            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", data, userId.ToString());

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
            try
            {
                var serializer = new XmlSerializer(typeof(List<ComHHListingVm>), new XmlRootAttribute("ComHHListings"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<ComHHListingVm> { model };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_xml", xml, userId.ToString());

                var spName = "AddEditComValHHListing";
                var parameterNames = "@ProgrammeId,@HouseHoldXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("ProgrammeId",model.ProgrammeId)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldXml",xml)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return Ok(apiFeedback);
        }

        /// <summary>
        /// This Endpoint Accepts the Households Data Collected from the HTM Pre-populated on the tablets that was Synced after Successful Household listing.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns an ApiStatus Model</returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
       //[Authorize]
        [Route("api/Targeting/Registration")]
        public IHttpActionResult PostRegistration(TabletHouseholdViewModel model)
        {
            ApiStatus apiFeedback;
            LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(model), " ");
            var userId = int.Parse(User.Identity.GetUserId());

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", description, userId.ToString());
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
            try
            {
                var householdInfo = JsonConvert.DeserializeObject<RegistrationHHVm>(model.HouseholdInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(householdInfo), "Household");

                var deviceInfo = JsonConvert.DeserializeObject<TabEnvironment>(model.DeviceInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(deviceInfo), "Household");

                var serializer = new XmlSerializer(typeof(List<RegistrationHHVm>), new XmlRootAttribute("Registrations"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<RegistrationHHVm> { householdInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }

                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xml, userId.ToString());

                var serializerx = new XmlSerializer(typeof(List<TabEnvironment>), new XmlRootAttribute("Registrations"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xmlx = string.Empty;
                var listViewModelx = new List<TabEnvironment> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    xmlx += sw.ToString();
                }
                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xmlx, userId.ToString());

                var spName = "AddEditRegistrationHH";
                var parameterNames = "@HouseHoldInfoXml,@DeviceInfoXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldInfoXml",xml)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("DeviceInfoXml",xmlx)},
                };

                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);

                apiFeedback = apiDbFeedback;

                apiFeedback = new ApiStatus {
                    StatusId = 0,
                    Description = "The HouseHold Registration details were Successfully Submitted"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return Ok(apiFeedback);
        }

        /// <summary>
        /// This Endpoint Accepts the Potential Beneficiary Households Data Collected from the HTM Pre-populated on the tablets that was Synced after Recertification plan was setup.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns an ApiStatus Model</returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
       //[Authorize]
        [Route("api/Targeting/Recertification")]
        public IHttpActionResult PostRecertification(TabletHouseholdViewModel model)
        {
            ApiStatus apiFeedback;
            LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(model), " ");
            var userId = int.Parse(User.Identity.GetUserId());

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", description, userId.ToString());
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }
            try
            {
                var householdInfo = JsonConvert.DeserializeObject<RecertificationHHVm>(model.HouseholdInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(householdInfo), "Household");

                var deviceInfo = JsonConvert.DeserializeObject<TabEnvironment>(model.DeviceInfo);
                LogService.FileLog(MisKeys.RETARGETINGDATA, JsonConvert.SerializeObject(deviceInfo), "Household");

                var serializer = new XmlSerializer(typeof(List<RegistrationHHVm>), new XmlRootAttribute("Recertifications"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                var listViewModel = new List<RecertificationHHVm> { householdInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, listViewModel);
                    xml += sw.ToString();
                }

                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xml, userId.ToString());

                var serializerx = new XmlSerializer(typeof(List<TabEnvironment>), new XmlRootAttribute("Recertifications"));
                var settingsx = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xmlx = string.Empty;
                var listViewModelx = new List<TabEnvironment> { deviceInfo };
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settingsx);
                    serializerx.Serialize(xw, listViewModelx);
                    xmlx += sw.ToString();
                }
                LogService.FileLog(MisKeys.RETARGETINGDATA + "_xml", xmlx, userId.ToString());

                var spName = "AddEditRecertificationHH";
                var parameterNames = "@HouseHoldInfoXml,@DeviceInfoXml";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("HouseHoldInfoXml",xml)},
                    new ParameterEntity { ParameterTuple =new Tuple<string, object>("DeviceInfoXml",xmlx)},
                };
                var apiDbFeedback = GenericService.GetOneBySp<ApiStatus>(spName, parameterNames, parameterList);
                apiFeedback = apiDbFeedback;

                apiFeedback = new ApiStatus {
                    StatusId = 0,
                    Description = "The HouseHold Re-certification details were Successfully Submitted"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = "\n" + e.Message + " \n" + e.InnerException?.StackTrace + " \n" + e.InnerException?.Message + " \n"
                };
            }
            return Ok(apiFeedback);
        }

        /// <summary>
        /// This Endpoint downloads the Community validation households and data from the server to the Tablets . This is automatically executed from the tablets using randomly assigned Enumerator Token numbers. Refer to the View Models for details.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/Targeting/GetCommunityVal/{id}/")]
        [HttpGet]
        public IHttpActionResult GetCommunityVal(int id)
        {
            var returnModel = new CvSetupVm();
            returnModel = GetCommunityValidationDownloadData(id);
            return Ok(returnModel);
        }

        /// <summary>
        /// This Endpoint downloads the Community validation households and data from the server to the Tablets. Upon successful Enumerator login Refer to the View Models for details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Targeting/GetCommunityVal")]
        [HttpPost]
        public IHttpActionResult GetCommunityValOptions(LoginVM model)
        {
            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetCommunityValidationDownloadData(enumerator.Id);
            return Ok(returnModel);
        }

        /// <summary>
        /// This endpoints downloads the Household registration data from the server into the Tablets. This required Enumerator Token Numbers. Refer to the View Models for details.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/Targeting/GetRegistration/{id}/")]
        [HttpGet]
        public IHttpActionResult GetRegistration(int id)
        {
            var returnModel = new CvSetupVm();
            returnModel = GetRegistrationDownloadData(id);
            return Ok(returnModel);
        }

        /// <summary>
        /// This Endpoints downloads necessary Geo locations and settings from the Server for local use into the tablet. Requires a successful Enumerator login before execution
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Targeting/GetOptions")]
        [HttpPost]
        public IHttpActionResult GetOptions(LoginVM model)
        {
            string hashPin;
            var returnModel = new InitialSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin && x.IsActive == true);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }

            returnModel = GetEnumeratorLoginDownloadData(enumerator.Id);
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_LOGIN", JsonConvert.SerializeObject(returnModel), "");

            return Ok(returnModel);
        }

        /// <summary>
        /// This is the Endpoint is incharge of downloading all the Households that are in Active Household Recertification Plans in corresponding Enumerator locations.
        /// </summary>
        /// <param name="model"> The User View Model </param>
        /// <returns> Repurposed Registration Households into H.T.M. Tool </returns>
        [Route("api/Targeting/GetRecertification")]
        [HttpPost]
        public IHttpActionResult GetRecertificationData(LoginVM model)
        {
            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetRecertificationDownloadData(enumerator.Id);
            return Ok(returnModel);
        }

        /// <summary>
        /// Get Necessary information in readiness to the active Targeting exercise
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("api/Targeting/GetListing")]
        [HttpPost]
        public IHttpActionResult GetListingOptions(LoginVM model)
        {
            string hashPin;
            var returnModel = new InitialSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetEnumeratorListingData(enumerator.Id);

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(model), "");

            return Ok(returnModel);
        }
        [HttpGet]

        [AllowAnonymous]
        [Route("api/Targeting/MyOptions/{id}")]
        public IHttpActionResult MyOptions(int id)
        {
            string hashPin;
            var returnModel = new InitialSetupVm();
            Enumerator enumerator;
            //if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            //{
            //    returnModel.Error = "Your National ID Number and Pin is required.";
            //    return Ok(returnModel);
            //}
            //if (!string.IsNullOrEmpty(id))
            //{
            var enumeratorId = id;
            enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            //}
            //else
            //{
            //    hashPin = EasyMD5.Hash(model.Pin);
            //    enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            //}
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            returnModel = GetEnumeratorLoginDownloadData(enumerator.Id);

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_return", JsonConvert.SerializeObject(returnModel), "");

            return Ok(returnModel);
        }

        /// <summary>
        /// This is the Endpoint of getting all the beneficiary Households that are in Active Targeting Registration Plans.
        /// </summary>
        /// <param name="model"> The User View Model </param>
        /// <returns> Repurposed Registration Households into H.T.M. Tool </returns>
        [Route("api/Targeting/GetRegistration")]
        [HttpPost]
        public IHttpActionResult GetRegistrationData(LoginVM model)
        {
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(model), "");

            string hashPin;
            var returnModel = new CvSetupVm();
            Enumerator enumerator;
            if (string.IsNullOrEmpty(model.Id) && (string.IsNullOrEmpty(model.NationalId) || string.IsNullOrEmpty(model.Pin)))
            {
                returnModel.Error = "Your National ID Number and Pin is required.";
                return Ok(returnModel);
            }
            if (!string.IsNullOrEmpty(model.Id))
            {
                var enumeratorId = int.Parse(model.Id);
                enumerator = db.Enumerators.FirstOrDefault(x => x.Id == enumeratorId);
            }
            else
            {
                hashPin = EasyMD5.Hash(model.Pin);
                enumerator = db.Enumerators.FirstOrDefault(x => x.NationalIdNo == model.NationalId && x.PasswordHash == hashPin);
            }
            if (enumerator == null)
            {
                returnModel.Error = "Your Account Does not exist or is Deactivated. \n Check National Id and PIN and Try Again";
                return Ok(returnModel);
            }
            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(enumerator), "");

            returnModel = GetRegistrationDownloadData(enumerator.Id);
            return Ok(returnModel);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private InitialSetupVm GetEnumeratorListingData(int enumeratorId)
        {
            var returnModel = new InitialSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);

            returnModel.Programmes = (from c in db.Programmes
                                      select new ProgrammeVm() {
                                          Id = c.Id,
                                          Code = c.Code,
                                          Name = c.Name,
                                          BeneficiaryTypeId = c.BeneficiaryTypeId,
                                          IsActive = c.IsActive,
                                          PrimaryRecipientId = c.PrimaryRecipientId,
                                          SecondaryRecipientId = c.SecondaryRecipientId,
                                          SecondaryRecipientMandatory = c.SecondaryRecipientMandatory,
                                      }).ToList();

            returnModel.TargetPlans = (from c in db.TargetPlans
                                       select new TargetPlanVm {
                                           Id = c.Id,
                                           Start = c.Start,
                                           End = c.End,
                                           CategoryId = c.CategoryId,
                                           Name = c.Name,
                                           StatusId = c.StatusId
                                       }).ToList();

            return returnModel;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private InitialSetupVm GetEnumeratorLoginDownloadData(int enumeratorId)
        {
            var returnModel = new InitialSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);
            var mobileDownloadCodes = "System Settings,Beneficiary Type,Sex,Registration Group,Member Role,Relationship,Other SP Programme,SP Benefit Type,REG_CATEGORIES,REG_STATUS,Registration Status,Tenure Status,Wall Material,Floor Material,Roof Material,Toilet Type,Water Source,Lighting Source,Cooking Fuel,Dwelling Unit Risk,Household Assets,Household Asset Type,Boolean Options,Household Conditions,Household Option,Marital Status,Disability,Education Attendance,Education Level,Work Type,Interview Type,Interview Result,Interview Status,ID Document Type";

            returnModel.Enumerator = enumeratorVm;
            returnModel.EnumeratorLocations = (from c in db.EnumeratorLocations
                                               //where c.EnumeratorId ==enumeratorId  // && c.IsActive
                                               select new EnumeratorLocationVm() {
                                                   Id = c.Id,
                                                   LocationId = c.LocationId,
                                                   EnumeratorId = c.EnumeratorId,
                                                   IsActive = c.IsActive
                                               }).Distinct().ToList();

            returnModel.Locations = (from c in db.EnumeratorLocations
                                     join l in db.Locations on c.LocationId equals l.Id
                                     //where c.EnumeratorId == enumerator.Id && c.IsActive
                                     select new LocationVm() {
                                         Id = l.Id,
                                         Name = l.Name,
                                         Code = l.Code
                                     }).Distinct().ToList();

            returnModel.SubLocations = (from c in db.EnumeratorLocations
                                        join l in db.Locations on c.LocationId equals l.Id
                                        join s in db.SubLocations on l.Id equals s.LocationId
                                        //where c.EnumeratorId == enumerator.Id && c.IsActive
                                        select new SubLocationVm() {
                                            LocationId = s.LocationId,
                                            Id = s.Id,
                                            Name = s.Name,
                                        }).Distinct().ToList();

            returnModel.TargetPlans = (from c in db.TargetPlans

                                       select new TargetPlanVm {
                                           Id = c.Id,
                                           Start = c.Start,
                                           End = c.End,
                                           CategoryId = c.CategoryId,
                                           Name = c.Name,
                                           StatusId = c.StatusId
                                       }).Distinct().ToList();

            returnModel.SystemCodes = (from c in db.SystemCodes
                                       where mobileDownloadCodes.Contains(c.Code)
                                       select new SystemCodeVm() {
                                           Id = c.Id,
                                           Code = c.Code,
                                           Description = c.Description
                                       }).Distinct().ToList();

            returnModel.SystemCodeDetails =
                (from c in db.SystemCodeDetails
                 where mobileDownloadCodes.Contains(c.SystemCode.Code)
                 select new SystemCodeDetailVm() {
                     Id = c.Id,
                     Code = c.Code,
                     Description = c.Description,
                     SystemCodeId = c.SystemCodeId
                 }).Distinct().ToList();

            returnModel.Programmes = (from c in db.Programmes

                                      select new ProgrammeVm() {
                                          Id = c.Id,
                                          Code = c.Code,
                                          Name = c.Name,
                                          BeneficiaryTypeId = c.BeneficiaryTypeId,
                                          IsActive = c.IsActive,
                                          PrimaryRecipientId = c.PrimaryRecipientId,
                                          SecondaryRecipientId = c.SecondaryRecipientId,
                                          SecondaryRecipientMandatory = c.SecondaryRecipientMandatory,
                                      }).Distinct().ToList();

            LogService.FileLog(MisKeys.REGISTRATIONDATA + "_result", JsonConvert.SerializeObject(returnModel), "");

            return returnModel;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetCommunityValidationDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);

            var spName = "GetHouseholdsReadyForComValDownload";
            var parameterNames = "@EnumeratorId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("EnumeratorId",enumeratorId)},
            };
            returnModel.ComValListingPlanHHs = GenericService.GetManyBySp<ComValDownloadListingPlanHH>(spName, parameterNames, parameterList).ToList();

            return returnModel;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetRegistrationDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var spName = "GetHouseholdsReadyForRegistration";
            var parameterNames = "@EnumeratorId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity { ParameterTuple =new Tuple<string, object>("EnumeratorId",enumeratorId)},
            };
            try
            {
                var reg = GenericService.GetManyBySp<ComValListingPlanHH>(spName, parameterNames, parameterList).ToList();

                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(reg), "");

                returnModel.Registrations = reg;
                var members = new List<CvMember>();
                if (!reg.Any())
                {
                    return returnModel;
                }

                foreach (var household in reg)
                {
                    var cgId = Guid.NewGuid().ToString();
                    var beneId = Guid.NewGuid().ToString();
                    var cg = new CvMember {
                        FirstName = household.CgFirstName,
                        MiddleName = household.CgMiddleName,
                        Surname = household.CgSurname,
                        RegistrationId = household.Id,
                        SexId = household.CgSexId,
                        DateOfBirth = household.CgDoB.ToString("O"),
                        IdentificationNumber = household.CgNationalIdNo,
                        PhoneNumber = household.CgPhoneNumber,
                        MemberId = cgId,
                    };
                    members.Add(cg);

                    if (string.IsNullOrEmpty(household.BeneNationalIdNo) || !household.BeneSexId.HasValue || !household.BeneDoB.HasValue)
                    {
                        continue;
                    }
                    else
                    {
                        var bene = new CvMember {
                            FirstName = household.BeneFirstName,
                            MiddleName = household.BeneMiddleName,
                            Surname = household.BeneSurname,
                            RegistrationId = household.Id,
                            SexId = household.BeneSexId.Value,
                            DateOfBirth = household.BeneDoB.Value.ToString("O"),
                            IdentificationNumber = household.BeneNationalIdNo,
                            PhoneNumber = household.BenePhoneNumber,
                            MemberId = beneId,
                            CareGiverId = cgId
                        };
                        members.Add(bene);
                    }
                }

                returnModel.RegistrationMembers = members;
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(members), "");
            }
            catch (Exception e)
            {
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", e.Message, "");
                LogService.FileLog(MisKeys.REGISTRATIONDATA + "_POSTED", JsonConvert.SerializeObject(e), "");
            }
            return returnModel;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        private CvSetupVm GetRecertificationDownloadData(int enumeratorId)
        {
            var returnModel = new CvSetupVm();
            var enumerator = db.Enumerators.Single(x => x.Id == enumeratorId);
            var enumeratorVm = new EnumeratorVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, enumeratorVm);

            //returnModel.ValRegistrationHhVms =
            //    (from r in db.RegistrationHhs
            //     join v in db.RegExceptions on r.Id equals v.Id
            //     join s in db.SubLocations on r.SubLocationId equals s.Id
            //     join c in db.TargetPlans on r.RegPlanId equals c.Id
            //     join l in db.Locations on s.LocationId equals l.Id
            //     join el in db.EnumeratorLocations on l.Id equals el.LocationId
            //     join tpc in db.TarPlanProgrammes on r.RegPlanId equals tpc.TargetPlanId
            //     join p in db.Programmes on tpc.ProgrammeId equals p.Id
            //     join t in db.TargetPlans on tpc.TargetPlanId equals t.Id
            //     where el.EnumeratorId == enumeratorId
            //           && el.IsActive
            //           && t.Status.Code == "ACTIVE"
            //           && c.Status.Code == "CLOSED"
            //     select r
            //    ).GroupBy(r => r.Id)
            //    .Select(grp => grp.FirstOrDefault()).ToList();
            return returnModel;
        }
    }
}