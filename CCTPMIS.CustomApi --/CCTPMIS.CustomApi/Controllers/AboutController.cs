﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Configuration;
using System.Web.Http;

namespace CCTPMIS.CustomApi.Controllers
{
    public class AboutController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] {
                "Version", WebConfigurationManager.AppSettings["Version"], "UpdateDate", System.IO.File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location).ToString("O")};
        }
    }
}