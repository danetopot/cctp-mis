﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.RegApi;
using CCTPMIS.Services;
using Newtonsoft.Json;

namespace CCTPMIS.CustomApi.Controllers
{
    /// <summary>
    /// Targeting Module - This module 
    /// </summary>
    [Authorize(Users = "mis.inuajamii@socialprotection.or.ke")]
    public class OptionsController : GenericApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public readonly IGenericService GenericService;
        public readonly LogService LogService;

        public OptionsController(IGenericService genericService, LogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}