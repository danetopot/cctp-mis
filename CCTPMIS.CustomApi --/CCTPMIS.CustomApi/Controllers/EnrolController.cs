﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models;
using CCTPMIS.Models.Api;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.CustomApi.Controllers
{
    /// <summary>
    /// Enrolment - PSP Card Opening Module
    /// </summary>
    [Authorize]
    public class EnrolController : ApiController
    {
      //  private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IGenericService GenericService;
        private readonly ILogService LogService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericService"></param>
        /// <param name="logService"></param>
        public EnrolController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }


        /// <summary>
        /// This endpoints accepts Beneficiary Household information.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST: api/Enrol
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Enrol")]
        public IHttpActionResult Post(PspEnrolBeneficiaryViewModel model)
        {
            //var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            // var LogService = new LogService(GenericService);
            var userId = int.Parse(User.Identity.GetUserId());
            model.UserId = userId;
            var data = JsonConvert.SerializeObject(model);

            LogService.FileLog(MisKeys.ENROLSUBMISSION, data, userId.ToString());

            ApiStatus apiFeedback;

            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                LogService.FileLog(MisKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), userId.ToString());
                return Ok(apiFeedback);
            }
            if (!User.Identity.IsAuthenticated)
            {
                apiFeedback = new ApiStatus { StatusId = -1, Description = "The session is not Authenticated" };
                LogService.FileLog(MisKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), userId.ToString());
                return Ok(apiFeedback);


            }
            model.UserId = userId;

            var parameterNames = "@EnrolmentNo,@TokenId,@BankCode,@BranchCode,@AccountNo,@AccountName,@AccountOpenedOn,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@PriReciSex,@PriReciDoB,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SecReciSex,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@UserId";

            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("EnrolmentNo",model.EnrolmentNo)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TokenId",model.TokenId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("BankCode",model.BankCode)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("BranchCode",model.BranchCode)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("AccountNo",model.AccountNo)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("AccountName",model.AccountName)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("AccountOpenedOn",model.AccountOpenedOn)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciFirstName",model.PriReciFirstName)},
                !string.IsNullOrEmpty(model.PriReciMiddleName)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciMiddleName",model.PriReciMiddleName)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciMiddleName",DBNull.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciSurname",model.PriReciSurname)},new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciNationalIdNo",model.PriReciNationalIdNo)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciSex",model.PriReciSex)},new ParameterEntity{ParameterTuple =new Tuple<string, object>("PriReciDoB",model.PriReciDoB)},
                !string.IsNullOrEmpty(model.SecReciFirstName)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciFirstName",model.SecReciFirstName)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciFirstName",DBNull.Value)},
                !string.IsNullOrEmpty(model.SecReciMiddleName)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciMiddleName",model.SecReciMiddleName)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciMiddleName",DBNull.Value)},
                !string.IsNullOrEmpty(model.SecReciSurname)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciSurname",model.SecReciSurname)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciSurname",DBNull.Value)},
                !string.IsNullOrEmpty(model.SecReciNationalIdNo)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciNationalIdNo",model.SecReciNationalIdNo)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciNationalIdNo",DBNull.Value)},
                !string.IsNullOrEmpty(model.SecReciSex)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciSex",model.SecReciSex)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciSex",DBNull.Value)},
                !string.IsNullOrEmpty(model.SecReciDoB)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciDoB",model.SecReciDoB)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("SecReciDoB",DBNull.Value)},
                !string.IsNullOrEmpty(model.PaymentCardNo)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("PaymentCardNo",model.PaymentCardNo)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("PaymentCardNo",DBNull.Value)},
                !string.IsNullOrEmpty(model.MobileNo1)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("MobileNo1",model.MobileNo1)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("MobileNo1",DBNull.Value)},
                !string.IsNullOrEmpty(model.MobileNo2)? new ParameterEntity{ParameterTuple =new Tuple<string, object>("MobileNo2",model.MobileNo2)}: new ParameterEntity{ParameterTuple =new Tuple<string, object>("MobileNo2",DBNull.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",model.UserId)}

            };

            try
            {
                var pspEnrolFeedbackStep01Vm = GenericService.GetOneBySp<PspEnrolFeedbackStep01VM>("PSPEnrolBeneficiary", parameterNames, parameterList);

                LogService.FPLog(data, pspEnrolFeedbackStep01Vm.PaymentCardId.ToString());

                apiFeedback = new ApiStatus { StatusId = 0, Description = "The Enrolment was completed Successfully" };

                LogService.FileLog(MisKeys.MisData, JsonConvert.SerializeObject(apiFeedback), userId.ToString());

                /*
                var parameterNames2 = "@BenePaymentCardId,@PriReciRT,@PriReciRI,@PriReciRMF,@PriReciRRF,@PriReciRP,@PriReciLT,@PriReciLI,@PriReciLMF,@PriReciLRF,@PriReciLP,@SecReciRT,@SecReciRI,@SecReciRMF,@SecReciRRF,@SecReciRP,@SecReciLT,@SecReciLI,@SecReciLMF,@SecReciLRF,@SecReciLP,@UserId";
                List<ParameterEntity> parameterList2;
                if (!string.IsNullOrEmpty(model.SecReciRI))
                {
                    parameterList2 = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple = new Tuple<string, object>("BenePaymentCardId", pspEnrolFeedbackStep01Vm.PaymentCardId)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRT", System.Text.Encoding.ASCII.GetBytes(model.PriReciRT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRI", System.Text.Encoding.ASCII.GetBytes(model.PriReciRI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRMF", System.Text.Encoding.ASCII.GetBytes(model.PriReciRMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRRF", System.Text.Encoding.ASCII.GetBytes(model.PriReciRRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRP", System.Text.Encoding.ASCII.GetBytes(model.PriReciRP)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLT", System.Text.Encoding.ASCII.GetBytes(model.PriReciLT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLI", System.Text.Encoding.ASCII.GetBytes(model.PriReciLI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLMF", System.Text.Encoding.ASCII.GetBytes(model.PriReciLMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLRF", System.Text.Encoding.ASCII.GetBytes(model.PriReciLRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLP", System.Text.Encoding.ASCII.GetBytes(model.PriReciLP)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRT", System.Text.Encoding.ASCII.GetBytes(model.SecReciRT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRI", System.Text.Encoding.ASCII.GetBytes(model.SecReciRI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRMF", System.Text.Encoding.ASCII.GetBytes(model.SecReciRMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRRF", System.Text.Encoding.ASCII.GetBytes(model.SecReciRRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRP", System.Text.Encoding.ASCII.GetBytes(model.SecReciRP)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLT", System.Text.Encoding.ASCII.GetBytes(model.SecReciLT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLI", System.Text.Encoding.ASCII.GetBytes(model.SecReciLI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLMF", System.Text.Encoding.ASCII.GetBytes(model.SecReciLMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLRF", System.Text.Encoding.ASCII.GetBytes(model.SecReciLRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLP", System.Text.Encoding.ASCII.GetBytes(model.SecReciLP))},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", model.UserId) }
                };
                }
                else
                {
                    parameterList2 = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple = new Tuple<string, object>("BenePaymentCardId", pspEnrolFeedbackStep01Vm.PaymentCardId)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRT", System.Text.Encoding.ASCII.GetBytes(model.PriReciRT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRI", System.Text.Encoding.ASCII.GetBytes(model.PriReciRI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRMF", System.Text.Encoding.ASCII.GetBytes(model.PriReciRMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRRF", System.Text.Encoding.ASCII.GetBytes(model.PriReciRRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciRP", System.Text.Encoding.ASCII.GetBytes(model.PriReciRP)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLT", System.Text.Encoding.ASCII.GetBytes(model.PriReciLT)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLI", System.Text.Encoding.ASCII.GetBytes(model.PriReciLI)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLMF", System.Text.Encoding.ASCII.GetBytes(model.PriReciLMF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLRF", System.Text.Encoding.ASCII.GetBytes(model.PriReciLRF)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("PriReciLP", System.Text.Encoding.ASCII.GetBytes(model.PriReciLP)) },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRT", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRI", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRMF", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRRF", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRP", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLT", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLI", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLMF", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLRF", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLP", DBNull.Value)},
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", model.UserId) }
                };
                }
                var spIntVm = GenericService.GetOneBySp<SpIntVm>("AddEditPaymentCardBiometrics", parameterNames2, parameterList2);
                apiFeedback = new ApiStatus { StatusId = spIntVm.NoOfRows, Description = "The Enrolment was completed Successfully" };

                */
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus { StatusId = -1, Description = e.Message };
                if (e.Message.Contains("'IX_BeneficiaryAccount_PSPBranchId_AccountNo"))
                {
                    apiFeedback.Description = "One Account cannot be  used by different Beneficiary Households";
                }
                if (e.Message.Contains("SqlAzureExecutionStrategy"))
                {
                    apiFeedback.Description = "The timeout period elapsed prior to obtaining a response.";
                }

                LogService.FileLog(MisKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), userId.ToString());

                if(!string.IsNullOrEmpty(e.InnerException?.Message))
                LogService.FileLog(MisKeys.ERROR, e.InnerException?.Message, userId.ToString());

            }

            return Ok(apiFeedback);
        }

        /// <summary>
        /// This endpoints accepts Beneficiary Payment Card information.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Enrol/PaymentCard")]
        public IHttpActionResult PostPaymentCard(PspUpdatePaymentCardViewModel model)
        {
            //var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            //var LogService = new LogService(GenericService);

            var userId = int.Parse(User.Identity.GetUserId());
            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.ENROLSUBMISSION, data, userId.ToString());

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = description
                };
                return Ok(apiFeedback);
            }

            if (!User.Identity.IsAuthenticated)
            {
                apiFeedback =
                    new ApiStatus { StatusId = -1, Description = "The session is not Authenticated" };
                return Ok(apiFeedback);
            }

            // var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@EnrolmentNo,@AccountNo,@PaymentCardNo,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
ParameterTuple =
    new Tuple<string, object>(
        "EnrolmentNo",
        model.EnrolmentNo)
                                            },
                                        new ParameterEntity
                                            {
ParameterTuple =
    new Tuple<string, object>(
        "AccountNo",
        model.AccountNo)
                                            },
                                        new ParameterEntity
                                            {
ParameterTuple =
    new Tuple<string, object>(
        "PaymentCardNo",
        model.PaymentCardNo)
                                            },
                                        new ParameterEntity
                                            {
ParameterTuple =
    new Tuple<string, object>(
        "UserId",
        userId)
                                            }
                                    };
            try
            {
                var spIntVm = GenericService.GetOneBySp<SpIntVm>("PspCardBeneficiary", parameterNames, parameterList);

                apiFeedback = new ApiStatus {
                    StatusId = spIntVm.NoOfRows,
                    Description = "The Payment Card details were Successfully Updated"
                };
            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus { StatusId = -1, Description = e.Message };
            }

            return Ok(apiFeedback);
        }

        /// <summary>
        /// This endpoints accepts Beneficiary Caregiver   information.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Enrol/CaregiverDetail")]
        public IHttpActionResult PostCaregiverDetail(PspEnrolCareGiverViewModel model)
        {
            //var GenericService = new GenericService(new GenericRepository<ApplicationDbContext>(new ApplicationDbContext()));
            //var LogService = new LogService(GenericService);

            var userId = int.Parse(User.Identity.GetUserId());
            var data = JsonConvert.SerializeObject(model);
            LogService.FileLog(MisKeys.ENROLSUBMISSION, data, userId.ToString());

            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var desc = GetErrorListFromModelState(ModelState);
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = $"{desc} - The Posted Data has been rejected"
                };
                return Ok(apiFeedback);
            }

            if (!User.Identity.IsAuthenticated)
            {
                apiFeedback = new ApiStatus { StatusId = -1, Description = "The session is not Authenticated" };
                return Ok(apiFeedback);
            }

            try
            {
                var parameterNames = "@EnrolmentNo,@BankCode,@BranchCode,@AccountNo,@AccountName,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SecReciSex,@SecReciDoB,@SecReciRT,@SecReciRI,@SecReciRMF,@SecReciRRF,@SecReciRP,@SecReciLT,@SecReciLI,@SecReciLMF,@SecReciLRF,@SecReciLP,@UserId";
                var parameterList = new List<ParameterEntity>();
                {
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("EnrolmentNo", model.EnrolmentNo) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("BankCode", model.BankCode) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("BranchCode", model.BranchCode) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("AccountNo", model.AccountNo) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("AccountName", model.AccountName) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciFirstName) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciFirstName", model.SecReciFirstName) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciFirstName", DBNull.Value) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciMiddleName) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciMiddleName", model.SecReciMiddleName) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciMiddleName", DBNull.Value) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciSurname) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciSurname", model.SecReciSurname) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciSurname", DBNull.Value) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciNationalIdNo) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciNationalIdNo", model.SecReciNationalIdNo) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciNationalIdNo", DBNull.Value) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciSex) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciSex", model.SecReciSex) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciSex", DBNull.Value) });
                    parameterList.Add(!string.IsNullOrEmpty(model.SecReciDoB) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciDoB", model.SecReciDoB) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciDoB", DBNull.Value) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRT", Encoding.ASCII.GetBytes(model.SecReciRT)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRI", Encoding.ASCII.GetBytes(model.SecReciRI)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRMF", Encoding.ASCII.GetBytes(model.SecReciRMF)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRRF", Encoding.ASCII.GetBytes(model.SecReciRRF)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciRP", Encoding.ASCII.GetBytes(model.SecReciRP)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLT", Encoding.ASCII.GetBytes(model.SecReciLT)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLI", Encoding.ASCII.GetBytes(model.SecReciLI)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLMF", Encoding.ASCII.GetBytes(model.SecReciLMF)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLRF", Encoding.ASCII.GetBytes(model.SecReciLRF)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("SecReciLP", Encoding.ASCII.GetBytes(model.SecReciLP)) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", userId) });
                }

                var spIntVm = GenericService.GetOneBySp<SpIntVm>("PSPEnrolCargiver", parameterNames, parameterList);

                LogService.FPLog(data, spIntVm.Id.ToString());

                apiFeedback = new ApiStatus { StatusId = spIntVm.NoOfRows, Description = "The Payment Card details were Successfully Updated" };

                LogService.FileLog(MisKeys.MisData, JsonConvert.SerializeObject(apiFeedback), userId.ToString());
            }
            catch (Exception ex)
            {
                apiFeedback = new ApiStatus {
                    StatusId = -1,
                    Description = ex.Message
                };

                LogService.FileLog(MisKeys.ERROR, JsonConvert.SerializeObject(apiFeedback), userId.ToString());

                if (!string.IsNullOrEmpty(ex.InnerException?.Message))
                    LogService.FileLog(MisKeys.ERROR, ex.InnerException?.Message, userId.ToString());
            }

            return Ok(apiFeedback);
        }



        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;
            var delimiter = @" ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GenericService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}