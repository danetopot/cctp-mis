﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace CCTPMIS.CustomApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "CCTPMIS API  DOCUMENTATION ";

            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                 
            }
            base.Dispose(disposing);
        }
    }
}
