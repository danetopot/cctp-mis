﻿using AutoMapper;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.RegApi;

namespace CCTPMIS.CustomApi
{
    public class AutoMapperConfig
    {
        public static void InitializeMappings()
        {

            Mapper.Initialize(
                cfg => {

                    // EnumeratorVm
                    cfg.CreateMap<RegistrationHh, HHListingVm>(MemberList.Destination)
                        .ConstructUsing(x => new HHListingVm());

                    cfg.CreateMap<HHListingVm, RegistrationHh>(MemberList.Destination)
                        .ConstructUsing(x => new RegistrationHh());

                    cfg.CreateMap<Enumerator, EnumeratorVm>(MemberList.Destination)
                        .ConstructUsing(x => new EnumeratorVm());

                    cfg.CreateMap<EnumeratorVm, Enumerator>(MemberList.Destination)
                        .ConstructUsing(x => new Enumerator());



                    cfg.CreateMap<RegistrationHh, RegistrationHHCvVm>(MemberList.Destination).ConstructUsing(x => new RegistrationHHCvVm());
                    cfg.CreateMap<RegistrationHHCvVm, RegistrationHh>(MemberList.Destination).ConstructUsing(x => new RegistrationHh());


                    //RegistrationHHCvVm

                });
        }
    }
}