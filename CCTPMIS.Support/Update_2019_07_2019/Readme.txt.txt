Update reconciliation Module
----------------------------
- Chaange the check of dates on reconciliatioon procedures to use PayrollApvOn instead of PostPayrollApvOn that falls outside the payment cycles

- Change the ajax Check to use the /Funds/ instead of /Payments/ on files

- Truncate the DSDG, DCS and Legacy Databases log files. These are only readonly databases that you requested to shrink or truncate after taking the backup.



How to apply the change
----------------------------
1. Run the 2019-07-24 Reconciliation_Update.sql on the Live CCTP-MIS database to fix the reconciliation bug
2. Copy and replace the the files as follows : - 
   (i)  \Update_2019_07_2019\Files\Reconciliation\Create.cshtml copy to E:\LIVE_CCTP_MIS\Areas\Funds\Views\Reconciliation\
   (ii) \Update_2019_07_2019\Files\ReconciliationDetails\Create.cshtml copy to E:\LIVE_CCTP_MIS\Areas\Funds\Views\ReconciliationDetails\
3. Run 2019-07-24 Truncate ReadOnlyDatabases.sql on the Live  CCTP-MIS database to truncate the databases
