
IF NOT OBJECT_ID('GetReconciliationOptions') IS NULL	
DROP PROC GetReconciliationOptions
GO
CREATE PROC GetReconciliationOptions
	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @Id=ISNULL(@Id,0)
	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PayrollApvOn<=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'
ELSE IF EXISTS( SELECT 1 FROM  PaymentAccountActivityMonth T1  INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId = T2.Id INNER JOIN PaymentCycleDetail T0 ON T0.PaymentCycleId = T2.Id LEFT JOIN BeneAccountMonthlyActivity T3 ON T3.MonthId = T1.MonthId AND T3.Year = T1.Year  AND T3.ClosedOn IS NOT NULL WHERE T0.FundsRequestOn>=@StartDate AND (T0.PayrollApvOn<=@EndDate ) AND T3.Id IS NULL)
		SET @ErrorMsg='There exists pending monthly activity reports between '+convert(varchar,@StartDate)+' and  '+ convert(varchar,@EndDate)+' ' 

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT T1.Id,T3.[Description] AS FromMonth,T4.[Description] AS ToMonth,T5.[Description] AS FinancialYear
	FROM PaymentCycle T1 INNER JOIN (
									  SELECT PaymentCycleId
									  FROM PaymentCycleDetail
									  WHERE FundsRequestOn>=@StartDate AND PayrollApvOn<=@EndDate
									  GROUP BY PaymentCycleId
									) T2 ON T1.Id=T2.PaymentCycleId
						 INNER JOIN SystemCodeDetail T3 ON T1.FromMonthId=T3.Id
						 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
						 INNER JOIN SystemCodeDetail T5 ON T1.FinancialYearId=T5.Id
END
GO

IF NOT OBJECT_ID('AddEditReconciliation') IS NULL	
DROP PROC AddEditReconciliation
GO
CREATE PROC AddEditReconciliation

	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
   ,@ApplicablePaymentCyclesXML XML
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentCycle TABLE(
		Id int
	   ,PaymentCylceId int
	)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO @tblPaymentCycle(Id,PaymentCylceId)
	SELECT T1.Id,T1.PaymentCycleId
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(PaymentCycleId)[1]','int') AS PaymentCycleId
		FROM @ApplicablePaymentCyclesXML.nodes('PaymentCycles/Record') AS U(R)
	) T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id

	SET @Id=ISNULL(@Id,0)

	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentCycle) AND EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE FundsRequestOn>=@StartDate)
		SET @ErrorMsg='Please specify valid ApplicablePaymentCyclesXML parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PayrollApvOn<=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail
					  WHERE FundsRequestOn>=@StartDate AND PayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 RIGHT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T1.PaymentCycleId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) in ApplicablePaymentCyclesXML parameter are invalid for the period specified'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail 
					  WHERE FundsRequestOn>=@StartDate AND PayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 LEFT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T2.PaymentCylceId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(
					SELECT 1
					FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
										 INNER JOIN PaymentAccountActivityMonth T3 ON T1.Id=T3.PaymentCycleId
										 LEFT JOIN BeneAccountMonthlyActivity T4 ON T3.MonthId=T4.MonthId AND T3.[Year]=T4.[Year] 
					WHERE ISNULL(T4.StatusId,0)<>@SystemCodeDetailId3
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.StartDate=@StartDate
		   ,T1.EndDate=@EndDate
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Reconciliation T1
		WHERE T1.Id=@Id

		UPDATE T1
		SET T1.ReconciliationId=NULL
		FROM PaymentCycle T1
		WHERE T1.ReconciliationId=@Id

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END
	ELSE
	BEGIN
		INSERT INTO Reconciliation(StartDate,EndDate,StatusId,CreatedBy,CreatedOn)
		SELECT @StartDate,@EndDate,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @Id=Id FROM Reconciliation WHERE StartDate=@StartDate AND EndDate=@EndDate

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId
	END
END
GO


IF NOT OBJECT_ID('GetReconciliationDetailOptions') IS NULL	
DROP PROC GetReconciliationDetailOptions
GO
CREATE PROC GetReconciliationDetailOptions
	@ReconciliationId int
   ,@PSPId int
   ,@UserId int
AS
BEGIN
	DECLARE @tbl_PaymentCycles TABLE(
		PaymentCycleId int
	);
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @CrFundsRequests money
	DECLARE @CrClawBacks money
	DECLARE @DrPayments money
	DECLARE @DrCommissions money
	DECLARE @Balance money
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0)

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT PaymentCycleId
								FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PayrollApvOn<=T2.EndDate
								GROUP BY PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	INSERT INTO @tbl_PaymentCycles(PaymentCycleId)
	SELECT T1.PaymentCycleId
	FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T1.FundsRequestOn>=T2.StartDate AND t1.PayrollApvOn<=T2.EndDate
	WHERE T2.Id=@ReconciliationId
	GROUP BY PaymentCycleId

	SELECT @CrFundsRequests=SUM(T2.FundsRequestAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T2.PaymentCycleId,SUM(T1.EntitlementAmount+T1.OtherAmount) AS FundsRequestAmount
											FROM FundsRequestDetail T1 INNER JOIN FundsRequest T2 ON T1.FundsRequestId=T2.Id
											WHERE T1.PSPId=@PSPId
											GROUP BY T2.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId 

	SELECT @CrClawBacks=SUM(T1.ClawbackAmount)
	FROM (
			SELECT DISTINCT T1.PaymentCycleId,T2.MonthId,T2.[Year],T3.ClawbackAmount
			FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
									   INNER JOIN (
													SELECT T1.Id,T1.MonthId,T1.[Year],SUM(T2.ClawbackAmount) AS ClawbackAmount
													FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
																					   INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
																					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
													WHERE T4.PSPId=@PSPId
													GROUP BY T1.Id,T1.MonthId,T1.[Year]
												 ) T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
		) T1

	SELECT @DrPayments=SUM(T2.TrxAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T1.PaymentCycleId,SUM(T1.TrxAmount) AS TrxAmount
											FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
															INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
															INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
											WHERE T4.PSPId=@PSPId
											GROUP BY T1.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId
	
	SELECT @DrCommissions=SUM(T1.PaymentZoneCommAmt)
	FROM  (
			SELECT T1.PaymentCycleId,T1.HHId,T2.PaymentZoneCommAmt AS PaymentZoneCommAmt
			FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
							INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
							INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
			WHERE T1.WasTrxSuccessful=1 AND T4.PSPId=@PSPId
		) T1 INNER JOIN (
							SELECT T1.PaymentCycleId,T6.HhId,MAX(CONVERT(int,T4.HadUniqueWdl)) AS HadUniqueWdl
							FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
													   INNER JOIN BeneAccountMonthlyActivity T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
													   INNER JOIN BeneAccountMonthlyActivityDetail T4 ON T3.Id=T4.BeneAccountMonthlyActivityId
													   INNER JOIN BeneficiaryAccount T5 ON T4.BeneAccountId=T5.Id
													   INNER JOIN HouseholdEnrolment T6 ON T5.HhEnrolmentId=T6.Id
													   INNER JOIN PSPBranch T7 ON T5.PSPBranchId=T7.Id
							WHERE T7.PSPId=@PSPId
							GROUP BY T1.PaymentCycleId,T6.HhId
							HAVING MAX(CONVERT(int,T4.HadUniqueWdl))<>0
						) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HhId=T2.HhId

	SELECT @CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrClawBacks=ISNULL(@CrClawBacks,0),@DrPayments=ISNULL(@DrPayments,0),@DrCommissions=ISNULL(@DrCommissions,0)

	SELECT @Balance=(@CrFundsRequests+@CrClawBacks)-(@DrPayments-@DrCommissions)			 

	SELECT @CrFundsRequests AS CrFundsRequests,@CrClawBacks AS CrClawBacks,@DrPayments AS DrPayments,@DrCommissions AS DrCommissions,@Balance AS Balance
END
GO


IF NOT OBJECT_ID('AddEditReconciliationDetail') IS NULL	
DROP PROC AddEditReconciliationDetail
GO
CREATE PROC AddEditReconciliationDetail
	@ReconciliationId int=NULL
   ,@PSPId int
   ,@OpeningBalance money
   ,@CrFundsRequests money
   ,@CrFundsRequestsStatement money
   ,@CrFundsRequestsDiffNarration varchar(128)
   ,@CrClawBacks money
   ,@CrClawBacksStatement money
   ,@CrClawBacksDiffNarration varchar(128)
   ,@DrPayments money
   ,@DrPaymentsStatement money
   ,@DrPaymentsDiffNarration varchar(128)
   ,@DrCommissions money
   ,@DrCommissionsStatement money
   ,@DrCommissionsDiffNarration varchar(128)
   ,@Balance money
   ,@BalanceStatement money
   ,@BalanceDiffNarration varchar(128)
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)	
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0),@OpeningBalance=ISNULL(@OpeningBalance,0)
		,@CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrFundsRequestsStatement=ISNULL(@CrFundsRequestsStatement,0),@CrFundsRequestsDiffNarration=ISNULL(@CrFundsRequestsDiffNarration,'')
		,@CrClawBacks=ISNULL(@CrClawBacks,0),@CrClawBacksStatement=ISNULL(@CrClawBacksStatement,0),@CrClawBacksDiffNarration=ISNULL(@CrClawBacksDiffNarration,'')
		,@DrPayments=ISNULL(@DrPayments,0),@DrPaymentsStatement=ISNULL(@DrPaymentsStatement,0)	,@DrPaymentsDiffNarration=ISNULL(@DrPaymentsDiffNarration,'')
		,@DrCommissions=ISNULL(@DrCommissions,0),@DrCommissionsStatement=ISNULL(@DrCommissionsStatement,0),@DrCommissionsDiffNarration=ISNULL(@DrCommissionsDiffNarration,'')
		,@Balance=ISNULL(@Balance,0),@BalanceStatement=ISNULL(@BalanceStatement,0),@BalanceDiffNarration=ISNULL(@BalanceDiffNarration,'')

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT PaymentCycleId
								FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PayrollApvOn<=T2.EndDate
								GROUP BY PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF (@CrFundsRequests<>@CrFundsRequestsStatement AND @CrFundsRequestsDiffNarration='')
		SET @ErrorMsg='Please specify valid CrFundsRequestsDiffNarration parameter'
	ELSE IF (@CrClawBacks<>@CrClawBacksStatement AND @CrClawBacksDiffNarration='')
		SET @ErrorMsg='Please specify valid CrClawBacksDiffNarration parameter'
	ELSE IF (@DrPayments<>@DrPaymentsStatement AND @DrPaymentsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrPaymentsDiffNarration parameter'
	ELSE IF (@DrCommissions<>@DrCommissionsStatement AND @DrCommissionsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrCommissionsDiffNarration parameter'
	ELSE IF (@Balance<>@BalanceStatement AND @BalanceDiffNarration='')
		SET @ErrorMsg='Please specify valid BalanceDiffNarration parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId)
	BEGIN
		UPDATE T1
		SET T1.OpeningBalance=@OpeningBalance
		   ,T1.CrFundsRequests=@CrFundsRequests
		   ,T1.CrFundsRequestsStatement=@CrFundsRequestsStatement
		   ,T1.CrFundsRequestsDiffNarration=@CrFundsRequestsDiffNarration
		   ,T1.CrClawBacks=@CrClawBacks
		   ,T1.CrClawBacksStatement=@CrClawBacksStatement
		   ,T1.CrClawBacksDiffNarration=@CrClawBacksDiffNarration
		   ,T1.DrPayments=@DrPayments
		   ,T1.DrPaymentsStatement=@DrPaymentsStatement
		   ,T1.DrPaymentsDiffNarration=@DrPaymentsDiffNarration
		   ,T1.DrCommissions=@DrCommissions
		   ,T1.DrCommissionsStatement=@DrCommissionsStatement
		   ,T1.DrCommissionsDiffNarration=@DrCommissionsDiffNarration
		   ,T1.Balance=@Balance
		   ,T1.BalanceStatement=@BalanceStatement
		   ,T1.BalanceDiffNarration=@BalanceDiffNarration
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ReconciliationDetail T1
		WHERE T1.ReconciliationId=@ReconciliationId AND T1.PSPId=@PSPId

		SELECT @FileCreationId=BankStatementFileId FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId
	END
	ELSE
	BEGIN
		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @FileName='SPT'+'_'+'RECON'+'_'+CONVERT(varchar(11),T1.StartDate,106)+'-'+CONVERT(varchar(11),T1.EndDate,106) FROM Reconciliation T1 WHERE Id=@ReconciliationId
		SELECT @FileName=@FileName+'_'+T1.Code FROM PSP T1 WHERE Id=@PSPId
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		INSERT INTO ReconciliationDetail(ReconciliationId,PSPId,OpeningBalance,CrFundsRequests,CrFundsRequestsStatement,CrFundsRequestsDiffNarration,CrClawBacks,CrClawBacksStatement,CrClawBacksDiffNarration,DrPayments,DrPaymentsStatement,DrPaymentsDiffNarration,DrCommissions,DrCommissionsStatement,DrCommissionsDiffNarration,Balance,BalanceStatement,BalanceDiffNarration,BankStatementFileId,CreatedBy,CreatedOn)
		SELECT @ReconciliationId,@PSPId,@OpeningBalance,@CrFundsRequests,@CrFundsRequestsStatement,@CrFundsRequestsDiffNarration,@CrClawBacks,@CrClawBacksStatement,@CrClawBacksDiffNarration,@DrPayments,@DrPaymentsStatement,@DrPaymentsDiffNarration,@DrCommissions,@DrCommissionsStatement,@DrCommissionsDiffNarration,@Balance,@BalanceStatement,@BalanceDiffNarration,@FileCreationId,@UserId,GETDATE()
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId,@FileCreationId AS FileId,@FileName+@FileExtension AS SupportingDoc
	END
END
GO



IF NOT OBJECT_ID('FinalizeReconciliationDetail') IS NULL	
DROP PROC FinalizeReconciliationDetail
GO
CREATE PROC FinalizeReconciliationDetail
	@ReconciliationId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows finalization'
	ELSE IF EXISTS( SELECT 1
					FROM (
							SELECT PaymentCycleId
							FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PayrollApvOn<=T2.EndDate
							GROUP BY PaymentCycleId
						) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
								LEFT JOIN ReconciliationDetail T4 ON T4.ReconciliationId=@ReconciliationId AND T3.PSPId=T4.PSPId
					WHERE T4.PSPId IS NULL
					)
		SET @ErrorMsg='One or more PSPs Reconciliation detail are missing for the specified reconciliation'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Status'
	SET @SysDetailCode='RECONAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM Reconciliation T1
	WHERE T1.Id=@ReconciliationId
	
	SELECT @@ROWCOUNT AS NoOfRows
END
GO
 