﻿using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.Registration
{
    public class RegistrationSummaryVm
    {
        public GetRegistrationSummaryVm GetRegistrationSummaryVm { get; set; }
        public TargetPlan TargetPlan { get; set; }
        public int Id { get; set; }

    }
    public class ComValSummaryVm
    {
        public GetComValSummaryVm GetComValSummaryVm { get; set; }
        public TargetPlan TargetPlan { get; set; }
        public int Id { get; set; }

    }
    public class GetComValSummaryVm
    {
        [Display(Name = "Total Listed Households")]
        public int? RegHH { get; set; }
        [Display(Name = "Total Exited Households")]
        public int? ExitedHH { get; set; }
        [Display(Name = "Total Corrected Households ")]
        public int? AmendedHH { get; set; }
        [Display(Name = "Total Confirmed Households")]
        public int? ConfirmedHH { get; set; }
        [Display(Name = "Total Households Submitted After Community Validation")]
        public int? CvalHH { get; set; }
        [Display(Name = "Total Households Pending After Community Validation")]
        public int? PendingAcceptanceHH { get; set; }


    }

    //public class GetRegApvSummary
    //{
    //    public RegPlan RegPlan { get; set; }
    //    public GetRegApvSummaryVm GetRegApvSummaryVm { get; set; }
    //    public int Id { get; set; }

    //}

    //public class RegistrationCvSummaryVm
    //{
    //    public GetRegistrationCvSummaryVm GetRegistrationCvSummaryVm { get; set; }
    //    public RegPlan RegPlan { get; set; }
    //}
    public class GetRegistrationSummaryVm
    {
        public int? ExceptionsFileId { get; set; }
        public int? ValidationFileId { get; set; }
        public int? RegisteredHouseholds { get; set; }
        public int? AcceptedBatches { get; set; }
        public int? AcceptedHouseholds { get; set; }
        public int? PendingHouseholds { get; set; }
        public int? CG_IDNoExists { get; set; }
        public int? CGSexMatch { get; set; }
        public int? CG_DoBYearMatch { get; set; }
        public int? CG_DoBMatch { get; set; }
        public int? HHDIDNoExists { get; set; }
        public int? HHDSexMatch { get; set; }
        public int? HHDDoBYearMatch { get; set; }
        public int? HHDDoBMatch { get; set; }
        public int? Exceptions { get; set; }
        public int? Validated { get; set; }
    }


    public class GetTargetingSummaryVm
    {
        public int? ExceptionsFileId { get; set; }
        public int? ValidationFileId { get; set; }
        public int? RegisteredHouseholds { get; set; }
        public int? AcceptedBatches { get; set; }
        public int? AcceptedHouseholds { get; set; }
        public int? PendingHouseholds { get; set; }
        public int? CG_IDNoExists { get; set; }
        public int? CGSexMatch { get; set; }
        public int? CG_DoBYearMatch { get; set; }
        public int? CG_DoBMatch { get; set; }
        public int? HHDIDNoExists { get; set; }
        public int? HHDSexMatch { get; set; }
        public int? HHDDoBYearMatch { get; set; }
        public int? HHDDoBMatch { get; set; }
        public int? Exceptions { get; set; }
        public int? Validated { get; set; }
    }


    public class GetRegistrationCvSummaryVm
    {
        public int? ExceptionsFileId { get; set; }
        public int? ValidationFileId { get; set; }
        public int? RegisteredHouseholds { get; set; }
        public int? AcceptedBatches { get; set; }
        public int? AcceptedHouseholds { get; set; }
        public int? PendingHouseholds { get; set; }
        public int? CG_IDNoExists { get; set; }
        public int? CGSexMatch { get; set; }
        public int? CG_DoBYearMatch { get; set; }
        public int? CG_DoBMatch { get; set; }
        public int? HHDIDNoExists { get; set; }
        public int? HHDSexMatch { get; set; }
        public int? HHDDoBYearMatch { get; set; }
        public int? HHDDoBMatch { get; set; }
        public int? Exceptions { get; set; }
        public int? Validated { get; set; }
    }
}