﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Registration
{



    public class HHListingClosureVm
    {
        public int Id { get; set; }
        public TargetPlan TargetPlan { get; set; }

        public List<GetHHListingClosureSummaryVm> Summary { get; set; }
    }

    public class GetHHListingClosureSummaryVm
    {
        public byte Id { get; set; }

        public int BeneficiaryHousehold { get; set; }
        public string ProgrammeName { get; set; }


    }
    public class HhREjectVm
    {
        public int Id { get; set; }

        [Display(Name = "Reason for Rejecting the Household")]
        [DataType(DataType.MultilineText)]
      
        public string Reason { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        public int? TargetPlanId { get; set; }
    }

    public class HouseholdRegAcceptVm
    {
        [Display(Name = "Household Listing Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }

    }
    public class ListingAcceptVm
    {
        [Display(Name = "Household Listing Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        
    }
    public class RegAcceptVm
    {
        [Display(Name = "Registration Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
    }

    public class SrIprsValidateVm
    {
        public int Id { get; set; }
        public int? TargetPlanId { get; set; }
    }

    public class EnLocation
    {
        public int Id { get; set; }
        public string Constituency { get; set; }
        public string Display { get; set; }
        public string Location { get; set; }
        public bool IsSelected { get; set; }
    }

    public class BatchesViewModel
    {
        [Display(Name = "Households Pending Acceptance")]
        public int PendingAcceptance { get; set; }
        public IPagedList<ListingAccept> Batches { get; set; }
        public  List<AcceptanceDetailVm> ListingBatches { get; set; }

        public IPagedList<ComValListingAccept> ComValListingAccepts { get; set; }
        public IPagedList<RegAccept> RegAccepts { get; set; }
    }
    public class AcceptanceDetailVm
    {
        public int Id { get; set; }
        public int AcceptById { get; set; }
        public string AcceptBy { get; set; }
        public DateTime AcceptDate { get; set; }
        public int? AcceptApvById { get; set; }
        public string AcceptApvBy { get; set; }
        public DateTime? AcceptApvDate { get; set; }
        public int ReceivedHHs { get; set; }
        public string BatchName { get; set; }
        public int ConstituencyId { get; set; }
        public string Constituency { get; set; }
        public string County { get; set; }
        public int TargetPlanId { get; set; }

        public string TargetPlan { get; set; }

        public bool IsValidated { get; set; }
        public int PendingValidation { get; set; }

    }

    public class RejectBatches
    {
        public List<RejectedBatchesViewModel> RejectedBatches { get; set; }
    }

    public class RejectedBatchesViewModel
    {
        public int TargetPlanId { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int CountyId { get; set; }
        public int ConstituencyId { get; set; }
        public int TotalRejected { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }


    public class CvBatchesViewModel
    {
        public int PendingAcceptance { get; set; }
        public IPagedList<ComValListingAccept> Batches { get; set; }
    }
    public class PendingBatchesViewModel
    {
        public int PendingAcceptance { get; set; }
        public  List<PendingBatches> Batches { get; set; }
    }
    

    public class PendingBatches
    {
        public int ConstituencyId { get; set; }
        public string Constituency { get; set; }
        public string County { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }
        public int PendingHHs { get; set; }
        public int TargetPlanId { get; set; }  
    }

    public class PendingAcceptanceNoVm
    {

        public int PendingHHs { get; set; }
    }

    public class EnumeratorAddLoc
    {
        public int EnumeratorId { get; set; }
        public List<EnLocation> Locations { get; set; }
    }
    public class EnrolmentSetup
    {
        public int FinancialYearId { get; set; }
        public int ExpansionStatusId { get; set; }
        public int SublocationId { get; set; }
        public int RegistrationGroupId { get; set; }
        public int Quota { get; set; }
    }
    public class RegistrationScreener
    {
        public DateTime RegistrationDate { get; set; }
        public int SublocationId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HhdIdNo { get; set; }
        public string HHdSex { get; set; }
        public DateTime? HhdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgidNo { get; set; }
        public string CgSex { get; set; }
        public DateTime? CgdoB { get; set; }
        public int HouseholdMembers { get; set; }
    }




}
