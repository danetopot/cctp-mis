﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.Reports;

namespace CCTPMIS.Models
{
    public static class ControllerExtensions
    {

        public static SelectList GetPager(this Controller controller, int? pageSize)
        {
            return new SelectList(new List<FilterParameter> {
                new FilterParameter { Id = 10, Name = "10"  },
                new FilterParameter { Id = 20, Name = "20" },
                new FilterParameter { Id = 50, Name = "50" },
                new FilterParameter { Id = 100, Name = "100"},
                new FilterParameter { Id = 1000, Name = "1000"},
                new FilterParameter { Id = 0, Name = "All"  },

            }, "Id", "Name", pageSize);
        }

        public static SelectList GetReportType(this Controller controller, int? reportTypeId)
        {
            return new SelectList(new List<FilterParameter> {
               new FilterParameter { Id = 1, Name = "Detailed" },
               new FilterParameter { Id = 2, Name = "Summary" }
           }, "Id", "Name", reportTypeId);
        }
        public static SelectList BooleanSelectList(this Controller controller, int? boolStatus)
        {
            return new SelectList(new List<FilterParameter> {
                new FilterParameter { Id = 1, Name = "Yes" },
                new FilterParameter { Id = 0, Name = "No" }

            }, "Id", "Name", boolStatus);
        }
        public static SelectList GetDOBRangeType(this Controller controller, string dateType)
        {
            return new SelectList(new List<StringParameter> {
                new StringParameter { Id = "ALL", Name = "All" },
                new StringParameter { Id = "SPECIFIC", Name = "Specific" }
            }, "Id", "Name", dateType);
        }

        public static SelectList GroupBy(this Controller controller, string groupBy, string eliminate = "")
        {
            var list = new List<StringParameter> {
                new StringParameter {Id = "PSP", Name = "PSP"},
                new StringParameter {Id = "COUNTY", Name = "County"},
                new StringParameter {Id = "SUBCOUNTY", Name = "Sub County"},
                new StringParameter {Id = "LOCATION", Name = "Location"},
                new StringParameter {Id = "SUBLOCATION", Name = "SubLocation"}
            };
            if (!string.IsNullOrEmpty(eliminate))
            {
                list = list.Where(x => x.Id != eliminate).ToList();
            }

            return new SelectList(list, "Id", "Name", groupBy);
        }

        public static SelectList GroupBy2(this Controller controller, string groupBy, string eliminate = "")
        {
            var list = new List<StringParameter> {
                new StringParameter {Id = "COUNTY", Name = "County"},
                new StringParameter {Id = "SUBCOUNTY", Name = "Sub County"}
            };
            if (!string.IsNullOrEmpty(eliminate))
            {
                list = list.Where(x => x.Id != eliminate).ToList();
            }

            return new SelectList(list, "Id", "Name", groupBy);
        }

        public static SelectList NameMatch(this Controller controller, string groupBy, string eliminate = "")
        {
            var list = new List<StringParameter> {
                new StringParameter {Id = "Full Match", Name = "Full Match"},
                new StringParameter {Id = "Partial", Name = "Partial"},
                new StringParameter {Id = "FullMismatch", Name = "Full Mismatch"},
                
            };
            if (!string.IsNullOrEmpty(eliminate))
            {
                list = list.Where(x => x.Id != eliminate).ToList();
            }

            return new SelectList(list, "Id", "Name", groupBy);
        }

        public static string GetXml(this Controller controller, List<int> ints)
        {

            var serializer = new XmlSerializer(typeof(List<ListInt>), new XmlRootAttribute("Report") {
                Namespace = "",
            });
            var settings = new XmlWriterSettings {
                Indent = false,
                OmitXmlDeclaration = true,
                Encoding = Encoding.UTF8
            };
            var xml = string.Empty;
            var listInt = new List<ListInt>();
            listInt.AddRange(ints.Select(item => new ListInt { Id = item }));

            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");


            using (var sw = new StringWriter())
            {
                var xw = XmlWriter.Create(sw, settings);
                serializer.Serialize(xw, listInt, ns);
                xml += sw.ToString();
            }

            return xml;
        }

    }

    public static class MisKeys
    {
        public static string ACCOUNTACTIVITY = "ACCOUNTACTIVITY";

        public static string ACTIVITYREPORT = "ACTIVITYREPORT";

        public static string ACTIVITYREPORTFINALIZE = "ACTIVITYREPORTFINALIZE";

        public static string ApiData = "/apiData";

        public static string BENEFICIARIESSEARCH = "BENEFICIARIESSEARCH";

        public static string CREDITREPORT = "CREDITREPORT";

        public static string CREDITREPORTFINALIZE = "CREDITREPORTFINALIZE";

        public static string ENROLSUBMISSION = "ENROLSUBMISSION";

        public static string ERROR = "ERROR";

        public static string EMAIL = "EMAIL";

        public static string MisData = "MisData";

        public static string PAYMENTCARDSUBMISSION = "PAYMENTCARDSUBMISSION";

        public static string PAYROLLTRANSACTION = "PAYROLLTRANSACTION";
        public static string REGISTRATIONDATA = "REGISTRATION_DATA";
        public static string REGISTRATIONDATACV = "REGISTRATION_DATA_CV";
        public static string TARGETINGDATA = "TARGETING_DATA";
        public static string RETARGETINGDATA = "RE_TARGETING_DATA";

    }

    public class UserGroupCheckBoxListItem
    {
        public int? ParentModuleId { get; set; }
        public int Id { get; set; }
        public string Display { get; set; }
        public bool IsChecked { get; set; }
        public List<EnLocation> Rights { get; set; }

    }

    public class UserGroupEditorVm
    {
        public UserGroup UserGroup { get; set; }
        public List<ModuleRight> ModuleRights { get; set; }
        public List<Business.Model.Module> Modules { get; set; }
        public List<GroupRight> GroupRights { get; set; }
        public int UserGroupProfileId { get; set; }
        public int UserGroupTypeId { get; set; }
        public string[] ModuleRightId { get; set; }
    }




}