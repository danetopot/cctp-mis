﻿using System;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.RegApi
{
    public class SubLocationVm : RegionVm
    {
        public int LocationId { get; set; }
    }

    public class EnumeratorVm
    {

        public int Id { get; set; }
        public int ConstituencyId { get; set; }
        public Constituency Constituency { get; set; }
        public string SecurityStamp { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }

        [Display(Name = "Full names")]
        public string FullName => $"{FirstName} {MiddleName} {Surname}";
        public string NationalIdNo { get; set; }
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? ActivityDate { get; set; }
        public int AccessFailedCount { get; set; } = 0;
        public bool IsLocked { get; set; }
        public int? DeactivatedBy { get; set; }
        public DateTime? DeactivatedOn { get; set; }
        public DateTime? PasswordChangeDate { get; set; }
        public bool LockoutEnabled { get; set; }   
        public DateTime? LockoutEndDateUtc { get; set; }

    }


    public class ProgrammeVm :RegionVm
    {
        public int BeneficiaryTypeId { get; set; }
        public string Code { get; set; }
        public int PrimaryRecipientId { get; set; }
        public int? SecondaryRecipientId { get; set; }
        public bool SecondaryRecipientMandatory { get; set; }
        public bool IsActive { get; set; }

    }
    public class EnumeratorLocationVm
    {
        public int Id { get; set; }
        public int EnumeratorId { get; set; }
        public int LocationId { get; set; }
        public Enumerator Enumerator { get; set; }
        public Location Location { get; set; }
        public bool IsActive { get; set; }
    }

    public class TargetPlanVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int? TargetHHs { get; set; }
        public int CategoryId { get; set; }
        public int IsActive { get; set; }
        public int StatusId { get; set; }
    }
}