﻿namespace CCTPMIS.Models.RegApi
{
    public class CodeVm
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }
    }
}