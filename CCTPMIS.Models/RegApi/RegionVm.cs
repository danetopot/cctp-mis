﻿namespace CCTPMIS.Models.RegApi
{
    public class RegionVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}