﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.RegApi
{

    [XmlType(TypeName = "Record", Namespace = "")]
    public class RegistrationHHCvVm
    {
        public int Id { get; set; }
        public int CvStatusId { get; set; }
        public int CvEnumeratorId { get; set; }
        public int CvSyncEnumeratorId { get; set; }
        public DateTime CvSyncUpDate { get; set; }
        public DateTime CvUpdateDate { get; set; }
        public DateTime CvSyncDownDate { get; set; }
        public int? RegAcceptCvId { get; set; }
        public int RegistrationHhId { get; set; }
        public string UniqueId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public byte ProgrammeId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int RegPlanId { get; set; }
        public int EnumeratorId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HHdNationalIdNo { get; set; }
        public int HHdSexId { get; set; }
        public DateTime HHdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public DateTime CgDoB { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int SyncEnumeratorId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }
        public int? RegAcceptId { get; set; }

    }


    public class ComValDownloadListingPlanHH
    {
        public int Id { get; set; }
        public string UniqueId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public byte ProgrammeId { get; set; }
        public DateTime RegDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneNationalIdNo { get; set; }
        public string BenePhoneNumber { get; set; }
        public int? BeneSexId { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public string CgPhoneNumber { get; set; }
        public int CgSexId { get; set; }
        public DateTime CgDoB { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int SyncEnumeratorId { get; set; }
        public int DownEnumeratorId { get; set; }
        public DateTime SyncDate { get; set; }
        public int CgMatches { get; set; }
        public int BeneMatches { get; set; }
        public int HasBene { get; set; }
    }

    public class CvMember
    {

       // public int Id { get; set; }
        public string MemberId { get; set; }
        public string CareGiverId { get; set; }
        public string DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string IdentificationNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int SexId { get; set; }
        public int RegistrationId { get; set; }
        public int? RelationshipId { get; set; }
     }




    public class CvSetupVm
    {
        public List<ComValDownloadListingPlanHH> ComValListingPlanHHs { get; set; }
        public List<ComValListingPlanHH> Registrations { get; set; }
        public List<CvMember> RegistrationMembers { get; set; }
        public List<CvMember> RecerificationMembers { get; set; }
        public string Error { get; set; }
    }

    public class InitialSetup
    {
        public List<EnumeratorLocationVm> EnumeratorLocations { get; set; }

        public EnumeratorVm Enumerator { get; set; }
        public List<LocationVm> Locations { get; set; }
        public List<SubLocationVm> SubLocations { get; set; }
        public List<SystemCodeVm> SystemCodes { get; set; }
        public List<SystemCodeDetailVm> SystemCodeDetails { get; set; }
        public List<ProgrammeVm> Programmes { get; set; }
        public List<TargetPlanVm> TargetPlans { get; set; }

        public string Error { get; set; }
    }

    public class InitialSetupVm : InitialSetup
    {

    }

    public class ValRegistrationHhVm
    {
        public int Id { get; set; }
        public int? CvStatusId { get; set; }
        public DateTime CvDownloadDate { get; set; }
        public int? CvEnumeratorId { get; set; }
        public int? CvSyncEnumeratorId { get; set; }
        [Required, StringLength(36)] public string UniqueId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int RegPlanId { get; set; }
        public int EnumeratorId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HHdNationalIdNo { get; set; }
        public int HHdSexId { get; set; }
        public DateTime HHdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public DateTime CgDoB { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int SyncEnumeratorId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }


    }

   
    [XmlType(TypeName = "Record", Namespace = "")]
    public class RegistrationHhComVm : HHListingVm
    {
        public int Id { get; set; }
        public int CvStatusId { get; set; }
        public int CvEnumeratorId { get; set; }
        public int CvSyncEnumeratorId { get; set; }
        public DateTime CvSyncUpDate { get; set; }
        public DateTime CvUpdateDate { get; set; }
        public DateTime CvSyncDownDate { get; set; }
        public int? RegAcceptId { get; set; }
    }

    public class ComHHListingVm : HHListingVm
    {
        public int StatusId { get; set; }
        public int Id { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorDeviceId { get; set; }
        public int? ListingAcceptId { get; set; }
        public string SyncUpDate { get; set; }
        public string ComValDate { get; set; }
        public string DownloadDate { get; set; }
        public int? ComValListingAcceptId { get; set; }
        public int ListingPlanHhId { get; set; }

    }

    [XmlType(TypeName = "Record", Namespace = "")]
    public class HHListingVm: TabEnvironment
    {
        [Required, StringLength(36)] public string UniqueId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public DateTime RegDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int EnumeratorId { get; set; }
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneNationalIdNo { get; set; }
        public int? BeneSexId { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string BenePhoneNumber { get; set; }

        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public DateTime CgDoB { get; set; }

        public string CgPhoneNumber { get; set; }

        public int HouseholdMembers { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public string ExitReason { get; set; }

    }

    public class LoginVM
    {
        public string EmailAddress { get; set; }
        public string NationalId { get; set; }
        public string Pin { get; set; }
        public string Id { get; set; }
    }

    public class TabEnvironment
    {
        public int SyncEnumeratorId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }
    }

   
}


 