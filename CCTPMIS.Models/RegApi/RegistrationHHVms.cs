﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.RegApi
{

    public class  TabletHouseholdViewModel
    {
        [Required]
        public string HouseholdInfo { get; set; }
        [Required]
        public string DeviceInfo { get; set; }
    }

    public class RegistrationHHVm  
    {

        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public string RegistrationDate { get; set; }
        public int LocationId { get; set; }
        public int SubLocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }


        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        public int IsTelevisionId { get; set; }

        public int IsMotorcycleId { get; set; }
        public int IsTukTukId { get; set; }
        public int IsRefrigeratorId { get; set; }
        public int IsCarId { get; set; }
        public int IsMobilePhoneId { get; set; }
        public int IsBicycleId { get; set; }

        public int ExoticCattle { get; set; }
        public int IndigenousCattle { get; set; }
        public int Sheep { get; set; }
        public int Goats { get; set; }
        public int Camels { get; set; }
        public int Donkeys { get; set; }
        public int Pigs { get; set; }
        public int Chicken { get; set; }
        public int LiveBirths { get; set; }
        public int Deaths { get; set; }

        public int IsSkippedMealId { get; set; }
        public int IsReceivingSocialId { get; set; }
        public string Programmes { get; set; }
        public decimal LastReceiptAmount { get; set; }
        public string InKindBenefitId { get; set; }
        public int WasteDisposalModeId { get; set; }
        public int WaterSourceId { get; set; }
        public int WallConstructionMaterialId { get; set; }
        public int IsOwnedId { get; set; }
        public int TenureStatusId { get; set; }
        public int RoofConstructionMaterialId { get; set; }
        public string OtherProgrammeNames { get; set; }
        public int NsnpProgrammesId { get; set; }
        public int OtherProgrammesId { get; set; }
        public int LightingFuelTypeId { get; set; }
        public string IsMonetary { get; set; }
        public int HabitableRooms { get; set; }
        public int HouseHoldConditionId { get; set; }
        public int FloorConstructionMaterialId { get; set; }
        public int DwellingUnitRiskId { get; set; }
        public int CookingFuelTypeId { get; set; }
        public int BenefitTypeId { get; set; }
        public int InterviewResultId { get; set; }       
        public int? InterviewStatusId { get; set; }
        public string DownloadDate { get; set; }
        public string RegDate1 { get; set; }
        public string RegDate2 { get; set; }
        public string RegDate3 { get; set; }

        public List<RegistrationMember> RegistrationMembers { get; set; }

        public List<RegistrationMemberDisability> RegistrationMemberDisabilities { get; set; }

        public List<RegistrationProgramme> RegistrationProgrammes { get; set; }
    }

    public class RegistrationMemberDisability
    {

        public int Id { get; set; }
        public string RegistrationMemberId { get; set; }
        public int DisabilityId { get; set; }

        public int RegistrationId { get; set; }
    }

    public class RegistrationProgramme
    {

        public int Id { get; set; }
        public int RegistrationId { get; set; }
        public int ProgrammeId { get; set; }


    }
    public class RegistrationMember
    {

        public int Id { get; set; }
        public string MemberId { get; set; }
        public string CareGiverId { get; set; }
        public string DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string IdentificationNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int? SpouseInHouseholdId { get; set; }
        public string SpouseId { get; set; }
        public string Surname { get; set; }
        public int? ChronicIllnessStatusId { get; set; }

        public int? DisabilityCareStatusId { get; set; }

        public int? DisabilityTypeId { get; set; }

        public int? EducationLevelId { get; set; }

        public int? FatherAliveStatusId { get; set; }

        public int? FormalJobNgoId { get; set; }

        public int? IdentificationDocumentTypeId { get; set; }

        public int? LearningStatusId { get; set; }

        public int? MaritalStatusId { get; set; }

        public int? MotherAliveStatusId { get; set; }

        public int? RelationshipId { get; set; }

        public int? SexId { get; set; }
        public int? WorkTypeId { get; set; }

        public string MiddleName { get; set; }

        public int RecertificationId { get; set; }

        public string AddTime { get; set; }

    }

    public class RecertificationHHVm : TabEnvironment
    {

        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public string RecertificationDate { get; set; }
        public int LocationId { get; set; }
        public int SubLocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int TargetPlanId { get; set; }
        public int EnumeratorId { get; set; }
        public string HHdFullName { get; set; }
        public string CgFullName { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }

        public int IsTelevisionId { get; set; }

        public int IsMotorcycleId { get; set; }
        public int IsTukTukId { get; set; }
        public int IsRefrigeratorId { get; set; }
        public int IsCarId { get; set; }
        public int IsMobilePhoneId { get; set; }
        public int IsBicycleId { get; set; }

        public int ExoticCattle { get; set; }
        public int IndigenousCattle { get; set; }
        public int Sheep { get; set; }
        public int Goats { get; set; }
        public int Camels { get; set; }
        public int Donkeys { get; set; }
        public int Pigs { get; set; }
        public int Chicken { get; set; }
        public int LiveBirths { get; set; }
        public int Deaths { get; set; }

        public int IsSkippedMealId { get; set; }
        public int IsReceivingSocialId { get; set; }
        public string Programmes { get; set; }
        public decimal LastReceiptAmount { get; set; }
        public string InKindBenefitId { get; set; }
        public int WasteDisposalModeId { get; set; }
        public int WaterSourceId { get; set; }
        public int WallConstructionMaterialId { get; set; }
        public int IsOwnedId { get; set; }
        public int TenureStatusId { get; set; }
        public int RoofConstructionMaterialId { get; set; }
        public string OtherProgrammeNames { get; set; }
        public int NsnpProgrammesId { get; set; }
        public int OtherProgrammesId { get; set; }
        public int LightingFuelTypeId { get; set; }
        public string IsMonetary { get; set; }
        public int HabitableRooms { get; set; }
        public int HouseHoldConditionId { get; set; }
        public int FloorConstructionMaterialId { get; set; }
        public int DwellingUnitRiskId { get; set; }
        public int CookingFuelTypeId { get; set; }
        public int BenefitTypeId { get; set; }
        public int? InterviewStatusId { get; set; }
        public string DownloadDate { get; set; }
        public string RegDate1 { get; set; }
        public string RegDate2 { get; set; }
        public string RegDate3 { get; set; }

        public List<RecertificationMember> RecertificationMembers { get; set; }

        public List<RecertificationMemberDisability> RecertificationMemberDisabilities { get; set; }

        public List<RecertificationProgramme> RecertificationProgrammes { get; set; }
    }

    public class RecertificationMemberDisability
    {

        public int Id { get; set; }
        public string RecertificationMemberId { get; set; }
        public int DisabilityId { get; set; }

        public int RecertificationId { get; set; }
    }

    public class RecertificationProgramme
    {

        public int Id { get; set; }
        public int RecertificationId { get; set; }
        public int ProgrammeId { get; set; }


    }
    public class RecertificationMember
    {

        public int Id { get; set; }
        public string MemberId { get; set; }
        public string CareGiverId { get; set; }
        public string DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string IdentificationNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int? SpouseInHouseholdId { get; set; }
        public string SpouseId { get; set; }
        public string Surname { get; set; }
        public int? ChronicIllnessStatusId { get; set; }

        public int? DisabilityCareStatusId { get; set; }

        public int? DisabilityTypeId { get; set; }

        public int? EducationLevelId { get; set; }

        public int? FatherAliveStatusId { get; set; }

        public int? FormalJobNgoId { get; set; }

        public int? IdentificationDocumentTypeId { get; set; }

        public int? LearningStatusId { get; set; }

        public int? MaritalStatusId { get; set; }

        public int? MotherAliveStatusId { get; set; }

        public int? RelationshipId { get; set; }

        public int? SexId { get; set; }
        public int? WorkTypeId { get; set; }

        public string MiddleName { get; set; }

        public int RecertificationId { get; set; }

        public string AddTime { get; set; }

    }


}
