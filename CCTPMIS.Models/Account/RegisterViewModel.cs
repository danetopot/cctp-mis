﻿using System.Collections.Generic;
using CCTPMIS.Models.Registration;

namespace CCTPMIS.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Department")]
        public string Department { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        // [Required]
        // [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        // [DataType(DataType.Password)]
        // [Display(Name = "Password")]
        // public string Password { get; set; }

        // [Required]
        // [DataType(DataType.Password)]
        // [Display(Name = "Confirm password")]
        // [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        // public string ConfirmPassword { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Organization")]
        public string Organization { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Position")]
        public string Position { get; set; }

        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
        [Required]
        [Display(Name = "User Group")]
        public int UserGroupId { get; set; }

        
        [Display(Name = "Staff Number")]
        public string StaffNo { get; set; }
         
        [Display(Name = "National ID No.")]
        public string NationalIdNo { get; set; }
        
    }

    public class EditEnumeratorViewModel : RegisterEnumeratorViewModel
    {
        public int Id { get; set; }
    }


    public class RegisterEnumeratorViewModel
    {
        

        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        

        [Required]
        [Display(Name = "Phone Number")]
        public string MobileNo { get; set; }

       
        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
 

 
        [Required]
        [Display(Name = "National ID No.")]
        public string NationalIdNo { get; set; }


        public List<EnLocation> Locations { get; set; }


    }
}