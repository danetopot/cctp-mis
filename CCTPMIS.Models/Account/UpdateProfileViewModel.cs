﻿namespace CCTPMIS.Models.Account
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class UpdateProfileViewModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public int Id { get; set; }

        public string IsActive { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Surname")]
        public string Surname { get; set; }
    }
}