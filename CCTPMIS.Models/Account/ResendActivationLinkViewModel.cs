﻿namespace CCTPMIS.Models.Account
{
    public class ResendActivationLinkViewModel
    {
        public string Code { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }

        public string UserId { get; set; }
    }
}