﻿namespace CCTPMIS.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class EditUserContactViewModel
    {
        public int Id { get; set; }
        [StringLength(20)]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }

    public class EditUserProfileViewModel
    {
        [StringLength(50)]
        [RegularExpression("[A-Z]*$\\w*", ErrorMessage = "Invalid!")]
        public string Department { get; set; }

        [Display(Name = "First Name")]
        [StringLength(30)]
        [Required]
        [RegularExpression("^[ A-Za-z]*$", ErrorMessage = "Invalid!")]
        public string FirstName { get; set; }

        public int Id { get; set; }
        [RegularExpression("^[ A-Za-z]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Middle Name")]
        [StringLength(30)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        [RegularExpression("^[ A-Za-z0-9]*$", ErrorMessage = "Invalid!")]
        public string Organization { get; set; }

        [StringLength(20)]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [StringLength(20)]
        [RegularExpression("\\w*", ErrorMessage = "Invalid!")]
        public string Position { get; set; }

        [StringLength(30)]
        [RegularExpression("^[ A-Za-z]*$", ErrorMessage = "Invalid!")]
        public string Surname { get; set; }
    }
}