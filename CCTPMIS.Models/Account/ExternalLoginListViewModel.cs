﻿namespace CCTPMIS.Models.Account
{
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}