﻿namespace CCTPMIS.Models.Api
{
    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string State { get; set; }

        public string Url { get; set; }
    }
}