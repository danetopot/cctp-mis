﻿namespace CCTPMIS.Models.Api
{
    using System.ComponentModel.DataAnnotations;

    public class PspUpdatePaymentCardViewModel
    {
        [Required]
        public string AccountNo { get; set; }

        [Required]
        public string EnrolmentNo { get; set; }

        [Required]
        public string PaymentCardNo { get; set; }
    }
}