﻿namespace CCTPMIS.Models.Api
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class CreditReportViewModel
    {
        [Required]
        public string AccountName { get; set; }

        [Required]
        public string AccountNo { get; set; }

        [Required]
        public decimal AmountTransferred { get; set; }

        [Required]
        public string BankCode { get; set; }

        [Required]
        public string BranchCode { get; set; }

        [Required]
        public int EnrolmentNo { get; set; }

        [Required]
        public int PaymentCycleId { get; set; }

        public DateTime? TrxDate { get; set; }

        public string TrxNarration { get; set; }

        public string TrxNo { get; set; }

        [Required]
        public decimal WasTransferSuccessful { get; set; }
    }
}