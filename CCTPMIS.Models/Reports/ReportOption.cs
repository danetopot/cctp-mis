﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Reports
{
    public class ReportOption
    {
        [Display(Name = "Page")]
        public int Page { get; set; } = 1;
        [Display(Name = "Page Size")]
        public int PageSize { get; set; } = 10;

        [Display(Name = "Skip Records")]
        public int Skip => (this.Page * this.PageSize) - this.PageSize;


        [Display(Name = "Date Range")]
        public string DateRange { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        public int CompareDateRange { get; set; }
        public DateTime? CompareStartDate { get; set; }
        public DateTime? CompareEndDate { get; set; }
        [Display(Name = "Programme")]
        public IList<int> ProgrammeId { get; set; }
        [Display(Name = "Export to Csv")]
        public bool IsExportCsv { get; set; }
        [Display(Name = "County")]
        public IList<int> CountyId { get; set; }
        [Display(Name = "Sex of the Beneficiary")]
        public IList<int> SexId { get; set; }
        [Display(Name = "Sub County")]
        public IList<int> ConstituencyId { get; set; }
        [Display(Name = "Category")]
        public IList<int> ReportCategoryId { get; set; }

        [Display(Name = "Type of the Report")]
        public int ReportTypeId { get; set; }
        [Display(Name = "Beneficiary Date of Birth Range")]
        public string DobDateRangeId { get; set; }

        [Display(Name = "Beneficiary Date Range")]
        public string DobDateRange { get; set; }
        public DateTime? DobStartDate { get; set; }
        public DateTime? DobEndDate { get; set; }

        [Display(Name = "Group By (For Summary Reports)")]
        public string GroupBy { get; set; }

        [Display(Name = "Type of the Report")]
        public string Select { get; set; }

    }
}