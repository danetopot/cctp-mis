﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Reports
{
    public class HouseholdsEnrolmentReportOption : ReportOption
    {

        [Display(Name = "PSP")]
        public IList<int> PspId { get; set; }

        [Display(Name = "Card ")]
        public IList<int> CardStatusId { get; set; }

        [Display(Name = "Registration Group")]
        public IList<int> RegGroupId { get; set; }

        [Display(Name = "Enrolment Group")]
        public IList<int> EnrolmentGroupId { get; set; }

        [Display(Name = "Source")]
        public IList<int> SourceId { get; set; }

        [Display(Name = "Status")]
        public IList<int> StatusId { get; set; }

        [Display(Name = "Sex of Primary Recipient")]
        public IList<int> PriReciSexId { get; set; }

        [Display(Name = "Sex of Secondary Recipient")]
        public IList<int> SecReciSexId { get; set; }

        public ReportSummaryHouseholdsPendingEnrolment SummaryReport { get; set; }
        public ReportDetailedHouseholdEnrolment DetailedReport { get; set; }

        public IPagedList<ReportDetailedHouseholdEnrolment> Details { get; set; }
        public IPagedList<ReportSummaryHouseholdsPendingEnrolment> Summary { get; set; }


        public IPagedList<SummaryReportVieModel> SummaryReportVieModels { get; set; }


        public BeneficiaryAccount BeneficiaryAccount { get; set; }
        public IPagedList<BeneficiaryAccount> BeneficiaryAccounts { get; set; }




    }


    public class ReportSummaryHouseholdsPendingEnrolment:Report
    {
        public int MalePriReci { get; set; }

        public int MaleSecReci { get; set; }

        public int FemalePriReci { get; set; }

        public int FemaleSecReci { get; set; }

        public int TotalSecReci { get; set; }
        public int TotalPriReci { get; set; }

    }

    public class ReportDetailedHouseholdEnrolment :Report
    {

        public int Id { get; set; }
        public string BeneFullName => $"{BeneFirstName} {BeneMiddleName} {BeneSurname}";
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneIDNo { get; set; }
        public string BeneSex { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string CGFullName => $"{CGFirstName} {CGMiddleName} {CGSurname}";


        public string CGFirstName { get; set; }
        public string CGMiddleName { get; set; }
        public string CGSurname { get; set; }
        public string CGIDNo { get; set; }
        public string CGSex { get; set; }
        public DateTime? CGDoB { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public DateTime? CreatedOn { get; set; }

        public string Psp { get; set; }
        public string PSPBranch { get; set; }
        public string CardingStatus { get; set; }
        public string AccountStatus { get; set; }
        public string HouseholdStatus { get; set; }

        public string AccountName { get; set; }

        public string AccountNo { get; set; }

        public string PaymentCardNo { get; set; }
        public DateTime? AccountOpenedOn { get; set; }
        public DateTime? AccountAddedOn { get; set; }

    }

}
