﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Reports
{
    public class CaseManagementReportOption : ReportOption
    {

        [Display(Name = "Complaint Type")]
        public IList<int> ComplaintTypeId { get; set; }
        [Display(Name = "Complaint Level")]
        public IList<int> ComplaintLevelId { get; set; }
        [Display(Name = "Complaint Status")]
        public IList<int> ComplaintStatusId { get; set; }
        

        [Display(Name = "Updates Type")]
        public IList<int> ChangeTypeId { get; set; }
        [Display(Name = "Updates Level")]
        public IList<int> ChangeLevelId { get; set; }
        [Display(Name = "Updates Status")]
        public IList<int> ChangeStatusId { get; set; }
        [Display(Name = "Source of EFC,Complaint or Update")]

        public IList<int> SourceId { get; set; }

        [Display(Name = "EFC Type")]
        public IList<int> EFCTypeId { get; set; }

        [Display(Name = "Status")]
        public IList<int> StatusId { get; set; }


        public IPagedList<Complaint> Complaints { get; set; }
        public Complaint Complaint { get; set; }
        public ReportSummaryComplaint SummaryReport { get; set; }
        public IPagedList<ReportSummaryComplaint> Summary { get; set; }
        public IPagedList<ReportDetailComplaint> Details { get; set; }
        public IPagedList<ReportDetailComplaint> DetailReports { get; set; }

        public IPagedList<ReportSummaryChange> SummaryChanges { get; set; }
        public IPagedList<ReportDetailChange> DetailChanges { get; set; }
        public ReportDetailComplaint DetailReport { get; set; }

        //public List<SummaryReportVieModel> SummaryReportVieModels { get; set; }
        public IPagedList<SummaryReportVieModel> SummaryReportVieModels { get; set; }
        public SummaryReportVieModel SummaryReportVieModel { get; set; }

        public IPagedList<EFC> EFCs { get; set; }
        public EFC EFC { get; set; }
        public IPagedList<Change> Changes { get; set; }
        public Change Change { get; set; }
        public IPagedList<MultipleUpdate> MultipleUpdates { get; set; }
        public MultipleUpdate MultipleUpdate { get; set; }
    }

    public class MultipleUpdate
    {
        public int? ReportTypeId { get; set; }
    }

    public class ComplaintDetailReport
    {
        public int? SerialNo { get; set; }
        public string Programme { get; set; }
        public string ComplaintType { get; set; }
        public string ComplaintLevel { get; set; }
        public string ReportedByType { get; set; }
        public string ReportedBySex { get; set; }
        public DateTime ReceivedOn { get; set; }
        public string ReceivedBy { get; set; }
        public string Constituency { get; set; }
        public string County { get; set; }
    }
    public class ComplaintSummaryReport
    {
        public string Name { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }

    public class EFCDetailReport
    {
        public int? Id { get; set; }
        public int? SerialNo { get; set; }
        public string EFCType { get; set; }
        public string ReportedByType { get; set; }
        public string ReportedBySex { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime? ReceivedOn { get; set; }
        public string Constituency { get; set; }
        public string County { get; set; }
        public string Status { get; set; }
    }
    public class EFCSummaryReport
    {
        public string Name { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }
    

    public class ChangeDetailReport
    {
        
        public string Programme { get; set; }
        public string ChangeType { get; set; }
        public string ReportedByType { get; set; }
        public string ReportedBySex { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceivedOn { get; set; }
        public string SLA { get; set; }
        public string Constituency { get; set; }
        public string County { get; set; }
    }
    public class ChangeSummaryReport
    {
         public string Name { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }

    public class ChangeSummaryReportCounty
    {
        public string Name { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }
    public class ChangeSummaryReportSubCounty
    {
        public string Name { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }


    }

    public class MultipleUpdateDetailReport
    {
        public string ProgrammeCode { get; set; }
        public int EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public string CaseCategory { get; set; }
        public string ChangeLevel { get; set; }
        public string ChangeStatus { get; set; }
        public string PriReciFirstName { get; set; }
        public string PriReciMiddleName { get; set; }
        public string PriReciSurname { get; set; }
        public string PriReciSex { get; set; }
        public string SecReciFirstName { get; set; }
        public string SecReciMiddleName { get; set; }
        public string SecReciSurname { get; set; }
        public string SecReciSex { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public string ReportedBySex { get; set; }
        public string ReportedByType { get; set; }
        public string ReceivedBy { get; set; }
        public string SLA { get; set; }
    }
    public class MultipleUpdateSummaryReport
    {
        public string County { get; set; }
        public string Constituency { get; set; }
        public string Location { get; set; }
        public string Programme { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }

    }


    public class ReportDetailChange : Report
    {
        public string CaseCategory { get; set; }
        public string ChangeLevel { get; set; }
        public string ChangeStatus { get; set; }

        public string ReportedBySex { get; set; }
        public string ReportedByType { get; set; }
        public string ReceivedBy { get; set; }
        //public DateTime Type { get; set; }
        public string ProgrammeCode { get; set; }
        //public byte ProgrammeId { get; set; }
        public int? ChangeTypeId { get; set; }
        public string ChangeType { get; set; }
        public int? ChangeStatusId { get; set; }
        public int Id { get; set; }
        public int? ReportedByTypeId { get; set; }

        public int? PriReciSexId { get; set; }

        public DateTime? ReceivedOn { get; set; }

        public int? ReportTypeId { get; set; }
        /*
        public string CaseCategory { get; set; }
        public string ChangeLevel { get; set; }
        public string ChangeStatus { get; set; }
        public string ReportedBySex { get; set; }
        public string ReportedByType { get; set; }
        public string ReceivedBy { get; set; }
        //public DateTime Type { get; set; }
        public string ProgrammeCode { get; set; }
        public byte ProgrammeId { get; set; }
        public int? ChangeTypeId { get; set; }
        public string ChangeType { get; set; }
        public int? ChangeStatusId { get; set; }
        public int Id { get; set; }
        public int? ReportedByTypeId { get; set; }
        public int? PriReciSexId { get; set; }
        public DateTime? ReceivedOn { get; set; }
        */
    }
    public class ReportSummaryChange : Report
    {

        public string CaseCategory { get; set; }
        public string ChangeLevel { get; set; }
        public string ChangeStatus { get; set; }
        public string ReportedBySex { get; set; }
        public string ReportedByType { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }

    }

    public class GroupedgeoReport
    {
        public string County { get; set; }
    }
    public class GroupedReport
    {
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public IList<Report> Reports { get; set; }

        public string Programme { get; set; }
    }

    public class BeneAccountMonthlyActivityDetailReport
    {
        public string Programme { get; set; }
        public string BeneProgNoPrefix { get; set; }
        public int HhEnrolmentId { get; set; }
        public int ProgrammeNumber { get; set; }

        public string ProgrammeNo => $"{BeneProgNoPrefix}{ProgrammeNumber:D6}";

        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string Surname { get; set; }

        public string PersonDisplayName {
            get
            {
                return FirstName + " " + MiddleName + " " + Surname;
            }
        }

        public int? MemberRoleId { get; set; }
        public int? PrimaryRecipientId { get; set; }
        public string Sex { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public DateTime OpenedOn { get; set; }

        public string Branch { get; set; }
        public string Psp { get; set; }
    }

    public class BeneAccountMonthlyActivitySummaryReport
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string SubLocation { get; set; }
        public string Location { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }

    public class BeneAccountMonthlyActivityDetailFullReport : BeneAccountMonthlyActivityDetailReport
    {

        public bool HadUniqueWdl { get; set; }
        public string UniqueWdlTrxNo { get; set; }
        public DateTime? UniqueWdlDate { get; set; }
        public bool IsDueForClawback { get; set; }
        public decimal? ClawbackAmount { get; set; }
        public bool IsDormant { get; set; }
        public DateTime? DormancyDate { get; set; }
        public bool HadBeneBiosVerified { get; set; }
    }

    public class BeneAccountMonthlyActivityWithdrawalReport : BeneAccountMonthlyActivityDetailReport
    {

        public bool HadUniqueWdl { get; set; }
        public string UniqueWdlTrxNo { get; set; }
        public DateTime? UniqueWdlDate { get; set; }


    }
    public class BeneAccountMonthlyActivityClawbackReport : BeneAccountMonthlyActivityDetailReport
    {


        public bool IsDueForClawback { get; set; }
        public decimal? ClawbackAmaount { get; set; }


    }
    public class BeneAccountMonthlyActivityDormancyReport : BeneAccountMonthlyActivityDetailReport
    {


        public bool IsDormant { get; set; }
        public DateTime? DormancyDate { get; set; }


    }
    public class BeneAccountMonthlyActivityBiosReport : BeneAccountMonthlyActivityDetailReport
    {


        public bool HadBeneBiosVerified { get; set; }

    }


    public class SummaryReportVieModelMin
    {
        public int GeoMasterId { get; set; }
        public int? Id { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int CountyId { get; set; }
        public int ConstituencyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SubLocation { get; set; }
        public string Location { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }


    public class SummaryReportVieModel: SummaryReportVieModelMin
    {

        [Display(Name="Total Amount")]
        public decimal TotalAmount { get; set; }
        [Display(Name = "Female Amount")]
        public decimal FemaleAmount { get; set; }
        [Display(Name = "Male Amount")]
        public decimal MaleAmount { get; set; }
        [Display(Name = "Entitlement Amount")]
        public decimal Entitlementmount { get; set; }
        [Display(Name = "Arrears Amount")]
        public decimal AdjustmentAmount { get; set; }
    }




    public class GroupedSummaryReport
    {
        public object PaymentCycle { get; set; }
        public int Households { get; set; }

        public string County { get; set; }
        public string Constituency { get; set; }
        public int ConstituencyId { get; set; }
        public string SubLocation { get; set; }

        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }

        [Display(Name = "Total Amount")]
        public decimal TotalAmount { get; set; }
        [Display(Name = "Total Amount")]
        public decimal FemaleAmount { get; set; }
        [Display(Name = "Male Amount")]
        public decimal MaleAmount { get; set; }
    }


    public class GenericSummaryReportMin
    {
        public int? Id { get; set; }
        public int CountyId { get; set; }
        public int ConstituencyId { get; set; }
        public int LocationId { get; set; }
        public int SubLocationId { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }

    public class GenericSummaryReport : GenericSummaryReportMin
    {

        [Display(Name = "Total Amount")]
        public decimal TotalAmount { get; set; }
        [Display(Name = "Female Amount")]
        public decimal FemaleAmount { get; set; }
        [Display(Name = "Male Amount")]
        public decimal MaleAmount { get; set; }
        [Display(Name = "Entitlement Amount")]
        public decimal Entitlementmount { get; set; }
        [Display(Name = "Arrears Amount")]
        public decimal AdjustmentAmount { get; set; }
    }

    public class SummaryReportMin
    {

        public int Id { get; set; }
        public int? CountyId { get; set; }
        public int? ConstituencyId { get; set; }
        public int? LocationId { get; set; }
        public int? SubLocationId { get; set; }
        public int? Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
        public string BeneSexId { get; set; }
        public string CgSexId { get; set; }
    }

    public class SummaryReport: SummaryReportMin
    {
       
       

        public int? PSPId { get; set; }
        public decimal PaymentAmount { get; set; }

        [Display(Name = "Total Amount")]
        public decimal TotalAmount { get; set; }
        [Display(Name = "Total Amount")]
        public decimal FemaleAmount { get; set; }
        [Display(Name = "Male Amount")]
        public decimal MaleAmount { get; set; }
        [Display(Name = "Entitlement Amount")]
        public decimal Entitlementmount { get; set; }
        [Display(Name = "Arrears Amount")]
        public decimal AdjustmentAmount { get; set; }

    }

    public class Report
    {
        public string SLA { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public int ConstituencyId { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public int? GeoMasterId { get; set; }
        public int? CountyId { get; set; }
        public int NoOfRows { get; set; }
        public int? MeetSLA { get; set; }
        public int? FailSLA { get; set; }
        public string ProgrammeNo { get; set; }
        public string Programme { get; set; }
        public byte? ProgrammeId { get; set; }
        public int? HHId { get; set; }
        public int? Updates { get; set; }
        public int? PaymentCycleId { get; set; }

    }

    public class ReportSummaryEFC : Report
    {
        public string CaseCategory { get; set; }
        public string EFCStatus { get; set; }
        public string EFCType { get; set; }

        public int Open { get; set; }
        public int Closed { get; set; }
        public int Total { get; set; }
        public int NoOfRows { get; set; }
    }


    public class ReportDetailEFC : Report
    {
        public string EFCType { get; set; }
        public string Status { get; set; }
        public string ReportedBySex { get; set; }
        public string ReportedByType { get; set; }
        public string ReceivedBy { get; set; }
        public int? EFCTypeId { get; set; }
        public int? EFCStatusId { get; set; }
        public int Id { get; set; }
        public int? ReportedByTypeId { get; set; }
        public DateTime? ReportedDate { get; set; }
    }

    public class PaymentDetailReport
    {
        //public int HhEnrolmentId { get; set; }
        public string Programme { get; set; }
        public string PaymentCycle { get; set; }
        public int ProgrammeNumber { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }

        
        public string AccountOpenedOn { get; set; }

       
        public string AccountSubmittedOn { get; set; }

        public string AccountStatus { get; set; }
        public string BeneficiaryFirstName { get; set; }
        public string BeneficiaryMiddleName { get; set; }
        public string BeneficiarySurName { get; set; }

        
        public string BeneficiaryDOB { get; set; }

        public string BeneficiaryGender { get; set; }
        public string BeneficiaryNationalIDNumber { get; set; }
        public string PSP { get; set; }
        public decimal? PaymentAmount { get; set; }
        public decimal? EntitlementAmount { get; set; }
        public decimal? ArrearsAmount { get; set; }
        public string TransactionSuccessful { get; set; }
        public decimal? TransactionAmount { get; set; }
        public string TransactionNumber { get; set; }

        
        public string TransactionDate { get; set; }

        public string TransactionNarration { get; set; }
    }
}