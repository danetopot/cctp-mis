﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Payment;
using PagedList;

namespace CCTPMIS.Models.Reports
{

    public class PrepayrollExceptionsReportViewModel : ReportOption
    {


        [Display(Name = "Exception Type")]
        public IList<int> ExceptionTypeId { get; set; }

        [Display(Name = "PSP")]
        public IList<int> PspId { get; set; }

        [Display(Name = "Status")]
        public IList<int> StatusId { get; set; }

        [Display(Name = "Sex of Primary Recipient")]
        public IList<int> PriReciSexId { get; set; }

        [Display(Name = "Sex of Secondary Recipient")]
        public IList<int> SecReciSexId { get; set; }


        public PrepayrollDuplicateId PrepayrollDuplicateIdDetail { get; set; }

        public PrepayrollIneligible PrepayrollIneligibleDetail { get; set; }
        public PrepayrollInvalidId PrepayrollInvalidIdDetail { get; set; }

        public PrepayrollInvalidPaymentAccount PrepayrollInvalidPaymentAccountDetail { get; set; }

        public PrepayrollInvalidPaymentCard PrepayrollInvalidPaymentCardDetail { get; set; }

        public PrepayrollSuspicious PrepayrollSuspiciousDetail { get; set; }

        public IPagedList<PrepayrollDuplicateId> PrepayrollDuplicateIdList { get; set; }
        public PrepayrollExceptionsFilterViewModel PrepayrollExceptionsFilterViewModel { get; set; }
        public IPagedList<PrepayrollIneligible> PrepayrollIneligiblesList { get; set; }
        public IPagedList<PrepayrollInvalidId> PrepayrollInvalidIdsList { get; set; }
        public IPagedList<PrepayrollInvalidPaymentAccount> PrepayrollInvalidPaymentAccountsList { get; set; }
        public IPagedList<PrepayrollInvalidPaymentCard> PrepayrollInvalidPaymentCardsList { get; set; }
        public IPagedList<PrepayrollSuspicious> PrepayrollSuspiciousList { get; set; }
    }



    public class PrepayrollReportOption : ReportOption
    {
        [Display(Name = "Exception Type")]
        public string ExceptionTypeId { get; set; }

        [Display(Name = "PSP")]
        public IList<int> PspId { get; set; }

        [Display(Name = "Status")]
        public IList<int> StatusId { get; set; }

        [Display(Name = "Sex of Primary Recipient")]
        public IList<int> PriReciSexId { get; set; }

        [Display(Name = "Sex of Secondary Recipient")]
        public IList<int> SecReciSexId { get; set; }

        public ReportSummaryPrepayroll SummaryReport { get; set; }
        public ReportDetailedPrepayroll DetailedReport { get; set; }
        public Prepayroll Prepayroll { get; set; }
        public Payroll Payroll { get; set; }

        public List<ReportDetailedPrepayroll> Details { get; set; }
        public List<ReportSummaryPrepayroll> Summary { get; set; }
        public IPagedList<Prepayroll> Prepayrolls { get; set; }
        public IPagedList<Payroll> Payrolls { get; set; }
        public IPagedList<FundsRequestDetail> FundsRequestDetails { get; set; }
        public IPagedList<ReconciliationDetail> ReconciliationDetails { get; set; }
        public ReconciliationDetail ReconciliationDetail { get; set; }
        public FundsRequestDetail FundsRequestDetail { get; set; }

        public Business.Model.Payment Payment { get; set; }
        public IPagedList<Business.Model.Payment> Payments { get; set; }
        [Display(Name = "Was Transaction Successful")]
        public int? WasTrxSuccessful { get; set; }
        [Display(Name = "Has Arrears")]
        public int? HasArrears { get; set; }


        public IPagedList<BeneAccountMonthlyActivityDetail> BeneAccountMonthlyActivityDetails { get; set; }
        public BeneAccountMonthlyActivityDetail BeneAccountMonthlyActivityDetail { get; set; }


        public IPagedList<GenericSummaryReport> GenericSummaryReports { get; set; }
        public IPagedList<SummaryReport> SummaryReports { get; set; }

        public GenericSummaryReport GenericSummaryReport { get; set; }
        public SummaryReport GenericReport { get; set; }

        public List<SummaryReportVieModel> SummaryReportVieModels { get; set; }

        public SummaryReportVieModel SummaryReportVieModel { get; set; }
        public int? HasUniqueWithdrawal { get; set; }
        public int? HasBiosVerified { get; set; }
        public int? IsDormant { get; set; }
        public int? IsDueForClawback { get; set; }

        public IPagedList<BeneAccountMonthlyActivityDetailFullReport> BeneAccountMonthlyActivityDetailFullReports { get; set; }

        public BeneAccountMonthlyActivityDetailFullReport BeneAccountMonthlyActivityDetailFullReport { get; set; }

        public IPagedList<BeneAccountMonthlyActivityWithdrawalReport> BeneAccountMonthlyActivityWithdrawalReports { get; set; }

        public IPagedList<BeneAccountMonthlyActivityClawbackReport> BeneAccountMonthlyActivityClawbackReports { get; set; }

        public IPagedList<BeneAccountMonthlyActivityBiosReport> BeneAccountMonthlyActivityBiosReports { get; set; }

        public IPagedList<BeneAccountMonthlyActivityDormancyReport> BeneAccountMonthlyActivityDormancyReports { get; set; }

        public BeneAccountMonthlyActivityWithdrawalReport BeneAccountMonthlyActivityWithdrawalReport { get; set; }
        public BeneAccountMonthlyActivityClawbackReport BeneAccountMonthlyActivityClawbackReport { get; set; }
        public BeneAccountMonthlyActivityBiosReport BeneAccountMonthlyActivityBiosReport { get; set; }
        public BeneAccountMonthlyActivityDormancyReport BeneAccountMonthlyActivityDormancyReport { get; set; }
    }

    public class ReportDetailedPrepayroll : Report
    {
        public int Id { get; set; }
        public string BeneFullName => $"{BeneFirstName} {BeneMiddleName} {BeneSurname}";
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneIDNo { get; set; }
        public string BeneSex { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string CGFullName => $"{CGFirstName} {CGMiddleName} {CGSurname}";


        public string CGFirstName { get; set; }
        public string CGMiddleName { get; set; }
        public string CGSurname { get; set; }
        public string CGIDNo { get; set; }
        public string CGSex { get; set; }
        public DateTime? CGDoB { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public DateTime? CreatedOn { get; set; }

        public string Psp { get; set; }
        public string PSPBranch { get; set; }
        public string CardingStatus { get; set; }
        public string AccountStatus { get; set; }
        public string HouseholdStatus { get; set; }

        public string AccountName { get; set; }

        public string AccountNo { get; set; }

        public string PaymentCardNo { get; set; }
        public DateTime? AccountOpenedOn { get; set; }
        public DateTime? AccountAddedOn { get; set; }
    }
    public class ReportSummaryPrepayroll : Report
    {
        public int MalePriReci { get; set; }

        public int MaleSecReci { get; set; }

        public int FemalePriReci { get; set; }

        public int FemaleSecReci { get; set; }

        public int TotalSecReci { get; set; }
        public int TotalPriReci { get; set; }
    }
}
