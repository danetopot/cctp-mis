﻿using System;
using System.Xml.Serialization;

namespace CCTPMIS.Models.Reports
{
    [XmlType(TypeName = "Record", Namespace = "", IncludeInSchema = false)]
    public class ListInt
    {
        public int Id { get; set; }
    }

    public class DateRange
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }

    public class ReportGeoTables
    {
        public int GeoMasterId { get; set; }
        public int? Id { get; set; }
        public int SubLocationId { get; set; }
        public string SubLocation { get; set; }
        public string Location { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }


}