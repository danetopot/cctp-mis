﻿using System;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Reports
{

    public class ReportVM
    {

    }

    public class ComplaintReportVM : ReportVM
    {
        [Display(Name = "Report Type")]
        public int? ReportTypeId { get; set; }
        [Display(Name = "Programme")]
        public int ProgrammeId { get; set; }
        [Display(Name = "Type")]
        public int ComplaintTypeId { get; set; }
        [Display(Name = "Status")]
        public int ComplaintStatusId { get; set; }
        [Display(Name = "Source Date")]
        public int? SourceId { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Period")]
        public int? Period { get; set; }
        public IPagedList<Complaint> Complaints { get; set; }
        public Complaint Complaint { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        [Display(Name = "Category")]
        public int ComplaintCategoryId { get; set; }
        [Display(Name = "Escalation Level")]
        public int ComplaintLevelId { get; set; }


        public IPagedList<Change> Changes { get; set; }
        public Change Change { get; set; }
        public IPagedList<EFC> EFCs { get; set; }
        public EFC EFC { get; set; }
    }

   

    public class ChangeReportVM
    {
        [Display(Name = "Report Type")]
        public int? ReportTypeId { get; set; }
        [Display(Name = "Programme")]
        public int ProgrammeId { get; set; }
        [Display(Name = "Type")]
        public int ComplaintTypeId { get; set; }
        [Display(Name = "Status")]
        public int ComplaintStatusId { get; set; }
        [Display(Name = "Source Date")]
        public int? SourceId { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Period")]
        public int? Period { get; set; }
        public IPagedList<Complaint> Complaints { get; set; }
        public Complaint Complaint { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        [Display(Name = "Category")]
        public int ComplaintCategoryId { get; set; }
        [Display(Name = "Escalation Level")]
        public int ComplaintLevelId { get; set; }


        public IPagedList<Change> Changes { get; set; }
        public Change Change { get; set; }
        public IPagedList<EFC> EFCs { get; set; }
        public EFC EFC { get; set; }
    }
}
