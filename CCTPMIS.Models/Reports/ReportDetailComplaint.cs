﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Reports
{
    public class ReportDetailComplaint :Report 
    {
        public int Id { get; set; }
        public byte ProgrammeId { get; set; }
        public int HhEnrolmentId { get; set; }
        public int? ComplaintTypeId { get; set; }
        public int? SerialNo { get; set; }
        public string ReportedByName { get; set; }
        public string ReportedByIdNo { get; set; }
        public string ReportedByPhone { get; set; }
        public int? ReportedBySexId { get; set; }
        public int ReportedByTypeId { get; set; }
        public DateTime? ComplaintDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public int? ApvBy { get; set; }
        public DateTime? ApvOn { get; set; }
        public int? ResolvedBy { get; set; }
        public DateTime? ResolvedOn { get; set; }
        public int? ClosedBy { get; set; }
        public DateTime? ClosedOn { get; set; }
        public int? VerifiedBy { get; set; }
        public DateTime? VerifiedOn { get; set; }
        public int? EscalatedBy { get; set; }
        public DateTime? EscalatedOn { get; set; }
        public int? Escalated2By { get; set; }
        public DateTime? Escalated2On { get; set; }
        public int? ComplaintStatusId { get; set; }
        public int? ComplaintLevelId { get; set; }

        [Display(Name = "Received By")]
        public string ReceivedBy { get; set; }

        public string Designation { get; set; }
        public int? SourceId { get; set; }
        public int? ConstituencyId { get; set; }

        [Display(Name = "Received On")]
        public DateTime? ReceivedOn { get; set; }

        public string ProgrammeCode { get; set; }
        public string ProgrammeName { get; set; }
        public int EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public int PriReciPersonId { get; set; }
        public string PriReciFirstName { get; set; }
        public string PriReciMiddleName { get; set; }
        public string PriReciSurname { get; set; }
        public int PriReciSexId { get; set; }
        public string PriReciSex { get; set; }
        public string PriReciNationalIdNo { get; set; }
        public string PriReciBirthCertNo { get; set; }
        public string PriReciMobileNo1 { get; set; }
        public string PriReciMobileNo2 { get; set; }
        public int? SecReciPersonId { get; set; }
        public string SecReciFirstName { get; set; }
        public string SecReciMiddleName { get; set; }
        public string SecReciSurname { get; set; }
        public int? SecReciSexId { get; set; }
        public string SecReciSex { get; set; }
        public string SecReciNationalIdNo { get; set; }
        public string SecReciBirthCertNo { get; set; }
        public string SecReciMobileNo1 { get; set; }
        public string SecReciMobileNo2 { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public int GeoMasterId { get; set; }
        public int CountyId { get; set; }
        public string CaseCategory { get; set; }
        public string ComplaintLevel { get; set; }
        public string ComplaintStatus { get; set; }

        [Display(Name = "Reported By Sex")]
        public string ReportedBySex { get; set; }

        [Display(Name = "Reported By")]
        public string ReportedByType { get; set; }
        public int NoOfRows { get; set; }
    }
}