﻿namespace CCTPMIS.Models.Reports
{
    public class ReportSummaryComplaint :Report 
    {
        public string County { get; set; }
        public string Constituency { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public string Programme { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }

        public int NoOfRows { get; set; }


    }
}