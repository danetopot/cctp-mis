﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Reports
{
    public class TargetingReportVm : ReportOption
    {


        public IPagedList<ListingPlanHHReportVm> HouseholdListingDetails { get; set; }
        public IPagedList<ListingException> HouseholdListingExceptions { get; set; }
        public IPagedList<ComValListingException> ComValListingExceptions { get; set; }
        public ListingException ListingException { get; set; }

        public ComValListingException ComValListingException { get; set; }
         
        public IPagedList<ComValListingPlanHHReportVm> ComValListingPlanDetails { get; set; }

        public IPagedList<HouseholdReg> HouseholdRegs { get; set; }
        public IPagedList<HouseholdRec> HouseholdRecs { get; set; }

        public HouseholdRec HouseholdRec { get; set; }
        public HouseholdReg HouseholdReg { get; set; }
        public ListingPlanHH ListingPlanHH { get; set; }
        public ComValListingPlanHH ComValListingPlanHH { get; set; }

        [Display(Name = "Exception Type")]
        public int? ExceptionTypeId { get; set; }

        [Display(Name = "PSP")]
        public IList<int> PspId { get; set; }

        [Display(Name = "Status")]
        public IList<int> StatusId { get; set; }

        [Display(Name = "Sex of Caregiver")]
        public IList<int> CgSexId { get; set; }

        [Display(Name = "Sex of Beneficiary")]
        public IList<int> BeneSexId { get; set; }


        [Display(Name = "Successful")]
        public int? RejectedId { get; set; }

        public List<SummaryReportVieModelMin> SummaryReportVieModelMins { get; set; }
        public SummaryReportVieModelMin SummaryReportVieModelMin { get; set; }

        [Display(Name = "Caregiver Name Match")]
        public string CgNameMatch { get; set; }

        [Display(Name = "Beneficiary Name Match")]
        public string BeneNameMatch { get; set; }

        [Display(Name = "Beneficiary ID No. Exists")]
        public int? BeneIdNoExists { get; set; }


         

        [Display(Name = "Beneficiary Date Of Birth Matches")]
        public int? BeneDobMatches { get; set; }

        [Display(Name = "Beneficiary Year Of Birth Matches")]
        public int? BeneDobYearMatches { get; set; }


        [Display(Name = "Beneficiary Sex Matches")]
        public int? BeneSexMatches { get; set; }


        [Display(Name = "Caregiver ID No. Exists")]
        public int? CgIdNoExists { get; set; }

        [Display(Name = "Caregiver Date Of Birth Matches")]
        public int? CgDobMatches { get; set; }

        [Display(Name = "Caregiver Year Of Birth Matches")]
        public int? CgDobYearMatches { get; set; }


        [Display(Name = "Caregiver Sex Matches")]
        public int? CgSexMatches { get; set; }



        [Display(Name = "Interview Status")]
        public IList<int> InterviewStatusId { get; set; }

        [Display(Name = "Interview Result")]
        public IList<int> InterviewResultId { get; set; }

        [Display(Name = "Has Orphans")]
        public int? HasOrphans { get; set; }

        [Display(Name = "Has Disabled Persons")]
        public int? HasDisability { get; set; }
        

    }

    public class ComValListingExceptionReportVm : ListingExceptionReportVm 
    {
    }

    public class ListingExceptionReportVm
    {
       
        public int Id { get; set; }

       
        public string UniqueId { get; set; }
        public string BeneName { get; set; }
        public string BeneSex { get; set; }
        public string BeneDob { get; set; }
        public string BeneNationalIdNo { get; set; }
        public long? IPRS_IDNo { get; set; }
        public string IPRS_Name { get; set; }
        public string IPRS_Sex { get; set; }
        public DateTime? IPRS_DoB { get; set; }
        public bool Bene_IDNoExists { get; set; }
        public bool Bene_FirstNameExists { get; set; }
        public bool Bene_MiddleNameExists { get; set; }
        public bool Bene_SurnameExists { get; set; }
        public bool Bene_DoBMatch { get; set; }
        public bool Bene_DoBYearMatch { get; set; }
        public bool Bene_SexMatch { get; set; }
        public string CgName { get; set; }
        public string CgSex { get; set; }
        public DateTime CgDob { get; set; }
        public string CgNationalIdNo { get; set; }
        public string IPRS_CG_IDNo { get; set; }
        public string IPRS_CG_Name { get; set; }
        public string IPRS_CG_Sex { get; set; }
        public DateTime? IPRS_CG_DoB { get; set; }
        public bool CG_IDNoExists { get; set; }
        public bool CG_FirstNameExists { get; set; }
        public bool CG_MiddleNameExists { get; set; }
        public bool CG_SurnameExists { get; set; }
        public bool CG_DoBMatch { get; set; }
        public bool CG_DoBYearMatch { get; set; }
        public bool CG_SexMatch { get; set; }
        public int TargetPlanId { get; set; }
        public string TargetPlan { get; set; }
        public DateTime DateValidated { get; set; }
    }

    public class ComValListingPlanHHReportVm : ListingPlanHHReportVm
    {
        public DateTime?  ComValDate { get; set; }


    }

    public class ListingPlanHHReportVm
    {
        [Display(Name = "Targeting ID")]
        public int Id { get; set; }
        [Display(Name = "Reference No.")]
        public string UniqueId { get; set; }
        [Display(Name = "Start Time")]
        public DateTime? StartTime { get; set; }
        [Display(Name = "End Time")]
        public DateTime? EndTime { get; set; }
        [Display(Name = "Programme")]
        public string Programme { get; set; }
        [Display(Name = "Registration Date")]
        public DateTime RegDate { get; set; }
        [Display(Name = "Sub Location ")]
        public string SubLocation { get; set; }

        [Display(Name = "Location ")]
        public string Location { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }
        [Display(Name = "Target Plan")]
        public string TargetPlan { get; set; }


        [Display(Name = "Enumerator")]
        public string Enumerator { get; set; }
         [Display(Name = "First Name")]
        public string BeneFirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string BeneMiddleName { get; set; }
        [Display(Name = "Surname")]
        public string BeneSurname { get; set; }
        [Display(Name = "National ID No. ")]
        public string BeneNationalIdNo { get; set; }
        [Display(Name = "Phone Number ")]
        public string BenePhoneNumber { get; set; }
        [Display(Name = "Full Name")]
        public string BeneFullName => $"{BeneFirstName}  {BeneMiddleName} {BeneSurname}";

        public string BeneSex { get; set; }

        [Display(Name = "Date Of Birth ")]
        public DateTime? BeneDoB { get; set; }
        [Display(Name = "First name")]
        public string CgFirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string CgMiddleName { get; set; }
        [Display(Name = " Surname")]
        public string CgSurname { get; set; }
        [Display(Name = "Full Name ")]
        public string CgFullName => $"{CgFirstName}  {CgMiddleName} {CgSurname}";
        [Display(Name = "National ID No. ")]
        public string CgNationalIdNo { get; set; }
        [Display(Name = " Phone Number")]
        public string CgPhoneNumber { get; set; }
        [Display(Name = "Sex ")]
        public string CgSex { get; set; }
        [Display(Name = "Date Of Birth ")]
        public DateTime CgDoB { get; set; }
        [Display(Name = "Total Household Members ")]
        public int HouseholdMembers { get; set; }
        [Display(Name = "Status ")]
        public string Status { get; set; }
        [Display(Name = "Village")]
        public string Village { get; set; }
        [Display(Name = "Physical Address ")]
        public string PhysicalAddress { get; set; }
        [Display(Name = "Nearest Religious Building ")]
        public string NearestReligiousBuilding { get; set; }
        [Display(Name = " Nearest School ")]
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        [Display(Name = "Sync Enumerator ")]
        public string SyncEnumerator { get; set; }
        [Display(Name = "Sync Date")]
        public DateTime SyncDate { get; set; }

        public string AppBuild { get; set; }
        [Display(Name = "Reject Reason ")]
        public string RejectReason { get; set; }
        [Display(Name = "Reject Date ")]
        public DateTime? RejectDate { get; set; }


    }



}