﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.Targeting
{

    [XmlType(TypeName = "Record", Namespace = "")]
    public class TargetPlanProgrammeOptionsVm
    {
        public byte ProgrammeId { get; set; }
        public int ListingPlanId { get; set; }
        public string TarName { get; set; }
        public bool IsSelected { get; set; }

    }

    [XmlType(TypeName = "Record", Namespace = "")]
    public class ListingPlanProgrammeOptionsVm
    {
        public byte ProgrammeId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }


    public class TargetingSummaryVm
    {
        public GetTargetSummaryVm GetTargetSummaryVm { get; set; }

        public GetRegSummaryVm GetRegSummaryVm { get; set; }
        public int Id { get; set; }=0;

        public TargetPlan TargetPlan { get; set; }

    }
    public class GetRegSummaryVm
    {
        [Display(Name = "Exceptions File")]
        public int? ExceptionsFileId { get; set; }
        [Display(Name = "Validations File")]
        public int? ValidationFileId { get; set; }
        [Display(Name = "Registered Households")]
        public int? RegisteredHouseholds { get; set; }
        [Display(Name = "Accepted Batches")]
        public int? AcceptedBatches { get; set; }
        [Display(Name = "Accepted Households")]
        public int? AcceptedHouseholds { get; set; }
        [Display(Name = "Pending Households")]
        public int? PendingHouseholds { get; set; }
        [Display(Name = "Completed Household")]
        public int? CompletedHouseholds { get; set; }
        [Display(Name = "No one at the Home")]
        public int? NoOneAtHomeHouseholds { get; set; }
        [Display(Name = "Cannot Find Household")]
        public int? CannotFindHouseholds { get; set; }
        [Display(Name = "Exceptions")]
        public int? Exceptions { get; set; }
        [Display(Name = "Validated")]
        public int? Validated { get; set; }
        [Display(Name = "Listed Households")]
        public int? ListedHouseholds { get; set; }
        [Display(Name = "Approved Batches")]
        public int? ApprovedBatches { get; set; }
    }
    public class GetTargetSummaryVm
    {
        [Display(Name = "Exceptions File")]
        public int? ExceptionsFileId { get; set; }
        [Display(Name = "Validation File")]
        public int? ValidationFileId { get; set; }
        [Display(Name = "Total Listed Households")]
        public int? RegisteredHouseholds { get; set; }
        [Display(Name = "Accepted Batches")]
        public int? AcceptedBatches { get; set; }
        [Display(Name = "Accepted Households")]
        public int? AcceptedHouseholds { get; set; }
        [Display(Name = "Pending Households")]
        public int? PendingHouseholds { get; set; }

        [Display(Name = "Caregiver ID No. Exists")]
        public int? CG_IDNoExists { get; set; }
        [Display(Name = "Caregiver Sex Matches")]
        public int? CGSexMatch { get; set; }
        [Display(Name = "Caregiver Year of Birth Matches")]
        public int? CG_DoBYearMatch { get; set; }
        [Display(Name = "Caregiver Date of Birth Matches")]
        public int? CG_DoBMatch { get; set; }
         
        [Display(Name = "Beneficiary ID No. Exists")]
        public int? BeneIDNoExists { get; set; }
        [Display(Name = "Beneficiary Sex Matches")]
        public int? BeneSexMatch { get; set; }
        [Display(Name = "Beneficiary Year of Birth Matches")]
        public int? BeneDoBYearMatch { get; set; }
        [Display(Name = "Beneficiary Date of Birth Matches")]
        public int? BeneDoBMatch { get; set; }



        [Display(Name = "Exception Households")]
        public int? Exceptions { get; set; }
        [Display(Name = "Validated Households")]
        public int? Validated { get; set; }

        [Display(Name = "Rejected Households")]
        public int? RejectedHouseholds { get; set; }

        [Display(Name = "Batches Pending Approval")]
        public int? PendingApproval { get; set; }

        [Display(Name = "Batches Approved")]
        public int ApprovedBatches => AcceptedBatches ?? 0 - PendingApproval ?? 0 ;

        

        [Display(Name = "Total Beneficiaries")]
        public int? TotalBeneficiaries { get; set; }

        [Display(Name = "Total Caregivers")]
        public int? TotalCaregivers { get; set; }




        [Display(Name = "Households Pending Acceotance")]
        public int? PendingAcceptanceHH { get; set; }

        [Display(Name = "Community Validated Households")]
        public int? CValHH { get; set; }
        [Display(Name = "Amended Households")]
        public int? AmendedHH { get; set; }

        [Display(Name = "Confirmed Households")]
        public int? ConfirmedHH { get; set; }

        [Display(Name = "Exited Households")]
        public int? ExitedHH { get; set; }

        [Display(Name = "Total Listed Households")]
        public int? RegHH { get; set; }

         

    }
    public class TargetPlanVm 
    {
        public int Id { get; set; }
        [StringLength(30)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Exercise Name!")]
        [Display(Name = "Exercise Name")]
        public string Name { get; set; }
        [StringLength(128)]
        [DataType(DataType.MultilineText)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 .,! ]*$", ErrorMessage = "Invalid Exercise Description!")]
        [Display(Name = "Exercise Description")]
        public string Description { get; set; }
        [Display(Name = "Exercise Duration")]
        public string DateRange => $"{Start:D} - {End:D}";
        [Required]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date")]
        public DateTime Start { get; set; }
        [Required]

        [Display(Name = "End Date")]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime End { get; set; }
      //  public int? TargetHHs { get; set; }    
        public List<TargetPlanProgrammeOptionsVm> TargetPlanProgrammeOptionsVms { get; set; }
        public List<ListingPlanProgrammeOptionsVm> ListingPlanProgrammeOptions { get; set; }
    }
}
