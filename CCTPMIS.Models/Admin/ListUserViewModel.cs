﻿namespace CCTPMIS.Models.Admin
{
    using System.Collections.Generic;

    using Business.Model;

    public class ListUserViewModel
    {
        public IList<string> Roles { get; set; }

        public ApplicationUser User { get; set; }

        public IList<ApplicationUser> Users { get; set; }
    }
}