﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class SPOutput
    {
        [Display(Name = "File ID")]
        public int? FileCreationId { get; set; }
        [Display(Name = "File ID")]
        public int? FileId { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "Toal Rows Affected")]
        public int? NoOfRows { get; set; }
        [Display(Name = "Output")]
        public int? Output { get; set; }
    }
}