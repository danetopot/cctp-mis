﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class SpIntVm
    {
        [Display(Name = "Total Number Of Rows")]
        public int NoOfRows { get; set; }

        public int? Id { get; set; }


    }
}