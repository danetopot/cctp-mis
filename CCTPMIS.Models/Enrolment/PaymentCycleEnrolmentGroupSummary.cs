﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PaymentCycleEnrolmentGroupSummary
    {
        [Display(Name = "Enrolment Group ")]
        public string EnrolmentGroup { get; set; }
        [Display(Name = "Enrolment Group")]
        public int EnrolmentGroupId { get; set; }
        [Display(Name = "Entitlement Amount ")]
        public decimal EntitlementAmount { get; set; }
    }
}