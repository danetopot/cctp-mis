﻿using System.Collections.Generic;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.Enrolment
{
    using System.ComponentModel.DataAnnotations;

    public class BankEnrolmentProgress
    {
        [Display(Name = "Accounts Opened")]
        public int? AccountOpenedHhs { get; set; }

        [Display(Name = "Carded Households")]
        public int? CardedHhs { get; set; }
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Pending Households")]
        public int? PendingHhs => this.AccountOpenedHhs - this.CardedHhs;

        [Display(Name = "PSP")]
        public string Psp { get; set; }
    }

    public class EnrolmentFileViewModel
    {
        public List<HouseholdEnrolmentPlan> Plans { get; set; }
        public  FileCreation File { get; set; }

    }
}