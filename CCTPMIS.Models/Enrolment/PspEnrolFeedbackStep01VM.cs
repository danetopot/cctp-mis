﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class PspEnrolFeedbackStep01VM
    {
        [Display(Name = "Beneficiary Account  ID")]
        public int? BeneAccountId { get; set; }
        [Display(Name = "Payment Card ID")]
        public int? PaymentCardId { get; set; }
        [Display(Name = "Primary Recipient ID")]
        public int? PriReciId { get; set; }
        [Display(Name = "Secondary Recipient ID")]
        public int? SecReciId { get; set; }
    }
}