﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using PagedList;

namespace CCTPMIS.Models.Enrolment
{
    public class EnrolmentListViewModel
    {
        [DisplayName("Programme")]
        public int? ProgrammeId { get; set; }

        [DisplayName("Enrolment Plan")]
        public int? HhEnrolmentPlanId { get; set; }

        [DisplayName("Report Type")]
        public int? ReportTypeId { get; set; }

        [DisplayName("With Accounts Opened")]
        public int? AccountOpenedId { get; set; }

        [DisplayName("With Card Issued")]
        public int? CardIssuedId { get; set; }

        [DisplayName("With Caregiver")]
        public int? WithCaregiverId { get; set; }

        public EnrolmentBenViewModel Beneficiary { get; set; }
        public IPagedList<EnrolmentBenViewModel> Details { get; set; }
        public IEnumerable<EnrolmentBenViewModel> IDetails { get; set; }
        public ReportSummaryViewModel Summary { get; set; }
        public IPagedList<ReportSummaryViewModel> Summaries { get; set; }
        public IEnumerable<ReportSummaryViewModel> ISummaries { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }

    public class EnrolmentBenViewModel
    {
        public string Programme { get; set; }

        [DisplayName("Enrolment No")]
        public int EnrolmentNo { get; set; }

        [DisplayName("Account Name")]
        public string AccountName { get; set; }

        [DisplayName("Beneficiary ID No")]
        public string BeneficiaryIdNo { get; set; }

        [DisplayName("Beneficiary Name")]
        public string BeneficiaryName { get; set; }

        [DisplayName("Beneficiary Sex")]
        public string BeneficiarySex { get; set; }

        [DisplayName("Beneficiary DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? BeneficiaryDOB { get; set; }

        [DisplayName("Caregiver ID No")]
        public string CaregiverIdNo { get; set; }

        [DisplayName("Caregiver Name")]
        public string CaregiverName { get; set; }

        [DisplayName("Caregiver Sex")]
        public string CaregiverSex { get; set; }

        [DisplayName("Caregiver DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? CaregiverDOB { get; set; }

        public string MobileNo { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }

        [DisplayName("Payment Card No")]
        public string PaymentCardNo { get; set; }

        public string Status { get; set; }

        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
    }

    public class ReportSummaryViewModel
    {
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
    }
}