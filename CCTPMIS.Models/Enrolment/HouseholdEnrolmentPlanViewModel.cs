﻿namespace CCTPMIS.Models.Enrolment
{
    using System.ComponentModel.DataAnnotations;

    using Business.Model;

    public class HouseholdEnrolmentPlanViewModel : CreateModifyFields
    {
        [Display(Name = "Beneficiaries HouseHolds")]
        public int? BeneHhs { get; set; }

        [Display(Name = "Enrolled Beneficiaries")]
        public int EnrolledHH => BeneHhs ?? 0;

        [Display(Name = "Enrolment Beneficiaries")]
        public int? EnrolmentNumbers { get; set; }

        [Display(Name = "Expansion Plan Equal Share Beneficiaries")]
        public int? ExpPlanEqualShare { get; set; }

        [Display(Name = "Expansion Plan Poverty Prioritized Beneficiaries")]
        public int? ExpPlanPovertyPrioritized { get; set; }

        [Display(Name = "Maximum Beneficiaries to Enrol")]
        public int MaxToEnrol
        {
            get
            {
                var expPlanEqualShare = this.ExpPlanEqualShare;
                if (expPlanEqualShare != null)
                    return expPlanEqualShare.Value + this.ExpPlanPovertyPrioritized.Value;
                return 0;
            }
        }

        public int PlanEqualShare => this.ExpPlanEqualShare ?? 0;

        public int PlanPovertyPrioritized => this.ExpPlanPovertyPrioritized ?? 0;

        [Display(Name = "Ready For Enrolment")]
        public int ReadyForEnrolment => this.RegGroupHhs ?? 0;

        [Display(Name = "Beneficiaries to Enroll")]
        public int? RegGroupHhs { get; set; }
    }
}