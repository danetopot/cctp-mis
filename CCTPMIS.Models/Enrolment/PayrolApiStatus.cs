﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace CCTPMIS.Models.Enrolment
{
    public class PayrolApiStatus
    {
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Total Records Affected")]
        public int? NoOfRecs { get; set; }
        [Display(Name = "Status ID")]
        public int StatusId { get; set; }
    }

    public class CustomApiStatus
    {
        [Display(Name = "PSP")]
        public string PSP { get; set; }
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class NotificationEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Action { get; set; }
        public int Id { get; set; }

        public string ActionDate { get; set; }

    }

    public class EmailGlobal : Postal.Email
    {
        public EmailGlobal()
        {
            FromEmail = WebConfigurationManager.AppSettings["EMAIL_FROM"];
            FromName = WebConfigurationManager.AppSettings["EMAIL_FROM_NAME"];
            PortalLink = WebConfigurationManager.AppSettings["ROOT_LINK"];
            PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"];
        }

        public string FirstName { get; set; }

        public string FromEmail { get; set; }

        public string FromName { get; set; }

        public string PortalLink { get; set; }

        public string PortalName { get; set; }

        public string Subject { get; set; }

        public string Title { get; set; }

        public string To { get; set; }
    }
}