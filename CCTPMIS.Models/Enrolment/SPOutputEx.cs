﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Enrolment
{
    public class SPOutputEx
    {
        [Display(Name = "File ID")]
        public int? FileId { get; set; }
        [Display(Name = "Output")]
        public int? Output { get; set; }
    }
}