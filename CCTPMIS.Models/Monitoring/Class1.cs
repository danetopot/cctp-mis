﻿using System;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Monitoring
{
    public class MonitoringReportCreateModel
    {
        public int Id { get; set; }

        public int? MonitoringReportCategoryId { get; set; }
        public int? StatusId { get; set; }
        public string FileName { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }


        [DataType(DataType.MultilineText)]
        [Required]
        [StringLength(256)]
        public string Description { get; set; }


    }
    public class MonitoringListViewModel
    {
        public IPagedList<MonitoringReport> Reports { get; set; }

        public MonitoringReport Report { get; set; }
        public int? Page { get; set; }

        public int? PageSize { get; set; }

        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        [Display(Name = "Category")]
        public int? MonitoringReportCategoryId { get; set; }
    }
    public class BeneMonthlyStatement
    {
        public int BeneAccountMonthlyActivityId { get; set; }
        public int BeneAccountId { get; set; }
        public bool HadUniqueWdl { get; set; }
        public string UniqueWdlTrxNo { get; set; }
        public DateTime? UniqueWdlDate { get; set; }
        public bool HadBeneBiosVerified { get; set; }
        public bool IsDormant { get; set; }
        public DateTime? DormancyDate { get; set; }
        public bool IsDueForClawback { get; set; }
        public decimal? ClawbackAmount { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public short Year { get; set; }
        public string Month { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public DateTime OpenedOn { get; set; }
        public DateTime? DateAdded { get; set; }
        public string PSP { get; set; }
        public string BRANCH { get; set; }
    }
    public class BeneCreditStatement
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }

        public int PaymentCycleId { get; set; }
        public string PaymentCycle { get; set; }
        public byte ProgrammeId { get; set; }
        public int HhId { get; set; }
        public bool WasTrxSuccessful { get; set; }
        public decimal? TrxAmount { get; set; }
        public string TrxNo { get; set; }
        public DateTime? TrxDate { get; set; }
        public string TrxNarration { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal PaymentAmount { get; set; }
        public string FromMonth { get; set; }
        public string ToMonth { get; set; }
        public string FinancialYear { get; set; }
        public string Status { get; set; }
        public string PaymentStage { get; set; }
    }
    public class BenePrepayrollStatement
    {
        public int PaymentCycleId { get; set; }
        public string PaymentCycle { get; set; }
        public byte ProgrammeId { get; set; }
        public string Programme { get; set; }
        public int EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public DateTime BeneDoB { get; set; }
        public string BeneSex { get; set; }
        public string BeneNationalIDNo { get; set; }
        public bool? PriReciCanReceivePayment { get; set; }
        public int? IsInvalidBene { get; set; }
        public int? IsDuplicateBene { get; set; }
        public int? IsIneligibleBene { get; set; }
        public string CGFirstName { get; set; }
        public string CGMiddleName { get; set; }
        public string CGSurname { get; set; }
        public DateTime? CGDoB { get; set; }
        public string CGSex { get; set; }
        public string CGNationalIDNo { get; set; }
        public int? IsInvalidCG { get; set; }
        public int? IsDuplicateCG { get; set; }
        public int? IsIneligibleCG { get; set; }
        public string HhStatus { get; set; }
        public int? IsIneligibleHh { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public int? IsInvalidAccount { get; set; }
        public int? IsDormantAccount { get; set; }
        public string PaymentCardNumber { get; set; }
        public int? IsInvalidPaymentCard { get; set; }
        public string PaymentZone { get; set; }
        public decimal PaymentZoneCommAmt { get; set; }
        public byte ConseAccInactivity { get; set; }
        public decimal EntitlementAmount { get; set; }
        public decimal AdjustmentAmount { get; set; }
        public int? IsSuspiciousAmount { get; set; }
        public DateTime? PrepayrollApvOn { get; set; }
        public string PrepayrollApvdOn => PrepayrollApvOn.ToString()=="1/1/1900 12:00:00 AM"? $"": $"{PrepayrollApvOn.ToString()}";
    }
    public class BenePayrollStatement
    {
        public int PaymentCycleId { get; set; }
        public string PaymentCycle { get; set; }
        public int EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public byte PSPId { get; set; }
        public string PSPCode { get; set; }
        public string PSPName { get; set; }
        public string PSPBranchCode { get; set; }
        public string PSPBranchName { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; }
        public string BeneficiaryIDNo { get; set; }
        public string BeneficiaryContact { get; set; }
        public string CaregiverIDNo { get; set; }
        public string CaregiverContact { get; set; }
        public string SubLocation { get; set; }
        public string Location { get; set; }
        public string Division { get; set; }
        public string District { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
    }


    public class BeneficiarySummary
    {
        public int Payrolls { get; set; }
        public int Payments { get; set; }
        public decimal? PayrolAmount { get; set; }
        public decimal? FailedCreditAmount { get; set; }
        public int? SuccessfulCredits { get; set; }
        public decimal? SuccessfulCreditAmount { get; set; }

        public int Prepayrolls { get; set; }

        public int Changes { get; set; }
        public int OpenChanges { get; set; }

        public int Complaints { get; set; }

        public int OpenComplaints { get; set; }

        public int Members { get; set; }
        public int ActiveMembers { get; set; }

        public int ActivityMonths { get; set; }

        public string PSP { get; set; }

        public string County { get; set; }


    }
}
