﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;


namespace CCTPMIS.Models.CaseManagement
{
    public class ComplaintListViewModel
    {
        [Display(Name = "  County")]
        public int? CountyId { get; set; }
         
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "Category")]

        public int? ComplaintCategoryId { get; set; }

        [DisplayName("Form Number")]
        public int? FormNumber { get; set; }

        [DisplayName("Name of Complainant")]
        public string NameofComplainant { get; set; }

        [DisplayName("Account No")]
        public string AccountNo { get; set; }

        [DisplayName("Sex of complainant")]
        public int? PRGender { get; set; }

        [DisplayName("Complaint Code")]
        public string ComplaintCode { get; set; }

        [DisplayName("Date of Complaint")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? ComplaintDate { get; set; }
        [Display(Name = "Date Closed")]
        public DateTime DateClosed { get; set; }
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Complaint Status")]
        public int? ComplaintStatusId { get; set; }

        [DisplayName("Beneficiary S/No")]
        public int? BeneficiarySNo { get; set; }

        public Complaint Complaint { get; set; }
        public IPagedList<Complaint> Complaints { get; set; }

        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int SLALimit { get; set; }
        public int? Filter { get; set; }

        [DisplayName("Districts")]
        public IList<int> DistrictIds { get; set; }
    }
}
