﻿
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCTPMIS.Models.CaseManagement
{ 
    public class CaseFlowDocForSelection
    {
        public int Id { get; set; }
        [Display(Name = "Selected")]
        public bool IsSelected { get; set; }
        public string Description { get; set; }
    }

    public class CaseFlowVm : CaseFlow
    {
        [Display(Name = "Worflow Documents")]
        public List<CaseFlowDocForSelection> CaseFlowDocsForSelection { get; set; }

        public CaseFlowVm(CaseFlow cFlow) {
            this.CanResolve = cFlow.CanResolve;
            this.CaseFlowDocs = cFlow.CaseFlowDocs;
            this.CaseIssue = cFlow.CaseIssue;
            this.CaseIssueId = cFlow.CaseIssueId;
            this.CreatedBy = cFlow.CreatedBy;
            this.CreatedByUser = cFlow.CreatedByUser;
            this.CreatedOn = cFlow.CreatedOn;
            this.Description = cFlow.Description;
            this.Id = cFlow.Id;
            this.ModifiedBy = cFlow.ModifiedBy;
            this.ModifiedOn = cFlow.ModifiedOn;
            this.Status = cFlow.Status;
            this.StatusId = cFlow.StatusId;
            this.StepName = cFlow.StepName;
            this.StepNo = cFlow.StepNo;
            this.UserGroup = cFlow.UserGroup;
            this.UserGroupId = cFlow.UserGroupId;
            this.CaseFlowDocsForSelection = null;
        }

        public CaseFlowVm(CaseFlow cFlow, List<CaseFlowDocForSelection> docs)
        {
            this.CanResolve = cFlow.CanResolve;
            this.CaseFlowDocs = cFlow.CaseFlowDocs;
            this.CaseIssue = cFlow.CaseIssue;
            this.CaseIssueId = cFlow.CaseIssueId;
            this.CreatedBy = cFlow.CreatedBy;
            this.CreatedByUser = cFlow.CreatedByUser;
            this.CreatedOn = cFlow.CreatedOn;
            this.Description = cFlow.Description;
            this.Id = cFlow.Id;
            this.ModifiedBy = cFlow.ModifiedBy;
            this.ModifiedOn = cFlow.ModifiedOn;
            this.Status = cFlow.Status;
            this.StatusId = cFlow.StatusId;
            this.StepName = cFlow.StepName;
            this.StepNo = cFlow.StepNo;
            this.UserGroup = cFlow.UserGroup;
            this.UserGroupId = cFlow.UserGroupId;
            this.CaseFlowDocsForSelection = docs;
        }

        public CaseFlowVm(List<CaseFlowDocForSelection> docs) { CaseFlowDocsForSelection = docs; }
        public CaseFlowVm() {}
    }

    public class CaseIssueVm : CaseIssue
    {
        [Display(Name = "Worflow Documents")]
        [JsonIgnore]
        public List<CaseFlowDocForSelection> CaseFlowDocsForSelection { get; set; }
        public CaseIssueVm(List<CaseFlowDocForSelection> docs) { CaseFlowDocsForSelection = docs; }
        public CaseIssueVm() {}
    }
}
