﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.CaseManagement
{
    public class ComplaintActionViewModel
    {
        public int Id { get; set; }
        public int? HhEnrolmentId { get; set; }

        [DisplayName("Upload Document")]

        public string Document { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        public Complaint Complaint { get; set; }
        public Change Change { get; set; }
    }


    public class ComplaintViewModel
    {

        [DisplayName("Reported By Name"),]
        public string ReportedByName { get; set; }
        [DisplayName("Reported By National ID No")]
        public string ReportedByIdNo { get; set; }
        [DisplayName("Reported By Phone Number")]
        public string ReportedByPhone { get; set; }
        [DisplayName("Reported By Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }

        [DisplayName("Reported By:"), Required]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }

        public int? Id { get; set; }
        public int? HhEnrolmentId { get; set; }
        public byte ProgrammeId { get; set; }

        [DisplayName("Category")]
        public int ComplaintCategoryId { get; set; }

        [DisplayName("Complaint Type"), Required]
        public int ComplaintTypeId { get; set; }

        [DisplayName("Source"), Required]
        public int SourceId { get; set; }

        public SystemCodeDetail Source { get; set; }



        public Complaint Complaint { get; set; }
        public CaseCategory ComplaintType { get; set; }

        [DisplayName("Document Type")]
        public int? DocumentTypeId { get; set; }

        public SystemCodeDetail DocumentType { get; set; }
        public string OtherDocumentType { get; set; }

        [DisplayName("Serial No")]
        public int SerialNo { get; set; }

        [DisplayName("Date of Received"), Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? ReceivedOn { get; set; }

        [DisplayName("Captured By")]
        public string EnteredByUserId { get; set; }

        [DisplayName("Complaint Status")]
        public int ComplaintStatusId { get; set; }

        [DisplayName("Complaint Status")]
        public string ComplaintStatus { get; set; }
         
        public SystemCodeDetail CgSex { get; set; }

        [DisplayName("Upload Document")]
        public string Document { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        public ICollection<CaseCategory> ComplaintTypes { get; set; }
        public ICollection<SystemCodeDetail> DocumentTypes { get; set; }

        public ICollection<ComplaintNote> ComplaintNotes { get; set; }
        public ICollection<ComplaintDocument> ComplaintDocuments { get; set; }
        public ComplaintNote ComplaintNote { get; set; }
        public ApplicationUser EnteredBy { get; set; }

        [DisplayName("Received By"), Required]
        public string ReceivedBy { get; set; }

        [Required]
        public string Designation { get; set; }

        public HouseholdEnrolment HouseholdEnrolment { get; set; }
        public GetBeneInfoVm GetBeneInfoVm { get; set; }


        [DisplayName("County"), Required]
        public int CountyId { get; set; }

        [DisplayName("Sub County"), Required]
        public int ConstituencyId { get; set; }

        



    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class FileTypeAttribute : ValidationAttribute, IClientValidatable
    {
        private const string _DefaultErrorMessage = "Only the following file types are allowed: {0}";
        private IEnumerable<string> _ValidTypes { get; set; }

        public FileTypeAttribute(string validTypes)
        {
            _ValidTypes = validTypes.Split(',').Select(s => s.Trim().ToLower());
            ErrorMessage = string.Format(_DefaultErrorMessage, string.Join(" or ", _ValidTypes));
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IEnumerable<HttpPostedFileBase> files = value as IEnumerable<HttpPostedFileBase>;
            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null && !_ValidTypes.Any(e => file.FileName.EndsWith(e)))
                    {
                        return new ValidationResult(ErrorMessageString);
                    }
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "filetype",
                ErrorMessage = ErrorMessageString
            };
            rule.ValidationParameters.Add("validtypes", string.Join(",", _ValidTypes));
            yield return rule;
        }
    }

    public class ActionViewModel
    {
        public int Id { get; set; }

        [DisplayName("Notes"), DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        [DisplayName("Notes"), DataType(DataType.MultilineText)]
        public string NotesFinal { get; set; }

        public string Action { get; set; }

        [DisplayName("Category")]
        public int ComplaintCategoryId { get; set; }
    }
}