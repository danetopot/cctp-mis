﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.CaseManagement
{
    public class EFCListViewModel
    {
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }

        [Display(Name = "County")]
        public int? CountyId { get; set; }
        
        public EFC EFC { get; set; }
        public IPagedList<EFC> EFCs { get; set; }


        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int SLALimit { get; set; }
        public int? Filter { get; set; }
        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        [Display(Name = "Type")]
        public int? TypeId { get; set; }
          

    }

    public class EFCViewModel
    {
        [DisplayName("Reported By Name"),]
        public string ReportedByName { get; set; }
        [DisplayName("Reported By National ID No")]
        public string ReportedByIdNo { get; set; }
        [DisplayName("Reported By Phone Number")]
        public string ReportedByPhone { get; set; }
        [DisplayName("Reported By Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }

        [DisplayName("Reported By:"), Required]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }
        public int? Id { get; set; }

        [DisplayName("Source"), Required]
        public int SourceId { get; set; }

        public SystemCodeDetail Source { get; set; }



        [DisplayName("EFC Type"), Required]
        public int EFCTypeId { get; set; }

        public EFC EFC { get; set; }
        public SystemCodeDetail EFCType { get; set; }
       

        [DisplayName("Serial No")]
        public int? SerialNo { get; set; }

        [DisplayName("Date of Received"), Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? ReceivedOn { get; set; }
         
        [DisplayName("  Status")]
        public int  StatusId { get; set; }

        [DisplayName("EFC Status")]
        public string EFCStatus { get; set; }

        public SystemCodeDetail CgSex { get; set; }

        [DisplayName("Upload Document")]
        public string Document { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        public ICollection<SystemCodeDetail> EFCTypes { get; set; }
        
        public ICollection<EFCNote> EFCNotes { get; set; }
        public ICollection<EFCDocument> EFCDocuments { get; set; }
        public EFCNote EFCNote { get; set; }
        

        [DisplayName("Received By"), Required]
        public string ReceivedBy { get; set; }

        [Required]
        public string Designation { get; set; }



        [DisplayName("Reporting On : "), Required]
        public string Regards { get; set; }


        [DisplayName("County"), Required]

        public int CountyId { get; set; }

        [DisplayName("Sub County"), Required]
        public int ConstituencyId { get; set; }

    }


    public class EFCActionViewModel
    {
        public int Id { get; set; }
        public int? HhEnrolmentId { get; set; }

        [DisplayName("Upload Document")]

        public string Document { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        public EFC EFC { get; set; }
     }
}