﻿using System;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;
using System.Web;
using System.Xml.Serialization;

namespace CCTPMIS.Models.CaseManagement
{
    //public class CaseReportVm : CaseReport
    //{
    //    [Display(Name = "Timeline")]
    //    public int? Timeline { get; set; }

    //    [DataType(DataType.Upload)]
    //    public HttpPostedFileBase Upload { get; set; }

    //    [Display(Name = "Supp. Doc.")]
    //    public string UploadFile { get; set; }
    //}

    public class UpdateCaseIssueVm
    {
        public int CaseIssueId { get; set; }
        public string CaseIssueCode { get; set; }
        public int UpdateEffectId { get; set; }
        public string UpdateEffectCode { get; set; }
    }

    public class SupportingDocVm
    {
        public int NoOfRows { get; set; }
        public string SupportingDoc { get; set; }

    }





    public class CaseReportHistoryViewModel
    {
        
        [Display(Name = "Case Report")]
        public int Id { get; set; }

        [Display(Name = "Relationship")]
        public int? RelationshipId { get; set; }

         

        [Display(Name = "Person")]
        public int? PersonId { get; set; }

         

        [Display(Name = "First Name ")]
        [StringLength(50),]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string MiddleName { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50),]
        public string Surname { get; set; }

        [Display(Name = "Sex")]
        public int? SexId { get; set; }

      

        [StringLength(50)]
        [Display(Name = "Birth Cert.")]
        public string BirthCertNo { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No.")]
        public string NationalIdNo { get; set; }

        [Display(Name = "Mobile #1 ")]
        [StringLength(20)]
        public string MobileNo1 { get; set; }

        [Display(Name = "Mobile #2 ")]
        [StringLength(20)]
        public string MobileNo2 { get; set; }

      

        [Display(Name = "SubLocation")]
        public int? SubLocationId { get; set; }

        [Display(Name = "Relationship")]
        public int? UpdatedRelationshipId { get; set; }

       

        [Display(Name = "Person")]
        public int? UpdatedPersonId { get; set; }

         

        [Display(Name = "First Name ")]
        [StringLength(50)]
        public string UpdatedFirstName { get; set; }

        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string UpdatedMiddleName { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50)]
        public string UpdatedSurname { get; set; }

        [Display(Name = "Sex")]
        public int? UpdatedSexId { get; set; }

         

        [StringLength(50)]
        [Display(Name = "Birth Cert.")]
        public string UpdatedBirthCertNo { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No. ")]
        public string UpdatedNationalIdNo { get; set; }

        [Display(Name = "Mobile #1")]
        [StringLength(20)]
        public string UpdatedMobileNo1 { get; set; }

        [Display(Name = "Mobile #2")]
        [StringLength(20)]
        public string UpdatedMobileNo2 { get; set; }

       

        [Display(Name = "SubLocation")]
        public int? UpdatedSubLocationId { get; set; }
    }
    //[XmlType(TypeName = "Record", Namespace = "")]
    //public class CaseReportViewModel :CaseReportHistory
    //{
    //    [Display(Name = "Timeline")]
    //    public int? Timeline { get; set; }

    //    [DataType(DataType.Upload)]
    //    public HttpPostedFileBase Upload { get; set; }

    //    [Display(Name = "Supp. Doc.")]
    //    public string UploadFile { get; set; }


    //    [Display(Name = "ID")]

    //    public int Id { get; set; }

    //    [StringLength(20)]
    //    [Display(Name = "Serial No.")]
    //    [RegularExpression("^[A-Za-z0-9-]*$", ErrorMessage = "Invalid!")]
    //    public string SerialNo { get; set; }


    //    [Display(Name = "Programme")]
    //    public byte ProgrammeId { get; set; }
    //    [Display(Name = "Prefix")]
    //    public int BeneProgNoPrefix { get; set; }

    //    [Display(Name = "Prog. No.")]
    //    public int? ProgrammeNo { get; set; }

    //    [Display(Name = "Relation")]
    //    public int? MemberRoleId { get; set; }

        

    //    [Display(Name = "Person")]
    //    public int? PersonId { get; set; }

       
    //    [StringLength(100)]
    //    [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Name")]
    //    [Display(Name = "Names")]
    //    public string Names { get; set; }

    //    [Display(Name = "Sex")]
    //    public int? SexId { get; set; }



    //    [StringLength(30)]
    //    [Display(Name = "ID No.")]
    //    [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid!")]
    //    public string IdNo { get; set; }

    //    [StringLength(20)]
    //    [Display(Name = "Mobile No.")]
    //    [RegularExpression("^[0-9+]*$", ErrorMessage = "Invalid!")]
    //    public string MobileNo { get; set; }



    //    [Display(Name = "Case Type")]
    //    public int? CaseIssueId { get; set; }

    //    [StringLength(4000)]
    //    [Required]
    //    [Display(Name = "Notes")]
    //    [RegularExpression("^[A-Za-z0-9- ]*$", ErrorMessage = "Invalid!")]
    //    [DataType(DataType.MultilineText)]
    //    public string Notes { get; set; }



    //    [Display(Name = "County")]
    //    public int? CountyId { get; set; }



    //    [Display(Name = "Constituency")]
    //    public int? ConstituencyId { get; set; }



    //    [Display(Name = "SubLocation")]
    //    public int? SubLocationId { get; set; }

    //    [StringLength(20)]
    //    [Display(Name = "Received By")]
    //    [RegularExpression("^[A-Za-z- ]*$", ErrorMessage = "Invalid!")]
    //    public string ReceivedBy { get; set; }

    //    [StringLength(20)]
    //    [Display(Name = "Designation")]
    //    [RegularExpression("^[A-Za-z0-9-]*$", ErrorMessage = "Invalid!")]
    //    public string ReceivedByDesig { get; set; }


    //    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    //    [Display(Name = "Received On")]
    //    public DateTime ReceivedOn { get; set; }



    //    [Display(Name = "File Creation")]
    //    public int? FileCreationId { get; set; }

    //    [Display(Name = "Latest Action")]
    //    public int CaseActionId { get; set; }



    //    [Display(Name = "Next Workflow Stage")]
    //    public int CaseFlowId { get; set; }


    //    [Display(Name = "Status")]
    //    public int StatusId { get; set; }






    //}

}
