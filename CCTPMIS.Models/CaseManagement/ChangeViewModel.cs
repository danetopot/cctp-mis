﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Legacy;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Monitoring;
using PagedList;

namespace CCTPMIS.Models.CaseManagement
{

    public class CaseFilterViewModel
    {
        [DisplayName("Enrolment No.")]
        public int? HouseholdEnrolmentId { get; set; }



        [DisplayName("Programme No.")]
        [StringLength(7), MaxLength(7), MinLength(7)]
        public string ProgrammeNo { get; set; }



        public int? CountyId { get; set; }
        public int? ConstituencyId { get; set; }
        public int? LocationId { get; set; }
        public int? SubLocationId { get; set; }

        public IEnumerable<HouseholdEnrolment> HouseholdEnrolments { get; set; }
        public IEnumerable<Constituency> SubCounties { get; set; }

        [DisplayName("List Type")]
        public int? ListTypeId { get; set; }

        [DisplayName("Status")]
        public int? StatusId { get; set; }

        [DisplayName("Status")]
        public int? ImportStatusId { get; set; }

        [DisplayName("National ID No.")]
        public string NationalId { get; set; }

        [DisplayName("Any Name")]
        public string Name { get; set; }

        [DisplayName("Batch No")]
        public int? BatchNo { get; set; }

        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int Count { get; set; }
    }




    public class BeneStatementModel
    {
        public int Id { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }

        public Household Household { get; set; }
        public BeneficiarySummary Summary { get; set; }
        public List<BeneficiaryAccount> BeneficiaryAccount { get; set; }
        public List<HouseholdMember> HouseholdMember { get; set; }
        public List<Change> Changes { get; set; }
        public List<Complaint> Complaints { get; set; }
        public List<GetBeneInfoVm> GetBeneInfoVm { get; set; }
        public List<HouseholdEnrolment> HouseholdEnrolments { get; set; }
        public List<HouseholdMember> HouseholdMembers { get; set; }
        public List<BenePayrollStatement> BenePayrollStatements { get; set; }
        public List<BenePrepayrollStatement> BenePrepayrollStatements { get; set; }
        public List<BeneCreditStatement> BeneCreditStatements { get; set; }
        public List<BeneMonthlyStatement> BeneMonthlyStatements { get; set; }


    }

    public class LegacyBeneStatementFilterViewModel
    {
        [DisplayName("Legacy Programme No.")]

        public string ProgrammeNo { get; set; }
        [DisplayName("National ID No.")]
        public string NationalId { get; set; }

        

        public IEnumerable<HhRegistration> Households { get; set; }

        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int Count { get; set; }
    }

    public class BeneStatementFilterViewModel
    {
        [DisplayName("Programme No.")]
        [StringLength(7), MaxLength(7), MinLength(7)]
        public string ProgrammeNo { get; set; }

        [DisplayName("Enrolment No.")]
        public int? EnrolmentNo { get; set; }

        [DisplayName("Account No.")]
        public string AccountNo { get; set; }
        public int? CountyId { get; set; }
        public int? ConstituencyId { get; set; }
        public int? LocationId { get; set; }
        public int? SubLocationId { get; set; }
        public IEnumerable<Household> Households { get; set; }
        public IEnumerable<Constituency> SubCounties { get; set; }



        [DisplayName("National ID No.")]
        public string NationalId { get; set; }

        [DisplayName("Any Name")]
        public string Name { get; set; }

        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int Count { get; set; }
    }

    public class CaseFilterViewModelDisplayViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string NationalId { get; set; }

        public DateTime DOB { get; set; }
        public string Gender { get; set; }

        public string District { get; set; }
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Parish { get; set; }
        public string Village { get; set; }
        public string Status { get; set; }
        public string BranchCode { get; set; }
        public string Reason { get; set; }
        public DateTime? EnrolmentDate { get; set; }
        public string AccountNo { get; set; }

        public string FullName => FirstName + " " + (string.IsNullOrEmpty(MiddleName) ? "" : MiddleName.Trim() + " ") + LastName;

        public int Age
        {
            get
            {
                DateTime now = DateTime.Today;
                int age = now.Year - DOB.Year;
                if (DOB > now.AddYears(-age))
                {
                    age--;
                }

                return age;
            }
        }
    }

    public class ChangeViewModel
    {

        [DisplayName("Reported By Name"),]
        public string ReportedByName { get; set; }
        [DisplayName("Reported By National ID No")]
        public string ReportedByIdNo { get; set; }
        [DisplayName("Reported By Phone Number")]
        public string ReportedByPhone { get; set; }
        [DisplayName("Reported By Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }

        [DisplayName("Reported By:"), Required]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }
        public int Id { get; set; }
        public int HhEnrolmentId { get; set; }
        public HouseholdEnrolment HouseholdEnrolment { get; set; }
        public GetBeneInfoVm GetBeneInfoVm { get; set; }
        public Change Change { get; set; }

        public CaseCategory ChangeType { get; set; }
        public string BeneficiaryName { get; set; }

        [Required]
        [DisplayName("Attach Supporting Document")]
        public string Document { get; set; }


        public DateTime? ChangeDate { get; set; }

        [DisplayName("Type of Change")]
        public int? ChangeTypeId { get; set; }


        [DisplayName("Change Level")]
        public int? ChangeLevelId { get; set; }
        [DisplayName("Programme Names")]
        public byte ProgrammeId { get; set; }


        [DataType(DataType.MultilineText), Required]
        public string Notes { get; set; }

        [DisplayName("Notification Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? NotificationDate { get; set; }

        [DisplayName("Death Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DeathDate { get; set; }
        [Required]
        [DisplayName("Received On")]
        public DateTime ReceivedOn { get; set; }
        [DisplayName("Serial No")]
        [Required]
        public int SerialNo { get; set; }
        public int? CreatedById { get; set; }
        public ApplicationUser CreatedBy { get; set; }
        public DateTime? DateActioned { get; set; }
        public int? ActionedById { get; set; }
        public ApplicationUser ActionedBy { get; set; }

        public SystemCodeDetail Status { get; set; }
        public int? StatusId { get; set; }



        [DisplayName("Sub County")]
        public int? SubCountyId { get; set; }

        [DisplayName("County")]
        public int? CountyId { get; set; }

        [DisplayName("Sub County")]
        public int? ConstituencyId { get; set; }

        public ICollection<County> Counties { get; set; }
        public ICollection<Constituency> SubCounties { get; set; }
        public ICollection<Location> Locations { get; set; }
        public ICollection<SubLocation> SubLocations { get; set; }
        public ICollection<ChangeNote> ChangeNotes { get; set; }
        public string Action { get; set; }

        [DisplayName("Change Source")]
        public int? ChangeSourceId { get; set; }

        public SystemCodeDetail ChangeSource { get; set; }
        public int Count { get; set; }
        public int? PageNo { get; set; }

        [Required]
        [DisplayName("Received By")]
        public string ReceivedBy { get; set; }
        [Required]
        public string Designation { get; set; }


        [DisplayName("Beneficiary Name")]
        public string BeneName { get; set; }

        [DisplayName("Beneficiary I.D. No")]
        public string BeneIdNo { get; set; }

        [DisplayName("Beneficiary Phone")]
        public string BenePhone { get; set; }

        [DisplayName("Beneficiary Sex")]
        public int? BeneSexId { get; set; }

        public SystemCodeDetail BenSex { get; set; }

        [DisplayName("Caregiver Name")]
        public string CgName { get; set; }

        [DisplayName("Caregiver I.D. No")]
        public string CgIdNo { get; set; }

        [DisplayName("Caregiver Phone")]
        public string CgPhone { get; set; }

        [DisplayName("Caregiver Sex")]
        public int? CgSexId { get; set; }

        public SystemCodeDetail CgSex { get; set; }


        [Display(Name = "SubLocation")]
        public int? SubLocationId { get; set; }

        public SubLocation SubLocation { get; set; }

        [Display(Name = "Relationship")]
        public int? UpdatedRelationshipId { get; set; }

        [Display(Name = "Member Role")]
        public int? UpdatedRoleId { get; set; }

        public SystemCodeDetail UpdatedRelationship { get; set; }

        [Display(Name = "Person")]
        public int? UpdatedPersonId { get; set; }

        public Person UpdatedPerson { get; set; }

        [Display(Name = "First Name ")]
        [StringLength(50)]
        public string UpdatedFirstName { get; set; }

        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string UpdatedMiddleName { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50)]
        public string UpdatedSurname { get; set; }

        [Display(Name = "Sex")]
        public int? UpdatedSexId { get; set; }

        public SystemCodeDetail UpdatedSex { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime? UpdatedDob { get; set; }

        [StringLength(50)]
        [Display(Name = "Birth Cert.")]
        public string UpdatedBirthCertNo { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No. ")]
        public string UpdatedNationalIdNo { get; set; }

        [Display(Name = "Mobile #1")]
        [StringLength(20)]
        public string UpdatedMobileNo1 { get; set; }

        [Display(Name = "Mobile #2")]
        [StringLength(20)]
        public string UpdatedMobileNo2 { get; set; }

        [Display(Name = "SubLocation")]
        public int? UpdatedSubLocationId { get; set; }
        public SubLocation UpdatedSubLocation { get; set; }

        [Display(Name = "Household Member To Exclude")]
        public int? PersonId { get; set; }

        public int? IsIPRSed { get; set; }

    }

    public class ChangeListViewModel
    {
        public int? Id { get; set; }
        public IPagedList<Change> Changes { get; set; }

        [DisplayName("Status")]
        public int? StatusId { get; set; }

        public int? ChangeStatusId { get; set; }

        [DisplayName("Status")]
        public int? ImportStatusId { get; set; }

        [DisplayName("National ID")]
        public string NationalId { get; set; }

        [DisplayName("Enrolment No.")]
        public int? HouseholdEnrolmentId { get; set; }

        [DisplayName("Programme No.")]
        public int? ProgrammeNo { get; set; }

        [DisplayName("Any Name")]
        public string Name { get; set; }

        [DisplayName("Type of Change")]
        public int? ChangeTypeId { get; set; }

        public int? Page { get; set; }
        public int? PageSize { get; set; }

        public int UserId { get; set; }
        public ICollection<ApplicationUser> Users { get; set; }
        public IList<int> UserIds { get; set; }

        public ICollection<Constituency> Constituencies { get; set; }


        public ICollection<County> Counties { get; set; }
        public ICollection<Location> Locations { get; set; }

        public ICollection<HouseholdEnrolment> HouseholdEnrolments { get; set; }

        public ICollection<SubLocation> SubLocations { get; set; }
        public IList<int> SubLocationIds { get; set; }
        public int Count { get; set; }

        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        [Display(Name = "Sub Location")]
        public int? SubLocationId { get; set; }

        public int? IsIPRSed { get; set; }


    }
}
