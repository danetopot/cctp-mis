﻿using System.ComponentModel.DataAnnotations;
using System;

namespace CCTPMIS.Models.CaseManagement
{

    public class IprsCacheVm
    {

        public long? Serial_Number { get; set; }
        public string First_Name { get; set; }
        public string Surname { get; set; }
        public string Middle_Name { get; set; }
        [Key]
        public string ID_Number { get; set; }
        public string Gender { get; set; }
        public string Date_of_Birth { get; set; }
        public string Date_of_Issue { get; set; }
        public string Place_of_Birth { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public DateTime? DateCached { get; set; }
    }

    public class ChangesSendForApprovalViewModel
    {
        public int Count { get; set; }
        public int[] Ids { get; set; }
    }
    public class CaseActionVm
    {
        public int Id { get; set; }
        [Display(Name = "Serial No.")]
        public string SerialNo { get; set; }

        public string ProgrammeCode { get; set; }
        public byte ProgrammeId { get; set; }

        public string BeneProgNo { get; set; }

        [Display(Name = "Names")]
        public string Names { get; set; }
        
        [Display(Name = "ID No.")]
        public string IdNo { get; set; }

        public string CaseIssueCode { get; set; }
        public int CaseIssueId { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "County")]
        public string CountyName { get; set; }
        public int CountyId { get; set; }

        [Display(Name = "Constituency")]
        public string ConstituencyName { get; set; }
        public int ConstituencyId { get; set; }

        [Display(Name = "SubLocation")]
        public string SubLocationName { get; set; }
        public int SubLocationId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Received On")]
        public DateTime ReceivedOn { get; set; }

        [Display(Name = "Status")]
        public int StatusName { get; set; }
        public int StatusId { get; set; }

        [Display(Name = "Timeline")]
        public int? Timeline { get; set; }
    }


}
