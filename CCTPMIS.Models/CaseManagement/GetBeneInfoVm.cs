﻿using System;

namespace CCTPMIS.Models.CaseManagement
{


    public class GetBeneInfoVm
    {
        public byte ProgrammeId { get; set; }
        public string ProgrammeCode { get; set; }
        public string ProgrammeName { get; set; }
        public int EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public int PriReciPersonId { get; set; }
        public string PriReciFirstName { get; set; }
        public string PriReciMiddleName { get; set; }
        public string PriReciSurname { get; set; }
        public int PriReciSexId { get; set; }
        public string PriReciSex { get; set; }
        public string PriReciNationalIdNo { get; set; }
        public string PriReciBirthCertNo { get; set; }
        public string PriReciMobileNo1 { get; set; }
        public string PriReciMobileNo2 { get; set; }
        public int? SecReciPersonId { get; set; }
        public string SecReciFirstName { get; set; }
        public string SecReciMiddleName { get; set; }
        public string SecReciSurname { get; set; }
        public int? SecReciSexId { get; set; }
        public string SecReciSex { get; set; }
        public string SecReciNationalIdNo { get; set; }
        public string SecReciBirthCertNo { get; set; }
        public string SecReciMobileNo1 { get; set; }
        public string SecReciMobileNo2 { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string PSPBranch { get; set; }
        public string PSP { get; set; }
        public DateTime? AccountOpenedOn { get; set; }
        public string AccountStatus { get; set; }
        public DateTime? AccountAddedOn { get; set; }
        public string PaymentCardNo { get; set; }
        public int SubLocationId { get; set; }
        public string SubLocationName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int CountyId { get; set; }
        public string CountyName { get; set; }
        public int ConstituencyId { get; set; }
        public string ConstituencyName { get; set; }

        public string PriReciName => $"{PriReciFirstName} {PriReciMiddleName} {PriReciSurname}";
        public string SecReciName => $"{SecReciFirstName} {SecReciMiddleName} {SecReciSurname}";

    }
    public class GetBeneInfoVmv3
    {
        public byte ProgrammeId { get; set; }
        public int? ProgrammeCode { get; set; }
        public string ProgrammeName { get; set; }
        public int PriReciPersonId { get; set; }
        public string PriReciFirstName { get; set; }
        public string ProgrammeNo { get; set; }
        public int? EnrolmentNo { get; set; }
        public string PriReciMiddleName { get; set; }
        public string PriReciSurname { get; set; }
        public string BeneName => $"{PriReciFirstName} {PriReciMiddleName} {PriReciSurname}";
        public string CgName => $"{SecReciFirstName} {SecReciMiddleName} {SecReciSurname}";

        public DateTime? EnrolmentDate { get; set; }
        public int PriReciSexId { get; set; }
        public string PriReciSex { get; set; }
        public string PriReciNationalIdNo { get; set; }
        public string PriReciBirthCertNo { get; set; }
        public string PriReciMobileNo1 { get; set; }
        public string PriReciMobileNo2 { get; set; }
        public int? SecReciPersonId { get; set; }
        public string SecReciFirstName { get; set; }
        public string SecReciMiddleName { get; set; }
        public string SecReciSurname { get; set; }
        public int? SecReciSexId { get; set; }
        public string SecReciSex { get; set; }
        public string SecReciNationalIdNo { get; set; }
        public string SecReciBirthCertNo { get; set; }
        public string SecReciMobileNo1 { get; set; }
        public string SecReciMobileNo2 { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string PSPBranch { get; set; }
        public string PSP { get; set; }
        public DateTime? AccountOpenedOn { get; set; }
        public string PaymentCardNo { get; set; }
        public int SubLocationId { get; set; }
        public string SubLocationName { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int CountyId { get; set; }
        public string CountyName { get; set; }
        public int ConstituencyId { get; set; }
        public string ConstituencyName { get; set; }
    }
}