﻿namespace CCTPMIS.Models.AuditTrail
{
     
    public class AuditTrailVm
    {
        public string UserId { get; set; }
        public bool WasSuccessful { get; set; }
        public int? Key1 { get; set; }
        public int? Key2 { get; set; }
        public int? Key3 { get; set; }
        public int? Key4 { get; set; }
        public int? Key5 { get; set; }
        public string Record { get; set; }
        public string ModuleRightCode { get; set; }
        public string Description { get; set; } = "";

        
        public string IMEI { get; set; } = "";
        public string TableName { get; set; } = "";





    }
}
