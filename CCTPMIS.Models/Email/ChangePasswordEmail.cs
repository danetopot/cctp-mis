﻿namespace CCTPMIS.Models.Email
{
    public class ChangePasswordEmail : EmailGlobal
    {
        public string Password { get; set; }
    }


    public class ChangePinEmail : EmailGlobal
    {
        public string Pin { get; set; }
    }

}