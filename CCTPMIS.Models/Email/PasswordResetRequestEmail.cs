﻿namespace CCTPMIS.Models.Email
{
    public class PasswordResetRequestEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }
    }
}