﻿namespace CCTPMIS.Models.Email
{
    using System;

    /// <summary>
    ///
    /// </summary>
    public class PspPayrolResendFileEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Description { get; set; }

        public string FileChecksum { get; set; }

        public string FileName { get; set; }

        public string Password { get; set; }

        public string PaymentCycle { get; set; }

        public string PspName { get; set; }
    }

    public class PspPayrolFileEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Description { get; set; }

        public string FileChecksum { get; set; }

        public string FileName { get; set; }

        public string Password { get; set; }

        public string PaymentCycle { get; set; }

        public string PspName { get; set; }
    }
}