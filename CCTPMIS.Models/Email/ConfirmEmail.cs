﻿using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.Email
{
    public class ConfirmEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }

        public string Password { get; set; }
    }

    public class ActivateUserEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }

        public string Password { get; set; }
    }
    public class UnlockUserEmail : EmailGlobal
    {
        public string ConfirmationLink { get; set; }

        public string Password { get; set; }
    }


    
    public class ConfirmPinEmail : EmailGlobal
    { 
        public string Pin { get; set; }
        public string NationalIdNo { get; set; }

    }


    public class ComplaintsEditEmail : EmailGlobal
    {
        public string Name { get; set; }
        public Complaint Complaint { get; set; }
        public string Action { get; set; }
        public ApplicationUser User { get; set; }
        public ComplaintNote ComplaintNote { get; set; }
    }

    public class CaseManagementEmail : EmailGlobal
    {
        public string Action { get; set; }
        public string Item { get; set; }
        public string Narration { get; set; }
        public ApplicationUser User { get; set; }
        public int Count { get; set; }
        public Change Change { get; set; }
        public string Name { get; set; }
    }
}