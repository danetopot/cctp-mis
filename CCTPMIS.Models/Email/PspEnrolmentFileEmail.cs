﻿namespace CCTPMIS.Models.Email
{
    using System;

    public class PspEnrolmentFileEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Description { get; set; }

        public string FileChecksum { get; set; }

        public string FileName { get; set; }

        public string Password { get; set; }

        public string PspName { get; set; }
    }


    public class ComplaintNotificationEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Action { get; set; }

        public string ActionDate { get; set; }

        public int Id { get; set; }
    }

    
    public class ChangeNotificationEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Action { get; set; }
        public int Id { get; set; }

        public string ActionDate { get; set; }

    }

    public class EFCNotificationEmail : EmailGlobal
    {
        public DateTime DateCreated { get; set; }

        public string Action { get; set; }
        public int Id { get; set; }

        public string ActionDate { get; set; }

    }
}