﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Recertification
{

    public class HhREjectVm
    {
        public int Id { get; set; }

        [Display(Name = "Reason for Rejecting the Household")]
        [DataType(DataType.MultilineText)]
        public string Reason { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        public int? TargetPlanId { get; set; }


    }

    public class HouseholdRegAcceptVm
    {
        [Display(Name = "Household Listing Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }

    }
    public class ListingAcceptVm
    {
        [Display(Name = "Household Listing Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
        [Display(Name = "Location")]
        public int? LocationId { get; set; }
        
    }
    public class RecAcceptVm
    {
        [Display(Name = "Registration Group")]
        public int? TargetPlanId { get; set; }
        [Display(Name = "Name of the Batch")]
        public string BatchName { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        [Display(Name = "County")]
        public int? CountyId { get; set; }
    }

    public class SrIprsValidateVm
    {
        public int Id { get; set; }
        public int? TargetPlanId { get; set; }
    }

    public class EnLocation
    {
        public int Id { get; set; }
        public string Display { get; set; }
         public bool IsSelected { get; set; }
    }

    
    public class EnumeratorAddLoc
    {
        public int EnumeratorId { get; set; }
        public List<EnLocation> Locations { get; set; }
    }
    public class EnrolmentSetup
    {
        public int FinancialYearId { get; set; }
        public int ExpansionStatusId { get; set; }
        public int SublocationId { get; set; }
        public int RegistrationGroupId { get; set; }
        public int Quota { get; set; }
    }
    public class RecertificationScreener
    {
        public DateTime RecertificationDate { get; set; }
        public int SublocationId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HhdIdNo { get; set; }
        public string HHdSex { get; set; }
        public DateTime? HhdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgidNo { get; set; }
        public string CgSex { get; set; }
        public DateTime? CgdoB { get; set; }
        public int HouseholdMembers { get; set; }
    }




}
