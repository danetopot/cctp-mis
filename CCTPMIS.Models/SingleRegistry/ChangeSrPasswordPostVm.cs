﻿namespace CCTPMIS.Models.SingleRegistry
{
    public class ChangeSrPasswordPostVm  
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string TokenCode { get; set; }
    }
}