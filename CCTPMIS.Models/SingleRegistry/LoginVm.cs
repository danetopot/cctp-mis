﻿namespace CCTPMIS.Models.SingleRegistry
{
    public class LoginVm
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}