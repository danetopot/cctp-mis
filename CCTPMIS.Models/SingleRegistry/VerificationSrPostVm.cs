﻿namespace CCTPMIS.Models.SingleRegistry
{
    public class  VerificationSrPostVm
    {
        public string TokenCode { get; set; }

        public string IDNumber { get; set; }

        public string Names { get; set; }
    }
}