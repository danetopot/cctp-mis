﻿namespace CCTPMIS.Models.SingleRegistry
{
    public class IprsVerificationVm : SRAPI
    {
        public string First_Name { get; set; }
        public string Surname { get; set; }
        public string Middle_Name { get; set; }
        public string ID_Number { get; set; }
        public string Gender { get; set; }
        public string Date_of_Birth { get; set; }
        public string Date_of_Issue { get; set; }
        public string Place_of_Birth { get; set; }
        public string Serial_Number { get; set; }
        public string Address { get; set; }
    }
}