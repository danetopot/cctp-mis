﻿namespace CCTPMIS.Models.SingleRegistry
{
    public class SrVerificationVm : SRAPI
    {
        public string ProgrammeCode { get; set; }
        public string NamesOnSR { get; set; }
        public string IDNumber { get; set; }
        public string Sex { get; set; }
        public string DateOfBirth { get; set; }
        public string CountyName { get; set; }
        public string ConstituencyName { get; set; }
        public string LocationName { get; set; }
        public string SubLocationName { get; set; }
        public string Village { get; set; }
    }
}
