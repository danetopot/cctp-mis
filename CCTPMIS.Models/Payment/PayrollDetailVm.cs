﻿namespace CCTPMIS.Models.Payment
{
    using Business.Model;

    public class PayrollDetailVm
    {
        public GetPayrollSummaryVm GetPayrollSummaryVm { get; set; }

        public PaymentCycleDetail PaymentCycleDetail { get; set; }
    }
}