﻿namespace CCTPMIS.Models.Payment
{
    public class SelectListVm
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}