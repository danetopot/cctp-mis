﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class PrepayrollExceptionsFilterViewModel
    {
        [Display(Name = "Payment Cycle ")]
        public int PaymentCycle { get; set; }
        [Display(Name = "Programme ")]
        public int Programme { get; set; }
    }
}