﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Models.Enrolment;
using PagedList;

namespace CCTPMIS.Models.Payment
{


    public class PayrollStatementListViewModel
    {
        [DisplayName("Programme")]
        public int? ProgrammeId { get; set; }

        [DisplayName("Payment Cycle")]
        public int? PaymentCycleId { get; set; }

        [DisplayName("Credit Status")]
        public int? CreditStatusId { get; set; }

        [DisplayName("Report Type")]
        public int? ReportTypeId { get; set; }

        [DisplayName("Payment Type")]
        public int? PaymentTypeId { get; set; }

        public PayrollStatementDetailsViewModel PayrollStatement { get; set; }
        public IPagedList<PayrollStatementDetailsViewModel> Details { get; set; }
        public IEnumerable<PayrollStatementDetailsViewModel> IDetails { get; set; }
        public ReportSummaryViewModel Summary { get; set; }
        public IPagedList<ReportSummaryViewModel> Summaries { get; set; }
        public IEnumerable<ReportSummaryViewModel> ISummaries { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
    public class StringParameter
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class FilterParameter
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class PayrollStatementDetailsViewModel
    {
        public string Programme { get; set; }

        [DisplayName("Enrolment No")]
        public int EnrolmentNo { get; set; }

        [DisplayName("Payroll No")]
        public string PayrollNo { get; set; }

        [DisplayName("Account Name")]
        public string AccountName { get; set; }

        [DisplayName("Beneficiary ID No")]
        public string BeneficiaryIdNo { get; set; }

        [DisplayName("Beneficiary Name")]
        public string BeneficiaryName { get; set; }

        [DisplayName("Beneficiary Sex")]
        public string BeneficiarySex { get; set; }

        [DisplayName("Beneficiary DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime BeneficiaryDOB { get; set; }

        [DisplayName("Caregiver ID No")]
        public string CaregiverIdNo { get; set; }

        [DisplayName("Caregiver Name")]
        public string CaregiverName { get; set; }

        [DisplayName("Caregiver Sex")]
        public string CaregiverSex { get; set; }

        [DisplayName("Caregiver DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime CaregiverDOB { get; set; }

        [DisplayName("Payment Cycle")]
        public string PaymentCycle { get; set; }

        [DisplayName("Entitlement Amount")]
        public decimal EntitlementAmount { get; set; }

        [DisplayName("Arrears Amount")]
        public decimal ArrearsAmount { get; set; }

        [DisplayName("")]
        public string PSP { get; set; }

        [DisplayName("Credit Status")]
        public string CreditStatus { get; set; }

        [DisplayName("TransactionNo")]
        public string TransactionNo { get; set; }

        [DisplayName("Account No")]
        public string AccountNo { get; set; }

        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
    }

    public class PayrollStatementSummaryViewModel
    {
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public decimal Beneficiaries { get; set; }
        public decimal Amount { get; set; }
    }
}