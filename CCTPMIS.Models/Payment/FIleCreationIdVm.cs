﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class FIleCreationIdVm
    {
        [Display(Name = "File ID")]
        public int? FileCreationId { get; set; }
    }
}