﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class PaymentCycleAmmendViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Cycle Period")]
        public string Period { get; set; }

        [Display(Name = "Programme")]
        public string Programme { get; set; }

        [Display(Name = "User ID")]
        public int UserId { get; set; }
        [Display(Name = "Financial Year")]
        public string Year { get; set; }
    }
}