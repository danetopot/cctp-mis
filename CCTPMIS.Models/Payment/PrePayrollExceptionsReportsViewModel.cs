﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Models.Enrolment;
using PagedList;

namespace CCTPMIS.Models.Payment
{
    public class PrePayrollExceptionsListViewModel
    {
        [DisplayName("Programme")]
        public int? ProgrammeId { get; set; }

        [DisplayName("Payment Cycle")]
        public int? PaymentCycleId { get; set; }

        [DisplayName("Credit Status")]
        public int? CreditStatusId { get; set; }

        [DisplayName("Status")]
        public int? StatusId { get; set; }

        [DisplayName("Exception Category")]
        public int? ExceptionCategoryId { get; set; }

        [DisplayName("Report Type")]
        public int? ReportTypeId { get; set; }

        [DisplayName("Payment Type")]
        public int? PaymentTypeId { get; set; }

        public PrePayrollExceptionsViewModel PrePayrollException { get; set; }
        public IPagedList<PrePayrollExceptionsViewModel> Details { get; set; }
        public IEnumerable<PrePayrollExceptionsViewModel> IDetails { get; set; }
        public ReportSummaryViewModel Summary { get; set; }
        public IPagedList<ReportSummaryViewModel> Summaries { get; set; }
        public IEnumerable<ReportSummaryViewModel> ISummaries { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }

    public class PrePayrollExceptionsViewModel
    {
        public string Programme { get; set; }

        [DisplayName("Enrolment No")]
        public int EnrolmentNo { get; set; }

        [DisplayName("Account Name")]
        public string AccountName { get; set; }

        [DisplayName("Beneficiary ID No")]
        public string BeneficiaryIdNo { get; set; }

        [DisplayName("Beneficiary Name")]
        public string BeneficiaryName { get; set; }

        [DisplayName("Beneficiary Sex")]
        public string BeneficiarySex { get; set; }

        [DisplayName("Beneficiary DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? BeneficiaryDOB { get; set; }

        [DisplayName("Caregiver ID No")]
        public string CaregiverIdNo { get; set; }

        [DisplayName("Caregiver Name")]
        public string CaregiverName { get; set; }

        [DisplayName("Caregiver Sex")]
        public string CaregiverSex { get; set; }

        [DisplayName("Caregiver DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? CaregiverDOB { get; set; }

        [DisplayName("Payment Cycle")]
        public string PaymentCycle { get; set; }

        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public string Actioned { get; set; }
        public string Reasons { get; set; }

        [DisplayName("Exception Category")]
        public string ExceptionCategory { get; set; }
    }
}