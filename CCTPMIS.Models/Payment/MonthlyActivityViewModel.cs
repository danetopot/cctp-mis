﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using CCTPMIS.Models.Enrolment;
using PagedList;

namespace CCTPMIS.Models.Payment
{
    public class MonthlyActivityListViewModel
    {
        [DisplayName("Programme")]
        public int? ProgrammeId { get; set; }

        [DisplayName("Activity Month")]
        public int? ActivityMonthId { get; set; }

        [DisplayName("Credit Status")]
        public int? CreditStatusId { get; set; }

        [DisplayName("Report Type")]
        public int? ReportTypeId { get; set; }

        [DisplayName("Unique Withdrawals")]
        public int? UniqueWithdrawalsId { get; set; }

        [DisplayName("Dormancy")]
        public int? DormancyId { get; set; }

        [DisplayName("Proof of Life")]
        public int? ProofOfLifeId { get; set; }

        [DisplayName("Payment Type")]
        public int? PaymentTypeId { get; set; }

        public MonthlyActivityViewModel PrePayrollException { get; set; }
        public IPagedList<MonthlyActivityViewModel> Details { get; set; }
        public IEnumerable<MonthlyActivityViewModel> IDetails { get; set; }
        public ReportSummaryViewModel Summary { get; set; }
        public IPagedList<ReportSummaryViewModel> Summaries { get; set; }
        public IEnumerable<ReportSummaryViewModel> ISummaries { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }

    public class MonthlyActivityViewModel
    {
        public string Programme { get; set; }

        [DisplayName("Enrolment No")]
        public int EnrolmentNo { get; set; }

        [DisplayName("Account Name")]
        public string AccountName { get; set; }

        [DisplayName("Beneficiary ID No")]
        public string BeneficiaryIdNo { get; set; }

        [DisplayName("Beneficiary Name")]
        public string BeneficiaryName { get; set; }

        [DisplayName("Beneficiary Sex")]
        public string BeneficiarySex { get; set; }

        [DisplayName("Beneficiary DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? BeneficiaryDOB { get; set; }

        [DisplayName("Caregiver ID No")]
        public string CaregiverIdNo { get; set; }

        [DisplayName("Caregiver Name")]
        public string CaregiverName { get; set; }

        [DisplayName("Caregiver Sex")]
        public string CaregiverSex { get; set; }

        [DisplayName("Caregiver DOB")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? CaregiverDOB { get; set; }

        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }

        [DisplayName("Activity Month")]
        public string ActivityMonth { get; set; }

        [DisplayName("Had Unique Withdrawal")]
        public string HadUniqueWithdrawal { get; set; }

        [DisplayName("Identity of Withdrawal")]
        public string IdentityOfWithdrawal { get; set; }

        [DisplayName("Date of Withdrawal")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DateOfWithdrawal { get; set; }

        [DisplayName("Is Account Dormant")]
        public string AccountDormant { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        [DisplayName("Dormancy Date")]
        public DateTime? DormancyDate { get; set; }

        [DisplayName("Proof of Life")]
        public string ProofOfLife { get; set; }

        [DisplayName("Verification Date")]
        public string VerificationDate { get; set; }

        [DisplayName("IsDue for Clawback")]
        public string IsDueForClawback { get; set; }

        [DisplayName("Clawback Amount")]
        public decimal ClawbackAmount { get; set; }

        [DisplayName("Date of Clawback")]
        public string DateOfClawback { get; set; }
    }

    public class MonthlyActivityReportSummaryViewModel
    {
        public string County { get; set; }
        public string SubCounty { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
        public decimal Beneficiaries { get; set; }
        public decimal Amount { get; set; }
    }
}