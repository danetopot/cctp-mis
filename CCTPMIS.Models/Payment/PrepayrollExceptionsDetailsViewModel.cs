﻿namespace CCTPMIS.Models.Payment
{
    using Business.Model;

    public class PrepayrollExceptionsDetailsViewModel
    {
        public PrepayrollBulkActionViewModel PrepayrollBulkActionViewModel { get; set; }

        public PrepayrollDuplicateId PrepayrollDuplicateId { get; set; }

        public PrepayrollIneligible PrepayrollIneligible { get; set; }

        public PrepayrollInvalidId PrepayrollInvalidId { get; set; }

        public PrepayrollInvalidPaymentAccount PrepayrollInvalidPaymentAccount { get; set; }

        public PrepayrollInvalidPaymentCard PrepayrollInvalidPaymentCard { get; set; }

        public PrepayrollSingleActionViewModel PrepayrollSingleActionViewModel { get; set; }

        public PrepayrollSuspicious PrepayrollSuspicious { get; set; }
    }
}