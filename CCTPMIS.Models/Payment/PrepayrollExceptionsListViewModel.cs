﻿namespace CCTPMIS.Models.Payment
{
    using Business.Model;
    using PagedList;

    public class PrepayrollExceptionsListViewModel
    {
        public IPagedList<PrepayrollDuplicateId> PrepayrollDuplicateIdList { get; set; }
        public PrepayrollExceptionsFilterViewModel PrepayrollExceptionsFilterViewModel { get; set; }
        public IPagedList<PrepayrollIneligible> PrepayrollIneligiblesList { get; set; }
        public IPagedList<PrepayrollInvalidId> PrepayrollInvalidIdsList { get; set; }
        public IPagedList<PrepayrollInvalidPaymentAccount> PrepayrollInvalidPaymentAccountsList { get; set; }
        public IPagedList<PrepayrollInvalidPaymentCard> PrepayrollInvalidPaymentCardsList { get; set; }
        public IPagedList<PrepayrollSuspicious> PrepayrollSuspiciousList { get; set; }
    }
}