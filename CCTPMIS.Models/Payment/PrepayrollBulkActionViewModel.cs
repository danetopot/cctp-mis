﻿namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class PrepayrollBulkActionViewModel
    {
        [Required]
        [Display(Name = "Enrolment Group")]
        public int[] EnrolmentGroupId { get; set; }

        [Required]
        [Display(Name = "Exception Type")]
        public int[] ExceptionTypeId { get; set; }

        [Display(Name = "Household")]
        public int? HhId { get; set; }

        [Required]
        [Display(Name = "ID")]
        public string Id { get; set; }
        [Display(Name = "Notes " )]
        [Required]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [Required]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycle { get; set; }

        [Display(Name = "Person")]
        public int? PersonId { get; set; }

        [Required]
        [Display(Name = "Programme")]
        public int Programme { get; set; }

        [Display(Name = "Remove existing Supporting Document")]
        public bool RemovePreviousSupportingDoc { get; set; }

        [Display(Name = "Replace Previous Actions")]
        public bool ReplacePreviousActioning { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Upload { get; set; }

        [Display(Name = "Upload Supporting documents")]
        public string UploadFile { get; set; }
    }
}