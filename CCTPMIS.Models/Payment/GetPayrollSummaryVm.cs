﻿namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;

    public class GetPayrollSummaryVm
    {
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households with Duplicated ID Numbers")]
        public int PayrollDuplicateIdHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households Exceptions")]
        public int PayrollExceptionsHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Ineligible  Beneficiary Households")]
        public int PayrollIneligibleHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households with IPRS Mismatch")]
        public int PayrollInvalidIdHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households without Payment Accounts ")]
        public int PayrollInvalidPaymentAccountHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households without Payment Cards")]
        public int PayrollInvalidPaymentCardHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,###}")]
        [Display(Name = "Total Beneficiary Households  with Suspicious Payment Amounts")]
        public int PayrollSuspiciousHhs { get; set; }
        
        [Display(Name = "Total Payroll Amount (KES)")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? TotalPayrollAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0: #,##0}")]
        [Display(Name = "Total Beneficiary Households in Payroll")]
        public int TotalPayrollHhs { get; set; }


    }
}