﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class ProcessPrePayrollExceptionsActionVm
    {
        [Display(Name = "Rows")]
        public int? NoOfRows { get; set; }
        [Display(Name = "Supporting Document")]
        public string SupportingDoc { get; set; }
    }
}