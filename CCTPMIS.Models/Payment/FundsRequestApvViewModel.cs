﻿namespace CCTPMIS.Models.Payment
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class FundsRequestApvViewModel
    {
        [Display(Name = "Funds Request File ")]
        public int? FileCreationId { get; set; }

        public int Id { get; set; }

        [Display(Name = "Payment Cycle ")]
        public int PaymentCycleId { get; set; }

        [DataType(DataType.Upload)]
        [Required]
        public HttpPostedFileBase Upload { get; set; }

        [Display(Name = "Upload   Funds Request Document")]
        public string UploadFile { get; set; }
    }
}