﻿using System.ComponentModel.DataAnnotations;
using CCTPMIS.Business.Model;

namespace CCTPMIS.Models.Payment
{
    public class SelectListA
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }


    public class MonthlyActivitySummaryViewModel
    {
        public int? BeneAccountMonthlyActivityId { get; set; }
        public byte PSPId { get; set; }
        [Display(Name = "Bank")]
        public string PSPName { get; set; }
        [Display(Name = "Total Accounts")]
        public int TotalAccounts { get; set; }
        [Display(Name = "Total Reported  Accounts")]
        public int ReportedAccounts { get; set; }
        [Display(Name = "Total Pending Accounts")]
        public int PendingAccounts { get; set; }
        [Display(Name = "Total Accounts with Unique Transactions")]
        public int HadUniqueWdl { get; set; }
 
        [Display(Name = "Total Accounts With Bios Verified")]
        public int HadBeneBiosVerified { get; set; }
        [Display(Name = "Total Dormant Accounts")]
        public int IsDormant { get; set; }
        [Display(Name = "Total Accounts Due for Clawback")]
        public int IsDueForClawback { get; set; }
        public string PSPActivityStatus { get; set; }
    }

    public class MonthlyActivityPSPSummaryViewModel
    {
        [Display(Name = "PSP")]
        public string PSP { get; set; }

        [Display(Name = "Payment Cycle")]
        public string PaymentCycle { get; set; }

        [Display(Name = "Payment Zone")]
        public string PaymentZone { get; set; }

        [Display(Name = "Payment Zone Commission Amount")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal PaymentZoneCommAmt { get; set; }
    }

    public class PostPayrollSummaryViewModel
    {
        [Display(Name = "Total Beneficiary Households in Payroll")]
        public int TotalPaymentHhs { get; set; }
        [Display(Name = "Total Beneficiary Households Successful Credits")]
        public int SuccessfulPayments { get; set; }
        [Display(Name = "Total Beneficiary Households with Failed Credits")]
        public int FailedPayments { get; set; }
        [Display(Name = "Total Beneficiary Households in Payroll with Exceptions")]
        public int PaymentExceptionsHhs { get; set; }
        [Display(Name = "Total Beneficiary Households with Invalid National IDs")]
        public int PaymentInvalidIDHhs { get; set; }
        [Display(Name = "Total Beneficiary Households in Payroll with Duplicate IDs")]
        public int PaymentDuplicateIDHhs { get; set; }

        [Display(Name = "Total Beneficiary Households  Without Payment Accounts / Cards ")]
        public int? PaymentInvalidHhs { get; set; }

        [Display(Name = "Without Payment Accounts")]
        public int PaymentInvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Without Payment Cards")]
        public int PaymentInvalidPaymentCardHhs { get; set; }
        [Display(Name = "Total Ineligible Beneficiary Households in Payroll")]
        public int PaymentIneligibleHhs { get; set; }
        [Display(Name = "Total Beneficiary Households with Suspicious Payments")]
        public int PaymentSuspiciousHhs { get; set; }
        [Display(Name = "Total Amount in Payroll (KES)")]
        public decimal TotalPaymentAmount { get; set; }
        [Display(Name = "Total Beneficiary Households in Payroll")]
        public int? TotalPayrollHhs { get; set; }
        [Display(Name = "Total Payroll Amount (KES)")]
        public decimal? TotalPayrollAmount { get; set; }
        [Display(Name = "Total Successful Credit Amount (KES)")]
        public decimal? SuccessfulPaymentsAmount { get; set; }
        [Display(Name = "Total Failed Credit Amount (KES)")]
        public decimal? FailedPaymentsAmount { get; set; }

        [Display(Name = "IPRS Mismatch  - Beneficiaries")]
        public int? PaymentInvalidIDHhs_BENE { get; set; }

        [Display(Name = "IPRS Mismatch  - Caregivers")]
        public int? PaymentInvalidIDHhs_CG { get; set; }

        [Display(Name = "Duplicated Beneficiary National IDs  Within Programme")]
        public int? PaymentDuplicateIDHhs_BENEIN { get; set; }

        [Display(Name = "Duplicated Beneficiary National IDs  Across Programme")]
        public int? PaymentDuplicateIDHhs_BENEACC { get; set; }

        [Display(Name = "Duplicated Caregiver National ID  Within Programme")]

        public int? PaymentDuplicateIDHhs_CGIN { get; set; }
        [Display(Name = "Duplicated Caregiver National ID  Across Programme")]
        public int? PaymentDuplicateIDHhs_CGAC { get; set; }

        [Display(Name = "Ineligible Beneficiaries ")]
        public int? PaymentIneligibleHhs_BENE { get; set; }

        [Display(Name = "Ineligible Caregivers ")]
        public int? PaymentIneligibleHhs_CG { get; set; }
 
    }

}