﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class PrepayrollAuditViewModel
    {

        /*
        [Display(Name = "Actioned Beneficiary Households with Duplicate National ID Numbers")]
        public int ActionedDuplicateNationalIdHhs { get; set; }
        [Display(Name = "Actioned Ineligible Beneficiary Households")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int ActionedIneligibleHhs { get; set; }
        [Display(Name = "Actioned Beneficiary Households with Invalid National ID Numbers")]
        public int ActionedInvalidNationalIdHhs { get; set; }
        [Display(Name = "Actioned Beneficiary Households with Invalid Bank Accounts")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int ActionedInvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Actioned Beneficiary Households with Invalid Payment Cards")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int ActionedInvalidPaymentCardHhs { get; set; }
        [Display(Name = "Actioned Beneficiary Households with Suspicious Payments")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int ActionedSuspiciousPaymentHhs { get; set; }
        [Display(Name = "Beneficiary Households with Duplicate National ID Numbers Action File")]
        public int DuplicateIdActionsFileId { get; set; }
        [Display(Name = "Beneficiary Households with Duplicate National ID Numbers e.g. Double Dipping")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int DuplicateNationalIdHhs { get; set; }
        [Display(Name = "Total Eligible Beneficiary Households ")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int EnroledHhs { get; set; }
        [Display(Name = "Total Enrolled Beneficiary Households ")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int EnrolmentGroupHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Exceptions File ID")]
        public int ExceptionsFileId { get; set; }
        [Display(Name = "Ineligible Beneficiary Households Actions File ID")]
        public int IneligibleBeneficiaryActionsFileId { get; set; }
        [Display(Name = "Ineligible Beneficiary Households")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int IneligibleHhs { get; set; }
        [Display(Name = "Actions File for Primary Recipient with   Invalid National ID Numbers")]
        public int InvalidIdActionsFileId { get; set; }
        [Display(Name = "Households with Invalid National ID Numbers")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int InvalidNationalIdHhs { get; set; }
        [Display(Name = "Action File for Beneficiary Households with Invalid Payment Accounts")]
        public int InvalidPaymentAccountActionsFileId { get; set; }
        [Display(Name = "Beneficiary Households with Invalid Payment Accounts")]
        public int InvalidPaymentAccountHhs { get; set; }
        [Display(Name = "Action file for Beneficiary Households with Invalid Payment Cards")]
        public int InvalidPaymentCardActionsFileId { get; set; }
        [Display(Name = "Beneficiary Households without Payment Cards")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int InvalidPaymentCardHhs { get; set; }
        [Display(Name = "Total Arrears Amount")]
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? PayrollAdjustmentAmount { get; set; }
        [Display(Name = "Payroll Total Amount")]
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? PayrollAmount { get; set; }
        [Display(Name = "Basic Entitlement Amount")]
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? PayrollEntitlementAmount { get; set; }
        [Display(Name = "Payroll Has been Actioned")]
        public int PayrollHasBeenAcioned { get; set; }
        [Display(Name = "Total Beneficiary Household in Payroll")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public int PayrollHhs { get; set; }
        [Display(Name = "Actions File for Beneficiary Households with Suspicious Payments ")]
        public int SuspiciousPaymentActionsFileId { get; set; }
        [Display(Name = "Beneficiary Households  with Suspicious Payments")]
        public int SuspiciousPaymentHhs { get; set; }

        */


        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Prepayroll Exceptions File")]
        public int? ExceptionsFileId { get; set; }


        [Display(Name = "Total Enrolled Beneficiary Households")]
        [DisplayFormat(DataFormatString = "{0:#,##0}")] 
        public int? EnroledHhs { get; set; }

        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Total Eligible  Beneficiary Households")]
        public int? EnrolmentGroupHhs { get; set; }


        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Total  Beneficiary Households in Payroll ")]
        public int? PayrollHhs { get; set; }
        [Display(Name = "Basic Entitlement Amount")]
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? PayrollEntitlementAmount { get; set; }
        [Display(Name = "Total Arrears Amount")]
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        public decimal? PayrollAdjustmentAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:KES #,##0.00}")]
        [Display(Name = "Total Payroll Amount")]
        public decimal? PayrollAmount { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with IPRS Mismatch")]
        public int? InvalidNationalIDHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with IPRS Mismatch for Primary Recipient")]
        public int? InvalidNationalIDHhs_BENE { get; set; }

        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with IPRS Mismatch for Secondary Recipient")]
        public int? InvalidNationalIDHhs_CG { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with  SR/IPRS Mismatch ")]
        public int? ActionedInvalidNationalIDHhs { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with  SR/IPRS Mismatch for Primary Recipient ")]
        public int? ActionedInvalidNationalIDHhs_BENE { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with  SR/IPRS Mismatch for Secondary Recipient")]
        public int? ActionedInvalidNationalIDHhs_CG { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Duplicated National ID Numbers")]
        public int? DuplicateNationalIDHhs { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Duplicated Primary Recipient National ID Numbers ")]
        public int? DuplicateNationalIDHhs_BENEIN { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Duplicated Primary Recipient National ID Across Programmes ")]
        public int? DuplicateNationalIDHhs_BENEACC { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Duplicated Secondary Recipient National ID Numbers ")]
        public int? DuplicateNationalIDHhs_CGIN { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Duplicated Secondary Recipient National ID Numbers Across Programmes ")]
        public int? DuplicateNationalIDHhs_CGACC { get; set; } = 0;

        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Duplicated National ID Numbers ")]
        public int? ActionedDuplicateNationalIDHhs { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Duplicated Primary Recipient National ID Numbers ")]
        public int? ActionedDuplicateNationalIDHhs_BENEIN { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Duplicated Primary Recipient National ID Numbers Across Programmes")]
        public int? ActionedDuplicateNationalIDHhs_BENEACC { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Duplicated Secondary Recipient National ID Numbers ")]
        public int? ActionedDuplicateNationalIDHhs_CGIN { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Duplicated Secondary Recipient National ID Numbers Across Programmes")]
        public int? ActionedDuplicateNationalIDHhs_CGACC { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Invalid Payment Account")]
        public int? InvalidPaymentAccountHhs { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Invalid Payment Account")]
        public int? ActionedInvalidPaymentAccountHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Invalid Payment Cards")]
        public int? InvalidPaymentCardHhs { get; set; } = 0;
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Invalid Payment Cards")]
        public int? ActionedInvalidPaymentCardHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Ineligible Beneficiary Households")]
        public int? IneligibleHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Ineligible Primary Recipients")]
        public int? IneligibleHhs_BENE { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Ineligible Secondary Recipients")]
        public int? IneligibleHhs_CG { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households")]
        public int? ActionedIneligibleHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Ineligible Primary Recipients")]
        public int? ActionedIneligibleHhs_BENE { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Ineligible Secondary Recipients")]
        public int? ActionedIneligibleHhs_CG { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Beneficiary Households with Suspicious Payment Amounts")]
        public int? SuspiciousPaymentHhs { get; set; }
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        [Display(Name = "Actioned Beneficiary Households with Suspicious Payment Amounts")]
        public int? ActionedSuspiciousPaymentHhs { get; set; }

        [Display(Name = "Payroll has been Actioned")]
        public int? PayrollHasBeenActioned { get; set; }
    }
}