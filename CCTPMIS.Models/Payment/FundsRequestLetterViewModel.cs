﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using System;
    using System.Collections.Generic;

    using Business.Model;

    public class FundsRequestLetterViewModel
    {
        [Display(Name = "Exclusions")]
        public string Exclusions { get; set; }

        [Display(Name = "Request Date")]
        public DateTime FundsRequestDate { get; set; }

        public List<FundsRequestDetail> FundsRequestDetails { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Display(Name = "Programmes")]
        public string Programmes { get; set; }

        [Display(Name = "Reference No.")]
        public string ReferenceNo { get; set; }
        [Display(Name = "End Month")]
        public string ToMonth { get; set; }
        [Display(Name = "Total Households")]
        public int TotalHouseholds { get; set; }
    }
}