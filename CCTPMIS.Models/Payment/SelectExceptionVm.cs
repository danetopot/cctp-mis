﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    public class SelectExceptionVm
    {
        [Display(Name = "Exception Code")]
        public string ExceptionCode { get; set; }

        [Display(Name = "Exception Description")]
        public string ExceptionDesc { get; set; }

        [Display(Name = "Exception Type")]
        public int ExceptionTypeId { get; set; }
    }
}