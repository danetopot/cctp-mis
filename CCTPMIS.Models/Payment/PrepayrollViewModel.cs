﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using Business.Model;

    public class PrepayrollViewModel
    {
        public PaymentCycleDetail PaymentCycleDetail { get; set; }
        [Display(Name = "Payment Cycle")]
        public int? PaymentCycleId { get; set; }

        public PrepayrollAuditViewModel PrepayrollAuditViewModel { get; set; }
        [Display(Name = "Programme")]
        public int? ProgrammeId { get; set; }
    }
}