﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Models.Payment
{
    using System.Collections.Generic;

    using Business.Model;

    public class RequestForFunds
    {
        public FundsRequest FundsRequest { get; set; }

        public List<ReqForFundsListVM> ReqForFundsVMs { get; set; }
    }

    public class PspAnalysis
    {       
        [Display(Name = "Psp Name")]
        public string Psp { get; set; }
        [Display(Name = "Processed Payment")]
        public int ProcessedPayment { get; set; }
        [Display(Name = "Successful payments")]
        public int SuccessfulPayments { get; set; }
        [Display(Name = "Failed Payments")]
        public int FailedPayments { get; set; }
        [Display(Name = "Pending Payments")]
        public int PendingPayments { get; set; }
        [Display(Name = "Total Payments")]
        public int TotalPayments => ProcessedPayment + PendingPayments;
    }
    public class CreditReportViewModel
    {
        [Display(Name = "ID")]
        public int? Id { get; set; }
        [Display(Name = "Payment Cycle")]
        public int? PaymentCycleId { get; set; }
        public PaymentCycle PaymentCycle { get; set; }     
        public List<PspAnalysis> PspAnalysis { get; set; }

        public PostPayrollSummaryViewModel PostPayrollSummaryViewModel { get; set; }
    }


    public class ActivityReportSummaryViewModel
    {
        [Display(Name = "ID")]
        public int? Id { get; set; }
        public BeneAccountMonthlyActivity Month { get; set; }
        public List<MonthlyActivitySummaryViewModel> PspActivity { get; set; }
        public List<MonthlyActivityPSPSummaryViewModel> PspSummaryActivity { get; set; }
    }

    public class ActivityReportPSPSummaryViewModel
    {
        [Display(Name = "ID")]
        public int? Id { get; set; }
        public BeneAccountMonthlyActivity Month { get; set; }
        public List<MonthlyActivityPSPSummaryViewModel> PspActivity { get; set; }
    }


    public class ReqForFundsListVM
    {
        [Display(Name = "Amount")]
        [DataType(DataType.Currency), 
         DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal Amount { get; set; }
        [Display(Name = "Commission")]
        [DataType(DataType.Currency),
         DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal Commission { get; set; }
        [Display(Name = "Ref. Number")]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public int No { get; set; }
        [Display(Name = "Total No. Of Households")]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]

        public int NoOfHouseholds { get; set; }

        [Display(Name = "Programme")]
        public string Programme { get; set; }

        [Display(Name = "PSP")]
        public string PSP { get; set; }
        [Display(Name = "Total.")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal Total { get; set; }
    }

    public class AddEditReconciliationDetailVm : AddEditPaymentCycleVm
    {
    }

    public class GetReconDetailVm
    {
        [Display(Name = "Total Funds Requests Amount")]
        public decimal CrFundsRequests { get; set; }
        [Display(Name = "Total Claw back Amount")]
        public decimal CrClawBacks { get; set; }
        [Display(Name = "Total Payment Amount")]
        public decimal DrPayments { get; set; }
        [Display(Name = "Total Commissions Amount")]
        public decimal DrCommissions { get; set; }
        [Display(Name = "Total Opening Balance Amount (Statement)")]
        public decimal OpeningBalance { get; set; }
        [Display(Name = "Total Opening Balance Amount (Sytem)")]
        public decimal OpeningBalanceSystem { get; set; }
        [Display(Name = "Total Closing Balance Amount")]
        public decimal ClosingBalance { get; set; }
    }

    public class AddEditPaymentCycleVm
    {
        [Display(Name = "Rows")]
        public int NoOfRows { get; set; }

        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        [Display(Name = "Payment Stage")]
        public string PaymentCycleStage { get; set; }
        [Display(Name = "Payment Stage")]
        public int PaymentCycleStageId { get; set; }
        [Display(Name = "Supporting Documents")]
        public string SupportingDoc { get; set; }
    }

    public class AddEditReconSpVm
    {
        public int Id { get; set; }
        [Display(Name = "Payment Cycle ID")]
        public int PaymentCycleId => Id;
        [Display(Name = "Start Month")]
        public string FromMonth { get; set; }
        [Display(Name = "End Month")]
        public string ToMonth { get; set; }
        [Display(Name = "Financial Year")]
        public string FinancialYear { get; set; }
        [Display(Name = "Payment Cycle")]
        public string CycleName => $"{FinancialYear} : {FromMonth} - {ToMonth}";

    }

    public class ReconAnalysisVm
    {
        public List<AddEditReconSpVm> results { get; set; }
        public string Error { get; set; }
    }

    public class AddEditReconDetailSpVm
    {
        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        [Display(Name = "Supporting Document ID")]
        public int? FileCreationId { get; set; }
        [Display(Name = "Supporting Document ID")]
        public int? FileId { get; set; }
        [Display(Name = "Supporting Document")]
        public string SupportingDoc { get; set; }


    }
}