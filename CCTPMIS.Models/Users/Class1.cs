﻿using CCTPMIS.Business.Model;
using PagedList;

namespace CCTPMIS.Models.Users
{

    public class IdClass
    {
        public int Id { get; set; }
    }
    public class Pin
    {
        public string Password { get; set; }
        public string HashedPassword { get; set; }
    }
    public class ProgrammeOfficersListViewModel
    {
        public int? County { get; set; }
        public int? Page { get; set; }
        public int? Constituency { get; set; }
        public string Name { get; set; }

        public IPagedList<ProgrammeOfficer> Officers { get; set; }

    }
    public class EnumeratorsListViewModel
    {
        public string NationalIdNo { get; set; }
        public string EnumeratorNo { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public IPagedList<Enumerator> Enumerators { get; set; }

    }

    public class UsersListViewModel
    {
        public string NationalIdNo { get; set; }
        public string StaffNo { get; set; }
        public string PhoneNumber { get; set; }
        public string UserGroup { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public IPagedList<ApplicationUser> Users { get; set; }

    }
}
