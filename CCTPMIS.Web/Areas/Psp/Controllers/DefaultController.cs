﻿using System.Linq;

namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using Services;

    [Authorize]
    public class DefaultController : Controller
    {
        // GET: Psp/Default
        protected readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public DefaultController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:VIEW")]
        public async Task<ActionResult> Details()
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var allPsp = (await GenericService.GetAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true)).ToList();
            if (!allPsp.Any())
            {
                return View("NotConfigured");
            }

            if (allPsp.Count() > 1)
            {
                return View("NotConfigured");
            }

            var model = allPsp.FirstOrDefault();

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:VIEW")]
        public ActionResult Index()
        {
            return RedirectToAction($@"Details");
        }
    }
}