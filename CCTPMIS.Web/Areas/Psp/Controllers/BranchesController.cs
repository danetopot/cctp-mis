﻿namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class BranchesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public BranchesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:ENTRY")]
        // GET: admin/PspBranches/Create
        public ActionResult Create()
        {
            // ViewBag.SubLocationId = new SelectList(await
            // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name");
            return View();
        }

        // POST: admin/PspBranches/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PSP-PROFILE:ENTRY")]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Code,Name,IsActive,Longitude,Latitude,SubLocationId")]
            PspBranch pspBranch)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var pspId = (await GenericService.GetOneAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true)).Id;

                pspBranch.PspId = pspId;

                pspBranch.CreatedBy = int.Parse(User.Identity.GetUserId());
                pspBranch.CreatedOn = DateTime.Now;

                if (ModelState.IsValid)
                {
                    GenericService.Create(pspBranch);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred. Kindly contact the Administrator to confirm your Bank Details";
            }
            return View(pspBranch);
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:VIEW")]
        // GET: admin/PspBranches/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var pspBranch = await GenericService.GetOneAsync<PspBranch>(
                                x => x.Id == id.Value,
                                "CreatedByUser,ModifiedByUser,Psp,SubLocation").ConfigureAwait(true);
            var userId = int.Parse(User.Identity.GetUserId());
            var pspId = (await GenericService.GetOneAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true)).Id;
            if (pspBranch == null)
            {
                return HttpNotFound();
            }

            if (pspId != pspBranch.PspId)
            {
                return HttpNotFound();
            }

            return View(pspBranch);
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:MODIFIER")]
        // GET: admin/PspBranches/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var pspBranch = await GenericService.GetOneAsync<PspBranch>(x => x.Id == id.Value).ConfigureAwait(true);
            var userId = int.Parse(User.Identity.GetUserId());
            var pspId = (await GenericService.GetOneAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true)).Id;

            if (pspBranch == null)
            {
                return HttpNotFound();
            }

            if (pspId != pspBranch.PspId)
            {
                return HttpNotFound();
            }

            // ViewBag.SubLocationId = new SelectList(await
            // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name", pspBranch.SubLocationId);
            return View(pspBranch);
        }

        // POST: admin/PspBranches/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PSP-PROFILE:MODIFIER")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,PspId,Code,Name,IsActive,Longitude,Latitude,SubLocationId,ModifiedOn,ModifiedBy,CreatedOn,CreatedBy")] PspBranch pspBranch)
        {
            pspBranch.ModifiedBy = int.Parse(User.Identity.GetUserId());
            pspBranch.ModifiedOn = DateTime.Now;
            var userId = int.Parse(User.Identity.GetUserId());
            var pspId = (await GenericService.GetOneAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true)).Id;

            if (pspId != pspBranch.PspId)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                GenericService.Update(pspBranch);
                return RedirectToAction("Index");
            }

            // ViewBag.SubLocationId = new SelectList(await
            // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name", pspBranch.SubLocationId);
            return View(pspBranch);
        }

        [GroupCustomAuthorize(Name = "PSP-PROFILE:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var userId = int.Parse(User.Identity.GetUserId());
            var psp = await GenericService.GetFirstAsync<Psp>(x => x.UserId == userId).ConfigureAwait(true);
            if (psp == null)
            {
                return View("NotConfigured");
            }
            else
            {
                var pspBranches = (await GenericService.GetAsync<PspBranch>(
                                       x => x.PspId == psp.Id,
                                       x => x.OrderBy(y => y.Code),
                                       "CreatedByUser,ModifiedByUser,Psp,SubLocation").ConfigureAwait(true))
                    .ToPagedList(pageNum, pageSize);

                return View(pspBranches);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}