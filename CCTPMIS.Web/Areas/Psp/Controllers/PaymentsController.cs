﻿using System.Linq;

namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PaymentsController : Controller
    {
        protected readonly IEmailService EmailService;

        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public PaymentsController(IGenericService genericService, EmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PSP-PAYMENT:VIEW")]
        public async Task<ActionResult> Index(int? page, string str)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var userId = int.Parse(User.Identity.GetUserId());
            var payrollFiles = (await GenericService.GetAsync<FileCreation>(
                                    x => x.IsShared == true && x.TargetUserId == userId && x.Type.Code == "PAYMENT",
                                    x => x.OrderByDescending(y => y.Id),
                                    "FileDownloads,Type").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(payrollFiles);
        }
    }
}