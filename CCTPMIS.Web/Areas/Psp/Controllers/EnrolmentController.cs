﻿namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Interfaces;
    using CCTPMIS.Models.Admin;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class EnrolmentController : Controller
    {
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly IEmailService EmailService;

        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public EnrolmentController(IGenericService genericService, EmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
        }

        // GET: Psp/Enrolment
        [GroupCustomAuthorize(Name = "PSP-ENROLMENT:VIEW")]
        public ActionResult Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var userId = int.Parse(User.Identity.GetUserId());
            const string Spname = "GetEnrolmentPspFiles";
            const string SpParameters = "@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            }
                                    };

            var enrolmentFiles = GenericService
                .GetManyBySp<FilesViewModel>(Spname, SpParameters, parameterList.ToList())
                .ToPagedList(pageNum, pageSize);

            return View(enrolmentFiles);
        }
    }
}