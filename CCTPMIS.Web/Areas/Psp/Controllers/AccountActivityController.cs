﻿using System.Web.Mvc;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    [Authorize]
    [GroupCustomAuthorize(Name = "PSP-ACCOUNTACTIVITY:VIEW")]
    public class AccountActivityController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public AccountActivityController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: Psp/AccountActivity
        public ActionResult Index()
        {
            return View();
        }
    }
}