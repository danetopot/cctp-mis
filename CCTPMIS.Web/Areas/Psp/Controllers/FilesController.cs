﻿namespace CCTPMIS.Web.Areas.Psp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Interfaces;
    using Business.Model;
    using CCTPMIS.Models.Enrolment;
    using Microsoft.AspNet.Identity;
    using Services;

    [Authorize]
    public class FilesController : Controller
    {
        protected readonly IEmailService EmailService;

        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public FilesController(IGenericService genericService, EmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PSP-ENROLMENT:VIEW")]
        public FileResult Download(int id, int? fileDownloadId)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var file = this.GenericService.GetOne<FileCreation>(x => x.Id == id, "FileDownloads,Type");
            if (file.IsShared.HasValue)
            {
                if (file.IsShared.Value)
                {
                    if (file.Type.Code == "ENROLMENT")
                    {
                        var fileName = $"{WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]}{file.Name}";
                        Response.AppendHeader("content-disposition", "attachment; filename=" + file.Name);
                        Response.ContentType = "application/octet-stream";
                        Response.WriteFile(fileName);
                        Response.Flush();
                        Response.End();
                    }
                    else if (file.Type.Code != "PAYMENT")
                    {
                        return null;
                    }
                }
            }

            {
                if (file.TargetUserId != userId)
                {
                    return null;
                }

                var fileName = $"{WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]}{file.Name}";
                this.Response.AppendHeader("content-disposition", "attachment; filename=" + file.Name);
                this.Response.ContentType = "application/octet-stream";
                this.Response.WriteFile(fileName);
                this.Response.Flush();
                this.Response.End();
            }
            return null;
        }

        public ActionResult Verify(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var file = this.GenericService.GetOne<FileCreation>(x => x.Id == id, "FileDownloads,Type");

            if (file.IsShared.HasValue)
            {
                if (file.IsShared.Value)
                {
                    switch (file.Type.Code)
                    {
                        case "ENROLMENT":
                            // Ensure that if the file is an enrolment file and IsShared, can be seen
                            return this.View(file);

                        case "PAYMENT":
                            // ensure that the PSP can only verify their own Payment file
                            if (file.TargetUserId == userId)
                            {
                                return this.View(file);
                            }

                            break;

                        default:
                            //return error if the user tries to verify another file
                            return View("Error");
                    }
                }
            }

            return View("Error");
        }

        [HttpGet]
        [ClientErrorHandler]
        public ActionResult VerifyCheckSum(int? userId, string fileChecksum, int fileCreationId)
        {
            var userId2 = int.Parse(User.Identity.GetUserId());
            const string Sp = "BeneEnrolFileDownloaded";
            const string SpParameters = @"@FileCreationId,@FileCheckSum,@UserId";
            var spParameterList = new List<ParameterEntity>
                                      {
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCreationId",
                                                          fileCreationId)
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCheckSum",
                                                          fileChecksum.Trim())
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "UserId",
                                                          userId2)
                                              }
                                      };
            var hhEPvm = GenericService.GetOneBySp<PassWordVm>(Sp, SpParameters, spParameterList);
            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }
    }
}