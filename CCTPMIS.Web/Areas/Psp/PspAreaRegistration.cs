﻿namespace CCTPMIS.Web.Areas.Psp
{
    using System.Web.Mvc;

    public class PspAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Psp";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Psp_default",
                "Psp/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "CCTPMIS.Web.Areas.Psp.Controllers" });
        }
    }
}