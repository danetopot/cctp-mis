﻿namespace CCTPMIS.Web.Areas.Payments
{
    using System.Web.Mvc;

    public class PaymentsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Payments";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Payments_default",
                "Payments/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CCTPMIS.Web.Areas.Payments.Controllers" });
        }
    }
}