﻿using System.Web.Mvc;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    [Authorize]
    [GroupCustomAuthorize(Name = "PAYMENT CYCLE:VIEW")]
    public class DefaultController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public DefaultController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: Payments/Default
        public ActionResult Index()
        {
            return RedirectToAction("Index", "PaymentCycles");
        }
    }
}