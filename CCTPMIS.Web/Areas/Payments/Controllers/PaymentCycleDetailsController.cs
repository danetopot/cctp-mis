﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using System.Xml;
    using System.Xml.Serialization;
    using AutoMapper;
    using Business.Context;
    using Business.Interfaces;
    using Business.Model;
    using Business.Repositories;
    using CCTPMIS.Models.Enrolment;
    using CCTPMIS.Models.Payment;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PaymentCycleDetailsController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public PaymentCycleDetailsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:ENTRY")]
        public ActionResult Analysis(int? programmeId, int? paymentCycleId)
        {
            try
            {
                var spName = "GetPaymentCycleDetailOptions";
                var parameterNames = "@PaymentCycleId,@ProgrammeId";

                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            paymentCycleId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ProgrammeId",
                                                            programmeId)
                                                }
                                        };
                var results = new ApplicationDbContext().MultipleResults().With<PaymentCycleSummaryVm>()
                    .With<PaymentCycleEnrolmentGroupSummary>().Execute(spName, parameterNames, parameterList);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var errors = new List<string> { e.Message };

                return Json(errors, JsonRequestBehavior.AllowGet);
            }
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:APPROVAL")]
        public async Task<ActionResult> Approve(int? programmeId, int? paymentCycleId)
        {
            if (paymentCycleId == null && programmeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.PaymentCycleId == paymentCycleId.Value && x.ProgrammeId == programmeId,
                                       "PaymentStage,PaymentCycle,CreatedByUser,Programme,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:APPROVAL")]
        public ActionResult ApproveConfirmed(int? programmeId, int? paymentCycleId)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApprovePaymentCycleDetail";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentCycleId,
                    Key2 = programmeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PAYMENT DETAIL:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Programme Payment Cycle Approval"
                });

                TempData["MESSAGE"] = $" The Payment Cycle was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Programmes", "PaymentCycles", new { id = paymentCycleId });
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:ENTRY")]
        // GET: Payments/PaymentCycles/Create
        public async Task<ActionResult> Create(int? id)
        {
            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAsync<Programme>(x => x.IsActive).ConfigureAwait(false),
                "Id",
                "Name");

            var paymentcycles = await GenericService.GetAsync<PaymentCycle>(
                                    x => x.Id == id && x.Status.Code == "PAYMENTOPEN",
                                    null,
                                    "FinancialYear,ToMonth,FromMonth").ConfigureAwait(false);

            ViewBag.PaymentCycleId = id.HasValue
                                         ? new SelectList(paymentcycles, "Id", "CycleName", id)
                                         : new SelectList(paymentcycles, "Id", "CycleName");

            var model = new PaymentCycleDetailCreateViewModel() { };

            return View(model);
        }

        // POST: Payments/PaymentCycles/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:ENTRY")]
        public async Task<ActionResult> Create(int? id, PaymentCycleDetailCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;

                var userId = int.Parse(User.Identity.GetUserId());

                var serializer = new XmlSerializer(
                    typeof(List<PaymentCycleAddEnrolmentGroupViewModel>),
                    new XmlRootAttribute("EnrolmentGroups"));
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;

                model.PaymentCycleAddEnrolmentGroupViewModels.ToList()
                    .ForEach(a => { a.PaymentCycleId = model.PaymentCycleId; });

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, model.PaymentCycleAddEnrolmentGroupViewModels);
                    xml += sw.ToString();
                }

                var spName = "AddEditPaymentCycleDetail";
                var parameterNames =
                    "@PaymentCycleId,@ProgrammeId,@EnrolmentGroupsXML,@FilePath,@HasSupportingDoc,@UserId";

                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ProgrammeId",
                                                            model.ProgrammeId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycleId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "EnrolmentGroupsXML",
                                                            xml)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FilePath",
                                                            WebConfigurationManager
                                                                .AppSettings[
                                                                    "DIRECTORY_SHARED_FILES"])
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "HasSupportingDoc",
                                                            hasSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                },
                                        };

                var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);
                if (model.Upload != null && model.Upload.ContentLength > 0)
                {
                    model.Upload.SaveAs(
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    Key2 = model.ProgrammeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PAYMENT DETAIL:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Programme Payment Cycle Created"
                });

                TempData["MESSAGE"] = $" The Payment Cycle was  successfully created. This must be approved!";
                TempData["KEY"] = "success";
                return RedirectToAction("Programmes", "PaymentCycles", new { id = model.PaymentCycleId });
            }
            else
            {
                var desc = this.GetErrorListFromModelState(ModelState);
                // TempData["ViewModel"] = JsonConvert.SerializeObject(string.Empty);
                TempData["MESSAGE"] = "The Programme was not added to the Payment Cycle    " + desc;
                TempData["KEY"] = "danger";
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAsync<Programme>(x => x.IsActive).ConfigureAwait(false),
                "Id",
                "Name",
                model.ProgrammeId);

            var paymentcycles = await GenericService.GetAsync<PaymentCycle>(
                                    x => x.Id == model.PaymentCycleId && x.Status.Code == "PAYMENTOPEN",
                                    null,
                                    "FinancialYear,ToMonth,FromMonth").ConfigureAwait(false);

            ViewBag.PaymentCycleId = id.HasValue
                                         ? new SelectList(paymentcycles, "Id", "CycleName", id)
                                         : new SelectList(paymentcycles, "Id", "CycleName");

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:DELETION")]
        public async Task<ActionResult> Delete(int? programmeId, int? paymentCycleId)
        {
            if (paymentCycleId == null && programmeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.PaymentCycleId == paymentCycleId.Value && x.ProgrammeId == programmeId,
                                       "PaymentStage,PaymentCycle,CreatedByUser,Programme,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:DELETION")]
        public ActionResult DeleteConfirmed(int? programmeId, int? paymentCycleId)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "DeletePaymentCycleDetail";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentCycleId,
                    Key2 = programmeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PAYMENT DETAIL:DELETION",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Programme Payment Cycle Deleted"
                });

                TempData["MESSAGE"] = $" The Payment Cycle was  successfully Deleted.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Programmes", "PaymentCycles", new { id = paymentCycleId });
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:VIEW")]
        public async Task<ActionResult> Details(int? programmeId, int? paymentCycleId)
        {
            if (paymentCycleId == null && programmeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.PaymentCycleId == paymentCycleId.Value && x.ProgrammeId == programmeId,
                                       "PaymentStage,PaymentCycle,PaymentCycle.Status,CreatedByUser,ApvByUser,ModifiedByUser,Programme,Programme.PaymentFrequency,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth,PaymentEnrolmentGroups.EnrolmentGroup")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:MODIFIER")]
        public async Task<ActionResult> Edit(int? paymentCycleId, int? programmeId)
        {
            if (paymentCycleId == null || programmeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAsync<Programme>(x => x.IsActive).ConfigureAwait(false),
                "Id",
                "Name");

            var paymentcycles = await GenericService.GetAsync<PaymentCycle>(
                                    x => x.Id == paymentCycleId && x.Status.Code == "PAYMENTOPEN",
                                    null,
                                    "FinancialYear,ToMonth,FromMonth").ConfigureAwait(false);

            ViewBag.PaymentCycleId = paymentCycleId.HasValue
                                         ? new SelectList(paymentcycles, "Id", "CycleName", paymentCycleId)
                                         : new SelectList(paymentcycles, "Id", "CycleName");

            var paymentcycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                             "Programme,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                         .ConfigureAwait(false);

            if (paymentcycleDetail.FileCreationId != null)
            {
                ViewBag.File = await GenericService
                                   .GetOneAsync<FileCreation>(x => x.Id == paymentcycleDetail.FileCreationId)
                                   .ConfigureAwait(false);
            }

            ViewBag.PaymentCycle = paymentcycleDetail;
            var enrolmentGroups = (await this.GenericService.GetAsync<PaymentEnrolmentGroup>(
                                       x => x.PaymentCycleId == paymentCycleId && x.ProgrammeId == programmeId,
                                       null,
                                       "EnrolmentGroup").ConfigureAwait(false)).ToList();

            var pceModel = new List<PaymentCycleAddEnrolmentGroupViewModel>();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                .Map(enrolmentGroups, pceModel);

            var model = new PaymentCycleDetailCreateViewModel();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                .Map(paymentcycleDetail, model);
            model.PaymentCycleAddEnrolmentGroupViewModels = pceModel;
            return View(model);
        }

        // POST: Payments/PaymentCycles/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:MODIFIER")]
        public async Task<ActionResult> Edit(
            int? paymentCycleId,
            int? programmeId,
            PaymentCycleDetailCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;
                var userId = int.Parse(User.Identity.GetUserId());
                var serializer = new XmlSerializer(
                    typeof(List<PaymentCycleAddEnrolmentGroupViewModel>),
                    new XmlRootAttribute("EnrolmentGroups"));
                XmlWriterSettings settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true, };
                var xml = string.Empty;
                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    serializer.Serialize(xw, model.PaymentCycleAddEnrolmentGroupViewModels);
                    xml += sw.ToString();
                }

                var spName = "AddEditPaymentCycleDetail";
                var parameterNames =
                    "@PaymentCycleId,@ProgrammeId,@EnrolmentGroupsXML,@FilePath,@HasSupportingDoc,@UserId";

                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ProgrammeId",
                                                            model.ProgrammeId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycleId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "EnrolmentGroupsXML",
                                                            xml)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FilePath",
                                                            WebConfigurationManager
                                                                .AppSettings[
                                                                    "DIRECTORY_SHARED_FILES"])
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "HasSupportingDoc",
                                                            hasSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                },
                                        };

                var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);
                if (model.Upload != null && model.Upload.ContentLength > 0)
                {
                    model.Upload.SaveAs(
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    Key2 = model.ProgrammeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PAYMENT DETAIL:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Programme Payment Cycle Modified"
                });
                TempData["MESSAGE"] = $" The Payment Cycle was  successfully updated. This must be approved!";
                TempData["KEY"] = "success";
                return RedirectToAction("Programmes", "PaymentCycles", new { id = model.PaymentCycleId });
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAsync<Programme>(x => x.IsActive).ConfigureAwait(false),
                "Id",
                "Name");

            var paymentcycles = await GenericService.GetAsync<PaymentCycle>(
                                    x => x.Id == paymentCycleId && x.Status.Code == "PAYMENTOPEN",
                                    null,
                                    "FinancialYear,ToMonth,FromMonth").ConfigureAwait(false);

            ViewBag.PaymentCycleId = paymentCycleId.HasValue
                                         ? new SelectList(paymentcycles, "Id", "CycleName", paymentCycleId)
                                         : new SelectList(paymentcycles, "Id", "CycleName");

            var paymentcycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                             "Programme,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                         .ConfigureAwait(false);

            if (paymentcycleDetail.FileCreationId != null)
            {
                ViewBag.File = await GenericService
                                   .GetOneAsync<FileCreation>(x => x.Id == paymentcycleDetail.FileCreationId)
                                   .ConfigureAwait(false);
            }

            ViewBag.PaymentCycle = paymentcycleDetail;
            var enrolmentGroups = (await this.GenericService.GetAsync<PaymentEnrolmentGroup>(
                                       x => x.PaymentCycleId == paymentCycleId && x.ProgrammeId == programmeId,
                                       null,
                                       "EnrolmentGroup").ConfigureAwait(false)).ToList();

            var pceModel = new List<PaymentCycleAddEnrolmentGroupViewModel>();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                .Map(enrolmentGroups, pceModel);

            model.PaymentCycleAddEnrolmentGroupViewModels = pceModel;
            return View(model);
        }

        // GET: Payments/PaymentCycleDetails
        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var payrollCodes =
                "PAYROLL,PAYROLLVER,PAYROLLAPV,PAYROLLEX,PAYROLLEXCONFPOST,PAYROLLPOST,PAYMENTRECON,PAYMENTRECONAPV,CLOSED";
            var paymentCycle = (await GenericService.GetAsync<PaymentCycleDetail>(
                                        x => payrollCodes.Contains(x.PaymentStage.Code + ","),
                                        x => x.OrderByDescending(y => y.PaymentCycleId),
                                        includeProperties:
                                        "CreatedByUser,PaymentStage,Programme,PaymentCycle.FinancialYear,PaymentCycle.ToMonth,PaymentCycle.FromMonth")
                                    .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
    }
}