﻿using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using AutoMapper;
    using Business.Context;
    using Business.Interfaces;
    using Business.Model;
    using CCTPMIS.Models.Payment;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PaymentCyclesController : Controller
    {
        public IGenericService GenericService;

        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public PaymentCyclesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                                            .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
                              .ConfigureAwait(false)).ToList();
            ViewBag.FromMonthId = new SelectList(months, "Id", "Description");
            ViewBag.ToMonthId = new SelectList(months, "Id", "Description");

            try
            {
                var spName = "GetPaymentCycleOptions";
                var model = GenericService.GetOneBySp<PaymentCycleCreateViewModel>(spName);

                return View(model);
            }
            catch (Exception e)
            {
                // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                TempData["MESSAGE"] = $" You cannot create a  new Payment Cycle  " + e.Message;
                TempData["KEY"] = "danger";

                return RedirectToAction("Index");
            }
        }

        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:MODIFIER")]
        public async Task<ActionResult> Skip(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                    x => x.Id == id.Value,
                    "Status,CreatedByUser,FinancialYear,ModifiedByUser,FromMonth,ToMonth,PaymentCycleDetails,PaymentCycleDetails.CreatedByUser,PaymentCycleDetails.PaymentStage,,PaymentCycleDetails.Programme,PaymentAccountActivityMonths.Month")
                .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Skip")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:MODIFIER")]
        public ActionResult SkipConfirmed(int? id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "SkipPayroll";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                TempData["MESSAGE"] = $" The Payment Cycle was  successfully Skipped.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index", "PaymentCycles", new { id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:ENTRY")]
        public async Task<ActionResult> Create(PaymentCycleCreateViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var spName = "AddEditPaymentCycle";
                var parameterNames = "@Id,@Description,@FinancialYearId,@FromMonthId,@ToMonthId,@FromMonthActivityDate,@ToMonthActivityDate,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Id",
                                                            DBNull.Value)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Description",
                                                            model.Description)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FinancialYearId",
                                                            model.FinancialYearId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FromMonthId",
                                                            model.FromMonthId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ToMonthId",
                                                            model.ToMonthId)
                                                },

                                            new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "FromMonthActivityDate",
                                                        model.StartDate)
                                            }, new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ToMonthActivityDate",
                                                        model.EndDate)
                                            },

                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "PaymentCycle",
                        ModuleRightCode = "PAYMENT CYCLE:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Payment Cycle Created"
                    });
                    TempData["MESSAGE"] = $" The Payment Cycle was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                    TempData["MESSAGE"] = $" The Payment Cycle was not created  successfully .  " + e.Message;
                    TempData["KEY"] = "danger";
                }
            }

            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
                .ConfigureAwait(false)).ToList();
            ViewBag.FromMonthId = new SelectList(months, "Id", "Description");
            ViewBag.ToMonthId = new SelectList(months, "Id", "Description");
            return View(model);
        }

        // GET: Payments/PaymentCycles/Details/5
        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,FinancialYear,ModifiedByUser,FromMonth,ToMonth,PaymentCycleDetails,PaymentCycleDetails.CreatedByUser,PaymentCycleDetails.PaymentStage,,PaymentCycleDetails.Programme,PaymentAccountActivityMonths.Month")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                   x => x.Id == id.Value,
                                   "FromMonth,ToMonth,FinancialYear,Status").ConfigureAwait(false);

            var model = new PaymentCycleCreateViewModel();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(paymentCycle, model);

            model.ToMonth = paymentCycle.ToMonth.Description;
            model.FromMonth = paymentCycle.FromMonth.Description;
            model.FinancialYear = paymentCycle.FinancialYear.Description;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:MODIFIER")]
        public ActionResult Edit(int id, PaymentCycleCreateViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var spName = "AddEditPaymentCycle";
                var parameterNames = "@Id,@Description,@FinancialYearId,@FromMonthId,@ToMonthId,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Id",
                                                            model.Id)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Description",
                                                            model.Description)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FinancialYearId",
                                                            model.FinancialYearId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FromMonthId",
                                                            model.FromMonthId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ToMonthId",
                                                            model.ToMonthId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "PaymentCycle",
                    ModuleRightCode = "PAYMENT CYCLE:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Payment Cycle Modified"
                });
                // TempData["ViewModel"] = JsonConvert.SerializeObject(newModel);
                TempData["MESSAGE"] = $" The Payment Cycle was  successfully Edited.  ";
                TempData["KEY"] = "success";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["MESSAGE"] = $" The Payment Cycle was  not Updated.  " + GetErrorListFromModelState(ModelState);
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PAYMENT CYCLE:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var paymentCycles = (await GenericService.GetAsync<PaymentCycle>(
                                         null,
                                         x => x.OrderByDescending(y => y.Id),
                                         "Status,FinancialYear,CreatedByUser,ModifiedByUser,FromMonth,ToMonth,PaymentCycleDetails.PaymentStage")
                                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(paymentCycles);
        }

        [GroupCustomAuthorize(Name = "PAYMENT DETAIL:VIEW")]
        public async Task<ActionResult> Programmes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                       x => x.Id == id.Value,
                                       "PaymentCycleDetails,PaymentCycleDetails.CreatedByUser,PaymentCycleDetails.PaymentStage,PaymentCycleDetails.Programme")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}