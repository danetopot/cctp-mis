﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Context;
    using Business.Interfaces;
    using Business.Model;
    using CCTPMIS.Models.Enrolment;
    using CCTPMIS.Models.Payment;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class FundsRequestsController : Controller
    {
        private readonly IEmailService EmailService;

        private readonly IGenericService GenericService;

        private readonly IPdfService PdfService;
        private readonly ILogService LogService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public FundsRequestsController(IGenericService genericService, EmailService emailService, PdfService pdfService, ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            PdfService = pdfService;
            LogService = logService;
        }

        [ActionName("Approve")]
        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:APPROVAL")]
        public async Task<ActionResult> ApproveFundsRequest(int id)
        {
            var frRequest = await GenericService.GetOneAsync<FundsRequest>(
                                    x => x.Id == id,
                                    "PaymentCycle.ToMonth,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,FundsRequestDetails.Psp,FundsRequestDetails.Programme,PaymentCycle.Status,FileCreation")
                                .ConfigureAwait(true);

            var model = new FundsRequestApvViewModel() {
                Id = frRequest.Id,
                PaymentCycleId = frRequest.PaymentCycleId,
            };

            return View(model);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:APPROVAL")]
        public ActionResult ApproveFundsRequestConfirmed(FundsRequestApvViewModel model)
        {
            if (ModelState.IsValid)
            {
                var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;

                var userId = int.Parse(User.Identity.GetUserId());
                var storedProcedure = "ApproveFundsRequest";
                var parameterNames = "@PaymentCycleId,@FilePath,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycleId),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FilePath",
                                                            WebConfigurationManager
                                                                .AppSettings[
                                                                    "DIRECTORY_SHARED_FILES"])
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId),
                                                },
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNames, parameterList);

                    if (model.Upload != null && model.Upload.ContentLength > 0)
                    {
                        model.Upload.SaveAs(
                            WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.FileName);
                    }
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "FundsRequest",
                        ModuleRightCode = "REQUEST FOR FUNDS:APPROVAL",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Funds Request Approval Complete"
                    });
                    TempData["Title"] = "Funds Request Approval Complete";
                    TempData["MESSAGE"] =
                        $" Funds Request Approval  was successful. Kindly proceed to Generate the Payroll";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["Title"] = "Error Updating Funds Request Approval. ";
                    TempData["MESSAGE"] = e.Message;
                    TempData["KEY"] = "danger";
                }
            }
            else
            {
                TempData["Title"] = "Error Updating Funds Request Approval. ";
                TempData["MESSAGE"] = GetErrorListFromModelState(ModelState);
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            var fundsReq = await GenericService.GetOneAsync<FundsRequest>(
                                   x => x.Id == id,
                                   "PaymentCycle.ToMonth,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,FundsRequestDetails.Psp,FundsRequestDetails.Programme,PaymentCycle.Status,PaymentCycle.PaymentCycleDetails.FundsRequestByUser,PaymentCycle.PaymentCycleDetails.FundsRequestApvByUser")
                               .ConfigureAwait(true);

            var storedProcedure = "GetRequestForFunds";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        fundsReq.PaymentCycleId),
                                            },
                                    };

            var listModel = GenericService.GetManyBySp<ReqForFundsListVM>(
                storedProcedure,
                parameterNames,
                parameterList);

            var model = new RequestForFunds { FundsRequest = fundsReq, ReqForFundsVMs = listModel.ToList() };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:MODIFIER")]
        public ActionResult GenerateFundsRequestLetter(int id, string name)
        {
            PdfService.ExportToPDF(
                $"Payments/FundsRequests/ProcessFundsRequest/{id}",
                $"REQFUNDS_{name.Replace(" ", "_")}",
                WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]);
            try
            {
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var paymentCycles = (await GenericService.GetAsync<FundsRequest>(
                                         null,
                                         x => x.OrderByDescending(y => y.PaymentCycleId),
                                         "PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth,PaymentCycle.PaymentCycleDetails,PaymentCycle.Status,FundsRequestDetails,FileCreation")
                                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            var ddpdata = GenericService.GetManyBySp<SelectListA>("GetPaymentCyclesDueForFundsRequest");
            ViewBag.PaymentCycleId = new SelectList(ddpdata, "Id", "Name");
            return View(paymentCycles);
        }

        [AllowAnonymous]
        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:MODIFIER")]
        public async Task<ActionResult> ProcessFundsRequest(int? id)
        {
            var fundsReq = await GenericService.GetOneAsync<FundsRequest>(
                                   x => x.Id == id,
                                   "PaymentCycle.ToMonth,PaymentCycle.FinancialYear,PaymentCycle.FromMonth,FundsRequestDetails.Psp,FundsRequestDetails.Programme,PaymentCycle.Status")
                               .ConfigureAwait(true);

            var storedProcedure = "GetRequestForFunds";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        fundsReq.PaymentCycleId),
                                            },
                                    };

            var listModel = GenericService.GetManyBySp<ReqForFundsListVM>(
                storedProcedure,
                parameterNames,
                parameterList);

            LogService.AuditTrail(new AuditTrailVm {
                UserId = $"{User.Identity.GetUserId()}",
                Key1 = id,
                TableName = "FundsRequest",
                ModuleRightCode = "REQUEST FOR FUNDS:MODIFIER",
                Record = $"{JsonConvert.SerializeObject(listModel)}",
                WasSuccessful = true,
                Description = "Funds Request Process Complete"
            });

            var model = new RequestForFunds { FundsRequest = fundsReq, ReqForFundsVMs = listModel.ToList() };

            return View(model);
        }

        [ActionName("Run")]
        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:MODIFIER")]
        public async Task<ActionResult> RunFundsRequest(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycleId = id;

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                       x => x.Id == id,
                                       "FinancialYear,FromMonth,ToMonth,Status,PaymentCycleDetails.Programme,PaymentCycleDetails.PaymentStage")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Run")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:MODIFIER")]
        public ActionResult RunFundsRequestConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ProcessFundsRequest";
            var parameterNames = "@PaymentCycleId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        id),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "FundsRequest",
                    ModuleRightCode = "REQUEST FOR FUNDS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Funds Request Generation Complete"
                });

                TempData["Title"] = "Success";
                TempData["MESSAGE"] = $" The Generating Funds Request was completed successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Generating Funds Request";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
    }
}