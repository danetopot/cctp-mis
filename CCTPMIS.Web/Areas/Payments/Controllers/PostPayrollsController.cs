﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    [Authorize]
    public class PostPayrollsController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public PostPayrollsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        #region View Post Payroll

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var paymentCycles = (await GenericService.GetAsync<PaymentCycle>(
                                        x => x.Payrolls.Any(),
                                         x => x.OrderByDescending(y => y.Id),
                                         "Status,FinancialYear,CreatedByUser,FromMonth,ToMonth,PaymentCycleDetails.PaymentStage")
                                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(paymentCycles);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                             x => x.Id == id.Value, "Status,FinancialYear,CreatedByUser,FromMonth,ToMonth")
                                         .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            var spName = "GetPayrollTrxSummary";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        id)
                                            },
                                    };
            var pspAnalyses = GenericService.GetManyBySp<PspAnalysis>(spName, parameterNames, parameterList);
            var model = new CreditReportViewModel {
                PspAnalysis = pspAnalyses.ToList(),
                PaymentCycle = paymentCycle
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> Exceptions(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                    x => x.Id == id.Value, "Status,FinancialYear,CreatedByUser,FromMonth,ToMonth")
                .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            var spName = "GetPostPayrollSummary";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentCycleId",
                            id)
                },
            };

            var postPayrollAnalysis = GenericService.GetOneBySp<PostPayrollSummaryViewModel>(spName, parameterNames, parameterList);

            var model = new CreditReportViewModel {
                PostPayrollSummaryViewModel = postPayrollAnalysis,
                PaymentCycle = paymentCycle
            };
            return View(model);
        }

        #endregion View Post Payroll

        #region Close Post Payroll

        [GroupCustomAuthorize(Name = "POST-PAYROLL:APPROVAL")]
        public async Task<ActionResult> ClosePostPayroll(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                    x => x.Id == id.Value, "Status,FinancialYear,CreatedByUser,FromMonth,ToMonth")
                .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            var spName = "GetPayrollTrxSummary ";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ ParameterTuple = new Tuple<string, object>( "PaymentCycleId", id)}
            };

            var pspAnalyses = GenericService.GetManyBySp<PspAnalysis>(spName, parameterNames, parameterList);

            var model = new CreditReportViewModel {
                PspAnalysis = pspAnalyses.ToList(),
                PaymentCycle = paymentCycle
            };
            return View(model);
        }

        [HttpPost, ActionName("ClosePostPayroll")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "POST-PAYROLL:APPROVAL")]
        public ActionResult ClosePostPayrollConfirmed(CreditReportViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ApprovePostPayroll";
            var parameterNames = "@PaymentCycleId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        model.Id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };
            try
            {
                var newModel = GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    TableName = "PaymentCycle",
                    ModuleRightCode = "POST-PAYROLL:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Post Payroll Approval Complete"
                });
                TempData["Title"] = "Post Payroll  Approval Complete";
                TempData["MESSAGE"] = $" The Post Payroll was  approved successfully. \n This Payment Cycle has been Closed. \n New Payment Cycles can now be created";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Post Payroll  Approval Failed";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Close Post Payroll

        #region Exceptions File Generate

        [GroupCustomAuthorize(Name = "POST-PAYROLL:APPROVAL")]
        public async Task<ActionResult> GenerateExceptionsFile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                    x => x.Id == id.Value, "Status,FinancialYear,CreatedByUser,FromMonth,ToMonth")
                .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            var spName = "GetPayrollTrxSummary ";
            var parameterNames = "@PaymentCycleId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentCycleId",
                            id)
                },
            };

            var pspAnalyses = GenericService.GetManyBySp<PspAnalysis>(spName, parameterNames, parameterList);

            var model = new CreditReportViewModel {
                PspAnalysis = pspAnalyses.ToList(),
                PaymentCycle = paymentCycle
            };
            return View(model);
        }

        [HttpPost, ActionName("GenerateExceptionsFile")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "POST-PAYROLL:APPROVAL")]
        public async Task<ActionResult> GenerateExceptionsFileConfirmed(PrepayrollViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GeneratePostpayrollExceptionsFile";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@PaymentCycleId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
                                           {
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "PaymentCycleId",
                                                               model
                                                                   .PaymentCycleId),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "FilePath",
                                                               WebConfigurationManager
                                                                   .AppSettings[
                                                                       "DIRECTORY_SHARED_FILES"]),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBServer",
                                                               connection
                                                                   .DataSource),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBName",
                                                               connection
                                                                   .InitialCatalog),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBUser",
                                                               connection.UserID),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBPassword",
                                                               connection
                                                                   .Password),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "UserId",
                                                               userId),
                                                   },
                                           };
            try
            {
                var newModelGen = GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                               .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                               .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    TableName = "PaymentCycle",
                    ModuleRightCode = "POST-PAYROLL:APPROVAL",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Post Payroll Exceptions generated"
                });

                TempData["Title"] = "Post Payroll Exceptions Files Generation Complete";
                TempData["MESSAGE"] = $" Post Payroll Exceptions Files generations was completed successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Payroll Files Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Exceptions File Generate

        #region Exceptions List

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> IneligibleExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollIneligible>(
                                x => x.PaymentCycleId == id && x.Actioned && x.Payment != null,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollIneligiblesList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle = id
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidAccExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollInvalidPaymentAccount>(
                                 x => x.PaymentCycleId == id && x.Actioned && x.Payment != null,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidPaymentAccountsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle = id
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidCardExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollInvalidPaymentCard>(
                                 x => x.PaymentCycleId == id && x.Actioned && x.Payment != null,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidPaymentCardsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle = id
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidIdExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollInvalidId>(
                                 x => x.PaymentCycleId == id && x.Actioned && x.Payment != null,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidIdsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle = id
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> SuspiciousExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollSuspicious>(
                                 x => x.PaymentCycleId == id && x.Actioned && x.Payment != null && x.Household.HouseholdMembers.Any(z => z.Relationship.Code == "BENEFICIARY"),
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollSuspiciousList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                            = id,
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "POST-PAYROLL:VIEW")]
        public async Task<ActionResult> DuplicateIdExceptions(int? page, int id)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollDuplicateId>(
                    x => x.PaymentCycleId == id && x.Actioned && x.Payment != null && x.Household.HouseholdMembers.Any(z => z.Relationship.Code == "BENEFICIARY" && z.PersonId == x.PersonId),
                    x => x.OrderBy(y => y.HhId),
                    "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household,Person")
                .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollDuplicateIdList = data,
                PrepayrollExceptionsFilterViewModel =
                    new PrepayrollExceptionsFilterViewModel {
                        PaymentCycle
                            = id,
                    }
            };

            return View(model);
        }

        // ExceptionDetails
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> ExceptionDetails(
            string id,
            int programme,
            int paymentCycle,
            int? hhId,
            int? personId,
            int exceptionTypeId)
        {
            var model = new PrepayrollExceptionsDetailsViewModel {
                PrepayrollSingleActionViewModel =
                                    new PrepayrollSingleActionViewModel {
                                        Programme = programme,
                                        PaymentCycle = paymentCycle,
                                        PersonId = personId,
                                        HhId = hhId,
                                        ExceptionTypeId = exceptionTypeId,
                                        Id = id
                                    }
            };

            if (!string.IsNullOrEmpty(id))
            {
                switch (id.ToUpper())
                {
                    case "DUPLICATEID":
                        model.PrepayrollDuplicateId = await this.GenericService.GetOneAsync<PrepayrollDuplicateId>(
                                                               x => x.PersonId == personId
                                                                    && x.HhId == hhId
                                                                    && x.ProgrammeId == programme
                                                                    && x.Actioned
                                                                    && x.PaymentCycleId == paymentCycle,
                                                               "Person,HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                           .ConfigureAwait(false);
                        break;

                    case "INVALIDID":
                        model.PrepayrollInvalidId = await this.GenericService.GetOneAsync<PrepayrollInvalidId>(
                                                             x => x.PersonId == personId
                                                                  && x.Actioned
                                                                  && x.HhId == hhId
                                                                  && x.ProgrammeId == programme
                                                                  && x.PaymentCycleId == paymentCycle,
                                "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                         .ConfigureAwait(false);
                        break;

                    case "INELIGIBLE":
                        model.PrepayrollIneligible = await this.GenericService.GetOneAsync<PrepayrollIneligible>(
                                                              x => x.HhId == hhId
                                                                   && x.Actioned
                                                                   && x.ProgrammeId == programme
                                                                   && x.PaymentCycleId == paymentCycle,
                                "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                          .ConfigureAwait(false);
                        break;

                    case "SUSPICIOUS":
                        model.PrepayrollSuspicious = await this.GenericService.GetOneAsync<PrepayrollSuspicious>(
                                                              x => x.HhId == hhId
                                                                   && x.Actioned
                                                                   && x.ProgrammeId == programme
                                                                   && x.PaymentCycleId == paymentCycle,
                                "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                          .ConfigureAwait(false);
                        break;

                    case "INVALIDCARD":
                        model.PrepayrollInvalidPaymentCard = await this.GenericService
                                                                  .GetOneAsync<PrepayrollInvalidPaymentCard>(
                                                                      x => x.HhId == hhId
                                                                           && x.Actioned
                                                                           && x.ProgrammeId == programme
                                                                           && x.PaymentCycleId == paymentCycle,
                                "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                                  .ConfigureAwait(false);
                        break;

                    case "INVALIDACC":
                        model.PrepayrollInvalidPaymentAccount = await this.GenericService
                                                                     .GetOneAsync<PrepayrollInvalidPaymentAccount>(
                                                                         x => x.HhId == hhId
                                                                              && x.Actioned
                                                                              && x.ProgrammeId == programme
                                                                              && x.PaymentCycleId == paymentCycle,
                                "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                                     .ConfigureAwait(false);
                        break;

                    default:

                        break;
                }
            }

            return View(model);
        }

        #endregion Exceptions List
    }
}