﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using System.Xml;
    using System.Xml.Serialization;
    using Business.Context;
    using Business.Interfaces;
    using Business.Model;
    using CCTPMIS.Models.Payment;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PrepayrollsController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public PrepayrollsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:APPROVAL")]
        public async Task<ActionResult> ApprovePrepayroll(int programmeId, int paymentCycleId)
        {
            var paymentCycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.PaymentCycleId == paymentCycleId && x.ProgrammeId == programmeId,
                                             "PaymentStage,PaymentCycle.FinancialYear,PaymentCycle.Status,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                         .ConfigureAwait(true);

            var spName = "GetPrepayrollSummary";
            var parameterNames = "@PaymentCycleId,@ProgrammeId";

            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId)
                                            },
                                    };

            var prepayrollAuditViewModel =
                GenericService.GetOneBySp<PrepayrollAuditViewModel>(spName, parameterNames, parameterList);

            var model = new PrepayrollViewModel {
                PaymentCycleDetail = paymentCycleDetail,
                PrepayrollAuditViewModel = prepayrollAuditViewModel
            };
            return View(model);
        }

        // POST: Payments/Prepayrolls/ApprovePrepayroll/
        [HttpPost, ActionName("ApprovePrepayroll")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:APPROVAL")]
        public ActionResult ApprovePrepayrollConfirmed(PrepayrollViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ApprovePrepayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        model.PaymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        model.ProgrammeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    Key2 = model.ProgrammeId,
                    TableName = "PaymentCycle",
                    ModuleRightCode = "PRE-PAYROLL:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Approve PrePayroll"
                });

                TempData["Title"] = "Draft Pre-payroll Approval Complete";
                TempData["MESSAGE"] =
                    $" The Draft Pre-payroll was  approved successfully. Kindly proceed to action necessary exceptions";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Updating the Draft Pre-payroll. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:ACTION")]
        public async Task<ActionResult> BulkActioning(string id, int programme, int paymentCycle)
        {
            var data = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Exception Type")
                           .ConfigureAwait(true);
            var data2 = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Enrolment Group")
                            .ConfigureAwait(true);
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Category",
                                                        id)
                                            },
                                    };

            var data3 = this.GenericService.GetManyBySp<SelectExceptionVm>(
                "GetActionableExceptionTypes",
                "@Category",
                parameterList);

            ViewBag.ExceptionTypeId = new SelectList(data3, "ExceptionTypeId", "ExceptionDesc");
            ViewBag.EnrolmentGroupId = new SelectList(data2, "Id", "Description");
            var model = new PrepayrollExceptionsDetailsViewModel {
                PrepayrollBulkActionViewModel =
                                    new PrepayrollBulkActionViewModel {
                                        Programme
                                                = programme,
                                        PaymentCycle
                                                = paymentCycle,
                                    }
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:ACTION")]
        public ActionResult BulkActioning(PrepayrollExceptionsDetailsViewModel parentmodel)
        {
            var model = parentmodel.PrepayrollBulkActionViewModel;
            var prepayrollExceptionsXmlModel = new List<PrepayrollException>();
            var exceptionTypeXmlModel = new List<ExceptionType>();
            var enrolmentGroupIdsXmlModel = new List<EnrolmentGroupModel>();

            var exceptionTypeXml = string.Empty;
            var enrolmentGroupIdsXml = string.Empty;
            var prepayrollExceptionXml = string.Empty;
            var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };

            if (ModelState.IsValid)
            {
                var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;
                var userId = int.Parse(User.Identity.GetUserId());

                var prepayrollExceptionsSerializer = new XmlSerializer(
                   typeof(List<PrepayrollException>),
                   new XmlRootAttribute("PrepayrollExceptions"));

                prepayrollExceptionsXmlModel.Add(
                    new PrepayrollException {
                        HhId = model.HhId,
                        PaymentCycleId = model.PaymentCycle,
                        ProgrammeId = model.Programme,
                        PersonId = model.PersonId
                    });

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, prepayrollExceptionsXmlModel);
                    prepayrollExceptionXml += sw.ToString();
                }

                exceptionTypeXmlModel.AddRange(
                   model.ExceptionTypeId.Select(item => new ExceptionType { ExceptionTypeId = item }));

                var exceptionTypesSerializer = new XmlSerializer(
                    typeof(List<ExceptionType>),
                    new XmlRootAttribute("ExceptionTypes"));

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    exceptionTypesSerializer.Serialize(xw, exceptionTypeXmlModel);
                    exceptionTypeXml += sw.ToString();
                }

                var enrolmentGroupIdsSerializer = new XmlSerializer(
                    typeof(List<EnrolmentGroupModel>),
                    new XmlRootAttribute("EnrolmentGroups"));
                enrolmentGroupIdsXmlModel.AddRange(
                    model.EnrolmentGroupId.Select(item => new EnrolmentGroupModel { EnrolmentGroupId = item }));

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    enrolmentGroupIdsSerializer.Serialize(xw, enrolmentGroupIdsXmlModel);
                    enrolmentGroupIdsXml += sw.ToString();
                }

                var storedProcedure = "ProcessPrepayrollExceptionsAction";
                var parameterNames =
                    "@PaymentCycleId,@ProgrammeId,@ExceptionTypeIdsXML,@Notes,@EnrolmentGroupIdsXML,@PrepayrollExceptionsXML,@ReplacePreviousActioning,@FilePath,@HasSupportingDoc,@RemovePreviousSupportingDoc,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycle)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ProgrammeId",
                                                            model.Programme)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ExceptionTypeIdsXML",
                                                            exceptionTypeXml)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Notes",
                                                            model.Notes)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "EnrolmentGroupIdsXML ",
                                                            enrolmentGroupIdsXml),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PrepayrollExceptionsXML",
                                                            prepayrollExceptionXml),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ReplacePreviousActioning",
                                                            model
                                                                .ReplacePreviousActioning),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FilePath",
                                                            WebConfigurationManager
                                                                .AppSettings[
                                                                    "DIRECTORY_SHARED_FILES"])
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "HasSupportingDoc",
                                                            hasSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "RemovePreviousSupportingDoc",
                                                            model
                                                                .RemovePreviousSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };
                try
                {
                    var newModel = GenericService.GetOneBySp<ProcessPrePayrollExceptionsActionVm>(
                        storedProcedure,
                        parameterNames,
                        parameterList);
                    if (model.Upload != null && model.Upload.ContentLength > 0)
                    {
                        model.Upload.SaveAs(
                            WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                    }

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.PaymentCycle,
                        Key2 = model.Programme,
                        TableName = "PaymentCycle",
                        ModuleRightCode = "PRE-PAYROLL:ACTION",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = " PrePayroll Bulk Actioning"
                    });

                    TempData["Title"] = "Exception successfully Actioned ";
                    TempData["MESSAGE"] =
                        $" Exception successfully Actioned Kindly proceed to action other exceptions. Once you are through, Finalize the Prepayroll";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["Title"] = "Error Actioning the Exception  ";
                    TempData["MESSAGE"] = e.Message;
                    TempData["KEY"] = "danger";
                }

                return RedirectToAction(
                    "Summary",
                    new { paymentcycle = model.PaymentCycle, programme = model.Programme });
            }
            else
            {
                TempData["Title"] = "Error Actioning the Exception  ";
                TempData["MESSAGE"] = GetErrorListFromModelState(ModelState);
                TempData["KEY"] = "danger";
                return RedirectToAction(
                    "Summary",
                    new { paymentcycle = model.PaymentCycle, programme = model.Programme });
            }
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> Details(int? programme, int? paymentcycle)
        {
            if (programme == null || paymentcycle == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.PaymentCycleId == paymentcycle.Value && x.ProgrammeId == programme,
                                             "PaymentStage,PaymentCycle.FinancialYear,PaymentCycle.Status,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth,ApvByUser,ModifiedByUser")
                                         .ConfigureAwait(true);

            var spName = "GetPrepayrollSummary";
            var parameterNames = "@PaymentCycleId,@ProgrammeId";
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentcycle)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programme)
                                            },
                                    };

            var prepayrollAuditViewModel =
                GenericService.GetOneBySp<PrepayrollAuditViewModel>(spName, parameterNames, parameterList);

            if (paymentCycleDetail == null)
            {
                return HttpNotFound();
            }

            var model = new PrepayrollViewModel {
                PaymentCycleDetail = paymentCycleDetail,
                PrepayrollAuditViewModel = prepayrollAuditViewModel
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> DuplicateIdExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollDuplicateId>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household,Person")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollDuplicateIdList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        // ExceptionDetails
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> ExceptionDetails(
            string id,
            int programme,
            int paymentCycle,
            int? hhId,
            int? personId,
            int exceptionTypeId)
        {
            var model = new PrepayrollExceptionsDetailsViewModel {
                PrepayrollSingleActionViewModel =
                                    new PrepayrollSingleActionViewModel {
                                        Programme
                                                = programme,
                                        PaymentCycle
                                                = paymentCycle,
                                        PersonId
                                                = personId,
                                        HhId =
                                                hhId,
                                        ExceptionTypeId
                                                = exceptionTypeId,
                                        Id = id
                                    }
            };

            if (!string.IsNullOrEmpty(id))
            {
                switch (id.ToUpper())
                {
                    case "DUPLICATEID":
                        model.PrepayrollDuplicateId = await this.GenericService.GetOneAsync<PrepayrollDuplicateId>(
                                                               x => x.PersonId == personId && x.HhId == hhId
                                                                                           && x.ProgrammeId == programme
                                                                                           && x.PaymentCycleId
                                                                                           == paymentCycle,
                                                               "Person,HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                           .ConfigureAwait(false);
                        break;

                    case "INVALIDID":
                        model.PrepayrollInvalidId = await this.GenericService.GetOneAsync<PrepayrollInvalidId>(
                                                             x => x.PersonId == personId && x.HhId == hhId
                                                                                         && x.ProgrammeId == programme
                                                                                         && x.PaymentCycleId
                                                                                         == paymentCycle,
                                                             "Person,HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                         .ConfigureAwait(false);
                        break;

                    case "INELIGIBLE":
                        model.PrepayrollIneligible = await this.GenericService.GetOneAsync<PrepayrollIneligible>(
                                                              x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                  && x.PaymentCycleId == paymentCycle,
                                                              "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                          .ConfigureAwait(false);
                        break;

                    case "SUSPICIOUS":
                        model.PrepayrollSuspicious = await this.GenericService.GetOneAsync<PrepayrollSuspicious>(
                                                              x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                  && x.PaymentCycleId == paymentCycle,
                                                              "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                          .ConfigureAwait(false);
                        break;

                    case "INVALIDCARD":
                        model.PrepayrollInvalidPaymentCard = await this.GenericService
                                                                  .GetOneAsync<PrepayrollInvalidPaymentCard>(
                                                                      x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                          && x.PaymentCycleId
                                                                                          == paymentCycle,
                                                                      "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                                  .ConfigureAwait(false);
                        break;

                    case "INVALIDACC":
                        model.PrepayrollInvalidPaymentAccount = await this.GenericService
                                                                     .GetOneAsync<PrepayrollInvalidPaymentAccount>(
                                                                         x => x.HhId == hhId
                                                                              && x.ProgrammeId == programme
                                                                              && x.PaymentCycleId == paymentCycle,
                                                                         "HouseHold,ExceptionType,Programme,PaymentCycle,Household.HouseholdMembers.Relationship,Household.HouseholdMembers.Person")
                                                                     .ConfigureAwait(false);
                        break;

                    default:

                        break;
                }
            }

            return View(model);
        }

        [ActionName("Final")]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:FINALIZE")]
        public async Task<ActionResult> FinalPrepayroll(int programmeId, int paymentCycleId)
        {
            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Final")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:FINALIZE")]
        public ActionResult FinalPrepayrollConfirmed(int ProgrammeId, int PaymentCycleId)
        {
            // var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            // SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder(conString);
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "FinalizePrepayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        PaymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        ProgrammeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = PaymentCycleId,
                    Key2 = ProgrammeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PRE-PAYROLL:FINALIZE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = " PrePayroll Bulk Actioning"
                });

                TempData["Title"] = "Draft Pre-payroll Approval Complete";
                TempData["MESSAGE"] =
                    $" The Draft Pre-payroll was  approved successfully. Kindly proceed to action necessary exceptions";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Updating the Draft Pre-payroll. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [ActionName("GenerateExceptionsFile")]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:RUN & GENERATE EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptionsFile(int programmeId, int paymentCycleId)
        {
            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("GenerateExceptionsFile")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:RUN & GENERATE EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptionsFileConfirmed(int ProgrammeId, int PaymentCycleId)
        {
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var userId = int.Parse(User.Identity.GetUserId());

            var storedProcedure = "GeneratePrepayrollExceptionsFile";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        PaymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        ProgrammeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "FilePath",
                                                        WebConfigurationManager
                                                            .AppSettings[
                                                                "DIRECTORY_SHARED_FILES"]),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "DBServer",
                                                        connection.DataSource),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "DBName",
                                                        connection
                                                            .InitialCatalog),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "DBUser",
                                                        connection.UserID),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "DBPassword",
                                                        connection.Password),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<FIleCreationIdVm>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                var fileService = new FileService();
                var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == newModel.FileCreationId)
                               .ConfigureAwait(false);

                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = PaymentCycleId,
                    Key2 = ProgrammeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PRE-PAYROLL:RUN & GENERATE EXCEPTIONS",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = " PrePayroll Bulk Run and Generate Exceptions"
                });

                TempData["Title"] = "Prepayroll Exceptions File generated Successfully";
                TempData["MESSAGE"] = $"Prepayroll Exceptions File generated  successfully. Kindly proceed to Download";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Generating Prepayroll Exceptions File.";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Summary", new { programme = ProgrammeId, paymentcycle = PaymentCycleId });
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var paymentStageCodes =
                "CLOSED,FUNDSREQUEST,FUNDSREQUESTAPV,PAYMENTRECON,PAYMENTRECONAPV,PAYROLL,PAYROLLAPV,PAYROLLEX,PAYROLLEXCONF,PAYROLLVER,POSTPAYROLL,POSTPAYROLLAPV,PREPAYROLLAPV,PREPAYROLLFINAL,";
            var paymentCycles = (await GenericService.GetAsync<PaymentCycleDetail>(
                                         x => paymentStageCodes.Contains(x.PaymentStage.Code + ","),
                                         x => x.OrderByDescending(y => y.PaymentCycleId),
                                         "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,PaymentCycle.FromMonth,PaymentCycle.ToMonth,PaymentCycle.Status,Programme")
                                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            var ddpdata = GenericService.GetManyBySp<SelectListVm>(
                "GetPrepayrollPaymentCycles",
                "@Code",
                new List<ParameterEntity>()
                    {
                        new ParameterEntity
                            {
                                ParameterTuple = new Tuple<string, object>(
                                    "Code",
                                    "PREPAYROLLDRAFT")
                            }
                    });
            ViewBag.PaymentCycleId = new SelectList(ddpdata, "Id", "Name");

            return View(paymentCycles);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> IneligibleExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollIneligible>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollIneligiblesList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidAccExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollInvalidPaymentAccount>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidPaymentAccountsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidCardExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollInvalidPaymentCard>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidPaymentCardsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> InvalidIdExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetSearchableQueryable<PrepayrollInvalidId>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                x => x.OrderByDescending(y => y.ExceptionTypeId).ThenBy(y=>y.Person.NationalIdNo),
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Person,ActionedByUser,ActionedApvByUser")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollInvalidIdsList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        [ActionName("Run")]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:RE-RUN")]
        public async Task<ActionResult> RunPrepayroll(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string[] parts = id.Split('-');
            var programmeId = int.Parse(parts[1]);
            var paymentCycleId = int.Parse(parts[0]);

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Run")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:RE-RUN")]
        public ActionResult RunPrepayrollConfirmed(int ProgrammeId, int PaymentCycleId)
        {
            // var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            // SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder(conString);
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ProcessPrepayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        PaymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        ProgrammeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = PaymentCycleId,
                    Key2 = ProgrammeId,
                    TableName = "PaymentCycleDetail",
                    ModuleRightCode = "PRE-PAYROLL:RE-RUN",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = " PrePayroll Rerun Draft pre-payroll"
                });

                TempData["Title"] = " Pre-payroll Process Complete";
                TempData["MESSAGE"] = $" The Pre-payroll Process was completed successfully. The below is the Analysis";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Running Pre-payroll. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Summary", new { paymentcycle = PaymentCycleId, programme = ProgrammeId });
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:ACTION")]
        public async Task<ActionResult> SingleActioning(
            string id,
            int programme,
            int paymentCycle,
            int? hhId,
            int? personId,
            int exceptionTypeId)
        {
            var model = new PrepayrollExceptionsDetailsViewModel {
                PrepayrollSingleActionViewModel =
                                    new PrepayrollSingleActionViewModel {
                                        Programme
                                                = programme,
                                        PaymentCycle
                                                = paymentCycle,
                                        PersonId
                                                = personId,
                                        HhId =
                                                hhId,
                                        ExceptionTypeId
                                                = exceptionTypeId,
                                        Id = id
                                    }
            };

            if (!string.IsNullOrEmpty(id))
            {
                switch (id.ToUpper())
                {
                    case "DUPLICATEID":
                        model.PrepayrollDuplicateId = await this.GenericService.GetOneAsync<PrepayrollDuplicateId>(
                                                               x => x.PersonId == personId && x.HhId == hhId
                                                                                           && x.ProgrammeId == programme
                                                                                           && x.PaymentCycleId == paymentCycle,
                                                               "Person,HouseHold,ExceptionType,Programme,PaymentCycle")
                                                           .ConfigureAwait(false);
                        break;

                    case "INVALIDID":
                        model.PrepayrollInvalidId = await this.GenericService.GetOneAsync<PrepayrollInvalidId>(
                                                             x => x.PersonId == personId && x.HhId == hhId
                                                                                         && x.ProgrammeId == programme
                                                                                         && x.PaymentCycleId
                                                                                         == paymentCycle,
                                                             "Person,HouseHold,ExceptionType,Programme,PaymentCycle")
                                                         .ConfigureAwait(false);
                        break;

                    case "INELIGIBLE":
                        model.PrepayrollIneligible = await this.GenericService.GetOneAsync<PrepayrollIneligible>(
                                                              x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                  && x.PaymentCycleId == paymentCycle,
                                                              "HouseHold,ExceptionType,Programme,PaymentCycle")
                                                          .ConfigureAwait(false);
                        break;

                    case "SUSPICIOUS":
                        model.PrepayrollSuspicious = await this.GenericService.GetOneAsync<PrepayrollSuspicious>(
                                                              x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                  && x.PaymentCycleId == paymentCycle,
                                                              "HouseHold,ExceptionType,Programme,PaymentCycle")
                                                          .ConfigureAwait(false);
                        break;

                    case "INVALIDCARD":
                        model.PrepayrollInvalidPaymentCard = await this.GenericService
                                                                  .GetOneAsync<PrepayrollInvalidPaymentCard>(
                                                                      x => x.HhId == hhId && x.ProgrammeId == programme
                                                                                          && x.PaymentCycleId
                                                                                          == paymentCycle,
                                                                      "HouseHold,ExceptionType,Programme,PaymentCycle")
                                                                  .ConfigureAwait(false);
                        break;

                    case "INVALIDACC":
                        model.PrepayrollInvalidPaymentAccount = await this.GenericService
                                                                     .GetOneAsync<PrepayrollInvalidPaymentAccount>(
                                                                         x => x.HhId == hhId
                                                                              && x.ProgrammeId == programme
                                                                              && x.PaymentCycleId == paymentCycle,
                                                                         "HouseHold,ExceptionType,Programme,PaymentCycle")
                                                                     .ConfigureAwait(false);
                        break;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PRE-PAYROLL:ACTION")]
        public ActionResult SingleActioning(PrepayrollExceptionsDetailsViewModel parentmodel)
        {
            var model = parentmodel.PrepayrollSingleActionViewModel;
            var prepayrollExceptionsXmlModel = new List<PrepayrollException>();
            var exceptionTypeXmlModel = new List<ExceptionType>();
            var exceptionTypeXml = string.Empty;
            var prepayrollExceptionXml = string.Empty;
            var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };

            if (ModelState.IsValid)
            {
                var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;
                var userId = int.Parse(User.Identity.GetUserId());

                var prepayrollExceptionsSerializer = new XmlSerializer(
                   typeof(List<PrepayrollException>),
                   new XmlRootAttribute("PrepayrollExceptions"));

                prepayrollExceptionsXmlModel.Add(
                    new PrepayrollException {
                        HhId = model.HhId,
                        PaymentCycleId = model.PaymentCycle,
                        ProgrammeId = model.Programme,
                        PersonId = model.PersonId
                    });

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, prepayrollExceptionsXmlModel);
                    prepayrollExceptionXml += sw.ToString();
                }

                exceptionTypeXmlModel.Add(new ExceptionType { ExceptionTypeId = model.ExceptionTypeId });
                var exceptionTypesSerializer = new XmlSerializer(
                    typeof(List<ExceptionType>),
                    new XmlRootAttribute("ExceptionTypes"));

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    exceptionTypesSerializer.Serialize(xw, exceptionTypeXmlModel);
                    exceptionTypeXml += sw.ToString();
                }

                var storedProcedure = "ProcessPrePayrollExceptionsAction";
                var parameterNames =
                    "@PaymentCycleId,@ProgrammeId,@ExceptionTypeIdsXML,@Notes,@EnrolmentGroupIdsXML,@PrepayrollExceptionsXML,@ReplacePreviousActioning,@FilePath,@HasSupportingDoc,@RemovePreviousSupportingDoc,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycle)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ProgrammeId",
                                                            model.Programme)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ExceptionTypeIdsXML",
                                                            exceptionTypeXml)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Notes",
                                                            model.Notes)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "EnrolmentGroupIdsXML ",
                                                            DBNull.Value),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PrepayrollExceptionsXML",
                                                            prepayrollExceptionXml),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "ReplacePreviousActioning",
                                                            model
                                                                .ReplacePreviousActioning),
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FilePath",
                                                            WebConfigurationManager
                                                                .AppSettings[
                                                                    "DIRECTORY_SHARED_FILES"])
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "HasSupportingDoc",
                                                            hasSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "RemovePreviousSupportingDoc",
                                                            model
                                                                .RemovePreviousSupportingDoc)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };
                try
                {
                    var newModel = GenericService.GetOneBySp<ProcessPrePayrollExceptionsActionVm>(
                        storedProcedure,
                        parameterNames,
                        parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.PaymentCycle,
                        Key2 = model.Programme,
                        TableName = "PaymentCycleDetail",
                        ModuleRightCode = "PRE-PAYROLL:ACTION",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Prepayroll Single Actioning"
                    });

                    if (model.Upload != null && model.Upload.ContentLength > 0)
                    {
                        model.Upload.SaveAs(
                            WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                    }

                    TempData["Title"] = "Exception successfully Actioned ";
                    TempData["MESSAGE"] =
                        $" Exception successfully Actioned Kindly proceed to action other exceptions. Once you are through, Finalize the Prepayroll";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["Title"] = "Error Actioning the Exception  ";
                    TempData["MESSAGE"] = e.Message;
                    TempData["KEY"] = "danger";
                }

                return RedirectToAction(
                    $"{model.Id}Exceptions",
                    new { paymentcycle = model.PaymentCycle, programme = model.Programme });
            }
            else
            {
                TempData["Title"] = "Error Actioning the Exception  ";
                TempData["MESSAGE"] = GetErrorListFromModelState(ModelState);
                TempData["KEY"] = "danger";
                return RedirectToAction(
                    $"{model.Id}Exceptions",
                    new { paymentcycle = model.PaymentCycle, programme = model.Programme });
            }
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> Summary(int? programme, int? paymentcycle)
        {
            if (programme == null || paymentcycle == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.PaymentCycleId == paymentcycle.Value && x.ProgrammeId == programme,
                                             "PaymentStage,PaymentCycle.FinancialYear,PaymentCycle.Status,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                         .ConfigureAwait(true);

            var spName = "GetPrepayrollSummary";
            var parameterNames = "@PaymentCycleId,@ProgrammeId";
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentcycle)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programme)
                                            },
                                    };

            var prepayrollAuditViewModel =
                GenericService.GetOneBySp<PrepayrollAuditViewModel>(spName, parameterNames, parameterList);

            var model = new PrepayrollViewModel {
                PaymentCycleDetail = paymentCycleDetail,
                PrepayrollAuditViewModel = prepayrollAuditViewModel
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "PRE-PAYROLL:VIEW")]
        public async Task<ActionResult> SuspiciousExceptions(int? page, int programme, int paymentCycle)
        {
            var pageNumber = page ?? 1;
            var data = (await GenericService.GetAsync<PrepayrollSuspicious>(
                                x => x.PaymentCycleId == paymentCycle && x.ProgrammeId == programme,
                                null,
                                "ExceptionType,PaymentCycleDetail.Programme,PaymentCycleDetail.PaymentCycle,Household")
                            .ConfigureAwait(true)).ToPagedList(pageNumber, 20);

            var model = new PrepayrollExceptionsListViewModel {
                PrepayrollSuspiciousList = data,
                PrepayrollExceptionsFilterViewModel =
                                    new PrepayrollExceptionsFilterViewModel {
                                        PaymentCycle
                                                = paymentCycle,
                                        Programme
                                                = programme
                                    }
            };

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
    }
}