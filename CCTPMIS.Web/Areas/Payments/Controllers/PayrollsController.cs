﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Payments.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Context;
    using Business.Interfaces;
    using Business.Model;
    using Business.Repositories;
    using CCTPMIS.Models.Email;
    using CCTPMIS.Models.Enrolment;
    using CCTPMIS.Models.Payment;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PayrollsController : Controller
    {
        public readonly IGenericService GenericService;

        protected readonly IEmailService EmailService;

        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public PayrollsController(IGenericService genericService, EmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PAYROLL:APPROVAL")]
        public async Task<ActionResult> ApprovePayroll(int programmeId, int paymentCycleId)
        {
            var paymentCycleDetail = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                             x => x.PaymentCycleId == paymentCycleId && x.ProgrammeId == programmeId,
                                             "PaymentStage,PaymentCycle.FinancialYear,PaymentCycle.Status,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                         .ConfigureAwait(true);

            var spName = "GetPrepayrollSummary";
            var parameterNames = "@PaymentCycleId,@ProgrammeId";
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId)
                                            },
                                    };

            var prepayrollAuditViewModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(spName, parameterNames, parameterList);

            var model = new PrepayrollViewModel {
                PaymentCycleDetail = paymentCycleDetail,
                PrepayrollAuditViewModel = prepayrollAuditViewModel
            };
            return View(model);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("ApprovePayroll")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYROLL:APPROVAL")]
        public async Task<ActionResult> ApprovePayrollConfirmed(PrepayrollViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ApprovePayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        model.PaymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        model.ProgrammeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.PaymentCycleId,
                    Key2 = model.ProgrammeId,
                    TableName = "Payroll",
                    ModuleRightCode = "PAYROLL:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Payroll Generated Successfully"
                });

                var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
                var connection = new SqlConnectionStringBuilder(conString);

                var storedProcedureGen = "GeneratePayrollFile";
                var parameterNamesGen = "@PaymentCycleId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
                var parameterListGen = new List<ParameterEntity>
                                           {
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "PaymentCycleId",
                                                               model
                                                                   .PaymentCycleId),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "FilePath",
                                                               WebConfigurationManager
                                                                   .AppSettings[
                                                                       "DIRECTORY_SHARED_FILES"]),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBServer",
                                                               connection
                                                                   .DataSource),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBName",
                                                               connection
                                                                   .InitialCatalog),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBUser",
                                                               connection.UserID),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBPassword",
                                                               connection
                                                                   .Password),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "UserId",
                                                               userId),
                                                   },
                                           };
                try
                {
                    var newModel_gen = GenericService.GetManyBySp<SPOutput>(
                        storedProcedureGen,
                        parameterNamesGen,
                        parameterListGen);

                    var fileService = new FileService();

                    try
                    {
                        var newModelGenList = newModel_gen.ToList();
                        if (newModelGenList.Any())
                        {
                            foreach (var item in newModelGenList)
                            {
                                var file = await GenericService
                                               .GetOneAsync<FileCreation>(x => x.Id == item.FileCreationId.Value)
                                               .ConfigureAwait(false);
                                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                                GenericService.Update(file);
                            }
                        }

                        TempData["Title"] = "Payroll Files Generation Complete";
                        TempData["MESSAGE"] = $" The PPayroll Files Generation completed successfully.";
                        TempData["KEY"] = "success";
                    }
                    catch (Exception ex)
                    {
                        TempData["Title"] = "Payroll Files Generation Complete";
                        TempData["MESSAGE"] = ex.Message;
                        TempData["KEY"] = "danger";
                    }

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.PaymentCycleId,
                        Key2 = model.ProgrammeId,
                        TableName = "Payroll",
                        ModuleRightCode = "PAYROLL:APPROVAL",
                        Record = $"{JsonConvert.SerializeObject(newModel_gen)}",
                        WasSuccessful = true,
                        Description = "Payroll Generated Successfully"
                    });
                }

                // loop through each file
                catch (Exception e)
                {
                    TempData["Title"] = "Payroll Files Generation Complete";
                    TempData["MESSAGE"] = e.Message;
                    TempData["KEY"] = "danger";
                }

                //TempData["Title"] = "Payroll Approval Complete";
                //TempData["MESSAGE"] =
                //    $" The Payroll was  Completed successfully. Kindly Proceed to share with the  PSPs";
                //TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error on Payroll Approval. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "PAYROLL:VIEW")]
        public async Task<ActionResult> Details(int? programmeId, int? paymentCycleId)
        {
            if (programmeId == null || paymentCycleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);
            var spName = "GetPayrollSummary";
            var parameterNames = "@PaymentCycleId,@ProgrammeId";
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",programmeId)
                                            },
                                    };
            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            var getPayrollSummaryVm =
                GenericService.GetOneBySp<GetPayrollSummaryVm>(spName, parameterNames, parameterList);
            var model = new PayrollDetailVm {
                PaymentCycleDetail = paymentCycle,
                GetPayrollSummaryVm = getPayrollSummaryVm
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PAYROLL:VIEW")]
        public async Task<ActionResult> Files(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var plans = (await GenericService.GetAsync<FileCreation>(
                             x => x.Type.Code == "PAYMENT",
                             x => x.OrderByDescending(y => y.Id),
                             "FileDownloads,TargetUser").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(plans);
        }

        // GET: Payments/Payrolls
        [GroupCustomAuthorize(Name = "PAYROLL:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var payrollCodes =
                "PAYROLL,PAYROLLVER,PAYROLLAPV,PAYROLLEX,PAYROLLEXCONFPOST,PAYROLLPOST,PAYMENTRECON,PAYMENTRECONAPV,CLOSED,PAYROLLEXCONF,POSTPAYROLL,";
            var paymentCycle = (await GenericService.GetAsync<PaymentCycleDetail>(
                                        x => payrollCodes.Contains(x.PaymentStage.Code + ","),
                                        x => x.OrderByDescending(y => y.PaymentCycleId),
                                        includeProperties:
                                        "CreatedByUser,PaymentStage,Programme,PaymentCycle.FinancialYear,PaymentCycle.ToMonth,PaymentCycle.FromMonth")
                                    .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        [GroupCustomAuthorize(Name = "PAYROLL:VIEW")]
        public async Task<ActionResult> MobilizationLists(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var plans = (await GenericService.GetAsync<FileCreation>(
                             x => x.Type.Code == "PAYROLL",
                             x => x.OrderByDescending(y => y.Id),
                             "FileDownloads").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(plans);
        }

        [ActionName("ProcessPayroll")]
        [GroupCustomAuthorize(Name = "PAYROLL:PROCESS")]
        public async Task<ActionResult> ProcessPayroll(int? programmeId, int? paymentCycleId)
        {
            if (programmeId == null || paymentCycleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        [HttpPost, ActionName("ProcessPayroll")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYROLL:PROCESS")]
        public ActionResult ProcessPayrollConfirmed(int programmeId, int paymentCycleId)
        {
            // var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            // SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder(conString);
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "ProcessPayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<PrepayrollAuditViewModel>(
                    storedProcedure,
                    parameterNames,
                    parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentCycleId,
                    Key2 = programmeId,
                    TableName = "Payroll",
                    ModuleRightCode = "PAYROLL:PROCESS",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Payroll Processed Successfully"
                });

                TempData["Title"] = " Pre-payroll Process Complete";
                TempData["MESSAGE"] = $" The Pre-payroll Process was completed successfully. The below is the Analysis";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Running Pre-payroll. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        // Share with Psps
        [GroupCustomAuthorize(Name = "PAYROLL:SHARE")]
        [HttpGet]
        public ActionResult ResendPayrollFile(int? programmeId, int? paymentCycleId)
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "PAYROLL:SHARE")]
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> ResendPayrollFile(
            int? programmeId,
            int? paymentCycleId,
            ResendEnrolmentFileViewModel newModel)
        {
            try
            {
                var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                       x => x.Id == paymentCycleId,
                                       "Status,FinancialYear,FromMonth,ToMonth").ConfigureAwait(true);
                var psps = await GenericService.GetAsync<Psp>(x => x.IsActive, null, "User").ConfigureAwait(false);

                foreach (var psp in psps)
                {
                    await EmailService.SendAsync(
                        new PspPayrolResendFileEmail {
                            FirstName = psp.User.DisplayName,
                            To = psp.User.Email,
                            PspName = psp.Name,
                            PaymentCycle = paymentCycle.CycleName,
                            Subject =
                                    $"Payroll File Notification for {paymentCycle.CycleName} Payment Cycle Resend",
                            Title =
                                    $"Payroll File Notification for {paymentCycle.CycleName} Payment Cycle Resend",
                        }).ConfigureAwait(true);
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentCycleId,
                    Key2 = programmeId,
                    TableName = "Payroll",
                    ModuleRightCode = "PAYROLL:SHARE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Payroll Files Re-Shared Successfully"
                });
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Payroll Files Generation Complete";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "PAYROLL:SHARE")]
        public ActionResult ShareWithPsp(int? programmeId, int? paymentCycleId)
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "PAYROLL:SHARE")]
        [HttpPost, ValidateAntiForgeryToken, ActionName("ShareWithPsp")]
        public async Task<ActionResult> ShareWithPspConfirmed(int? programmeId, int? paymentCycleId)
        {
            try
            {
                var paymentCycle = await GenericService.GetOneAsync<PaymentCycle>(
                                           x => x.Id == paymentCycleId,
                                           "Status,FinancialYear,CreatedByUser,ModifiedByUser,FromMonth,ToMonth")
                                       .ConfigureAwait(true);
                var psps = await GenericService.GetAsync<Psp>(x => x.IsActive, null, "User").ConfigureAwait(false);

                var userId = int.Parse(User.Identity.GetUserId());
                var storedProcedure = "FlagFilesToShare";
                var parameterNames = "@FileTypeCode";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "FileTypeCode",
                                                            "PAYMENT")
                                                },
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNames, parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = paymentCycleId,
                        Key2 = programmeId,
                        TableName = "PaymentCycle",
                        ModuleRightCode = "PAYROLL:SHARE",
                        Record = $"{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "Payroll Files Shared Successfully"
                    });

                    foreach (var psp in psps)
                    {
                        await EmailService.SendAsync(
                            new PspPayrolFileEmail {
                                FirstName = psp.User.DisplayName,
                                To = psp.User.Email,
                                PspName = psp.Name,
                                PaymentCycle = paymentCycle.CycleName,
                                Subject =
                                        $"Payroll File for {paymentCycle.CycleName} Payment Cycle Is Ready",
                                Title =
                                        $"Payroll File for {paymentCycle.CycleName} Payment Cycle Is Ready",
                            }).ConfigureAwait(true);
                    }
                }
                catch (Exception ex)
                {
                    TempData["Title"] = "Payroll Files Generation Complete";
                    TempData["MESSAGE"] = ex.Message;
                    TempData["KEY"] = "danger";
                }

                TempData["MESSAGE"] = $"Payroll files Notification was broadcasted successfully";
                TempData["KEY"] = "success";
                TempData["Title"] = "Payroll Sharing File With PSPs";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"An Error Occured Prior to the Sharing  the Payroll File with the PSPs : "
                                      + ex.Message;
                TempData["KEY"] = "danger";
                TempData["Title"] = "Error on Payroll File Sharing File With PSPs";
                return RedirectToAction("Index");
            }
        }

        [ActionName("VerifyPayroll")]
        [GroupCustomAuthorize(Name = "PAYROLL:VERIFY")]
        public async Task<ActionResult> VerifyPayroll(int? programmeId, int? paymentCycleId)
        {
            if (programmeId == null || paymentCycleId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<PaymentCycleDetail>(
                                       x => x.ProgrammeId == programmeId && x.PaymentCycleId == paymentCycleId,
                                       "PaymentStage,PaymentCycle.FinancialYear,PrePayrollByUser,PrepayrollApvByUser,CreatedByUser,Programme,PaymentCycle.FromMonth,PaymentCycle.ToMonth")
                                   .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        [HttpPost, ActionName("VerifyPayroll")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PAYROLL:VERIFY")]
        public ActionResult VerifyPayrollConfirmed(int programmeId, int paymentCycleId)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "VerifyPayroll";
            var parameterNames = "@PaymentCycleId,@ProgrammeId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PaymentCycleId",
                                                        paymentCycleId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ProgrammeId",
                                                        programmeId),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };
            try
            {
                var results = new ApplicationDbContext().MultipleResults().With<VerifyPayrollVm1>()
                    .With<GetPayrollSummaryVm>().With<GetPayrollSummaryVm>().Execute(
                        storedProcedure,
                        parameterNames,
                        parameterList);

                var model = new VerifyPayrollVm();
                var x = 0;
                var y = 0;
                foreach (var item in results)
                {
                    y = 0;
                    foreach (var vm in item)
                    {
                        switch (x)
                        {
                            case 0 when y == 0:
                                model.VerifyPayrollVm1 = vm as VerifyPayrollVm1;
                                break;

                            case 1 when y == 0:
                                model.VerifyPayrollSummaryVm = vm as GetPayrollSummaryVm;
                                break;

                            case 2 when y == 0:
                                model.GetPayrollSummaryVm = vm as GetPayrollSummaryVm;
                                break;
                        }

                        y++;
                    }

                    x++;
                }

                if (model.VerifyPayrollVm1 != null && model.VerifyPayrollVm1.Verified)
                {
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = paymentCycleId,
                        Key2 = programmeId,
                        TableName = "PaymentCycle",
                        ModuleRightCode = "PAYROLL:VERIFY",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Payroll Verified Successfully"
                    });

                    TempData["Title"] = " Payroll Verification Complete";
                    TempData["MESSAGE"] =
                        $"Payroll Verification was completed successfully. The Verification Payroll and the Initial Payroll Matched.";
                    TempData["KEY"] = "success";
                }
                else
                {
                    TempData["Title"] = " Payroll Verification Failed";
                    TempData["MESSAGE"] =
                        $"Payroll Verification was Failed. The Verification Payroll and the Initial Payroll did not Match.";
                    TempData["KEY"] = "danger";
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = paymentCycleId,
                        Key2 = programmeId,
                        TableName = "PaymentCycle",
                        ModuleRightCode = "PAYROLL:VERIFY",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Payroll Verification failed"
                    });
                }

                return View("Verification", model);
            }
            catch (Exception e)
            {
                TempData["Title"] = "Error Running Pre-payroll. ";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }
    }
}