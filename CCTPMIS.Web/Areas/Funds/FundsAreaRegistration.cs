﻿using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.Funds
{
    public class FundsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Funds";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Funds_default",
                "Funds/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                    namespaces: new[] { "CCTPMIS.Web.Areas.Funds.Controllers" });
        }
    }
}