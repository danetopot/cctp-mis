﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Funds.Controllers
{
    [Authorize]
    public class ReconciliationController : Controller
    {
        public readonly IGenericService GenericService;

        protected readonly IEmailService EmailService;

        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public ReconciliationController(IGenericService genericService, EmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public ActionResult Analysis(DateTime? startDate, DateTime? endDate, int? id)
        {
            var model = new ReconAnalysisVm();
            try
            {
                var spName = "GetReconciliationOptions";
                var parameterNames = "@Id,@StartDate,@EndDate";
                object param;
                if (id.HasValue)
                {
                    param = id;
                }
                else
                {
                    param = DBNull.Value;
                }

                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "Id",
                                param)
                    }, new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "StartDate",
                                startDate)
                    },
                    new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "EndDate",
                                endDate)
                    }
                };
                var results = GenericService.GetManyBySp<AddEditReconSpVm>(
                    spName,
                    parameterNames,
                    parameterList).ToList();
                model.results = results;
                model.Error = "";

                // return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                // Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var errors = new List<string> { e.Message };
                model.results = null;
                model.Error = e.Message;
                // return Json(errors, JsonRequestBehavior.AllowGet);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var reconciliations = (await GenericService.GetAsync<Reconciliation>(
                   null,
                   x => x.OrderByDescending(y => y.Id),
                    includeProperties:
                    "ReconciliationDetails,CreatedByUser,ApvByUser,Status")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            if (reconciliations == null)
            {
                return HttpNotFound();
            }

            return View(reconciliations);
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var maxrec = await GenericService.GetFirstAsync<Reconciliation>(null, x => x.OrderByDescending(y => y.Id));
            ViewBag.MaxDate = maxrec?.EndDate.AddDays(1).ToString("yyy-MM-dd") ?? "";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public async Task<ActionResult> Create(ReconciliationCreateViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var applicableCyclesXml = string.Empty;
                var prepayrollExceptionsSerializer = new XmlSerializer(typeof(List<PaymentCycleXmlModel>), new XmlRootAttribute("PaymentCycles"));
                if (model.PaymentCycles != null)
                {
                    if (model.PaymentCycles.Any())
                    {
                        var applicableCyclesXmlModel = model.PaymentCycles.Select(item => new PaymentCycleXmlModel { Id = item.Id, PaymentCycleId = item.PaymentCycleId, }).ToList();
                        var sw = new StringWriter();
                        using (sw)
                        {
                            prepayrollExceptionsSerializer.Serialize(XmlWriter.Create(sw, new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true }), applicableCyclesXmlModel);
                            applicableCyclesXml += sw.ToString();
                        }

                        var spName = "AddEditReconciliation";
                        var parameterNames = "@Id,@StartDate,@EndDate,@ApplicablePaymentCyclesXML,@UserId";
                        var parameterList = new List<ParameterEntity>
                                            {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Id",
                                                            DBNull.Value)
                                                },

                                            new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "StartDate",
                                                        model.StartDate)
                                            }, new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "EndDate",
                                                        model.EndDate)
                                            }
                                            , new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ApplicablePaymentCyclesXML",
                                                        applicableCyclesXml)
                                            },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                        try
                        {
                            var newModel = GenericService.GetOneBySp<AddEditReconSpVm>(
                                spName,
                                parameterNames,
                                parameterList);

                            LogService.AuditTrail(new AuditTrailVm {
                                UserId = $"{User.Identity.GetUserId()}",
                                Key1 = model.Id,
                                TableName = "Reconciliation",
                                ModuleRightCode = "RECONCILIATION:ENTRY",
                                Record = $"{JsonConvert.SerializeObject(model)}",
                                WasSuccessful = true,
                                Description = "Reconciliation Create"
                            });

                            TempData["MESSAGE"] = $" The Reconciliation period was  added successfully. Please fill for every PSP.";
                            TempData["KEY"] = "success";
                            return RedirectToAction("Index");
                        }
                        catch (Exception e)
                        {
                            // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                            TempData["MESSAGE"] = $" The Reconciliation period was not created .  \n " + e.Message;
                            TempData["KEY"] = "danger";
                        }
                    }
                }
            }
            else
            {
                // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                TempData["MESSAGE"] = $" The Reconciliation period was not created .  \n " + "There are no Applicable Payment Cycles";
                TempData["KEY"] = "danger";
            }
            return View(model);
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var paymentCycle = await GenericService.GetOneAsync<Reconciliation>(
                    x => x.Id == id.Value,
                    "ReconciliationDetails.Status,CreatedByUser,ReconciliationDetails.CreatedByUser,ApvByUser,Status,ReconciliationDetails.Psp")
                .ConfigureAwait(true);

            if (paymentCycle == null)
            {
                return HttpNotFound();
            }

            return View(paymentCycle);
        }

        #region Approve

        [GroupCustomAuthorize(Name = "RECONCILIATION:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                                       x => x.Id == id.Value,
                    "ReconciliationDetails,CreatedByUser,ModifiedByUser,Status")
                                   .ConfigureAwait(true);

            if (reconciliation == null)
            {
                return HttpNotFound();
            }

            return View(reconciliation);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveReconciliation";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "Reconciliation",
                    ModuleRightCode = "RECONCILIATION:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Reconciliation Approved"
                });
                TempData["MESSAGE"] = $" The Reconciliation was Approved successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = $"The Reconciliation was not approved Successfully {e.Message}.";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Details", "Reconciliation", new { id = id });
        }

        #endregion Approve

        #region CLOSE

        [GroupCustomAuthorize(Name = "RECONCILIATION:CLOSE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                                       x => x.Id == id.Value,
                    "ReconciliationDetails,CreatedByUser,ModifiedByUser,Status")
                                   .ConfigureAwait(true);

            if (reconciliation == null)
            {
                return HttpNotFound();
            }

            return View(reconciliation);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:CLOSE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeReconciliation";
            var parameterNames = "@ReconciliationId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ReconciliationId",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "Reconciliation",
                    ModuleRightCode = "RECONCILIATION:CLOSE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Reconciliation Finalized"
                });

                TempData["MESSAGE"] = $" The Reconciliation was finalized successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = $"The Reconciliation was not finalized successfully {e.Message}.";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index", "Reconciliation", new { id = id });
        }

        #endregion CLOSE
    }
}