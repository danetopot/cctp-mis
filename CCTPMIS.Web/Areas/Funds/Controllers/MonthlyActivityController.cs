﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Funds.Controllers
{
    [Authorize]
    public class MonthlyActivityController : Controller
    {
        private readonly IEmailService EmailService;

        private readonly IGenericService GenericService;

        private readonly IPdfService PdfService;

        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public MonthlyActivityController(IGenericService genericService, EmailService emailService, PdfService pdfService, ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            PdfService = pdfService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var paymentCycles = (await GenericService.GetAsync<BeneAccountMonthlyActivity>(
                    null,
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Month")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(paymentCycles);
        }

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var month = await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(
                    x => x.Id == id.Value, "Status,Month")
                .ConfigureAwait(true);

            if (month == null)
            {
                return HttpNotFound();
            }

            var spName = "GetBeneAccountMonthlyActivitySummary";
            var parameterNames = "@MonthNo, @Year";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "MonthNo",
                            month.Month.Code),
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Year",
                            month.Year),
                }
            };

            var pspAnalyses = GenericService.GetManyBySp<MonthlyActivitySummaryViewModel>(spName, parameterNames, parameterList);

            var model = new ActivityReportSummaryViewModel {
                PspActivity = pspAnalyses.ToList(),
                Month = month
            };
            return View(model);
        }

        #region Create BeneAccountMonthlyActivity

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
            .ConfigureAwait(false)).ToList();
            ViewBag.MonthId = new SelectList(months, "Id", "Description");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:ENTRY")]
        public async Task<ActionResult> Create(BeneAccountMonthlyActivity model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var spName = "AddEditBeneAccountMonthlyActivity";
                var parameterNames = "@Id,@MonthId,@Year,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Id",
                                                            DBNull.Value)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "MonthId",
                                                            model.MonthId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Year",
                                                            model.Year)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "BeneAccountMonthlyActivity",
                        ModuleRightCode = "ACCOUNTACTIVITY:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "Monthly Account Activity Created"
                    });

                    TempData["MESSAGE"] = $" The Activity Month was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                    TempData["MESSAGE"] = $" The Activity Month  was not created  successfully .  " + e.Message;
                    TempData["KEY"] = "danger";
                }
            }
            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
                .ConfigureAwait(false)).ToList();
            ViewBag.MonthId = new SelectList(months, "Id", "Description", model.MonthId);
            return View(model);
        }

        #endregion Create BeneAccountMonthlyActivity

        #region View BeneAccountMonthlyActivity
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:VIEW")]
        public async Task<ActionResult> View(int? id, int? pspid)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var month = await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(
                    x => x.Id == id.Value, "Status,Month")
                .ConfigureAwait(true);

            if (month == null)
            {
                return HttpNotFound();
            }

            var spName = "GetPSPAccountMonthlyActivitySummary";
            var parameterNames = "@Id,@MonthId,@Year,@PSPId,@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Id",
                            id),
                }, 
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "MonthId",
                            month.Month.Id),
                },
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Year",
                            month.Year),
                },
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PSPId",
                            pspid),
                },
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "UserId",
                            userId),
                },
            };

            var pspAnalyses = GenericService.GetManyBySp<MonthlyActivityPSPSummaryViewModel>(spName, parameterNames, parameterList);

            var model = new ActivityReportSummaryViewModel {
                PspSummaryActivity = pspAnalyses.ToList(),
                Month = month
            };

            return View(model);
        }
        #endregion View BeneAccountMonthlyActivity

        #region Edit   BeneAccountMonthlyActivity

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:MODIFIER")]
        public async Task<ActionResult> Edit(int id)
        {
            var model = (await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(x => x.Id == id)
                .ConfigureAwait(false));
            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
                .ConfigureAwait(false)).ToList();
            ViewBag.MonthId = new SelectList(months, "Id", "Description", model.MonthId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:MODIFIER")]
        public async Task<ActionResult> Edit(BeneAccountMonthlyActivity model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var spName = "AddEditBeneAccountMonthlyActivity";
                var parameterNames = "@Id,@MonthId,@Year,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Id",
                                                            model.Id)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "MonthId",
                                                            model.MonthId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Year",
                                                            model.Year)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "BeneAccountMonthlyActivity",
                        ModuleRightCode = "ACCOUNTACTIVITY:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "Monthly Account Activity Modified"
                    });

                    TempData["MESSAGE"] = $" The Activity Month was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    // TempData["ViewModel"] = JsonConvert.SerializeObject("Error");
                    TempData["MESSAGE"] = $" The Activity Month  was not created  successfully .  " + e.Message;
                    TempData["KEY"] = "danger";
                }
            }
            var calendarMonthsCodeId = (await GenericService.GetOneAsync<SystemCode>(x => x.Code == "Calendar Months")
                .ConfigureAwait(false)).Id;
            var months = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == calendarMonthsCodeId)
                .ConfigureAwait(false)).ToList();
            ViewBag.MonthId = new SelectList(months, "Id", "Description", model.MonthId);
            return View(model);
        }

        #endregion Edit   BeneAccountMonthlyActivity

        #region Approve BeneAccountMonthlyActivity

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(
                                       x => x.Id == id.Value,
                                       "Month,Status,CreatedByUser,ModifiedByUser,ApvByUser")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveBeneAccountMonthlyActivity";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "BeneAccountMonthlyActivity",
                    ModuleRightCode = "ACCOUNTACTIVITY:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Monthly Account Activity Approved"
                });

                TempData["MESSAGE"] = $" The Activity Month  was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Approve BeneAccountMonthlyActivity

        #region Close BeneAccountMonthlyActivity after all data reciept

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:CLOSE")]
        public async Task<ActionResult> Close(int? id, int? pspid)
        {
            var model = new BeneAccountPSPMonthlyActivity();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(
                                       x => x.Id == id.Value,
                                       "Month,Status,CreatedByUser,ModifiedByUser,ApvByUser")
                                   .ConfigureAwait(true);

            var Psp = await GenericService.GetOneAsync<Business.Model.Psp>(x => x.Id == pspid,null).ConfigureAwait(false);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            model.BeneAccountMonthlyActivity = monthlyActivity;
            model.PspId = Psp.Id;
            ViewBag.Psp = Psp.Name;

            return View(model);
        }

        [HttpPost, ActionName("Close")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:CLOSE")]
        public ActionResult CloseConfirmed(int id, int pspid)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "CloseBeneAccountMonthlyActivity";
            var parameterNames = "@Id, @PSPId, @UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PSPId",
                                                        pspid)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "BeneAccountPSPMonthlyActivity",
                    ModuleRightCode = "ACCOUNTACTIVITY:CLOSE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Monthly Account Activity Accepted"
                });

                TempData["MESSAGE"] = $" The Activity Month for PSP was  successfully Accepted.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = id });
        }

        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:CLOSE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = await GenericService.GetOneAsync<BeneAccountMonthlyActivity>(
                                       x => x.Id == id.Value,
                                       "Month,Status,CreatedByUser,ModifiedByUser,ApvByUser")
                                   .ConfigureAwait(true);

            if (model == null)
            {
                return HttpNotFound();
            }
            

            return View(model);
        }

        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "ACCOUNTACTIVITY:CLOSE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeBeneAccountMonthlyActivity";
            var parameterNames = "@Id, @UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "BeneAccountMonthlyActivity",
                    ModuleRightCode = "ACCOUNTACTIVITY:CLOSE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Monthly Account Activity Closed"
                });

                TempData["MESSAGE"] = $" The Activity Month  was  successfully closed.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = id });
        }

        #endregion Close BeneAccountMonthlyActivity after all data reciept
    }
}