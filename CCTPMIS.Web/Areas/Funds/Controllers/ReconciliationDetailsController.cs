﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.Funds.Controllers
{
    [Authorize]
    public class ReconciliationDetailsController : Controller
    {
        public readonly IGenericService GenericService;

        protected readonly IEmailService EmailService;
        private readonly IPdfService PdfService;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public ReconciliationDetailsController(IGenericService genericService, EmailService emailService, ILogService logService, PdfService pdfService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            LogService = logService;
            PdfService = pdfService;
        }

        // GET: Payments/ReconciliationDetails
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Reconciliation");
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public ActionResult Analysis(int? reconciliationId, int? pspId)
        {
            try
            {
                var spName = "GetReconciliationDetailOptions";
                var parameterNames = "@ReconciliationId,@PSPId,@UserId";
                var userId = User.Identity.GetUserId();

                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "ReconciliationId",
                                reconciliationId)
                    }, new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "PSPId",
                                pspId)
                    },
                    new ParameterEntity
                    {
                        ParameterTuple =
                            new Tuple<string, object>(
                                "UserId",
                                userId)
                    }
                };
                var results = GenericService.GetOneBySp<GetReconDetailVm>(
                    spName,
                    parameterNames,
                    parameterList);
                return Json(results, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                var errors = new List<string> { e.Message };
                return Json(errors, JsonRequestBehavior.AllowGet);
            }
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        // GET: Payments/Reconciliations/Create
        public async Task<ActionResult> Create(int? id)
        {
            if (string.IsNullOrEmpty(Request["reconciliationid"]))
            {
                return RedirectToAction("Index", "Reconciliation");
            }

            var reconId = int.Parse(Request["reconciliationid"]);

            ViewBag.PspId = new SelectList(
                   await GenericService.GetAsync<Business.Model.Psp>(x => x.IsActive).ConfigureAwait(false),
                   "Id",
                   "Name");
           

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                                    x => x.Id == reconId,

                                    "ReconciliationDetails,CreatedByUser,ApvByUser,Status").ConfigureAwait(false);

            ViewBag.Reconciliation = reconciliation;
            ViewBag.StartDate = reconciliation.StartDate;
            ViewBag.EndDate = reconciliation.EndDate;
            var model = new ReconciliationDetailCreateViewModel();

            return View(model);
        }

        // POST: Payments/Reconciliations/Create To protect from over posting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public async Task<ActionResult> Create(int? reconciliationId, ReconciliationDetailCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.Identity.GetUserId();
                    var filePath = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"];
                    var spName = "AddEditReconciliationDetail";
                    var parameterNames = "@ReconciliationId,@PSPId,@OpeningBalance,@OpeningBalanceDiffNarration,@CrFundsRequests,@CrFundsRequestsStatement,@CrFundsRequestsDiffNarration,@CrClawBacks,@CrClawBacksStatement,@CrClawBacksDiffNarration,@DrPayments,@DrPaymentsStatement,@DrPaymentsDiffNarration,@DrCommissions,@DrCommissionsStatement,@DrCommissionsDiffNarration,@Balance,@BalanceStatement,@BalanceDiffNarration,@FilePath,@UserId";

                    var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;

                    var parameterList = new List<ParameterEntity>{
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ReconciliationId",model.ReconciliationId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("PSPId",model.PspId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("OpeningBalance",model.OpeningBalance)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("OpeningBalanceDiffNarration",model.OpeningBalanceDiffNarration)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequests",model.CrFundsRequests)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequestsStatement",model.CrFundsRequestsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequestsDiffNarration",model.CrFundsRequestsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacks",model.CrClawBacks)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacksStatement",model.CrClawBacksStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacksDiffNarration",model.CrClawBacksDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPayments",model.DrPayments)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPaymentsStatement",model.DrPaymentsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPaymentsDiffNarration",model.DrPaymentsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissions",model.DrCommissions)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissionsStatement",model.DrCommissionsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissionsDiffNarration",model.DrCommissionsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Balance",model.Balance)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BalanceStatement",model.BalanceStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BalanceDiffNarration",model.BalanceDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("FilePath",filePath)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                };

                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);
                    if (hasSupportingDoc)
                    {
                        model.Upload.SaveAs(WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                    }

                    model.Upload = null;

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.ReconciliationId,
                        Key2 = model.PspId,
                        TableName = "Reconciliation",
                        ModuleRightCode = "RECONCILIATION:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(parameterList)},{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "Reconciliation Details Added"
                    });

                    TempData["MESSAGE"] = " The PSP Reconciliation was  successfully Created.";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Details", "Reconciliation", new { id = model.ReconciliationId });
                }
                else
                {
                    var desc = this.GetErrorListFromModelState(ModelState);
                    // TempData["ViewModel"] = JsonConvert.SerializeObject(string.Empty);
                    TempData["MESSAGE"] = "The Psp was not added to the Reconciliation Period.    " + desc + ".";
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = "The Psp was not added to the Reconciliation Period.    " + ex.Message + ".";
                TempData["KEY"] = "danger";
            }

            ViewBag.PspId = new SelectList(
                  await GenericService.GetAsync<Business.Model.Psp>(x => x.IsActive).ConfigureAwait(false),
                  "Id",
                  "Name");

            var reconId = int.Parse(Request["reconciliationid"]);

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                x => x.Id == reconId,

                "ReconciliationDetails,CreatedByUser,ApvByUser,Status").ConfigureAwait(false);

            //GetReconDetailVm

            ViewBag.Reconciliation = reconciliation;

            ViewBag.StartDate = reconciliation.StartDate;
            ViewBag.EndDate = reconciliation.EndDate;
            return View(model);
        }

        public async Task<ActionResult> Edit(int reconciliationId, int pspId)
        {
            var reconciliationDetail = await GenericService.GetOneAsync<ReconciliationDetail>(
                x => x.PspId == pspId && x.ReconciliationId == reconciliationId, "Reconciliation,Psp");

            var model = new ReconciliationDetailCreateViewModel();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(reconciliationDetail, model);

            //ViewBag.PspId = new SelectList(
            //    await GenericService.GetAsync<Business.Model.Psp>(x => x.IsActive).ConfigureAwait(false),
            //    "Id",
            //    "Name");

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                x => x.Id == reconciliationId,

                "ReconciliationDetails,CreatedByUser,ApvByUser,Status").ConfigureAwait(false);

            //GetReconDetailVm

            ViewBag.Psp = reconciliationDetail.Psp.Name;

            ViewBag.Reconciliation = reconciliation;
            ViewBag.StartDate = reconciliation.StartDate;
            ViewBag.EndDate = reconciliation.EndDate;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:MODIFIER")]
        public async Task<ActionResult> Edit(int reconciliationId, int pspId, ReconciliationDetailCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = User.Identity.GetUserId();
                    var filePath = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"];
                    var spName = "AddEditReconciliationDetail";
                    var parameterNames = "@ReconciliationId,@PSPId,@OpeningBalance,@OpeningBalanceDiffNarration, @CrFundsRequests,@CrFundsRequestsStatement,@CrFundsRequestsDiffNarration,@CrClawBacks,@CrClawBacksStatement,@CrClawBacksDiffNarration,@DrPayments,@DrPaymentsStatement,@DrPaymentsDiffNarration,@DrCommissions,@DrCommissionsStatement,@DrCommissionsDiffNarration,@Balance,@BalanceStatement,@BalanceDiffNarration,@FilePath,@UserId";

                    var hasSupportingDoc = false || model.Upload != null && model.Upload.ContentLength > 0;

                    var parameterList = new List<ParameterEntity>{
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ReconciliationId",model.ReconciliationId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("PSPId",model.PspId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("OpeningBalance",model.OpeningBalance)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("OpeningBalanceDiffNarration",model.OpeningBalanceDiffNarration)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequests",model.CrFundsRequests)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequestsStatement",model.CrFundsRequestsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrFundsRequestsDiffNarration",model.CrFundsRequestsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacks",model.CrClawBacks)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacksStatement",model.CrClawBacksStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CrClawBacksDiffNarration",model.CrClawBacksDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPayments",model.DrPayments)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPaymentsStatement",model.DrPaymentsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrPaymentsDiffNarration",model.DrPaymentsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissions",model.DrCommissions)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissionsStatement",model.DrCommissionsStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("DrCommissionsDiffNarration",model.DrCommissionsDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Balance",model.Balance)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BalanceStatement",model.BalanceStatement)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BalanceDiffNarration",model.BalanceDiffNarration)},

                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("FilePath",filePath)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                };

                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);
                    if (hasSupportingDoc)
                    {
                        model.Upload.SaveAs(WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + newModel.SupportingDoc);
                    }

                    model.Upload = null;
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.ReconciliationId,
                        Key2 = model.PspId,
                        TableName = "Reconciliation",
                        ModuleRightCode = "RECONCILIATION:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(parameterList)},{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "Reconciliation Details Modified"
                    });

                    TempData["MESSAGE"] = " The PSP Reconciliation was  was  successfully modified. This must be approved!";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Details", "Reconciliation", new { id = model.ReconciliationId });
                }
                else
                {
                    var desc = this.GetErrorListFromModelState(ModelState);
                    TempData["MESSAGE"] = "The PSP Reconciliation did not update successfully.    " + desc;
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception e)
            {
                // TempData["ViewModel"] = JsonConvert.SerializeObject(string.Empty);
                TempData["MESSAGE"] = "The PSP Reconciliation did not update successfully.   " + e.Message;
                TempData["KEY"] = "danger";
            }

            //ViewBag.PspId = new SelectList(
            //    await GenericService.GetAsync<Business.Model.Psp>(x => x.IsActive).ConfigureAwait(false),
            //    "Id",
            //    "Name");

            var reconId = int.Parse(Request["reconciliationId"]);

            var reconciliation = await GenericService.GetOneAsync<Reconciliation>(
                x => x.Id == reconId,

                "ReconciliationDetails,CreatedByUser,ApvByUser,Status").ConfigureAwait(false);

            ViewBag.Reconciliation = reconciliation;

            ViewBag.StartDate = reconciliation.StartDate;
            ViewBag.EndDate = reconciliation.EndDate;
            return View(model);
        }


        [GroupCustomAuthorize(Name = "RECONCILIATION:VIEW")]
        public async Task<ActionResult> Details(int? pspId, int? reconciliationId)
        {
            if (reconciliationId == null && pspId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var reconciliationDetail = await GenericService.GetOneAsync<ReconciliationDetail>(
                x => x.PspId == pspId && x.ReconciliationId == reconciliationId, "Reconciliation.Status,Psp");

            if (reconciliationDetail == null)
            {
                return HttpNotFound();
            }

            return View(reconciliationDetail);
        }

        [GroupCustomAuthorize(Name = "RECONCILIATION:ENTRY")]
        public async Task<ActionResult> Finalize(int? pspId, int? reconciliationId)
        {
            var reconciliationDetail = await GenericService.GetOneAsync<ReconciliationDetail>(
                x => x.PspId == pspId && x.ReconciliationId == reconciliationId, "Reconciliation.Status,Psp");

            if (reconciliationId == null && pspId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (reconciliationDetail == null)
            {
                return HttpNotFound();
            }

            return View(reconciliationDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECONCILIATION:MODIFIER")]
        public async Task<ActionResult> Finalize(ReconciliationDetail reconciliationDetail)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var id = reconciliationDetail.ReconciliationId;
            var pspid = reconciliationDetail.PspId;
            var spName = "FinalizeReconciliationDetail";
            var parameterNames = "@ReconciliationId,@PSPId,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "ReconciliationId",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "PSPId",
                                                        pspid)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "Reconciliation",
                    ModuleRightCode = "RECONCILIATION:CLOSE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Reconciliation Closed"
                });

                TempData["MESSAGE"] = $" The Reconciliation was Approved successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = $"The Reconciliation was not approved Successfully {e.Message}";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Details", "Reconciliation", new { id = id });

            return View();
        }


        [GroupCustomAuthorize(Name = "REQUEST FOR FUNDS:MODIFIER")]
        public ActionResult GenerateReconciliationReport(int id, int pspId)
        {
            PdfService.ExportToPDF(
                $"Payments/ReconciliationDetails/ProcessReconciliation/{id}?pspId={pspId}",
                $"RECONCILIATION_{id}_{pspId}.pdf", "A4"
                 );
            try
            {
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                //Console.WriteLine(e);
                // throw;
            }
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [GroupCustomAuthorize(Name = "RECONCILIATION:VIEW")]
        public async Task<ActionResult> ProcessReconciliation(int? id, int pspId)
        {
            if (id == null && pspId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var reconciliationDetail = await GenericService.GetOneAsync<ReconciliationDetail>(
                x => x.PspId == pspId && x.ReconciliationId == id, "BankStatementFile,Reconciliation.Status,Reconciliation.CreatedByUser,Reconciliation.ApvByUser,Reconciliation.FinalizedByUser,Psp");

            if (reconciliationDetail == null)
            {
                return HttpNotFound();
            }

            return View(reconciliationDetail);
        }

      

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
    }
}