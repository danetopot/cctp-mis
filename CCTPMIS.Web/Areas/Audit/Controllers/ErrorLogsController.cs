﻿using System.Web.Mvc;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.Audit.Controllers
{
    [Authorize]
    public class ErrorLogsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public ErrorLogsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public ActionResult Index(int? page)
        {
            return View();
        }
    }
}

//var pageNm = page ?? 1;
//var pagesize = int.Parse( WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
//var model = db.ELMAH_Errors.OrderByDescending(x=>x.Sequence).ToPagedList(pageNm, pagesize);
//public async Task<ActionResult> Details(string id, int sequence)
//{
//    if (id == null)
//    {
//        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//    }

// var model = db.ELMAH_Errors.Single(x=>x.ErrorId.ToString() == id && x.Sequence==sequence);

// ViewBag.ModalSize = "modal-lg"; return View(model);

//}