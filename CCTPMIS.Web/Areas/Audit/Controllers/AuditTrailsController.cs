﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Audit.Controllers
{
    [Authorize]
    public class AuditTrailsController : Controller
    {
        private ElmahDbContext db = new ElmahDbContext();
        protected readonly IGenericService GenericService;

        public AuditTrailsController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        // GET: Audit/AuditTrails
        public ActionResult Index(int? page)
        {
            var pageNm = page ?? 1;
            var pagesize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var model = db.AuditTrails.OrderByDescending(x => x.Id).Include(x => x.Table).ToPagedList(pageNm, pagesize);
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = db.AuditTrails.Include(x => x.Table).Single(x => x.Id == id);

            ViewBag.ModalSize = "modal-lg";
            return View(model);
        }
    }
}