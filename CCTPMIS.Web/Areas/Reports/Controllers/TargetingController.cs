﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Reports;
using CCTPMIS.Services;
using ClosedXML.Excel;
using ClosedXML.Extensions;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Microsoft.Owin.Security.Provider;
using PagedList;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class TargetingController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public readonly IGenericService GenericService;

        protected readonly IEmailService EmailService;

        // private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IPdfService PdfService;

        public TargetingController(IGenericService genericService, EmailService emailService, PdfService pdfService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            PdfService = pdfService;
        }

        // GET: Reports/Targeting
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> HouseholdListing(TargetingReportVm vm)
        {
            Expression<Func<ListingPlanHH, bool>> filter = x => x.RegDate >= vm.StartDate && x.RegDate <= vm.EndDate;
            Expression<Func<ListingPlanHH, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.RejectedId != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.RejectedId.Value }, 0);
                if (success)
                {
                    filterAppend = x => x.RejectDate == null;
                }
                else
                {
                    filterAppend = x => x.RejectDate != null;
                }

                filter = filter.And(filterAppend);
            }

            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.CgSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.BeneSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneDoB != null || (x.BeneDoB.Value >= vm.StartDate && x.BeneDoB.Value <= vm.EndDate);
                filter = filter.And(filterAppend);

                filterAppend = x => x.CgDoB >= vm.StartDate && x.CgDoB <= vm.EndDate;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<ListingPlanHH, ListingPlanHHReportVm>>
                              firstSelector =
                                  p => new ListingPlanHHReportVm {
                                      Programme = p.Programme.Code,
                                      SubLocation = p.SubLocation.Code,
                                      Location = p.Location.Name,
                                      Id = p.Id,
                                      RegDate = p.RegDate,
                                      BeneDoB = p.BeneDoB,
                                      CgDoB = p.CgDoB,
                                      Status = p.Status.Description,
                                      HouseholdMembers = p.HouseholdMembers,
                                      BeneSurname = p.BeneSurname,
                                      BeneNationalIdNo = p.BeneNationalIdNo,
                                      BeneFirstName = p.BeneFirstName,
                                      BeneSex = p.BeneSex.Code,
                                      BeneMiddleName = p.BeneMiddleName,
                                      BenePhoneNumber = p.BenePhoneNumber,
                                      CgSurname = p.CgSurname,
                                      CgNationalIdNo = p.CgNationalIdNo,
                                      CgFirstName = p.CgFirstName,
                                      CgSex = p.CgSex.Code,
                                      CgMiddleName = p.CgMiddleName,
                                      CgPhoneNumber = p.CgPhoneNumber,
                                      Village = p.Village,
                                      TargetPlan = p.TargetPlan.Name,
                                      AppBuild = p.AppBuild,
                                      EndTime = p.EndTime,
                                      StartTime = p.StartTime,
                                      Enumerator = p.Enumerator.FirstName,
                                      SyncDate = p.SyncDate,
                                      SyncEnumerator = p.SyncEnumerator.FirstName,
                                      Latitude = p.Latitude,
                                      Longitude = p.Longitude,
                                      Months = p.Months,
                                      Years = p.Years,
                                      NearestReligiousBuilding = p.NearestReligiousBuilding,
                                      NearestSchool = p.NearestSchool,
                                      PhysicalAddress = p.PhysicalAddress,
                                      RejectDate = p.RejectDate,
                                      RejectReason = p.RejectReason,
                                      UniqueId = p.UniqueId
                                  };

                        Expression<Func<ListingPlanHHReportVm, int?>> orderBy = v => -v.Id;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Listing Report");
                                var ws = wb.Worksheet("Household Listing Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("MonthlyActivityDetailFullReport.xlsx");
                            }
                        }
                        else
                        {
                            vm.HouseholdListingDetails = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<ListingPlanHH, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.SubLocation.LocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.SubLocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                )?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Monthly Activity Summary Report");
                                    var ws = wb.Worksheet("Monthly Activity Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Monthly_Activity_Summary_Report.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.RejectedId = this.BooleanSelectList(vm.RejectedId);
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdListingException(TargetingReportVm vm)
        {
            Expression<Func<ListingException, bool>> filter = x => x.DateValidated >= vm.StartDate && x.DateValidated <= vm.EndDate;
            Expression<Func<ListingException, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ListingPlanHh.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.ListingPlanHh.CgSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.ListingPlanHh.BeneSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.ListingPlanHh.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.ListingPlanHh.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.ListingPlanHh.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.ListingPlanHh.BeneDoB != null || (x.ListingPlanHh.BeneDoB.Value >= vm.StartDate && x.ListingPlanHh.BeneDoB.Value <= vm.EndDate);
                filter = filter.And(filterAppend);

                filterAppend = x => x.ListingPlanHh.CgDoB >= vm.StartDate && x.ListingPlanHh.CgDoB <= vm.EndDate;
                filter = filter.And(filterAppend);
            }
            if (vm.CgDobYearMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgDobYearMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.CgDobMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgDobMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CgIdNoExists != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgIdNoExists.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.CgSexMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgSexMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneDobYearMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneDobYearMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneDobMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneDobMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            if (vm.BeneIdNoExists != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneIdNoExists.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneSexMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneSexMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        var from = new ListingException();
                        var to = new ListingExceptionReportVm();
                        new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(from, to);

                        Expression<Func<ListingException, ListingExceptionReportVm>> firstSelector = p => to;
                        Expression<Func<ListingException, int?>> orderBy = v => -v.Id;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), null, null, null)).ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Listing Exception Report");
                                var ws = wb.Worksheet("Household Listing Exception Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Household Listing Exception.xlsx");
                            }
                        }
                        else
                        {
                            vm.HouseholdListingExceptions = ((await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), null, null, null).ConfigureAwait(false))).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<ListingException, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.ListingPlanHh.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.ListingPlanHh.SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.ListingPlanHh.SubLocation.LocationId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.ListingPlanHh.SubLocationId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).AsEnumerable()?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Household Listing Exception Summary Report");
                                    var ws = wb.Worksheet("Household Listing Exception Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Household Listing Exception Summary Report.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.CgSexMatches = this.BooleanSelectList(vm.CgSexMatches);
            ViewBag.CgDobYearMatches = this.BooleanSelectList(vm.CgDobYearMatches);
            ViewBag.CgDobMatches = this.BooleanSelectList(vm.CgDobMatches);
            ViewBag.CgIdNoExists = this.BooleanSelectList(vm.CgIdNoExists);
            ViewBag.BeneSexMatches = this.BooleanSelectList(vm.BeneSexMatches);
            ViewBag.BeneDobYearMatches = this.BooleanSelectList(vm.BeneDobYearMatches);
            ViewBag.BeneDobMatches = this.BooleanSelectList(vm.BeneDobMatches);
            ViewBag.BeneIdNoExists = this.BooleanSelectList(vm.BeneIdNoExists);

            ViewBag.BeneNameMatch = this.NameMatch(vm.BeneNameMatch);
            ViewBag.CgNameMatch = this.NameMatch(vm.CgNameMatch);

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdListingCommunityValidation(TargetingReportVm vm)
        {
            Expression<Func<ComValListingPlanHH, bool>> filter = x => x.ComValDate >= vm.StartDate && x.ComValDate <= vm.EndDate;
            Expression<Func<ComValListingPlanHH, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.CgSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.BeneSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneDoB != null || (x.BeneDoB.Value >= vm.StartDate && x.BeneDoB.Value <= vm.EndDate);
                filter = filter.And(filterAppend);

                filterAppend = x => x.CgDoB >= vm.StartDate && x.CgDoB <= vm.EndDate;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<ComValListingPlanHH, ComValListingPlanHHReportVm>>
                              firstSelector =
                                  p => new ComValListingPlanHHReportVm {
                                      Programme = p.Programme.Code,
                                      SubLocation = p.SubLocation.Code,
                                      Location = p.Location.Name,
                                      Id = p.Id,
                                      RegDate = p.RegDate,
                                      BeneDoB = p.BeneDoB,
                                      CgDoB = p.CgDoB,
                                      Status = p.Status.Description,
                                      HouseholdMembers = p.HouseholdMembers,
                                      BeneSurname = p.BeneSurname,
                                      BeneNationalIdNo = p.BeneNationalIdNo,
                                      BeneFirstName = p.BeneFirstName,
                                      BeneSex = p.BeneSex.Code,
                                      BeneMiddleName = p.BeneMiddleName,
                                      BenePhoneNumber = p.BenePhoneNumber,
                                      CgSurname = p.CgSurname,
                                      CgNationalIdNo = p.CgNationalIdNo,
                                      CgFirstName = p.CgFirstName,
                                      CgSex = p.CgSex.Code,
                                      CgMiddleName = p.CgMiddleName,
                                      CgPhoneNumber = p.CgPhoneNumber,
                                      Village = p.Village,
                                      TargetPlan = p.TargetPlan.Name,
                                      AppBuild = p.AppBuild,
                                      EndTime = p.EndTime,
                                      StartTime = p.StartTime,
                                      Enumerator = p.Enumerator.FirstName,
                                      SyncDate = p.SyncDate,
                                      SyncEnumerator = p.SyncEnumerator.FirstName,
                                      Latitude = p.Latitude,
                                      Longitude = p.Longitude,
                                      Months = p.Months,
                                      Years = p.Years,
                                      NearestReligiousBuilding = p.NearestReligiousBuilding,
                                      NearestSchool = p.NearestSchool,
                                      PhysicalAddress = p.PhysicalAddress,
                                      ComValDate = p.ComValDate,
                                      UniqueId = p.UniqueId
                                  };

                        Expression<Func<ComValListingPlanHHReportVm, int?>> orderBy = v => -v.Id;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Listing Report");
                                var ws = wb.Worksheet("Household Listing Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Community_Valdation_Household_Listing.xlsx");
                            }
                        }
                        else
                        {
                            vm.ComValListingPlanDetails = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<ComValListingPlanHH, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.SubLocation.LocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.SubLocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Community Validation Summary Report");
                                    var ws = wb.Worksheet("Community Validation Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Community_Valdation_Household_Listing_Summary.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdListingCommunityValidationException(TargetingReportVm vm)
        {
            Expression<Func<ComValListingException, bool>> filter = x => x.DateValidated >= vm.StartDate && x.DateValidated <= vm.EndDate;
            Expression<Func<ComValListingException, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ListingPlanHh.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.ListingPlanHh.CgSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.ListingPlanHh.BeneSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.ListingPlanHh.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.ListingPlanHh.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.ListingPlanHh.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.ListingPlanHh.BeneDoB != null || (x.ListingPlanHh.BeneDoB.Value >= vm.StartDate && x.ListingPlanHh.BeneDoB.Value <= vm.EndDate);
                filter = filter.And(filterAppend);

                filterAppend = x => x.ListingPlanHh.CgDoB >= vm.StartDate && x.ListingPlanHh.CgDoB <= vm.EndDate;
                filter = filter.And(filterAppend);
            }
            if (vm.CgDobYearMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgDobYearMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.CgDobMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgDobMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CgIdNoExists != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgIdNoExists.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.CgSexMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.CgSexMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneDobYearMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneDobYearMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneDobMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneDobMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            if (vm.BeneIdNoExists != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneIdNoExists.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }
            if (vm.BeneSexMatches != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.BeneSexMatches.Value }, 0);
                filterAppend = x => x.Bene_DoBYearMatch == success;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<ComValListingException, ComValListingExceptionReportVm>> firstSelector = Mapper.Map<Expression<Func<ComValListingException, ComValListingExceptionReportVm>>>(new ComValListingExceptionReportVm());
                        Expression<Func<ComValListingExceptionReportVm, int?>> orderBy = v => -v.Id;

                        if (vm.IsExportCsv)
                        {
                            var list = ((await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), null, null, null).ConfigureAwait(false)));
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Listing Exception Report");
                                var ws = wb.Worksheet("Household Listing Exception Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Household Listing Exception.xlsx");
                            }
                        }
                        else
                        {
                            vm.ComValListingExceptions = ((await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), null, null, null).ConfigureAwait(false))).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<ComValListingException, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.ListingPlanHh.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.ListingPlanHh.SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.ListingPlanHh.SubLocation.LocationId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.ListingPlanHh.SubLocationId,
                                    BeneSexId = h.BeneSex,
                                    CgSexId = h.CgSex,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).AsEnumerable()?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Household Listing Exception Summary Report");
                                    var ws = wb.Worksheet("Household Listing Exception Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Household Listing Exception Summary Report.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.CgSexMatches = this.BooleanSelectList(vm.CgSexMatches);
            ViewBag.CgDobYearMatches = this.BooleanSelectList(vm.CgDobYearMatches);
            ViewBag.CgDobMatches = this.BooleanSelectList(vm.CgDobMatches);
            ViewBag.CgIdNoExists = this.BooleanSelectList(vm.CgIdNoExists);
            ViewBag.BeneSexMatches = this.BooleanSelectList(vm.BeneSexMatches);
            ViewBag.BeneDobYearMatches = this.BooleanSelectList(vm.BeneDobYearMatches);
            ViewBag.BeneDobMatches = this.BooleanSelectList(vm.BeneDobMatches);
            ViewBag.BeneIdNoExists = this.BooleanSelectList(vm.BeneIdNoExists);
            ViewBag.CgNameMatch = this.NameMatch(vm.CgNameMatch);
            ViewBag.BeneNameMatch = this.NameMatch(vm.BeneNameMatch);

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");

            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);

            return View(vm);
        }

        public async Task<ActionResult> Registration(TargetingReportVm vm)
        {
            Expression<Func<HouseholdReg, bool>> filter = x => x.RegistrationDate >= vm.StartDate && x.RegistrationDate <= vm.EndDate;
            Expression<Func<HouseholdReg, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            /*
            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRegId == x.Id).SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.HouseholdRegMembers
                    .FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRegId == x.Id).SexId);
                filter = filter.And(filterAppend);
            }
            */

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.InterviewStatusId != null)
            {
                filterAppend = x => vm.InterviewStatusId.Contains(x.InterviewStatusId);
                filter = filter.And(filterAppend);
            }
            if (vm.InterviewResultId != null)
            {
                filterAppend = x => vm.InterviewResultId.Contains(x.InterviewResultId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            /*
            if (vm.DobDateRangeId != "ALL")
            {
                //filterAppend = x => x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRegId == x.Id).DateOfBirth != null || (x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRegId == x.Id).DateOfBirth >= vm.StartDate && x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRegId == x.Id).DateOfBirth <= vm.EndDate);
                //filter = filter.Or(filterAppend);
                filterAppend = x => x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRegId == x.Id).DateOfBirth != null || (x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRegId == x.Id).DateOfBirth >= vm.StartDate && x.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRegId == x.Id).DateOfBirth <= vm.EndDate);
                filter = filter.And(filterAppend);
            }
            **/

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        if (vm.IsExportCsv)
                        {
                            var list = (await (GenericService.GetSearchableQueryable(filter, null, null, null)).ConfigureAwait(false));
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Registration Report");
                                var ws = wb.Worksheet("Household Registration Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Registration.xlsx");
                            }
                        }
                        else
                        {
                            vm.HouseholdRegs = (await GenericService.GetSearchableQueryable(filter, null, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<HouseholdReg, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.SubLocation.Constituency.CountyId
                                    /*
                                     * ,
                                    BeneSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRegId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRegId == h.Id).Sex.Code,
                                    */
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.SubLocation.ConstituencyId
                                    /*,
                                    BeneSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRegId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRegId == h.Id).Sex.Code,
                                    */
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.SubLocation.LocationId
                                    /*,
                                    BeneSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRegId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRegId == h.Id).Sex.Code,
                                    */
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.SubLocationId
                                    /*,
                                    BeneSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRegId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRegMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRegId == h.Id).Sex.Code,
                                    */
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Household_Registration_Summary Report");
                                    var ws = wb.Worksheet("Household_Registration_Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Household_Registration_Summary.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> Recertification(TargetingReportVm vm)
        {
            Expression<Func<HouseholdRec, bool>> filter = x => x.RegistrationDate >= vm.StartDate && x.RegistrationDate <= vm.EndDate;
            Expression<Func<HouseholdRec, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.CgSexId != null)
            {
                filterAppend = x => vm.CgSexId.Contains(x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRecId == x.Id).SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.BeneSexId != null)
            {
                filterAppend = x => vm.BeneSexId.Contains(x.HouseholdRecMembers
                    .FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRecId == x.Id).SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                //filterAppend = x => x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRecId == x.Id).DateOfBirth != null || (x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRecId == x.Id).DateOfBirth >= vm.StartDate && x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.BeneId && y.HouseholdRecId == x.Id).DateOfBirth <= vm.EndDate);
                //filter = filter.Or(filterAppend);
                filterAppend = x => x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRecId == x.Id).DateOfBirth != null || (x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRecId == x.Id).DateOfBirth >= vm.StartDate && x.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == x.CgId && y.HouseholdRecId == x.Id).DateOfBirth <= vm.EndDate);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        if (vm.IsExportCsv)
                        {
                            var list = (await (GenericService.GetSearchableQueryable(filter, null, null, null)).ConfigureAwait(false));
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Household Registration Report");
                                var ws = wb.Worksheet("Household Registration Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Registration.xlsx");
                            }
                        }
                        else
                        {
                            vm.HouseholdRecs = (await GenericService.GetSearchableQueryable(filter, null, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<HouseholdRec, SummaryReportMin>> firstSelector = null;
                        Func<SummaryReportMin, int?> groupSelector = null;
                        Expression<Func<SummaryReportMin, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReportMin>, GenericSummaryReportMin> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    CountyId = h.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRecId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRecId == h.Id).Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    ConstituencyId = h.SubLocation.ConstituencyId,
                                    BeneSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRecId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRecId == h.Id).Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    LocationId = h.SubLocation.LocationId,
                                    BeneSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRecId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRecId == h.Id).Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReportMin {
                                    SubLocationId = h.SubLocationId,
                                    BeneSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.BeneId && y.HouseholdRecId == h.Id).Sex.Code,
                                    CgSexId = h.HouseholdRecMembers.FirstOrDefault(y => y.MemberId == h.CgId && y.HouseholdRecId == h.Id).Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            selector = g => new GenericSummaryReportMin {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Household_Registration_Summary Report");
                                    var ws = wb.Worksheet("Household_Registration_Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Household_Registration_Summary.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModelMins = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModelMin {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> RecertificationExceptions(TargetingReportVm vm)
        {
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> RegistrationStatus(TargetingReportVm vm)
        {
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> RegistrationProfiles(TargetingReportVm vm)
        {
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> RecertificationStatus(TargetingReportVm vm)
        {
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> RecertificationProfiles(TargetingReportVm vm)
        {
            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "PSP");
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);

            ViewBag.InterviewStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Status").ConfigureAwait(true), "Id", "Description", vm.InterviewStatusId);
            ViewBag.InterviewResultId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Interview Result").ConfigureAwait(true), "Id", "Description", vm.InterviewResultId);

            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.CgSexId = new SelectList(sex, "Id", "Description", vm.CgSexId);
            ViewBag.BeneSexId = new SelectList(sex, "Id", "Description", vm.BeneSexId);
            ViewBag.HasOrphans = this.BooleanSelectList(vm.HasOrphans);
            ViewBag.HasDisability = this.BooleanSelectList(vm.HasDisability);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }
    }
}