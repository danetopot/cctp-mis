﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Monitoring;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class BeneficiaryStatementController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;
        public IEmailService EmailService;

        public BeneficiaryStatementController(IGenericService genericService, IEmailService emailService,
            ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            LogService = logService;
        }

        public async Task<ActionResult> Index(BeneStatementFilterViewModel model)
        {
            Expression<Func<Household, bool>> filter = x => x.CreatedOn <= DateTime.Now; // && x.HouseholdMembers.Any(y => y.Status.Id == 29);
            Expression<Func<Household, bool>> filterAppend = null;

            if (model.NationalId != null || model.EnrolmentNo != null || model.AccountNo != null || model.ProgrammeNo != null || !string.IsNullOrEmpty(model.Name) || model.ConstituencyId != null || model.CountyId != null)
            {

                if (model.EnrolmentNo != null)
                {
                    filterAppend = x => x.HouseholdEnrolment.Any(y => y.Id == model.EnrolmentNo.Value);
                    filter = filter.And(filterAppend);
                }

                if (model.ProgrammeNo != null)
                {
                    if (model.ProgrammeNo.Length > 6)
                    {
                        var prefix = model.ProgrammeNo.Substring(0, 1);
                        var beneNo = int.Parse(model.ProgrammeNo.Substring(1, 6));

                        filterAppend = x => x.HouseholdEnrolment.Any(y => y.ProgrammeNo == beneNo && y.BeneProgNoPrefix == prefix);
                        filter = filter.And(filterAppend);
                    }
                }

                if (model.NationalId != null)
                {
                    filterAppend = x => x.HouseholdMembers.Any(y => y.Person.NationalIdNo == model.NationalId);
                    filter = filter.And(filterAppend);
                }

                if (model.AccountNo != null)
                {
                    filterAppend = x => x.HouseholdEnrolment.Any(v => v.BeneficiaryAccounts.Any(c => c.AccountNo == model.AccountNo));
                    filter = filter.And(filterAppend);
                }

                if (!string.IsNullOrEmpty(model.Name))
                {
                    filterAppend = b => b.HouseholdMembers.Any(x => x.Person.FirstName.Contains(model.Name.Trim())) ||
                                        b.HouseholdMembers.Any(x => x.Person.MiddleName.Contains(model.Name.Trim())) ||
                                        b.HouseholdMembers.Any(x => x.Person.Surname.Contains(model.Name.Trim()));

                    filter = filter.And(filterAppend);
                }

                var page = model.Page ?? 1;
                var pageSize = model.PageSize ?? 10;

                model.Households = ((await GenericService.GetSearchableQueryable(filter,
                    x => x.OrderBy(z => z.HouseholdEnrolment.FirstOrDefault().BeneProgNoPrefix).ThenBy(z => z.HouseholdEnrolment.FirstOrDefault().ProgrammeNo),
                    "CreatedByUser,HouseholdSubLocation,HouseholdEnrolment,Programme.PrimaryRecipient,Programme.SecondaryRecipient,HouseholdMembers.Person.Sex,HouseholdMembers.Status,HouseholdMembers.Relationship,HouseholdMembers.MemberRole,Status", null, null).ConfigureAwait(false)).ToPagedList(page, pageSize));
            }

            return View(model);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public async Task<ActionResult> Filter(BeneStatementFilterViewModel model)
        {
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            if (model.NationalId != null || model.ProgrammeNo != null || !string.IsNullOrEmpty(model.Name) ||
                model.ConstituencyId != null || model.CountyId != null)
            {
                var households =
                        db.Households.AsNoTracking()
                            .Include(b => b.Status).Include(b => b.CreatedByUser)
                            .Include(b => b.HouseholdSubLocation)
                            .Include(b => b.HouseholdEnrolment)
                            .Include(b => b.Programme.PrimaryRecipient)
                            .Include(b => b.Programme.SecondaryRecipient)
                            .Include(b => b.HouseholdMembers.Select(x => x.Person.Sex))
                            .Include(b => b.HouseholdMembers.Select(x => x.Status))
                            .Include(b => b.HouseholdMembers.Select(x => x.Relationship));

                if (model.ProgrammeNo != null)
                {
                    if (model.ProgrammeNo.Length > 6)
                    {
                        var prefix = model.ProgrammeNo.Substring(0, 1);
                        var beneNo = int.Parse(model.ProgrammeNo.Substring(1, 6));

                        households = households.Where(b => b.HouseholdEnrolment.Any(x => x.BeneProgNoPrefix == prefix && x.ProgrammeNo == beneNo));
                    }
                }

                if (model.NationalId != null)
                {
                    households = households.Where(b => b.HouseholdMembers.Any(x => x.Person.NationalIdNo == model.NationalId));
                }

                if (!string.IsNullOrEmpty(model.Name))
                {
                    households = households.Where(
                        b => b.HouseholdMembers.Any(x => x.Person.FirstName.Contains(model.Name.Trim())) ||
                             b.HouseholdMembers.Any(x => x.Person.MiddleName.Contains(model.Name.Trim())) ||
                             b.HouseholdMembers.Any(x => x.Person.Surname.Contains(model.Name.Trim())));
                }

                if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                            "Name", progOfficer.CountyId);

                    ViewBag.SubLocationId =
                        new SelectList(
                            await GenericService.GetSearchableQueryable<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                            "Id", "Name");
                }
                if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
                }

                if (!progOfficer.CountyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                            "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
                }

                var page = model.Page ?? 1;
                var pageSize = model.PageSize ?? 10;
                model.Households = households.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                        "Name", progOfficer.CountyId);

                ViewBag.SubLocationId =
                    new SelectList(
                        await GenericService.GetSearchableQueryable<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                        "Id", "Name");
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                        "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
            }

            ViewBag.ComplaintTypeId = (await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "UPDATE", n => n.OrderBy(i => i.Name)).ConfigureAwait(false)).ToList();
            return View(model);
        }

        public async Task<ActionResult> Householdmembers(int? id, int? page)
        {
            // var enrolmentId = (await GenericService.GetFirstAsync<HouseholdEnrolment>(x => x.HhId
            // == id)).HhId;
            Expression<Func<HouseholdMember, bool>> filter = x => x.HhId == id;

            var pagenm = page ?? 1;
            // filter = filter.And(filterx);

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser).Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var householdMembers = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), "Relationship,MemberRole,Person,Person.Sex,Status").ConfigureAwait(false))
                .ToList();
            var model = new BeneStatementModel {
                HouseholdMembers = householdMembers,
                Household = household
            };
            return View(model);
        }

        public async Task<ActionResult> Details(int? id)
        {
            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Programme)
                    .Include(b => b.HouseholdSubLocation)
                    .Include(b => b.HouseholdEnrolment)
                    .Include(b => b.RegGroup)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.HouseholdEnrolment.Select(x => x.HouseholdEnrolmentPlan))
                    .Include(b => b.Programme.PrimaryRecipient)
                    .Include(b => b.Programme.SecondaryRecipient)
                    .Include(b => b.HouseholdMembers.Select(x => x.Person.Sex))
                    .Include(b => b.HouseholdMembers.Select(x => x.Status))
                    .Include(b => b.HouseholdMembers.Select(x => x.Relationship))
                    .Single();

            var spName = "GetBeneStatementSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var beneSummary = GenericService.GetOneBySp<BeneficiarySummary>(spName, parameterNames, parameterList);

            var model = new BeneStatementModel {
                Household = household,
                Summary = beneSummary
            };
            return View(model);
        }

        public async Task<ActionResult> Enrolment(int id)
        {
            var spName = "GetBeneHouseholdEnrolmentInfo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var caseIssues = GenericService.GetManyBySp<GetBeneInfoVm>(spName, parameterNames, parameterList).ToList();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var model = new BeneStatementModel {
                Household = household,
                GetBeneInfoVm = caseIssues
            };

            return View(model);
        }

        public async Task<ActionResult> Updates(int id, int? page)
        {
            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var pagenm = page ?? 1;
            var pageSize = 20;

            var model = new BeneStatementModel();
            try
            {
                var enrolmentId = (await GenericService.GetFirstAsync<HouseholdEnrolment>(x => x.HhId == id)).Id;
                Expression<Func<Change, bool>> filter = x => x.HhEnrolmentId == enrolmentId;

                var changes = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id),
                        "ChangeStatus,ChangeType,Programme,ChangeLevel,HhEnrolment,CreatedByUser").ConfigureAwait(false))
                    .ToList();

                model = new BeneStatementModel {
                    Changes = changes,
                    Household = household,
                    Id = id,
                    Page = page,
                    PageSize = pageSize
                };
                return View(model);
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "This Household has not been enrolled into the Programme";
                TempData["KEY"] = "warning";
                model = new BeneStatementModel {
                    Household = household,
                    Id = id,
                    Page = page,
                    PageSize = pageSize
                };
            }

            return View(model);
        }

        public async Task<ActionResult> Complaints(int id, int? page)
        {
            var pagenm = page ?? 1;
            var pageSize = 20;
            var model = new BeneStatementModel();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            try
            {
                var enrolmentId = (await GenericService.GetFirstAsync<HouseholdEnrolment>(x => x.HhId == id)).Id;
                Expression<Func<Complaint, bool>> filter = x => x.HhEnrolmentId == enrolmentId;

                var complaints = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), "ComplaintStatus,ComplaintType,Programme,ComplaintLevel").ConfigureAwait(false)).ToList();

                model = new BeneStatementModel {
                    Complaints = complaints,
                    Household = household,
                    Id = id,
                    Page = page,
                    PageSize = pageSize
                };
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "This Household has not been enrolled into the Programme";
                TempData["KEY"] = "warning";

                model = new BeneStatementModel {
                    Household = household,
                    Id = id,
                    Page = page,
                    PageSize = pageSize
                };
            }
            return View(model);
        }

        public async Task<ActionResult> Print(int id)
        {
            Expression<Func<HouseholdMember, bool>> filter = x => x.HhId == id;
            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();

            var spName = "GetBenePrepayrollSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var benePrepayrollStatements = GenericService.GetManyBySp<BenePrepayrollStatement>(spName, parameterNames, parameterList).ToList();

            spName = "GetBeneHouseholdEnrolmentInfo";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var GetBeneInfoVms = GenericService.GetManyBySp<GetBeneInfoVm>(spName, parameterNames, parameterList).ToList();

            spName = "GetBenePayrollSummary";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var benePayrollStatements = GenericService.GetManyBySp<BenePayrollStatement>(spName, parameterNames, parameterList).ToList();

            spName = "GetBeneStatementCreditReport";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var beneCreditStatements = GenericService.GetManyBySp<BeneCreditStatement>(spName, parameterNames, parameterList).ToList();

            spName = "GetBeneStatementMonthlyStatement";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var beneMonthlyStatements = GenericService.GetManyBySp<BeneMonthlyStatement>(spName, parameterNames, parameterList).ToList();

            var householdMembers = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), "Relationship,MemberRole,Person,Person.Sex").ConfigureAwait(false))
                .ToList();

            var model = new BeneStatementModel {
                Household = household,
                BenePrepayrollStatements = benePrepayrollStatements,
                GetBeneInfoVm = GetBeneInfoVms,
                BenePayrollStatements = benePayrollStatements,
                BeneCreditStatements = beneCreditStatements,
                BeneMonthlyStatements = beneMonthlyStatements,
                HouseholdMembers = householdMembers
            };

            return View(model);
        }

        public ActionResult PrePayroll(int id)
        {
            var spName = "GetBenePrepayrollSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var benePrepayrollStatements = GenericService.GetManyBySp<BenePrepayrollStatement>(spName, parameterNames, parameterList).ToList();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var model = new BeneStatementModel {
                Household = household,
                BenePrepayrollStatements = benePrepayrollStatements
            };

            return View(model);
        }

        public ActionResult Payroll(int id)
        {
            var spName = "GetBenePayrollSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var benePayrollStatements = GenericService.GetManyBySp<BenePayrollStatement>(spName, parameterNames, parameterList).ToList();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var model = new BeneStatementModel {
                Household = household,
                BenePayrollStatements = benePayrollStatements
            };

            return View(model);
        }

        public ActionResult Payments(int id)
        {
            var spName = "GetBeneStatementCreditReport";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var beneCreditStatements = GenericService.GetManyBySp<BeneCreditStatement>(spName, parameterNames, parameterList).ToList();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var model = new BeneStatementModel {
                Household = household,
                BeneCreditStatements = beneCreditStatements
            };

            return View(model);
        }

        public ActionResult MonthlyStatement(int id)
        {
            var spName = "GetBeneStatementMonthlyStatement";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var beneMonthlyStatements = GenericService.GetManyBySp<BeneMonthlyStatement>(spName, parameterNames, parameterList).ToList();

            var household =
                db.Households.Where(x => x.Id == id)
                    .Include(b => b.Status).Include(b => b.CreatedByUser)
                    .Include(b => b.Source)
                    .Include(b => b.CreatedByUser)
                    .Include(b => b.Programme).Single();
            var model = new BeneStatementModel {
                Household = household,
                BeneMonthlyStatements = beneMonthlyStatements
            };

            return View(model);
        }
    }
}