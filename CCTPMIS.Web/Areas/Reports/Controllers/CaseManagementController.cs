﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Reports;
using CCTPMIS.Services;
using ClosedXML.Excel;
using ClosedXML.Extensions;
using DocumentFormat.OpenXml.Spreadsheet;
using PagedList;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class CaseManagementController : Controller
    {
        public readonly IGenericService GenericService;
        private readonly IPdfService PdfService;
        protected readonly IEmailService EmailService;
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public CaseManagementController(IGenericService genericService, EmailService emailService, PdfService pdfService)
        {
            GenericService = genericService;
            EmailService = emailService;
            PdfService = pdfService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Complaints(CaseManagementReportOption vm)
        {
            Expression<Func<Complaint, bool>> filter = x => x.Id > 0;
            Expression<Func<Complaint, bool>> filterAppend = null;
            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.SourceId != null)
            {
                filterAppend = x => vm.SourceId.Contains(x.SourceId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.SexId != null)
            {
                filterAppend = x => vm.SexId.Contains(x.ReportedBySexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.ComplaintStatusId != null)
            {
                filterAppend = x => vm.ComplaintTypeId.Contains(x.ComplaintStatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.ComplaintTypeId != null)
            {
                filterAppend = x => vm.ComplaintTypeId.Contains(x.ComplaintTypeId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            ViewBag.GroupBy = this.GroupBy2(vm.GroupBy);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = new SelectList(new List<FilterParameter> { new FilterParameter { Id = 1, Name = "Detailed" }, new FilterParameter { Id = 2, Name = "Summary" } }, "Id", "Name", vm.ReportTypeId);
            ViewBag.ComplaintCategoryId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "Case Category").ConfigureAwait(true), "Id", "Description");
            ViewBag.ComplaintTypeId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE").ConfigureAwait(true), "Id", "Name", vm.ComplaintTypeId);
            ViewBag.ComplaintLevelId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Escalation Levels").ConfigureAwait(true), "Id", "Description", vm.ComplaintLevelId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source").ConfigureAwait(true), "Id", "Description", vm.SourceId);
            ViewBag.ComplaintStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Status").ConfigureAwait(true), "Id", "Description", vm.ComplaintStatusId);
            ViewBag.SexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true), "Id", "Description", vm.SexId);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.PageSize = this.GetPager(vm.PageSize);

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<Complaint, ComplaintDetailReport>>
                                   firstSelector =
                                       p => new ComplaintDetailReport {
                                           Programme = p.Programme.Code,
                                           ComplaintType = p.ComplaintType.Name,
                                           ComplaintLevel = p.ComplaintLevel.Code,
                                           ReportedByType = p.ReportedByType.Code,
                                           ReportedBySex = p.ReportedBySex.Code,
                                           ReceivedBy = p.ReceivedBy,
                                           ReceivedOn = p.ReceivedOn,
                                           County = p.Constituency.County.Name,
                                           Constituency = p.Constituency.Name
                                       };
                        Expression<Func<ComplaintDetailReport, string>> orderBy = v => v.Programme;

                        if (vm.IsExportCsv)
                        {
                            var list = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToList();
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Complaints Report");
                                var ws = wb.Worksheet("Complaints Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Complaints_Report.xlsx");
                            }
                        }
                        else
                        {
                            vm.Complaints = (await GenericService.GetSearchableQueryable(
                            filter,
                            x => x.OrderByDescending(y => y.ReceivedOn),
                            "Programme,ComplaintLevel,ReportedByType,ReportedBySex,Constituency.County"
                             ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }

                case 2:
                    {
                        Expression<Func<Complaint, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                                    }
                            };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.Constituency.CountyId,
                                    BeneSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.ConstituencyId,
                                    BeneSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F")
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                )?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new ComplaintSummaryReport {
                                        Name = rgt.Name,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Complaints Report");
                                    var ws = wb.Worksheet("Complaints Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Complaints_Reports.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToList();
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Name = rgt.Name,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        Total = lft.Total,
                                    }).ToPagedList(vm.Page, vm.PageSize);
                            }
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;

                    }
                    
            }

            return View(vm);
        }

        public string GetXml(List<int> ints)
        {
            var serializer = new XmlSerializer(typeof(List<ListInt>), new XmlRootAttribute("Report") {
                Namespace = "",
            });
            var settings = new XmlWriterSettings {
                Indent = false,
                OmitXmlDeclaration = true,
                Encoding = Encoding.UTF8
            };
            var xml = string.Empty;
            var listInt = new List<ListInt>();
            listInt.AddRange(ints.Select(item => new ListInt { Id = item }));

            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (var sw = new StringWriter())
            {
                var xw = XmlWriter.Create(sw, settings);
                serializer.Serialize(xw, listInt, ns);
                xml += sw.ToString();
            }

            return xml;
        }

        public async Task<ActionResult> Changes(CaseManagementReportOption vm)
        {
            //Expression<Func<Change, bool>> filter = x => x.ReceivedOn >= vm.StartDate && x.ReceivedOn <= vm.EndDate;
            Expression<Func<Change, bool>> filter = x => x.Id > 0;
            Expression<Func<Change, bool>> filterAppend = null;
            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.SexId != null)
            {
                filterAppend = x => vm.SexId.Contains(x.ReportedBySexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.ChangeStatusId != null)
            {
                filterAppend = x => vm.ChangeStatusId.Contains(x.ChangeStatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.ChangeTypeId != null)
            {
                filterAppend = x => vm.ChangeTypeId.Contains(x.ChangeTypeId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    { 
                        Expression<Func<Change, ChangeDetailReport>>
                                   firstSelector =
                                       p => new ChangeDetailReport {
                                           Programme = p.Programme.Code,
                                           ChangeType = p.ChangeType.Name,
                                           ReportedByType = p.ReportedByType.Code,
                                           ReportedBySex = p.ReportedBySex.Code,
                                           ReceivedBy = p.ReceivedBy,
                                           ReceivedOn = p.ReceivedOn,
                                           SLA = (p.ChangeType.Timeline).ToString(),
                                           County = p.Constituency.County.Name,
                                           Constituency = p.Constituency.Name
                                       };
                        Expression<Func<ChangeDetailReport, string>> orderBy = v => v.Programme;

                        if (vm.IsExportCsv)
                        {
                            var list =  (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToList();
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Changes Report");
                                var ws = wb.Worksheet("Changes Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Changes_Report.xlsx");
                            }
                        }
                        else
                        {
                            vm.Changes = (await GenericService.GetAsync(
                                filter,
                                x => x.OrderByDescending(y => y.ReceivedOn),
                                "Programme, ChangeType, ReportedByType, ReportedBySex, Constituency.County"
                             ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        
                    break;
                    }
                case 2:
                    { 
                        Expression<Func<Change, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                                new ParameterEntity {
                                    ParameterTuple = new Tuple<string, object>("Type",
                                        vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                                }
                            };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.Constituency.CountyId,
                                    BeneSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.ConstituencyId,
                                    BeneSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HhEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRoleId == h.HhEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F")
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                )?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new 
                                         ChangeSummaryReportCounty {
                                      //  Name = rgt.,
                                        Name = rgt.Name,
                                      //  Constituency = rgt.Constituency,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Total = lft.Total
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Changes Report");
                                    var ws = wb.Worksheet("Changes Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Changes_Reports.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToList();
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        /*
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        */
                                        Total = lft.Total,
                                    }).ToPagedList(vm.Page, vm.PageSize);
                            }
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy2(vm.GroupBy);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = new SelectList(new List<FilterParameter> { new FilterParameter { Id = 1, Name = "Detailed" }, new FilterParameter { Id = 2, Name = "Summary" } }, "Id", "Name", vm.ReportTypeId);
            ViewBag.ChangeCategoryId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "Case Category").ConfigureAwait(true), "Id", "Description");
            ViewBag.ChangeTypeId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "UPDATE").ConfigureAwait(true), "Id", "Name", vm.ChangeTypeId);
            ViewBag.ChangeLevelId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Escalation Levels").ConfigureAwait(true), "Id", "Description", vm.ChangeLevelId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source").ConfigureAwait(true), "Id", "Description", vm.SourceId);
            ViewBag.ChangeStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Change Status").ConfigureAwait(true), "Id", "Description", vm.ChangeStatusId);
            ViewBag.SexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true), "Id", "Description", vm.SexId);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);

            ViewBag.PageSize = this.GetPager(vm.PageSize);

            return View(vm);
        }

        public async Task<ActionResult> EFCs(CaseManagementReportOption vm)
        {
            ViewBag.GroupBy = this.GroupBy2(vm.GroupBy);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = new SelectList(new List<FilterParameter> { new FilterParameter { Id = 1, Name = "Detailed" }, new FilterParameter { Id = 2, Name = "Summary" } }, "Id", "Name", vm.ReportTypeId);
            ViewBag.EFCCategoryId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "EFC Types").ConfigureAwait(true), "Id", "Description");
            ViewBag.EFCTypeId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "EFC").ConfigureAwait(true), "Id", "Name", vm.EFCTypeId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(true), "Id", "Description", vm.SourceId);
            ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Status" && (x.Code == "OPEN" || x.Code == "CLOSED")).ConfigureAwait(true), "Id", "Description", vm.StatusId);
            ViewBag.SexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true), "Id", "Description", vm.SexId);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            ViewBag.PageSize = this.GetPager(vm.PageSize);

            Expression<Func<EFC, bool>> filter = x => x.Id > 0;
            Expression<Func<EFC, bool>> filterAppend = null;
            if (vm.EFCTypeId != null)
            {
                filterAppend = x => vm.EFCTypeId.Contains(x.EFCTypeId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.SexId != null)
            {
                filterAppend = x => vm.SexId.Contains(x.ReportedBySexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<EFC, EFCDetailReport>>
                                   firstSelector =
                                       p => new EFCDetailReport {
                                           SerialNo = p.SerialNo,
                                           EFCType = p.EFCType.Description,
                                           ReportedByType = p.ReportedByType.Code,
                                           ReportedBySex = p.ReportedBySex.Code,
                                           ReceivedBy = p.ReceivedBy,
                                           ReceivedOn = p.ReportedDate,
                                           Status = p.Status.Description,
                                           County = p.Constituency.County.Name,
                                           Constituency = p.Constituency.Name

                                       };
                        Expression<Func<EFCDetailReport, int?>> orderBy = v => v.SerialNo;

                        if (vm.IsExportCsv)
                        {
                            var list = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToList();
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("EFC Report");
                                var ws = wb.Worksheet("EFC Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("EFC_Report.xlsx");
                            }
                        }
                        else
                        {
                            vm.EFCs = (await GenericService.GetAsync(
                                filter,
                                x => x.OrderBy(y => y.Id),
                                "EFCType,Status, Source, ReportedByType, ReportedBySex, Constituency.County"
                             ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        }

                        break;
                    }

                case 2:
                    {
                        Expression<Func<EFC, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                                }
                            };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.Constituency.CountyId,
                                    BeneSexId = h.ReportedBySex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.ConstituencyId,
                                    BeneSexId = h.ReportedBySex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F")
                                //MaleAmount = g.Sum(b => b.MaleAmount),
                                //FemaleAmount = g.Sum(b => b.FemaleAmount),
                                //TotalAmount = g.Sum(b => b.TotalAmount)
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                )?.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new EFCSummaryReport {
                                        Name = rgt.Name,
                                        County = rgt.County,
                                        Constituency = rgt.Constituency,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Total = lft.Total
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("EFCs Report");
                                    var ws = wb.Worksheet("EFCs Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("EFCs_Reports.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToList();
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        /*
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        */
                                        Total = lft.Total,
                                    }).ToPagedList(vm.Page, vm.PageSize);
                            }
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            return View(vm);
        }

        public async Task<ActionResult> MultipleChanges(CaseManagementReportOption vm)
        {
            ViewBag.GroupBy = this.GroupBy2(vm.GroupBy);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = new SelectList(new List<FilterParameter> { new FilterParameter { Id = 1, Name = "Detailed" }, new FilterParameter { Id = 2, Name = "Summary" } }, "Id", "Name", vm.ReportTypeId);
            ViewBag.ChangeCategoryId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "Case Category").ConfigureAwait(true), "Id", "Description");
            ViewBag.ChangeTypeId = new SelectList(await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "UPDATE").ConfigureAwait(true), "Id", "Name", vm.ChangeTypeId);
            ViewBag.ChangeLevelId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Escalation Levels").ConfigureAwait(true), "Id", "Description", vm.ChangeLevelId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source").ConfigureAwait(true), "Id", "Description", vm.SourceId);
            ViewBag.ChangeStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Change Status").ConfigureAwait(true), "Id", "Description", vm.ChangeStatusId);
            ViewBag.SexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true), "Id", "Description", vm.SexId);
            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            ViewBag.PageSize = new SelectList(new List<FilterParameter> {
                new FilterParameter { Id = 10, Name = "10"  },
                new FilterParameter { Id = 20, Name = "20" },
                new FilterParameter { Id = 50, Name = "50" },
                new FilterParameter { Id = 100, Name = "100"},
                new FilterParameter { Id = 1000, Name = "1000"},
            }, "Id", "Name", vm.PageSize);

            var spName = "RptGetChangeMultiple";
            var parameterNames = "@DateRange,@StartDate,@EndDate,@ProgrammeXml,@SexXml,@ChangeTypeXml,@ChangeStatusXml,@CountyXml,@ConstituencyXml,@Page,@PageSize,@Type";

            if (vm.ReportTypeId == 1)
            {
                var parameterList = new List<ParameterEntity> {
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("StartDate",
                            vm.StartDate ?? (object) DBNull.Value),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("EndDate",
                            vm.EndDate ?? (object) DBNull.Value),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                            vm.ProgrammeId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ProgrammeId)),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("SexXml",
                            vm.SexId == null ? (object) DBNull.Value : GetXml((List<int>)vm.SexId)),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ChangeTypeXml",
                            vm.ChangeTypeId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ChangeTypeId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ChangeStatusXml",
                            vm.ChangeStatusId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ChangeStatusId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("CountyXml",
                            vm.CountyId == null ? (object) DBNull.Value : GetXml((List<int>)vm.CountyId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                            vm.ConstituencyId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ConstituencyId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("Page",
                            vm.Page ),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("PageSize",
                            vm.PageSize ),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("Type", "DETAILS"  ),
                    }
                };

                if (vm.IsExportCsv)
                {
                    var list = GenericService.GetManyBySp<MultipleUpdateDetailReport>(spName, parameterNames, parameterList).ToList();

                    using (var wb = new XLWorkbook())
                    {
                        wb.AddWorksheet("Multiple Updates Report");
                        var ws = wb.Worksheet("Multiple Updates Report");
                        if (list?.Count() > 0)
                        {
                            var titles = list.First()
                                .GetType()
                                .GetProperties()
                                .Select(p => p.Name).ToList();
                            var i = 0;
                            foreach (var x in titles)
                            {
                                i++;
                                ws.Cell(1, i).Value = x;
                            }
                            ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                        }

                        ws.Cell(2, 1).InsertData(list);

                        return wb.Deliver("Multiple_Updates_Reports.xlsx");
                    }
                }
                else
                {
                    vm.DetailChanges = GenericService.GetManyBySp<ReportDetailChange>(spName, parameterNames, parameterList).ToPagedList(vm.Page, vm.PageSize);
                }
            }
            else if (vm.ReportTypeId == 2)
            {
                var parameterList = new List<ParameterEntity> {
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("StartDate",
                            vm.StartDate ?? (object) DBNull.Value),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("EndDate",
                            vm.EndDate ?? (object) DBNull.Value),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                            vm.ProgrammeId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ProgrammeId)),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("SexXml",
                            vm.SexId == null ? (object) DBNull.Value : GetXml((List<int>)vm.SexId)),
                    },

                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ChangeTypeXml",
                            vm.ChangeTypeId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ChangeTypeId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ChangeStatusXml",
                            vm.ChangeStatusId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ChangeStatusId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("CountyXml",
                            vm.CountyId == null ? (object) DBNull.Value : GetXml((List<int>)vm.CountyId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                            vm.ConstituencyId == null ? (object) DBNull.Value : GetXml((List<int>)vm.ConstituencyId)),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("Page",
                            vm.Page  ),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("PageSize",
                            vm.PageSize  ),
                    },
                    new ParameterEntity {
                        ParameterTuple = new Tuple<string, object>("Type", "SUMMARY"  ),
                    }
                };


                if (vm.IsExportCsv)
                {
                    var list = GenericService.GetManyBySp<MultipleUpdateSummaryReport>(spName, parameterNames, parameterList).ToList();

                    using (var wb = new XLWorkbook())
                    {
                        wb.AddWorksheet("Multiple Updates Report");
                        var ws = wb.Worksheet("Multiple Updates Report");
                        if (list?.Count() > 0)
                        {
                            var titles = list.First()
                                .GetType()
                                .GetProperties()
                                .Select(p => p.Name).ToList();
                            var i = 0;
                            foreach (var x in titles)
                            {
                                i++;
                                ws.Cell(1, i).Value = x;
                            }
                            ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                        }

                        ws.Cell(2, 1).InsertData(list);

                        return wb.Deliver("Multiple_Updates_Reports.xlsx");
                    }
                }
                else
                {
                    vm.SummaryChanges = GenericService.GetManyBySp<ReportSummaryChange>(spName, parameterNames, parameterList).ToPagedList(vm.Page, vm.PageSize);
                }
            }

            return View(vm);
        }
    }
}