﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Legacy;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class LegacyBeneficiaryStatementController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private LegacyDbContext legacyDb = new LegacyDbContext();
        protected readonly IGenericService GenericService;
        protected readonly GenericService LegacyGenericService;
        private readonly ILogService LogService;
        public IEmailService EmailService;

        public LegacyBeneficiaryStatementController(IGenericService genericService, IEmailService emailService,
            ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            LogService = logService;
            LegacyGenericService = new GenericService(new GenericRepository<LegacyDbContext>(new LegacyDbContext()));
        }

        // GET: Reports/LegacyBeneficiaryStatement
        public async Task<ActionResult> Index(LegacyBeneStatementFilterViewModel model)
        {
            if (!string.IsNullOrEmpty(model.NationalId) ||
                !string.IsNullOrEmpty(model.ProgrammeNo))
            {
                Expression<Func<HhRegistration, bool>> filter = x => x.Id > 0;
                Expression<Func<HhRegistration, bool>> filterx = null;
                var page = model.Page ?? 1;

                if (!string.IsNullOrEmpty(model.NationalId))
                {
                    filterx = y => y.HhMembers.Any(x => x.NationalIdNo.Equals(model.NationalId.Trim()));
                    filter = filter.And(filterx);
                }

                if (!string.IsNullOrEmpty(model.ProgrammeNo))
                {
                    filterx = y => y.ProgrammeNumber.Equals(model.ProgrammeNo);
                    filter = filter.And(filterx);
                }

                var households = (await LegacyGenericService.GetAsync(filter).ConfigureAwait(false)).ToList();
                ;

                model.Households = households;
            }

            return View(model);
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Expression<Func<HhRegistration, bool>> filter = x => x.Id == id.Value;

            var model = (await LegacyGenericService.GetOneAsync(filter, "HHMembers,Payments,CaseUpdates,CaseGrievances,Payments").ConfigureAwait(false));

            return View(model);
        }
    }
}