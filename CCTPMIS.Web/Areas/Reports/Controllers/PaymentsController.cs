﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.WebControls.Expressions;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Reports;
using CCTPMIS.Services;
using ClosedXML.Excel;
using ClosedXML.Extensions;
using CsvHelper;
using iTextSharp.text.pdf.spatial.objects;
using PagedList;
using Filter = DocumentFormat.OpenXml.Spreadsheet.Filter;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class PaymentsController : Controller
    {
        public readonly IGenericService GenericService;

        protected readonly IEmailService EmailService;

        // private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IPdfService PdfService;

        public PaymentsController(IGenericService genericService, EmailService emailService, PdfService pdfService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            PdfService = pdfService;
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Prepayroll(PrepayrollReportOption vm)
        {
            Expression<Func<Prepayroll, bool>> filter = x =>
                x.PaymentCycle.CreatedOn > vm.StartDate && x.PaymentCycle.CreatedOn < vm.EndDate;
            Expression<Func<Prepayroll, bool>> filterAppend = null;
            Expression<Func<Prepayroll, bool>> subFilter = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.BeneSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.CgSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneDoB > vm.DobStartDate && x.BeneDoB < vm.DobEndDate;
                filter = filter.And(filterAppend);
            }

            if (vm.HasArrears != null)
            {
                filterAppend = vm.HasArrears == 1 ? (x => (x.EntitlementAmount + x.AdjustmentAmount) != 4000) : (Expression<Func<Prepayroll, bool>>)(x => (x.EntitlementAmount + x.AdjustmentAmount) == 4000);
                filter = filter.And(filterAppend);
            }

            if (vm.ExceptionTypeId != null)
            {
                if (vm.ExceptionTypeId.Contains("INVALIDBENEID") || vm.ExceptionTypeId.Contains("INVALIDCGID"))
                {
                    subFilter = x =>
                        x.PrepayrollInvalidIds.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;

                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("SUSPICIOUS"))
                {
                    subFilter = x =>
                    x.PrepayrollSuspicious.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("DUPLICATE"))
                {
                    subFilter = x =>
                    x.PrepayrollDuplicateIds.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INELIGIBLE"))
                {
                    subFilter = x =>
                        x.PrepayrollIneligibles.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDACC"))
                {
                    subFilter = x =>
                    x.PrepayrollInvalidPaymentAccounts.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDCARD"))
                {
                    subFilter = x =>
                        x.PrepayrollInvalidPaymentCards.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        vm.Prepayrolls = (await GenericService.GetSearchableQueryable(
                            filter,
                            x => x.OrderBy(y => y.PaymentCycleId),
                            "PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth,PaymentZone,HHStatus,CgSex,BeneSex,BeneficiaryPaymentCard,Household.HouseholdEnrolment,PaymentCycle,Programme,BeneficiaryAccount.Status,BeneficiaryAccount.PspBranch.Psp,BeneficiaryAccount,BeneficiaryAccount.PspBranch"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
                case 2:
                    {
                        Expression<Func<Prepayroll, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                    MaleAmount = h.BeneSex.Code == "M" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    FemaleAmount = h.BeneSex.Code == "F" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    TotalAmount = (h.AdjustmentAmount + h.EntitlementAmount)
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                    MaleAmount = h.BeneSex.Code == "M" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    FemaleAmount = h.BeneSex.Code == "F" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    TotalAmount = (h.AdjustmentAmount + h.EntitlementAmount)
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.SubLocation.LocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                    MaleAmount = h.BeneSex.Code == "M" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    FemaleAmount = h.BeneSex.Code == "F" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    TotalAmount = (h.AdjustmentAmount + h.EntitlementAmount)
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.SubLocationId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                    MaleAmount = h.BeneSex.Code == "M" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    FemaleAmount = h.BeneSex.Code == "F" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    TotalAmount = (h.AdjustmentAmount + h.EntitlementAmount)
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneSex.Code,
                                    CgSexId = h.CgSex.Code,
                                    MaleAmount = h.BeneSex.Code == "M" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    FemaleAmount = h.BeneSex.Code == "F" ? (h.AdjustmentAmount + h.EntitlementAmount) : 0,
                                    TotalAmount = (h.AdjustmentAmount + h.EntitlementAmount)
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                                MaleAmount = g.Sum(b => b.MaleAmount),
                                FemaleAmount = g.Sum(b => b.FemaleAmount),
                                TotalAmount = g.Sum(b => b.TotalAmount)
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            foreach (GenericSummaryReport item in summaryReportDb.Where(x => x.Id == null))
                            {
                                item.Id = 0;
                            }

                            vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                geolocs,
                                lft => lft.Id,
                                rgt => rgt.Id,
                                (lft, rgt) => new SummaryReportVieModel {
                                    Id = lft.Id,
                                    Name = rgt.Name,
                                    Code = rgt.Code,
                                    Male = lft.Male,
                                    Female = lft.Female,
                                    Constituency = rgt.Constituency,
                                    County = rgt.County,
                                    SubLocation = rgt.SubLocation,
                                    Location = rgt.Location,
                                    District = rgt.District,
                                    Division = rgt.Division,
                                    Total = lft.Total,
                                    MaleAmount = lft.MaleAmount,
                                    FemaleAmount = lft.FemaleAmount,
                                    TotalAmount = lft.TotalAmount
                                }).ToList();
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasArrears = this.BooleanSelectList(vm.HasArrears);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.ExceptionTypeId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Exception Type").ConfigureAwait(true), "Code", "Description", vm.ExceptionTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);

            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> FundsRequests(PrepayrollReportOption vm)
        {
            vm.ReportTypeId = 1;
            Expression<Func<FundsRequestDetail, bool>> filter = x => x.FundsRequest.PaymentCycle.CreatedOn > vm.StartDate && x.FundsRequest.PaymentCycle.CreatedOn < vm.EndDate;
            Expression<Func<FundsRequestDetail, bool>> filterAppend = null;
            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }
            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.PspId);
                filter = filter.And(filterAppend);
            }

            vm.PageSize = vm.PageSize != 0 ? vm.PageSize : int.MaxValue;

            switch (vm.ReportTypeId)
            {
                case 1:
                case
                    2:
                    {
                        vm.FundsRequestDetails = (await GenericService.GetAsync(
                            filter,
                            x => x.OrderBy(y => y.FundsRequest.PaymentCycleId),
                            "FundsRequest,Programme,Psp"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
            }

            vm.PageSize = vm.PageSize != int.MaxValue ? vm.PageSize : 0;

            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);

            return View(vm);
        }

        public async Task<ActionResult> Payroll(PrepayrollReportOption vm)
        {
            Expression<Func<Payroll, bool>> filter = x =>
                x.PaymentCycle.CreatedOn > vm.StartDate && x.PaymentCycle.CreatedOn < vm.EndDate;
            Expression<Func<Payroll, bool>> filterAppend = null;
            Expression<Func<Payroll, bool>> subFilter = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.Prepayroll.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.Prepayroll.BeneSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.Prepayroll.CgSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Prepayroll.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.Prepayroll.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.Prepayroll.BeneDoB > vm.DobStartDate && x.Prepayroll.BeneDoB < vm.DobEndDate;
                filter = filter.And(filterAppend);
            }

            if (vm.HasArrears != null)
            {
                filterAppend = vm.HasArrears == 1 ? (x => (x.PaymentAmount) != 4000) : (Expression<Func<Payroll, bool>>)(x => (x.PaymentAmount) == 4000);
                filter = filter.And(filterAppend);
            }

            if (vm.ExceptionTypeId != null)
            {
                if (vm.ExceptionTypeId.Contains("INVALIDBENEID") || vm.ExceptionTypeId.Contains("INVALIDCGID"))
                {
                    subFilter = x =>
                        x.Prepayroll.PrepayrollInvalidIds.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;

                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("SUSPICIOUS"))
                {
                    subFilter = x =>
                    x.Prepayroll.PrepayrollSuspicious.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("DUPLICATE"))
                {
                    subFilter = x =>
                    x.Prepayroll.PrepayrollDuplicateIds.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INELIGIBLE"))
                {
                    subFilter = x =>
                        x.Prepayroll.PrepayrollIneligibles.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDACC"))
                {
                    subFilter = x =>
                    x.Prepayroll.PrepayrollInvalidPaymentAccounts.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDCARD"))
                {
                    subFilter = x =>
                        x.Prepayroll.PrepayrollInvalidPaymentCards.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        vm.Payrolls = (await GenericService.GetAsync(
                            filter,
                            x => x.OrderBy(y => y.PaymentCycleId),
                            "PaymentCycle.FinancialYear,PaymentCycle.FromMonth,PaymentCycle.ToMonth,Prepayroll.PaymentZone,Prepayroll.HHStatus,Prepayroll.CgSex,Prepayroll.BeneSex,Household.HouseholdEnrolment,PaymentCycle,Programme,Prepayroll.BeneficiaryAccount.Status,Prepayroll.BeneficiaryAccount.PspBranch.Psp,Prepayroll.BeneficiaryAccount,Prepayroll.BeneficiaryAccount.PspBranch"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
                case 2:
                    {
                        Expression<Func<Payroll, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();

                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.Prepayroll.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Prepayroll.BeneSex.Code == "M" ? h.PaymentAmount : 0,
                                    FemaleAmount = h.Prepayroll.BeneSex.Code == "F" ? h.PaymentAmount : 0,
                                    TotalAmount = (h.PaymentAmount)
                                };

                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.Prepayroll.SubLocation.ConstituencyId,
                                    BeneSexId = h.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Prepayroll.BeneSex.Code == "M" ? h.PaymentAmount : 0,
                                    FemaleAmount = h.Prepayroll.BeneSex.Code == "F" ? h.PaymentAmount : 0,
                                    TotalAmount = (h.PaymentAmount)
                                };

                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.Prepayroll.SubLocation.LocationId,
                                    BeneSexId = h.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Prepayroll.BeneSex.Code == "M" ? h.PaymentAmount : 0,
                                    FemaleAmount = h.Prepayroll.BeneSex.Code == "F" ? h.PaymentAmount : 0,
                                    TotalAmount = (h.PaymentAmount)
                                };

                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.Prepayroll.SubLocationId,
                                    BeneSexId = h.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Prepayroll.BeneSex.Code == "M" ? h.PaymentAmount : 0,
                                    FemaleAmount = h.Prepayroll.BeneSex.Code == "F" ? h.PaymentAmount : 0,
                                    TotalAmount = (h.PaymentAmount)
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.Prepayroll.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Prepayroll.BeneSex.Code == "M" ? h.PaymentAmount : 0,
                                    FemaleAmount = h.Prepayroll.BeneSex.Code == "F" ? h.PaymentAmount : 0,
                                    TotalAmount = (h.PaymentAmount)
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                                MaleAmount = g.Sum(b => b.MaleAmount),
                                FemaleAmount = g.Sum(b => b.FemaleAmount),
                                TotalAmount = g.Sum(b => b.TotalAmount)
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                geolocs,
                                lft => lft.Id,
                                rgt => rgt.Id,
                                (lft, rgt) => new SummaryReportVieModel {
                                    Id = lft.Id,
                                    Name = rgt.Name,
                                    Code = rgt.Code,
                                    Male = lft.Male,
                                    Female = lft.Female,
                                    Constituency = rgt.Constituency,
                                    County = rgt.County,
                                    SubLocation = rgt.SubLocation,
                                    Location = rgt.Location,
                                    District = rgt.District,
                                    Division = rgt.Division,
                                    Total = lft.Total,
                                    MaleAmount = lft.MaleAmount,
                                    FemaleAmount = lft.FemaleAmount,
                                    TotalAmount = lft.TotalAmount
                                }).ToList();
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasArrears = this.BooleanSelectList(vm.HasArrears);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId =
                new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name",
                    vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.ExceptionTypeId =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Exception Type")
                        .ConfigureAwait(true), "Code", "Description", vm.ExceptionTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);

            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);
            return View(vm);
        }

        public async Task<ActionResult> CreditReports(PrepayrollReportOption vm)
        {
            Expression<Func<Payment, bool>> filter = x =>
                x.PaymentCycle.CreatedOn > vm.StartDate && x.PaymentCycle.CreatedOn < vm.EndDate;
            Expression<Func<Payment, bool>> filterAppend = null;
            Expression<Func<Payment, bool>> subFilter = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.Payroll.Prepayroll.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.Payroll.Prepayroll.BeneSexId);
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.Payroll.Prepayroll.CgSexId.Value);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Payroll.Prepayroll.SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.Payroll.Prepayroll.SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.Payroll.Prepayroll.BeneDoB > vm.DobStartDate && x.Payroll.Prepayroll.BeneDoB < vm.DobEndDate;
                filter = filter.And(filterAppend);
            }

            if (vm.HasArrears != null)
            {
                filterAppend = vm.HasArrears == 1 ? (x => (x.Payroll.PaymentAmount) != 4000) : (Expression<Func<Payment, bool>>)(x => (x.Payroll.PaymentAmount) == 4000);
                filter = filter.And(filterAppend);
            }
            if (vm.WasTrxSuccessful != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.WasTrxSuccessful.Value }, 0);
                filterAppend = x => x.WasTrxSuccessful == success;
                filter = filter.And(filterAppend);
            }

            if (vm.ExceptionTypeId != null)
            {
                if (vm.ExceptionTypeId.Contains("INVALIDBENEID") || vm.ExceptionTypeId.Contains("INVALIDCGID"))
                {
                    subFilter = x =>
                        x.Payroll.Prepayroll.PrepayrollInvalidIds.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;

                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("SUSPICIOUS"))
                {
                    subFilter = x =>
                    x.Payroll.Prepayroll.PrepayrollSuspicious.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("DUPLICATE"))
                {
                    subFilter = x =>
                    x.Payroll.Prepayroll.PrepayrollDuplicateIds.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INELIGIBLE"))
                {
                    subFilter = x =>
                        x.Payroll.Prepayroll.PrepayrollIneligibles.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDACC"))
                {
                    subFilter = x =>
                    x.Payroll.Prepayroll.PrepayrollInvalidPaymentAccounts.Count(y => vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId && y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }

                if (vm.ExceptionTypeId.Contains("INVALIDCARD"))
                {
                    subFilter = x =>
                        x.Payroll.Prepayroll.PrepayrollInvalidPaymentCards.Count(y =>
                            vm.ExceptionTypeId.Equals(y.ExceptionType.Code) && y.HhId == x.HhId &&
                            y.ProgrammeId == x.ProgrammeId && y.PaymentCycleId == x.PaymentCycleId) > 0;
                    filterAppend = filterAppend.And(subFilter);
                }
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<Payment, PaymentDetailReport>>
                               firstSelector =
                                   p => new PaymentDetailReport {
                                        Programme = p.Household.Programme.Code,
                                        PaymentCycle = p.PaymentCycle.Description,
                                        ProgrammeNumber = p.Household.HouseholdEnrolment.FirstOrDefault().ProgrammeNo,
                                        AccountName = p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).AccountName,
                                        AccountNumber = p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).AccountNo,
                                        AccountOpenedOn = (p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).OpenedOn).ToString(),
                                        AccountSubmittedOn = (p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).DateAdded).ToString(),
                                        AccountStatus = p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).Status.Description,
                                        BeneficiaryFirstName = p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.FirstName,
                                        BeneficiaryMiddleName = p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.MiddleName,
                                        BeneficiarySurName = p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.Surname,
                                        BeneficiaryDOB = (p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.Dob).ToString(),
                                        BeneficiaryGender = p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.Sex.Code == "M"? "Male" : "Female",
                                        BeneficiaryNationalIDNumber = p.Household.HouseholdEnrolment.FirstOrDefault().Household.HouseholdMembers.FirstOrDefault(j => j.MemberRoleId == p.Household.Programme.PrimaryRecipientId).Person.NationalIdNo,
                                        PSP = p.Household.HouseholdEnrolment.FirstOrDefault().BeneficiaryAccounts.FirstOrDefault(j => j.HouseholdEnrolment.HhId == p.HhId).PspBranch.Psp.Name,
                                        PaymentAmount = p.Payroll.PaymentAmount,
                                        EntitlementAmount = p.Payroll.Prepayroll.EntitlementAmount,
                                        ArrearsAmount = p.Payroll.Prepayroll.AdjustmentAmount,
                                        TransactionSuccessful = p.WasTrxSuccessful? "YES": "NO",
                                        TransactionAmount = p.TrxAmount,
                                        TransactionNumber = p.TrxNo,
                                        TransactionDate = (p.TrxDate).ToString(),
                                        TransactionNarration = p.TrxNarration
                                   };

                        Expression<Func<PaymentDetailReport, int?>> orderBy = v => v.ProgrammeNumber;

                        if (vm.IsExportCsv)
                        {
                            try
                            {

                                var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                                var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"];
                                var filepath = path + "Beneficiary_Credit_Report.csv";

                                using (var writer = new StreamWriter(filepath))
                                using (var csv = new CsvWriter(writer))
                                {
                                    csv.WriteRecords(list);
                                }

                                byte[] filedata = System.IO.File.ReadAllBytes(filepath);
                                string contentType = MimeMapping.GetMimeMapping(filepath);
                                var cd = new System.Net.Mime.ContentDisposition {
                                    FileName = "Beneficiary_Credit_Report.csv",
                                    Inline = true,
                                };

                                Response.AppendHeader("Content-Disposition", cd.ToString());

                                return File(filedata, contentType);
                            }
                            catch(Exception ex)
                            {
                                return null;
                            }
                        }
                        else
                        {
                            vm.Payments = (await GenericService.GetAsync(
                            filter,
                            x => x.OrderBy(y => y.PaymentCycleId),
                            "PaymentCycle.FinancialYear, PaymentCycle.FromMonth, PaymentCycle.ToMonth, Household.Status, Household.HouseholdEnrolment, PaymentCycle, Programme, Payroll.Prepayroll.BeneficiaryAccount.PspBranch.Psp, Payroll.Prepayroll.BeneficiaryAccount.Status, Payroll.Prepayroll.SubLocation.Constituency.County"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<Payment, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();

                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.Payroll.Prepayroll.SubLocation.Constituency.CountyId,
                                    BeneSexId = h.Payroll.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Payroll.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "M" ? h.Payroll.PaymentAmount : 0,
                                    FemaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "F" ? h.Payroll.PaymentAmount : 0,
                                    TotalAmount = (h.Payroll.PaymentAmount),
                                    Entitlementmount = h.Payroll.Prepayroll.EntitlementAmount,
                                    AdjustmentAmount = h.Payroll.Prepayroll.AdjustmentAmount
                                };

                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.Payroll.Prepayroll.SubLocation.ConstituencyId,
                                    BeneSexId = h.Payroll.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Payroll.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "M" ? h.Payroll.PaymentAmount : 0,
                                    FemaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "F" ? h.Payroll.PaymentAmount : 0,
                                    TotalAmount = (h.Payroll.PaymentAmount),
                                    Entitlementmount = h.Payroll.Prepayroll.EntitlementAmount,
                                    AdjustmentAmount = h.Payroll.Prepayroll.AdjustmentAmount
                                };

                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.Payroll.Prepayroll.SubLocation.LocationId,
                                    BeneSexId = h.Payroll.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Payroll.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "M" ? h.Payroll.PaymentAmount : 0,
                                    FemaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "F" ? h.Payroll.PaymentAmount : 0,
                                    TotalAmount = (h.Payroll.PaymentAmount),
                                    Entitlementmount = h.Payroll.Prepayroll.EntitlementAmount,
                                    AdjustmentAmount = h.Payroll.Prepayroll.AdjustmentAmount
                                };

                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.Payroll.Prepayroll.SubLocationId,
                                    BeneSexId = h.Payroll.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Payroll.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "M" ? h.Payroll.PaymentAmount : 0,
                                    FemaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "F" ? h.Payroll.PaymentAmount : 0,
                                    TotalAmount = (h.Payroll.PaymentAmount),
                                    Entitlementmount = h.Payroll.Prepayroll.EntitlementAmount,
                                    AdjustmentAmount = h.Payroll.Prepayroll.AdjustmentAmount
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.Payroll.Prepayroll.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.Payroll.Prepayroll.BeneSex.Code,
                                    CgSexId = h.Payroll.Prepayroll.CgSex.Code,
                                    MaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "M" ? h.Payroll.PaymentAmount : 0,
                                    FemaleAmount = h.Payroll.Prepayroll.BeneSex.Code == "F" ? h.Payroll.PaymentAmount : 0,
                                    TotalAmount = (h.Payroll.PaymentAmount),
                                    Entitlementmount = h.Payroll.Prepayroll.EntitlementAmount,
                                    AdjustmentAmount = h.Payroll.Prepayroll.AdjustmentAmount
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                                MaleAmount = g.Sum(b => b.MaleAmount),
                                FemaleAmount = g.Sum(b => b.FemaleAmount),
                                TotalAmount = g.Sum(b => b.TotalAmount),
                                Entitlementmount = g.Sum(b => b.Entitlementmount),
                                AdjustmentAmount = g.Sum(b => b.AdjustmentAmount),
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                geolocs,
                                lft => lft.Id,
                                rgt => rgt.Id,
                                (lft, rgt) => new SummaryReportVieModel {
                                    Id = lft.Id,
                                    Name = rgt.Name,
                                    Code = rgt.Code,
                                    Male = lft.Male,
                                    Female = lft.Female,
                                    Constituency = rgt.Constituency,
                                    County = rgt.County,
                                    SubLocation = rgt.SubLocation,
                                    Location = rgt.Location,
                                    District = rgt.District,
                                    Division = rgt.Division,
                                    Total = lft.Total,
                                    MaleAmount = lft.MaleAmount,
                                    FemaleAmount = lft.FemaleAmount,
                                    TotalAmount = lft.TotalAmount,
                                    AdjustmentAmount = lft.AdjustmentAmount,
                                    Entitlementmount = lft.Entitlementmount
                                }).ToList();

                            if (vm.IsExportCsv)
                            {
                                try
                                {

                                    var list = vm.SummaryReportVieModels;
                                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"];
                                    var filepath = path + "Beneficiary_Credit_Report.csv";

                                    using (var writer = new StreamWriter(filepath))
                                    using (var csv = new CsvWriter(writer))
                                    {
                                        csv.WriteRecords(list);
                                    }

                                    byte[] filedata = System.IO.File.ReadAllBytes(filepath);
                                    string contentType = MimeMapping.GetMimeMapping(filepath);
                                    var cd = new System.Net.Mime.ContentDisposition {
                                        FileName = "Beneficiary_Credit_Report.csv",
                                        Inline = true,
                                    };

                                    Response.AppendHeader("Content-Disposition", cd.ToString());

                                    return File(filedata, contentType);
                                }
                                catch (Exception ex)
                                {
                                    return null;
                                }
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.WasTrxSuccessful = this.BooleanSelectList(vm.WasTrxSuccessful);

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy, "SUBLOCATION");
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasArrears = this.BooleanSelectList(vm.HasArrears);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId =
                new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name",
                    vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.ExceptionTypeId =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Exception Type")
                        .ConfigureAwait(true), "Code", "Description", vm.ExceptionTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);

            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);
            return View(vm);
        }

        public async Task<ActionResult> PayableCommissions(PrepayrollReportOption vm)
        {
            vm.ReportTypeId = 1;
            Expression<Func<FundsRequestDetail, bool>> filter = x => x.FundsRequest.PaymentCycle.CreatedOn > vm.StartDate && x.FundsRequest.PaymentCycle.CreatedOn < vm.EndDate;
            Expression<Func<FundsRequestDetail, bool>> filterAppend = null;
            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.ProgrammeId);
                filter = filter.And(filterAppend);
            }
            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.PspId);
                filter = filter.And(filterAppend);
            }

            vm.PageSize = vm.PageSize != 0 ? vm.PageSize : int.MaxValue;

            switch (vm.ReportTypeId)
            {
                case 1:
                case
                    2:
                    {
                        vm.FundsRequestDetails = (await GenericService.GetAsync(
                            filter,
                            x => x.OrderBy(y => y.FundsRequest.PaymentCycleId),
                            "FundsRequest,Programme,Psp"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
            }

            vm.PageSize = vm.PageSize != int.MaxValue ? vm.PageSize : 0;

            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            return View(vm);
        }

        public async Task<ActionResult> Reconciliation(PrepayrollReportOption vm)
        {
            vm.ReportTypeId = 1;
            Expression<Func<ReconciliationDetail, bool>> filter = x => x.Reconciliation.StartDate > vm.StartDate && x.Reconciliation.EndDate < vm.EndDate;
            Expression<Func<ReconciliationDetail, bool>> filterAppend = null;

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.PspId);
                filter = filter.And(filterAppend);
            }

            vm.PageSize = vm.PageSize != 0 ? vm.PageSize : int.MaxValue;

            switch (vm.ReportTypeId)
            {
                case 1:
                case
                    2:
                    {
                        vm.ReconciliationDetails = (await GenericService.GetAsync(
                            filter,
                            x => x.OrderBy(y => y.Reconciliation.StartDate),
                            "Reconciliation,Reconciliation.Status,Psp"
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
            }

            vm.PageSize = vm.PageSize != int.MaxValue ? vm.PageSize : 0;

            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var reconFileTypes = new List<StringParameter>
            {
                new StringParameter { Id = "ALL", Name = "All" },
                new StringParameter { Id = "PY",Name = "Payment"},
                new StringParameter { Id = "FR",Name = "Funds Request"},
                new StringParameter { Id = "BL",Name = "Balance"},
                new StringParameter { Id = "CO",Name = "Commissions"},
                new StringParameter { Id = "CB",Name = "ClawBacks"}
            };

            ViewBag.Select =
                new SelectList(reconFileTypes, "Id", "Name", vm.Select);

            return View(vm);
        }

        public async Task<ActionResult> MonthlyActivity(PrepayrollReportOption vm)
        {
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filter = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                                       z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                                           .PrimaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .SecondaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.HasBiosVerified != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasBiosVerified.Value }, 0);
                filterAppend = x => x.HadBeneBiosVerified == success;
                filter = filter.And(filterAppend);
            }

            if (vm.HasUniqueWithdrawal != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasUniqueWithdrawal.Value }, 0);
                filterAppend = x => x.HadUniqueWdl == success;
                filter = filter.And(filterAppend);
            }

            if (vm.IsDueForClawback != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDueForClawback.Value }, 0);
                filterAppend = x => x.IsDueForClawback == success;
                filter = filter.And(filterAppend);
            }
            if (vm.IsDormant != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDormant.Value }, 0);
                filterAppend = x => x.IsDormant == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Any(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .PrimaryRecipientId && z.Person.Dob >= vm.StartDate && z.Person.Dob <= vm.EndDate);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, BeneAccountMonthlyActivityDetailFullReport>>
                              firstSelector =
                                  p => new BeneAccountMonthlyActivityDetailFullReport {
                                      HhEnrolmentId = p.BeneficiaryAccount.HhEnrolmentId,
                                      ProgrammeNumber = p.BeneficiaryAccount.HouseholdEnrolment.ProgrammeNo,
                                      BeneProgNoPrefix = p.BeneficiaryAccount.HouseholdEnrolment.BeneProgNoPrefix,
                                      Programme = p.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.Code,
                                      Psp = p.BeneficiaryAccount.PspBranch.Psp.Name,
                                      Branch = p.BeneficiaryAccount.PspBranch.Name,
                                      AccountName = p.BeneficiaryAccount.AccountName,
                                      AccountNo = p.BeneficiaryAccount.AccountNo,
                                      OpenedOn = p.BeneficiaryAccount.OpenedOn,
                                      HadUniqueWdl = p.HadUniqueWdl,
                                      UniqueWdlTrxNo = p.UniqueWdlTrxNo,
                                      UniqueWdlDate = p.UniqueWdlDate,
                                      MemberRoleId = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).MemberRoleId,
                                      FirstName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.FirstName,
                                      MiddleName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.MiddleName,
                                      Surname = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.Surname,
                                      Sex = p.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers
                                          .FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.Sex.Code,
                                  };

                        Expression<Func<BeneAccountMonthlyActivityDetailFullReport, int?>> orderBy = v => v.HhEnrolmentId;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Monthly Activity Report");
                                var ws = wb.Worksheet("Monthly Activity Report");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                    .GetType()
                                    .GetProperties()
                                    .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("MonthlyActivityDetailFullReport.xlsx");
                            }
                        }
                        else
                        {
                            vm.BeneAccountMonthlyActivityDetailFullReports = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Monthly Activity Summary Report");
                                    var ws = wb.Worksheet("Monthly Activity Summary Report");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Monthly_Activity_Summary_Report.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasUniqueWithdrawal = this.BooleanSelectList(vm.HasUniqueWithdrawal);
            ViewBag.HasBiosVerified = this.BooleanSelectList(vm.HasBiosVerified);
            ViewBag.IsDueForClawback = this.BooleanSelectList(vm.IsDueForClawback);
            ViewBag.IsDormant = this.BooleanSelectList(vm.IsDormant);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> UniqueWithdrawal(PrepayrollReportOption vm)
        {
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filter = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                                       z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                                           .PrimaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .SecondaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.HasBiosVerified != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasBiosVerified.Value }, 0);
                filterAppend = x => x.HadBeneBiosVerified == success;
                filter = filter.And(filterAppend);
            }

            if (vm.HasUniqueWithdrawal != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasUniqueWithdrawal.Value }, 0);
                filterAppend = x => x.HadUniqueWdl == success;
                filter = filter.And(filterAppend);
            }

            if (vm.IsDueForClawback != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDueForClawback.Value }, 0);
                filterAppend = x => x.IsDueForClawback == success;
                filter = filter.And(filterAppend);
            }
            if (vm.IsDormant != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDormant.Value }, 0);
                filterAppend = x => x.IsDormant == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Any(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .PrimaryRecipientId && z.Person.Dob >= vm.StartDate && z.Person.Dob <= vm.EndDate);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, BeneAccountMonthlyActivityWithdrawalReport>>
                              firstSelector =
                                  p => new BeneAccountMonthlyActivityWithdrawalReport {
                                      HhEnrolmentId = p.BeneficiaryAccount.HhEnrolmentId,
                                      ProgrammeNumber = p.BeneficiaryAccount.HouseholdEnrolment.ProgrammeNo,
                                      BeneProgNoPrefix = p.BeneficiaryAccount.HouseholdEnrolment.BeneProgNoPrefix,
                                      Programme = p.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.Code,
                                      Psp = p.BeneficiaryAccount.PspBranch.Psp.Name,
                                      Branch = p.BeneficiaryAccount.PspBranch.Name,
                                      AccountName = p.BeneficiaryAccount.AccountName,
                                      AccountNo = p.BeneficiaryAccount.AccountNo,
                                      OpenedOn = p.BeneficiaryAccount.OpenedOn,
                                      HadUniqueWdl = p.HadUniqueWdl,
                                      UniqueWdlTrxNo = p.UniqueWdlTrxNo,
                                      UniqueWdlDate = p.UniqueWdlDate,
                                      MemberRoleId = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).MemberRoleId,
                                      FirstName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.FirstName,
                                      MiddleName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.MiddleName,
                                      Surname = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                          .HouseholdMembers.FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.Surname,
                                      Sex = p.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers
                                          .FirstOrDefault(j =>
                                              j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                  .Programme.PrimaryRecipientId).Person.Sex.Code,
                                  };

                        Expression<Func<BeneAccountMonthlyActivityWithdrawalReport, int?>> orderBy = v => v.HhEnrolmentId;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Unique Withdrawals");
                                var ws = wb.Worksheet("Unique Withdrawals");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }

                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Unique_Withdrawals.xlsx");
                            }
                        }
                        else
                        {
                            vm.BeneAccountMonthlyActivityWithdrawalReports = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            //var summaryReportDb = .ToList();

                            if (vm.IsExportCsv)
                            {
                                var list = GenericService.GetGrouped(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Unique Withdrawals");
                                    var ws = wb.Worksheet("Unique Withdrawals");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }
                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Unique_Withdrawals_Summary.xlsx");
                                }
                            }
                            else
                            {
                                var summaryReportDb = GenericService.GetGroupedSet(
                                    filter,
                                    firstSelector,
                                    orderSelector,
                                    groupSelector,
                                    selector,
                                    null,
                                    null
                                ).ToPagedList(vm.Page, vm.PageSize).ToList();
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasUniqueWithdrawal = this.BooleanSelectList(vm.HasUniqueWithdrawal);
            ViewBag.HasBiosVerified = this.BooleanSelectList(vm.HasBiosVerified);

            ViewBag.IsDueForClawback = this.BooleanSelectList(vm.IsDueForClawback);
            ViewBag.IsDormant = this.BooleanSelectList(vm.IsDormant);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> DueForClawback(PrepayrollReportOption vm)
        {
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filter = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                                       z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                                           .PrimaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .SecondaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.HasBiosVerified != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasBiosVerified.Value }, 0);
                filterAppend = x => x.HadBeneBiosVerified == success;
                filter = filter.And(filterAppend);
            }

            if (vm.HasUniqueWithdrawal != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasUniqueWithdrawal.Value }, 0);
                filterAppend = x => x.HadUniqueWdl == success;
                filter = filter.And(filterAppend);
            }

            if (vm.IsDueForClawback != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDueForClawback.Value }, 0);
                filterAppend = x => x.IsDueForClawback == success;
                filter = filter.And(filterAppend);
            }
            if (vm.IsDormant != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDormant.Value }, 0);
                filterAppend = x => x.IsDormant == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DateRange != "ALL_TIME")
            {
                filterAppend = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, BeneAccountMonthlyActivityClawbackReport>>
                               firstSelector =
                                   p => new BeneAccountMonthlyActivityClawbackReport {
                                       HhEnrolmentId = p.BeneficiaryAccount.HhEnrolmentId,
                                       ProgrammeNumber = p.BeneficiaryAccount.HouseholdEnrolment.ProgrammeNo,
                                       BeneProgNoPrefix = p.BeneficiaryAccount.HouseholdEnrolment.BeneProgNoPrefix,
                                       Programme = p.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.Code,
                                       Psp = p.BeneficiaryAccount.PspBranch.Psp.Name,
                                       Branch = p.BeneficiaryAccount.PspBranch.Name,
                                       AccountName = p.BeneficiaryAccount.AccountName,
                                       AccountNo = p.BeneficiaryAccount.AccountNo,
                                       OpenedOn = p.BeneficiaryAccount.OpenedOn,
                                       ClawbackAmaount = p.ClawbackAmount,
                                       IsDueForClawback = p.IsDueForClawback,
                                       MemberRoleId = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).MemberRoleId,
                                       FirstName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.FirstName,
                                       MiddleName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.MiddleName,
                                       Surname = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.Surname,
                                       Sex = p.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers
                                           .FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.Sex.Code,
                                   };

                        Expression<Func<BeneAccountMonthlyActivityClawbackReport, int?>> orderBy = v => v.HhEnrolmentId;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("MONTHLY CLAW BACK REPORTS");
                                var ws = wb.Worksheet("MONTHLY CLAW BACK REPORTS");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }
                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("MonthlyActivityClawbackReports.xlsx");
                            }
                        }
                        else
                        {
                            vm.BeneAccountMonthlyActivityClawbackReports =
                                (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            if (vm.IsExportCsv)
                            {
                                var list = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Unique Withdrawals");
                                    var ws = wb.Worksheet("Unique Withdrawals");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }
                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Unique_Withdrawals_Summary.xlsx");
                                }
                            }
                            else
                            {
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }

                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasUniqueWithdrawal = this.BooleanSelectList(vm.HasUniqueWithdrawal);
            ViewBag.HasBiosVerified = this.BooleanSelectList(vm.HasBiosVerified);

            ViewBag.IsDueForClawback = this.BooleanSelectList(vm.IsDueForClawback);
            ViewBag.IsDormant = this.BooleanSelectList(vm.IsDormant);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> Dormancy(PrepayrollReportOption vm)
        {
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filter = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            //if (vm.PriReciSexId != null)
            //{
            //    filterAppend = x => vm.PriReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(z =>
            //                           z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
            //                               .PrimaryRecipientId).Person.SexId);
            //    filter = filter.And(filterAppend);
            //}

            //if (vm.SecReciSexId != null)
            //{
            //    filterAppend = x => vm.SecReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(z =>
            //                           z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
            //                               .SecondaryRecipientId).Person.SexId);
            //    filter = filter.And(filterAppend);
            //}

            //if (vm.HasBiosVerified != null)
            //{
            //    var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasBiosVerified.Value }, 0);
            //    filterAppend = x => x.HadBeneBiosVerified == success;
            //    filter = filter.And(filterAppend);
            //}

            //if (vm.HasUniqueWithdrawal != null)
            //{
            //    var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasUniqueWithdrawal.Value }, 0);
            //    filterAppend = x => x.HadUniqueWdl == success;
            //    filter = filter.And(filterAppend);
            //}

            //if (vm.IsDueForClawback != null)
            //{
            //    var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDueForClawback.Value }, 0);
            //    filterAppend = x => x.IsDueForClawback == success;
            //    filter = filter.And(filterAppend);
            //}
            if (vm.IsDormant != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDormant.Value }, 0);
                filterAppend = x => x.IsDormant == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            //if (vm.DobDateRangeId != "ALL")
            //{
            //    filterAppend = x => x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Any(z =>
            //        z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
            //            .PrimaryRecipientId && z.Person.Dob >= vm.StartDate && z.Person.Dob <= vm.EndDate);
            //    filter = filter.And(filterAppend);
            //}

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, BeneAccountMonthlyActivityDormancyReport>>
                               firstSelector =
                                   p => new BeneAccountMonthlyActivityDormancyReport {
                                       HhEnrolmentId = p.BeneficiaryAccount.HhEnrolmentId,
                                       ProgrammeNumber = p.BeneficiaryAccount.HouseholdEnrolment.ProgrammeNo,
                                       BeneProgNoPrefix = p.BeneficiaryAccount.HouseholdEnrolment.BeneProgNoPrefix,
                                       Programme = p.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.Code,
                                       Psp = p.BeneficiaryAccount.PspBranch.Psp.Name,
                                       Branch = p.BeneficiaryAccount.PspBranch.Name,
                                       AccountName = p.BeneficiaryAccount.AccountName,
                                       AccountNo = p.BeneficiaryAccount.AccountNo,
                                       OpenedOn = p.BeneficiaryAccount.OpenedOn,
                                       IsDormant = p.IsDormant,
                                       DormancyDate = p.DormancyDate,
                                       MemberRoleId = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).MemberRoleId,
                                       FirstName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.FirstName,
                                       MiddleName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.MiddleName,
                                       Surname = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                           .HouseholdMembers.FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.Surname,
                                       Sex = p.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers
                                           .FirstOrDefault(j =>
                                               j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                   .Programme.PrimaryRecipientId).Person.Sex.Code,
                                   };

                        Expression<Func<BeneAccountMonthlyActivityDormancyReport, int?>> orderBy = v => v.HhEnrolmentId;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("DormancyReports");
                                var ws = wb.Worksheet("DormancyReports");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }
                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("MonthlyActivityDormancyReports_Detailed.xlsx");
                            }
                        }
                        else
                        {
                            vm.BeneAccountMonthlyActivityDormancyReports = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            if (vm.IsExportCsv)
                            {
                                var list = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new BeneAccountMonthlyActivitySummaryReport {
                                        //Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("MonthlyActivityDormancyReports");
                                    var ws = wb.Worksheet("MonthlyActivityDormancyReports");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }

                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("MonthlyActivityDormancyReports_Summary.xlsx");
                                }
                            }
                            else
                            {
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }

                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasUniqueWithdrawal = this.BooleanSelectList(vm.HasUniqueWithdrawal);
            ViewBag.HasBiosVerified = this.BooleanSelectList(vm.HasBiosVerified);

            ViewBag.IsDueForClawback = this.BooleanSelectList(vm.IsDueForClawback);
            ViewBag.IsDormant = this.BooleanSelectList(vm.IsDormant);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }

        public async Task<ActionResult> ProofOfLife(PrepayrollReportOption vm)
        {
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filter = x => x.BeneAccountMonthlyActivity.StartDate >= vm.StartDate && x.BeneAccountMonthlyActivity.EndDate <= vm.EndDate;
            Expression<Func<BeneAccountMonthlyActivityDetail, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.BeneficiaryAccount.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                                       z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                                           .PrimaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Count(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .SecondaryRecipientId));
                filter = filter.And(filterAppend);
            }

            if (vm.HasBiosVerified != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasBiosVerified.Value }, 0);
                filterAppend = x => x.HadBeneBiosVerified == success;
                filter = filter.And(filterAppend);
            }

            if (vm.HasUniqueWithdrawal != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.HasUniqueWithdrawal.Value }, 0);
                filterAppend = x => x.HadUniqueWdl == success;
                filter = filter.And(filterAppend);
            }

            if (vm.IsDueForClawback != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDueForClawback.Value }, 0);
                filterAppend = x => x.IsDueForClawback == success;
                filter = filter.And(filterAppend);
            }
            if (vm.IsDormant != null)
            {
                var success = BitConverter.ToBoolean(new byte[] { (byte)vm.IsDormant.Value }, 0);
                filterAppend = x => x.IsDormant == success;
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.Any(z =>
                    z.MemberRoleId == x.BeneficiaryAccount.HouseholdEnrolment.Household.Programme
                        .PrimaryRecipientId && z.Person.Dob >= vm.StartDate && z.Person.Dob <= vm.EndDate);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, BeneAccountMonthlyActivityBiosReport>>
                            firstSelector =
                                p => new BeneAccountMonthlyActivityBiosReport {
                                    HhEnrolmentId = p.BeneficiaryAccount.HhEnrolmentId,
                                    ProgrammeNumber = p.BeneficiaryAccount.HouseholdEnrolment.ProgrammeNo,
                                    BeneProgNoPrefix = p.BeneficiaryAccount.HouseholdEnrolment.BeneProgNoPrefix,
                                    Programme = p.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.Code,
                                    Psp = p.BeneficiaryAccount.PspBranch.Psp.Name,
                                    Branch = p.BeneficiaryAccount.PspBranch.Name,
                                    AccountName = p.BeneficiaryAccount.AccountName,
                                    AccountNo = p.BeneficiaryAccount.AccountNo,
                                    OpenedOn = p.BeneficiaryAccount.OpenedOn,
                                    HadBeneBiosVerified = p.HadBeneBiosVerified,

                                    MemberRoleId = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                        .HouseholdMembers.FirstOrDefault(j =>
                                            j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                .Programme.PrimaryRecipientId).MemberRoleId,
                                    FirstName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                        .HouseholdMembers.FirstOrDefault(j =>
                                            j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                .Programme.PrimaryRecipientId).Person.FirstName,
                                    MiddleName = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                        .HouseholdMembers.FirstOrDefault(j =>
                                            j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                .Programme.PrimaryRecipientId).Person.MiddleName,
                                    Surname = p.BeneficiaryAccount.HouseholdEnrolment.Household
                                        .HouseholdMembers.FirstOrDefault(j =>
                                            j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                .Programme.PrimaryRecipientId).Person.Surname,
                                    Sex = p.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers
                                        .FirstOrDefault(j =>
                                            j.MemberRoleId == p.BeneficiaryAccount.HouseholdEnrolment.Household
                                                .Programme.PrimaryRecipientId).Person.Sex.Code,
                                };

                        Expression<Func<BeneAccountMonthlyActivityBiosReport, int?>> orderBy = v => v.HhEnrolmentId;

                        if (vm.IsExportCsv)
                        {
                            var list = await (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToListAsync().ConfigureAwait(false);
                            using (var wb = new XLWorkbook())
                            {
                                wb.AddWorksheet("Unique Withdrawals");
                                var ws = wb.Worksheet("Unique Withdrawals");
                                if (list?.Count() > 0)
                                {
                                    var titles = list.First()
                                        .GetType()
                                        .GetProperties()
                                        .Select(p => p.Name).ToList();
                                    var i = 0;
                                    foreach (var x in titles)
                                    {
                                        i++;
                                        ws.Cell(1, i).Value = x;
                                    }
                                    ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                }
                                ws.Cell(2, 1).InsertData(list);
                                return wb.Deliver("Unique_Withdrawals.xlsx");
                            }
                        }
                        else
                        {
                            vm.BeneAccountMonthlyActivityBiosReports = (GenericService.GetSet(firstSelector, orderBy, filter, null, null)).ToPagedList(vm.Page, vm.PageSize);
                        }
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneAccountMonthlyActivityDetail, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };
                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.BeneficiaryAccount.PspBranch.PspId,
                                    BeneSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.PrimaryRecipient.Code).Person.Sex.Code,
                                    CgSexId = h.BeneficiaryAccount.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(x => x.MemberRole.Code == h.BeneficiaryAccount.HouseholdEnrolment.Household.Programme.SecondaryRecipient.Code).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            var summaryReportDb = GenericService.GetGrouped(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToList();

                            if (vm.IsExportCsv)
                            {
                                var list = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();

                                using (var wb = new XLWorkbook())
                                {
                                    wb.AddWorksheet("Unique Withdrawals");
                                    var ws = wb.Worksheet("Unique Withdrawals");
                                    if (list?.Count() > 0)
                                    {
                                        var titles = list.First()
                                            .GetType()
                                            .GetProperties()
                                            .Select(p => p.Name).ToList();
                                        var i = 0;
                                        foreach (var x in titles)
                                        {
                                            i++;
                                            ws.Cell(1, i).Value = x;
                                        }
                                        ws.Cell(1, titles.Count() + 1).AsRange().AddToNamed("Titles");
                                    }
                                    ws.Cell(2, 1).InsertData(list);

                                    return wb.Deliver("Unique_Withdrawals_Summary.xlsx");
                                }
                            }
                            else
                            {
                                vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                                    geolocs,
                                    lft => lft.Id,
                                    rgt => rgt.Id,
                                    (lft, rgt) => new SummaryReportVieModel {
                                        Id = lft.Id,
                                        Name = rgt.Name,
                                        Code = rgt.Code,
                                        Male = lft.Male,
                                        Female = lft.Female,
                                        Constituency = rgt.Constituency,
                                        County = rgt.County,
                                        SubLocation = rgt.SubLocation,
                                        Location = rgt.Location,
                                        District = rgt.District,
                                        Division = rgt.Division,
                                        Total = lft.Total,
                                        //MaleAmount = lft.MaleAmount,
                                        //FemaleAmount = lft.FemaleAmount,
                                        //TotalAmount = lft.TotalAmount
                                    }).ToList();
                            }

                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By and Report Type Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.GroupBy = this.GroupBy(vm.GroupBy);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            ViewBag.HasUniqueWithdrawal = this.BooleanSelectList(vm.HasUniqueWithdrawal);
            ViewBag.HasBiosVerified = this.BooleanSelectList(vm.HasBiosVerified);

            ViewBag.IsDueForClawback = this.BooleanSelectList(vm.IsDueForClawback);
            ViewBag.IsDormant = this.BooleanSelectList(vm.IsDormant);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null
                ? new SelectList(
                    await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId))
                        .ConfigureAwait(true), "Id", "Name", vm.ConstituencyId)
                : new SelectList(
                    await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id",
                    "Name", vm.ConstituencyId);
            ViewBag.DateRange =
                new SelectList(
                    await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range")
                        .ConfigureAwait(true), "Code", "Description", vm.DateRange);
            return View(vm);
        }
    }
}