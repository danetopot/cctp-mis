﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Reports;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Reports.Controllers
{
    [Authorize]
    public class EnrolmentController : Controller
    {
        public readonly IGenericService GenericService;
        private readonly IPdfService PdfService;
        protected readonly IEmailService EmailService;
        private ApplicationDbContext db = new ApplicationDbContext();
        // private ApplicationDbContext db = new ApplicationDbContext();

        public EnrolmentController(IGenericService genericService, EmailService emailService, PdfService pdfService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
            PdfService = pdfService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> HouseholdsPendingEnrolment(HouseholdsEnrolmentReportOption vm)
        {
            Expression<Func<HouseholdEnrolment, bool>> filter = x =>
                x.HouseholdEnrolmentPlan.ApvOn > vm.StartDate && x.HouseholdEnrolmentPlan.ApvOn < vm.EndDate;
            Expression<Func<HouseholdEnrolment, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }
            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.Household.HouseholdMembers.FirstOrDefault(c => c.MemberRoleId == x.Household.Programme.PrimaryRecipientId).Person.SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.Household.HouseholdMembers.FirstOrDefault(c => c.MemberRoleId == x.Household.Programme.SecondaryRecipientId).Person.SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.Household.HouseholdSubLocation.FirstOrDefault(c => c.HhId == x.HhId).SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.Household.HouseholdSubLocation.FirstOrDefault(c => c.HhId == x.HhId).SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.Household.HouseholdMembers.FirstOrDefault(c => c.MemberRoleId == x.Household.Programme.PrimaryRecipientId).Person.Dob >= vm.DobStartDate &&
                    x.Household.HouseholdMembers.FirstOrDefault(c => c.MemberRoleId == x.Household.Programme.PrimaryRecipientId).Person.Dob <= vm.DobEndDate;

                filter = filter.And(filterAppend);
            }

            if (vm.StatusId != null)
            {
                filterAppend = x => vm.StatusId.Contains(x.Household.StatusId);
                filter = filter.And(filterAppend);
            }

            if (vm.RegGroupId != null)
            {
                filterAppend = x => vm.RegGroupId.Contains(x.Household.RegGroupId);
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        //vm.Details = (await GenericService.GetSearchableQueryable(
                        //    filter,
                        //    x => x.OrderBy(y => y.BeneProgNoPrefix).ThenBy(y=>y.ProgrammeNo),
                        //    ""
                        //).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        //break;

                        // vm.Details =
                        // GenericService.GetManyBySp<ReportDetailedHouseholdEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
                case 2:
                    {
                        // var parameterList = new List<ParameterEntity> { new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange), },

                        // new ParameterEntity { ParameterTuple = new Tuple<string,
                        // object>("StartDate", vm.StartDate ?? (object) DBNull.Value), }, new
                        // ParameterEntity { ParameterTuple = new Tuple<string, object>("EndDate",
                        // vm.EndDate ?? (object) DBNull.Value), }, new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("ProgrammeXml", vm.ProgrammeId
                        // == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)), },

                        // new ParameterEntity { ParameterTuple = new Tuple<string,
                        // object>("PrimarySexXml", vm.PriReciSexId == null ? (object) DBNull.Value :
                        // this.GetXml((List<int>)vm.PriReciSexId)), }, new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                        // vm.SecReciSexId == null ? (object) DBNull.Value :
                        // this.GetXml((List<int>)vm.SecReciSexId)), }, new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("StatusXml", vm.StatusId ==
                        // null ? (object) DBNull.Value : this.GetXml((List<int>)vm.StatusId)), },
                        // new ParameterEntity { ParameterTuple = new Tuple<string,
                        // object>("RegGroupXml", vm.RegGroupId == null ? (object) DBNull.Value :
                        // this.GetXml((List<int>)vm.RegGroupId)), }, new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("CountyXml", vm.CountyId ==
                        // null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)), },
                        // new ParameterEntity { ParameterTuple = new Tuple<string,
                        // object>("ConstituencyXml", vm.ConstituencyId == null ? (object)
                        // DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)), }, new
                        // ParameterEntity { ParameterTuple = new Tuple<string, object>("Page",
                        // vm.Page ), }, new ParameterEntity { ParameterTuple = new Tuple<string,
                        // object>("PageSize", vm.PageSize ), }, new ParameterEntity { ParameterTuple
                        // = new Tuple<string, object>("Type", "SUMMARY" ), }, new ParameterEntity {
                        // ParameterTuple = new Tuple<string, object>("DobDateRangeId",
                        // vm.DobDateRangeId), },

                        //        new ParameterEntity {
                        //            ParameterTuple = new Tuple<string, object>("DobStartDate",
                        //                vm.DobStartDate ?? (object) DBNull.Value),
                        //        },
                        //        new ParameterEntity {
                        //            ParameterTuple = new Tuple<string, object>("DobEndDate",
                        //                vm.DobEndDate ?? (object) DBNull.Value),
                        //        },
                        //};

                        // vm.Summary =
                        // GenericService.GetManyBySp<ReportSummaryHouseholdsPendingEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
            }

            ViewBag.PageSize = this.GetPager(vm.PageSize);

            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<Source>().ConfigureAwait(true), "Id", "Name", vm.SourceId);
            ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "HHStatus").ConfigureAwait(true), "Id", "Description", vm.StatusId);
            ViewBag.RegGroupId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Registration Group").ConfigureAwait(true), "Id", "Description", vm.RegGroupId);
            var sex = await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true);
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);

            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);
            return View(vm);
        }

        public async Task<ActionResult> HouseholdsEnrolledByProgrammeGender(HouseholdsEnrolmentReportOption vm)
        {
            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StatusXml",
                                vm.StatusId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.StatusId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("RegGroupXml",
                                vm.RegGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.RegGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "DETAILS"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                        };
                        //vm.Details = GenericService.GetManyBySp<ReportDetailedHouseholdEnrolment>(spName, parameterNames, parameterList).ToList();
                        break;
                    }
                case 2:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StatusXml",
                                vm.StatusId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.StatusId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("RegGroupXml",
                                vm.RegGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.RegGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "SUMMARY"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                    };

                        // vm.Summary =
                        // GenericService.GetManyBySp<ReportSummaryHouseholdsPendingEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
            }
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<Source>().ConfigureAwait(true), "Id", "Name", vm.SourceId);
            ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "HHStatus").ConfigureAwait(true), "Id", "Description", vm.StatusId);
            ViewBag.RegGroupId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Registration Group").ConfigureAwait(true), "Id", "Description", vm.RegGroupId);
            var sex = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true)).ToList();
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);

            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdAccounts(HouseholdsEnrolmentReportOption vm)
        {
            Expression<Func<BeneficiaryAccount, bool>> filter = x =>
                x.OpenedOn > vm.StartDate && x.OpenedOn < vm.EndDate;
            Expression<Func<BeneficiaryAccount, bool>> filterAppend = null;

            if (vm.ProgrammeId != null)
            {
                filterAppend = x => vm.ProgrammeId.Contains(x.HouseholdEnrolment.Household.ProgrammeId);
                filter = filter.And(filterAppend);
            }

            if (vm.PspId != null)
            {
                filterAppend = x => vm.PspId.Contains(x.PspBranch.PspId);
                filter = filter.And(filterAppend);
            }

            if (vm.PriReciSexId != null)
            {
                filterAppend = x => vm.PriReciSexId.Contains(x.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == x.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.SecReciSexId != null)
            {
                filterAppend = x => vm.SecReciSexId.Contains(x.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == x.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.SexId);
                filter = filter.And(filterAppend);
            }

            if (vm.CountyId != null)
            {
                filterAppend = x => vm.CountyId.Contains(x.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId);
                filter = filter.And(filterAppend);
            }

            if (vm.ConstituencyId != null)
            {
                filterAppend = x => vm.ConstituencyId.Contains(x.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId);
                filter = filter.And(filterAppend);
            }

            if (vm.DobDateRangeId != "ALL")
            {
                filterAppend = x => x.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == x.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Dob > vm.DobStartDate && x.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == x.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Dob < vm.DobEndDate;
                filter = filter.And(filterAppend);
            }

            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        vm.BeneficiaryAccounts = (await GenericService.GetSearchableQueryable(
                            filter,
                            x => x.OrderBy(y => y.OpenedOn),
                            ""
                        ).ConfigureAwait(true)).ToPagedList(vm.Page, vm.PageSize);
                        break;
                    }
                case 2:
                    {
                        Expression<Func<BeneficiaryAccount, SummaryReport>> firstSelector = null;
                        Func<SummaryReport, int?> groupSelector = null;
                        Expression<Func<SummaryReport, int?>> orderSelector = null;
                        Func<IGrouping<int?, SummaryReport>, GenericSummaryReport> selector = null;

                        if (!string.IsNullOrEmpty(vm.GroupBy))
                        {
                            var proc = "GetGeoLocations";
                            var parameters = "@Type";
                            var parameterList = new List<ParameterEntity> {
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("Type",
                                    vm.GroupBy == null ? (object) DBNull.Value : vm.GroupBy),
                            }
                        };

                            var geolocs = GenericService.GetManyBySp<ReportGeoTables>(proc, parameters, parameterList)
                                .ToList();
                            // add a Null Row
                            geolocs.Add(new ReportGeoTables {
                                Id = 0,
                                Name = "None"
                            });

                            if (vm.GroupBy == "COUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    CountyId = h.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.Constituency.CountyId,
                                    BeneSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.CountyId;
                                orderSelector = hs => hs.CountyId;
                            }

                            if (vm.GroupBy == "SUBCOUNTY")
                            {
                                firstSelector = h => new SummaryReport {
                                    ConstituencyId = h.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.ConstituencyId,
                                    BeneSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.ConstituencyId;
                                orderSelector = hs => hs.ConstituencyId;
                            }
                            if (vm.GroupBy == "LOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    LocationId = h.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocation.LocationId,
                                    BeneSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };
                                groupSelector = hs => hs.LocationId;
                                orderSelector = hs => hs.LocationId;
                            }
                            if (vm.GroupBy == "SUBLOCATION")
                            {
                                firstSelector = h => new SummaryReport {
                                    SubLocationId = h.HouseholdEnrolment.Household.HouseholdSubLocation.FirstOrDefault().SubLocationId,
                                    BeneSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.SubLocationId;
                                orderSelector = hs => hs.SubLocationId;
                            }

                            if (vm.GroupBy == "PSP")
                            {
                                firstSelector = h => new SummaryReport {
                                    PSPId = h.PspBranch.PspId,
                                    BeneSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.PrimaryRecipientId).Person.Sex.Code,
                                    CgSexId = h.HouseholdEnrolment.Household.HouseholdMembers.FirstOrDefault(y => y.MemberRoleId == h.HouseholdEnrolment.Household.Programme.SecondaryRecipientId).Person.Sex.Code,
                                };

                                groupSelector = hs => hs.PSPId;
                                orderSelector = hs => hs.PSPId;
                            }

                            selector = g => new GenericSummaryReport {
                                Id = g.Key,
                                Total = g.Count(),
                                Male = g.Count(b => b.BeneSexId == "M"),
                                Female = g.Count(b => b.BeneSexId == "F"),
                            };

                            var summaryReportDb = await GenericService.GetGroupedSet(
                                filter,
                                firstSelector,
                                orderSelector,
                                groupSelector,
                                selector,
                                null,
                                null
                            ).ToListAsync().ConfigureAwait(false);

                            foreach (GenericSummaryReport item in summaryReportDb.Where(x => x.Id == null))
                            {
                                item.Id = 0;
                            }

                            //vm.SummaryReportVieModels = summaryReportDb.LeftOuterJoin(
                            //    geolocs,
                            //    lft => lft.Id,
                            //    rgt => rgt.Id,
                            //    (lft, rgt) => new SummaryReportVieModel {
                            //        Id = lft.Id,
                            //        Name = rgt.Name,
                            //        Code = rgt.Code,
                            //        Male = lft.Male,
                            //        Female = lft.Female,
                            //        Constituency = rgt.Constituency,
                            //        County = rgt.County,
                            //        SubLocation = rgt.SubLocation,
                            //        Location = rgt.Location,
                            //        District = rgt.District,
                            //        Division = rgt.Division,
                            //        Total = lft.Total,
                            //        MaleAmount = lft.MaleAmount,
                            //        FemaleAmount = lft.FemaleAmount,
                            //        TotalAmount = lft.TotalAmount
                            //    }).ToList();
                            break;
                        }
                        else
                        {
                            TempData["Key"] = "danger";
                            TempData["MESSAGE"] = "You must specify the Group By Parameter for Summary Reports";
                        }
                        break;
                    }
            }

            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            var sex = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true)).ToList();
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdCarding(HouseholdsEnrolmentReportOption vm)
        {
            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PspXml",
                                vm.PspId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PspId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CardStatusXml",
                                vm.CardStatusId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CardStatusId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "DETAILS"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                    };
                        // vm.Details =
                        // GenericService.GetManyBySp<ReportDetailedHouseholdEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
                case 2:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("PspXml",
                                    vm.PspId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PspId)),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("CardStatusXml",
                                    vm.EnrolmentGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CardStatusId)),
                            },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "SUMMARY"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                    };

                        // vm.Summary =
                        // GenericService.GetManyBySp<ReportSummaryHouseholdsPendingEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
            }

            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);

            ViewBag.CardStatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Card Status").ConfigureAwait(true), "Id", "Description", vm.CardStatusId);
            ViewBag.PspId = new SelectList(await GenericService.GetAsync<Business.Model.Psp>().ConfigureAwait(true), "Id", "Name", vm.PspId);
            var sex = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true)).ToList();
            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);
            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            return View(vm);
        }

        public async Task<ActionResult> HouseholdsPendingAccountOpening(HouseholdsEnrolmentReportOption vm)
        {
            switch (vm.ReportTypeId)
            {
                case 1:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StatusXml",
                                vm.StatusId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.StatusId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("RegGroupXml",
                                vm.RegGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.RegGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EnrolmentGroupXml",
                                vm.EnrolmentGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.EnrolmentGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "DETAILS"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                    };
                        // vm.Details =
                        // GenericService.GetManyBySp<ReportDetailedHouseholdEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
                case 2:
                    {
                        var parameterList = new List<ParameterEntity> {
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("DateRange", vm.DateRange),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StartDate",
                                vm.StartDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EndDate",
                                vm.EndDate ?? (object) DBNull.Value),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ProgrammeXml",
                                vm.ProgrammeId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.ProgrammeId)),
                        },

                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PrimarySexXml",
                                vm.PriReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.PriReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("SecondarySexXml",
                                vm.SecReciSexId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.SecReciSexId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("StatusXml",
                                vm.StatusId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.StatusId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("RegGroupXml",
                                vm.RegGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.RegGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("EnrolmentGroupXml",
                                vm.EnrolmentGroupId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.EnrolmentGroupId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("CountyXml",
                                vm.CountyId == null ? (object) DBNull.Value : this.GetXml((List<int>)vm.CountyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("ConstituencyXml",
                                vm.ConstituencyId == null ? (object) DBNull.Value :this. GetXml((List<int>)vm.ConstituencyId)),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Page",
                                vm.Page ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("PageSize",
                                vm.PageSize ),
                        },
                        new ParameterEntity {
                            ParameterTuple = new Tuple<string, object>("Type", "SUMMARY"  ),
                        },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobDateRangeId", vm.DobDateRangeId),
                            },

                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobStartDate",
                                    vm.DobStartDate ?? (object) DBNull.Value),
                            },
                            new ParameterEntity {
                                ParameterTuple = new Tuple<string, object>("DobEndDate",
                                    vm.DobEndDate ?? (object) DBNull.Value),
                            },
                    };

                        // vm.Summary =
                        // GenericService.GetManyBySp<ReportSummaryHouseholdsPendingEnrolment>(spName,
                        // parameterNames, parameterList).ToList();
                        break;
                    }
            }

            ViewBag.PageSize = this.GetPager(vm.PageSize);
            ViewBag.ProgrammeId = new SelectList(await GenericService.GetAsync<Programme>().ConfigureAwait(true), "Id", "Name", vm.ProgrammeId);
            ViewBag.ReportTypeId = this.GetReportType(vm.ReportTypeId);
            ViewBag.DobDateRangeId = this.GetDOBRangeType(vm.DobDateRangeId);
            ViewBag.SourceId = new SelectList(await GenericService.GetAsync<Source>().ConfigureAwait(true), "Id", "Name", vm.SourceId);
            ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "HHStatus").ConfigureAwait(true), "Id", "Description", vm.StatusId);
            ViewBag.RegGroupId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Registration Group").ConfigureAwait(true), "Id", "Description", vm.RegGroupId);
            ViewBag.EnrolmentGroupId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Enrolment Group").ConfigureAwait(true), "Id", "Description", vm.EnrolmentGroupId);
            var sex = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                .ConfigureAwait(true)).ToList();

            ViewBag.PriReciSexId = new SelectList(sex, "Id", "Description", vm.PriReciSexId);
            ViewBag.SecReciSexId = new SelectList(sex, "Id", "Description", vm.SecReciSexId);

            ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(true), "Id", "Name", vm.CountyId);
            ViewBag.ConstituencyId = vm.CountyId != null ? new SelectList(await GenericService.GetAsync<Constituency>(x => vm.CountyId.Contains(x.CountyId)).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId) : new SelectList(await GenericService.GetAsync<Constituency>(x => x.Id == 0).ConfigureAwait(true), "Id", "Name", vm.ConstituencyId);

            ViewBag.DateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Code", "Description", vm.DateRange);
            ViewBag.CompareDateRange = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Date Range").ConfigureAwait(true), "Id", "Description", vm.CompareDateRange);

            return View(vm);
        }
    }
}