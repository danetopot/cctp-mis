﻿using System;
using CCTPMIS.Models.AuditTrail;
using Elmah;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using PagedList;
    using Services;

    [Authorize]
    public class UserGroupProfilesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public UserGroupProfilesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/UserGroupProfiles/Create
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:ENTRY")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/UserGroupProfiles/Create To protect from overposting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:ENTRY")]
        public ActionResult Create(
            [Bind(Include = "Id,Name,Description")]
            UserGroupProfile userGroupProfile)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.Create(userGroupProfile);
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = userGroupProfile.Id,
                        TableName = "UserGroupProfile",
                        ModuleRightCode = "USER PROFILE:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(userGroupProfile)}",
                        WasSuccessful = true,
                        Description = "UserGroupProfile Created Successfully"
                    });
                    TempData["MESSAGE"] = "User Group Profile has been added Successfully. ";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = "User Group Profile has not been added. Kindly Try again";
                    TempData["KEY"] = "danger";
                    ErrorSignal.FromCurrentContext().Raise(e);
                }
                return RedirectToAction("Index");
            }

            return View(userGroupProfile);
        }

        // GET: admin/UserGroupProfiles/Details/5
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserGroupProfile userGroupProfile = await GenericService
                                                    .GetOneAsync<UserGroupProfile>(x => x.Id == id.Value, "UserGroups.UserGroupType")
                                                    .ConfigureAwait(true);
            if (userGroupProfile == null)
            {
                return HttpNotFound();
            }

            return View(userGroupProfile);
        }

        // GET: admin/UserGroupProfiles/Edit/5
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserGroupProfile userGroupProfile =
                await GenericService.GetOneAsync<UserGroupProfile>(x => x.Id == id.Value).ConfigureAwait(true);

            if (userGroupProfile == null)
            {
                return HttpNotFound();
            }

            return View(userGroupProfile);
        }

        // POST: admin/UserGroupProfiles/Edit/5 To protect from overposting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:MODIFIER")]
        public ActionResult Edit(UserGroupProfile userGroupProfile)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.Update(userGroupProfile);
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = userGroupProfile.Id,
                        TableName = "UserGroupProfile",
                        ModuleRightCode = "USER PROFILE:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(userGroupProfile)}",
                        WasSuccessful = true,
                        Description = "UserGroupProfile Modified Successfully"
                    });
                    TempData["MESSAGE"] = "User Group Profile modified successfully";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = "User Group Profile has not been modified. Kindly Try again";
                    TempData["KEY"] = "danger";
                    ErrorSignal.FromCurrentContext().Raise(e);
                }
                return RedirectToAction("Index");
            }

            return View(userGroupProfile);
        }

        // GET: admin/UserGroupProfiles
        //[GroupCustomAuthorize(Name = "USER GROUP PROFILE:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var geoMasters =
                (await GenericService.GetAsync<UserGroupProfile>(null, x => x.OrderBy(y => y.Name), "UserGroups")
                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(geoMasters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}