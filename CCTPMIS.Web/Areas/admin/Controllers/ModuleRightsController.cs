﻿using CCTPMIS.Models.AuditTrail;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using PagedList;
    using Services;

    [Authorize]
    public class ModuleRightsController : Controller
    {
        // private ApplicationDbContext db = new ApplicationDbContext();
        protected readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public ModuleRightsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/ModuleRights/Create
        public async Task<ActionResult> Create()
        {
            var rightSystemCodeId = int.Parse(WebConfigurationManager.AppSettings["RIGHTSYSTEMCODE"].ToString());

            ViewBag.ModuleId = new SelectList(
                await GenericService.GetAsync<Module>().ConfigureAwait(true),
                "Id",
                "Name");
            ViewBag.RightId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "System Right")
                    .ConfigureAwait(true),
                "Id",
                "Code");
            return View();
        }

        // POST: admin/ModuleRights/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,ModuleId,RightId,Description")]
            ModuleRight moduleRight)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.Create(moduleRight);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = moduleRight.Id,
                        TableName = "ModuleRight",
                        ModuleRightCode = "GEOUNITS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(moduleRight)}",
                        WasSuccessful = true,
                        Description = "Module Right Added Success"
                    });

                    return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    // this.GenericService.Reseed<ModuleRight>( "Id");
                    ViewBag.Message = exception.Message;
                }
            }

            var rightSystemCodeId = int.Parse(WebConfigurationManager.AppSettings["RIGHTSYSTEMCODE"].ToString());
            ViewBag.ModuleId = new SelectList(
                await GenericService.GetAsync<Module>().ConfigureAwait(true),
                "Id",
                "Name",
                moduleRight.ModuleId);
            ViewBag.RightId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "System Right")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                moduleRight.RightId);

            return View(moduleRight);
        }

        // GET: admin/ModuleRights/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var moduleRight = await GenericService.GetOneAsync<ModuleRight>(x => x.Id ==
        // id.Value).ConfigureAwait(true); if (moduleRight == null) { return HttpNotFound(); }

        //    return View(moduleRight);
        //}

        //// POST: admin/ModuleRights/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    var moduleRight = await GenericService.GetOneAsync<ModuleRight>(x => x.Id == id).ConfigureAwait(true);

        //    return RedirectToAction("Index");
        //}

        // GET: admin/ModuleRights/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var moduleRight = await GenericService
                                  .GetOneAsync<ModuleRight>(x => x.Id == id.Value, "SystemCodeDetail,Module")
                                  .ConfigureAwait(true);
            if (moduleRight == null)
            {
                return HttpNotFound();
            }

            return View(moduleRight);
        }

        // GET: admin/ModuleRights/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var moduleRight = await GenericService.GetOneAsync<ModuleRight>(x => x.Id == id.Value).ConfigureAwait(true);
            if (moduleRight == null)
            {
                return HttpNotFound();
            }

            var rightSystemCodeId = int.Parse(WebConfigurationManager.AppSettings["RIGHTSYSTEMCODE"].ToString());
            ViewBag.ModuleId = new SelectList(
                await GenericService.GetAsync<Module>().ConfigureAwait(true),
                "Id",
                "Name",
                moduleRight.ModuleId);
            ViewBag.RightId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "System Right")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                moduleRight.RightId);

            return View(moduleRight);
        }

        // POST: admin/ModuleRights/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "Id,ModuleId,RightId,Description")]
            ModuleRight moduleRight)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.Update(moduleRight);
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = moduleRight.Id,
                        TableName = "ModuleRight",
                        ModuleRightCode = "GEOUNITS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(moduleRight)}",
                        WasSuccessful = true,
                        Description = "Module Right Added Success"
                    });

                    return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ViewBag.Message = exception.Message;
                }
            }

            var rightSystemCodeId = int.Parse(WebConfigurationManager.AppSettings["RIGHTSYSTEMCODE"].ToString());
            ViewBag.ModuleId = new SelectList(
                await GenericService.GetAsync<Module>().ConfigureAwait(true),
                "Id",
                "Name",
                moduleRight.ModuleId);
            ViewBag.RightId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "System Right")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                moduleRight.RightId);

            return View(moduleRight);
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var modules =
                (await GenericService.GetAsync<ModuleRight>(null, x => x.OrderBy(y => y.Id), "SystemCodeDetail,Module")
                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(modules);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}