﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class ExpansionPlansController : Controller
    {
        public readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public ExpansionPlansController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public async Task<ActionResult> Create()
        {
            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAllAsync<Programme>().ConfigureAwait(false),
                "Id",
                "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ExpansionPlanMaster expansionPlanMaster)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            expansionPlanMaster.CreatedBy = userId;
            expansionPlanMaster.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlanMaster.Id,
                    TableName = "ExpansionPlanMaster",
                    ModuleRightCode = "EXPANSION PLAN:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(expansionPlanMaster)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Create Success"
                };
                LogService.AuditTrail(auditTrailVm);

                GenericService.Create(expansionPlanMaster);
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAllAsync<Programme>().ConfigureAwait(false),
                "Id",
                "Name",
                expansionPlanMaster.ProgrammeId);

            return View(expansionPlanMaster);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanMaster = GenericService.GetOne<ExpansionPlanMaster>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,ExpansionPlans,Programme");
            if (expansionPlanMaster == null)
            {
                return HttpNotFound();
            }

            return View(expansionPlanMaster);
        }

        // GET: admin/ExpansionPlanMasters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanMaster = await this.GenericService.GetOneAsync<ExpansionPlanMaster>(x => x.Id == id.Value)
                                          .ConfigureAwait(false);

            if (expansionPlanMaster == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAllAsync<Programme>().ConfigureAwait(false),
                "Id",
                "Name",
                expansionPlanMaster.ProgrammeId);

            return View(expansionPlanMaster);
        }

        // POST: admin/ExpansionPlanMasters/Edit/5 To protect from overposting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ExpansionPlanMaster expansionPlanMaster)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            expansionPlanMaster.ModifiedBy = userId;
            expansionPlanMaster.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(expansionPlanMaster);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlanMaster.Id,
                    TableName = "ExpansionPlanMaster",
                    ModuleRightCode = "EXPANSION PLAN:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(expansionPlanMaster)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Create Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ProgrammeId = new SelectList(
                await GenericService.GetAllAsync<Programme>().ConfigureAwait(false),
                "Id",
                "Name",
                expansionPlanMaster.ProgrammeId);

            return View(expansionPlanMaster);
        }

        // GET: admin/ExpansionPlanMasters
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var expansionPlanMasters = (await GenericService.GetAsync<ExpansionPlanMaster>(
                                            null,
                                            x => x.OrderByDescending(y => y.Id),
                                            "CreatedByUser,ModifiedByUser,Programme").ConfigureAwait(false))
                .ToPagedList(pageNum, pageSize);
            return View(expansionPlanMasters);
        }

        public async Task<ActionResult> Plan(int? expansionPlanMasterId)
        {
            if (expansionPlanMasterId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(false),
                "Id",
                "Name");

            return View();
        }

        // POST: admin/ExpansionPlanMasters/Edit/5 To protect from overposting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Plan(int expansionPlanMasterId, ExpansionPlan expansionPlan)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            expansionPlan.CreatedBy = userId;
            expansionPlan.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(expansionPlan);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlan.Id,
                    TableName = "ExpansionPlan",
                    ModuleRightCode = "EXPANSION PLAN:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(expansionPlan)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Create Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction("Plans", new { id = expansionPlanMasterId });
            }

            return View(expansionPlan);
        }

        public async Task<ActionResult> PlanDetailCreate(int? expansionPlanId)
        {
            if (expansionPlanId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.FinancialYearId = new SelectList(
                await GenericService.GetAllAsync<SystemCodeDetail>().ConfigureAwait(false),
                "Id",
                "Description");

            return View();
        }

        // POST: admin/ExpansionPlanMasters/Edit/5 To protect from overposting attacks, please enable
        // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PlanDetailCreate(int expansionPlanId, ExpansionPlanDetail expansionPlanDetail)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            expansionPlanDetail.CreatedBy = userId;
            expansionPlanDetail.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(expansionPlanDetail);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlanDetail.Id,
                    TableName = "ExpansionPlanDetail",
                    ModuleRightCode = "EXPANSION PLAN:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(expansionPlanDetail)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Create Success"
                };
                LogService.AuditTrail(auditTrailVm);

                return RedirectToAction("PlanDetails", new { id = expansionPlanId });
            }

            ViewBag.FinancialYearId = new SelectList(GenericService.Get<SystemCodeDetail>(), "Id", "Description");

            return View(expansionPlanDetail);
        }

        public async Task<ActionResult> PlanDetailDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanDetail = await this.GenericService.GetOneAsync<ExpansionPlanDetail>(x => x.Id == id.Value)
                                          .ConfigureAwait(false);

            if (expansionPlanDetail == null)
            {
                return HttpNotFound();
            }

            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(false),
                "Id",
                "Name");

            return View(expansionPlanDetail);
        }

        public async Task<ActionResult> PlanDetailEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanDetail = await this.GenericService.GetOneAsync<ExpansionPlanDetail>(x => x.Id == id.Value)
                                          .ConfigureAwait(false);

            if (expansionPlanDetail == null)
            {
                return HttpNotFound();
            }

            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(false),
                "Id",
                "Name");

            return View(expansionPlanDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PlanDetailEdit(ExpansionPlanDetail expansionPlanDetail)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            expansionPlanDetail.ModifiedBy = userId;
            expansionPlanDetail.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(expansionPlanDetail);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlanDetail.Id,
                    TableName = "ExpansionPlanDetail",
                    ModuleRightCode = "EXPANSION PLAN:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(expansionPlanDetail)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Create Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction("PlanDetails", new { id = expansionPlanDetail.ExpansionPlanId });
            }

            return View(expansionPlanDetail);
        }

        public async Task<ActionResult> PlanDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanMaster = await this.GenericService.GetOneAsync<ExpansionPlan>(
                                              x => x.Id == id.Value,
                                              "ExpansionPlanMaster,ExpansionPlanDetails,CreatedByUser,ModifiedByUser,ExpansionPlanDetails.FinancialYear")
                                          .ConfigureAwait(false);

            if (expansionPlanMaster == null)
            {
                return HttpNotFound();
            }

            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(false),
                "Id",
                "Name");

            return View(expansionPlanMaster);
        }

        public async Task<ActionResult> PlanEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanMaster = await this.GenericService.GetOneAsync<ExpansionPlan>(x => x.Id == id.Value)
                                          .ConfigureAwait(false);

            if (expansionPlanMaster == null)
            {
                return HttpNotFound();
            }

            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(false),
                "Id",
                "Name");

            return View(expansionPlanMaster);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PlanEdit(ExpansionPlan expansionPlan)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            expansionPlan.ModifiedBy = userId;
            expansionPlan.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(expansionPlan);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = expansionPlan.Id,
                    TableName = "ExpansionPlan",
                    ModuleRightCode = "EXPANSION PLAN:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(expansionPlan)}",
                    WasSuccessful = true,
                    Description = "Expansion Plan Master Edit Success"
                });

                return RedirectToAction("Plans", new { id = expansionPlan.ExpansionPlanMasterId });
            }

            return View(expansionPlan);
        }

        public ActionResult Plans(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var expansionPlanMaster = GenericService.GetOne<ExpansionPlanMaster>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,Programme,ExpansionPlans,ExpansionPlans.Location");

            if (expansionPlanMaster == null)
            {
                return HttpNotFound();
            }

            return View(expansionPlanMaster);
        }
    }
}