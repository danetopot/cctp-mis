﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class DistrictsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public DistrictsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // GET: admin/Districts/Create
        public ActionResult Create()
        {
            ViewBag.GeoMasterId = new SelectList(GenericService.Get<GeoMaster>(), "Id", "Name");
            return View();
        }

        // POST: admin/Districts/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create(
            [Bind(Include = "Id,Name,Code,GeoMasterId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            District district)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            district.CreatedBy = userId;
            district.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(district);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = district.Id,
                    TableName = "District",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(district)}",
                    WasSuccessful = true,
                    Description = "District Create Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction("Index");
            }

            ViewBag.GeoMasterId = new SelectList(GenericService.Get<GeoMaster>(), "Id", "Name", district.GeoMasterId);
            return View(district);
        }

        /*
        // GET: admin/Districts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var district = GenericService.GetOne<District>(
                x => x.Id == id.Value,
                "GeoMaster,Divisions,CreatedByUser,ModifiedByUser");

            if (district == null)
            {
                return HttpNotFound();
            }

            return View(district);
        }

        // POST: admin/Districts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var district = GenericService.GetOne<District>(x => x.Id == id);
            GenericService.Delete(district);

            return RedirectToAction("Index");
        }
        */

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        // GET: admin/Districts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var district = GenericService.GetOne<District>(
                x => x.Id == id.Value,
                "GeoMaster,CountyDistricts,CreatedByUser,ModifiedByUser");
            if (district == null)
            {
                return HttpNotFound();
            }

            return View(district);
        }

        /*
                [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
                // GET: admin/Districts/Edit/5
                public ActionResult Edit(int? id)
                {
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }

                    var district = GenericService.GetOne<District>(x => x.Id == id.Value, string.Empty);

                    if (district == null)
                    {
                        return HttpNotFound();
                    }

                    ViewBag.GeoMasterId = new SelectList(GenericService.Get<GeoMaster>(), "Id", "Name", district.GeoMasterId);
                    return View(district);
                }

                // POST: admin/Districts/Edit/5 To protect from overposting attacks, please enable
                // the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
                [HttpPost]
                [ValidateAntiForgeryToken]
                [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
                public ActionResult Edit(
                    [Bind(Include = "Id,Name,Code,GeoMasterId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
                    District district)
                {
                    district.ModifiedBy = int.Parse(User.Identity.GetUserId());
                    district.ModifiedOn = DateTime.Now;

                    if (ModelState.IsValid)
                    {
                        GenericService.Update(district);
                        return RedirectToAction("Index");
                    }

                    ViewBag.GeoMasterId = new SelectList(GenericService.Get<GeoMaster>(), "Id", "Name", district.GeoMasterId);
                    return View(district);
                }
                */

        // GET: admin/Districts
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public async Task<ActionResult> Index(int? page, int? geoMasterId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<District> districts = null;
            districts = geoMasterId.HasValue
                            ? (await GenericService.GetAsync<District>(
                                   x => x.GeoMasterId == geoMasterId,
                                   x => x.OrderBy(y => y.Code),
                                   "CreatedByUser,ModifiedByUser,GeoMaster")).ToPagedList(pageNum, pageSize)
                            : (await GenericService.GetAsync<District>(
                                   null,
                                   x => x.OrderBy(y => y.Code),
                                   "CreatedByUser,ModifiedByUser,GeoMaster")).ToPagedList(pageNum, pageSize);
            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name");
            return View(districts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}