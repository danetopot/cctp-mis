﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class LocationsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public LocationsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Locations/Create
        public ActionResult Create()
        {
            ViewBag.DivisionId = new SelectList(GenericService.Get<Division>(), "Id", "Name");

            return View();
        }

        // POST: admin/Locations/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,Name,DivisionId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Location location)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            location.CreatedBy = int.Parse(User.Identity.GetUserId());
            location.CreatedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Create(location);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = location.Id,
                    TableName = "Location",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(location)}",
                    WasSuccessful = true,
                    Description = "Location Added Success"
                });
                return RedirectToAction("Index");
            }

            ViewBag.DivisionId = new SelectList(GenericService.Get<Division>(), "Id", "Name", location.DivisionId);

            return View(location);
        }

        // GET: admin/Locations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var location = GenericService.GetOne<Location>(
                x => x.Id == id.Value,
                "Division.CountyDistrict.District.GeoMaster,CreatedByUser,ModifiedByUser");

            if (location == null)
            {
                return HttpNotFound();
            }

            return View(location);
        }

        // POST: admin/Locations/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    var location = GenericService.GetOne<Location>(x => x.Id == id);
        //    GenericService.Delete(location);
        //    return RedirectToAction("Index");
        //}

        // public ActionResult Index() { var locations = db.Locations.Include(l => l.Division);
        // return View(locations.ToList()); }

        // GET: admin/Locations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var location = GenericService.GetOne<Location>(
                x => x.Id == id.Value,
                "Division.CountyDistrict.District.GeoMaster,CreatedByUser,ModifiedByUser");
            if (location == null)
            {
                return HttpNotFound();
            }

            return View(location);
        }

        // GET: admin/Locations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var location = GenericService.GetOne<Location>(x => x.Id == id);
            if (location == null)
            {
                return HttpNotFound();
            }

            ViewBag.DivisionId = new SelectList(GenericService.Get<Division>(), "Id", "Name", location.DivisionId);

            return View(location);
        }

        // POST: admin/Locations/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Name,DivisionId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Location location)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            location.ModifiedBy = userId;
            location.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(location);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = location.Id,
                    TableName = "Location",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(location)}",
                    WasSuccessful = true,
                    Description = "Location Added Success"
                });

                return RedirectToAction("Index");
            }

            ViewBag.DivisionId = new SelectList(GenericService.Get<Division>(), "Id", "Name", location.DivisionId);

            return View(location);
        }

        // GET: admin/Locations
        public async Task<ActionResult> Index(int? page, int? divisionId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<Location> locations = divisionId.HasValue
                                                 ? (await GenericService.GetAsync<Location>(
                                                            x => x.DivisionId == divisionId,
                                                            x => x.OrderBy(y => y.Code),
                                                            "CreatedByUser,ModifiedByUser,Division.CountyDistrict.District.GeoMaster")
                                                        .ConfigureAwait(false)).ToPagedList(pageNum, pageSize)
                                                 : (await GenericService.GetAsync<Location>(
                                                            null,
                                                            x => x.OrderBy(y => y.Code),
                                                            "CreatedByUser,ModifiedByUser,Division.CountyDistrict.District.GeoMaster")
                                                        .ConfigureAwait(false)).ToPagedList(pageNum, pageSize);

            ViewBag.DivisionId = new SelectList(
                await GenericService.GetAllAsync<Division>().ConfigureAwait(false),
                "Id",
                "Name");
            return View(locations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}