﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    [Authorize]
    public class SubLocationsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public SubLocationsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // GET: admin/SubLocations/Create
        public ActionResult Create()
        {
            ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name");
            ViewBag.LocationId = new SelectList(GenericService.Get<Location>(), "Id", "Name");
            return View();
        }

        // POST: admin/SubLocations/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create(
            [Bind(
                Include =
                    "Id,Name,Code,LocationId,LocalityId,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            SubLocation subLocation)
        {
            subLocation.CreatedBy = int.Parse(User.Identity.GetUserId());
            subLocation.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(subLocation);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = subLocation.Id,
                    TableName = "SubLocation",
                    ModuleRightCode = "PSPS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(subLocation)}",
                    WasSuccessful = true,
                    Description = "SubLocation Created Successfully"
                });
                return RedirectToAction("Index");
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                subLocation.ConstituencyId);
            ViewBag.LocationId = new SelectList(GenericService.Get<Location>(), "Id", "Name", subLocation.LocationId);
            return View(subLocation);
        }

        /*
        // GET: admin/SubLocations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var subLocation = GenericService.GetOne<SubLocation>(
                x => x.Id == id,
                "Locality,CreatedByUser,ModifiedByUser,Location.Division.CountyDistrict.District.GeoMaster,");
            if (subLocation == null)
            {
                return HttpNotFound();
            }

            return View(subLocation);
        }

        // POST: admin/SubLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var subLocation = GenericService.GetOne<SubLocation>(x => x.Id == id);
            GenericService.Delete(subLocation);
            return RedirectToAction("Index");
        }

        */

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        // GET: admin/SubLocations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var subLocation = GenericService.GetOne<SubLocation>(
                x => x.Id == id,
                "Locality,CreatedByUser,ModifiedByUser,Location.Division.CountyDistrict.District.GeoMaster");
            if (subLocation == null)
            {
                return HttpNotFound();
            }

            return View(subLocation);
        }

        /*
        // GET: admin/SubLocations/Edit/5
        [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var subLocation = GenericService.GetOne<SubLocation>(x => x.Id == id);
            if (subLocation == null)
            {
                return HttpNotFound();
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                subLocation.ConstituencyId);
            ViewBag.LocationId = new SelectList(GenericService.Get<Location>(), "Id", "Name", subLocation.LocationId);
            return View(subLocation);
        }

        // POST: admin/SubLocations/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
        public ActionResult Edit(
            [Bind(
                Include =
                    "Id,Name,Code,LocationId,LocalityId,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            SubLocation subLocation)
        {
            subLocation.ModifiedBy = int.Parse(User.Identity.GetUserId());
            subLocation.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(subLocation);
                return RedirectToAction("Index");
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                subLocation.ConstituencyId);
            ViewBag.LocationId = new SelectList(GenericService.Get<Location>(), "Id", "Name", subLocation.LocationId);
            return View(subLocation);
        }
        */

        // GET: admin/Locations
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public async Task<ActionResult> Index(int? page, int? locationId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var subLocations = locationId.HasValue
                                   ? (await GenericService.GetAsync<SubLocation>(
                                              x => x.LocationId == locationId,
                                              x => x.OrderBy(y => y.Code),
                                              "CreatedByUser,ModifiedByUser,Location.Division.CountyDistrict.District.GeoMaster,Locality")
                                          .ConfigureAwait(true)).ToPagedList(pageNum, pageSize)
                                   : (await GenericService.GetAsync<SubLocation>(
                                              null,
                                              x => x.OrderBy(y => y.Code),
                                              "CreatedByUser,ModifiedByUser,Location.Division.CountyDistrict.District.GeoMaster,Locality")
                                          .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            ViewBag.LocationId = new SelectList(
                await GenericService.GetAllAsync<Location>().ConfigureAwait(true),
                "Id",
                "Name");
            return View(subLocations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}