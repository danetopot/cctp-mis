﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class VillagesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public VillagesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Villages/Create
        public ActionResult Create()
        {
            ViewBag.SubLocationId = new SelectList(GenericService.Get<SubLocation>(), "Id", "Name");
            return View();
        }

        // POST: admin/Villages/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(
                Include =
                    "Id,Name,Code,LocationId,LocalityId,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Village village)
        {
            village.CreatedBy = int.Parse(User.Identity.GetUserId());
            village.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(village);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = village.Id,
                    TableName = "Village",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(village)}",
                    WasSuccessful = true,
                    Description = "village Created Successfully"
                });
                return RedirectToAction("Index");
            }

            ViewBag.SubLocationId = new SelectList(
                GenericService.Get<SubLocation>(),
                "Id",
                "Name",
                village.SubLocationId);
            return View(village);
        }

        //// GET: admin/Villages/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var village = GenericService.GetOne<Village>( x => x.Id == id,
        // "CreatedByUser,ModifiedByUser,Location.Division.District.GeoMaster,"); if (village ==
        // null) { return HttpNotFound(); }

        //    return View(village);
        //}

        //// POST: admin/Villages/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    var village = GenericService.GetOne<Village>(x => x.Id == id);
        //    GenericService.Delete(village);
        //    return RedirectToAction("Index");
        //}

        // GET: admin/Villages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var village = GenericService.GetOne<Village>(
                x => x.Id == id,
                "CreatedByUser,ModifiedByUser,SubLocation.Location.Division.District.GeoMaster");
            if (village == null)
            {
                return HttpNotFound();
            }

            return View(village);
        }

        // GET: admin/Villages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var village = GenericService.GetOne<Village>(x => x.Id == id);
            if (village == null)
            {
                return HttpNotFound();
            }

            ViewBag.SubLocationId = new SelectList(
                GenericService.Get<SubLocation>(),
                "Id",
                "Name",
                village.SubLocationId);

            return View(village);
        }

        // POST: admin/Villages/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(
                Include =
                    "Id,Name,Code,LocationId,LocalityId,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Village village)
        {
            village.ModifiedBy = int.Parse(User.Identity.GetUserId());
            village.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(village);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = village.Id,
                    TableName = "Village",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(village)}",
                    WasSuccessful = true,
                    Description = "village modified Successfully"
                });

                return RedirectToAction("Index");
            }

            ViewBag.SubLocationId = new SelectList(
                GenericService.Get<SubLocation>(),
                "Id",
                "Name",
                village.SubLocationId);

            return View(village);
        }

        // GET: admin/Locations
        public async Task<ActionResult> Index(int? page, int? subLocationId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var villages = subLocationId.HasValue
                               ? (await GenericService.GetAsync<Village>(
                                          x => x.SubLocationId == subLocationId,
                                          x => x.OrderBy(y => y.Code),
                                          "CreatedByUser,ModifiedByUser,SubLocation.Location.Division.District.GeoMaster")
                                      .ConfigureAwait(true)).ToPagedList(pageNum, pageSize)
                               : (await GenericService.GetAsync<Village>(
                                          null,
                                          x => x.OrderBy(y => y.Code),
                                          "CreatedByUser,ModifiedByUser,SubLocation.Location.Division.District.GeoMaster")
                                      .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            ViewBag.SubLocationId = new SelectList(
                await GenericService.GetAllAsync<SubLocation>().ConfigureAwait(true),
                "Id",
                "Name");
            return View(villages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}