﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using ExcelDataReader;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class SystemCodesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public SystemCodesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public async Task<ActionResult> BulkUpload(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var psp = await GenericService.GetOneAsync<SystemCode>(x => x.Id == id.Value, "SystemCodeDetails")
                          .ConfigureAwait(true);

            if (psp == null)
            {
                return HttpNotFound();
            }

            return View(psp);
        }

        // POST: admin/PspBranches/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see
        // https://go.microsoft.com/fwlink/?LinkId=317598.
        // [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted",
        // Justification = "Reviewed. Suppression is OK here.")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BulkUpload(int id, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    // ExcelDataReader works with the binary Excel file, so it needs a FileStream to
                    // get started. This is how we avoid dependencies on ACE or Interop:
                    var stream = upload.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;

                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }

                    var result = reader.AsDataSet(
                        new ExcelDataSetConfiguration() {
                            UseColumnDataType = true,
                            ConfigureDataTable = (tableReader) =>
                                new ExcelDataTableConfiguration() {
                                    EmptyColumnNamePrefix
                                            = "Column",
                                    UseHeaderRow =
                                            true,
                                }
                        });
                    reader.Close();
                    var model = result.Tables["SYSTEM_CODE_DETAILS"];
                    var userId = int.Parse(User.Identity.GetUserId());
                    List<SystemCodeDetail> pspBranches = new List<SystemCodeDetail>();
                    foreach (DataRow row in model.Rows)
                    {
                        var pspBranch = new SystemCodeDetail {
                            Code = row["Code"].ToString(),
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            ModifiedBy = null,
                            ModifiedOn = null,
                            Description = row["Description"].ToString(),
                            SystemCodeId = id
                        };

                        pspBranches.Add(pspBranch);
                    }

                    GenericService.AddRange(pspBranches);
                    await GenericService.SaveAsync().ConfigureAwait(true);
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }

            var systemCode = await GenericService
                                 .GetOneAsync<SystemCode>(x => x.Id == id, "CreatedByUser,ModifiedByUser,PspBranches")
                                 .ConfigureAwait(true);
            return View(systemCode);
        }

        // GET: admin/SystemCodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/SystemCodes/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,Code,Description,IsUserMaintained")]
            SystemCode systemCode)
        {
            if (ModelState.IsValid)
            {
                GenericService.Create(systemCode);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = systemCode.Id,
                    TableName = "SystemCode",
                    ModuleRightCode = "SYSTEM CODES:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(systemCode)}",
                    WasSuccessful = true,
                    Description = "systemCode Created Successfully"
                });

                return RedirectToAction("Index");
            }

            return View(systemCode);
        }

        public ActionResult CreateDetail(int id)
        {
            var model = new SystemCodeDetail { SystemCodeId = id };
            return View(model);
        }

        // POST: admin/SystemCodes/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDetail(int id, SystemCodeDetail systemCodeDetail)
        {
            systemCodeDetail.CreatedOn = DateTime.Now;
            systemCodeDetail.CreatedBy = int.Parse(User.Identity.GetUserId());
            systemCodeDetail.Id = 0;
            systemCodeDetail.SystemCodeId = id;

            if (ModelState.IsValid)
            {
                GenericService.Create(systemCodeDetail);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = systemCodeDetail.Id,
                    TableName = "SystemCodeDetail",
                    ModuleRightCode = "SYSTEM CODES:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(systemCodeDetail)}",
                    WasSuccessful = true,
                    Description = "systemCodeDetail Created Successfully"
                });

                return RedirectToAction("Details", new { id = systemCodeDetail.SystemCodeId });
            }

            return View(systemCodeDetail);
        }

        // GET: admin/SystemCodes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var systemCode = GenericService.GetOne<SystemCode>(x => x.Id == id.Value, "SystemCodeDetails");

            if (systemCode == null)
            {
                return HttpNotFound();
            }

            return View(systemCode);
        }

        // GET: admin/SystemCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var systemCode = GenericService.GetOne<SystemCode>(x => x.Id == id.Value);
            if (systemCode == null)
            {
                return HttpNotFound();
            }

            if (!systemCode.IsUserMaintained)
            {
                return RedirectToAction("Details", new { systemCode.Id });
            }

            return View(systemCode);
        }

        // POST: admin/SystemCodes/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Code,Description,IsUserMaintained")]
            SystemCode systemCode)
        {
            if (ModelState.IsValid)
            {
                GenericService.Update(systemCode);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = systemCode.Id,
                    TableName = "SystemCode",
                    ModuleRightCode = "SYSTEM CODES:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(systemCode)}",
                    WasSuccessful = true,
                    Description = "systemCode Modified Successfully"
                });

                return RedirectToAction("Index");
            }

            return View(systemCode);
        }

        public ActionResult EditDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var systemCodeDetail = GenericService.GetOne<SystemCodeDetail>(x => x.Id == id.Value, "SystemCode");
            if (systemCodeDetail == null)
            {
                return HttpNotFound();
            }

            if (!systemCodeDetail.SystemCode.IsUserMaintained)
            {
                return RedirectToAction("Details", new { systemCodeDetail.SystemCode.Id });
            }

            return View(systemCodeDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDetail(SystemCodeDetail systemCodeDetail)
        {
            if (ModelState.IsValid)
            {
                GenericService.Update(systemCodeDetail);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = systemCodeDetail.Id,
                    TableName = "SystemCodeDetail",
                    ModuleRightCode = "SYSTEM CODES:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(systemCodeDetail)}",
                    WasSuccessful = true,
                    Description = "systemCode Modified Successfully"
                });

                return RedirectToAction("Details", systemCodeDetail.SystemCodeId);
            }

            return View(systemCodeDetail);
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var systemCodes =
                (await GenericService
                     .GetAsync<SystemCode>(x => x.IsUserMaintained, x => x.OrderByDescending(y => y.Code))
                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(systemCodes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}