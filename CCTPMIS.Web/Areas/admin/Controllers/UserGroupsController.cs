﻿using CCTPMIS.Models;
using CCTPMIS.Models.AuditTrail;
using Elmah;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class UserGroupsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public UserGroupsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        //[GroupCustomAuthorize(Name = "USER GROUP:ENTRY")]
        // GET: admin/UserGroups/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.UserGroupProfileId = new SelectList(
                await GenericService.GetAsync<UserGroupProfile>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name");
            ViewBag.UserGroupTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "User Group Type").ConfigureAwait(true),
                "Id",
                "Description");

            var allModules = (await GenericService.GetAsync<Module>(null, null, "ModuleRights,ChildModules.ModuleRights").ConfigureAwait(true)).ToList();

            var model = new UserGroupEditorVm {
                Modules = allModules,
            };
            return View(model);
        }

        // POST: admin/UserGroups/Create To protect from over posting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "USER GROUP:ENTRY")]
        public async Task<ActionResult> Create(UserGroupEditorVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            model.UserGroup.CreatedBy = userId;
            model.UserGroup.CreatedOn = DateTime.Now;
            model.UserGroup.UserGroupProfileId = model.UserGroupProfileId;
            model.UserGroup.UserGroupTypeId = model.UserGroupTypeId;
            try
            {
                GenericService.Create(model.UserGroup);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.UserGroup.Id,
                    TableName = "UserGroup",
                    ModuleRightCode = "USER GROUP:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "UserGroup Created Successfully"
                });

                var curreGroupRights = await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == model.UserGroup.Id)
                                           .ConfigureAwait(true);
                GenericService.RemoveRange(curreGroupRights);
                var groupRights = new List<GroupRight>();
                if (model.ModuleRightId != null)
                {
                    groupRights.AddRange(model.ModuleRightId.Select(module => new GroupRight {
                        UserGroupId = model.UserGroup.Id,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                        ModuleRightId = int.Parse(module)
                    }));
                }

                GenericService.AddRange(groupRights);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.UserGroup.Id,
                    TableName = "UserGroup",
                    ModuleRightCode = "USER GROUPS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "UserGroup Created Successfully"
                });
                TempData["MESSAGE"] = "User Group Has been added successfully.";
                TempData["KEY"] = "success";

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "User Group has not been added. Kindly Try again" + e.Message;
                TempData["KEY"] = "danger";

                ErrorSignal.FromCurrentContext().Raise(e);
            }

            ViewBag.UserGroupTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "User Group Type").ConfigureAwait(true),
                "Id",
                "Description", model.UserGroup.UserGroupTypeId);

            ViewBag.UserGroupProfileId = new SelectList(
                await GenericService.GetAsync<UserGroupProfile>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name",
                model.UserGroup.UserGroupProfileId);

            var allModules = (await GenericService.GetAsync<Module>(null, null, "ModuleRights,ChildModules.ModuleRights").ConfigureAwait(true)).ToList();

            var userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == model.UserGroup.Id).ConfigureAwait(true);

            model.UserGroup = userGroup;
            model.Modules = allModules;
            if (model.ModuleRightId != null)
            {
                model.GroupRights = model.ModuleRightId.Select(
                    module => new GroupRight {
                        UserGroupId = 0,
                        CreatedBy = userId,
                        CreatedOn = DateTime.Now,
                        ModuleRightId = int.Parse(module)
                    }).ToList();
            }

            return View(model);
        }

        // GET: admin/UserGroups/Details/5
        //[GroupCustomAuthorize(Name = "USER GROUP:VIEW")]
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserGroup userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == id, "UserGroupProfile,UserGroupType").ConfigureAwait(true);

            if (userGroup == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserGroupProfileId = new SelectList(
                await GenericService.GetAsync<UserGroupProfile>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name",
                userGroup.UserGroupProfileId);

            var allModules = (await GenericService.GetAsync<Module>(null, null, "ModuleRights,ChildModules.ModuleRights").ConfigureAwait(true)).ToList();

            // var allModuleRights = (await GenericService.GetAsync<ModuleRight>(null, null, "Module").ConfigureAwait(true)).ToList();
            var assignedRights = (await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id, null, "ModuleRight").ConfigureAwait(true)).ToList();

            var model = new UserGroupEditorVm {
                UserGroup = userGroup,
                Modules = allModules,
                GroupRights = assignedRights
            };
            return View(model);
        }

        // GET: admin/UserGroups/Edit/5
        //[GroupCustomAuthorize(Name = "USER GROUP:MODIFIER")]
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserGroup userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == id).ConfigureAwait(true);

            if (userGroup == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserGroupProfileId = new SelectList(
                await GenericService.GetAsync<UserGroupProfile>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name",
                userGroup.UserGroupProfileId);

            ViewBag.UserGroupTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "User Group Type").ConfigureAwait(true),
                "Id",
                "Description", userGroup.UserGroupTypeId);

            var allModules = (await GenericService.GetAsync<Module>(null, null, "ModuleRights,ChildModules.ModuleRights").ConfigureAwait(true)).ToList();

            // var allModuleRights = (await GenericService.GetAsync<ModuleRight>(null, null, "Module").ConfigureAwait(true)).ToList();
            var assignedRights = (await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id, null, "ModuleRight").ConfigureAwait(true)).ToList();

            var model = new UserGroupEditorVm {
                UserGroup = userGroup,
                Modules = allModules,
                GroupRights = assignedRights
            };
            return View(model);
        }

        // POST: admin/UserGroups/Edit/5 To protect from over posting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "USER GROUP:MODIFIER")]
        public async Task<ActionResult> Edit(short id, UserGroupEditorVm model)
        {
            model.UserGroup.ModifiedBy = int.Parse(User.Identity.GetUserId());
            model.UserGroup.ModifiedOn = DateTime.Now;

            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var curreGroupRights = await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == model.UserGroup.Id)
                                      .ConfigureAwait(true);

                GenericService.RemoveRange(curreGroupRights);
                try
                {
                    model.UserGroup.UserGroupProfileId = model.UserGroupProfileId;
                    model.UserGroup.UserGroupTypeId = model.UserGroupTypeId;
                    GenericService.Update(model.UserGroup);

                    var groupRights = new List<GroupRight>();
                    if (model.ModuleRightId != null)
                    {
                        groupRights.AddRange(model.ModuleRightId.Select(module => new GroupRight {
                            UserGroupId = model.UserGroup.Id,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            ModuleRightId = int.Parse(module)
                        }));
                    }

                    GenericService.AddRange(groupRights);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.UserGroup.Id,
                        TableName = "UserGroup",
                        ModuleRightCode = "USER GROUPS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "UserGroup Modified Successfully"
                    });

                    TempData["MESSAGE"] = "User Group Has been updated successfully.";
                    TempData["KEY"] = "success";
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = "Error. There was an error modifying the User group. Kindly Try again ";
                    TempData["KEY"] = "danger";
                    ErrorSignal.FromCurrentContext().Raise(e);
                }
                return RedirectToAction("Index");
            }

            ViewBag.UserGroupProfileId = new SelectList(
                await GenericService.GetAsync<UserGroupProfile>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name",
                model.UserGroup.UserGroupProfileId);

            ViewBag.UserGroupTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "User Group Type").ConfigureAwait(true),
                "Id",
                "Description", model.UserGroup.UserGroupTypeId);

            var allModules = (await GenericService.GetAsync<Module>(null, null, "ModuleRights,ChildModules.ModuleRights").ConfigureAwait(true)).ToList();

            var assignedRights = (await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id, null, "ModuleRight").ConfigureAwait(true)).ToList();

            var userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == model.UserGroup.Id).ConfigureAwait(true);

            model.UserGroup = userGroup;
            model.Modules = allModules;
            model.GroupRights = model.ModuleRightId.Select(module => new GroupRight {
                UserGroupId = model.UserGroup.Id,
                CreatedBy = userId,
                CreatedOn = DateTime.Now,
                ModuleRightId = int.Parse(module)
            }).ToList();
            TempData["MESSAGE"] = "User Group has not been changed";
            TempData["KEY"] = "danger";
            return View(model);
        }

        // GET: admin/UserGroups
        //[GroupCustomAuthorize(Name = "USER GROUP:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var geoMasters = (await GenericService.GetAsync<UserGroup>(
                                  null,
                                  x => x.OrderBy(y => y.Name),
                                  "UserGroupProfile,UserGroupType,CreatedByUser,ModifiedByUser").ConfigureAwait(true))
                .ToPagedList(pageNum, pageSize);
            return View(geoMasters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}