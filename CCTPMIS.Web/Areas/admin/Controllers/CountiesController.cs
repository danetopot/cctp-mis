﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class CountiesController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public CountiesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Constituencies(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // County county = db.Counties.Find(id);
            var county = GenericService.GetOne<County>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,GeoMaster,Constituencies");

            if (county == null)
            {
                return HttpNotFound();
            }

            return View(county);
        }

        // GET: admin/Counties/Create
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create()
        {
            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name");
            return View();
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // POST: admin/Counties/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,GeoMasterId,Name,Code,CountyCode,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            County county)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            county.CreatedBy = userId;
            county.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(county);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = county.Id,
                    TableName = "County",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(county)}",
                    WasSuccessful = true,
                    Description = "County Modified Success"
                };
                LogService.AuditTrail(auditTrailVm);

                return RedirectToAction(nameof(Index));
            }

            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name", county.GeoMasterId);
            return View(county);
        }

        /*
        // GET: admin/Counties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // County county = db.Counties.Find(id);
            var county = GenericService.GetOne<County>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser,GeoMaster");
            if (county == null)
            {
                return HttpNotFound();
            }

            return View(county);
        }

        // POST: admin/Counties/Delete/5
        [HttpPost, ActionName(nameof(Delete))]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var county = GenericService.GetOne<County>(x => x.Id == id);

            GenericService.Delete<County>(county);
            return RedirectToAction(nameof(Index));
        }
        */

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        // GET: admin/Counties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // County county = db.Counties.Find(id);
            var county = GenericService.GetOne<County>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,GeoMaster,Constituencies");

            if (county == null)
            {
                return HttpNotFound();
            }

            return View(county);
        }

        // GET: admin/Counties/Details/5
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Districts(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // County county = db.Counties.Find(id);
            var county = GenericService.GetOne<County>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,GeoMaster,CountyDistricts");

            if (county == null)
            {
                return HttpNotFound();
            }

            return View(county);
        }

        // GET: admin/Counties/Edit/5
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // County county = db.Counties.Find(id);
            var county = GenericService.GetOne<County>(x => x.Id == id.Value);
            if (county == null)
            {
                return HttpNotFound();
            }

            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name", county.GeoMasterId);
            return View(county);
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // POST: admin/Counties/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,GeoMasterId,Name,Code,CountyCode,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            County county)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            county.ModifiedBy = userId;
            county.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(county);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = county.Id,
                    TableName = "County",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(county)}",
                    WasSuccessful = true,
                    Description = "County Modified Success"
                };
                LogService.AuditTrail(auditTrailVm);

                return RedirectToAction(nameof(Index));
            }

            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name", county.GeoMasterId);
            return View(county);
        }

        /**/

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        // GET: admin/Counties
        public async Task<ActionResult> Index(int? page, int? geoMasterId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<County> counties = null;
            counties = geoMasterId.HasValue
                           ? (await GenericService.GetAsync<County>(
                                  x => x.GeoMasterId == geoMasterId,
                                  x => x.OrderBy(y => y.Code),
                                  "CreatedByUser,ModifiedByUser,GeoMaster")).ToPagedList(pageNum, pageSize)
                           : (await GenericService.GetAsync<County>(
                                  null,
                                  x => x.OrderBy(y => y.Code),
                                  "CreatedByUser,ModifiedByUser,GeoMaster")).ToPagedList(pageNum, pageSize);
            ViewBag.GeoMasterId = new SelectList(GenericService.GetAll<GeoMaster>(), "Id", "Name");
            return View(counties);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}