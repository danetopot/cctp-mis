﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class PaymentZonesController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public PaymentZonesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/PaymentZones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: admin/PaymentZones/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,Name,Description,Commission,IsPerc,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")]
            PaymentZone paymentZone)
        {
            paymentZone.CreatedBy = int.Parse(User.Identity.GetUserId());
            paymentZone.CreatedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Create(paymentZone);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentZone.Id,
                    TableName = "PaymentZone",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(paymentZone)}",
                    WasSuccessful = true,
                    Description = "PaymentZone Added Success"
                });

                return RedirectToAction("Index");
            }

            return View(paymentZone);
        }

        // GET: admin/PaymentZones/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PaymentZone paymentZone = await GenericService.GetOneAsync<PaymentZone>(
                                          x => x.Id == id.Value,
                                          "CreatedByUser,ModifiedByUser,Locations").ConfigureAwait(false);
            if (paymentZone == null)
            {
                return HttpNotFound();
            }

            return View(paymentZone);
        }

        // GET: admin/PaymentZones/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            PaymentZone paymentZone =
                await GenericService.GetOneAsync<PaymentZone>(x => x.Id == id.Value).ConfigureAwait(false);

            if (paymentZone == null)
            {
                return HttpNotFound();
            }

            return View(paymentZone);
        }

        // POST: admin/PaymentZones/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Name,Description,Commission,IsPerc,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")]
            PaymentZone paymentZone)
        {
            paymentZone.ModifiedBy = int.Parse(User.Identity.GetUserId());
            paymentZone.ModifiedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Update(paymentZone);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = paymentZone.Id,
                    TableName = "PaymentZone",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(paymentZone)}",
                    WasSuccessful = true,
                    Description = "PaymentZone Added Success"
                });

                return RedirectToAction("Index");
            }

            return View(paymentZone);
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var geoMasters =
                (await GenericService.GetAsync<PaymentZone>(
                     null,
                     x => x.OrderBy(y => y.Name),
                     "CreatedByUser,ModifiedByUser").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(geoMasters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}