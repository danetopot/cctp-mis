﻿namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using PagedList;
    using Services;

    [Authorize]
    public class ModulesController : Controller
    {
        // private ApplicationDbContext db = new ApplicationDbContext();
        protected readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public ModulesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var modules = (await GenericService.GetAsync<Module>(
                               x => x.ParentModuleId == null,
                               x => x.OrderBy(y => y.Id),
                               "ParentModule,ChildModules").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(modules);
        }

        // GET: admin/Modules
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}