﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class GeoMastersController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        public GeoMastersController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Counties(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var geoMaster = GenericService.GetOne<GeoMaster>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,Counties");

            // GeoMasterService.GetGeoMasterAsync(id.Value);
            if (geoMaster == null)
            {
                return HttpNotFound();
            }

            return View(geoMaster);
        }

        // GET: admin/GeoMasters/Create
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create()
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // POST: admin/GeoMasters/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Year,IsActive,Description")]   GeoMaster geoMaster)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            geoMaster.CreatedBy = userId;
            geoMaster.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create<GeoMaster>(geoMaster);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = geoMaster.Id,
                    TableName = "GeoMaster",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(geoMaster)}",
                    WasSuccessful = true,
                    Description = "GeoMaster Created Success"
                });
                return RedirectToAction(nameof(Index));
            }

            return View(geoMaster);
        }

        /*
        // GET: admin/GeoMasters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var geoMaster = GenericService.GetOne<GeoMaster>(x => x.Id == id.Value);
            if (geoMaster == null)
            {
                return HttpNotFound();
            }

            return View(geoMaster);
        }

        // POST: admin/GeoMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            return RedirectToAction("Index");
        }
        */

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var geoMaster = GenericService.GetOne<GeoMaster>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser");

            // GeoMasterService.GetGeoMasterAsync(id.Value);
            if (geoMaster == null)
            {
                return HttpNotFound();
            }

            return View(geoMaster);
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Districts(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var geoMaster = GenericService.GetOne<GeoMaster>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,Districts");

            // GeoMasterService.GetGeoMasterAsync(id.Value);
            if (geoMaster == null)
            {
                return HttpNotFound();
            }

            return View(geoMaster);
        }

        /* [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
         // GET: admin/GeoMasters/Edit/5
         public ActionResult Edit(int? id)
         {
             if (id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }

             // GeoMaster geoMaster = GeoMasterService.GetGeoMasterAsync(id.Value);
             var geoMaster = GenericService.GetOne<GeoMaster>(x => x.Id == id.Value);

             if (geoMaster == null)
             {
                 return HttpNotFound();
             }

             return View(geoMaster);
         }

         // POST: admin/GeoMasters/Edit/5 To protect from overposting attacks, please enable the
         // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
         [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
         [HttpPost]
         [ValidateAntiForgeryToken]
         public ActionResult Edit(
             [Bind(Include = "Id,Name,Year,IsActive,Description,CreatedBy,CreatedOn,ModifiedOn,ModifiedBy")]
             GeoMaster geoMaster)
         {
             geoMaster.ModifiedBy = int.Parse(User.Identity.GetUserId());
             geoMaster.ModifiedOn = DateTime.Now;

             if (ModelState.IsValid)
             {
                 GenericService.Update<GeoMaster>(geoMaster);

                 return RedirectToAction(nameof(Index));
             }

             return View(geoMaster);
         }
         */

        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        // GET: admin/GeoMasters
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var geoMasters =
                (await GenericService.GetAsync<GeoMaster>(
                     null,
                     x => x.OrderBy(y => y.Name),
                     "CreatedByUser,ModifiedByUser")).ToPagedList(pageNum, pageSize);
            return View(geoMasters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}