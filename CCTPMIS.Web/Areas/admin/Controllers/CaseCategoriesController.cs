﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class CaseCategoriesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public CaseCategoriesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageNm = page ?? 1;
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var caseCategories = (await GenericService.GetAsync<CaseCategory>(null, x => x.OrderByDescending(y => y.Id), "CreatedByUser,ModifiedByUser,ApvByUser,CaseType,TypeofDay").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(caseCategories);
        }

        // GET: CaseManagement/CaseCategories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var caseCategory = await GenericService.GetOneAsync<CaseCategory>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser,ApvByUser,CaseType").ConfigureAwait(true);
            if (caseCategory == null)
            {
                return HttpNotFound();
            }
            return View(caseCategory);
        }

        // GET: CaseManagement/CaseCategories/Create
        public async Task<ActionResult> Create()
        {
            var caseCategories = "Change Types,EFC,Complaint Types";
            ViewBag.CaseTypeId = new SelectList(await GenericService.GetAsync<SystemCode>(x => caseCategories.Contains(x.Code)).ConfigureAwait(false), "Id", "Description");
            ViewBag.CategoryId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "None").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            ViewBag.TypeofDayId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SLA Day Types").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            return View();
        }

        // POST: CaseManagement/CaseCategories/Create To protect from overposting attacks, please
        // enable the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CaseCategory caseCategory)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    caseCategory.CreatedBy = int.Parse(User.Identity.GetUserId());
                    caseCategory.CreatedOn = DateTime.Now;
                    GenericService.AddOrUpdate(caseCategory);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = caseCategory.Id,
                        TableName = "CaseCategory",
                        ModuleRightCode = "CASE CATEGORIES:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(caseCategory)}",
                        WasSuccessful = true,
                        Description = "CaseCategory Modified Successfully"
                    });

                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                // log ex
            }
            finally
            {
                var caseCategories = "Change Types,EFC,Complaint Types";
                ViewBag.CaseTypeId =
                    new SelectList(
                        await GenericService.GetAsync<SystemCode>(x => caseCategories.Contains(x.Code))
                            .ConfigureAwait(false), "Id", "Description", caseCategory.CaseTypeId);

                ViewBag.TypeofDayId =
                    new SelectList(
                        (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SLA Day Types")
                            .ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description",
                        caseCategory.TypeofDayId);
            }

            return View(caseCategory);
        }

        // GET: CaseManagement/CaseCategories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var caseCategory = await GenericService.GetOneAsync<CaseCategory>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser,ApvByUser,CaseType").ConfigureAwait(true);

            if (caseCategory == null)
            {
                return HttpNotFound();
            }

            var caseCategories = "Change Types,EFC,Complaint Types";
            ViewBag.CaseTypeId = new SelectList(await GenericService.GetAsync<SystemCode>(x => caseCategories.Contains(x.Code)).ConfigureAwait(false), "Id", "Description", caseCategory.CaseTypeId);
            ViewBag.TypeofDayId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SLA Day Types").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", caseCategory.TypeofDayId);
            return View(caseCategory);
        }

        // POST: CaseManagement/CaseCategories/Edit/5 To protect from overposting attacks, please
        // enable the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CaseCategory caseCategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    caseCategory.ModifiedBy = int.Parse(User.Identity.GetUserId());
                    caseCategory.ModifiedOn = DateTime.Now;
                    GenericService.AddOrUpdate(caseCategory);

                    //LogService.AuditTrail(new AuditTrailVm
                    //{
                    //    UserId = $"{User.Identity.GetUserId()}",
                    //    Key1 = caseCategory.Id,
                    //    TableName = "CaseCategory",
                    //    ModuleRightCode = "CASE CATEGORIES:MODIFIER",
                    //    Record = $"{JsonConvert.SerializeObject(caseCategory)}",
                    //    WasSuccessful = true,
                    //    Description = "CaseCategory Modified Successfully"
                    //});
                    TempData["key"] = "success";
                    TempData["Message"] = "Case Category edited successfully";

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["key"] = "danger";
                    TempData["Message"] = "Not Changed " + e.Message + e?.InnerException?.Message;
                }
            }
            else
            {
                TempData["key"] = "danger";
                TempData["Message"] = "Not Changed . Invalid Model";
            }

            var caseCategories = "Change Types,EFC,Complaint Types";
            ViewBag.CaseTypeId = new SelectList(await GenericService.GetAsync<SystemCode>(x => caseCategories.Contains(x.Code)).ConfigureAwait(false), "Id", "Description", caseCategory.CaseTypeId);
            // ViewBag.CategoryId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x
            // => x.SystemCodeId == caseCategory.CaseTypeId).ConfigureAwait(false)).OrderBy(x =>
            // x.OrderNo), "Id", "Description", caseCategory.CategoryId);
            ViewBag.TypeofDayId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SLA Day Types").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", caseCategory.TypeofDayId);

            var caseCategoriesTypes = "Change Types,EFC,Complaint Types";
            ViewBag.CaseTypeId =
                new SelectList(
                    await GenericService.GetAsync<SystemCode>(x => caseCategoriesTypes.Contains(x.Code))
                        .ConfigureAwait(false), "Id", "Description", caseCategory.CaseTypeId);

            ViewBag.TypeofDayId =
                new SelectList(
                    (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SLA Day Types")
                        .ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description",
                    caseCategory.TypeofDayId);
            return View(caseCategory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}