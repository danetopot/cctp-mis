﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models;
using CCTPMIS.Models.Account;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Email;
using CCTPMIS.Models.Users;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        public IEmailService EmailService;
        private readonly ILogService LogService;
        public GenericService GenericService;

        public UserService UserService;

        private ApplicationRoleManager _roleManager;

        private ApplicationUserManager _userManager;

        public UsersController(EmailService emailService, GenericService genericService, ILogService logService)
        {
            EmailService = emailService;
            GenericService = genericService;
            LogService = logService;
        }

        public UsersController(
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager,
            UserService userService,
            EmailService emailService,
            ILogService logService,
        GenericService genericService)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            UserService = userService;
            EmailService = emailService;
            GenericService = genericService;
            LogService = logService;
            UserManager.MaxFailedAccessAttemptsBeforeLockout = 5;
            UserManager.UserLockoutEnabledByDefault = true;
            UserManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(60);

            UserManager.UserValidator =
                new UserValidator<ApplicationUser, int>(UserManager) {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

            // Configure validation logic for passwords
            UserManager.PasswordValidator = new PasswordValidator {
                RequiredLength = 8,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
        }

        public ApplicationRoleManager RoleManager
        {
            get => _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            private set => _roleManager = value;
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> Activate(int id)
        {
            try
            {
                var newId = UserService.ActivateUser(id);
                var password = UserService.GeneratePassword(14);
                var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == id);
                var hashedPassword = UserManager.PasswordHasher.HashPassword(password);
                user.PasswordChangeDate = DateTime.Now;
                user.PasswordHash = hashedPassword;
                user.PasswordChangeDate = DateTime.Now.AddDays(-90);
                user.IsActive = true;
                user.IsLocked = false;
                GenericService.AddOrUpdate(user);
                TempData["MESSAGE"] = "The User Activation was completed Successfully.";
                TempData["KEY"] = "success";

                var tEmail = new Thread(() => EmailService.SendAsync(
                    new ChangePasswordEmail {
                        FirstName = user.FirstName,
                        To = user.Email,
                        Password = password,
                        Subject =
                            "Account Activated"
                            + WebConfigurationManager.AppSettings["SYSTEM_NAME"]
                            + " -  ",
                        Title = "Account Activated"
                                + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                    })
                );
                tEmail.Start();
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "The user was not Activated. Kindly try again later";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = id });
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public ActionResult ChangePassword(int id)
        {
            var db = new ApplicationDbContext();
            var user = db.Users.Find(id);
            var model = new ResetPasswordViewModel { Email = user.Email };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(int id, ResetPasswordViewModel model)
        {
            try
            {
                var password = UserService.GeneratePassword(14);

                var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == id);
                var hashedPassword = UserManager.PasswordHasher.HashPassword(password);
                user.PasswordChangeDate = DateTime.Now;
                user.PasswordHash = hashedPassword;
                user.PasswordChangeDate = DateTime.Now.AddDays(-90);

                GenericService.AddOrUpdate(user);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = user.Id,
                    TableName = "User",
                    ModuleRightCode = "USER:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(user)}",
                    WasSuccessful = true,
                    Description = "User Modified Successfully"
                });

                var tEmail = new Thread(() => EmailService.SendAsync(
                    new ChangePasswordEmail {
                        FirstName = user.FirstName,
                        To = user.Email,
                        Password = password,
                        Subject =
                            "Password Changed "
                            + WebConfigurationManager.AppSettings["SYSTEM_NAME"]
                            + " -  Confirm your account",
                        Title = "Password Changed "
                                + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                    })
                    );
                tEmail.Start();

                return View("ChangePasswordSuccess", new { id });
            }
            catch
            {
                return View("ChangePasswordError", new { id });
            }
        }

        [GroupCustomAuthorize(Name = "USERS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            ViewBag.UserGroupId = new SelectList(
                await GenericService.GetAsync<UserGroup>(x => x.Id != 1).ConfigureAwait(true),
                "Id",
                "Name");
            return View();
        }

        [GroupCustomAuthorize(Name = "USERS:ENTRY")]
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var password = UserService.GeneratePassword(14);
                    var user = new ApplicationUser();
                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(userViewModel, user);
                    user.UserName = userViewModel.Email;
                    user.PasswordChangeDate = null;
                    user.CreatedOn = DateTime.Now;
                    user.LockoutEnabled = true;
                    user.PhoneNumberConfirmed = false;
                    user.EmailConfirmed = false;
                    user.Organization = userViewModel.Organization;
                    user.Department = userViewModel.Department;
                    user.Position = userViewModel.Position;

                    user.UserGroupId = userViewModel.UserGroupId;

                    user.CreatedBy = int.Parse(User.Identity.GetUserId());

                    var adminresult = await UserManager.CreateAsync(user, password).ConfigureAwait(true);

                    // Add User to the selected Groups
                    if (adminresult.Succeeded)
                    {
                        LogService.AuditTrail(new AuditTrailVm {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = user.Id,
                            TableName = "User",
                            ModuleRightCode = "USERS:ENTRY",
                            Record = $"{JsonConvert.SerializeObject(user)}",
                            WasSuccessful = true,
                            Description = "User Created Successfully"
                        });

                        var code = UserManager.GenerateEmailConfirmationToken(user.Id);

                        var callbackUrl = Url.Action(
                            "ConfirmEmail",
                            "Account",
                            new { userId = user.Id, @area = string.Empty, code },
                            protocol: Request.Url.Scheme);

                        await EmailService.SendAsync(
                            new ConfirmEmail {
                                FirstName = user.FirstName,
                                To = user.Email,
                                Password = password,
                                PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                                ConfirmationLink = callbackUrl,
                                Subject = "Welcome and Confirm your account",
                                Title = "Welcome to " + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                            }).ConfigureAwait(true);
                        TempData["MESSAGE"] = "The User was created successfully, An email has been sent to the associated Email.";
                        TempData["KEY"] = "success";

                        // await GroupService.SetUserGroupsAsync(user.Id, userViewModel.UserGroupId).ConfigureAwait(true);
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "The User Creation Failed. Kindly try again later";
                TempData["KEY"] = "danger";
            }
            ViewBag.UserGroupId = new SelectList(
                await GenericService.GetAllAsync<UserGroup>().ConfigureAwait(true),
                "Id",
                "Name",
                userViewModel.UserGroupId);

            return View(userViewModel);
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public ActionResult Deactivate(int id)
        {
            try
            {
                var newId = UserService.DeactivateUser(id);
                TempData["MESSAGE"] = "The User Deactivation was completed successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "The User Deactivation failed.";
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Details", new { id });
        }

        [GroupCustomAuthorize(Name = "USERS:VIEW")]
        public async Task<ActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // var user = await UserManager.FindByIdAsync(id,"s").ConfigureAwait(true);
            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == id, "UserGroup")
                           .ConfigureAwait(true);

            return View(user);
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (id == int.Parse(User.Identity.GetUserId()))
            {
                TempData["MESSAGE"] = "You cannot edit your own account!";
                TempData["KEY"] = "success";
                return this.RedirectToAction("Index");
            }

            var user = await UserManager.FindByIdAsync(id).ConfigureAwait(true);
            if (user == null)
            {
                return HttpNotFound();
            }

            var model = new EditUserViewModel();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, model);

            // Display a list of available Groups:
            ViewBag.UserGroupId = new SelectList(
                await GenericService.GetAllAsync<UserGroup>().ConfigureAwait(true),
                "Id",
                "Name",
                user.UserGroupId);

            model.UserGroup = await GenericService.GetOneAsync<SystemCodeDetail>(X => X.Id == user.UserGroup.UserGroupTypeId).ConfigureAwait(false);
            return View(model);
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditUserViewModel editUser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByIdAsync(editUser.Id);
                    if (user == null)
                    {
                        return HttpNotFound();
                    }

                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(editUser, user);

                    user.ModifiedBy = int.Parse(User.Identity.GetUserId());
                    user.ModifiedOn = DateTime.Now;

                    await UserManager.UpdateAsync(user).ConfigureAwait(true);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "USERS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(user)}",
                        WasSuccessful = true,
                        Description = "User Modified Successfully"
                    });

                    return RedirectToAction(nameof(Index));
                }

                TempData["MESSAGE"] = "The User was modified successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "The User Editing failed.";
                TempData["KEY"] = "danger";
            }
            ViewBag.UserGroupId = new SelectList(
                await GenericService.GetAllAsync<UserGroup>().ConfigureAwait(true),
                "Id",
                "Name",
                editUser.UserGroupId);

            ModelState.AddModelError(string.Empty, "Something failed.");
            return View(model: editUser);
        }

        [GroupCustomAuthorize(Name = "USERS:VIEW")]
        public async Task<ActionResult> Index(int? page, string staffNo, string nationalIdNo, string name, string userGroup, string email, string phoneNumber)
        {
            Expression<Func<ApplicationUser, bool>> filter = x => x.FirstName != null;
            Expression<Func<ApplicationUser, bool>> filterx = null;

            if (!string.IsNullOrEmpty(name))
            {
                filterx = x =>
                    (x.FirstName.Contains(name) || x.MiddleName.Contains(name) ||
                     x.Surname.Contains(name));

                filter = filter.And(filterx);
            }

            if (!string.IsNullOrEmpty(nationalIdNo))
            {
                filterx = x => x.NationalIdNo.Contains(nationalIdNo);
                filter = filter.And(filterx);
            }

            if (!string.IsNullOrEmpty(staffNo))
            {
                filterx = x => x.StaffNo.Contains(staffNo);
                filter = filter.And(filterx);
            }
            if (!string.IsNullOrEmpty(email))
            {
                filterx = x => x.Email.Contains(email);
                filter = filter.And(filterx);
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                filterx = x => x.PhoneNumber.Contains(phoneNumber);
                filter = filter.And(filterx);
            }
            if (!string.IsNullOrEmpty(userGroup))
            {
                filterx = x => x.UserGroup.Name.Contains(userGroup);
                filter = filter.And(filterx);
            }

            var users = (await GenericService.GetAsync(filter, x => x.OrderBy(z => z.FirstName), "UserGroup")
                .ConfigureAwait(true)).ToPagedList(
                page ?? 1,
                int.Parse(WebConfigurationManager.AppSettings["USERS_PAGESIZE"]));

            var model = new UsersListViewModel {
                Users = users,
                StaffNo = staffNo,
                UserGroup = userGroup,
                Email = email,
                PhoneNumber = phoneNumber,
                Name = name,
                NationalIdNo = nationalIdNo
            };

            // var model = UserManager.Users.OrderBy(x => x.FirstName,"UserGroup").ToPagedList(page
            // ?? 1, int.Parse(WebConfigurationManager.AppSettings["USERS_PAGESIZE"]));
            return View(model);
        }

        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        [HttpPost]
        public async Task<ActionResult> Lockout(int id)
        {
            var newId = UserService.LockUser(id);

            var u = await UserManager.FindByIdAsync(id);
            LogService.AuditTrail(new AuditTrailVm {
                UserId = $"{User.Identity.GetUserId()}",
                Key1 = u.Id,
                TableName = "User",
                ModuleRightCode = "USERS:MODIFIER",
                Record = $"{JsonConvert.SerializeObject(u)}",
                WasSuccessful = true,
                Description = "User Locked Out Successfully"
            });

            return RedirectToAction("Details", new { id = newId });
        }

        [GroupCustomAuthorize(Name = "USERS:ENTRY")]
        public ActionResult ResendActivationEmail(int id)
        {
            var db = new ApplicationDbContext();
            var user = db.Users.Find(id);
            var model = new ResendActivationLinkViewModel {
                Email = user.Email,
                FullName = user.FullName,
                PhoneNumber = user.PhoneNumber
            };
            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> ResendActivationEmail(int id, ResendActivationLinkViewModel model)
        {
            try
            {
                var password = UserService.GeneratePassword(14);
                var db = new ApplicationDbContext();
                var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == id);
                var hashedPassword = UserManager.PasswordHasher.HashPassword(password);
                user.PasswordChangeDate = DateTime.Now;
                user.PasswordHash = hashedPassword;
                user.PasswordChangeDate = DateTime.Now.AddDays(-90);

                GenericService.AddOrUpdate(user);
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id).ConfigureAwait(true);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = user.Id,
                    TableName = "User",
                    ModuleRightCode = "USERS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(user)}",
                    WasSuccessful = true,
                    Description = "User Resend Email Successfully"
                });

                var callbackUrl = Url.Action(
                    "ConfirmEmail",
                    "Account",
                    new { userId = user.Id, @area = string.Empty, code },
                    protocol: Request.Url.Scheme);

                await EmailService.SendAsync(
                    new ConfirmEmail {
                        FirstName = user.FirstName,
                        To = user.Email,
                        Password = password,
                        PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                        ConfirmationLink = callbackUrl,
                        Subject = "Welcome and Confirm your account",
                        Title = "Welcome to " + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                    }).ConfigureAwait(true);

                TempData["MESSAGE"] = "The Email has been sent Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                var loger = new LogService(GenericService);
                loger.FileLog(MisKeys.ERROR, e.StackTrace, User.Identity.GetUserId());
                TempData["MESSAGE"] = "The Email has not been sent." + e.Message;
                TempData["KEY"] = "danger";
            }

            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> Unlock(int id)
        {
            var newId = UserService.UnLockUser(id);
            var password = UserService.GeneratePassword(14);
            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == id);
            var hashedPassword = UserManager.PasswordHasher.HashPassword(password);
            user.PasswordChangeDate = DateTime.Now;
            user.PasswordHash = hashedPassword;
            user.PasswordChangeDate = DateTime.Now.AddDays(-90);

            GenericService.AddOrUpdate(user);

            var tEmail = new Thread(() => EmailService.SendAsync(
                new UnlockUserEmail {
                    FirstName = user.FirstName,
                    To = user.Email,
                    Password = password,
                    Subject =
                        "Your Account has been Unlocked "
                        + WebConfigurationManager.AppSettings["SYSTEM_NAME"]
                        + " ",
                    Title = "Your Account has been Unlocked"
                            + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                })
            );
            tEmail.Start();

            LogService.AuditTrail(new AuditTrailVm {
                UserId = $"{User.Identity.GetUserId()}",
                Key1 = user.Id,
                TableName = "User",
                ModuleRightCode = "USERS:MODIFIER",
                Record = $"{JsonConvert.SerializeObject(user)}",
                WasSuccessful = true,
                Description = "User Unlocked Successfully"
            });
            return RedirectToAction("Details", new { id = newId });
        }

        #region Programme Officer

        [HttpGet]
        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> Regions(int id, string region)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (id == int.Parse(User.Identity.GetUserId()))
            {
                TempData["MESSAGE"] = "You cannot edit your own account!";
                TempData["KEY"] = "success";

                return this.RedirectToAction("Index");
            }

            var user = await UserManager.FindByIdAsync(id).ConfigureAwait(true);
            if (user == null)
            {
                return HttpNotFound();
            }

            var programmeOfficer = (await GenericService.GetAsync<ProgrammeOfficer>(x => x.UserId == id).ConfigureAwait(true)).FirstOrDefault();

            var model = new RegionUserViewModel();

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, model);

            if (programmeOfficer != null)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAllAsync<County>().ConfigureAwait(true), "Id", "Name", programmeOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAllAsync<Constituency>().ConfigureAwait(true), "Id", "Name", programmeOfficer.ConstituencyId);
            }
            else
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAllAsync<County>().ConfigureAwait(true), "Id", "Name");
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAllAsync<Constituency>().ConfigureAwait(true), "Id", "Name");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "USERS:MODIFIER")]
        public async Task<ActionResult> Regions(RegionUserViewModel regionUser)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(regionUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(regionUser, user);

                await UserManager.UpdateAsync(user).ConfigureAwait(true);

                var programmeOfficer = (await GenericService.GetAsync<ProgrammeOfficer>
                    (x => x.UserId == regionUser.Id).ConfigureAwait(true)).FirstOrDefault();

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = user.Id,
                    TableName = "User",
                    ModuleRightCode = "USERS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(user)}",
                    WasSuccessful = true,
                    Description = "User Regions Updated Successfully"
                });

                var model = new RegionUserViewModel();

                new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, model);

                if (programmeOfficer != null)
                {
                    ViewBag.CountyId = new SelectList(await GenericService.GetAllAsync<County>().ConfigureAwait(true), "Id", "Name", programmeOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetAllAsync<Constituency>().ConfigureAwait(true), "Id", "Name", programmeOfficer.ConstituencyId);
                }
                else
                {
                    ViewBag.CountyId = new SelectList(await GenericService.GetAllAsync<County>().ConfigureAwait(true), "Id", "Name");
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetAllAsync<Constituency>().ConfigureAwait(true), "Id", "Name");
                }

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Something failed.");
            return View();
        }

        #endregion Programme Officer

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_roleManager != null)
                {
                    _roleManager.Dispose();
                    _roleManager = null;
                }
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}