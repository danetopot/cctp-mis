﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.Account;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Email;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.Users;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    [Authorize]
    public class EnumeratorsController : Controller
    {
        public IEmailService EmailService;
        public GenericService GenericService;
        public EnumeratorService EnumeratorService;
        private readonly ILogService LogService;
        private ApplicationDbContext db = new ApplicationDbContext();

        public EnumeratorsController(EnumeratorService enumeratorService, EmailService emailService, GenericService genericService, ILogService logService)
        {
            EnumeratorService = enumeratorService;
            EmailService = emailService;
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> ActivateLoc(int id)
        {
            var model = await GenericService.GetOneAsync<EnumeratorLocation>(x => x.Id == id);

            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> ActivateLoc(IdClass model)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var enumeratorLocation = await GenericService.GetOneAsync<EnumeratorLocation>(x => x.Id == model.Id);
                enumeratorLocation.IsActive = true;
                GenericService.Update(enumeratorLocation);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = enumeratorLocation.Id,
                    TableName = "EnumeratorLocation",
                    ModuleRightCode = "ENUMERATORS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(enumeratorLocation)}",
                    WasSuccessful = true,
                    Description = "Enumerator Activate Location Success"
                };
                LogService.AuditTrail(auditTrailVm);
            }
            catch (Exception)
            {
            }

            return RedirectToAction("Details", new { id = model.Id });
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> DeActivateLoc(int id)
        {
            var model = await GenericService.GetOneAsync<EnumeratorLocation>(x => x.Id == id);

            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> DeActivateLoc(IdClass model)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var enumeratorLocation = await GenericService.GetOneAsync<EnumeratorLocation>(x => x.Id == model.Id);
                enumeratorLocation.IsActive = false;
                GenericService.Update(enumeratorLocation);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = enumeratorLocation.Id,
                    TableName = "EnumeratorLocation",
                    ModuleRightCode = "ENUMERATORS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(enumeratorLocation)}",
                    WasSuccessful = true,
                    Description = "Enumerator Location DeActivateLoc "
                };
                LogService.AuditTrail(auditTrailVm);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public ActionResult Activate(int id)
        {
            var newId = EnumeratorService.ActivateEnumerator(id);
            return RedirectToAction("Details", new { id = newId });
        }

        public ActionResult NotFound()
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public ActionResult ChangePassword(int id)
        {
            var db = new ApplicationDbContext();
            var user = db.Enumerators.Find(id);
            var model = new ResetPasswordViewModel { Email = user.Email };
            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> ChangePassword(int id, ResetPasswordViewModel model)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var password = EnumeratorService.GeneratePin(6);
                var enumerator = await GenericService.GetOneAsync<Enumerator>(x => x.Id == id);
                var hashedPassword = EasyMD5.Hash(password);
                enumerator.PasswordChangeDate = DateTime.Now;
                enumerator.PasswordHash = hashedPassword;
                enumerator.PasswordChangeDate = DateTime.Now.AddDays(-90);
                GenericService.AddOrUpdate(enumerator);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = enumerator.Id,
                    TableName = "Enumerator",
                    ModuleRightCode = "ENUMERATORS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(enumerator)}",
                    WasSuccessful = true,
                    Description = "Enumerator Change Pin"
                };
                LogService.AuditTrail(auditTrailVm);
                await EmailService.SendAsync(
                    new ChangePinEmail {
                        FirstName = enumerator.FirstName,
                        To = enumerator.Email,
                        Pin = password,
                        Subject =
                                "Pin Changed "
                                + WebConfigurationManager.AppSettings["SYSTEM_NAME"]
                                + "",
                        Title = "Pin Changed "
                                    + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                    }).ConfigureAwait(true);

                return View("ChangePasswordSuccess", new { id });
            }
            catch
            {
                return View("ChangePasswordError", new { id });
            }
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var userId = int.Parse(User.Identity.GetUserId());

            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(x => x.UserId == userId);
            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetProgOfficerLocations";
            var parameterNames = "@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                 new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                 new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var locations = GenericService.GetManyBySp<Location>(spName, parameterNames, parameterList).ToList();

            
            var EnLocations = new List<EnLocation>();
            foreach (var loc in locations)
            {
                EnLocations.Add(new EnLocation() {
                    Id = loc.Id,
                    Constituency = db.Constituencies.Find(constituencyId).Name,
                    Display = loc.Name,
                    IsSelected = false
                });
            }

            

            var model = new RegisterEnumeratorViewModel {
                Locations = EnLocations
            };

            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:ENTRY")]
        public async Task<ActionResult> Create(RegisterEnumeratorViewModel userViewModel)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(x => x.UserId == userId);

            if (ModelState.IsValid)
            {
                if (progOfficer.ConstituencyId.HasValue)
                {
                    var guid = Guid.NewGuid().ToString();
                    var password = EnumeratorService.GeneratePin(4);
                    LogService logger = new LogService(GenericService);
                    logger.FileLog("ENUMERATOR_PIN", "PIN : " + password + " \n Password: " + userViewModel.Email + " \n National ID : " + userViewModel.NationalIdNo, userId.ToString());
                    var enumerator = new Enumerator();
                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(userViewModel, enumerator);
                    enumerator.Email = userViewModel.Email;
                    enumerator.SecurityStamp = guid;
                    enumerator.ConstituencyId = progOfficer.ConstituencyId.Value;
                    enumerator.PasswordChangeDate = null;
                    enumerator.CreatedOn = DateTime.Now;
                    enumerator.CreatedBy = int.Parse(User.Identity.GetUserId());
                    enumerator.PasswordHash = EasyMD5.Hash(password);
                    enumerator.IsActive = true;
                    GenericService.Create(enumerator);

                    var selectedLocs = userViewModel.Locations.Where(x => x.IsSelected == true).ToList();
                    var locs = new List<EnumeratorLocation>();
                    if (selectedLocs.Any())
                    {
                        foreach (var item in selectedLocs)
                        {
                            locs.Add(new EnumeratorLocation {
                                EnumeratorId = enumerator.Id,
                                LocationId = item.Id,
                                IsActive = true
                            });
                        }

                        if (locs.Any())
                        {
                            GenericService.AddRange(locs);
                        }
                    }
                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{userId}",
                        Key1 = enumerator.Id,
                        TableName = "Enumerator",
                        ModuleRightCode = "ENUMERATORS:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(enumerator)}",
                        WasSuccessful = true,
                        Description = "Create Enumerator Success"
                    };
                    LogService.AuditTrail(auditTrailVm);
                    await EmailService.SendAsync(
                        new ConfirmPinEmail {
                            FirstName = enumerator.FirstName,
                            To = enumerator.Email,
                            Pin = password,
                            NationalIdNo = enumerator.NationalIdNo,
                            PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                            Subject = " Your Secure PIN",
                            Title = "Welcome to " + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                        }).ConfigureAwait(true);
                }
                else
                {
                    TempData["MESSAGE"] = "Only Users with Assigned Sub Counties can Created Enumerators";
                    TempData["KEY"] = "danger";
                }
                return RedirectToAction("Index");
            }

            List<Location> locations = null;
            if (progOfficer != null)
            {
                object constituencyId;
                if (progOfficer.ConstituencyId.HasValue)
                {
                    constituencyId = progOfficer.ConstituencyId;
                }
                else
                {
                    constituencyId = DBNull.Value;
                }

                object countyId;
                if (progOfficer.CountyId.HasValue)
                {
                    countyId = progOfficer.CountyId;
                }
                else
                {
                    countyId = DBNull.Value;
                }

                var spName = "GetProgOfficerLocations";
                var parameterNames = "@CountyId,@ConstituencyId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                };
                locations = GenericService.GetManyBySp<Location>(spName, parameterNames, parameterList).ToList();

                var EnLocations = new List<EnLocation>();
                foreach (var loc in locations)
                {
                    EnLocations.Add(new EnLocation() {
                        Id = loc.Id,
                        Display = loc.Name,
                        IsSelected = userViewModel.Locations.Exists(x => x.Id == loc.Id)
                    });
                }

                userViewModel.Locations = EnLocations;
            }

            return View(userViewModel);
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:DEACTIVATION")]
        public ActionResult Deactivate(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (userId <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var newId = EnumeratorService.DeactivateEnumerator(id, userId);
            return RedirectToAction("Details", new { id = newId });
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:VIEW")]
        public async Task<ActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var enumerator = await GenericService.GetOneAsync<Enumerator>(x => x.Id == id, "EnumeratorLocations.Location")
                           .ConfigureAwait(true);

            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == enumerator.DeactivatedBy)
                .ConfigureAwait(true);

            ViewBag.DeactivatedByName = $"{user.FirstName} {user.MiddleName} {user.Surname}";
            return View(enumerator);
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var enumerator = await GenericService.GetOneAsync<Enumerator>(x => x.Id == id, "EnumeratorLocations")
                .ConfigureAwait(true);

            if (enumerator.Email == User.Identity.GetUserName())
            {
                TempData["MESSAGE"] = "You cannot edit your own account!";
                TempData["KEY"] = "success";
                return this.RedirectToAction("Index");
            }

            var userId = int.Parse(User.Identity.GetUserId());
            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(x => x.UserId == userId);

            var model = new EditEnumeratorViewModel();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(enumerator, model);

            List<Location> locations = null;
            if (progOfficer != null)
            {
                object constituencyId;
                if (progOfficer.ConstituencyId.HasValue)
                {
                    constituencyId = progOfficer.ConstituencyId;
                }
                else
                {
                    constituencyId = DBNull.Value;
                }

                object countyId;
                if (progOfficer.CountyId.HasValue)
                {
                    countyId = progOfficer.CountyId;
                }
                else
                {
                    countyId = DBNull.Value;
                }

                var spName = "GetProgOfficerLocations";
                var parameterNames = "@CountyId,@ConstituencyId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                };
                locations = GenericService.GetManyBySp<Location>(spName, parameterNames, parameterList).ToList();


                var EnLocations = new List<EnLocation>();
                foreach (var loc in locations)
                {
                    EnLocations.Add(new EnLocation() {
                        Id = loc.Id,
                        Constituency = db.Constituencies.Find(constituencyId).Name,
                        Display = loc.Name,
                        IsSelected = enumerator.EnumeratorLocations.Any(x => x.LocationId == loc.Id)
                    });
                };



                model.Locations = EnLocations;
            }
            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditEnumeratorViewModel editEnumerator)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var enumerator = await GenericService.GetOneAsync<Enumerator>(x => x.Id == editEnumerator.Id)
                    .ConfigureAwait(true);

                if (enumerator == null)
                {
                    return HttpNotFound();
                }

                new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(editEnumerator, enumerator);
                GenericService.Update(enumerator);

                var enumeratorLocat = await GenericService.GetAsync<EnumeratorLocation>(x => x.EnumeratorId == editEnumerator.Id).ConfigureAwait(true);
                GenericService.RemoveRange(enumeratorLocat);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = enumerator.Id,
                    TableName = "Enumerator",
                    ModuleRightCode = "ENUMERATORS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(enumerator)}",
                    WasSuccessful = true,
                    Description = "Create Enumerator Success"
                };
                LogService.AuditTrail(auditTrailVm);

                var selectedLocs = editEnumerator.Locations.Where(x => x.IsSelected).Select(x => x.Id).ToList();
                var locs = new List<EnumeratorLocation>();
                if (!selectedLocs.Any())
                {
                    return RedirectToAction(nameof(Index));
                }

                foreach (var item in selectedLocs)
                {
                    locs.Add(new EnumeratorLocation {
                        EnumeratorId = enumerator.Id,
                        LocationId = item,
                        IsActive = true
                    });
                }

                if (locs.Any())
                {
                    GenericService.AddRange(locs);
                }

                return RedirectToAction(nameof(Index));
            }

            ModelState.AddModelError(string.Empty, "Something failed.");
            return View();
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public async Task<ActionResult> AddLocation(int? id)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(x => x.UserId == userId);
            List<Location> locations = null;
            if (progOfficer != null)
            {
                if (progOfficer.ConstituencyId == null)
                {
                    locations = (await GenericService.GetAsync<Location>(x =>
                        x.Division.CountyDistrict.CountyId == progOfficer.CountyId)).ToList();
                }
                else
                {
                    locations = (await GenericService.GetAsync<Location>(x => x.Division.CountyDistrict.County.Constituencies.Count(v => v.CountyId == progOfficer.CountyId) > 0)).ToList();
                }

                var EnLocations = new List<EnLocation>();
                foreach (var loc in locations)
                {
                    EnLocations.Add(new EnLocation() {
                        Id = loc.Id,
                        Display = loc.Name,
                        IsSelected = false
                    });
                }

                var model = new EnumeratorAddLoc {
                    Locations = EnLocations
                };
            }

            return View();
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        [HttpPost]
        public async Task<ActionResult> AddLocation(EnumeratorAddLoc model)
        {
            var selectedLocs = model.Locations.Where(x => x.IsSelected).Select(x => x.Id).ToList();
            if (selectedLocs.Any())
            {
                List<EnumeratorLocation> locs = new List<EnumeratorLocation>();

                foreach (var item in selectedLocs)
                {
                    EnumeratorLocation loc = new EnumeratorLocation {
                        EnumeratorId = model.EnumeratorId,
                        LocationId = item,
                        IsActive = true
                    };
                    locs.Add(loc);
                }

                if (locs.Any())
                {
                    GenericService.AddRange(locs);

                    var userId = int.Parse(User.Identity.GetUserId());
                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{userId}",
                        Key1 = model.EnumeratorId,
                        TableName = "Enumerator",
                        ModuleRightCode = "ENUMERATORS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(locs)}",
                        WasSuccessful = true,
                        Description = "Create Enumerator Success"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    TempData["MESSAGE"] = "Locations added Successfully";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Details", new { id = model.EnumeratorId });
                }
            }
            TempData["MESSAGE"] = "No Locations that were added, Try Again";
            TempData["KEY"] = "info";

            return View(model);
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:VIEW")]
        public async Task<ActionResult> Index(int? page, string enumeratorNo, string nationalIdNo, string name, string email, string phoneNumber)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            Expression<Func<Enumerator, bool>> filter = x => x.FirstName != null;
            Expression<Func<Enumerator, bool>> filterx = null;

            if (!string.IsNullOrEmpty(name))
            {
                filterx = x =>
                    (x.FirstName.Contains(name) || x.MiddleName.Contains(name) ||
                     x.Surname.Contains(name));

                filter = filter.And(filterx);
            }

            if (nationalIdNo != null)
            {
                filterx = x => x.NationalIdNo.Contains(nationalIdNo);
                filter = filter.And(filterx);
            }

            if (email != null)
            {
                filterx = x => x.Email.Contains(email);
                filter = filter.And(filterx);
            }
            if (phoneNumber != null)
            {
                filterx = x => x.MobileNo.Contains(phoneNumber);
                filter = filter.And(filterx);
            }

            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(x => x.UserId == userId);
            List<int> constituencies = null;
            if (progOfficer != null)
            {
                if (progOfficer.ConstituencyId == null)
                {
                    constituencies = (await GenericService.GetAsync<Constituency>(x =>
                        x.CountyId == progOfficer.CountyId)).Select(x => x.Id).ToList();
                }
                else
                {
                    constituencies = (await GenericService.GetAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId))
                        .Select(x => x.Id).ToList();
                }

                filterx = x => constituencies.Contains(x.ConstituencyId);
                filter = filter.And(filterx);
            }
            else
            {
                TempData["MESSAGE"] = "It seems that you have not been allocated any County or Sub County.";
                TempData["KEY"] = "info";
            }

            var users = (await GenericService.GetAsync(filter, x => x.OrderBy(z => z.NationalIdNo), "EnumeratorLocations")
                    .ConfigureAwait(true)).ToPagedList(
                    page ?? 1,
                    int.Parse(WebConfigurationManager.AppSettings["USERS_PAGESIZE"]));

            var model = new EnumeratorsListViewModel {
                Enumerators = users,
                EnumeratorNo = enumeratorNo,
                Email = email,
                PhoneNumber = phoneNumber,
                Name = name,
                NationalIdNo = nationalIdNo
            };
            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public ActionResult Lockout(int id)
        {
            var newId = EnumeratorService.LockEnumerator(id);
            return RedirectToAction("Details", new { id = newId });
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:ENTRY")]
        public ActionResult ResendActivationEmail(int id)
        {
            var db = new ApplicationDbContext();
            var user = db.Enumerators.Find(id);
            var model = new ResendActivationLinkViewModel {
                Email = user.Email,
                FullName = user.FullName,
                PhoneNumber = user.MobileNo,
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "ENUMERATORS:ENTRY")]
        [HttpPost]
        public async Task<ActionResult> ResendActivationEmail(int id, ResendActivationLinkViewModel model)
        {
            try
            {
                var password = EnumeratorService.GeneratePin(4);
                var db = new ApplicationDbContext();
                var enumerator = db.Enumerators.Find(id);
                var hashedPassword = EasyMD5.Hash(password);

                LogService logger = new LogService(GenericService);

                if (enumerator != null)
                {
                    // logger.FileLog("ENUMERATOR_PIN", "PIN : " + password + " \n Password: " +
                    // enumerator.Email + " \n National ID : " + enumerator.NationalIdNo, enumerator.ToString());

                    enumerator.PasswordChangeDate = DateTime.Now;
                    enumerator.PasswordHash = hashedPassword;
                    enumerator.PasswordChangeDate = DateTime.Now.AddDays(-90);
                    GenericService.Update(enumerator);

                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = enumerator.Id,
                        TableName = "Enumerator",
                        ModuleRightCode = "ENUMERATORS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(enumerator)}",
                        WasSuccessful = true,
                        Description = "Enumerator ResendActivationEmail"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    new Thread(() =>
                      EmailService.SendAsync(
                        new ConfirmPinEmail {
                            FirstName = enumerator.FirstName,
                            To = enumerator.Email,
                            NationalIdNo = enumerator.NationalIdNo,
                            Pin = password,
                            PortalName = WebConfigurationManager.AppSettings["SYSTEM_NAME"],

                            Subject = "Welcome and Confirm your account",
                            Title = "Welcome to " + WebConfigurationManager.AppSettings["SYSTEM_NAME"],
                        })
                        ).Start();
                }

                TempData["MESSAGE"] = "The Pin Email has been sent Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                var loger = new LogService(GenericService);
                loger.FileLog(MisKeys.ERROR, e.StackTrace, User.Identity.GetUserId());
                TempData["MESSAGE"] = "The PIN Email has not been sent.";
                TempData["KEY"] = "danger";
            }
            return View(model);
        }

        [HttpPost]
        [GroupCustomAuthorize(Name = "ENUMERATORS:MODIFIER")]
        public ActionResult Unlock(int id)
        {
            var newId = EnumeratorService.UnLockEnumerator(id);
            return RedirectToAction("Details", new { id = newId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}