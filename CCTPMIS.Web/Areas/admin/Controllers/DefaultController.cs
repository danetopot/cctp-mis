﻿using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System.Web.Mvc;

    [Authorize]
    public class DefaultController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public DefaultController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Default
        public ActionResult Index()
        {
            //var callbackUrl = Url.Action(
            //    "ConfirmEmail",
            //    "Account",
            //    new { @area = string.Empty, userId = 1, code = "111" },
            //    protocol: Request.Url.Scheme);
            return View();
        }
    }
}