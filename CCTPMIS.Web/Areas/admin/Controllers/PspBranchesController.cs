﻿namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using PagedList;
    using Services;

    [Authorize]
    public class PspBranchesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public PspBranchesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/PspBranches/Create
        //[GroupCustomAuthorize(Name = "PSPBRANCHES:ENTRY")]
        //public async Task<ActionResult> Create()
        //{
        //    ViewBag.PspId = new SelectList(await GenericService.GetAsync<Psp>().ConfigureAwait(true), "Id", "Name");
        //    ViewBag.SubLocationId = new SelectList(
        //        await GenericService.GetAsync<SubLocation>().ConfigureAwait(true),
        //        "Id",
        //        "Name");

        //    return View();
        //}

        // POST: admin/PspBranches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[GroupCustomAuthorize(Name = "PSPBRANCHES:ENTRY")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create(
        //    [Bind(Include = "Id,PspId,Code,Name,IsActive,Longitude,Latitude,SubLocationId")]
        //    PspBranch pspBranch)
        //{
        //    pspBranch.CreatedBy = int.Parse(User.Identity.GetUserId());
        //    pspBranch.CreatedOn = DateTime.Now;

        // if (ModelState.IsValid) { GenericService.Create(pspBranch); return
        // RedirectToAction("Index"); }

        // ViewBag.PspId = new SelectList( await GenericService.GetAsync<Psp>().ConfigureAwait(true),
        // "Id", "Name", pspBranch.PspId); ViewBag.SubLocationId = new SelectList( await
        // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name", pspBranch.SubLocationId);

        //    return View(pspBranch);
        //}

        //// GET: admin/PspBranches/Delete/5
        //[GroupCustomAuthorize(Name = "PSPBRANCHES:DELETION")]
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var pspBranch = await GenericService.GetOneAsync<PspBranch>( x => x.Id == id.Value,
        // "CreatedByUser,ModifiedByUser,Psp"); if (pspBranch == null) { return HttpNotFound(); }

        //    return View(pspBranch);
        //}

        // POST: admin/PspBranches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PSPBRANCHES:DELETION")]
        public async Task<ActionResult> DeleteConfirmed(short id)
        {
            var pspBranch = await GenericService.GetOneAsync<PspBranch>(x => x.Id == id);
            GenericService.Delete(pspBranch);
            return RedirectToAction("Index");
        }

        // GET: admin/PspBranches/Details/5
        [GroupCustomAuthorize(Name = "PSPBRANCHES:VIEW")]
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var pspBranch = await GenericService.GetOneAsync<PspBranch>(
                                x => x.Id == id.Value,
                                "CreatedByUser,ModifiedByUser,Psp,SubLocation").ConfigureAwait(true);
            if (pspBranch == null)
            {
                return HttpNotFound();
            }

            return View(pspBranch);
        }

        // GET: admin/PspBranches/Edit/5
        //[GroupCustomAuthorize(Name = "PSPBRANCHES:MODIFIER")]
        //public async Task<ActionResult> Edit(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var pspBranch = await GenericService.GetOneAsync<PspBranch>(x => x.Id == id.Value);

        // if (pspBranch == null) { return HttpNotFound(); }

        // ViewBag.PspId = new SelectList( await GenericService.GetAsync<Psp>().ConfigureAwait(true),
        // "Id", "Name", pspBranch.PspId); ViewBag.SubLocationId = new SelectList( await
        // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name", pspBranch.SubLocationId);

        //    return View(pspBranch);
        //}

        // POST: admin/PspBranches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "PSPBRANCHES:MODIFIER")]
        //public async Task<ActionResult> Edit(
        //    [Bind(
        //        Include =
        //            "Id,PspId,Code,Name,IsActive,Longitude,Latitude,SubLocationId,ModifiedOn,ModifiedBy,CreatedOn,CreatedBy")]
        //    PspBranch pspBranch)
        //{
        //    pspBranch.ModifiedBy = int.Parse(User.Identity.GetUserId());
        //    pspBranch.ModifiedOn = DateTime.Now;

        // if (ModelState.IsValid) { GenericService.Update(pspBranch); return
        // RedirectToAction("PspBranches", "Psps", new { id = pspBranch.PspId }); }

        // ViewBag.PspId = new SelectList( await GenericService.GetAsync<Psp>().ConfigureAwait(true),
        // "Id", "Name", pspBranch.PspId); ViewBag.SubLocationId = new SelectList( await
        // GenericService.GetAsync<SubLocation>().ConfigureAwait(true), "Id", "Name", pspBranch.SubLocationId);

        //    return View(pspBranch);
        //}

        [GroupCustomAuthorize(Name = "PSPBRANCHES:VIEW")]
        public async Task<ActionResult> Index(int? page, int? pspId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var psps = pspId.HasValue
                           ? (await GenericService.GetAsync<PspBranch>(
                                  x => x.PspId == pspId,
                                  x => x.OrderBy(y => y.Code),
                                  "CreatedByUser,ModifiedByUser,Psp,SubLocation").ConfigureAwait(true))
                           .ToPagedList(pageNum, pageSize)
                           : (await GenericService.GetAsync<PspBranch>(
                                  null,
                                  x => x.OrderBy(y => y.Code),
                                  "CreatedByUser,ModifiedByUser,Psp,SubLocation").ConfigureAwait(true))
                           .ToPagedList(pageNum, pageSize);

            ViewBag.PspId = new SelectList(
                await GenericService.GetAsync<Psp>().ConfigureAwait(true),
                "Id",
                "Name",
                pspId);

            return View(psps);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}