﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Users;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    public class ProgrammeOfficersController : Controller
    {
        public string eligibleOfficersGroup = "SCC,CC,";
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public ProgrammeOfficersController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:VIEW")]
        public async Task<ActionResult> Index(int? page, string name, int? county, int? constituency)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<ProgrammeOfficer> officers = null;

            Expression<Func<ProgrammeOfficer, bool>> filter = x => eligibleOfficersGroup.Contains(x.User.UserGroup.Name);
            Expression<Func<ProgrammeOfficer, bool>> filter2 = null;
            Expression<Func<ProgrammeOfficer, bool>> filter3 = null;
            Expression<Func<ProgrammeOfficer, bool>> filter4 = null;

            if (!string.IsNullOrEmpty(name))
            {
                filter2 = x =>
                    (x.User.FirstName.Contains(name) || x.User.MiddleName.Contains(name) ||
                     x.User.Surname.Contains(name));
                filter = filter.And(filter2);
            }

            if (county != null)
            {
                filter3 = x => x.CountyId == county;

                filter = filter.And(filter3);
            }
            if (constituency != null)
            {
                filter4 = x => x.ConstituencyId == constituency;
                filter = filter.And(filter4);
            }

            officers = (await GenericService.GetAsync(
              filter,
                x => x.OrderBy(y => y.User.FirstName),
                "Constituency,County,CreatedByUser,ModifiedByUser,ApvByUser,DeactivatedByUser,DeactivatedApvByUser,User,Status"
            ).ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            var model = new ProgrammeOfficersListViewModel {
                Constituency = constituency,
                County = county,
                Name = name,
                Page = page,
                Officers = officers
            };

            ViewBag.County = new SelectList(
                await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id",
                "Name");

            ViewBag.Constituency = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == 0).ConfigureAwait(true),
                "Id",
                "Name");

            return View(model);
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var officers = await GenericService.GetAsync<ApplicationUser>(filter: x => eligibleOfficersGroup.Contains(x.UserGroup.Name));
            ViewBag.UserId = new SelectList(officers, "Id", "FullName");

            ViewBag.CountyId = new SelectList(
                await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id",
                "Name");

            ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == 0).ConfigureAwait(true),
                "Id",
                "Name");

            return View();
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:ENTRY")]
        // POST: admin/Counties/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProgrammeOfficer officer)
        {
            try
            {
                dynamic constituencyId;
                if (officer.ConstituencyId.HasValue)
                {
                    constituencyId = officer.ConstituencyId;
                }
                else
                {
                    constituencyId = DBNull.Value;
                }

                var userId = int.Parse(User.Identity.GetUserId());
                var parameterNames = "@Id,@ProgrammeOfficerUserId,@CountyId,@ConstituencyId,@UserId";
                var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity  { ParameterTuple =  new Tuple<string, object>("Id", DBNull.Value)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("ProgrammeOfficerUserId", officer.UserId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("CountyId", officer.CountyId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("ConstituencyId", constituencyId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", userId)},
            };

                if (ModelState.IsValid)
                {
                    var newModel = GenericService.GetOneBySp<SpIntVm>("AddEditProgrammeOfficer", parameterNames, parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = officer.Id,
                        TableName = "ProgrammeOfficer",
                        ModuleRightCode = "GEOUNITS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(officer)}",
                        WasSuccessful = true,
                        Description = "Programme Officer Added Success"
                    });

                    TempData["MESSAGE"] = "Programme Officer was created Successfully";
                    TempData["KEY"] = "success";
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Programme Officer was not created " + e.Message;
                TempData["KEY"] = "danger";
            }
            var officers =
                await GenericService.GetAsync<ApplicationUser>(filter: x =>
                    eligibleOfficersGroup.Contains(x.UserGroup.Name));

            ViewBag.UserId = new SelectList(officers, "Id", "FullName", officer.UserId);
            ViewBag.CountyId = new SelectList(
                await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id",
                "Name", officer.CountyId);

            ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == officer.CountyId).ConfigureAwait(true),
                "Id",
                "Name", officer.ConstituencyId);

            return View(officer);
        }

        #region Edit

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:VIEW")]
        public async Task<ActionResult> Details(int id)
        {
            var officer = (await GenericService.GetOneAsync<ProgrammeOfficer>(x => x.Id == id, "User,County,Constituency"));

            return View(officer);
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:MODIFIER")]
        public async Task<ActionResult> Edit(int id)
        {
            var officer = (await GenericService.GetOneAsync<ProgrammeOfficer>(x => x.Id == id, "User,County,Constituency"));

            ViewBag.CountyId = new SelectList(
                await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id",
                "Name", officer.CountyId);

            ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == officer.CountyId).ConfigureAwait(true),
                "Id",
                "Name", officer.ConstituencyId);

            return View(officer);
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:MODIFIER")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProgrammeOfficer officer)
        {
            dynamic constituencyId;
            if (officer.ConstituencyId.HasValue)
            {
                constituencyId = officer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@Id,@ProgrammeOfficerUserId,@CountyId,@ConstituencyId,@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity  { ParameterTuple =  new Tuple<string, object>("Id", officer.Id)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("ProgrammeOfficerUserId", officer.UserId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("CountyId", officer.CountyId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("ConstituencyId", constituencyId)},
                new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", userId)},
            };

            try
            {
                if (ModelState.IsValid)
                {
                    var newModel = GenericService.GetOneBySp<SpIntVm>("AddEditProgrammeOfficer", parameterNames, parameterList);

                    TempData["MESSAGE"] = "Programme Officer was created Successfully";
                    TempData["KEY"] = "success";
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Programme Officer was not Updated " + e.Message;
                TempData["KEY"] = "danger";
            }
            var officers =
                await GenericService.GetAsync<ApplicationUser>(filter: x =>
                    eligibleOfficersGroup.Contains(x.UserGroup.Name));

            ViewBag.UserId = new SelectList(officers, "Id", "FullName", officer.UserId);
            ViewBag.CountyId = new SelectList(
                await GenericService.GetAsync<County>().ConfigureAwait(true),
                "Id",
                "Name", officer.CountyId);

            ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == officer.CountyId).ConfigureAwait(true),
                "Id",
                "Name", officer.ConstituencyId);

            return View(officer);
        }

        #endregion Edit

        #region Approve

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:APPROVAL")]
        public ActionResult Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(
                x => x.Id == id,
                "User,CreatedByUser,ModifiedByUser,Status,County,Constituency");

            if (progOfficer == null)
            {
                return HttpNotFound();
            }

            return View(progOfficer);
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:APPROVAL")]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Approve(IdClass model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>("ApproveProgrammeOfficer", parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "ProgrammeOfficer",
                    ModuleRightCode = "PROGRAMME-OFFICERS:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Programme Officer Added Success"
                });

                TempData["MESSAGE"] = $" The  Programme Officer has been Approved Successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Programme Officer was not Approved " + e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        #endregion Approve

        #region Deactivate

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:DEACTIVATION")]
        public ActionResult Deactivate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(
                x => x.Id == id,
                "User,CreatedByUser,ModifiedByUser,Status,County,Constituency");

            if (progOfficer == null)
            {
                return HttpNotFound();
            }

            return View(progOfficer);
        }

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:DEACTIVATION")]
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Deactivate(IdClass model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };
            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>("DeactivateProgrammeOfficer", parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "ProgrammeOfficer",
                    ModuleRightCode = "PROGRAMME-OFFICERS:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Programme Officer Deactivated Success"
                });

                TempData["MESSAGE"] = $" The  Programme Officer has been Deactivated Successfully. This Needs to be Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Programme Officer was not Deactivated " + e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Deactivate

        #region Deactivate Approve

        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:APPROVAL")]
        public ActionResult DeactivateApprove(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var progOfficer = GenericService.GetOne<ProgrammeOfficer>(
                x => x.Id == id,
                "User,CreatedByUser,ModifiedByUser,Status,County,Constituency");

            if (progOfficer == null)
            {
                return HttpNotFound();
            }

            return View(progOfficer);
        }

        [HttpPost, ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PROGRAMME-OFFICERS:APPROVAL")]
        public ActionResult DeactivateApprove(IdClass model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id),
                                            },
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId),
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>("ApproveDeactivateProgrammeOfficer", parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "ProgrammeOfficer",
                    ModuleRightCode = "PROGRAMME-OFFICERS:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Programme Officer Deactivated approval Success"
                });

                TempData["MESSAGE"] = $" The  Programme Officer Deactivation has been Approved Successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Programme Officer Deactivation was not  Approved. " + e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Deactivate Approve
    }
}