﻿using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Account;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    public class GroupsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
     //   public IGroupService GroupService;

        private ApplicationRoleManager _roleManager;

        public ApplicationRoleManager RoleManager
        {
            get => _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            private set => _roleManager = value;
        }

        public GroupsController(GroupService groupService)
        {
            GroupService = groupService;
        }

        public ActionResult Index(int? page)
        {
            var pageNum = page ?? 1;
            var model = (GroupService.GetApplicationGroups().OrderBy(x => x.Name)).ToPagedList(pageNum, int.Parse(WebConfigurationManager.AppSettings["GROUPS_PAGESIZE"]));
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationgroup = await GroupService.GetApplicationGroupAsync(id);
            if (applicationgroup == null)
            {
                return HttpNotFound();
            }
            var groupRoles = GroupService.GetGroupRoles(applicationgroup.Id);
            var roleNames = groupRoles.Select(p => new ApplicationRole { Name = p.Name, Description = p.Description }).OrderBy(x => x.Name).ToList();
            ViewBag.RolesList = roleNames;
            ViewBag.RolesCount = roleNames.Count;
            return View(applicationgroup);
        }

        public ActionResult Create()
        {
            ViewBag.RolesList = new SelectList(RoleManager.Roles.ToList(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,Description,")] ApplicationGroup applicationgroup, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                // Create the new Group:
                var result = await GroupService.CreateGroupAsync(applicationgroup);
                if (!result.Succeeded) return RedirectToAction("Index");
                selectedRoles = selectedRoles ?? new string[] { };
                // Add the roles selected:
                await GroupService.SetGroupRolesAsync(applicationgroup.Id, selectedRoles);
                return RedirectToAction("Index");
            }

            // Otherwise, start over:
            ViewBag.RolesList = new SelectList(RoleManager.Roles.ToList(), "Id", "Name");
            return View(applicationgroup);
        }

        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationgroup = await GroupService.FindByIdAsync(id);
            if (applicationgroup == null)
            {
                return HttpNotFound();
            }

            // Get a list, not a DbSet or queryable:
            var allRoles = await RoleManager.Roles.ToListAsync();
            var groupRoles = await GroupService.GetGroupRolesAsync(id);

            var model = new GroupViewModel()
            {
                Id = applicationgroup.Id,
                Name = applicationgroup.Name,
                Description = applicationgroup.Description
            };

            // load the roles/Roles for selection in the form:
            foreach (var role in allRoles)
            {
                var enumerable = groupRoles.ToList();
                var applicationRoles = groupRoles as ApplicationRole[] ?? enumerable.Distinct().ToArray();
                var listItem = new SelectListItem()
                {
                    Text = role.Name,
                    Value = role.Name,
                    Selected = applicationRoles.Any(g => g.Name == role.Name)
                };

                model.RolesList.Add(listItem);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description")] GroupViewModel model, params string[] selectedRoles)
        {
            var group = await GroupService.FindByIdAsync(model.Id);
            if (group == null)
            {
                return HttpNotFound();
            }
            if (!ModelState.IsValid) return View(model);
            group.Name = model.Name;
            group.Description = model.Description;
            await GroupService.UpdateGroupAsync(group);
            selectedRoles = selectedRoles ?? new string[] { };
            await GroupService.SetGroupRolesAsync(group.Id, selectedRoles);
            return RedirectToAction("Index");

            //if model is invalid
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();

                if (_roleManager != null)
                {
                    _roleManager.Dispose();
                    _roleManager = null;
                }
            }
            base.Dispose(disposing);
            db.Dispose();
            db.Dispose();
        }
    }
}