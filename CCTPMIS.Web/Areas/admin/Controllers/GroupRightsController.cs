﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class GroupRightsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public GroupRightsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/UserGroups
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var userGroups =
                (await GenericService.GetAsync<UserGroup>(null, x => x.OrderBy(y => y.Name), "UserGroupProfile")
                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(userGroups);
        }

        // GET: admin/UserGroups/Edit/5
        public async Task<ActionResult> Rights(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserGroup userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == id).ConfigureAwait(true);

            var moduleRightIdsList = new List<SelectListItem>();

            var allModuleRights = await GenericService.GetAsync<ModuleRight>().ConfigureAwait(true);

            var assignedModuleRights =
                (await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id).ConfigureAwait(true)).ToList();

            foreach (var moduleRight in allModuleRights)
            {
                var listItem = new SelectListItem() {
                    Text = moduleRight.Description,
                    Value = moduleRight.Id.ToString(),
                    Selected = assignedModuleRights.Any(
                                           g => g.ModuleRightId == moduleRight.Id)
                };

                moduleRightIdsList.Add(listItem);
            }

            if (userGroup == null)
            {
                return HttpNotFound();
            }

            ViewBag.ModuleRightId = moduleRightIdsList;

            return View(model: userGroup);
        }

        // POST: admin/UserGroups/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Rights(int id, params byte[] selectedModuleId)
        {
            if (ModelState.IsValid)
            {
                // delete all the entries
                var curreGroupRights =
                    await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id).ConfigureAwait(true);
                var userId = int.Parse(User.Identity.GetUserId());
                GenericService.RemoveRange(curreGroupRights);

                var groupRights = new List<GroupRight>();
                if (selectedModuleId != null)
                {
                    foreach (var module in selectedModuleId)
                    {
                        var groupRight = new GroupRight {
                            UserGroupId = id,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            ModuleRightId = module
                        };
                        groupRights.Add(groupRight);
                    }
                }

                GenericService.AddRange(groupRights);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = id,
                    TableName = "GroupRight",
                    ModuleRightCode = "USER GROUP:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(groupRights)}",
                    WasSuccessful = true,
                    Description = "Group Rights Modified Success"
                });

                return RedirectToAction("Index");
            }

            var moduleRightIdsList = new List<SelectListItem>();

            var allModuleRights = await GenericService.GetAsync<ModuleRight>().ConfigureAwait(true);

            var assignedModuleRights =
                (await GenericService.GetAsync<GroupRight>(x => x.UserGroupId == id).ConfigureAwait(true)).ToList();

            foreach (var moduleRight in allModuleRights)
            {
                var listItem = new SelectListItem() {
                    Text = moduleRight.Description,
                    Value = moduleRight.Id.ToString(),
                    Selected = assignedModuleRights.Any(
                                           g => g.ModuleRightId == moduleRight.Id)
                };

                moduleRightIdsList.Add(listItem);
            }

            ViewBag.ModuleRightId = moduleRightIdsList;
            var userGroup = await GenericService.GetOneAsync<UserGroup>(x => x.Id == id).ConfigureAwait(true);

            return View(model: userGroup);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}