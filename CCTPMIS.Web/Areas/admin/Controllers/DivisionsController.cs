﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class DivisionsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public DivisionsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        // GET: admin/Divisions/Create
        public ActionResult Create()
        {
            ViewBag.CountyDistrictId = new SelectList(GenericService.Get<CountyDistrict>(), "Id", "Name");
            return View();
        }

        // POST: admin/Divisions/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create(
            [Bind(Include = "Id,Name,Code,DistrictId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Division division)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            division.CreatedBy = userId;
            division.CreatedBy = int.Parse(User.Identity.GetUserId());
            division.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(division);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = division.Id,
                    TableName = "Division",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(division)}",
                    WasSuccessful = true,
                    Description = "District Create Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction("Index");
            }

            ViewBag.CountyDistrictId = new SelectList(
                GenericService.Get<CountyDistrict>(),
                "Id",
                "Name",
                division.CountyDistrictId);
            return View(division);
        }

        /*
        // GET: admin/Divisions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var division = GenericService.GetOne<Division>(
                x => x.Id == id.Value,
                "District.GeoMaster,CreatedByUser,ModifiedByUser");

            if (division == null)
            {
                return HttpNotFound();
            }

            return View(division);
        }

        // POST: admin/Divisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var division = GenericService.GetOne<Division>(x => x.Id == id);
            GenericService.Delete(division);

            return RedirectToAction("Index");
        }
        */

        // GET: admin/Divisions/Details/5
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var division = GenericService.GetOne<Division>(
                x => x.Id == id.Value,
                "CountyDistrict.District.GeoMaster,CountyDistrict.County.GeoMaster");
            if (division == null)
            {
                return HttpNotFound();
            }

            return View(division);
        }

        /*
        [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
        // GET: admin/Divisions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var division = GenericService.GetOne<Division>(x => x.Id == id.Value, string.Empty);

            if (division == null)
            {
                return HttpNotFound();
            }

            ViewBag.CountyDistrictId = new SelectList(
                GenericService.Get<CountyDistrict>(),
                "Id",
                "Name",
                division.CountyDistrictId);
            return View(division);
        }

        // POST: admin/Divisions/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:MODIFIER")]
        public ActionResult Edit(
            [Bind(Include = "Id,Name,Code,DistrictId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Division division)
        {
            division.ModifiedBy = int.Parse(User.Identity.GetUserId());
            division.ModifiedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Update(division);
                return RedirectToAction("Index");
            }

            ViewBag.CountyDistrictId = new SelectList(
                GenericService.Get<CountyDistrict>(),
                "Id",
                "Name",
                division.CountyDistrictId);

            return View(division);
        }
        */

        // GET: admin/Divisions
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public async Task<ActionResult> Index(int? page, int? cdId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<Division> divisions = null;
            divisions = cdId.HasValue
                            ? (await GenericService.GetAsync<Division>(
                                       x => x.CountyDistrictId == cdId,
                                       x => x.OrderBy(y => y.Code),
                                       "CreatedByUser,ModifiedByUser,CountyDistrict.District.GeoMaster,CountyDistrict.County.GeoMaster")
                                   .ConfigureAwait(true)).ToPagedList(pageNum, pageSize)
                            : (await GenericService.GetAsync<Division>(
                                       null,
                                       x => x.OrderBy(y => y.Code),
                                       "CreatedByUser,ModifiedByUser,CountyDistrict.District.GeoMaster,CountyDistrict.County.GeoMaster")
                                   .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            ViewBag.CdId = new SelectList(
                await GenericService.GetAsync<CountyDistrict>(null, null, "District,County").ConfigureAwait(true),
                "Id",
                "District.Name");
            return View(divisions);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}