﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
    public class WardsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public WardsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Wards/Create
        public ActionResult Create()
        {
            ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name");
            return View();
        }

        // POST: admin/Wards/Create To protect from overposting attacks, please enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "Id,Code,Name,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Ward ward)
        {
            ward.CreatedBy = int.Parse(User.Identity.GetUserId());
            ward.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(ward);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = ward.Id,
                    TableName = "Ward",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(ward)}",
                    WasSuccessful = true,
                    Description = "Ward Added Successfully"
                });

                return RedirectToAction("Index");
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                ward.ConstituencyId);
            return View(ward);
        }

        //// GET: admin/Wards/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // Ward ward = await GenericService.GetOneAsync<Ward>( x => x.Id == id, "CreatedByUser,ModifiedByUser,Constituency.County.GeoMaster");

        // if (ward == null) { return HttpNotFound(); }

        //    return View(ward);
        //}

        //// POST: admin/Wards/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Ward ward = GenericService.GetOne<Ward>(x => x.Id == id);
        //    GenericService.Delete(ward);
        //    return RedirectToAction("Index");
        //}

        // GET: admin/Wards/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Ward ward = await GenericService.GetOneAsync<Ward>(
                            x => x.Id == id,
                            "CreatedByUser,ModifiedByUser,Constituency.County.GeoMaster");
            if (ward == null)
            {
                return HttpNotFound();
            }

            return View(ward);
        }

        // GET: admin/Wards/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Ward ward = await GenericService.GetOneAsync<Ward>(x => x.Id == id);
            if (ward == null)
            {
                return HttpNotFound();
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                ward.ConstituencyId);
            return View(ward);
        }

        // POST: admin/Wards/Edit/5 To protect from overposting attacks, please enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Code,Name,ConstituencyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Ward ward)
        {
            ward.ModifiedBy = int.Parse(User.Identity.GetUserId());
            ward.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(ward);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = ward.Id,
                    TableName = "Ward",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(ward)}",
                    WasSuccessful = true,
                    Description = "Ward Modified Successfully"
                });

                return RedirectToAction("Index");
            }

            ViewBag.ConstituencyId = new SelectList(
                GenericService.Get<Constituency>(),
                "Id",
                "Name",
                ward.ConstituencyId);
            return View(ward);
        }

        // GET: admin/Locations
        public async Task<ActionResult> Index(int? page, int? constituencyId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var wards = constituencyId.HasValue
                            ? (await GenericService.GetAsync<Ward>(
                                   x => x.ConstituencyId == constituencyId,
                                   x => x.OrderBy(y => y.Code),
                                   "CreatedByUser,ModifiedByUser,Constituency.County.GeoMaster"))
                            .ToPagedList(pageNum, pageSize)
                            : (await GenericService.GetAsync<Ward>(
                                   null,
                                   x => x.OrderBy(y => y.Code),
                                   "CreatedByUser,ModifiedByUser,Constituency.County.GeoMaster"))
                            .ToPagedList(pageNum, pageSize);
            ViewBag.ConstituencyId = new SelectList(GenericService.GetAll<Constituency>(), "Id", "Name");
            return View(wards);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}