﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class ProgrammesController : Controller
    {
        // private ApplicationDbContext db = new ApplicationDbContext();
        protected readonly IGenericService GenericService;

        private readonly ILogService LogService;

        public ProgrammesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Programmes/Create
        public async Task<ActionResult> Create()
        {
            //var systemCodeBeneficiary = int.Parse(WebConfigurationManager.AppSettings["CODE_BENEFICIARYTYPE"]);
            //var systemCodeRecipient = int.Parse(WebConfigurationManager.AppSettings["CODE_RECIPIENTTYPE"]);
            ViewBag.BeneficiaryTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Beneficiary Type")
                    .ConfigureAwait(true),
                "Id",
                "Code");
            ViewBag.PrimaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code");
            ViewBag.SecondaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code");

            ViewBag.PaymentFrequencyId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Payment Frequency")
                    .ConfigureAwait(true),
                "Id",
                "Code");
            return View();
        }

        // POST: admin/Programmes/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Programme programme)
        {
            programme.CreatedBy = int.Parse(User.Identity.GetUserId());
            programme.CreatedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Create(programme);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = programme.Id,
                    TableName = "Programme",
                    ModuleRightCode = "PROGRAMMES:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(programme)}",
                    WasSuccessful = true,
                    Description = "Programme Added Successfully"
                });

                return RedirectToAction("Index");
            }

            //var systemCodeBeneficiary = int.Parse(WebConfigurationManager.AppSettings["CODE_BENEFICIARYTYPE"]);
            //var systemCodeRecipient = int.Parse(WebConfigurationManager.AppSettings["CODE_RECIPIENTTYPE"]);

            ViewBag.BeneficiaryTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Beneficiary Type")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.BeneficiaryTypeId);
            ViewBag.PrimaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.PrimaryRecipientId);
            ViewBag.SecondaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.SecondaryRecipientId);

            ViewBag.PaymentFrequencyId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Payment Frequency")
                    .ConfigureAwait(true),
                "Id",
                "Code", programme.PaymentFrequencyId);

            return View(programme);
        }

        //// GET: admin/Programmes/Delete/5
        //public async Task<ActionResult> Delete(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // Programme programme = await GenericService.GetOneAsync<Programme>( x => x.Id == id.Value,
        // "CreatedByUser,ModifiedByUser,BeneficiaryType,PaymentFrequency,PrimaryRecipient,SecondaryRecipient")
        // .ConfigureAwait(true); if (programme == null) { return HttpNotFound(); }

        //    return View(programme);
        //}

        //// POST: admin/Programmes/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(short id)
        //{
        //    Programme programme = await GenericService.GetOneAsync<Programme>(x => x.Id == id).ConfigureAwait(true);
        //    GenericService.Delete(programme);
        //    return RedirectToAction("Index");
        //}

        // GET: admin/Programmes/Details/5
        public async Task<ActionResult> Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Programme programme = await GenericService.GetOneAsync<Programme>(
                                          x => x.Id == id.Value,
                                          "CreatedByUser,ModifiedByUser,BeneficiaryType,PaymentFrequency,PrimaryRecipient,SecondaryRecipient")
                                      .ConfigureAwait(true);

            if (programme == null)
            {
                return HttpNotFound();
            }

            return View(programme);
        }

        // GET: admin/Programmes/Edit/5
        public async Task<ActionResult> Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Programme programme =
                await GenericService.GetOneAsync<Programme>(x => x.Id == id.Value).ConfigureAwait(true);

            if (programme == null)
            {
                return HttpNotFound();
            }

            //var systemCodeBeneficiary = int.Parse(WebConfigurationManager.AppSettings["CODE_BENEFICIARYTYPE"]);
            //var systemCodeRecipient = int.Parse(WebConfigurationManager.AppSettings["CODE_RECIPIENTTYPE"]);
            ViewBag.BeneficiaryTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Beneficiary Type")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.BeneficiaryTypeId);
            ViewBag.PrimaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.PrimaryRecipientId);
            ViewBag.SecondaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.SecondaryRecipientId);

            ViewBag.PaymentFrequencyId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Payment Frequency")
                    .ConfigureAwait(true),
                "Id",
                "Code", programme.PaymentFrequencyId);

            return View(programme);
        }

        // POST: admin/Programmes/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Programme programme)
        {
            programme.ModifiedBy = int.Parse(User.Identity.GetUserId());
            programme.ModifiedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                GenericService.Update(programme);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = programme.Id,
                    TableName = "Programme",
                    ModuleRightCode = "PROGRAMMES:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(programme)}",
                    WasSuccessful = true,
                    Description = "Programme Modified Successfully"
                });

                return RedirectToAction("Index");
            }

            ViewBag.BeneficiaryTypeId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Beneficiary Type")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.BeneficiaryTypeId);
            ViewBag.PrimaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.PrimaryRecipientId);
            ViewBag.SecondaryRecipientId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role")
                    .ConfigureAwait(true),
                "Id",
                "Code",
                programme.SecondaryRecipientId);
            return View(programme);

            ViewBag.PaymentFrequencyId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Payment Frequency")
                    .ConfigureAwait(true),
                "Id",
                "Code", programme.PaymentFrequencyId);
        }

        // GET: admin/Programmes
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            // "CreatedByUser,ModifiedByUser,BeneficiaryTypeSystemCodeDetail,PrimaryRecipientSystemCodeDetail,SecondaryRecipientSystemCodeDetail,PaymentFrequencySystemCodeDetail"
            var programmes =
                (await GenericService.GetAllAsync<Programme>(x => x.OrderBy(y => y.Name)).ConfigureAwait(true))
                .ToPagedList(pageNum, pageSize);
            return View(programmes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }

            base.Dispose(disposing);
        }
    }
}