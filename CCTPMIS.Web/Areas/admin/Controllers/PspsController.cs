﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using ExcelDataReader;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class PspsController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public PspsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        //[GroupCustomAuthorize(Name = "PSPS:UPLOAD")]
        //public async Task<ActionResult> BulkUpload(short? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var psp = await GenericService .GetOneAsync<Psp>(x => x.Id == id.Value,
        // "CreatedByUser,ModifiedByUser,PspBranches") .ConfigureAwait(true);

        // if (psp == null) { return HttpNotFound(); }

        //    return View(psp);
        //}

        // POST: admin/PspBranches/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see
        // https://go.microsoft.com/fwlink/?LinkId=317598.
        // [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1503:CurlyBracketsMustNotBeOmitted",
        // Justification = "Reviewed. Suppression is OK here.")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PSPS:UPLOAD")]
        public async Task<ActionResult> BulkUpload(byte id, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    // ExcelDataReader works with the binary Excel file, so it needs a FileStream to
                    // get started. This is how we avoid dependencies on ACE or Interop:
                    var stream = upload.InputStream;

                    // We return the interface, so that
                    IExcelDataReader reader = null;

                    if (upload.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (upload.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }

                    var result = reader.AsDataSet(
                        new ExcelDataSetConfiguration() {
                            UseColumnDataType = true,
                            ConfigureDataTable = (tableReader) =>
                                new ExcelDataTableConfiguration() {
                                    EmptyColumnNamePrefix
                                            = "Column",
                                    UseHeaderRow =
                                            true,
                                }
                        });
                    reader.Close();
                    var model = result.Tables["PSP_BRANCH"];
                    var userId = int.Parse(User.Identity.GetUserId());
                    List<PspBranch> pspBranches = new List<PspBranch>();
                    foreach (DataRow row in model.Rows)
                    {
                        var pspBranch = new PspBranch {
                            Code = row["Code"].ToString(),
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            IsActive = true,
                            ModifiedBy = null,
                            ModifiedOn = null,
                            Name = row["Name"].ToString(),
                            PspId = id
                        };

                        if (!string.IsNullOrEmpty(row["Latitude"].ToString()))
                        {
                            pspBranch.Latitude = float.Parse(row["Latitude"].ToString());
                        }

                        if (!string.IsNullOrEmpty(row["Longitude"].ToString()))
                        {
                            pspBranch.Longitude = float.Parse(row["Longitude"].ToString());
                        }

                        pspBranches.Add(pspBranch);
                    }

                    GenericService.AddRange(pspBranches);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = int.Parse(id.ToString()),
                        TableName = "Psp",
                        ModuleRightCode = "PSPS:UPLOAD",
                        Record = $"{JsonConvert.SerializeObject(pspBranches)}",
                        WasSuccessful = true,
                        Description = "Psp Branches Import Success"
                    });

                    await GenericService.SaveAsync().ConfigureAwait(true);
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }

            var psp = await GenericService.GetOneAsync<Psp>(x => x.Id == id, "CreatedByUser,ModifiedByUser,PspBranches")
                          .ConfigureAwait(true);
            return View(psp);
        }

        [GroupCustomAuthorize(Name = "PSPS:ENTRY")]

        // GET: admin/Psps/Create
        public ActionResult Create()
        {
            var userGroupId = short.Parse(WebConfigurationManager.AppSettings["PSP_USERGROUPID"]);

            ViewBag.UserId = new SelectList(
                GenericService.Get<ApplicationUser>(x => x.UserGroup.Name == "PSP"),
                "Id",
                "DisplayName");

            return View();
        }

        // POST: admin/Psps/Create To protect from overposting attacks, please enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [GroupCustomAuthorize(Name = "PSPS:ENTRY")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Psp psp)
        {
            psp.CreatedBy = int.Parse(User.Identity.GetUserId());
            psp.CreatedOn = DateTime.Now;
            psp.UserId = int.Parse(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                GenericService.Create(psp);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = psp.Id,
                    TableName = "Psp",
                    ModuleRightCode = "PSPS:UPLOAD",
                    Record = $"{JsonConvert.SerializeObject(psp)}",
                    WasSuccessful = true,
                    Description = "Psp Created Success"
                });

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(
                GenericService.Get<ApplicationUser>(x => x.UserGroup.Name == "PSP"),
                "Id",
                "DisplayName",
                psp.UserId);
            return View(psp);
        }

        //// GET: admin/Psps/Delete/5
        //[GroupCustomAuthorize(Name = "PSPS:DELETION")]
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        // var psp = await GenericService .GetOneAsync<Psp>(x => x.Id == id.Value,
        // "CreatedByUser,ModifiedByUser,PspBranches") .ConfigureAwait(true); if (psp == null) {
        // return HttpNotFound(); }

        //    return View(psp);
        //}

        //// POST: admin/Psps/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "PSPS:DELETION")]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    Psp psp = await GenericService.GetOneAsync<Psp>(x => x.Id == id);
        //    GenericService.Delete(psp);
        //    return RedirectToAction("Index");
        //}

        [GroupCustomAuthorize(Name = "PSPS:VIEW")]

        // GET: admin/Psps/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var psp = await GenericService.GetOneAsync<Psp>(
                          x => x.Id == id.Value,
                          "CreatedByUser,ModifiedByUser,User,PspBranches").ConfigureAwait(true);
            if (psp == null)
            {
                return HttpNotFound();
            }

            return View(psp);
        }

        [GroupCustomAuthorize(Name = "PSPS:MODIFIER")]

        // GET: admin/Psps/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var psp = await GenericService.GetOneAsync<Psp>(x => x.Id == id).ConfigureAwait(true);
            if (psp == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = new SelectList(
                await this.GenericService.GetAsync<ApplicationUser>(x => x.UserGroup.Name == "PSP")
                    .ConfigureAwait(false),
                "Id",
                "DisplayName",
                psp.UserId);
            return View(psp);
        }

        // POST: admin/Psps/Edit/5 To protect from overposting attacks, please enable the specific
        // properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "PSPS:MODIFIER")]
        public ActionResult Edit(Psp psp)
        {
            psp.ModifiedBy = int.Parse(User.Identity.GetUserId());
            psp.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(psp);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = psp.Id,
                    TableName = "Psp",
                    ModuleRightCode = "PSPS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(psp)}",
                    WasSuccessful = true,
                    Description = "Psp Modified Successfully"
                });

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(
                this.GenericService.Get<ApplicationUser>(x => x.UserGroup.Name == "PSP"),
                "Id",
                "DisplayName",
                psp.UserId);
            return View(psp);
        }

        [GroupCustomAuthorize(Name = "PSPS:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var psps = (await GenericService.GetAsync<Psp>(
                            null,
                            x => x.OrderBy(y => y.Code),
                            "CreatedByUser,ModifiedByUser,User").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(psps);
        }

        [GroupCustomAuthorize(Name = "PSPS:VIEW")]

        // GET: admin/Psps/Details/5
        public async Task<ActionResult> PspBranches(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var pspBranches = (await GenericService.GetAsync<PspBranch>(
                                   x => x.PspId == id.Value,
                                   x => x.OrderBy(y => y.Code),
                                   "CreatedByUser,ModifiedByUser,SubLocation,Psp").ConfigureAwait(true))
                .ToPagedList(pageNum, pageSize);
            if (pspBranches == null)
            {
                return HttpNotFound();
            }

            return View(pspBranches);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}