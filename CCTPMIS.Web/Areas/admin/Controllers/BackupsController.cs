﻿namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Context;
    using Business.Model;
    using CCTPMIS.Models.Admin;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class BackupsController : Controller
    {
        protected readonly IGenericService GenericService;

        private readonly ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public BackupsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public string BackUpCommand(string databaseName, string fileAddress)
        {
            return @"BACKUP DATABASE " + databaseName + @" TO DISK = '" + fileAddress + "' WITH FORMAT";
        }

        [GroupCustomAuthorize(Name = "BACKUPS:ENTRY")]
        public ActionResult Create()
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "BACKUPS:ENTRY")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DbBackup dbBak)
        {
            var bkpViewModel = new BackupViewModel { Result = false };

            var command = "select db_name()";
            var databaseName = db.Database.SqlQuery(typeof(string), command).ToListAsync().Result.FirstOrDefault()
                ?.ToString();

            var filePath = WebConfigurationManager.AppSettings["BACKUP_DATABASE_DIRECTORY"] + databaseName + "-"
                           + DateTime.Now.ToString("yyyyMMddHHmmss") + ".bak";

            var backUpQuery = BackUpCommand(databaseName, filePath);
            var result = db.Database.SqlQuery<List<string>>(backUpQuery).ToList();

            foreach (var list in result)
            {
                bkpViewModel.Result = false;
                result.ForEach(x => { bkpViewModel.Message += x.ToString(); });
                break;
            }

            var dbBackup = new DbBackup {
                CreatedBy = int.Parse(User.Identity.GetUserId()),
                CreatedOn = DateTime.Now,
                FilePath = filePath,
            };
            try
            {
                GenericService.Create(dbBackup);

                ViewBag.Message = "Database backup taken Successfully";
            }
            catch (Exception exc)
            {
                ViewBag.Message = bkpViewModel.Message + " " + exc.Message.ToString();
            }
            return View(dbBackup);
        }

        public new void Dispose()
        {
            db.Dispose();
        }

        [GroupCustomAuthorize(Name = "BACKUPS:ENTRY")]
        public ActionResult Files()
        {
            var storagePath = WebConfigurationManager.AppSettings["APPLICATION_BACKUP_DIRECTORY"];
            var path = System.Web.HttpRuntime.AppDomainAppPath;
            using (var zip = new Ionic.Zip.ZipFile())
            {
                zip.AddDirectory(path, storagePath);
                var fileName = "Backup_" + DateTime.Now.ToString(@"yyyyMMddTHHmmss") + ".zip";
                zip.Save(fileName);
            }

            return View();
        }

        // GET: admin/Backups
        [GroupCustomAuthorize(Name = "BACKUPS:VIEW")]
        public async Task<ActionResult> Index(int? page, string name)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var backups =
                (await GenericService.GetAsync<DbBackup>(null, x => x.OrderByDescending(y => y.CreatedOn), "User")
                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(backups);
        }
    }
}