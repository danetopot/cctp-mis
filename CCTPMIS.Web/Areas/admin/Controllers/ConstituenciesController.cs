﻿using CCTPMIS.Models.AuditTrail;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Areas.admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Model;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class ConstituenciesController : Controller
    {
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public ConstituenciesController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        // GET: admin/Constituencies/Create
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create()
        {
            ViewBag.CountyId = new SelectList(GenericService.GetAll<County>(), "Id", "Name");
            return View();
        }

        // POST: admin/Constituencies/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOUNITS:ENTRY")]
        public ActionResult Create(
            [Bind(Include = "Id,Code,Name,CountyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Constituency constituency)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            constituency.CreatedBy = userId;
            constituency.CreatedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Create(constituency);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = constituency.Id,

                    TableName = "Constituency",
                    ModuleRightCode = "GEOUNITS:ENTRY",
                    Record = $"{JsonConvert.SerializeObject(constituency)}",
                    WasSuccessful = true,
                    Description = "Change Password Success"
                };
                LogService.AuditTrail(auditTrailVm);

                // await GenericService.SaveAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CountyId = new SelectList(GenericService.GetAll<County>(), "Id", "Name", constituency.CountyId);
            return View(constituency);
        }

        // GET: admin/Constituencies/Details/5
        [GroupCustomAuthorize(Name = "GEOUNITS:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var constituency = await GenericService.GetOneAsync<Constituency>(x => x.Id == id,
                                   "CreatedByUser,ModifiedByUser,County.GeoMaster,Wards");
            if (constituency == null)
            {
                return HttpNotFound();
            }

            return View(constituency);
        }

        // GET: admin/Constituencies/Edit/5
        [GroupCustomAuthorize(Name = "GEOMASTER:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var constituency = await GenericService.GetByIdAsync<Constituency>(id);
            if (constituency == null)
            {
                return HttpNotFound();
            }

            ViewBag.CountyId = new SelectList(GenericService.GetAll<County>(), "Id", "Name", constituency.CountyId);
            return View(constituency);
        }

        // POST: admin/Constituencies/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "GEOMASTER:MODIFIER")]
        public ActionResult Edit(
            [Bind(Include = "Id,Code,Name,CountyId,CreatedOn,ModifiedBy,ModifiedOn,CreatedBy")]
            Constituency constituency)
        {
            var userId = int.Parse(User.Identity.GetUserId());

            constituency.ModifiedBy = userId;
            constituency.ModifiedOn = DateTime.Now;

            if (ModelState.IsValid)
            {
                GenericService.Update(constituency);

                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = constituency.Id,

                    TableName = "Constituency",
                    ModuleRightCode = "GEOUNITS:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(constituency)}",
                    WasSuccessful = true,
                    Description = "constituency Modified Success"
                };
                LogService.AuditTrail(auditTrailVm);
                return RedirectToAction("Index");
            }

            ViewBag.CountyId = new SelectList(GenericService.GetAll<County>(), "Id", "Name", constituency.CountyId);
            return View(constituency);
        }

        [GroupCustomAuthorize(Name = "GEOMASTER:VIEW")]

        // GET: admin/Constituencies
        public async Task<ActionResult> Index(int? page, int? countyId)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            IPagedList<Constituency> constituencies = null;
            constituencies = countyId.HasValue
                                 ? (await GenericService.GetAsync<Constituency>(
                                        x => x.CountyId == countyId,
                                        x => x.OrderBy(y => y.Code),
                                        "Wards,County.GeoMaster")).ToPagedList(pageNum, pageSize)
                                 : (await GenericService.GetAsync<Constituency>(
                                        null,
                                        x => x.OrderBy(y => y.Code),
                                        "County,Wards")).ToPagedList(pageNum, pageSize);
            ViewBag.CountyId = new SelectList(GenericService.GetAll<County>(), "Id", "Name");

            return View(constituencies);
        }

        [GroupCustomAuthorize(Name = "GEOMASTER:VIEW")]
        public async Task<ActionResult> Wards(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var constituency = await GenericService.GetOneAsync<Constituency>(
                                   x => x.Id == id,
                                   "CreatedByUser,ModifiedByUser,County.GeoMaster,Wards");
            if (constituency == null)
            {
                return HttpNotFound();
            }

            return View(constituency);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}