﻿using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.Recertification
{
    public class RecertificationAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Recertification";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Recertification_default",
                "Recertification/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
            new[] { "CCTPMIS.Web.Areas.Recertification.Controllers" });
        }
    }
}