﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Model;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Recertification.Controllers
{
    [Authorize]
    public class ClosureController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        // private ApplicationDbContext db = new ApplicationDbContext();
        public ClosureController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "RECERTIFICATION CLOSURE:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = "CLOSED,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "Recertification",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }
    }
}