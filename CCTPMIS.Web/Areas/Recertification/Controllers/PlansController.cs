﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using PagedList;

namespace CCTPMIS.Web.Areas.Recertification.Controllers
{
    public class PlansController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public PlansController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        #region Targeting Plan View

        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:VIEW")]
        // GET: Targeting/Default
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => x.Category.Code == "Recertification",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var TargetPlan
                = await GenericService.GetOneAsync<TargetPlan>(
                        x => x.Id == id.Value,
                        "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                    .ConfigureAwait(true);
            if (TargetPlan == null)
            {
                return HttpNotFound();
            }

            TargetPlan.TargetPlanProgrammes = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == TargetPlan.Id, null, "Programme,ListingPlan")).ToList();

            var model = new RegistrationSummaryVm {
                TargetPlan = TargetPlan,
            };
            return View(model);
        }

        #endregion Targeting Plan View

        #region Targeting Plan Creation

        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var model = new TargetPlanVm {
                Start = DateTime.Today,
                End = DateTime.Today
            };
            var spName = "GetTargetPlanProgrammeOptions";
            var parameterNames = "";
            var parameterList = new List<ParameterEntity>();

            model.TargetPlanProgrammeOptionsVms = GenericService.GetManyBySp<TargetPlanProgrammeOptionsVm>(spName, parameterNames, parameterList).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:ENTRY")]
        public async Task<ActionResult> Create(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };
                var tarPlanProgrammeOptionsXmlModel = new List<TargetPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(
                    typeof(List<TargetPlanProgrammeOptionsVm>),
                    new XmlRootAttribute("TargetPlanProgrammeOptions"));
                var plans = model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList());
                    xml += sw.ToString();
                }

                var spName = "AddEditRegistrationPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Category,@UserId,@TargetPlanProgrammeXml";
                var parameterList = new List<ParameterEntity>
                                {
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id", DBNull.Value)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                                    new ParameterEntity{ParameterTuple = new Tuple<string, object>("Description",model.Description)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","Recertification")},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanProgrammeXml",xml)}
                                };
                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    TempData["MESSAGE"] = $" The Recertification Exercise was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The Recertification Exercise was not created." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }

            TempData["MESSAGE"] = "Recertification Exercise was not Created";
            TempData["KEY"] = "success";
            return View(model);
        }

        #endregion Targeting Plan Creation

        #region Targeting Plan Modifier

        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            var TargetPlan = (await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id)
                .ConfigureAwait(false));
            var model = new TargetPlanVm();

            TargetPlan.TargetPlanProgrammes = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == TargetPlan.Id, null, "Programme,ListingPlan")).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:MODIFIER")]
        public ActionResult Edit(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };
                var tarPlanProgrammeOptionsXmlModel = new List<TargetPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(
                    typeof(List<TargetPlanProgrammeOptionsVm>),
                    new XmlRootAttribute("TargetPlanProgrammeOptions"));
                var plans = model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList());
                    xml += sw.ToString();
                }

                var spName = "AddEditTargetPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Category,@UserId,@TargetPlanProgrammeXml";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",model.Id)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                                            new ParameterEntity{ ParameterTuple =new Tuple<string, object>("Description",model.Description)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},

                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","Recertification")},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("@TargetPlanProgrammeXml",xml)}
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    TempData["MESSAGE"] = $" The Targeting Exercise was  successfully Updated.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The Targeting Exercise was not Updated." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }

            TempData["MESSAGE"] = "Targeting Exercise was Updated";
            TempData["KEY"] = "danger";
            return View(model);
        }

        #endregion Targeting Plan Modifier

        #region Approve Targeting Exercise

        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "RECERTIFICATION PLANS:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The Targeting Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Approve Targeting Exercise
    }
}