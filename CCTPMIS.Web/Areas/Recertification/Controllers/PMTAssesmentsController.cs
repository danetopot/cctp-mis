﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Recertification.Controllers
{
    public class PMTAssesmentsController : Controller
    {
        public IGenericService GenericService;
        private ISingleRegistryService SingleRegistryService;
        private readonly ILogService LogService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public PMTAssesmentsController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "RECERTIFICATION PMT ASSESMENT:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => x.Category.Code == "Recertification",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "RECERTIFICATION PMT ASSESMENT:VIEW")]
        public async Task<ActionResult> Households(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<HouseholdReg, bool>> filter = x => x.TargetPlanId == id;

            Expression<Func<HouseholdReg, bool>> filterx = null;

            int? constituencyId = null;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }

            int? countyId = null;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }

            if (constituencyId.HasValue)
            {
                filterx = x => x.SubLocation.ConstituencyId == constituencyId;
                filter = filter.And(filterx);
            }

            if (countyId.HasValue)
            {
                filterx = x => x.Location.Division.CountyDistrict.CountyId == constituencyId;
                filter = filter.And(filterx);
            }

            var TargetPlans = (await GenericService.GetAsync<HouseholdReg>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(TargetPlans);
        }

        public async Task<ActionResult> HouseholdSummary(int id, int locationId)
        {
            var model = (await GenericService.GetOneAsync<HouseholdReg>(
                    x => x.Id == id,
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,Programme,EnumeratorDevice,HouseholdRegCharacteristic,HouseholdRegMembers,HouseholdRegProgrammes")
                .ConfigureAwait(true));
            ViewBag.ModalSize = "modal-lg";
            return View(model);
        }
    }
}