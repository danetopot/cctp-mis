﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Email;
using CCTPMIS.Services;
using Elmah;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.CaseManagement.Controllers
{
    [Authorize]
    public class ComplaintsController : Controller
    {
        protected readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        public IEmailService EmailService;
        private readonly ILogService LogService;

        public ComplaintsController(IGenericService genericService, IEmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VIEW")]
        public async Task<ActionResult> Index(ComplaintListViewModel vm, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            List<int> constituencies = null;

            if (progOfficer.ConstituencyId != null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetSearchableQueryable<Constituency>(x => x.Id == progOfficer.ConstituencyId).ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.ConstituencyId == null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetSearchableQueryable<Constituency>(x => x.CountyId == progOfficer.CountyId).ConfigureAwait(false))
                    .Select(x => x.Id).ToList();
            }

            if (progOfficer.ConstituencyId == null && progOfficer.CountyId == null)
            {
                constituencies = (await GenericService.GetAllAsync<Constituency>().ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>().ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(x => x.CountyId == vm.CountyId).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            Expression<Func<Complaint, bool>> filter = x => x.Id > 0;
            Expression<Func<Complaint, bool>> filterx = null;

            filterx = x => constituencies.Contains(x.ConstituencyId);
            filter = filter.And(filterx);

            var pagenm = page ?? 1;
            ViewBag.ComplaintStatusId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Case Status").ConfigureAwait(false), "Id", "Description", vm.ComplaintStatusId);
            ViewBag.ComplaintCategoryId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE").ConfigureAwait(false), "Id", "Name", vm.ComplaintCategoryId);

            if (vm.ComplaintStatusId != null)
            {
                filterx = c => c.ComplaintStatusId == vm.ComplaintStatusId;
                filter = filter.And(filterx);
            }

            if (vm.ComplaintCategoryId != null)
            {
                filterx = c => c.ComplaintTypeId == vm.ComplaintCategoryId;
                filter = filter.And(filterx);
            }
            if (vm.CountyId != null)
            {
                filterx = c => c.Constituency.CountyId == vm.CountyId;
                filter = filter.And(filterx);
            }
            if (vm.ConstituencyId != null)
            {
                filterx = c => c.ConstituencyId == vm.ConstituencyId;
                filter = filter.And(filterx);
            }
            vm.Complaints = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), includeProperties: "ComplaintStatus,ComplaintType,Programme,ComplaintLevel").ConfigureAwait(false)).ToPagedList(pagenm, 20); ;

            return View(vm);
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VIEW")]
        public async Task<ActionResult> Beneficiaries(CaseFilterViewModel model)
        {
            //var districtIds = userPermissions.Districts();
            //bool isGlobal = userPermissions.IsGlobal();
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            if (model.HouseholdEnrolmentId != null || model.ProgrammeNo != null || model.NationalId != null || !string.IsNullOrEmpty(model.Name) ||
                model.ConstituencyId != null || model.CountyId != null)
            {
                var enrollmentDetails = db.HouseholdEnrolments.AsNoTracking()
                    .Include(b => b.Household.Status)
                    .Include(b => b.Household.Programme)
                    .Include(b => b.Household.Programme.PrimaryRecipient)
                    .Include(b => b.Household.Programme.BeneficiaryType)
                    .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                    .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                    .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship));

                if (model.HouseholdEnrolmentId != null)
                {
                    enrollmentDetails = enrollmentDetails.Where(b => b.Id == model.HouseholdEnrolmentId);
                }

                if (model.ProgrammeNo != null)
                {
                    if (model.ProgrammeNo.Length > 6)
                    {
                        var prefix = model.ProgrammeNo.Substring(0, 1);
                        var beneNo = int.Parse(model.ProgrammeNo.Substring(1, 6));
                        enrollmentDetails = enrollmentDetails.Where(b => b.BeneProgNoPrefix == prefix && b.ProgrammeNo == beneNo);
                    }
                }

                if (model.NationalId != null)
                {
                    enrollmentDetails = enrollmentDetails.Where(b => b.Household.HouseholdMembers.Any(x => x.Person.NationalIdNo == model.NationalId));
                }
                //if (model.CountyId != null)
                //{
                //    enrollmentDetails =
                //        enrollmentDetails.Where(b => b.Household.HouseholdSubLocation.SubLocation.Constituency.CountyId == model.CountyId);
                //}
                //  if (model.ConstituencyId != null)
                //  {
                //      enrollmentDetails = enrollmentDetails.Where(b => b.Household.HouseholdSubLocation.SubLocation.Location == model.ConstituencyId);
                //}

                if (!string.IsNullOrEmpty(model.Name))
                {
                    enrollmentDetails = enrollmentDetails.Where(
                        b => b.Household.HouseholdMembers.Any(x => x.Person.FirstName.Contains(model.Name.Trim())) ||
                             b.Household.HouseholdMembers.Any(x => x.Person.MiddleName.Contains(model.Name.Trim())) ||
                             b.Household.HouseholdMembers.Any(x => x.Person.Surname.Contains(model.Name.Trim())));
                }

                if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                            "Name", progOfficer.CountyId);

                    ViewBag.SubLocationId =
                        new SelectList(
                            await GenericService.GetSearchableQueryable<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                            "Id", "Name");
                }
                if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
                }

                if (!progOfficer.CountyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                            "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
                }

                var page = model.Page ?? 1;
                var pageSize = model.PageSize ?? 10;
                model.HouseholdEnrolments = enrollmentDetails.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                        "Name", progOfficer.CountyId);

                ViewBag.SubLocationId =
                    new SelectList(
                        await GenericService.GetSearchableQueryable<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                        "Id", "Name");
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                        "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                ViewBag.SubLocationId = new SelectList(await GenericService.GetSearchableQueryable<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
            }

            return View(model);
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:ENTRY")]
        public async Task<ActionResult> Create(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();

            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");
            ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            ViewBag.ComplaintTypeId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE", n => n.OrderBy(i => i.Name)).ConfigureAwait(false), "Id", "Name");
            ViewBag.SourceId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false), "Id", "Description");

            var vm = new ComplaintViewModel {
                ComplaintTypes = (await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE", n => n.OrderBy(i => i.Id)).ConfigureAwait(false)).ToList()
            };

            vm.DocumentTypes = (await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Complaint Doc Type", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false)).ToList();

            var spName = "GetHouseholdInfo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id) } };
            var caseIssues = GenericService.GetOneBySp<GetBeneInfoVm>(spName, parameterNames, parameterList);

            var hhEnrolment = db.HouseholdEnrolments.AsNoTracking()
                .Include(b => b.Household.Status)
                .Include(b => b.Household.Programme)
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship))
                .SingleOrDefault(b => b.Id == id);

            if (hhEnrolment == null)
            {
                return HttpNotFound();
            }

            vm.HouseholdEnrolment = hhEnrolment;
            vm.GetBeneInfoVm = caseIssues;
            vm.HhEnrolmentId = id;

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
            }

            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:ENTRY")]
        public async Task<ActionResult> Create(ComplaintViewModel model, HttpPostedFileBase file)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var complaint = new Complaint();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(model, complaint);

            complaint.CreatedOn = DateTime.Now;
            complaint.CreatedBy = int.Parse(User.Identity.GetUserId());

            complaint.ComplaintStatusId = db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Open" && x.SystemCode.Code == "Case Status").Id; ;
            complaint.ComplaintLevelId = db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Sub County" && x.SystemCode.Code == "Escalation Levels").Id; ;
            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.AddOrUpdate(complaint);
                    var complaintNote = new ComplaintNote {
                        CategoryId = complaint.ComplaintStatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        ComplaintId = complaint.Id,
                        CreatedOn = DateTime.UtcNow,
                        Note = model.Notes
                    };
                    GenericService.AddOrUpdate(complaintNote);
                    var complaintDocument = new ComplaintDocument();
                    if (file != null && file.ContentLength > 0)
                    {
                        var path = "";

                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "-";
                        fileName = fileName + file.FileName;
                        path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                        file.SaveAs(path + fileName);

                        complaintDocument = new ComplaintDocument {
                            ComplaintId = complaint.Id,
                            CreatedOn = DateTime.UtcNow,

                            CategoryId = complaint.ComplaintStatusId,
                            CreatedById = int.Parse(User.Identity.GetUserId()),
                            FileName = fileName,
                            FilePath = path
                        };
                        GenericService.AddOrUpdate(complaintDocument);
                    }

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = complaint.Id,
                        TableName = "Complaint",
                        ModuleRightCode = "COMPLAINTS AND GRIEVANCES:ENTRY",
                        Record = $"[{JsonConvert.SerializeObject(complaint)},{JsonConvert.SerializeObject(complaintNote)},{JsonConvert.SerializeObject(complaintDocument)}]",
                        WasSuccessful = true,
                        Description = "Complaint Created Successfully"
                    });

                    TempData["MESSAGE"] = "Complaint has been Added successfully.";
                    TempData["KEY"] = "success";

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var error = ex.InnerException?.Message;

                    ViewBag.ComplaintTypeId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE", n => n.OrderBy(i => i.Name)).ConfigureAwait(false), "Id", "Name");
                    ViewBag.ComplaintStatusId = new SelectList(await GenericService.GetAllAsync<SystemCodeDetail>().ConfigureAwait(false), "Id", "Code");
                    model.ComplaintTypes = (await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE").ConfigureAwait(false)).ToList();

                    TempData["MESSAGE"] = error;
                    TempData["KEY"] = "danger";
                    // return View(model);
                }
            }

            /*

            if (file != null && file.ContentLength > 0)
            {
                
            }
            else
            {
                TempData["MESSAGE"] = "Uploads are mandatory. Try again";
                TempData["KEY"] = "danger";
            }
            */

            // var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                    "Name", progOfficer.CountyId);
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                        "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }
            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
            var message = string.Join(" | ", ModelState.Values
            .SelectMany(v => v.Errors)
            .Select(e => e.ErrorMessage));

            //TempData["MESSAGE"] = message;
            //TempData["KEY"] = "danger";

            ViewBag.DocumentTypeId = new SelectList(GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "Complaints Document Types"), "Id", "Code");

            ViewBag.BenSexId = new SelectList(
                await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");
            ViewBag.CgSexId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");

            model.ComplaintTypes = (await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE", n => n.OrderBy(i => i.Id)).ConfigureAwait(false)).ToList();
            model.DocumentTypes = (await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Complaint Doc Type", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false)).ToList();

            ViewBag.SourceId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false), "Id", "Description", model.SourceId);
            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description", model.ReportedBySexId);
            ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", model.ReportedByTypeId);

            // model.ComplaintCategoryId = 1;

            var spName = "GetHouseholdInfo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", model.Id) } };
            var caseIssues = GenericService.GetOneBySp<GetBeneInfoVm>(spName, parameterNames, parameterList);

            var hhEnrolment = db.HouseholdEnrolments.AsNoTracking()
                .Include(b => b.Household.Status)
                .Include(b => b.Household.Programme)
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship))
                .SingleOrDefault(b => b.Id == model.Id);

            if (hhEnrolment == null)
            {
                return HttpNotFound();
            }
            model.HouseholdEnrolment = hhEnrolment;
            model.GetBeneInfoVm = caseIssues;
            model.HhEnrolmentId = model.Id;
            return View(model);
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:MODIFIER")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType,Constituency,ReportedByType,Source,HhEnrolment,ReportedBySex,ResolvedByUser,ClosedByUser,VerifiedByUser,EscalatedByUser,Escalated2ByUser,Source");
            if (complaint == null)
            {
                return HttpNotFound();
            }
            var beneficiary = GenericService.GetOne<HouseholdEnrolment>(x => x.Id == complaint.HhEnrolmentId);

            var model = new ComplaintViewModel();
            //new MapperConfiguration(cfg => cfg.CreateMissingTypeMaps = true).CreateMapper().Map(beneficiary, model);
            new MapperConfiguration(cfg => cfg.CreateMissingTypeMaps = true).CreateMapper().Map(complaint, model);

            // new MapperConfiguration(cfg => cfg.ValidateInlineMaps =
            // false).CreateMapper().Map(model, complaint);

            model.ComplaintType = complaint.ComplaintType;

            model.ComplaintTypes = GenericService.Get<CaseCategory>(x => x.CaseType.Code == "GRIEVANCE").ToList();
            //model.ComplaintCategoryId = GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "GRIEVANCE", n => n.OrderBy(i => i.OrderNo).Find(model.ComplaintTypeId).ComplaintCategoryId;
            model.ComplaintStatus = GenericService.GetOne<SystemCodeDetail>(x => x.Id == model.ComplaintStatusId).Code;
            // List<string> result = p.Get(s => s.StartsWith("A"), orderBy: q => q.OrderBy(d =>
            // d.Length)).ToList(); var conditions = new List<Expression<Func<GrandChild, bool>>>() {
            // (t) => t.Child.Parent.ParentId == 1 }; var merchantTypes =
            // unifOfWork.MerchantTypes.Get(includeProperties: "Parent", orderBy: mt => mt.OrderBy(m
            // => m.ParentId).OrderBy(m => m.Name));
            var includes = new List<Expression<Func<ComplaintNote, object>>> { t => t.CreatedBy, t => t.CreatedBy };

            model.ComplaintNotes = GenericService.Get<ComplaintNote>(n => n.ComplaintId == complaint.Id).ToList();

            ViewBag.ComplaintStatusId = new SelectList(GenericService.GetAll<SystemCodeDetail>(), "Id", "Code", complaint.ComplaintStatusId);
            // ViewBag.ComplaintCategoryId = new
            // SelectList(GenericService.GetAll<ComplaintCategory>(), "Id", "Code", complaint.ComplaintType.ComplaintCategoryId);
            ViewBag.ComplaintTypeId = new SelectList(GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "GRIEVANCE", n => n.OrderBy(i => i.OrderNo)), "Id", "Code", complaint.ComplaintTypeId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:MODIFIER")]
        public ActionResult Edit(ComplaintViewModel model)
        {
            var complaint = GenericService.GetOne<Complaint>(c => c.Id == model.Id, "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType,Constituency,ReportedByType,Source,HhEnrolment,ReportedBySex,ResolvedByUser,ClosedByUser,VerifiedByUser,EscalatedByUser,Escalated2ByUser,Source");

            if (ModelState.IsValid)
            {
                new MapperConfiguration(cfg => cfg.CreateMissingTypeMaps = true).CreateMapper().Map(model, complaint);
                GenericService.AddOrUpdate(complaint);
                GenericService.Save();
                return RedirectToAction("Index");
            }
            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            ViewBag.ComplaintStatusId = new SelectList(GenericService.GetAll<SystemCodeDetail>(), "Id", "Code", complaint.ComplaintStatusId);
            ViewBag.ComplaintTypeId = new SelectList(GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "GRIEVANCE", n => n.OrderBy(i => i.OrderNo)), "Id", "Code", complaint.ComplaintTypeId);

            return View(model);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public ActionResult Preview(int id)
        {
            var file = this.GenericService.GetOne<ComplaintDocument>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            string fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();
            byte[] FileBytes = System.IO.File.ReadAllBytes(fileName);

            Response.AppendHeader("content-disposition", "inline; filename=" + file.FileName);
            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";
            Response.Write("<script>window.open('" + fileName + "','_blank');</script>");

            return File(FileBytes, "application/pdf");
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VIEW")]
        public FileResult Download(int id)
        {
            var file = this.GenericService.GetOne<ComplaintDocument>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            Response.AppendHeader("content-disposition", "attachment; filename=" + file.FileName);
            string fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();

            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";

            Response.WriteFile(fileName);
            Response.Flush();
            Response.End();

            return null;
        }

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VIEW")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var complaint = GenericService.GetOne<Complaint>(c => c.Id == id,
                includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType,Constituency,ReportedByType,Source,HhEnrolment,ReportedBySex,ResolvedByUser,ClosedByUser,VerifiedByUser,EscalatedByUser,Escalated2ByUser,Source,Programme");
            if (complaint == null)
            {
                return HttpNotFound();
            }

            complaint.ComplaintNotes = GenericService.Get<ComplaintNote>(n => n.ComplaintId == complaint.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ToList();

            complaint.ComplaintDocuments = GenericService.Get<ComplaintDocument>(n => n.ComplaintId == complaint.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ToList();
            ViewBag.ComplaintTypeId = new SelectList(GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "GRIEVANCE", n => n.OrderBy(i => i.OrderNo)), "Id", "Code", complaint.ComplaintTypeId);
            return View(complaint);
        }

        #region Verify

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VERIFY")]
        public ActionResult Verify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:VERIFY")]
        public async Task<ActionResult> Verify(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.Notes))
                {
                    var userId = int.Parse(User.Identity.GetUserId());
                    var complaint = await GenericService.GetOneAsync<Complaint>(c => c.Id == model.Id).ConfigureAwait(false);
                    complaint.ComplaintStatusId = (await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Verified" && x.SystemCode.Code == "Case Status").ConfigureAwait(false)).Id;
                    complaint.VerifiedBy = userId;
                    complaint.VerifiedOn = DateTime.Now;
                    GenericService.AddOrUpdate(complaint);

                    var complaintNote = new ComplaintNote {
                        CreatedOn = DateTime.Now,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        ComplaintId = complaint.Id,
                        Note = model.Notes,
                        CategoryId = complaint.ComplaintStatusId
                    };
                    GenericService.AddOrUpdate(complaintNote);

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                        var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                        var complaintDocument = new ComplaintDocument {
                            ComplaintId = complaint.Id,
                            CreatedOn = DateTime.UtcNow,

                            CategoryId = complaint.ComplaintStatusId,
                            CreatedById = int.Parse(User.Identity.GetUserId()),
                            FileName = fileName,
                            FilePath = path
                        };
                        GenericService.AddOrUpdate(complaintDocument);
                        file.SaveAs(path + fileName);
                    }

                    TempData["MESSAGE"] = "Complaint Verified Successfully";
                    TempData["KEY"] = "success";
                }
                else
                {
                    TempData["MESSAGE"] = "Error on Verifying the Complaint. Notes are required.";
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on Verifying the Complaint";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Verify

        #region Follow Up

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:FOLLOW UP")]
        public ActionResult FollowUp(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:FOLLOW UP")]
        public async Task<ActionResult> FollowUp(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var complaint = await GenericService.GetOneAsync<Complaint>(c => c.Id == model.Id).ConfigureAwait(false);
                var followUpCaytegory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Follow Up" && x.SystemCode.Code == "Case Status").ConfigureAwait(false);
                var complaintNote = new ComplaintNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    ComplaintId = complaint.Id,
                    Note = model.Notes,
                    CategoryId = followUpCaytegory.Id
                };

                GenericService.AddOrUpdate(complaintNote);
                var complaintDocument = new ComplaintDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    complaintDocument = new ComplaintDocument {
                        ComplaintId = complaint.Id,
                        CreatedOn = DateTime.UtcNow,

                        CategoryId = followUpCaytegory.Id,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(complaintDocument);
                    file.SaveAs(path + fileName);
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = complaint.Id,
                    TableName = "Complaint",
                    ModuleRightCode = "COMPLAINTS AND GRIEVANCES:FOLLOW UP",
                    Record = $"[{JsonConvert.SerializeObject(complaint)},{JsonConvert.SerializeObject(complaintNote)},{JsonConvert.SerializeObject(complaintDocument)}]",
                    WasSuccessful = true,
                    Description = "Complaint follow up Successfully"
                });

                await GenericService.SaveAsync().ConfigureAwait(false);
                TempData["MESSAGE"] = "Complaint Followed Up Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception x)
            {
                TempData["MESSAGE"] = "Error on Follow Up on  the Complaint";
                TempData["KEY"] = "danger";
                ErrorLog.GetDefault(null).Log(new Error(x));
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Follow Up

        #region Escalate

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:ESCALATE")]
        public ActionResult Escalate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:ESCALATE")]
        public async Task<ActionResult> Escalate(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var complaint = await GenericService.GetOneAsync<Complaint>(c => c.Id == model.Id)
                    .ConfigureAwait(false);
                var currentLevel =
                    await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Id == complaint.ComplaintLevelId);
                switch (currentLevel.Code)
                {
                    case "Sub County":
                        complaint.ComplaintLevelId = (await GenericService
                                .GetOneAsync<SystemCodeDetail>(x =>
                                    x.Code == "County" && x.SystemCode.Code == "Escalation Levels")
                                .ConfigureAwait(false))
                            .Id;
                        complaint.EscalatedBy = userId;
                        complaint.EscalatedOn = DateTime.Now;
                        complaint.ComplaintStatusId = (await GenericService
                            .GetOneAsync<SystemCodeDetail>(x =>
                                x.Code == "Escalated" && x.SystemCode.Code == "Case Status").ConfigureAwait(false)).Id;
                        break;

                    case "County":
                        complaint.ComplaintLevelId = (await GenericService
                                .GetOneAsync<SystemCodeDetail>(x =>
                                    x.Code == "National" && x.SystemCode.Code == "Escalation Levels")
                                .ConfigureAwait(false))
                            .Id;
                        complaint.Escalated2By = userId;
                        complaint.Escalated2On = DateTime.Now;
                        complaint.ComplaintStatusId = (await GenericService
                            .GetOneAsync<SystemCodeDetail>(x =>
                                x.Code == "Escalated" && x.SystemCode.Code == "Case Status").ConfigureAwait(false)).Id;

                        break;

                    default:
                        TempData["MESSAGE"] = "You cannot Escalate a casethat is at the National Level";
                        TempData["KEY"] = "danger";
                        return RedirectToAction("Details", new { id = model.Id });
                }

                GenericService.AddOrUpdate(complaint);
                var complaintNote = new ComplaintNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    ComplaintId = complaint.Id,
                    Note = model.Notes,
                    CategoryId = complaint.ComplaintStatusId
                };
                GenericService.AddOrUpdate(complaintNote);
                var complaintDocument = new ComplaintDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = "";
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "-";
                    fileName = fileName + file.FileName;
                    path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] +
                                          WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    file.SaveAs(path + fileName);

                    complaintDocument = new ComplaintDocument {
                        ComplaintId = complaint.Id,
                        CreatedOn = DateTime.UtcNow,

                        CategoryId = complaint.ComplaintStatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(complaintDocument);
                }

                var users = (await GenericService.GetSearchableQueryable<ComplaintNote>(x => x.ComplaintId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ComplaintNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Complaint #{model.Id} Escalated",
                            Title = $"Complaint #{model.Id} Escalated",
                            Action = "Escalated",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = complaint.Id,
                    TableName = "Complaint",
                    ModuleRightCode = "COMPLAINTS AND GRIEVANCES:ESCALATE",
                    Record = $"[{JsonConvert.SerializeObject(complaint)},{JsonConvert.SerializeObject(complaintNote)},{JsonConvert.SerializeObject(complaintDocument)}]",
                    WasSuccessful = true,
                    Description = "Complaint Escalated Successfully"
                });

                TempData["MESSAGE"] = "Complaint Escalated Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception x)
            {
                TempData["MESSAGE"] = "Error on   Complaint Escalation";
                TempData["KEY"] = "danger";
                ErrorLog.GetDefault(null).Log(new Error(x));
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Escalate

        #region Close

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:CLOSE")]
        public ActionResult Close(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:CLOSE")]
        public async Task<ActionResult> Close(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var closeCategory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Closed" && x.SystemCode.Code == "Case Status").ConfigureAwait(false);

                var complaint = await GenericService.GetOneAsync<Complaint>(c => c.Id == model.Id).ConfigureAwait(false);
                complaint.ClosedBy = userId;
                complaint.ClosedOn = DateTime.Now;
                complaint.ComplaintStatusId = closeCategory.Id;
                GenericService.AddOrUpdate(complaint);
                var complaintNote = new ComplaintNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = userId,
                    ComplaintId = complaint.Id,
                    Note = model.Notes,
                    CategoryId = closeCategory.Id
                };
                GenericService.AddOrUpdate(complaintNote);
                var complaintDocument = new ComplaintDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;

                    file.SaveAs(path + fileName);

                    complaintDocument = new ComplaintDocument {
                        ComplaintId = complaint.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = closeCategory.Id,
                        CreatedById = userId,
                        FileName = fileName,
                        FilePath = path
                    };

                    GenericService.AddOrUpdate(complaintDocument);
                }
                var users = (await GenericService.GetSearchableQueryable<ComplaintNote>(x => x.ComplaintId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ComplaintNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Complaint #{model.Id} Closed",
                            Title = $"Complaint #{model.Id} Closed",
                            Action = "Closed",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = complaint.Id,
                    TableName = "Complaint",
                    ModuleRightCode = "COMPLAINTS AND GRIEVANCES:CLOSE",
                    Record = $"[{JsonConvert.SerializeObject(complaint)},{JsonConvert.SerializeObject(complaintNote)},{JsonConvert.SerializeObject(complaintDocument)}]",
                    WasSuccessful = true,
                    Description = "Complaint Closed Successfully"
                });

                TempData["MESSAGE"] = "Complaint Closed Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception x)
            {
                TempData["MESSAGE"] = "Error on   Complaint Closure";
                TempData["KEY"] = "danger";
                ErrorLog.GetDefault(null).Log(new Error(x));
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Close

        #region Resolve

        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:RESOLVE")]
        public ActionResult Resolve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Complaint = GenericService.GetOne<Complaint>(c => c.Id == id, includeProperties: "ReportedBySex,ComplaintStatus,ComplaintLevel,ComplaintType")
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "COMPLAINTS AND GRIEVANCES:RESOLVE")]
        public async Task<ActionResult> Resolve(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var resolveCategory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Resolved" && x.SystemCode.Code == "Case Status").ConfigureAwait(false);

                var complaint = await GenericService.GetOneAsync<Complaint>(c => c.Id == model.Id).ConfigureAwait(false);
                complaint.ResolvedBy = userId;
                complaint.ResolvedOn = DateTime.Now;
                complaint.ComplaintStatusId = resolveCategory.Id;
                complaint.ComplaintLevelId = (await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "National" && x.SystemCode.Code == "Escalation Levels").ConfigureAwait(false)).Id;
                GenericService.AddOrUpdate(complaint);

                var complaintNote = new ComplaintNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = userId,
                    ComplaintId = complaint.Id,
                    Note = model.Notes,
                    CategoryId = resolveCategory.Id
                };
                GenericService.AddOrUpdate(complaintNote);

                var complaintDocument = new ComplaintDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;

                    file.SaveAs(path + fileName);

                    complaintDocument = new ComplaintDocument {
                        ComplaintId = complaint.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = resolveCategory.Id,
                        CreatedById = userId,
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(complaintDocument);
                }

                var users = (await GenericService.GetSearchableQueryable<ComplaintNote>(x => x.ComplaintId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ComplaintNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Complaint #{model.Id} Resolved",
                            Title = $"Complaint #{model.Id} Resolved",
                            Action = "Resolved",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(
                    new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = complaint.Id,
                        TableName = "Complaint",
                        ModuleRightCode = "COMPLAINTS AND GRIEVANCES:RESOLVE",
                        Record = $"[{JsonConvert.SerializeObject(complaint)},{JsonConvert.SerializeObject(complaintNote)},{JsonConvert.SerializeObject(complaintDocument)}]",
                        WasSuccessful = true,
                        Description = "Complaint Resolved Successfully"
                    });

                TempData["MESSAGE"] = "Complaint Resolved Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception x)
            {
                TempData["MESSAGE"] = "Error on   Complaint Resolution";
                TempData["KEY"] = "danger";
                // ErrorLog.GetDefault(null).Log(new Error(x));
                ErrorLog.GetDefault(null).Log(new Error(x));
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Resolve
    }
}