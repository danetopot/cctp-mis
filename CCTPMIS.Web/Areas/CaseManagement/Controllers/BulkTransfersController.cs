﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.CaseManagement.Controllers
{
    [Authorize]
    public class BulkTransfersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public BulkTransfersController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        public async Task<ActionResult> Index(int? page)
        {
            var pageNm = page ?? 1;
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var bulkTransfers = (await GenericService.GetSearchableQueryable<BulkTransfer>(null, x => x.OrderByDescending(y => y.Id), "Status,CreatedByUser,ModifiedByUser,ApvByUser").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(bulkTransfers);
        }

        #region Verify

        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public ActionResult Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = GenericService.GetOne<BulkTransfer>(c => c.Id == id, includeProperties: "");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public async Task<ActionResult> Approve(BulkTransfer model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);

            var storedProcedureGen = "GenerateBulkTransferFile";
            var parameterNamesGen = "@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@Id,@UserId";
            var parameterListGen = new List<ParameterEntity>
                                       {
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "FilePath",
                                                               WebConfigurationManager
                                                                   .AppSettings[
                                                                       "DIRECTORY_SHARED_FILES"]),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBServer",
                                                               connection
                                                                   .DataSource),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBName",
                                                               connection
                                                                   .InitialCatalog),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBUser",
                                                               connection.UserID),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "DBPassword",
                                                               connection
                                                                   .Password),
                                                   },
                                               new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "Id",
                                                               model.Id),
                                                   },
                                                   new ParameterEntity
                                                   {
                                                       ParameterTuple =
                                                           new Tuple<string,
                                                               object>(
                                                               "UserId",
                                                               userId),
                                                   },
                                           };

            try
            {
                var fileVm = GenericService.GetOneBySp<SPOutput>(
               storedProcedureGen,
               parameterNamesGen,
               parameterListGen);
                var fileService = new FileService();
                var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == fileVm.FileCreationId.Value).ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "BulkTransfer",
                    ModuleRightCode = "UPDATES:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "BulkTransfer Created Successfully"
                });

                TempData["Title"] = "Bulk Transfer File Generation Complete";
                TempData["MESSAGE"] = $"Bulk Transfer File Generation  successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Bulk Transfer File Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Verify

        #region Verify

        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public async Task<ActionResult> AddUpdates(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = await GenericService.GetOneAsync<BulkTransfer>(c => c.Id == id, includeProperties: "");
            ViewBag.ReadyUpdates = await GenericService.GetCountAsync<Change>(x => x.ChangeStatus.Code == "Escalated").ConfigureAwait(false);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public ActionResult AddUpdates(BulkTransfer model)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var spName = "AddEditBulkTransferDetail";
                var parameterNames = "@Id,@UserId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", model.Id), },
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", userId), }
                };

                var result = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "BulkTransfer",
                    ModuleRightCode = "UPDATES:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "BulkTransfer Added  Updates Successfully"
                });

                TempData["MESSAGE"] = $"{result.NoOfRows} Updates Added to Bulk Transfer Batch  Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception exc)
            {
                TempData["MESSAGE"] = $"Error on Approving the Bulk Transfer  \n {exc?.InnerException?.Message}";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Verify

        // GET: CaseManagement/BulkTransfers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var bulkTransfer = await GenericService.GetOneAsync<BulkTransfer>(c => c.Id == id, includeProperties: "Status,CreatedByUser,ModifiedByUser,ApvByUser,BulkTransferDetails.Change.ChangeType");

            var ChangeTypes =
                (await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Change Types")).ToList();

            ViewBag.ChangeTypes = ChangeTypes;
            if (bulkTransfer == null)
            {
                return HttpNotFound();
            }
            return View(bulkTransfer);
        }

        // GET: CaseManagement/BulkTransfers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CaseManagement/BulkTransfers/Create To protect from overposting attacks, please
        // enable the specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,ApvBy,ApvOn,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")] BulkTransfer bulkTransfer)
        {
            bulkTransfer.CreatedBy = int.Parse(User.Identity.GetUserId());
            bulkTransfer.CreatedOn = DateTime.Now;
            bulkTransfer.StatusId = db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Open" && x.SystemCode.Code == "Bulk Transfer Status").Id;
            if (ModelState.IsValid)
            {
                db.BulkTransfers.Add(bulkTransfer);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = bulkTransfer.Id,
                    TableName = "BulkTransfer",
                    ModuleRightCode = "UPDATES:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(bulkTransfer)}",
                    WasSuccessful = true,
                    Description = "BulkTransfer Created Successfully"
                });

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
            }

            return View(bulkTransfer);
        }

        // GET: CaseManagement/BulkTransfers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BulkTransfer bulkTransfer = await db.BulkTransfers.FindAsync(id);
            if (bulkTransfer == null)
            {
                return HttpNotFound();
            }
            return View(bulkTransfer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,ApvBy,ApvOn,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn")] BulkTransfer bulkTransfer)
        {
            bulkTransfer.ModifiedBy = int.Parse(User.Identity.GetUserId());
            bulkTransfer.ModifiedOn = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(bulkTransfer).State = EntityState.Modified;

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = bulkTransfer.Id,
                    TableName = "BulkTransfer",
                    ModuleRightCode = "UPDATES:MODIFIER",
                    Record = $"{JsonConvert.SerializeObject(bulkTransfer)}",
                    WasSuccessful = true,
                    Description = "BulkTransfer Modified Successfully"
                });

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(bulkTransfer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}