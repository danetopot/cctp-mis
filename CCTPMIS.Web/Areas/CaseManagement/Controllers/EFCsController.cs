﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Email;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.CaseManagement.Controllers
{
    [Authorize]
    public class EfCsController : Controller
    {
        protected readonly IGenericService GenericService;
        private ApplicationDbContext _db = new ApplicationDbContext();

        public IEmailService EmailService;
        private readonly ILogService _logService;

        public EfCsController(IGenericService genericService, IEmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            _logService = logService;
        }

        public ActionResult Intro()
        {
            return View();
        }

        [GroupCustomAuthorize(Name = "EFC:VIEW")]
        public async Task<ActionResult> Index(EFCListViewModel vm, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            List<int> constituencies = null;

            if (progOfficer.ConstituencyId != null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetSearchableQueryable<Constituency>(x => x.Id == progOfficer.ConstituencyId).ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.ConstituencyId == null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetSearchableQueryable<Constituency>(x => x.CountyId == progOfficer.CountyId).ConfigureAwait(false))
                    .Select(x => x.Id).ToList();
            }

            if (progOfficer.ConstituencyId == null && progOfficer.CountyId == null)
            {
                constituencies = (await GenericService.GetAllAsync<Constituency>().ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>().ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(x => x.CountyId == vm.CountyId).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            Expression<Func<EFC, bool>> filter = x => x.Id > 0;
            Expression<Func<EFC, bool>> filterx = null;

            ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", vm.ConstituencyId);

            //filter = filter.And(filterx);
            var statuses = "OPEN,CLOSED";

            if (vm.TypeId != null)
            {
                filterx = x => x.EFCTypeId == vm.TypeId;
                filter = filter.And(filterx);
            }
            if (vm.StatusId != null)
            {
                filterx = x => x.StatusId == vm.StatusId;
                filter = filter.And(filterx);
            }
            if (vm.CountyId != null)
            {
                filterx = x => x.Constituency.CountyId == vm.CountyId;
                filter = filter.And(filterx);
            }
            if (vm.ConstituencyId != null)
            {
                filterx = x => x.ConstituencyId == vm.ConstituencyId;
                filter = filter.And(filterx);
            }
            var pagenm = page ?? 1;
            ViewBag.StatusId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => statuses.Contains(x.Code) && x.SystemCode.Code == "Case Status").ConfigureAwait(false), "Id", "Code", vm.StatusId);
            ViewBag.TypeId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "EFC").ConfigureAwait(false), "Id", "Code", vm.TypeId);

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
            }

            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }

            vm.EFCs = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), includeProperties: "ReportedBySex,ReportedByType,Status,Source,Constituency").ConfigureAwait(false)).ToPagedList(pagenm, 20); ;
            return View(vm);
        }

        [GroupCustomAuthorize(Name = "EFC:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var progOfficer = User.GetProgOfficerSummary();

            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");
            ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            ViewBag.EFCTypeId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "EFC", n => n.OrderBy(i => i.Name)).ConfigureAwait(false), "Id", "Name");
            ViewBag.SourceId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false), "Id", "Description");

            var vm = new EFCViewModel {
                EFCTypes = (await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "EFC", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false)).ToList()
            };

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
            }

            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "EFC:ENTRY")]
        public async Task<ActionResult> Create(EFCViewModel model, HttpPostedFileBase file)
        {
            var efc = new EFC();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(model, efc);
            efc.ReportedDate = model.ReceivedOn;
            efc.Regarding = model.Regards;
            efc.CreatedOn = DateTime.Now;
            efc.CreatedBy = int.Parse(User.Identity.GetUserId());
            efc.StatusId = _db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Open" && x.SystemCode.Code == "Case Status").Id; ;

            if (ModelState.IsValid)
            {
                try
                {
                    GenericService.AddOrUpdate(efc);
                    var efcNote = new EFCNote {
                        CategoryId = efc.StatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        EFCId = efc.Id,
                        CreatedOn = DateTime.UtcNow,
                        Note = model.Notes
                    };
                    GenericService.AddOrUpdate(efcNote);
                    var efcDocument = new EFCDocument();
                    if (file != null && file.ContentLength > 0)
                    {
                        var path = "";

                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "-";
                        fileName = fileName + file.FileName;
                        path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                        file.SaveAs(path + fileName);

                        efcDocument = new EFCDocument {
                            EFCId = efc.Id,
                            CreatedOn = DateTime.UtcNow,

                            CategoryId = efc.StatusId,
                            CreatedById = int.Parse(User.Identity.GetUserId()),
                            FileName = fileName,
                            FilePath = path
                        };
                        GenericService.AddOrUpdate(efcDocument);
                    }
                    _logService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = efc.Id,
                        TableName = "EFC",
                        ModuleRightCode = "EFC:ENTRY",
                        Record = $"[{JsonConvert.SerializeObject(efc)},{JsonConvert.SerializeObject(efcNote)},{JsonConvert.SerializeObject(efcDocument)}]",
                        WasSuccessful = true,
                        Description = "EFC Resolved Successfully"
                    });

                    TempData["MESSAGE"] = "EFC has been Added successfully.";
                    TempData["KEY"] = "success";

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var error = ex.InnerException.InnerException.Message;
                    ViewBag.EFCTypeId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "EFC", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false), "Id", "Code");
                    model.EFCTypes = (await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "EFC").ConfigureAwait(false)).ToList();

                    TempData["MESSAGE"] = error;
                    TempData["KEY"] = "danger";
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                     .Where(y => y.Count > 0)
                     .ToList();
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                TempData["MESSAGE"] = $"{message}";
                TempData["KEY"] = "danger";
            }

            var progOfficer = User.GetProgOfficerSummary();
            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                    "Name", progOfficer.CountyId);
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetSearchableQueryable<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetSearchableQueryable<County>(), "Id",
                        "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetSearchableQueryable<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }

            ViewBag.SourceId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Change Source", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false), "Id", "Description", model.SourceId);
            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description", model.ReportedBySexId);
            ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetSearchableQueryable<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", model.ReportedByTypeId);
            // ViewBag.EFCTypeId = new SelectList(await GenericService.GetSearchableQueryable<SystemCodeDetail>(x
            // => x.SystemCode.Code == "EFC", n => n.OrderBy(i => i.OrderNo)).ConfigureAwait(false),
            // "Id", "Description", model.EFCTypeId);

            ViewBag.EFCTypeId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "EFC", n => n.OrderBy(i => i.Name)).ConfigureAwait(false), "Id", "Name");

            return View(model);
        }

        [GroupCustomAuthorize(Name = "EFC:VIEW")]
        public FileResult Download(int id)
        {
            var file = GenericService.GetOne<EFCDocument>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            Response.AppendHeader("content-disposition", "attachment; filename=" + file.FileName);
            var fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();

            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";

            Response.WriteFile(fileName);
            Response.Flush();
            Response.End();

            return null;
        }

        [GroupCustomAuthorize(Name = "EFC:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var efc = await GenericService.GetOneAsync<EFC>(c => c.Id == id,
                includeProperties: "ReportedBySex,Status,EFCType,Constituency,ReportedByType,Source,ReportedBySex,ClosedByUser").ConfigureAwait(false);
            if (efc == null)
            {
                return HttpNotFound();
            }

            efc.EFCNotes = (await GenericService.GetSearchableQueryable<EFCNote>(n => n.EFCId == efc.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ConfigureAwait(false)).ToList();

            efc.EFCDocuments = (await GenericService.GetSearchableQueryable<EFCDocument>(n => n.EFCId == efc.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ConfigureAwait(false)).ToList();

            ViewBag.EFCTypeId = new SelectList(await GenericService.GetSearchableQueryable<CaseCategory>(x => x.CaseType.Code == "EFC", n => n.OrderBy(i => i.Name)).ConfigureAwait(false), "Id", "Name");

            return View(efc);
        }

        #region Follow Up

        [GroupCustomAuthorize(Name = "EFC:FOLLOW UP")]
        public ActionResult FollowUp(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new EFCActionViewModel {
                Id = id.Value,
                EFC = GenericService.GetOne<EFC>(c => c.Id == id, includeProperties: "ReportedBySex,Status,EFCType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "EFC:FOLLOW UP")]
        public async Task<ActionResult> FollowUp(EFCActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var efc = await GenericService.GetOneAsync<EFC>(c => c.Id == model.Id).ConfigureAwait(false);
                var followUpCaytegory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Follow Up" && x.SystemCode.Code == "Case Status").ConfigureAwait(false);
                var efcNote = new EFCNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    EFCId = efc.Id,
                    Note = model.Notes,
                    CategoryId = followUpCaytegory.Id,
                };

                GenericService.AddOrUpdate(efcNote);
                var efcDocument = new EFCDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    efcDocument = new EFCDocument {
                        EFCId = efc.Id,
                        CreatedOn = DateTime.UtcNow,

                        CategoryId = followUpCaytegory.Id,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(efcDocument);
                    file.SaveAs(path + fileName);
                }
                // await GenericService.SaveAsync().ConfigureAwait(false);

                _logService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = efc.Id,
                    TableName = "EFC",
                    ModuleRightCode = "EFC:FOLLOW UP",
                    Record = $"[{JsonConvert.SerializeObject(efc)},{JsonConvert.SerializeObject(efcNote)},{JsonConvert.SerializeObject(efcDocument)}]",
                    WasSuccessful = true,
                    Description = "EFC  Followed Up  Successfully"
                });

                TempData["MESSAGE"] = "EFC Followed Up Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on Follow Up on  the EFC";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Follow Up

        #region Close

        [GroupCustomAuthorize(Name = "EFC:CLOSE")]
        public ActionResult Close(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new EFCActionViewModel {
                Id = id.Value,
                EFC = GenericService.GetOne<EFC>(c => c.Id == id, includeProperties: "ReportedBySex,Status,EFCType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "EFC:CLOSE")]
        public async Task<ActionResult> Close(EFCActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var closeCategory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Closed" && x.SystemCode.Code == "Case Status").ConfigureAwait(false);

                var efc = await GenericService.GetOneAsync<EFC>(c => c.Id == model.Id).ConfigureAwait(false);
                efc.ClosedBy = userId;
                efc.ClosedOn = DateTime.Now;
                efc.StatusId = closeCategory.Id;
                GenericService.AddOrUpdate(efc);
                var efcNote = new EFCNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = userId,
                    EFCId = efc.Id,
                    Note = model.Notes,
                    CategoryId = closeCategory.Id,
                };
                GenericService.AddOrUpdate(efcNote);
                var efcDocument = new EFCDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;

                    file.SaveAs(path + fileName);

                    efcDocument = new EFCDocument {
                        EFCId = efc.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = closeCategory.Id,
                        CreatedById = userId,
                        FileName = fileName,
                        FilePath = path
                    };

                    GenericService.AddOrUpdate(efcDocument);
                }

                var users = (await GenericService.GetSearchableQueryable<ChangeNote>(x => x.ChangeId == model.Id && x.Category.Code != "Open", null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct();
                foreach (var user in users)
                {
                    await EmailService.SendAsync(
                        new EFCNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"EFC #{model.Id} Closed",
                            Title = $"EFC #{model.Id} Closed",
                            Action = "Closed",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }).ConfigureAwait(true);
                }

                _logService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = efc.Id,
                    TableName = "EFC",
                    ModuleRightCode = "EFC:FOLLOW UP",
                    Record = $"[{JsonConvert.SerializeObject(efc)},{JsonConvert.SerializeObject(efcNote)},{JsonConvert.SerializeObject(efcDocument)}]",
                    WasSuccessful = true,
                    Description = "EFC  Followed Up Successfully"
                });

                TempData["MESSAGE"] = "EFC Closed Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on EFC Closure";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Close
    }
}