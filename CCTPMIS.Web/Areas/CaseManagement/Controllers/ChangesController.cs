﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Email;
using CCTPMIS.Services;
using DocumentFormat.OpenXml.Drawing;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;
using Path = System.IO.Path;

namespace CCTPMIS.Web.Areas.CaseManagement.Controllers
{
    [Authorize]
    [RouteArea("CaseManagement")]
    public class ChangesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;
        public IEmailService EmailService;

        public ChangesController(IGenericService genericService, IEmailService emailService, ILogService logService)
        {
            GenericService = genericService;
            EmailService = emailService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public async Task<ActionResult> Index(ChangeListViewModel vm)
        {
            var progOfficer = User.GetProgOfficerSummary();
            List<int> constituencies = null;

            if (progOfficer.ConstituencyId != null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId).ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.ConstituencyId == null && progOfficer.CountyId != null)
            {
                constituencies = (await GenericService.GetAsync<Constituency>(x => x.CountyId == progOfficer.CountyId).ConfigureAwait(false))
                    .Select(x => x.Id).ToList();
            }

            if (progOfficer.ConstituencyId == null && progOfficer.CountyId == null)
            {
                constituencies = (await GenericService.GetAllAsync<Constituency>().ConfigureAwait(false)).Select(x => x.Id).ToList();
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.CountyId == progOfficer.CountyId.Value).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>().ConfigureAwait(false), "Id", "Name", vm.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(x => x.CountyId == vm.CountyId).ConfigureAwait(false), "Id", "Name", vm.ConstituencyId);
            }

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            Expression<Func<Change, bool>> filter = x => x.Id > 0;
            Expression<Func<Change, bool>> filterx = null;

            var page = vm.Page ?? 1;

            if (!string.IsNullOrEmpty(vm.Name))
            {
                filterx = y =>
                    y.HhEnrolment.Household.HouseholdMembers.Any(
                        x => x.Person.FirstName.Contains(vm.Name.Trim()))
                    || y.HhEnrolment.Household.HouseholdMembers.Any(
                        x => x.Person.MiddleName.Contains(vm.Name.Trim()))
                    || y.HhEnrolment.Household.HouseholdMembers.Any(
                        x => x.Person.FirstName.Contains(vm.Name.Trim()));
                filter = filter.And(filterx);
            }
            if (vm.HouseholdEnrolmentId != null)
            {
                filterx = c => c.HhEnrolmentId == vm.HouseholdEnrolmentId;
                filter = filter.And(filterx);
            }
            if (vm.ChangeStatusId != null)
            {
                filterx = c => c.ChangeStatusId == vm.ChangeStatusId;
                filter = filter.And(filterx);
            }
            if (vm.ChangeTypeId != null)
            {
                filterx = c => c.ChangeTypeId == vm.ChangeTypeId;

                filter = filter.And(filterx);
            }
            if (vm.NationalId != null)
            {
                filterx = c => c.HhEnrolment.Household.HouseholdMembers.Any(x => x.Person.NationalIdNo == vm.NationalId.Trim());
                filter = filter.And(filterx);
            }
            if (vm.CountyId != null)
            {
                filterx = c => c.Constituency.CountyId == vm.CountyId;
                filter = filter.And(filterx);
            }
            if (vm.ConstituencyId != null)
            {
                filterx = c => c.ConstituencyId == vm.ConstituencyId;
                filter = filter.And(filterx);
            }

            filterx = x => constituencies.Contains(x.ConstituencyId);
            filter = filter.And(filterx);

            var pageSize = vm.PageSize ?? 20;
            var pagenm = vm.Page ?? 1;

            vm.Changes = (await GenericService.GetSearchableQueryable<Change>(filter, x => x.OrderByDescending(y => y.Id), "ChangeStatus,ChangeType,Programme,ChangeLevel,HhEnrolment,CreatedByUser").ConfigureAwait(false)).ToPagedList(pagenm, pageSize);

            ViewBag.ChangeTypeId = new SelectList(db.CaseCategories.Where(x => x.CaseType.Code == "UPDATE"), "Id", "Description", vm.ChangeTypeId);
            ViewBag.ChangeStatusId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Status"), "Id", "Description", vm.ChangeStatusId);

            ViewBag.PageSize = this.GetPager(vm.PageSize);
            return View(vm);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public async Task<ActionResult> Beneficiaries(CaseFilterViewModel model)
        {
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            if (model.HouseholdEnrolmentId != null || model.NationalId != null || model.ProgrammeNo != null || !string.IsNullOrEmpty(model.Name) ||
                model.ConstituencyId != null || model.CountyId != null)
            {
                var enrollmentDetails = db.HouseholdEnrolments.AsNoTracking()
                                            .Include(b => b.Household.Status)
                                            .Include(b => b.Household.Programme)
                                            .Include(b => b.Household.Programme.BeneficiaryType)
                                            .Include(b => b.Household.Programme.PrimaryRecipient)
                                            .Include(b => b.Household.Programme.SecondaryRecipient)
                                            .Include(b => b.Household.HouseholdMembers.Select(x => x.Person))
                                            .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                                            .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                                            .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship))
                                            .Include(b => b.Household.HouseholdMembers.Select(x => x.MemberRole));

                if (model.HouseholdEnrolmentId != null)
                {
                    enrollmentDetails = enrollmentDetails.Where(b => b.Id == model.HouseholdEnrolmentId);
                }

                if (model.ProgrammeNo != null)
                {
                    if (model.ProgrammeNo.Length > 6)
                    {
                        var prefix = model.ProgrammeNo.Substring(0, 1);
                        var beneNo = int.Parse(model.ProgrammeNo.Substring(1, 6));

                        enrollmentDetails = enrollmentDetails.Where(b => b.BeneProgNoPrefix == prefix && b.ProgrammeNo == beneNo);
                    }
                }

                if (model.NationalId != null)
                {
                    enrollmentDetails = enrollmentDetails.Where(b => b.Household.HouseholdMembers.Any(x => x.Person.NationalIdNo == model.NationalId));
                }

                if (!string.IsNullOrEmpty(model.Name))
                {
                    enrollmentDetails = enrollmentDetails.Where(
                        b => b.Household.HouseholdMembers.Any(x => x.Person.FirstName.Contains(model.Name.Trim())) ||
                             b.Household.HouseholdMembers.Any(x => x.Person.MiddleName.Contains(model.Name.Trim())) ||
                             b.Household.HouseholdMembers.Any(x => x.Person.Surname.Contains(model.Name.Trim())));
                }

                if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                            "Name", progOfficer.CountyId);

                    ViewBag.SubLocationId =
                        new SelectList(
                            await GenericService.GetAsync<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                            "Id", "Name");
                }
                if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
                {
                    ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetAsync<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
                }

                if (!progOfficer.CountyId.HasValue)
                {
                    ViewBag.CountyId =
                        new SelectList(await GenericService.GetAsync<County>(), "Id",
                            "Name", progOfficer.CountyId);
                    ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                    ViewBag.SubLocationId = new SelectList(await GenericService.GetAsync<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
                }

                var page = model.Page ?? 1;
                var pageSize = model.PageSize ?? 10;
                model.HouseholdEnrolments = enrollmentDetails.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
            }

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id",
                        "Name", progOfficer.CountyId);

                ViewBag.SubLocationId =
                    new SelectList(
                        await GenericService.GetAsync<SubLocation>(c => c.Constituency.CountyId == progOfficer.ConstituencyId),
                        "Id", "Name");
            }
            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
                ViewBag.SubLocationId = new SelectList(await GenericService.GetAsync<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId.Value), "Id", "Name");
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId =
                    new SelectList(await GenericService.GetAsync<County>(), "Id",
                        "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
                ViewBag.SubLocationId = new SelectList(await GenericService.GetAsync<SubLocation>(c => c.ConstituencyId == progOfficer.ConstituencyId), "Id", "Name");
            }

            ViewBag.ComplaintTypeId = (await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "UPDATE", n => n.OrderBy(i => i.Name)).ConfigureAwait(false)).ToList();

            /*
            ViewBag.ComplaintTypeId = (await GenericService.GetAsync<CaseCategory>(x => x.CaseType.Code == "UPDATE" && x.Code != "HH_MEMBER_EXIT", n => n.OrderBy(i => i.Name)).ConfigureAwait(false)).ToList();
            ViewBag.ComplaintMemberTypeId = (await GenericService.GetAsync<CaseCategory>(x => x.Code == "HH_MEMBER_EXIT", n => n.OrderBy(i => i.Name)).ConfigureAwait(false)).ToList();
            **/
            return View(model);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public ActionResult Person(int id)
        {
            Person person = db.Persons.SingleOrDefault(x => x.Id == id);

            return View(person);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public ActionResult HouseholdSummary(int id)
        {
            var spName = "GetHouseholdInfo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id), } };
            var caseIssues = GenericService.GetOneBySp<GetBeneInfoVm>(spName, parameterNames, parameterList);
            return View(caseIssues);
        }

        [GroupCustomAuthorize(Name = "UPDATES:ENTRY")]
        [Route("changes/create/{id}/{changeTypeId?}", Name = "ChangeCreate")]
        public async Task<ActionResult> Create(int id, int? changeTypeId)
        {
            // var change = (await GenericService.GetAsync<Change>(x => x.HhEnrolmentId == id  && x.ChangeTypeId==changeTypeId).ConfigureAwait(false)).FirstOrDefault();

            var progOfficer = User.GetProgOfficerSummary();
            var spName = "GetHouseholdInfo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id), } };
            var caseIssues = GenericService.GetOneBySp<GetBeneInfoVm>(spName, parameterNames, parameterList);


            var hhEnrolment = db.HouseholdEnrolments.AsNoTracking()
                .Include(b => b.Household.Status)
                .Include(b => b.Household.Programme)
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Person))
                .SingleOrDefault(b => b.Id == id);

            if (hhEnrolment == null)
            {
                return HttpNotFound();
            }

            var vm = new ChangeViewModel {
                HouseholdEnrolment = hhEnrolment,
                GetBeneInfoVm = caseIssues
            };
            var changeTypes = db.CaseCategories.Where(x => x.CaseType.Code == "UPDATE");

            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");
           ViewBag.ChangeTypeId = new SelectList(changeTypes.ToList(), "Id", "Name", changeTypeId);
            vm.HhEnrolmentId = hhEnrolment.Id;

            ViewBag.UpdatedRelationshipId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Relationship").ConfigureAwait(true), "Id", "Description");
            ViewBag.UpdatedRoleId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role").ConfigureAwait(true), "Id", "Description");

            ViewBag.UpdatedSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description");

            ViewBag.CgSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description");

            ViewBag.BenSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description");

            spName = "GetPersonByEnrolmentNo";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id), } };
            var persons = GenericService.GetManyBySp<Person>(spName, parameterNames, parameterList);

            ViewBag.PersonId = new SelectList(persons, "Id", "DisplayName");

            vm.ChangeTypeId = changeTypeId;
            if (changeTypeId != null)
            {
                vm.ChangeType = db.CaseCategories.SingleOrDefault(x => x.Id == changeTypeId);
            }

            if (vm.ChangeType.Code == "BENEFICIARY_DEATH")
            {
                ViewBag.ChangeSourceId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Source" && x.Code != "Beneficiary"), "Id", "Description");
                ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By" && x.Code != "Beneficiary" && x.Code != "Anonymous").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            }
            else
            {
                ViewBag.ChangeSourceId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Source"), "Id", "Description");
                ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By" && x.Code != "Anonymous").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");
            }

            if (vm.ChangeType.Code == "HH_MEMBER_EXIT")
            {
                spName = "GetPersonByEnrolmentNo";
                parameterNames = "@Id,@Type";
                parameterList = new List<ParameterEntity> {
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id), }
                    ,new ParameterEntity { ParameterTuple = new Tuple<string, object>("Type", "1"), }
                };
                persons = GenericService.GetManyBySp<Person>(spName, parameterNames, parameterList);
                ViewBag.PersonId = new SelectList(persons.ToList(), "Id", "DisplayName");
            }


            if (vm.ChangeType.Code == "HHM_CORRECT")
            {
                spName = "GetPersonByEnrolmentNo";
                parameterNames = "@Id,@Type";
                parameterList = new List<ParameterEntity> {
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", id), }
                    ,new ParameterEntity { ParameterTuple = new Tuple<string, object>("Type", "1"), }
                };
                persons = GenericService.GetManyBySp<Person>(spName, parameterNames, parameterList);
                
            }

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
            }

            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", progOfficer.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(), "Id", "Name", progOfficer.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name");
            }

            var pendingChange = db.Changes.Where(c => c.HhEnrolmentId == id && c.ChangeTypeId == changeTypeId && (c.ClosedBy == null && c.RejectedBy == null));

            if (pendingChange.Any())
            {
                TempData["MESSAGE"] = "There is a similar change already logged for this beneficiary which is not yet Approved/Rejected.";
                TempData["KEY"] = "danger";
                return RedirectToAction("Beneficiaries");
            }

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            spName = "GetProgOfficerLocations";
            parameterNames = "@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
          {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var locations = GenericService.GetManyBySp<Location>(spName, parameterNames, parameterList).ToList();

            ViewBag.UpdatedSubLocationId = new SelectList(locations,
                "Id",
                "Name");

            return View(vm);
        }

        [GroupCustomAuthorize(Name = "UPDATES:ENTRY")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("changes/create/{id}/{changeTypeId?}", Name = "ChangeCreateSave")]
        public async Task<ActionResult> Create(ChangeViewModel model, HttpPostedFileBase file, HttpPostedFileBase familyResolutionDocument)
        {
            var change = new Change();
            var userId = int.Parse(User.Identity.GetUserId());
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(model, change);

            var ChangeType = db.CaseCategories.SingleOrDefault(x => x.Id == change.ChangeTypeId);
            if (ModelState.IsValid)
            {
                var pendingChange = db.Changes.Where(c => c.HhEnrolmentId == model.HhEnrolmentId && c.ChangeTypeId == model.ChangeTypeId &&   (c.ClosedBy == null && c.RejectedBy == null));

                if (pendingChange.Any())
                {
                    TempData["MESSAGE"] = "There is a similar change already logged for this beneficiary which is not yet Approved/Rejected.";
                    TempData["KEY"] = "danger";
                }
                else
                {
                    if (model.NotificationDate < model.DeathDate)
                    {
                        TempData["MESSAGE"] = "There Death Notification Date cannot be before death date.";
                        TempData["KEY"] = "danger";
                        goto ExitProcess;
                    }

                    var changeStatusId = db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Open" && x.SystemCode.Code == "Change Status").Id;

                    var levelId = db.SystemCodeDetails.SingleOrDefault(x => x.Code == "Sub County" && x.SystemCode.Code == "Escalation Levels").Id; ;
                    change.DeathNotificationDate = model.NotificationDate;
                    change.CreatedBy = userId;
                    change.CreatedOn = DateTime.Now;
                    change.ChangeStatusId = changeStatusId;
                    change.ChangeLevelId = levelId;
                    GenericService.AddOrUpdate(change);

                    if (!string.IsNullOrEmpty(model.Notes))
                    {
                        var note = new ChangeNote {
                            ChangeId = change.Id,
                            Note = model.Notes,
                            CreatedOn = DateTime.Now,
                            CreatedById = userId,
                            CategoryId = changeStatusId
                        };
                        db.ChangeNotes.Add(note);
                        db.SaveChanges();
                    }

                    if (file != null && file.ContentLength > 0)
                    {
                        var path = "";

                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "-";
                        fileName = fileName + file.FileName;
                        path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                        file.SaveAs(path + fileName);

                        var changeDocument = new ChangeDocument {
                            ChangeId = change.Id,
                            CreatedOn = DateTime.UtcNow,
                            CategoryId = change.ChangeStatusId,
                            CreatedById = userId,
                            FileName = fileName,
                            FilePath = path
                        };
                        GenericService.AddOrUpdate(changeDocument);
                    }

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = change.Id,
                        TableName = "Change",
                        ModuleRightCode = "UPDATES:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(change)}",
                        WasSuccessful = true,
                        Description = "Change created Successfully"
                    });

                    TempData["Message"] = "Change Request has been logged successfully.";
                    TempData["Key"] = "success";
                    return RedirectToAction("Index");
                    // return RedirectToAction("Details", new { id = change.Id });
                }
            }

        ExitProcess:
            var vm = new ChangeViewModel {
                HouseholdEnrolment = db.HouseholdEnrolments.AsNoTracking()
                .Include(b => b.Household.Status)
                .Include(b => b.Household.Programme)
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Person.Sex))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Status))
                .Include(b => b.Household.HouseholdMembers.Select(x => x.Relationship))
                .SingleOrDefault(b => b.Id == model.Id)
            };

            vm.HhEnrolmentId = vm.HouseholdEnrolment.Id;

            var progOfficer = User.GetProgOfficerSummary();

            var spName = "GetPersonByEnrolmentNo";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", model.Id), } };
            var persons = GenericService.GetManyBySp<Person>(spName, parameterNames, parameterList);

            ViewBag.PersonId = new SelectList(persons, "Id", "FirstName");

            spName = "GetHouseholdInfo";
            parameterNames = "@Id";
            parameterList = new List<ParameterEntity> { new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", model.Id), } };
            vm.GetBeneInfoVm = GenericService.GetOneBySp<GetBeneInfoVm>(spName, parameterNames, parameterList);

            ViewBag.UpdatedRelationshipId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Relationship")
                    .ConfigureAwait(true),
                "Id",
                "Description", model.UpdatedRelationshipId);

            ViewBag.UpdatedRoleId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Member Role").ConfigureAwait(true), "Id", "Description", model.UpdatedRoleId);

            ViewBag.UpdatedSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description", model.UpdatedSexId);

            ViewBag.CgSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description", model.CgSexId);

            ViewBag.BenSexId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex")
                    .ConfigureAwait(true),
                "Id",
                "Description", model.BeneSexId);
            ViewBag.UpdatedSubLocationId = new SelectList(
                await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "SubLocation")
                    .ConfigureAwait(true),
                "Id",
                "Description", model.UpdatedSexId);

            ViewBag.ReportedBySexId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Sex").ConfigureAwait(false), "Id", "Description");
            ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description");

            ViewBag.ChangeTypeId = new SelectList(db.CaseCategories.Where(x => x.CaseType.Code == "UPDATE"), "Id", "Name", change.ChangeTypeId);
            ViewBag.ChangeSourceId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Source"), "Id", "Description", change.ChangeSourceId);

            ViewBag.HasCountyId = progOfficer.CountyId.HasValue;
            ViewBag.HasConstituencyId = progOfficer.ConstituencyId.HasValue;

            if (progOfficer.CountyId.HasValue && !progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", model.CountyId);
            }

            if (progOfficer.CountyId.HasValue && progOfficer.ConstituencyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(x => x.Id == progOfficer.CountyId.Value), "Id", "Name", model.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.Id == progOfficer.ConstituencyId.Value), "Id", "Name", model.ConstituencyId);
            }

            if (!progOfficer.CountyId.HasValue)
            {
                ViewBag.CountyId = new SelectList(await GenericService.GetAsync<County>(), "Id", "Name", model.CountyId);
                ViewBag.ConstituencyId = new SelectList(await GenericService.GetAsync<Constituency>(c => c.CountyId == progOfficer.CountyId), "Id", "Name", model.ConstituencyId);
            }

            if (change.ChangeTypeId > 0)
            {
                vm.ChangeType = db.CaseCategories.SingleOrDefault(x => x.Id == change.ChangeTypeId);
            }

            if (vm.ChangeType.Code == "BENEFICIARY_DEATH")
            {
                ViewBag.ChangeSourceId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Source" && x.Code != "Beneficiary"), "Id", "Description", vm.ChangeSourceId);
                ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By" && x.Code != "Beneficiary" && x.Code != "Anonymous").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", vm.ReportedByTypeId);
            }
            else
            {
                ViewBag.ChangeSourceId = new SelectList(db.SystemCodeDetails.Where(x => x.SystemCode.Code == "Change Source"), "Id", "Description", vm.ChangeSourceId);
                ViewBag.ReportedByTypeId = new SelectList((await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Case Report By" && x.Code != "Anonymous").ConfigureAwait(false)).OrderBy(x => x.OrderNo), "Id", "Description", vm.ReportedByTypeId);
            }

            if (vm.ChangeType.Code == "HHM_CORRECT")
            {
                spName = "GetPersonByEnrolmentNo";
                parameterNames = "@Id,@Type";
                parameterList = new List<ParameterEntity> {
                    new ParameterEntity { ParameterTuple = new Tuple<string, object>("Id", vm.HhEnrolmentId), }
                    ,new ParameterEntity { ParameterTuple = new Tuple<string, object>("Type", "1"), }
                };
                persons = GenericService.GetManyBySp<Person>(spName, parameterNames, parameterList);

                ViewBag.UpdatedPersonId = new SelectList(persons, "Id", "DisplayName", vm.UpdatedPersonId);
            }

            return View(vm);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var change = GenericService.GetOne<Change>(c => c.Id == id,
                includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType,Constituency,ReportedByType,HhEnrolment,ClosedByUser,VerifiedByUser,ChangeSource,Programme,ChangeLevel,Constituency,UpdatedSubLocation,UpdatedSex,UpdatedRelationship,UpdatedPerson,Sex,UpdatedRole,Relationship,ChangeStatus,ChangeSource,ChangeLevel,CgSex,BulkTransferByUser");
            if (change == null)
            {
                return HttpNotFound();
            }

            change.ChangeNotes = GenericService.Get<ChangeNote>(n => n.ChangeId == change.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ToList();

            change.ChangeDocuments = GenericService.Get<ChangeDocument>(n => n.ChangeId == change.Id,
                includeProperties: "CreatedBy,Category",
                orderBy: n => n.OrderByDescending(i => i.Id)).ToList();

            return View(change);
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public ActionResult Preview(int id)
        {
            var file = this.GenericService.GetOne<ChangeDocument>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            string fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();
            byte[] FileBytes = System.IO.File.ReadAllBytes(fileName);

            Response.AppendHeader("content-disposition", "inline; filename=" + file.FileName);
            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";
            Response.Write("<script>window.open('" + fileName + "','_blank');</script>");

            return File(FileBytes, "application/pdf");
        }

        [GroupCustomAuthorize(Name = "UPDATES:VIEW")]
        public FileResult Download(int id)
        {
            var file = this.GenericService.GetOne<ChangeDocument>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            Response.AppendHeader("content-disposition", "attachment; filename=" + file.FileName);
            string fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();

            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";

            Response.WriteFile(fileName);
            Response.Flush();
            Response.End();

            return null;
        }

        #region Verify

        [GroupCustomAuthorize(Name = "UPDATES:VERIFY")]
        public ActionResult Accept(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Change = GenericService.GetOne<Change>(c => c.Id == id, includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:VERIFY")]
        public async Task<ActionResult> Accept(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var change = await GenericService.GetOneAsync<Change>(c => c.Id == model.Id && c.ChangeStatus.Code == "Open").ConfigureAwait(false);
                if (change != null)
                {
                    change.ChangeStatusId = (await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Accepted" && x.SystemCode.Code == "Change Status").ConfigureAwait(false)).Id;
                    change.VerifiedBy = userId;
                    change.VerifiedOn = DateTime.Now;
                    GenericService.AddOrUpdate(change);

                    var ChangeNote = new ChangeNote {
                        CreatedOn = DateTime.Now,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        ChangeId = change.Id,
                        Note = model.Notes,
                        CategoryId = change.ChangeStatusId,
                    };
                    GenericService.AddOrUpdate(ChangeNote);

                    var ChangeDocument = new ChangeDocument();
                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                        var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                        ChangeDocument = new ChangeDocument {
                            ChangeId = change.Id,
                            CreatedOn = DateTime.UtcNow,

                            CategoryId = change.ChangeStatusId,
                            CreatedById = int.Parse(User.Identity.GetUserId()),
                            FileName = fileName,
                            FilePath = path
                        };
                        GenericService.AddOrUpdate(ChangeDocument);
                        file.SaveAs(path + fileName);
                    }

                    var users = (await GenericService.GetAsync<ChangeNote>(x => x.ChangeId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                    foreach (var user in users)
                    {
                        var tEmail = new Thread(() => EmailService.SendAsync(
                            new ChangeNotificationEmail {
                                FirstName = user.DisplayName,
                                To = user.Email,
                                Subject = $"Update #{model.Id} Accepted",
                                Title = $"Update #{model.Id} Accepted",
                                Action = "Accepted",
                                Id = model.Id,
                                ActionDate = DateTime.Now.ToString("F")
                            }));
                        tEmail.Start();
                    }

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = change.Id,
                        TableName = "Change",
                        ModuleRightCode = "UPDATES:VERIFY",
                        Record = $"[{JsonConvert.SerializeObject(change)},{JsonConvert.SerializeObject(ChangeNote)},{JsonConvert.SerializeObject(ChangeDocument)}]",
                        WasSuccessful = true,
                        Description = "Change Accepted Successfully"
                    });
                    TempData["MESSAGE"] = "Case Update Accepted Successfully";
                    TempData["KEY"] = "success";
                }
                else
                {
                    TempData["MESSAGE"] = "Case Update is not in the Open Stage to be Accepted";
                    TempData["KEY"] = "danger";
                }
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error on Accepting the Update" + e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        [GroupCustomAuthorize(Name = "UPDATES:VERIFY")]
        public ActionResult Reject(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Change = GenericService.GetOne<Change>(c => c.Id == id, includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:VERIFY")]
        public async Task<ActionResult> Reject(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var change = await GenericService.GetOneAsync<Change>(c => c.Id == model.Id).ConfigureAwait(false);
                change.ChangeStatusId = (await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "REJECTED" && x.SystemCode.Code == "Change Status").ConfigureAwait(false)).Id;
                change.RejectedBy = userId;
                change.RejectedOn = DateTime.Now;
                GenericService.AddOrUpdate(change);

                var ChangeNote = new ChangeNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    ChangeId = change.Id,
                    Note = model.Notes,
                    CategoryId = change.ChangeStatusId,
                };
                GenericService.AddOrUpdate(ChangeNote);
                var ChangeDocument = new ChangeDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    ChangeDocument = new ChangeDocument {
                        ChangeId = change.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = change.ChangeStatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(ChangeDocument);
                    file.SaveAs(path + fileName);
                }

                var person = (await GenericService.GetOneAsync<HouseholdEnrolment>(x => x.Id == change.HhEnrolmentId).ConfigureAwait(false)); 

                var users = (await GenericService.GetAsync<ChangeNote>(x => x.ChangeId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ChangeNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Update #{model.Id} Rejected",
                            Title = $"Update Rejected (ProgrammeNo #{person.ProgrammeNumber}  | EnrolmentNo #{change.HhEnrolmentId})",
                            Action = "Rejected",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = change.Id,
                    TableName = "Change",
                    ModuleRightCode = "UPDATES:VERIFY",
                    Record = $"[{JsonConvert.SerializeObject(change)},{JsonConvert.SerializeObject(ChangeNote)},{JsonConvert.SerializeObject(ChangeDocument)}]",
                    WasSuccessful = true,
                    Description = "Change Rejected Successfully"
                });

                TempData["MESSAGE"] = "Change has been Rejected";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on Rejecting the Update";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Verify

        #region Verify

        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public ActionResult Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Change = GenericService.GetOne<Change>(c => c.Id == id, includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:APPROVAL")]
        public async Task<ActionResult> Approve(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var spName = "ApproveChangeRequest";
                var parameterNames = "@Id,@UserId";
                var parameterList = new List<ParameterEntity>
                {
                     new ParameterEntity { ParameterTuple = new Tuple<string, object> ("Id", model.Id), },
                     new ParameterEntity { ParameterTuple = new Tuple<string, object> ("UserId", userId), }
                };
                var change = GenericService.GetOneBySp<Change>(spName, parameterNames, parameterList);

                var ChangeNote = new ChangeNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    ChangeId = change.Id,
                    Note = model.Notes,
                    CategoryId = change.ChangeStatusId,
                };
                GenericService.AddOrUpdate(ChangeNote);
                var changeDocument = new ChangeDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;
                    var path = $"{WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]}{WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"]}";
                    changeDocument = new ChangeDocument {
                        ChangeId = change.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = change.ChangeStatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(changeDocument);
                    file.SaveAs(path + fileName);
                }

                var users = (await GenericService.GetAsync<ChangeNote>(x => x.ChangeId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ChangeNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Update #{model.Id} Approved",
                            Title = $"Update #{model.Id} Approved",
                            Action = "Approved",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = change.Id,
                    TableName = "Change",
                    ModuleRightCode = "UPDATES:APPROVAL",
                    Record = $"[{JsonConvert.SerializeObject(change)},{JsonConvert.SerializeObject(ChangeNote)},{JsonConvert.SerializeObject(changeDocument)}]",
                    WasSuccessful = true,
                    Description = "Change Approved Successfully"
                });

                TempData["MESSAGE"] = "Case Update Approved Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception exc)
            {
                TempData["MESSAGE"] = $"Error on Approving the Update \n {exc?.InnerException?.Message}";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Verify

        #region Escalate

        [GroupCustomAuthorize(Name = "UPDATES:ESCALATE")]
        public ActionResult Escalate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Change = GenericService.GetOne<Change>(c => c.Id == id, includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:ESCALATE")]
        public async Task<ActionResult> Escalate(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var change = await GenericService.GetOneAsync<Change>(c => c.Id == model.Id)
                    .ConfigureAwait(false);
                var currentLevel = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Id == change.ChangeLevelId);

                change.ChangeLevelId = (await GenericService
                        .GetOneAsync<SystemCodeDetail>(x =>
                            x.Code == "County" && x.SystemCode.Code == "Escalation Levels")
                        .ConfigureAwait(false))
                    .Id;
                change.EscalatedBy = userId;
                change.EscalatedOn = DateTime.Now;
                change.ChangeStatusId = (await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Escalated" && x.SystemCode.Code == "Change Status").ConfigureAwait(false)).Id;

                GenericService.AddOrUpdate(change);
                var ChangeNote = new ChangeNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = int.Parse(User.Identity.GetUserId()),
                    ChangeId = change.Id,
                    Note = model.Notes,
                    CategoryId = change.ChangeStatusId,
                };
                GenericService.AddOrUpdate(ChangeNote);
                var changeDocument = new ChangeDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = "";
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "-";
                    fileName = fileName + file.FileName;
                    path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    file.SaveAs(path + fileName);

                    changeDocument = new ChangeDocument {
                        ChangeId = change.Id,
                        CreatedOn = DateTime.UtcNow,

                        CategoryId = change.ChangeStatusId,
                        CreatedById = int.Parse(User.Identity.GetUserId()),
                        FileName = fileName,
                        FilePath = path
                    };
                    GenericService.AddOrUpdate(changeDocument);
                }

                var users = (await GenericService.GetAsync<ChangeNote>(x => x.ChangeId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ChangeNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Update #{model.Id} Escalated",
                            Title = $"Update #{model.Id} Escalated",
                            Action = "Escalated",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = change.Id,
                    TableName = "Change",
                    ModuleRightCode = "UPDATES:ESCALATE",
                    Record = $"[{JsonConvert.SerializeObject(change)},{JsonConvert.SerializeObject(ChangeNote)},{JsonConvert.SerializeObject(changeDocument)}]",
                    WasSuccessful = true,
                    Description = "Change Escalated Successfully"
                });

                TempData["MESSAGE"] = "Update Escalated Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on Update Escalation for Bulk Transfer";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Escalate

        #region Close

        [GroupCustomAuthorize(Name = "UPDATES:CLOSE")]
        public ActionResult Close(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = new ComplaintActionViewModel {
                Id = id.Value,
                Change = GenericService.GetOne<Change>(c => c.Id == id, includeProperties: "ReportedBySex,ChangeStatus,ChangeLevel,ChangeType"),
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "UPDATES:CLOSE")]
        public async Task<ActionResult> Close(ComplaintActionViewModel model, HttpPostedFileBase file)
        {
            try
            {
                var userId = int.Parse(User.Identity.GetUserId());
                var closeCategory = await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "Closed" && x.SystemCode.Code == "Change Status").ConfigureAwait(false);

                var change = await GenericService.GetOneAsync<Change>(c => c.Id == model.Id).ConfigureAwait(false);
                change.ClosedBy = userId;
                change.ClosedOn = DateTime.Now;
                change.ChangeStatusId = closeCategory.Id;
                GenericService.AddOrUpdate(change);
                var ChangeNote = new ChangeNote {
                    CreatedOn = DateTime.Now,
                    CreatedById = userId,
                    ChangeId = change.Id,
                    Note = model.Notes,
                    CategoryId = closeCategory.Id,
                };
                GenericService.AddOrUpdate(ChangeNote);
                var changeDocument = new ChangeDocument();
                if (file != null && file.ContentLength > 0)
                {
                    var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] + WebConfigurationManager.AppSettings["DIRECTORY_CASEMANAGEMENT"];
                    var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + file.FileName;

                    file.SaveAs(path + fileName);

                    changeDocument = new ChangeDocument {
                        ChangeId = change.Id,
                        CreatedOn = DateTime.UtcNow,
                        CategoryId = closeCategory.Id,
                        CreatedById = userId,
                        FileName = fileName,
                        FilePath = path
                    };

                    GenericService.AddOrUpdate(changeDocument);
                }

                var users = (await GenericService.GetAsync<ChangeNote>(x => x.ChangeId == model.Id, null, "CreatedBy").ConfigureAwait(false)).Select(x => x.CreatedBy).Distinct().ToList();
                foreach (var user in users)
                {
                    var tEmail = new Thread(() => EmailService.SendAsync(
                        new ChangeNotificationEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = $"Update #{model.Id} Closed",
                            Title = $"Update #{model.Id} Closed",
                            Action = "Closed",
                            Id = model.Id,
                            ActionDate = DateTime.Now.ToString("F")
                        }));
                    tEmail.Start();
                }

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = change.Id,
                    TableName = "Change",
                    ModuleRightCode = "UPDATES:CLOSE",
                    Record = $"[{JsonConvert.SerializeObject(change)},{JsonConvert.SerializeObject(ChangeNote)},{JsonConvert.SerializeObject(changeDocument)}]",
                    WasSuccessful = true,
                    Description = "Change Closed Successfully"
                });

                TempData["MESSAGE"] = "Change Update Closed Successfully";
                TempData["KEY"] = "success";
            }
            catch (Exception)
            {
                TempData["MESSAGE"] = "Error on Change Update Closure";
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Details", new { id = model.Id });
        }

        #endregion Close

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}