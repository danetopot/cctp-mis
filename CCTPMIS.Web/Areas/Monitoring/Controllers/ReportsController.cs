﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Monitoring;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using PagedList;

namespace CCTPMIS.Web.Areas.Monitoring.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected readonly IGenericService GenericService;
        private readonly ILogService LogService;

        public ReportsController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:VIEW")]
        // GET: Monitoring/Reports
        public async Task<ActionResult> Index(MonitoringListViewModel vm, int? page)
        {
            Expression<Func<MonitoringReport, bool>> filter = x => x.Id > 0;
            Expression<Func<MonitoringReport, bool>> filterx = null;

            if (vm.StatusId != null)
            {
                filterx = x => x.StatusId == vm.StatusId;
                filter = filter.And(filterx);
            }

            if (vm.MonitoringReportCategoryId != null)
            {
                filterx = x => x.MonitoringReportCategoryId == vm.MonitoringReportCategoryId;
                filter = filter.And(filterx);
            }

            var pagenm = page ?? 1;
            ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCode.Code == "Monitoring Report Status").ConfigureAwait(false), "Id", "Code", vm.StatusId);
            ViewBag.MonitoringReportCategoryId = new SelectList(await GenericService.GetAsync<MonitoringReportCategory>(x => x.ApvOn != null).ConfigureAwait(false), "Id", "Name", vm.MonitoringReportCategoryId);
            vm.Reports = (await GenericService.GetAsync(filter, x => x.OrderByDescending(y => y.Id), includeProperties: "CreatedByUser,ModifiedByUser,ApvByUser,MonitoringReportCategory,Status").ConfigureAwait(false)).ToPagedList(pagenm, 20);
            return View(vm);
        }

        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:VIEW")]
        // GET: admin/Districts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monitoringReport = GenericService.GetOne<MonitoringReport>(
                x => x.Id == id.Value,
                "CreatedByUser,ModifiedByUser,ApvByUser,MonitoringReportCategory,Status");
            if (monitoringReport == null)
            {
                return HttpNotFound();
            }

            return View(monitoringReport);
        }

        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:ENTRY")]

        public async Task<ActionResult> Create()
        {
            // ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x =>
            // x.SystemCode.Code == "Monitoring Report Status").ConfigureAwait(false), "Id", "Code");
            ViewBag.MonitoringReportCategoryId = new SelectList(await GenericService.GetAsync<MonitoringReportCategory>(x => x.ApvOn != null).ConfigureAwait(false), "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:ENTRY")]
        public async Task<ActionResult> Create(MonitoringReportCreateModel model, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                var userId = int.Parse(User.Identity.GetUserId());
                if (upload != null && upload.ContentLength > 0)
                {
                    try
                    {
                        var fileName = DateTime.Now.ToString("yyyymmddhhmmss") + "_" + upload.FileName;
                        var path = WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"] +
                                   WebConfigurationManager.AppSettings["DIRECTORY_MONITORING"];

                        var createdStatus = (await GenericService.GetFirstAsync<SystemCodeDetail>(x =>
                            x.SystemCode.Code == "Monitoring Report Status" && x.Code == "Creation Approval")).Id;

                        var monitoring = new MonitoringReport {
                            FileName = fileName,
                            FilePath = path,
                            Name = model.Name,
                            Description = model.Description,
                            CreatedBy = userId,
                            CreatedOn = DateTime.Now,
                            StatusId = createdStatus
                        };
                        GenericService.AddOrUpdate(monitoring);
                        upload.SaveAs(path + fileName);

                        TempData["MESSAGE"] = "Report Uploaded Successfully";
                        TempData["KEY"] = "success";

                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        TempData["MESSAGE"] = "Error on  Uploading Report. Try Again Later. Select the File afresh";
                        TempData["KEY"] = "danger";
                    }
                }
            }

            // ViewBag.StatusId = new SelectList(await GenericService.GetAsync<SystemCodeDetail>(x =>
            // x.SystemCode.Code == "Monitoring Report Status").ConfigureAwait(false), "Id", "Code", model.StatusId);
            ViewBag.MonitoringReportCategoryId = new SelectList(await GenericService.GetAsync<MonitoringReportCategory>(x => x.ApvOn != null).ConfigureAwait(false), "Id", "Name", model.MonitoringReportCategoryId);
            return View(model);
        }

        public FileResult Download(int id)
        {
            var file = this.GenericService.GetOne<MonitoringReport>(x => x.Id == id);
            var fileName = $"{file.FilePath}{file.FileName}";
            Response.AppendHeader("content-disposition", "attachment; filename=" + file.FileName);
            string fileExt = Path.GetExtension(fileName).Split('.')[1].ToLower();

            Response.ContentType = fileExt == "pdf" ? "application/pdf" : "application/octet-stream";

            Response.WriteFile(fileName);
            Response.Flush();
            Response.End();

            return null;
        }

        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monitoringReport = await GenericService.GetOneAsync<MonitoringReport>(
                                       x => x.Id == id.Value,
                                       "MonitoringReportCategory,CreatedByUser,ModifiedByUser,ApvByUser,Status")
                                   .ConfigureAwait(true);

            if (monitoringReport == null)
            {
                return HttpNotFound();
            }

            return View(monitoringReport);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:APPROVAL")]
        public ActionResult ApproveConfirmed(int? id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveMonitoringReport";
            var parameterNames = "@Id,@UserId,@State";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                        new ParameterEntity
                                        {
                                            ParameterTuple =
                                                new Tuple<string, object>(
                                                    "State",
                                                    "APPROVE")
                                        },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                TempData["MESSAGE"] = $" The Monitoring Report was successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Details", "Reports", new { id = id });
        }

        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:VERIFY")]
        public async Task<ActionResult> Verify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monitoringReport = await GenericService.GetOneAsync<MonitoringReport>(
                                       x => x.Id == id.Value,
                                       "MonitoringReportCategory,CreatedByUser,ModifiedByUser,ApvByUser,Status")
                                   .ConfigureAwait(true);

            if (monitoringReport == null)
            {
                return HttpNotFound();
            }

            return View(monitoringReport);
        }

        // POST: admin/Programmes/Approve/5
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        //[GroupCustomAuthorize(Name = "MONITORING REPORTS:VERIFY")]
        public ActionResult VerifyConfirmed(int? id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveMonitoringReport";
            var parameterNames = "@Id,@UserId,@State";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                        new ParameterEntity
                                        {
                                            ParameterTuple =
                                                new Tuple<string, object>(
                                                    "State",
                                                    "APPROVE")
                                        },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                TempData["MESSAGE"] = $" The Monitoring Report was successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Details", "Reports", new { id = id });
        }
    }
}