﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class SupervisoryReviewController : Controller
    {
        public IGenericService GenericService;

        // private ISingleRegistryService SingleRegistryService; private ApplicationDbContext db =
        // new ApplicationDbContext();
        private readonly ILogService LogService;

        public SupervisoryReviewController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            // SingleRegistryService = singleRegistryService;
            LogService = logService;
        }

        #region Approve Registration View

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        // GET: Registration/Default
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = ",ACTIVE,CLOSED,COMMVAL,COMMVALAPV,IPRSVAL,PMTASS,SUBMISSIONAPV,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "REGISTRATION",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id.Value, "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category").ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }

            targetPlan.TargetPlanProgrammes = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == targetPlan.Id, null, "Programme,ListingPlan")).ToList();

            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetRegistrationSummary";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var getRegistrationSummaryVm = GenericService.GetOneBySp<GetRegSummaryVm>(spName, parameterNames, parameterList);
            var model = new TargetingSummaryVm {
                TargetPlan = targetPlan,
                GetRegSummaryVm = getRegistrationSummaryVm
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> HouseholdSummary(int id, int locationId)
        {
            var model = (await GenericService.GetOneAsync<HouseholdReg>(
                    x => x.Id == id,
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,Programme,EnumeratorDevice,HouseholdRegCharacteristic,HouseholdRegMembers,HouseholdRegProgrammes")
                .ConfigureAwait(true));
            ViewBag.ModalSize = "modal-lg";
            return View(model);
        }

        #endregion Approve Registration View

        #region View of Household HouseholdReg  Locations Enabled

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetUserRole";
            var parameterNames = "@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",progOfficer.UserId)},
            };
            var progRole = GenericService.GetOneBySp<UserRole>(spName, parameterNames, parameterList);
            var progOfficerRole = progRole.Role;
            if (progOfficerRole == "CC") { constituencyId = 0; }

            spName = "GetPendingRegistrationPlanHHCount";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            var pendingAcceptanceNoVm = GenericService.GetOneBySp<PendingAcceptanceNoVm>(spName, parameterNames, parameterList);

            spName = "GetRegistrationBatches";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };

            var HouseholdRegBatches = GenericService.GetManyBySp<AcceptanceDetailVm>(spName, parameterNames, parameterList).OrderByDescending(x => x.AcceptDate).ToList();

            var model = new BatchesViewModel {
                ListingBatches = HouseholdRegBatches,
                PendingAcceptance = pendingAcceptanceNoVm.PendingHHs
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> Pending(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetPendingRegistrationPlanHH";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            var batches = GenericService.GetManyBySp<PendingBatches>(spName, parameterNames, parameterList).ToList();

            var model = new PendingBatchesViewModel {
                Batches = batches
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> PendingHouseholds(int id, int locationId, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<HouseholdReg, bool>> filter = x => x.TargetPlanId == id;
            Expression<Func<HouseholdReg, bool>> filterx = null;

            if (locationId != null)
            {
                filterx = x => x.LocationId == locationId;
                filter = filter.And(filterx);
            }
            var TargetPlans = (await GenericService.GetAsync<HouseholdReg>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,InterviewResult,Programme")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<HouseholdReg, bool>> filter = x => x.HouseholdRegAcceptId == id;

            var TargetPlans = (await GenericService.GetAsync<HouseholdReg>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        #endregion View of Household HouseholdReg  Locations Enabled

        #region Acceptance of   Household Registrations per Constituency / Location

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:ACCEPT")]
        public async Task<ActionResult> Accept(int id, int? constituencyId, int? locationId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new HouseholdRegAcceptVm {
                BatchName = $"{constituencyId}_{id}_{DateTime.Now:s}",
                TargetPlanId = id,
                ConstituencyId = progOfficer.ConstituencyId,
                CountyId = progOfficer.CountyId,
                LocationId = locationId
            };
            if (progOfficer.ConstituencyId != null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.Id == progOfficer.ConstituencyId), "Id", "Name", constituencyId);
            }
            else if (progOfficer.CountyId != null && progOfficer.ConstituencyId == null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.CountyId == progOfficer.CountyId), "Id", "Name", constituencyId);
            }
            else
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name", constituencyId);
            }

            ViewBag.SubCounty = await GenericService.GetOneAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId, "County");
            ViewBag.County = await GenericService.GetOneAsync<County>(x => x.Id == progOfficer.CountyId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:ACCEPT")]
        public ActionResult Accept(int id, HouseholdRegAcceptVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();

            object locationId;
            if (model.LocationId.HasValue)
            {
                locationId = model.LocationId;
            }
            else
            {
                locationId = DBNull.Value;
            }

            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "AcceptHouseholdReg";
                var parameterNames = "@TargetPlanId,@ConstituencyId,@LocationId,@BatchName,@UserId";
                dynamic ConstituencyId = model.ConstituencyId;
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",model.TargetPlanId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",model.ConstituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("LocationId",locationId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BatchName",$"{model.TargetPlanId}_{model.ConstituencyId}_{DateTime.Now:s}")},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId", userId)}
                };
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                if (newModel.NoOfRows == 1)
                {
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = id,
                        TableName = "HouseholdRegAccept",
                        ModuleRightCode = "REGISTRATION SUPERVISORY:ACCEPT",
                        Record = $"{JsonConvert.SerializeObject(newModel)}",
                        WasSuccessful = true,
                        Description = "REGISTRATION SUPERVISORY Batch "
                    });

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "The data was accepted successfully";
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Kindly confirm that we have received data for the selected Sub County";
                }
                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion Acceptance of   Household Registrations per Constituency / Location

        #region Confirm Registration Exercise

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VERIFY")]
        public async Task<ActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var HouseholdRegAccept = await GenericService.GetOneAsync<HouseholdRegAccept>(x => x.Id == id.Value, "").ConfigureAwait(true);

            if (HouseholdRegAccept == null)
            {
                return HttpNotFound();
            }

            return View(HouseholdRegAccept);
        }

        [HttpPost, ActionName("Confirm")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:VERIFY")]
        public ActionResult ConfirmConfirmed(HouseholdRegAccept model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveHouseholdRegPlanBatch";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "HouseholdRegAccept",
                    ModuleRightCode = "REGISTRATION SUPERVISORY:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Registration Batch Approval"
                });

                TempData["MESSAGE"] = $" The Household  Registration Supervisory Review Batch was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion Confirm Registration Exercise

        #region Finalize Registration Exercise

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:FINALIZE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            var spName = "GetRegistrationSummary";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",DBNull.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",DBNull.Value)},
            };
            var getRegistrationSummaryVm = GenericService.GetOneBySp<GetRegSummaryVm>(spName, parameterNames, parameterList);
            var model = new TargetingSummaryVm {
                TargetPlan = targetPlan,
                GetRegSummaryVm = getRegistrationSummaryVm,
                Id = id.Value
            };
            return View(model);
        }

        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:FINALIZE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeRegistrationPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION SUPERVISORY:FINALIZE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Registration Plan Finalize"
                });

                TempData["MESSAGE"] = $" The Registration Supervisory Review was  successfully Finalized.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Finalize Registration Exercise

        #region Finalize ApprovalRegistration Exercise

        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:APPROVAL")]
        public async Task<ActionResult> FinalizeApv(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("FinalizeApv")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION SUPERVISORY:APPROVAL")]
        public ActionResult FinalizeApvConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeApvRegistrationPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION SUPERVISORY:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Registration Supervisory Review Finalize Approval"
                });

                TempData["MESSAGE"] = $" The Registration Supervisory Review was  successfully Approved for PMT Listing.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Finalize ApprovalRegistration Exercise
    }
}