﻿using System.Web.Mvc;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    public class DefaultController : Controller
    {
        private readonly ILogService LogService;

        // GET: Targeting/Default
        public ActionResult Index()
        {
            return View();
        }
    }
}