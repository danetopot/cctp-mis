﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    public class PlansController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public PlansController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        #region REGISTRATION Plan View

        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:VIEW")]
        // GET: REGISTRATION/Default
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => x.Category.Code == "REGISTRATION",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan
                = await GenericService.GetOneAsync<TargetPlan>(
                        x => x.Id == id.Value,
                        "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                    .ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }

            targetPlan.TargetPlanProgrammes = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == targetPlan.Id, null, "Programme,ListingPlan")).ToList();

            var model = new RegistrationSummaryVm {
                TargetPlan = targetPlan,
            };
            return View(model);
        }

        #endregion REGISTRATION Plan View

        #region REGISTRATION Plan Creation

        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var model = new TargetPlanVm {
                Start = DateTime.Today,
                End = DateTime.Today
            };
            var spName = "GetTargetPlanProgrammeOptions";
            var parameterNames = "";
            var parameterList = new List<ParameterEntity>();

            model.TargetPlanProgrammeOptionsVms = GenericService.GetManyBySp<TargetPlanProgrammeOptionsVm>(spName, parameterNames, parameterList).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:ENTRY")]
        public async Task<ActionResult> Create(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };
                var tarPlanProgrammeOptionsXmlModel = new List<TargetPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(
                    typeof(List<TargetPlanProgrammeOptionsVm>),
                    new XmlRootAttribute("TargetPlanProgrammeOptions"));
                var plans = model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList());
                    xml += sw.ToString();
                }

                var spName = "AddEditRegistrationPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Category,@UserId,@TargetPlanProgrammeXml";
                var parameterList = new List<ParameterEntity>
                                {
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id", DBNull.Value)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                                    new ParameterEntity{ParameterTuple = new Tuple<string, object>("Description",model.Description)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","REGISTRATION")},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanProgrammeXml",xml)}
                                };
                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "TargetPlan",
                        ModuleRightCode = "REGISTRATION PLANS:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Registration Plan Creation "
                    });

                    TempData["MESSAGE"] = $" The Registration Exercise was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The Registration Exercise was not created." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }

            TempData["MESSAGE"] = "Registration Exercise was not Created";
            TempData["KEY"] = "success";
            return View(model);
        }

        #endregion REGISTRATION Plan Creation

        #region REGISTRATION Plan Modifier

        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            var TargetPlan = (await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id)
                .ConfigureAwait(false));
            var model = new TargetPlanVm {
                Description = TargetPlan.Description,
                End = TargetPlan.End,
                Start = TargetPlan.Start,
                Id = TargetPlan.Id,

                Name = TargetPlan.Name
            };

            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(TargetPlan, model);

            var optiomns = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == TargetPlan.Id, null, "Programme,ListingPlan")).ToList();

            var spName = "GetTargetPlanProgrammeOptions";
            var parameterNames = "";
            var parameterList = new List<ParameterEntity>();

            var TargetPlanProgrammeOptionsVms = GenericService.GetManyBySp<TargetPlanProgrammeOptionsVm>(spName, parameterNames, parameterList).ToList();

            foreach (var item in TargetPlanProgrammeOptionsVms)
            {
                item.IsSelected = optiomns.Count(x =>
                    x.ListingPlanId == item.ListingPlanId && x.ProgrammeId == item.ProgrammeId) > 0;
            }

            model.TargetPlanProgrammeOptionsVms = TargetPlanProgrammeOptionsVms;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:MODIFIER")]
        public ActionResult Edit(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings { Indent = false, OmitXmlDeclaration = true };
                var tarPlanProgrammeOptionsXmlModel = new List<TargetPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(
                    typeof(List<TargetPlanProgrammeOptionsVm>),
                    new XmlRootAttribute("TargetPlanProgrammeOptions"));
                var plans = model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, model.TargetPlanProgrammeOptionsVms.Where(x => x.IsSelected).ToList());
                    xml += sw.ToString();
                }

                var spName = "AddEditRegistrationPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Category,@UserId,@TargetPlanProgrammeXml";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",model.Id)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                                            new ParameterEntity{ ParameterTuple =new Tuple<string, object>("Description",model.Description)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},

                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","REGISTRATION")},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("@TargetPlanProgrammeXml",xml)}
                                        };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(
                        spName,
                        parameterNames,
                        parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "TargetPlan",
                        ModuleRightCode = "REGISTRATION PLANS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Registration Plan Edit "
                    });
                    TempData["MESSAGE"] = $" The REGISTRATION Exercise was  successfully Updated.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The REGISTRATION Exercise was not Updated." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }

            TempData["MESSAGE"] = "Household Registration Exercise was Updated";
            TempData["KEY"] = "danger";
            return View(model);
        }

        #endregion REGISTRATION Plan Modifier

        #region Approve REGISTRATION Exercise

        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION PLANS:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION PLANS:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Registration Plan Approval "
                });

                TempData["MESSAGE"] = $" The Registration Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Approve REGISTRATION Exercise
    }
}