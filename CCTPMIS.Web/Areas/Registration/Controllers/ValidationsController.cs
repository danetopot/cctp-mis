﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Services;
using ExcelDataReader;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    public class ValidationsController : Controller
    {

        public IGenericService GenericService;
        private ISingleRegistryService SingleRegistryService;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public ValidationsController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
            LogService = logService;
        }


        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var VALIDATIONStatuses = ",CLOSED,COMMVAL,COMMVALAPV,IPRSVAL,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => VALIDATIONStatuses.Contains(x.Status.Code) && x.Category.Code == "REGISTRATION",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);

        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var RegAccepts =
                (await GenericService.GetAsync<RegAccept>(x => x.TargetPlanId == id, x => x.OrderByDescending(y => y.Id),
                    "TargetPlan,AccBy,AccApvBy,Constituency").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            var pendingValidation = (await GenericService.GetCountAsync<TargetingHh>
                (x => x.TarAcceptId == null).ConfigureAwait(true));
 
            var model = new BatchesViewModel
            {
                RegAccepts = RegAccepts,
                PendingAcceptance = pendingValidation
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var targetPlan
                = await GenericService.GetOneAsync<TargetPlan>(
                        x => x.Id == id.Value,
                        "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                    .ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }

            targetPlan.TargetPlanProgrammes = (await GenericService.GetAsync<TargetPlanProgramme>(x => x.TargetPlanId == targetPlan.Id, null, "Programme,ListingPlan")).ToList();


            //var spName = "GetTargetingSummary";
            //var parameterNames = "@TargetPlanId";
            //var parameterList = new List<ParameterEntity>
            //{
            //    new ParameterEntity
            //    {
            //        ParameterTuple =
            //            new Tuple<string, object>(
            //                "TargetPlanId",
            //                id.Value)
            //    },
            //};
            //var getTargetingSummaryVm = GenericService.GetOneBySp<GetRegistrationSummaryVm>(
            //    spName,
            //    parameterNames,
            //    parameterList);

            var model = new RegistrationSummaryVm
            {
                TargetPlan = targetPlan,
             //   GetRegistrationSummaryVm = getTargetingSummaryVm
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page, int? acceptId, int? constituency)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<TargetingHh, bool>> filter = x => x.TarAcceptId == id;
            Expression<Func<TargetingHh, bool>> filterx = null;

            if (acceptId != null)
            {
                filterx = x => x.TarAcceptId == acceptId;
                filter = filter.And(filterx);
            }

            if (constituency != null)
            {
                filterx = x => x.Enumerator.ConstituencyId == constituency;
                filter = filter.And(filterx);
            }

            var TargetPlans = (await GenericService.GetAsync<TargetingHh>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,RegAccept,BeneSex,CgSex,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);

        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VALIDATION")]
        public async Task<ActionResult> Validate(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new SrIprsValidateVm
            {
                Id = id,
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VALIDATION")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Validate(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            var message = "";
            try
            {
                var regHHs = (await GenericService.GetAsync<TargetingHh>(x => x.TarAcceptId == model.Id));

                List<IprsCache> IprsList = new List<IprsCache>();
                var iprsCache = new IprsCache();
                if (regHHs.Any())
                {
                    var login = new LoginVm
                    {
                        Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                        UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                    };
                    var auth = await SingleRegistryService.Login(login);
                    //if (auth.TokenAuth != null)
                    //{
                    //    foreach (var household in regHHs)
                    //    {

                    //        var hhd = new VerificationSrPostVm
                    //        {
                    //            TokenCode = auth.TokenAuth,
                    //            IDNumber = household.HHdNationalIdNo,
                    //            Names = household.HHdFullName
                    //        };

                    //        var cg = new VerificationSrPostVm
                    //        {
                    //            TokenCode = auth.TokenAuth,
                    //            IDNumber = household.CgNationalIdNo,
                    //            Names = household.CgFullName
                    //        };

                    //        var hhdIprs = await SingleRegistryService.IprsVerification(hhd);
                    //        if (!string.IsNullOrEmpty(hhdIprs.ID_Number))
                    //        {
                    //            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                    //                .Map(hhdIprs, iprsCache);
                    //            IprsList.Add(iprsCache);
                    //            iprsCache = new IprsCache();
                    //        }

                    //        var cgIprs = await SingleRegistryService.IprsVerification(cg);
                    //        if (!string.IsNullOrEmpty(cgIprs.ID_Number))
                    //        {
                    //            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                    //                .Map(cgIprs, iprsCache);
                    //            IprsList.Add(iprsCache);
                    //            iprsCache = new IprsCache();
                    //        }

                    //        if (IprsList.Count == 50)
                    //        {
                    //            IprsList.ForEach(x => x.DateCached = DateTime.Now);
                    //            GenericService.AddRange(IprsList);

                    //            Thread.Sleep(300);
                    //            IprsList = new List<IprsCache>();
                    //        }
                    //    }
                    //    IprsList.ForEach(x => x.DateCached = DateTime.Now);
                    //    GenericService.AddRange(IprsList);
                    //}
                    //else
                    //{
                    //    TempData["KEY"] = "danger";
                    //    TempData["MESSAGE"] = "Error " + "Your Session with SR Server was not Authenticated.";
                    //}
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Error " + "There are not Households in this  SR/IPRS batch that requires the service activation.";
                }


                return RedirectToAction("Batches", new { id = model.Id });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.Id });
        }


        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateTargetingExceptions";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };
            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);


                LogService.AuditTrail(new AuditTrailVm
                {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION VALIDATION:EXCEPTIONS",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Registration Plan Community Validation"
                });

                TempData["Title"] = "Exceptions File Generation Complete";
                TempData["MESSAGE"] = $"Exceptions File  was generated successfully.";
                TempData["KEY"] = "success";

            }
            catch (Exception ex)
            {
                TempData["Title"] = "Exceptions File Generation";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }


        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VALIDATION")]
        public async Task<ActionResult> GenerateValidations(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }
        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:VALIDATION")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GenerateValidations(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateCommunityValList";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };

            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);
                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm
                {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION VALIDATION:VALIDATION",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Registration Plan Community Validation"
                });

                TempData["Title"] = "Community Validation List Generation Complete";
                TempData["MESSAGE"] = $"Community Validation List  Generation was completed successfully.";
                TempData["KEY"] = "success";
                return RedirectToAction("Verify", "Services", new { area = "", id = newModelGen.FileCreationId.Value });
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Community Validation List Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");

        }


        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:EXPORTIPRS")]
        public async Task<ActionResult> GenerateIPRS(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:EXPORTIPRS")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GenerateIPRS(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateIPRSExportFile";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };
            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm
                {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION VALIDATION:EXPORTIPRS",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Registration Plan Export  SR/IPRS File"
                });

                TempData["Title"] = " SR/IPRS Export File Generation Complete";
                TempData["MESSAGE"] = $" SR/IPRS Export File  Generation    was completed successfully.";
                TempData["KEY"] = "success";
                return RedirectToAction("Verify", "Services", new { area = "", id = newModelGen.FileCreationId.Value });

            }
            catch (Exception ex)
            {
                TempData["Title"] = "IPRS Export File Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }


        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:IMPORTIPRS")]
        public async Task<ActionResult> ImportIprs()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:IMPORTIPRS")]
        public async Task<ActionResult> ImportIprs(HttpPostedFileBase upload)
        {
            try
            {


                if (ModelState.IsValid)
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        Stream stream = upload.InputStream;
                        IExcelDataReader reader = null;
                        if (upload.FileName.EndsWith(".xls"))
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else if (upload.FileName.EndsWith(".csv"))
                        {
                            reader = ExcelReaderFactory.CreateCsvReader(stream);
                        }
                        else if (upload.FileName.EndsWith(".xlsx"))
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        else
                        {
                            ModelState.AddModelError("File", "This file format is not supported");
                            return View();
                        }
                        var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true
                            }
                        });
                        reader.Close();

                        var model = result.Tables[0];

                        using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in model.Columns)
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);

                            bulkCopy.BulkCopyTimeout = 600;
                            bulkCopy.DestinationTableName = "temp_IPRSCache";
                            bulkCopy.WriteToServer(model);
                        }
                        Thread.Sleep(1000);

                        var spName = "ImportBulkIPRS";
                        var parameterNames = "";
                        var parameterList = new List<ParameterEntity>
                        {
                        };
                        var vm = GenericService.GetOneBySp<SPOutput>(
                            spName,
                            parameterNames,
                            parameterList);


                        LogService.AuditTrail(new AuditTrailVm
                        {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = 0,
                            TableName = "TargetPlan",
                            ModuleRightCode = "REGISTRATION VALIDATION:IMPORTIPRS",
                            Record = $"[{JsonConvert.SerializeObject(model)}]",
                            WasSuccessful = true,
                            Description = "Registration Plan Export  SR/IPRS  File"
                        });


                        TempData["Title"] = "IPRS Import was successful";
                        TempData["MESSAGE"] = $"IPRS Import was Successful Out of {model.Rows.Count} uploaded, {vm.NoOfRows} were Added";
                        TempData["KEY"] = "success";

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Please Upload Your file");
                    }
                }
            }
            catch (Exception e)
            {
                TempData["Title"] = "IPRS Import Failed";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
        
       

        #region Approve Targeting Exercise      

        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "REGISTRATION VALIDATION:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlanValidation";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm
                {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = 0,
                    TableName = "TargetPlan",
                    ModuleRightCode = "REGISTRATION VALIDATION:APPROVAL",
                    Record = $"[{JsonConvert.SerializeObject(newModel)}]",
                    WasSuccessful = true,
                    Description = "Registration Plan Export   SR/IPRS File"
                });

                TempData["MESSAGE"] = $" The Targeting Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
        #endregion


    }
}