﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Services;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class PMTAssesmentsController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        // private ApplicationDbContext db = new ApplicationDbContext();
        public PMTAssesmentsController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "REGISTRATION PMT ASSESMENT:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = ",CLOSED,COMMVAL,COMMVALAPV,PMTASS,POSTREG,";

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "REGISTRATION",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "REGISTRATION PMT ASSESMENT:VIEW")]
        public async Task<ActionResult> Households(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<HouseholdReg, bool>> filter = x => x.TargetPlanId == id;

            Expression<Func<HouseholdReg, bool>> filterx = null;

            int? constituencyId = null;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }

            int? countyId = null;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }

            if (constituencyId.HasValue)
            {
                filterx = x => x.SubLocation.ConstituencyId == constituencyId;
                filter = filter.And(filterx);
            }

            if (countyId.HasValue)
            {
                filterx = x => x.Location.Division.CountyDistrict.CountyId == constituencyId;
                filter = filter.And(filterx);
            }
            var householdRegs = (await GenericService.GetAsync<HouseholdReg>(
                    filter,
                    x => x.OrderByDescending(y => y.PMTScore),
                    "SubLocation,Location,Enumerator,HouseholdRegAccept,Programme,InterviewStatus,InterviewResult,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(householdRegs);
        }

        public async Task<ActionResult> HouseholdSummary(int id, int locationId)
        {
            var HouseholdReg = (await GenericService.GetOneAsync<HouseholdReg>(
                    x => x.Id == id,
                    "SubLocation,Location,Enumerator,Status,HouseholdRegAccept,Programme,EnumeratorDevice,InterviewStatus,InterviewResult")
                .ConfigureAwait(true));
            return View(HouseholdReg);
        }

        public async Task<ActionResult> HouseholdRegMembers(int id, int locationId)
        {
            var HouseholdReg = (await GenericService.GetAsync<HouseholdRegMember>(
                    x => x.HouseholdRegId == id, null,
                    "Relationship,SpouseInHousehold,Sex,MaritalStatus,ChronicIllnessStatus,DisabilityCareStatus,DisabilityType,EducationLevel,FatherAliveStatus,FormalJobNgo,LearningStatus,WorkType,MotherAliveStatus,IdentificationType")
                .ConfigureAwait(true));

            return View(HouseholdReg);
        }

        public async Task<ActionResult> HouseholdRegCharacteristic(int id, int locationId)
        {
            var householdRegCharacteristic = (await GenericService.GetFirstAsync<HouseholdRegCharacteristic>(
                    x => x.Id == id, null,
                    "IsOwned,TenureStatus,RoofMaterial,WallMaterial,FloorMaterial,DwellingUnitRisk,WaterSource,ToiletType,CookingFuelSource,LightingSource,IsTelevision,IsMotorcycle,IsTukTuk,IsRefrigerator,IsCar,IsMobilePhone,IsBicycle,HouseholdCondition,IsSkippedMeal,OtherProgrammes,BenefitType")
                .ConfigureAwait(true));

            return View(householdRegCharacteristic);
        }
    }
}