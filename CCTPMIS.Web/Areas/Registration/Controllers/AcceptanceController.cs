﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class AcceptanceController : Controller
    {
        public IGenericService GenericService;
        private ISingleRegistryService SingleRegistryService;
        private ApplicationDbContext db = new ApplicationDbContext();

        public AcceptanceController(IGenericService genericService, ISingleRegistryService singleRegistryService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
        }

        #region Approve Registration View   
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VIEW")]
        // GET: Registration/Default
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = ",ACTIVE,CLOSED,COMMVAL,COMMVALAPV,IPRSVAL,SUBMISSIONAPV,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "REGISTRATION",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);

        }

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var RegAccepts = (await GenericService.GetAsync<RegAccept>(x => x.TargetPlanId == id, x => x.OrderByDescending(y => y.Id), "TargetPlan,AccBy,AccApvBy,Constituency").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            var pendingAcceptance = (await GenericService.GetCountAsync<RegistrationHh>
            (x => x.RegAcceptId == null && x.TargetPlanId == id).ConfigureAwait(true));
            var model = new BatchesViewModel
            {
                RegAccepts = RegAccepts,
                PendingAcceptance = pendingAcceptance
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VIEW")]
        public async Task<ActionResult> Pending(int id)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);

            var spName = "GetPendingTargetingHH";
            var parameterNames = "@TargetPlanId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
            };
            var batches = GenericService.GetManyBySp<PendingBatches>(spName, parameterNames, parameterList).ToList();
            var pendingAcceptance = (await GenericService.GetCountAsync<TargetingHh>
                (x => x.TarAcceptId == null).ConfigureAwait(true));
            var model = new PendingBatchesViewModel
            {
                PendingAcceptance = pendingAcceptance,
                Batches = batches
            };
            return View(model);
        }


        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page, int? acceptId, int? constituency)
        {
            var acceptanceStatuses = ",ACTIVE,SUBMISSIONAPV,IPRS,VALIDATION,CLOSED,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<TargetingHh, bool>> filter =null;
            Expression<Func<TargetingHh, bool>> filterx = null;

            if (acceptId != null)
            {
                 filterx = x => x.TarAcceptId == acceptId;
                filter = filterx;
            }
            if (constituency != null)
            {
                filterx = x => x.Enumerator.ConstituencyId == constituency;
                filter.And(filterx);
            }

            var TargetPlans = (await GenericService.GetAsync<TargetingHh>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,RegAccept,HHdSex,CgSex,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);

        }

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var TargetPlan
                = await GenericService.GetOneAsync<TargetPlan>(
                        x => x.Id == id.Value,
                        "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                    .ConfigureAwait(true);
            if (TargetPlan == null)
            {
                return HttpNotFound();
            }

            var spName = "GetTargetingSummary";
            var parameterNames = "@TargetPlanId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "TargetPlanId",
                            id.Value)
                },
            };
            var getTargetingSummaryVm = GenericService.GetOneBySp<GetTargetingSummaryVm>(
                spName,
                parameterNames,
                parameterList);

            var model = new RegistrationSummaryVm
            {
                //TargetPlan = TargetPlan,
                //GetTargetingSummaryVm = getTargetingSummaryVm
            };
            return View(model);
        }




        #endregion

        #region Acceptance of Households

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:ACCEPT")]
        public async Task<ActionResult> Accept(int id, int? constituencyId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new RegAcceptVm
            {
                BatchName = "",
                TargetPlanId = id,
                ConstituencyId = progOfficer.ConstituencyId,
                CountyId = progOfficer.CountyId,
            };
            if (progOfficer.ConstituencyId != null)
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.Id == progOfficer.ConstituencyId), "Id", "Name", constituencyId);

            else if (progOfficer.CountyId != null && progOfficer.ConstituencyId == null)
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.CountyId == progOfficer.CountyId), "Id", "Name", constituencyId);
            else
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name", constituencyId);

            ViewBag.SubCounty = await GenericService.GetOneAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId, "County");
            ViewBag.County = await GenericService.GetOneAsync<County>(x => x.Id == progOfficer.CountyId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:ACCEPT")]
        public async Task<ActionResult> Accept(int id, RegAcceptVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "AcceptTargetingHousehold";
                var parameterNames = "@TargetPlanId,@ConstituencyId,@BatchName,@UserId";

                dynamic ConstituencyId = model.ConstituencyId;
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",model.TargetPlanId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",model.ConstituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BatchName",model.BatchName)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId", userId)}
                };
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                if (newModel.NoOfRows == 1)
                {
                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "The data was accepted successfully";
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Kindly confirm that we have received data for the selected Sub County";
                }
                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion

        #region Approve Targeting Exercise      
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }
        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlanBatches";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The Targeting Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Finalize Targeting Exercise

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:FINALIZE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }
        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:FINALIZE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The Targeting Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Finalize ApprovalTargeting Exercise

        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:APPROVAL")]
        public async Task<ActionResult> FinalizeApv(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }
        [HttpPost, ActionName("FinalizeApv")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:APPROVAL")]
        public ActionResult FinalizeApvConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeApvTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The Targeting Plan was  successfully Approved for IPRS Validation.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region IPRS Validate
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VALIDATE")]
        public async Task<ActionResult> Validate(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new SrIprsValidateVm
            {
                Id = id,
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "TARGETING ACCEPTANCE:VALIDATE")]
        public async Task<ActionResult> Validate(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            var message = "";
            try
            {
                var regHHs = (await GenericService.GetAsync<TargetingHh>(x => x.TarAcceptId == model.Id));

                List<IprsCache> IprsList = new List<IprsCache>();
                var iprsCache = new IprsCache();
                if (regHHs.Any())
                {
                    var login = new LoginVm
                    {
                        Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                        UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                    };
                    //var auth = await SingleRegistryService.Login(login);
                    //if (auth.TokenAuth != null)
                    //{
                    //    foreach (var household in regHHs)
                    //    {

                    //        var hhd = new VerificationSrPostVm
                    //        {
                    //            TokenCode = auth.TokenAuth,
                    //            IDNumber = household.HHdNationalIdNo,
                    //            Names = household.HHdFullName
                    //        };

                    //        var cg = new VerificationSrPostVm
                    //        {
                    //            TokenCode = auth.TokenAuth,
                    //            IDNumber = household.CgNationalIdNo,
                    //            Names = household.CgFullName
                    //        };

                    //        var hhdIprs = await SingleRegistryService.IprsVerification(hhd);
                    //        if (!string.IsNullOrEmpty(hhdIprs.ID_Number))
                    //        {
                    //            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                    //                .Map(hhdIprs, iprsCache);
                    //            IprsList.Add(iprsCache);
                    //            iprsCache = new IprsCache();
                    //        }

                    //        var cgIprs = await SingleRegistryService.IprsVerification(cg);
                    //        if (!string.IsNullOrEmpty(cgIprs.ID_Number))
                    //        {
                    //            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper()
                    //                .Map(cgIprs, iprsCache);
                    //            IprsList.Add(iprsCache);
                    //            iprsCache = new IprsCache();
                    //        }

                    //        if (IprsList.Count == 50)
                    //        {
                    //            IprsList.ForEach(x => x.DateCached = DateTime.Now);
                    //            GenericService.AddRange(IprsList);

                    //            Thread.Sleep(300);
                    //            IprsList = new List<IprsCache>();
                    //        }
                    //    }
                    //    IprsList.ForEach(x => x.DateCached = DateTime.Now);
                    //    GenericService.AddRange(IprsList);
                    //}
                    //else
                    //{
                    //    TempData["KEY"] = "danger";
                    //    TempData["MESSAGE"] = "Error " + "Your Session with SR Server was not Authenticated.";
                    //}
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Error " + "There are not Households in this IPRS batch that requires the service activation.";
                }


                return RedirectToAction("Batches", new { id = model.Id });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.Id });
        }

        #endregion
    }

}