﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Legacy;

namespace CCTPMIS.Web.Areas.Legacy.Controllers
{
    public class HhMembersController : Controller
    {
        private LegacyDbContext db = new LegacyDbContext();

        // GET: Legacy/HhMembers
        public async Task<ActionResult> Index()
        {
            return View(await db.HhMembers.ToListAsync());
        }

        // GET: Legacy/HhMembers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HhMembers hhMembers = await db.HhMembers.FindAsync(id);
            if (hhMembers == null)
            {
                return HttpNotFound();
            }
            return View(hhMembers);
        }

        // GET: Legacy/HhMembers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Legacy/HhMembers/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,MemberId,HhId,HhRegistrationId,FirstName,MiddleName,Surname,Sex,DateOfBirth,BirthCertNo,NationalIdNo,MarritalStatus,EducationLevel,AttendanceSchool,IllnessType,DisabilityType,NCPWDNo,AttendanceHeathCentre,Relationship,OccupationType,IsExited")] HhMembers hhMembers)
        {
            if (ModelState.IsValid)
            {
                db.HhMembers.Add(hhMembers);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(hhMembers);
        }

        // GET: Legacy/HhMembers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HhMembers hhMembers = await db.HhMembers.FindAsync(id);
            if (hhMembers == null)
            {
                return HttpNotFound();
            }
            return View(hhMembers);
        }

        // POST: Legacy/HhMembers/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,MemberId,HhId,HhRegistrationId,FirstName,MiddleName,Surname,Sex,DateOfBirth,BirthCertNo,NationalIdNo,MarritalStatus,EducationLevel,AttendanceSchool,IllnessType,DisabilityType,NCPWDNo,AttendanceHeathCentre,Relationship,OccupationType,IsExited")] HhMembers hhMembers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hhMembers).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(hhMembers);
        }

        // GET: Legacy/HhMembers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HhMembers hhMembers = await db.HhMembers.FindAsync(id);
            if (hhMembers == null)
            {
                return HttpNotFound();
            }
            return View(hhMembers);
        }

        // POST: Legacy/HhMembers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            HhMembers hhMembers = await db.HhMembers.FindAsync(id);
            db.HhMembers.Remove(hhMembers);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}