﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Legacy;

namespace CCTPMIS.Web.Areas.Legacy.Controllers
{
    public class CaseUpdatesController : Controller
    {
        private LegacyDbContext db = new LegacyDbContext();

        // GET: Legacy/CaseUpdates
        public async Task<ActionResult> Index()
        {
            return View(await db.CaseUpdates.ToListAsync());
        }

        // GET: Legacy/CaseUpdates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseUpdate caseUpdate = await db.CaseUpdates.FindAsync(id);
            if (caseUpdate == null)
            {
                return HttpNotFound();
            }
            return View(caseUpdate);
        }

        // GET: Legacy/CaseUpdates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Legacy/CaseUpdates/Create To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,CaseId,SourceProgrammeId,SourceProgramme,UpdateType,UpdateName,UpdateReason,UpdateDate,HhId,HhRegistrationId,OldCGMemberId,NewCGMemberId,OldCGHhMemberId,NewCGHhMemberId,FirstName,MiddleName,Surname,Sex,DateOfBirth,IDNumber,MarritalStatus,EducationLevel,AttendanceSchool,EducationChangeReason,IsChronicallyIll,IsDisabled,AttendanceHeathCentre,HealthChangeReason,Relationship,OldSubLocation,NewSubLocation,PSPBranch,PSP,ReplacePaymentCard,PSPChangeReason,AlternativeRecipientName,AlternativeRecipientIDNumber,CreatedOn,CreatedBy,ApprovedOn,ApprovedBy,ActionNotes")] CaseUpdate caseUpdate)
        {
            if (ModelState.IsValid)
            {
                db.CaseUpdates.Add(caseUpdate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(caseUpdate);
        }

        // GET: Legacy/CaseUpdates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseUpdate caseUpdate = await db.CaseUpdates.FindAsync(id);
            if (caseUpdate == null)
            {
                return HttpNotFound();
            }
            return View(caseUpdate);
        }

        // POST: Legacy/CaseUpdates/Edit/5 To protect from overposting attacks, please enable the
        // specific properties you want to bind to, for more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,CaseId,SourceProgrammeId,SourceProgramme,UpdateType,UpdateName,UpdateReason,UpdateDate,HhId,HhRegistrationId,OldCGMemberId,NewCGMemberId,OldCGHhMemberId,NewCGHhMemberId,FirstName,MiddleName,Surname,Sex,DateOfBirth,IDNumber,MarritalStatus,EducationLevel,AttendanceSchool,EducationChangeReason,IsChronicallyIll,IsDisabled,AttendanceHeathCentre,HealthChangeReason,Relationship,OldSubLocation,NewSubLocation,PSPBranch,PSP,ReplacePaymentCard,PSPChangeReason,AlternativeRecipientName,AlternativeRecipientIDNumber,CreatedOn,CreatedBy,ApprovedOn,ApprovedBy,ActionNotes")] CaseUpdate caseUpdate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(caseUpdate).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(caseUpdate);
        }

        // GET: Legacy/CaseUpdates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CaseUpdate caseUpdate = await db.CaseUpdates.FindAsync(id);
            if (caseUpdate == null)
            {
                return HttpNotFound();
            }
            return View(caseUpdate);
        }

        // POST: Legacy/CaseUpdates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CaseUpdate caseUpdate = await db.CaseUpdates.FindAsync(id);
            db.CaseUpdates.Remove(caseUpdate);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}