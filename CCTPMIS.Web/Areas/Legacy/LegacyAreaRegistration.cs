﻿using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.Legacy
{
    public class LegacyAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Legacy";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Legacy_default",
                "Legacy/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
            namespaces: new[] { "CCTPMIS.Web.Areas.Legacy.Controllers" });
        }
    }
}