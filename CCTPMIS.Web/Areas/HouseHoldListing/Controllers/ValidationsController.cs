﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using ExcelDataReader;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.HouseHoldListing.Controllers
{
    [Authorize]
    public class ValidationsController : Controller
    {
        public IGenericService GenericService;
        private ISingleRegistryService SingleRegistryService;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public ValidationsController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = "CLOSED,COMMVAL,COMMVALAPV,IPRSVAL,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var targetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "LISTING",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(targetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var listingAccepts = (await GenericService.GetAsync<ListingAccept>(x => x.TargetPlanId == id, x => x.OrderByDescending(y => y.Id), "TargetPlan.Status,AcceptBy,AcceptApvBy,Constituency").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id, "Status").ConfigureAwait(true);

            var statuses = "COMMVAL,IPRSVAL";

            ViewBag.CanValidate = statuses.Contains(targetPlan.Status.Code);

            var model = new BatchesViewModel {
                Batches = listingAccepts
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser,ApvByUser,Category,ListingPlanProgrammes.Programme,Status,ClosedByUser,ValidationFile,ExceptionsFile,FinalizeByUser,FinalizeApvByUser,ExceptionByUser,ValByUser,PostRegExceptionsFile,ComValApvByUser,ValByUser,").ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetListingSummary";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var getRegistrationSummaryVm = GenericService.GetOneBySp<GetTargetSummaryVm>(spName, parameterNames, parameterList);
            var model = new TargetingSummaryVm {
                TargetPlan = targetPlan,
                GetTargetSummaryVm = getRegistrationSummaryVm
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page, int? acceptId, int? constituency)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<ListingPlanHH, bool>> filter = x => x.ListingAcceptId == id;
            Expression<Func<ListingPlanHH, bool>> filterx = null;

            if (acceptId != null)
            {
                filterx = x => x.ListingAcceptId == id;
                filter = filter.And(filterx);
            }

            if (constituency != null)
            {
                filterx = x => x.Enumerator.ConstituencyId == constituency;
                filter = filter.And(filterx);
            }

            var TargetPlans = (await GenericService.GetAsync<ListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ListingAccept,BeneSex,CgSex,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        public async Task<ActionResult> Validate(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var listingAccept = await GenericService.GetOneAsync<ListingAccept>(x => x.Id == id);
            var model = new SrIprsValidateVm {
                Id = id,
                TargetPlanId = listingAccept.TargetPlanId
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Validate(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "GetPendingBatchIPRS";
                var parameterNames = "@ListingAcceptId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ListingAcceptId",id)},
                };
                var regHHs = GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();
                if (regHHs.Any())
                {
                    var IprsList = new List<IprsCache>();
                    var iprsCache = new IprsCache();

                    var login = new LoginVm {
                        Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                        UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                    };
                    var auth = await SingleRegistryService.Login(login);
                    if (auth.TokenAuth != null)
                    {
                        foreach (var household in regHHs)
                        {
                            var hhd = new VerificationSrPostVm {
                                TokenCode = auth.TokenAuth,
                                IDNumber = household.IDNumber,
                                Names = household.Names
                            };
                            var hhdIprs = await SingleRegistryService.IprsVerification(hhd);
                            if (string.IsNullOrEmpty(hhdIprs.ID_Number))
                            {
                                continue;
                            }

                            DateTime dateTime;
                            if (!DateTime.TryParse(hhdIprs.Date_of_Birth, result: out dateTime))
                            {
                                continue;
                            }

                            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(hhdIprs, iprsCache);
                            IprsList.Add(iprsCache);
                            iprsCache = new IprsCache();
                        }
                        IprsList.ForEach(x => x.DateCached = DateTime.Now);
                        var dt = Statics.ToDataTable(IprsList);

                        using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in dt.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                            }

                            bulkCopy.BulkCopyTimeout = 600;
                            bulkCopy.DestinationTableName = "temp_IPRSCache";
                            await bulkCopy.WriteToServerAsync(dt).ConfigureAwait(false);
                            Thread.Sleep(1000);
                        }

                        spName = "ImportBulkIPRS";
                        parameterNames = "";
                        parameterList = new List<ParameterEntity> { };
                        var vm = GenericService.GetOneBySp<SPOutput>(spName, parameterNames, parameterList);

                        LogService.AuditTrail(new AuditTrailVm {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = id,
                            TableName = "TargetPlan",
                            ModuleRightCode = "HH LISTING VALIDATION:VALIDATION",
                            Record = $"{JsonConvert.SerializeObject(IprsList)}",
                            WasSuccessful = true,
                            Description = "Household Listing SR/IPRS Validation"
                        });

                        TempData["KEY"] = "success";
                        TempData["MESSAGE"] = $"Success  - The Household Listing SR/IPRS Validation was Successful for this Batch #{model.Id} ";
                    }
                    else
                    {
                        TempData["KEY"] = "danger";
                        TempData["MESSAGE"] = "Error!  Your Session with SR/IPRS Server was not Authenticated.";
                    }
                }
                else
                {
                    TempData["KEY"] = "info";
                    TempData["MESSAGE"] = "Info - All Households in this batch, have been validated against SR/IPRS Database.";
                }

                // Update the Status after IPRS Validation.
                spName = "GetBatchHHListingPendingIPRS";
                parameterNames = "@ListingAcceptId";
                parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ListingAcceptId",id)},
                };
                GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error ! " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        public async Task<ActionResult> Override(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var listingAccept = await GenericService.GetOneAsync<ListingAccept>(x => x.Id == id);
            var model = new SrIprsValidateVm {
                Id = id,
                TargetPlanId = listingAccept.TargetPlanId
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Override(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "OverrideIPRSValidation";
                var parameterNames = "@Id,@Type";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",id)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Type","HHL")},
                };
                GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                TempData["KEY"] = "success";
                TempData["MESSAGE"] = "Success ! You have successfully ignored households that have completely failed SR/IPRS Validation.";

                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error ! " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateRegistrationExceptions";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };
            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING VALIDATION:EXCEPTIONS",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Exceptions File Generation Complete"
                });

                TempData["Title"] = "Exceptions File Generation Complete";
                TempData["MESSAGE"] = $"Exceptions File  was generated successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Exceptions File Generation";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        public async Task<ActionResult> GenerateValidations(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:VALIDATION")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GenerateValidations(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateCommunityValList";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };

            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);
                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING VALIDATION:VALIDATION",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "Community Validation List Generation Complete"
                });

                TempData["Title"] = "Community Validation List Generation Complete";
                TempData["MESSAGE"] = $"Community Validation List  Generation was completed successfully.";
                TempData["KEY"] = "success";
                return RedirectToAction("Index", "Validations", new { id = model.Id });
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Community Validation List Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:EXPORTIPRS")]
        public async Task<ActionResult> GenerateIPRS(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:EXPORTIPRS")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GenerateIPRS(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GenerateIPRSExportFile";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };
            try
            {
                var newModelGen =
                    GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING VALIDATION:EXPORTIPRS",
                    Record = $"[{JsonConvert.SerializeObject(model)},{JsonConvert.SerializeObject(file)}]",
                    WasSuccessful = true,
                    Description = "IPRS Export File Generation Complete"
                });

                TempData["Title"] = "IPRS Export File Generation Complete";
                TempData["MESSAGE"] = $"IPRS Export File  Generation    was completed successfully.";
                TempData["KEY"] = "success";
                return RedirectToAction("Verify", "Services", new { area = "", id = newModelGen.FileCreationId.Value });
            }
            catch (Exception ex)
            {
                TempData["Title"] = "IPRS Export File Generation Failed";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:IMPORTIPRS")]
        public async Task<ActionResult> ImportIprs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:IMPORTIPRS")]
        public async Task<ActionResult> ImportIprs(HttpPostedFileBase upload)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (upload != null && upload.ContentLength > 0)
                    {
                        var stream = upload.InputStream;
                        IExcelDataReader reader = null;
                        if (upload.FileName.EndsWith(".xls"))
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        }
                        else if (upload.FileName.EndsWith(".csv"))
                        {
                            reader = ExcelReaderFactory.CreateCsvReader(stream);
                        }
                        else if (upload.FileName.EndsWith(".xlsx"))
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        }
                        else
                        {
                            ModelState.AddModelError("File", "This file format is not supported");
                            return View();
                        }
                        var result = reader.AsDataSet(new ExcelDataSetConfiguration() {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration() {
                                UseHeaderRow = true
                            }
                        });
                        reader.Close();

                        var model = result.Tables[0];

                        using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in model.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                            }

                            bulkCopy.BulkCopyTimeout = 600;
                            bulkCopy.DestinationTableName = "temp_IPRSCache";
                            bulkCopy.WriteToServer(model);
                        }
                        Thread.Sleep(1000);

                        var spName = "ImportBulkIPRS";
                        var parameterNames = "";
                        var parameterList = new List<ParameterEntity> {
                        };
                        var vm = GenericService.GetOneBySp<SPOutput>(
                            spName,
                            parameterNames,
                            parameterList);

                        LogService.AuditTrail(new AuditTrailVm {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = 0,
                            TableName = "IPRSCache",
                            ModuleRightCode = "HH LISTING VALIDATION:EXPORTIPRS",
                            Record = $"[{JsonConvert.SerializeObject(model)}]",
                            WasSuccessful = true,
                            Description = "IPRS Export File Generation Complete"
                        });

                        TempData["Title"] = "IPRS Import was successful";
                        TempData["MESSAGE"] = $"IPRS Import was Successful Out of {model.Rows.Count} uploaded, {vm.NoOfRows} were Added";
                        TempData["KEY"] = "success";

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("File", "Please Upload Your file");
                    }
                }
            }
            catch (Exception e)
            {
                TempData["Title"] = "IPRS Import Failed";
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #region ApproveHousehold Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING VALIDATION:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlanValidation";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = 0,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING VALIDATION:APPROVAL",
                    Record = $"[{JsonConvert.SerializeObject(newModel)}]",
                    WasSuccessful = true,
                    Description = "Household Listing - SR/IPRS Validation Plan was  successfully Approved"
                });

                TempData["MESSAGE"] = $" The Household Listing - SR/IPRS Validation Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion ApproveHousehold Listing  Exercise
    }
}