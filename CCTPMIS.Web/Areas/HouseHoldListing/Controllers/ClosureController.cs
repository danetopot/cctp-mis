﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.HouseHoldListing.Controllers
{
    [Authorize]
    public class ClosureController : Controller
    {
        public IGenericService GenericService;

        private readonly ILogService LogService;

        public ClosureController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = "CLOSED,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "LISTING",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var TargetPlan
                = await GenericService.GetOneAsync<TargetPlan>(
                        x => x.Id == id.Value,
                        "CreatedByUser,ModifiedByUser,ApvByUser,Category,ListingPlanProgrammes.Programme,Status,ClosedByUser,ValidationFile,ExceptionsFile,FinalizeByUser,FinalizeApvByUser,ExceptionByUser,ValByUser,PostRegExceptionsFile,ComValApvByUser")
                    .ConfigureAwait(true);
            if (TargetPlan == null)
            {
                return HttpNotFound();
            }

            var spName = "GetComValSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Id",
                            id.Value)
                },
            };
            var getComValSummaryVm = GenericService.GetOneBySp<GetComValSummaryVm>(
                spName,
                parameterNames,
                parameterList);

            var model = new ComValSummaryVm {
                TargetPlan = TargetPlan,
                GetComValSummaryVm = getComValSummaryVm
            };
            return View(model);
        }

        #region CloseHousehold Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:CLOSURE")]
        public async Task<ActionResult> Close(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (targetPlan == null)
            {
                return HttpNotFound();
            }

            var spName = "GetHHListingClosureSummary";
            var parameterNames = "@TargetPlanId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
             };
            var summary = GenericService.GetManyBySp<GetHHListingClosureSummaryVm>(spName, parameterNames, parameterList).ToList();

            var model = new HHListingClosureVm {
                TargetPlan = targetPlan,
                Summary = summary,
                Id = id.Value
            };

            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:CLOSURE")]
        [HttpPost, ActionName("Close")]
        [ValidateAntiForgeryToken]
        public ActionResult CloseConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "CloseTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };
            try
            {
                GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                GenericService.GetOneBySp<SpIntVm>("ProcessListedHHs", parameterNames, parameterList);

                var targetPlan = GenericService.GetOne<TargetPlan>(x => x.Id == id);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING CLOSURE:CLOSE",
                    Record = $"{JsonConvert.SerializeObject(targetPlan)}",
                    WasSuccessful = true,
                    Description = "Household Listing Closure  Success"
                });

                TempData["MESSAGE"] = $" The Household Listing  Plan was  successfully Closed.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion CloseHousehold Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id)
        {
            var model = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING CLOSURE:EXCEPTIONS")]
        public async Task<ActionResult> GenerateExceptions(int id, TargetPlan model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var storedProcedure = "GeneratePostHHListingExceptions";
            var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
            var connection = new SqlConnectionStringBuilder(conString);
            var parameterNamesGen = "@TargetPlanId,@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
            var parameterListGen = new List<ParameterEntity>
            {
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("TargetPlanId", id),},
                new ParameterEntity
                {
                    ParameterTuple = new Tuple<string, object>("FilePath",
                        WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]),
                },

                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBServer", connection.DataSource),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBName", connection.InitialCatalog),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBUser", connection.UserID),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("DBPassword", connection.Password),},
                new ParameterEntity {ParameterTuple = new Tuple<string, object>("UserId", userId),},
            };
            try
            {
                var newModelGen = GenericService.GetOneBySp<SPOutput>(storedProcedure, parameterNamesGen, parameterListGen);

                var fileService = new FileService();
                var file = await GenericService
                    .GetOneAsync<FileCreation>(x => x.Id == newModelGen.FileCreationId.Value)
                    .ConfigureAwait(false);
                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
                GenericService.Update(file);
                var targetPlan = GenericService.GetOne<TargetPlan>(x => x.Id == id);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING CLOSURE:EXCEPTIONS",
                    Record = $"{JsonConvert.SerializeObject(targetPlan)}",
                    WasSuccessful = true,
                    Description = "Household Listing Closure  Exceptions"
                });
                TempData["Title"] = "Exceptions File Generation Complete";
                TempData["MESSAGE"] = $"Exceptions File  was generated successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["Title"] = "Exceptions File Generation";
                TempData["MESSAGE"] = ex.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }
    }
}