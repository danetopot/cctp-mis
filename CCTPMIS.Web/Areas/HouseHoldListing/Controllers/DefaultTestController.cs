﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class DefaultTestController : Controller
    {
        private ISingleRegistryService SingleRegistryService;
        public DefaultTestController(ISingleRegistryService singleRegistryService)
        {
            SingleRegistryService = singleRegistryService;
        }
        public async Task<ActionResult> Index()
        {
            var login = new LoginVm
            {
                Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]

            };
            var auth = await SingleRegistryService.Login(login);
            Debug.WriteLine(auth);
            if (auth.TokenAuth != null)
            {
                var verificationSrPostVm = new VerificationSrPostVm
                {
                    TokenCode = auth.TokenAuth,
                    IDNumber = "26316863",
                    Names = "Jane Waruguru Munyoro"

                };
               var sr = await SingleRegistryService.SrVerification(verificationSrPostVm);
                Debug.WriteLine(sr);
                var iprs = await SingleRegistryService.IprsVerification(verificationSrPostVm);
                Debug.WriteLine(iprs);
            }
            else
            {
                TempData["Message"] = auth.Status + "\n" + auth.Remarks;
                TempData["Message"] = "danger";
               
            }
            return View();
        }
    }
}