﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using PagedList;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class ScreenersController : Controller
    {
        public IGenericService GenericService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public ScreenersController(IGenericService genericService)
        {
            GenericService = genericService;
        }



        // GET: Registration/Screeners
        public async Task<ActionResult> Index(int? page, int? enumeratorId)
        {
            var userId = User.Identity.GetUserId();
            
            //var locationIds = 

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var RegistrationHhs = (await GenericService.GetAsync<RegistrationHh>(
                    null,
                    null,
                    includeProperties:
                    "RegistrationGroup,FinancialYear,Programme,SubLocation.Location")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);

            return View(RegistrationHhs);
        }

        public async Task<ActionResult> Details(int id)
        {
            var RegistrationHh = (await GenericService.GetOneAsync<RegistrationHh>(x=>x.Id ==id
                    , "RegistrationGroup,FinancialYear,Programme,SubLocation.Location")
                .ConfigureAwait(true));

            return View(RegistrationHh);
        }
    }
}