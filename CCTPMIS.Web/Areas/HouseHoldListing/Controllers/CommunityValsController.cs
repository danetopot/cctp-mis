﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.HouseHoldListing.Controllers
{
    [Authorize]
    // [RouteArea("HouseHoldListing")]
    public class CommunityValsController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        // ReSharper disable once InconsistentNaming
        private readonly ISingleRegistryService SingleRegistryService;

        private ApplicationDbContext db = new ApplicationDbContext();

        public CommunityValsController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
            LogService = logService;
        }

        #region Approve Household Listing View

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        // GET: Registration/Default
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = ",CLOSED,COMMVAL,COMMVALAPV,SUBMISSIONAPV,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "LISTING",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id.Value, "CreatedByUser,ModifiedByUser,ApvByUser,Category,ListingPlanProgrammes.Programme,Status,ClosedByUser,ValidationFile,ExceptionsFile,FinalizeByUser,FinalizeApvByUser,ExceptionByUser,ValByUser,PostRegExceptionsFile,ComValApvByUser").ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetComValListingSummary";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var getRegistrationSummaryVm = GenericService.GetOneBySp<GetTargetSummaryVm>(spName, parameterNames, parameterList);
            var model = new TargetingSummaryVm {
                TargetPlan = targetPlan,
                GetTargetSummaryVm = getRegistrationSummaryVm
            };
            return View(model);
        }

        public async Task<ActionResult> HouseholdSummary(int id, int locationId)
        {
            var model = (await GenericService.GetOneAsync<ComValListingPlanHH>(
                    x => x.Id == id,
                    "SubLocation,Location,Enumerator,Status,ComValListingAccept,BeneSex,CgSex,Programme,EnumeratorDevice")
                .ConfigureAwait(true));
            ViewBag.ModalSize = "modal-lg";
            return View(model);
        }

        #endregion Approve Household Listing View

        #region View of Household Listing  Locations Enabled

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetUserRole";
            var parameterNames = "@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",progOfficer.UserId)},
            };
            var progRole = GenericService.GetOneBySp<UserRole>(spName, parameterNames, parameterList);
            var progOfficerRole = progRole.Role;
            if (progOfficerRole == "CC") { constituencyId = 0; }

            spName = "GetPendingComValListingPlanHHCount";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            var pendingAcceptanceNoVm = GenericService.GetOneBySp<PendingAcceptanceNoVm>(spName, parameterNames, parameterList);

            spName = "GetComValListingBatches";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };

            var listingBatches = GenericService.GetManyBySp<AcceptanceDetailVm>(spName, parameterNames, parameterList).OrderByDescending(x => x.AcceptDate).ToList();

            var model = new BatchesViewModel {
                ListingBatches = listingBatches,
                PendingAcceptance = pendingAcceptanceNoVm.PendingHHs
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        public async Task<ActionResult> Pending(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetPendingComValListingPlanHH";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var batches = GenericService.GetManyBySp<PendingBatches>(spName, parameterNames, parameterList).ToList();

            var model = new PendingBatchesViewModel {
                Batches = batches
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        // [Route("communityvals/PendingHouseholds/{id}/{locationId?}/{page?}", Name = "PendingHouseholds")]
        public async Task<ActionResult> PendingHouseholds(int id, int? locationId, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            Expression<Func<ComValListingPlanHH, bool>> filter = x => x.TargetPlanId == id && x.ComValListingAcceptId == null;

            Expression<Func<ComValListingPlanHH, bool>> filterx = null;

            //var logService  = new LogService();
            //logService.LogWrite("TEST", id.ToString(), locationId.ToString(), page.ToString(), "");
            //logService.LogWrite("TEST", locationId.ToString(), locationId.ToString(), page.ToString(), "");
            if (locationId.HasValue)
            {
                filterx = x => x.LocationId == locationId;
                filter = filter.And(filterx);
            }

            var households = (await GenericService.GetAsync<ComValListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ComValListingAccept,BeneSex,CgSex,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(households);
        }

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<ComValListingPlanHH, bool>> filter = x => x.ComValListingAcceptId == id;
            var TargetPlans = (await GenericService.GetAsync<ComValListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ComValListingAccept,BeneSex,CgSex,Programme,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        #endregion View of Household Listing  Locations Enabled

        #region Acceptance of Households Listing per Constituency / Location

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:ACCEPT")]
        public async Task<ActionResult> Accept(int id, int? constituencyId, int? locationId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new ListingAcceptVm {
                BatchName = $"{constituencyId}_{id}_{DateTime.Now:s}",
                TargetPlanId = id,
                ConstituencyId = progOfficer.ConstituencyId,
                CountyId = progOfficer.CountyId,
                LocationId = locationId
            };
            if (progOfficer.ConstituencyId != null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.Id == progOfficer.ConstituencyId), "Id", "Name", constituencyId);
            }
            else if (progOfficer.CountyId != null && progOfficer.ConstituencyId == null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.CountyId == progOfficer.CountyId), "Id", "Name", constituencyId);
            }
            else
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name", constituencyId);
            }

            ViewBag.SubCounty = await GenericService.GetOneAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId, "County");
            ViewBag.County = await GenericService.GetOneAsync<County>(x => x.Id == progOfficer.CountyId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:ACCEPT")]
        public ActionResult Accept(int id, ListingAcceptVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();

            object locationId;
            if (model.LocationId.HasValue)
            {
                locationId = model.LocationId;
            }
            else
            {
                locationId = DBNull.Value;
            }

            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "AcceptComValListingHH";
                var parameterNames = "@TargetPlanId,@ConstituencyId,@LocationId,@BatchName,@UserId";
                dynamic ConstituencyId = model.ConstituencyId;
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",model.TargetPlanId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",model.ConstituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("LocationId",locationId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BatchName",$"{model.TargetPlanId}_{model.ConstituencyId}_{DateTime.Now:s}")},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId", userId)}
                };
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                if (newModel.NoOfRows == 1)
                {
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = id,
                        TableName = "ComValListingPlanHH",
                        ModuleRightCode = "HH LISTING COMMUNITY VAL:ACCEPTS",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Household Listing  Community Val  Accept "
                    });

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "The Household Listing  Community Validation Households accepted successfully";
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Kindly confirm that there exists Household Listing  Community Validation Households for the selected  Sub County";
                }
                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion Acceptance of Households Listing per Constituency / Location

        #region ConfirmHousehold Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:APPROVAL")]
        public async Task<ActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var listingAccept = await GenericService.GetOneAsync<ComValListingAccept>(x => x.Id == id.Value, "Constituency,AcceptBy,AcceptApvBy").ConfigureAwait(true);

            if (listingAccept == null)
            {
                return HttpNotFound();
            }

            return View(listingAccept);
        }

        [HttpPost, ActionName("Confirm")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:APPROVAL")]
        public ActionResult ConfirmConfirmed(ComValListingAccept model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveComValHHListingPlanBatch";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.TargetPlanId,
                    TableName = "ComValListingPlanHH",
                    ModuleRightCode = "HH LISTING COMMUNITY VAL:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Household Listing  Community Val   Batch  "
                });

                TempData["MESSAGE"] = $" The Household Listing  Community Validation Batch was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion ConfirmHousehold Listing  Exercise

        #region IPRS Validate

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VALIDATE")]
        public ActionResult Validate(int id, int targetPlanId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new SrIprsValidateVm {
                Id = id,
                TargetPlanId = targetPlanId
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VALIDATE")]
        public async Task<ActionResult> Validate(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();

            try
            {
                var spName = "GetComValPendingBatchIPRS";
                var parameterNames = "@ComValListingAcceptId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ComValListingAcceptId",id)},
                };

                var regHHs = GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                List<IprsCache> IprsList = new List<IprsCache>();
                var iprsCache = new IprsCache();
                if (regHHs.Any())
                {
                    var login = new LoginVm {
                        Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                        UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                    };
                    var auth = await SingleRegistryService.Login(login);
                    if (auth.TokenAuth != null)
                    {
                        foreach (var household in regHHs)
                        {
                            var hhd = new VerificationSrPostVm {
                                TokenCode = auth.TokenAuth,
                                IDNumber = household.IDNumber,
                                Names = household.Names
                            };
                            DateTime date1;
                            var hhdIprs = await SingleRegistryService.IprsVerification(hhd);
                            if (!string.IsNullOrEmpty(hhdIprs.ID_Number))
                            {
                                if (DateTime.TryParse(hhdIprs.Date_of_Birth, out date1))
                                {
                                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(hhdIprs, iprsCache);
                                    IprsList.Add(iprsCache);
                                    iprsCache = new IprsCache();
                                }
                            }
                        }
                        IprsList.ForEach(x => x.DateCached = DateTime.Now);

                        var dt = Statics.ToDataTable(IprsList);

                        using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in dt.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                            }

                            bulkCopy.BulkCopyTimeout = 600;
                            bulkCopy.DestinationTableName = "temp_IPRSCache";
                            bulkCopy.WriteToServer(dt);
                        }
                        Thread.Sleep(1000);

                        spName = "ImportBulkIPRS";
                        parameterNames = "";
                        parameterList = new List<ParameterEntity> { };
                        var vm = GenericService.GetOneBySp<SPOutput>(spName, parameterNames, parameterList);

                        TempData["KEY"] = "success";
                        TempData["MESSAGE"] = "Success " + "The Import was Successful";

                        spName = "GetBatchComValHHListingPendingIPRS";
                        parameterNames = "@ComValListingAcceptId";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("ComValListingAcceptId",id)},
                        };
                        GenericService.GetOneBySp<SPOutput>(spName, parameterNames, parameterList);

                        LogService.AuditTrail(new AuditTrailVm {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = model.TargetPlanId,
                            TableName = "IprsCache",
                            ModuleRightCode = "HH LISTING COMMUNITY VAL:VALIDATE",
                            Record = $"{JsonConvert.SerializeObject(IprsList)}",
                            WasSuccessful = true,
                            Description = "HH LISTING COMMUNITY VAL Confirm "
                        });
                    }
                    else
                    {
                        spName = "GetBatchComValHHListingPendingIPRS";
                        parameterNames = "@ComValListingAcceptId";
                        parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("ComValListingAcceptId",id)},
                        };
                        GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList)
                            .ToList();

                        TempData["KEY"] = "danger";
                        TempData["MESSAGE"] = "Error " + "Your Session with SR Server was not Authenticated.";
                    }
                }
                else
                {
                    spName = "GetBatchComValHHListingPendingIPRS";
                    parameterNames = "@ComValListingAcceptId";
                    parameterList = new List<ParameterEntity>
                    {
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("ComValListingAcceptId",id)},
                    };
                    GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList);

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "Important Notice " + "All Confirmed and Corrected Households Caregivers and Beneficiaries(if any) in this  batch have passed Corresponding IDs Validated.";
                }
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion IPRS Validate

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VALIDATE")]
        public async Task<ActionResult> Override(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var listingAccept = await GenericService.GetOneAsync<ListingAccept>(x => x.Id == id);
            var model = new SrIprsValidateVm {
                Id = id,
                TargetPlanId = listingAccept.TargetPlanId
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:VALIDATE")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Override(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "OverrideIPRSValidation";
                var parameterNames = "@Id,@Type";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",id)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Type","COMVAL")},
                };
                GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                TempData["KEY"] = "success";
                TempData["MESSAGE"] = "Success ! You have successfully ignored households that have completely failed SR/IPRS Validation.";

                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error ! " + e.Message;
            }

            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #region Finalize HH LISTING COMMUNITY VAL, make ready for Finalize

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:FINALIZE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var TargetPlan = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            var spName = "GetComValSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Id",
                            id)
                }
            };
            var model = new ComValSummaryVm {
                Id = id.Value,
                TargetPlan = TargetPlan,
                GetComValSummaryVm =
                    GenericService.GetOneBySp<GetComValSummaryVm>(spName, parameterNames, parameterList)
            };

            if (TargetPlan == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:FINALIZE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ProcessComVal";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };
            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The Household Listing  Community Validation was  successfully Finalized. This must be Approved before Closure.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Finalize HH LISTING COMMUNITY VAL, make ready for Finalize

        #region Close HH LISTING COMMUNITY VAL, make ready for Closure

        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:FINALIZE")]
        public async Task<ActionResult> Close(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var TargetPlan = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            var spName = "GetComValSummary";
            var parameterNames = "@Id";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "Id",
                            id)
                }
            };
            var model = new ComValSummaryVm {
                Id = id.Value,
                TargetPlan = TargetPlan,
                GetComValSummaryVm =
                    GenericService.GetOneBySp<GetComValSummaryVm>(spName, parameterNames, parameterList)
            };

            if (TargetPlan == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("Close")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING COMMUNITY VAL:FINALIZE")]
        public ActionResult CloseConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveComVal";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };
            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                TempData["MESSAGE"] = $" The  Household Listing  Community Validation was  successfully Closed. Proceed to Closure.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Close HH LISTING COMMUNITY VAL, make ready for Closure
    }
}