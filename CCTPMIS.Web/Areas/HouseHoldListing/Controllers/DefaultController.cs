﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.Registration.Controllers
{
    [Authorize]
    public class DefaultController : Controller
    {
        // GET: Registration/Default
        public ActionResult Index()
        {
            return View();
        }
    }
}