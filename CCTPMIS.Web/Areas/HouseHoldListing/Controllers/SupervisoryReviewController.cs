﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Registration;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Elmah;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.HouseHoldListing.Controllers
{
    [Authorize]
    public class SupervisoryReviewController : Controller
    {
        public IGenericService GenericService;
        private ISingleRegistryService SingleRegistryService;
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly ILogService LogService;

        public SupervisoryReviewController(IGenericService genericService, ISingleRegistryService singleRegistryService, ILogService logService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
            LogService = logService;
        }

        #region Approve Household Listing  View

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        // GET: Household Listing /Default
        public async Task<ActionResult> Index(int? page)
        {
            var acceptanceStatuses = ",ACTIVE,CLOSED,COMMVAL,COMMVALAPV,IPRSVAL,SUBMISSIONAPV,POSTREG,";
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => acceptanceStatuses.Contains(x.Status.Code) && x.Category.Code == "LISTING",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id.Value, "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,ListingPlanProgrammes.Programme").ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }
            var progOfficer = User.GetProgOfficerSummary();

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetListingSummary";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id.Value)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            var getRegistrationSummaryVm = GenericService.GetOneBySp<GetTargetSummaryVm>(spName, parameterNames, parameterList);
            var model = new TargetingSummaryVm {
                TargetPlan = targetPlan,
                GetTargetSummaryVm = getRegistrationSummaryVm
            };
            return View(model);
        }

        public async Task<ActionResult> HouseholdSummary(int id, int locationId)
        {
            var model = (await GenericService.GetOneAsync<ListingPlanHH>(
                    x => x.Id == id,
                    "SubLocation,Location,Enumerator,Status,ListingAccept,BeneSex,CgSex,Programme,EnumeratorDevice")
                .ConfigureAwait(true));
            ViewBag.ModalSize = "modal-lg";
            return View(model);
        }

        #endregion Approve Household Listing  View

        #region View of Household Listing  Locations Enabled

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> Batches(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }



            var spName = "GetUserRole";
            var parameterNames = "@UserId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",progOfficer.UserId)},
            };
            var progRole = GenericService.GetOneBySp<UserRole>(spName, parameterNames, parameterList);
            var progOfficerRole = progRole.Role;
            if (progOfficerRole == "CC") { constituencyId = 0; }


            spName = "GetPendingListingPlanHHCount";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            
            var pendingAcceptanceNoVm = GenericService.GetManyBySp<PendingAcceptanceNoVm>(spName, parameterNames, parameterList).FirstOrDefault();


            spName = "GetListingBatches";
            parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };

            var listingBatches = GenericService.GetManyBySp<AcceptanceDetailVm>(spName, parameterNames, parameterList).OrderByDescending(x => x.AcceptDate).ToList();

            var model = new BatchesViewModel {
                ListingBatches = listingBatches,
                PendingAcceptance = pendingAcceptanceNoVm!=null? pendingAcceptanceNoVm.PendingHHs : 0,
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> RejectedBatches(int id, int? page)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetRejectBatches";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
            };
            var rejectedBatches = GenericService.GetManyBySp<RejectedBatchesViewModel>(spName, parameterNames, parameterList).ToList();

            var model = new RejectBatches {
                RejectedBatches = rejectedBatches
            };


            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> RejectedBatchesDetails(int id, int countyid, int subcountyid, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            Expression<Func<ListingPlanHH, bool>> filter = x => x.TargetPlanId == id && x.SubLocation.Constituency.CountyId == countyid && x.SubLocation.ConstituencyId == subcountyid && x.RejectById != null;

            var TargetPlans = (await GenericService.GetAsync<ListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ListingAccept,BeneSex,CgSex,Programme,Location.Division.CountyDistrict.County,SubLocation.Constituency")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> Pending(int id)
        {
            var progOfficer = User.GetProgOfficerSummary();

            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            object constituencyId;
            if (progOfficer.ConstituencyId.HasValue)
            {
                constituencyId = progOfficer.ConstituencyId;
            }
            else
            {
                constituencyId = DBNull.Value;
            }

            object countyId;
            if (progOfficer.CountyId.HasValue)
            {
                countyId = progOfficer.CountyId;
            }
            else
            {
                countyId = DBNull.Value;
            }

            var spName = "GetPendingListingPlanHH";
            var parameterNames = "@TargetPlanId,@CountyId,@ConstituencyId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",id)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("CountyId",countyId)},
                new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
            };
            var batches = GenericService.GetManyBySp<PendingBatches>(spName, parameterNames, parameterList).ToList();

            var model = new PendingBatchesViewModel {
                Batches = batches
            };
            return View(model);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> PendingHouseholds(int id, int locationId, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<ListingPlanHH, bool>> filter = x => x.TargetPlanId == id && x.RejectReason == null;
            Expression<Func<ListingPlanHH, bool>> filterx = null;

            if (locationId > 0)
            {
                filterx = x => x.LocationId == locationId;
                filter = filter.And(filterx);
            }

            var TargetPlans = (await GenericService.GetAsync<ListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ListingAccept,BeneSex,CgSex,Programme,Location.Division.CountyDistrict.County,SubLocation.Constituency")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VIEW")]
        public async Task<ActionResult> BatchDetails(int id, int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            Expression<Func<ListingPlanHH, bool>> filter = x => x.ListingAcceptId == id;

            var TargetPlans = (await GenericService.GetAsync<ListingPlanHH>(
                    filter,
                    x => x.OrderByDescending(y => y.Id),
                    "SubLocation,Location,Enumerator,Status,ListingAccept,BeneSex,CgSex,Programme,Location.Division.CountyDistrict.County,SubLocation.Constituency")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        #endregion View of Household Listing  Locations Enabled

        #region Reject Household Listing  View

        public async Task<ActionResult> RejectHouseHold(int id, int? locationId)
        {
            var model = (await GenericService.GetOneAsync<ListingPlanHH>(x => x.Id == id)
                .ConfigureAwait(true));
            var vm = new HhREjectVm {
                Id = model.Id,
                LocationId = model.LocationId,
                TargetPlanId = model.TargetPlanId
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:ACCEPT")]
        [ActionName("RejectHouseHold")]
        public ActionResult RejectHouseHoldConfirmed(int id, int locationId, HhREjectVm model)
        {
            try
            {
                var userId = User.Identity.GetUserId();
                var spName = "RejectListingHH";
                var parameterNames = "@Id,@Reason,@UserId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",model.Id)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("Reason",model.Reason)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)},
                };
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "ListingPlanHH",
                    ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:ACCEPT",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Household Listing Rejected"
                });

                TempData["KEY"] = "success";
                TempData["MESSAGE"] = "The Household was Rejected successfully";
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error - The Household Rejection failed. Kindly ensure you supply the reason. ";

                ErrorSignal.FromCurrentContext().Raise(e);
            }

            return RedirectToAction("PendingHouseholds", new { id = model.TargetPlanId, locationid = model.LocationId });
        }

        #endregion Reject Household Listing  View

        #region Acceptance of Households Listing per Constituency / Location

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:ACCEPT")]
        public async Task<ActionResult> Accept(int id, int? constituencyId, int? locationId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new ListingAcceptVm {
                BatchName = $"{constituencyId}_{id}_{DateTime.Now:s}",
                TargetPlanId = id,
                ConstituencyId = progOfficer.ConstituencyId,
                CountyId = progOfficer.CountyId,
                LocationId = locationId
            };
            if (progOfficer.ConstituencyId != null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.Id == progOfficer.ConstituencyId), "Id", "Name", constituencyId);
            }
            else if (progOfficer.CountyId != null && progOfficer.ConstituencyId == null)
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(x => x.CountyId == progOfficer.CountyId), "Id", "Name", constituencyId);
            }
            else
            {
                ViewBag.ConstituencyId = new SelectList(GenericService.Get<Constituency>(), "Id", "Name", constituencyId);
            }

            ViewBag.SubCounty = await GenericService.GetOneAsync<Constituency>(x => x.Id == progOfficer.ConstituencyId, "County");
            ViewBag.County = await GenericService.GetOneAsync<County>(x => x.Id == progOfficer.CountyId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:ACCEPT")]
        public ActionResult Accept(int id, ListingAcceptVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();

            object locationId;
            if (model.LocationId.HasValue)
            {
                locationId = model.LocationId;
            }
            else
            {
                locationId = DBNull.Value;
            }

            var userId = User.Identity.GetUserId();
            try
            {
                var spName = "AcceptListingHH";
                var parameterNames = "@TargetPlanId,@ConstituencyId,@LocationId,@BatchName,@UserId";
                dynamic constituencyId = model.ConstituencyId;
                if (model.ConstituencyId == null)
                {
                    constituencyId = DBNull.Value;
                }

                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("TargetPlanId",model.TargetPlanId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ConstituencyId",constituencyId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("LocationId",locationId)},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("BatchName",$"{model.TargetPlanId}_{model.ConstituencyId}_{DateTime.Now:s}")},
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId", userId)}
                };
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                if (newModel.NoOfRows == 1)
                {
                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = id,
                        TableName = "ListingAccept",
                        ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:ACCEPT",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Household Listing Batch Acceptance "
                    });

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "The data was accepted successfully";
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Kindly confirm that we have received data for the selected Sub County";
                }
                return RedirectToAction("Batches", new { id = model.TargetPlanId });
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "Error Accepting the Batch. -" + e.Message;
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion Acceptance of Households Listing per Constituency / Location

        #region Confirm Household Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public async Task<ActionResult> Confirm(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var listingAccept = await GenericService.GetOneAsync<ListingAccept>(x => x.Id == id.Value, "AcceptApvBy,AcceptBy,Constituency").ConfigureAwait(true);

            if (listingAccept == null)
            {
                return HttpNotFound();
            }

            return View(listingAccept);
        }

        [HttpPost, ActionName("Confirm")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public ActionResult ConfirmConfirmed(ListingAccept model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveHHListingPlanBatch";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        model.Id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                spName = "GetBatchHHListingPendingIPRS";
                parameterNames = "@ListingAcceptId";
                parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ListingAcceptId",model.Id)},
                };
                GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = model.Id,
                    TableName = "ListingAccept",
                    ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(model)}",
                    WasSuccessful = true,
                    Description = "Household Listing Batch Approval "
                });
                TempData["MESSAGE"] = $" The Household Listing Batch  was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error During the Household Listing Batch Confirmation -" + e.Message;
                TempData["KEY"] = "danger";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion Confirm Household Listing  Exercise

        #region Approve Household Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlanBatches";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Household listing Supervisory review successfully Approved"
                });

                TempData["MESSAGE"] = $" The Household listing Supervisory review was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error During Approval -" + e.Message;
                TempData["KEY"] = "danger";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return RedirectToAction("Index");
        }

        #endregion Approve Household Listing  Exercise

        #region Finalize Household Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:FINALIZE")]
        public async Task<ActionResult> Finalize(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("Finalize")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:FINALIZE")]
        public ActionResult FinalizeConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:FINALIZE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Household listing Supervisory review Finalized"
                });
                TempData["MESSAGE"] = $" The Household listing Supervisory review was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error during Finalize -" + e.Message;
                TempData["KEY"] = "danger";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return RedirectToAction("Index");
        }

        #endregion Finalize Household Listing  Exercise

        #region Finalize ApprovalHousehold Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public async Task<ActionResult> FinalizeApv(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var monthlyActivity = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (monthlyActivity == null)
            {
                return HttpNotFound();
            }

            return View(monthlyActivity);
        }

        [HttpPost, ActionName("FinalizeApv")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:APPROVAL")]
        public ActionResult FinalizeApvConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "FinalizeApvTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);

                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:FINALIZE",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Household listing Supervisory review Finalize Approved"
                });
                TempData["MESSAGE"] = $" The Household listing Supervisory review was  successfully Approved for SR/IPRS Validation.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error during Approval -" + e.Message;
                TempData["KEY"] = "danger";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return RedirectToAction("Index");
        }

        #endregion Finalize ApprovalHousehold Listing  Exercise

        #region IPRS Validate

        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VALIDATE")]
        public ActionResult Validate(int id, int targetPlanId)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var model = new SrIprsValidateVm {
                Id = id,
                TargetPlanId = targetPlanId
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING SUPERVISORY REVIEW:VALIDATE")]
        public async Task<ActionResult> Validate(int id, SrIprsValidateVm model)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();

            try
            {
                var spName = "GetBatchHHListingPendingIPRS";
                var parameterNames = "@ListingAcceptId";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("ListingAcceptId",id)},
                };

                var regHHs = GenericService.GetManyBySp<VerificationSrPostVm>(spName, parameterNames, parameterList).ToList();

                List<IprsCache> IprsList = new List<IprsCache>();
                var iprsCache = new IprsCache();
                if (regHHs.Any())
                {
                    var login = new LoginVm {
                        Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                        UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                    };
                    var auth = await SingleRegistryService.Login(login);
                    DateTime date1;
                    if (auth.TokenAuth != null)
                    {
                        foreach (var household in regHHs)
                        {
                            var hhd = new VerificationSrPostVm {
                                TokenCode = auth.TokenAuth,
                                IDNumber = household.IDNumber,
                                Names = household.Names
                            };
                            var hhdIprs = await SingleRegistryService.IprsVerification(hhd);
                            if (!string.IsNullOrEmpty(hhdIprs.ID_Number))
                            {
                                if (DateTime.TryParse(hhdIprs.Date_of_Birth, out date1))
                                {
                                    new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(hhdIprs, iprsCache);
                                    IprsList.Add(iprsCache);
                                    iprsCache = new IprsCache();
                                }
                            }
                        }
                        IprsList.ForEach(x => x.DateCached = DateTime.Now);

                        var dt = Statics.ToDataTable(IprsList);

                        using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                        {
                            foreach (DataColumn col in dt.Columns)
                            {
                                bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                            }

                            bulkCopy.BulkCopyTimeout = 600;
                            bulkCopy.DestinationTableName = "temp_IPRSCache";
                            bulkCopy.WriteToServer(dt);
                        }
                        Thread.Sleep(1000);

                        spName = "ImportBulkIPRS";
                        parameterNames = "";
                        parameterList = new List<ParameterEntity> { };
                        var vm = GenericService.GetOneBySp<SPOutput>(spName, parameterNames, parameterList);

                        LogService.AuditTrail(new AuditTrailVm {
                            UserId = $"{User.Identity.GetUserId()}",
                            Key1 = id,
                            TableName = "TargetPlan",
                            ModuleRightCode = "HH LISTING SUPERVISORY REVIEW:VALIDATE",
                            Record = $"{JsonConvert.SerializeObject(vm)}",
                            WasSuccessful = true,
                            Description = "Household Listing  SR/IPRS Validation"
                        });

                        TempData["KEY"] = "success";
                        TempData["MESSAGE"] = "Success " + "The Import was Successful";
                    }
                    else
                    {
                        TempData["KEY"] = "danger";
                        TempData["MESSAGE"] = "Error " + "Your Session with SR Server was not Authenticated.";
                    }
                }
                else
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "Error " + "There are   Households in this  SR/IPRS batch that requires the service activation.";
                }

                // return RedirectToAction("Batches", new { id = model.Id });
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = "Error during SR/IPRS validation  -" + e.Message;
                TempData["KEY"] = "danger";
                ErrorSignal.FromCurrentContext().Raise(e);
            }

            return RedirectToAction("Batches", new { id = model.TargetPlanId });
        }

        #endregion IPRS Validate
    }
}