﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using AutoMapper;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Models.Targeting;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PagedList;

namespace CCTPMIS.Web.Areas.HouseHoldListing.Controllers
{
    [Authorize]
    public class PlansController : Controller
    {
        public IGenericService GenericService;
        private readonly ILogService LogService;

        // private ApplicationDbContext db = new ApplicationDbContext();

        public PlansController(IGenericService genericService, ILogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }

        #region Household Listing Plan View

        [GroupCustomAuthorize(Name = "HH LISTING PLANS:VIEW")]
        // GET: Registration/Default
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;
            var TargetPlans = (await GenericService.GetAsync<TargetPlan>(
                    x => x.Category.Code == "LISTING",
                    x => x.OrderByDescending(y => y.Id),
                    "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
            return View(TargetPlans);
        }

        [GroupCustomAuthorize(Name = "HH LISTING PLANS:VIEW")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id.Value, "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category,ListingPlanProgrammes.Programme").ConfigureAwait(true);
            if (targetPlan == null)
            {
                return HttpNotFound();
            }
            var model = new TargetingSummaryVm { TargetPlan = targetPlan };
            return View(model);
        }

        #endregion Household Listing Plan View

        #region Household Listing Plan Creation

        /// <summary>
        /// Create the Household Listing Plan
        /// </summary>
        /// <returns></returns>
        [GroupCustomAuthorize(Name = "HH LISTING PLANS:ENTRY")]
        public async Task<ActionResult> Create()
        {
            var progs = await GenericService.GetAsync<Programme>(x => x.IsActive);
            var list = progs.Select(p => new ListingPlanProgrammeOptionsVm { Name = p.Name, ProgrammeId = p.Id }).ToList();
            // Pre-populate the Create View with
            var model = new TargetPlanVm {
                ListingPlanProgrammeOptions = list
            };
            //Return the View with Pre-populated Model
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING PLANS:ENTRY")]
        public ActionResult Create(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings {
                    Indent = false,
                    OmitXmlDeclaration = true
                };
                var tarPlanProgrammeOptionsXmlModel = new List<ListingPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(typeof(List<ListingPlanProgrammeOptionsVm>), new XmlRootAttribute("ListingPlanProgrammeOptions"));
                var plans = model.ListingPlanProgrammeOptions.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, plans);
                    xml += sw.ToString();
                }

                var spName = "AddEditHhListingPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Programmes,@Category,@UserId";
                var parameterList = new List<ParameterEntity>
                    {
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",DBNull.Value)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Description",model.Description)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Programmes",xml)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","LISTING")},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)}
                    };

                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "TargetPlan",
                        ModuleRightCode = "HH LISTING PLANS:ENTRY",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Household Listing Plan created "
                    });

                    TempData["MESSAGE"] = $" The Household Listing Exercise was  successfully created.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The Household Listing Exercise was not created." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Programmes = GenericService.GetAsync<Programme>(x => x.IsActive);
            TempData["MESSAGE"] = "Household Listing Exercise was not Created";
            TempData["KEY"] = "danger";
            return View(model);
        }

        #endregion Household Listing Plan Creation

        #region Household Listing Plan Modifier

        [GroupCustomAuthorize(Name = "HH LISTING PLANS:MODIFIER")]
        public async Task<ActionResult> Edit(int? id)
        {
            var targetPlan = (await GenericService.GetOneAsync<TargetPlan>(x => x.Id == id)
                .ConfigureAwait(false));
            var model = new TargetPlanVm();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(targetPlan, model);

            var planProgs = (await GenericService.GetAsync<ListingPlanProgramme>(x => x.TargetPlanId == id, null, "Programme", null, null).ConfigureAwait(false));

            var progs = await GenericService.GetAsync<Programme>(x => x.IsActive);
            var list = progs.Select(p => new ListingPlanProgrammeOptionsVm { Name = p.Name, ProgrammeId = p.Id, IsSelected = planProgs.Any(x => x.TargetPlanId == id && x.ProgrammeId == p.Id) }).ToList();

            model.ListingPlanProgrammeOptions = list;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING PLANS:MODIFIER")]
        public ActionResult Edit(TargetPlanVm model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {
                var settings = new XmlWriterSettings {
                    Indent = false,
                    OmitXmlDeclaration = true
                };
                var tarPlanProgrammeOptionsXmlModel = new List<ListingPlanProgrammeOptionsVm>();

                var prepayrollExceptionsSerializer = new XmlSerializer(typeof(List<ListingPlanProgrammeOptionsVm>), new XmlRootAttribute("ListingPlanProgrammeOptions"));
                var plans = model.ListingPlanProgrammeOptions.Where(x => x.IsSelected).ToList();
                tarPlanProgrammeOptionsXmlModel.AddRange(plans);
                var xml = String.Empty;

                using (var sw = new StringWriter())
                {
                    var xw = XmlWriter.Create(sw, settings);
                    prepayrollExceptionsSerializer.Serialize(xw, plans);
                    xml += sw.ToString();
                }

                var spName = "AddEditHhListingPlan";
                var parameterNames = "@Id,@Name,@Description,@FromDate,@ToDate,@Programmes,@Category,@UserId";
                var parameterList = new List<ParameterEntity>
                    {
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Id",model.Id)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Name",model.Name)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Description",model.Description)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("FromDate",model.Start)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("ToDate",model.End)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Programmes",xml)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("Category","LISTING")},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",userId)}
                    };
                try
                {
                    var newModel = GenericService.GetOneBySp<AddEditPaymentCycleVm>(spName, parameterNames, parameterList);

                    LogService.AuditTrail(new AuditTrailVm {
                        UserId = $"{User.Identity.GetUserId()}",
                        Key1 = model.Id,
                        TableName = "TargetPlan",
                        ModuleRightCode = "HH LISTING PLANS:MODIFIER",
                        Record = $"{JsonConvert.SerializeObject(model)}",
                        WasSuccessful = true,
                        Description = "Household Listing Plan created "
                    });

                    TempData["MESSAGE"] = $" The Household Listing Exercise was  successfully updated.  ";
                    TempData["KEY"] = "success";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["MESSAGE"] = $" The Household Listing Exercise was not updated." + e.Message;
                    TempData["KEY"] = "danger";
                    return RedirectToAction("Index");
                }
            }

            TempData["MESSAGE"] = "Household Listing  Exercise was Updated";
            TempData["KEY"] = "danger";
            return View(model);
        }

        #endregion Household Listing Plan Modifier

        #region Approve Household Listing  Exercise

        [GroupCustomAuthorize(Name = "HH LISTING PLANS:APPROVAL")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var targetPlan = await GenericService.GetOneAsync<TargetPlan>(
                                       x => x.Id == id.Value,
                                       "Status,CreatedByUser,ModifiedByUser,ApvByUser,Category")
                                   .ConfigureAwait(true);

            if (targetPlan == null)
            {
                return HttpNotFound();
            }

            return View(targetPlan);
        }

        [HttpPost, ActionName("Approve")]
        [ValidateAntiForgeryToken]
        [GroupCustomAuthorize(Name = "HH LISTING PLANS:APPROVAL")]
        public ActionResult ApproveConfirmed(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var spName = "ApproveTargetPlan";
            var parameterNames = "@Id,@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "Id",
                                                        id)
                                            },

                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            },
                                    };

            try
            {
                var newModel = GenericService.GetOneBySp<SpIntVm>(spName, parameterNames, parameterList);
                LogService.AuditTrail(new AuditTrailVm {
                    UserId = $"{User.Identity.GetUserId()}",
                    Key1 = id,
                    TableName = "TargetPlan",
                    ModuleRightCode = "HH LISTING PLANS:APPROVAL",
                    Record = $"{JsonConvert.SerializeObject(newModel)}",
                    WasSuccessful = true,
                    Description = "Household Listing Plan Approval "
                });

                TempData["MESSAGE"] = $"The Household Listing Plan was  successfully Approved.";
                TempData["KEY"] = "success";
            }
            catch (Exception e)
            {
                TempData["MESSAGE"] = e.Message;
                TempData["KEY"] = "danger";
            }
            return RedirectToAction("Index");
        }

        #endregion Approve Household Listing  Exercise
    }
}