﻿using System.Web.Mvc;

namespace CCTPMIS.Web.Areas.HouseHoldListing
{
    public class HouseHoldListingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HouseHoldListing";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HouseHoldListing_default",
                "HouseHoldListing/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
            new[] { "CCTPMIS.Web.Areas.HouseHoldListing.Controllers" });
        }
    }
}