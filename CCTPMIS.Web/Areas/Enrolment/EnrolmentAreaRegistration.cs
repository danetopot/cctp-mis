﻿namespace CCTPMIS.Web.Areas.Enrolment
{
    using System.Web.Mvc;

    public class EnrolmentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Enrolment";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Enrolment_default",
                "Enrolment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CCTPMIS.Web.Areas.Enrolment.Controllers" });
        }
    }
}