﻿using System.Linq;
using CCTPMIS.Models.Enrolment;

namespace CCTPMIS.Web.Areas.Enrolment.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using Business.Interfaces;
    using Business.Model;
    using CCTPMIS.Models.Email;
    using Microsoft.AspNet.Identity;
    using PagedList;
    using Services;

    [Authorize]
    public class FilesController : Controller
    {
        protected readonly IEmailService EmailService;

        protected readonly IGenericService GenericService;

        public FilesController(IGenericService genericService, IEmailService emailService)
        {
            GenericService = genericService;
            EmailService = emailService;
        }
        [GroupCustomAuthorize(Name = "ENROLMENT:APPROVAL")]
        public ActionResult Approve(int id)
        {
            var file = GenericService.GetOne<FileCreation>(x => x.Id == id);

            return View(file);
        }
        [GroupCustomAuthorize(Name = "ENROLMENT:APPROVAL")]
        [HttpPost]
        public async Task<ActionResult> Approve(FileCreation model)
        {
            var fileService = new FileService();
            var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == model.Id).ConfigureAwait(false);
            try
            {
                // file.ApprovedBy = int.Parse(User.Identity.GetUserId()); file.ApprovedOn = DateTime.Now;
                file.FileChecksum = fileService.GetChecksum(file.FilePath);
                GenericService.Update(file);

                // get the PSP User List
                var psps = await GenericService.GetAsync<ApplicationUser>(x => x.UserGroup.Name == "PSP", null, "Psps")
                               .ConfigureAwait(false);
                foreach (var psp in psps)
                {
                    await EmailService.SendAsync(
                        new PspEnrolmentFileEmail {
                            FirstName = psp.FirstName,
                            To = psp.DisplayName,
                            FileName = file.Name,
                            Subject = $"New Enrolment File  -  {file.Name} ",
                        }).ConfigureAwait(true);
                }

                TempData["MESSAGE"] = $" The File  {file.Name} was approved  successfully.";
                TempData["KEY"] = "success";
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = $"An Error Occured Prior to the Approval of the  {file.Name}." + ex.Message;
                TempData["KEY"] = "danger";
            }

            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "ENROLMENT:ENTRY")]
        public ActionResult Create()
        {
            return View();
        }
        [GroupCustomAuthorize(Name = "ENROLMENT:ENTRY")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FileCreation model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var parameterNames = "@UserId";
            var parameterList = new List<ParameterEntity>
                                    {
                                        new ParameterEntity
                                            {
                                                ParameterTuple =
                                                    new Tuple<string, object>(
                                                        "UserId",
                                                        userId)
                                            }
                                    };
            var newModel = GenericService.GetOneBySp<FileCreation>("Get_Enrolment_File", parameterNames, parameterList);
            // TempData["ViewModel"] = JsonConvert.SerializeObject(newModel);
            TempData["MESSAGE"] = $" The file  was successfully generated. {newModel.Name} The file must be approved!";
            TempData["KEY"] = "success";
            return RedirectToAction("Index");
        }

        [GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        public async Task<ActionResult> Index(int? page)
        {
            var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
            var pageNum = page ?? 1;

            var userId = int.Parse(User.Identity.GetUserId());
            var files = (await GenericService.GetAsync<FileCreation>(x=>x.Name.Contains("ENROL"), x => x.OrderByDescending(y => y.Id), "CreatedByUser,FileDownloads").ConfigureAwait(true)).ToPagedList(
                pageNum,
                pageSize);
            return View(files);
        }

        [GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        public async Task<ActionResult> Details(int id)
        {
            var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == id,"FileDownloads.DownloadedByUser").ConfigureAwait(false);
            var plans = (await GenericService.GetAsync<HouseholdEnrolmentPlan>(
                    x=>x.FileCreationId == id,
                    x => x.OrderByDescending(y => y.Id),
                    "ApvByUser,CreatedByUser,ModifiedByUser,Status,EnrolmentGroup,RegGroup,Programme,FileCreation")
                .ConfigureAwait(true)).ToList();
            var model = new EnrolmentFileViewModel {
                File = file,
                Plans = plans

            };
            return View(model);
        }
    }
}