﻿using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Reports;

namespace CCTPMIS.Web
{
    using AutoMapper;

    using Business.Model;
    using CCTPMIS.Models.Account;
    using CCTPMIS.Models.Payment;

    public class AutoMapperConfig
    {
        public static void InitializeMappings()
        {
            Mapper.Initialize(
                cfg =>
                    {
                        cfg.CreateMap<RegisterViewModel, ApplicationUser>().ReverseMap()
                            .ForAllMembers(opt => opt.Ignore());

                        // User Mappings
                        cfg.CreateMap<ApplicationUser, EditUserViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditUserViewModel());
                        cfg.CreateMap<EditUserViewModel, ApplicationUser>(MemberList.Destination)
                            .ConstructUsing(x => new ApplicationUser());

                        cfg.CreateMap<ApplicationUser, EditUserProfileViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditUserProfileViewModel());
                        cfg.CreateMap<EditUserProfileViewModel, ApplicationUser>(MemberList.Destination)
                            .ConstructUsing(x => new ApplicationUser());

                        cfg.CreateMap<ApplicationUser, ChangePasswordViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new ChangePasswordViewModel());
                        cfg.CreateMap<ChangePasswordViewModel, ApplicationUser>(MemberList.Destination)
                            .ConstructUsing(x => new ApplicationUser());

                        cfg.CreateMap<ApplicationUser, EditUserContactViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditUserContactViewModel());
                        cfg.CreateMap<EditUserContactViewModel, ApplicationUser>(MemberList.Destination)
                            .ConstructUsing(x => new ApplicationUser());

                        cfg.CreateMap<Enumerator, EditUserViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditUserViewModel());
                        cfg.CreateMap<EditUserViewModel, Enumerator>(MemberList.Destination)
                            .ConstructUsing(x => new Enumerator());
                        cfg.CreateMap<Enumerator, EditUserProfileViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditUserProfileViewModel());
                        cfg.CreateMap<EditUserProfileViewModel, Enumerator>(MemberList.Destination)
                            .ConstructUsing(x => new Enumerator());

                        cfg.CreateMap<Enumerator, EditEnumeratorViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new EditEnumeratorViewModel());
                        cfg.CreateMap<EditEnumeratorViewModel, Enumerator>(MemberList.Destination)
                            .ConstructUsing(x => new Enumerator());

                        cfg.CreateMap<Change, ChangeViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new ChangeViewModel());
                        cfg.CreateMap<ChangeViewModel, Change>(MemberList.Destination)
                            .ConstructUsing(x => new Change());

                        // Payment Mappings
                        cfg.CreateMap<PaymentCycle, PaymentCycleCreateViewModel>(MemberList.Destination)
                            .ConstructUsing(x => new PaymentCycleCreateViewModel());
                        cfg.CreateMap<PaymentCycleCreateViewModel, PaymentCycle>(MemberList.Destination)
                            .ConstructUsing(x => new PaymentCycle());

                        cfg.CreateMap<ListingException, ListingExceptionReportVm>(MemberList.Destination)
                            .ForMember(d => d.UniqueId, opt => opt.MapFrom(c => c.ListingPlanHh.UniqueId))
                            .ForMember(d => d.TargetPlan, opt => opt.MapFrom(c => c.TargetPlan.Name))
                            .ConstructUsing(x => new ListingExceptionReportVm());

                        cfg.CreateMap<ComValListingException, ComValListingExceptionReportVm>(MemberList.Destination)
                            .ForMember(d => d.UniqueId, opt => opt.MapFrom(c => c.ListingPlanHh.UniqueId))
                            .ForMember(d => d.TargetPlan, opt => opt.MapFrom(c => c.TargetPlan.Name))
                            .ConstructUsing(x => new ComValListingExceptionReportVm());
                        // cfg.AddExpressionMapping();
                        cfg.ValidateInlineMaps = false;
                    });
        }
    }
}