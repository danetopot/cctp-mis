﻿using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Statics;

namespace CCTPMIS.Web
{
    public class GroupCustomAuthorize : AuthorizeAttribute
    {
        public string Name { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return string.IsNullOrEmpty(Name) || HttpContext.Current.User.UserGroupHasRight(this.Name);
        }
    }
}