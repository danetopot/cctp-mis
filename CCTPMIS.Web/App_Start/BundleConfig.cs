﻿namespace CCTPMIS.Web
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/jquery.validate.unobtrusive*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when
            // you're ready for production, use the build tool at https://modernizr.com to pick only
            // the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            bundles.Add(
                new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js", "~/Scripts/respond.js"));

            //bundles.Add(
            //    new ScriptBundle("~/signalr/hubs")
            //        .Include("~/signalr/hubs"
            //        ));

            bundles.Add(
                new ScriptBundle("~/bundles/plugins").Include(
                    "~/Scripts/angular.js",
                    "~/Scripts/css3-animate-it.js",
                    "~/Scripts/jquery-cookie-{version}.js",
                    "~/Scripts/moment-with-locales.js",
                    "~/Scripts/bootstrap-datetimepicker.js",
                    "~/Scripts/jstree3/jstree.js",
                    "~/Scripts/jquery.equalheights.js",
                    "~/Scripts/jquery.signalR-2.3.0.js",
                    "~/Scripts/jquery.cctpmis.realtime-1.0.1.js",
                    "~/bower_components/iCheck/icheck.js",
                    "~/bower_components/sweet-modal/dist/min/jquery.sweet-modal.min.js",
                    "~/bower_components/metisMenu/dist/metisMenu.js",
                     "~/Scripts/sweetalert.min.js"
                  ));
            //"~/Scripts/jstree3/jstree.js",  "~/bower_components/metisMenuAutoOpen/jquery.metismenuAutoOpen.js"
            bundles.Add(new ScriptBundle("~/bundles/cctpmis").Include("~/Scripts/jquery-cctpmis.js"));

            bundles.Add(new ScriptBundle("~/bundles/encryption").Include("~/Scripts/encryption.js"));

            bundles.Add(
                new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/font-awesome.css",
                    "~/Content/entypo.css",
                    "~/Content/hummingbird-treeview.css",
                    "~/bower_components/sweet-modal/dist/min/jquery.sweet-modal.min.css",
                     "~/bower_components/iCheck/skins/minimal/_all.css",
                    "~/bower_components/metisMenu/dist/metisMenu.css",
                    "~/Content/theme_styles.css",
                    "~/Content/bootstrap-datetimepicker.css",
                    "~/Content/sweetalert.css",
                    "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                "~/Scripts/highcharts/highcharts.js",
                "~/Scripts/highcharts/highcharts-3d.js",
                "~/Scripts/highcharts/modules/exporting.js",
                "~/Scripts/dashboards.js",
                "~/Scripts/jquery.table2excel.min.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular/angular.min.js",
                "~/Scripts/angular/angular-sanitize.min.js",
                              "~/Scripts/dirPagination.js",
            "~/Scripts/jquery.bootstrap.wizard.min.js"
                ));
        }
    }
}