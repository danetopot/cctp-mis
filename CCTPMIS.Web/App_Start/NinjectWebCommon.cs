﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CCTPMIS.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CCTPMIS.Web.App_Start.NinjectWebCommon), "Stop")]

namespace CCTPMIS.Web.App_Start
{
    using System;
    using System.Web;
    using Business.Context;
    using Business.Model;
    using Business.Repositories;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using Services;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ApplicationUserManager>().ToSelf();
            kernel.Bind<ApplicationRoleManager>().ToSelf();

            // kernel.Bind<ApplicationGroupManager>().ToSelf();
            kernel.Bind<ApplicationSignInManager>().ToSelf();
            kernel.Bind<UserManager<ApplicationUser, int>>().ToSelf();
            kernel.Bind<RoleManager<ApplicationRole, int>>().ToSelf();
            kernel.Bind<IUserStore<ApplicationUser, int>>().To<ApplicationUserStore>();
            kernel.Bind<IRoleStore<ApplicationRole, int>>().To<ApplicationRoleStore>();
            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication)
                .InRequestScope();

            kernel.Bind<ApplicationDbContext>().ToSelf().InRequestScope();
            kernel.Bind<IGenericRepository>().To<GenericRepository<ApplicationDbContext>>().InRequestScope();
            kernel.Bind<IGenericService>().To<GenericService>().InRequestScope();

            kernel.Bind<IExportService>().To<ExportService>();
            kernel.Bind<IEmailService>().To<EmailService>();
            kernel.Bind<IBackupService>().To<BackupService>();

            kernel.Bind<IUploadService>().ToConstructor(_ => new UploadService(new ApplicationDbContext()));
            kernel.Bind<IUserService>().ToConstructor(_ => new UserService(new ApplicationDbContext()));
            kernel.Bind<IEnumeratorService>().ToConstructor(_ => new EnumeratorService(new ApplicationDbContext()));
            kernel.Bind<ILogService>().ToConstructor(_ => new LogService(new GenericService(new GenericRepository<ApplicationDbContext>())));
            kernel.Bind<ISingleRegistryService>().ToConstructor(_ => new SingleRegistryService(new GenericService(new GenericRepository<ApplicationDbContext>())));
        }
    }
}