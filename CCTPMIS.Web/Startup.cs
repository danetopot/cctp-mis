﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(CCTPMIS.Web.Startup))]

namespace CCTPMIS.Web
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}