﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Statics;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        protected readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        public PaymentController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        public ActionResult Index()
        {
            return User.UserGroupHasRight("PSP-PROFILE:VIEW")
                ? (ActionResult)RedirectToAction("Details", "Default", new { area = "Psp" })
                : View();
        }


        [HttpPost]
        public JsonResult FirstChart()
        {
            var Psps = db.Psps.Select(d => new IdName { Name = d.Name, Id = d.Id });

            var BeneIndicatorFirst = db.Indicators.FirstOrDefault(x => x.Code.Contains("FUNDS"));
            var defaultReportingPeriod = db.MonitoringPeriods.FirstOrDefault(x => x.IsActive);
            var data = db.PeriodicPaymentIndicators.Where(x => x.IndicatorId == BeneIndicatorFirst.Id).Include(x=>x.Psp).ToList();
            var Programmes = db.Programmes.Select(x => new ProgName { Id = x.Id, Name = x.Name });
            var scaleTotal = data.Sum(d => d.AmountTotal);

            var PieChart = data.GroupBy(x => x.Psp.Name).Select(c => new PieVm {
                PSP = c.Key,
                AmountWithin = c.Sum(x => x.AmountWithin),
                AmountWithout = c.Sum(x => x.AmountWithout)
            });

            var BarChart = new 
             {
                 PieChart,
                AmountWithin = data.Sum(x => x.AmountWithin),
                AmountWithout = data.Sum(x => x.AmountWithout),
                AmountTotal = data.Sum(x=>x.AmountTotal)
            };
           

            var list = new {
                Payments = data,
                BarChart,
                PieChart,

                Indicators = db.Indicators.Where(i => i.Code.Contains("FUNDS") || i.Code.Contains("PAYMENT")),
                Indicator= BeneIndicatorFirst,
                Periods = db.MonitoringPeriods.OrderByDescending(x=>x.Id),
                Programmes,
                Psps,
                ScaleTotal = scaleTotal
            };

            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RefreshData(DashboardsFilterViewModel vm)
        {
            var Psps = db.Psps.Select(d => new IdName { Name = d.Name, Id = d.Id });

            var BeneIndicatorFirst = db.Indicators.FirstOrDefault(x => x.Code.Contains("FUNDS"));
            var defaultReportingPeriod = db.MonitoringPeriods.FirstOrDefault(x => x.IsActive);
            var data = db.PeriodicPaymentIndicators.Include(x=>x.Psp).Where(x => x.IndicatorId == BeneIndicatorFirst.Id).ToList();
            var Programmes = db.Programmes.Select(x => new ProgName { Id = x.Id, Name = x.Name });
            var scaleTotal = data.Sum(d => d.AmountTotal);

            var PieChart = data.GroupBy(x => x.Psp.Name).Select(c => new PieVm {
                PSP = c.Key,
                AmountWithin = c.Sum(x => x.AmountWithin),
                AmountWithout = c.Sum(x => x.AmountWithout)
            });

            var BarChart = new {
                PieChart,
                AmountWithin = data.Sum(x => x.AmountWithin),
                AmountWithout = data.Sum(x => x.AmountWithout),
                AmountTotal = data.Sum(x => x.AmountTotal)
            };


            var list = new {
                Payments = data,
                BarChart,
                PieChart,
                Indicators = db.Indicators.Where(i => i.Code.Contains("FUNDS") || i.Code.Contains("PAYMENT")),
                Periods = db.MonitoringPeriods.OrderByDescending(x => x.Id),
                Programmes,
                Psps,
                ScaleTotal = scaleTotal
            };

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

    public class PieVm
    {
       

        public string PSP { get; set; }
        public decimal? AmountWithin { get; set; }
        public decimal? AmountWithout { get; set; }
    }

    public class ChartVm
    {

        public decimal? TotalAmount { get; set; }
        public string PSP { get; set; }
        public decimal? Amount { get; set; }
    }
}