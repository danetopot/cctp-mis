﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]

    public class HomeController : Controller
    {
        protected readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        public HomeController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        public ActionResult Index()
        {
            return User.UserGroupHasRight("PSP-PAYMENT:VIEW")
                ? (ActionResult) RedirectToAction("Details", "Default", new {area = "Psp"})
                : View();
        }

        [HttpPost]
        public JsonResult FirstChart()
        {
            var defaultReportingPeriod = db.MonitoringPeriods.FirstOrDefault(x => x.IsActive);


            var indicatorsSum = db.PeriodicConstituencyBeneIndicators
                .Where(x => x.MonitoringPeriodId == defaultReportingPeriod.Id && x.Indicator.Code.Contains("BENE"))
                .GroupBy(g => new {
                    Programme = g.Programme.Code,
                    Indicator = g.Indicator.Code
                }).Select(s => new DashBoardVM {
                    Programme = s.Key.Programme ?? "",
                    Indicator = s.Key.Indicator ?? "",
                    Male = s.Sum(x => x.Male),
                    Female = s.Sum(x => x.Female),
                    Total = s.Sum(x => x.Total),
                }).ToList();

            var paymentBeneOpct = new List<int?>();
            var paymentBeneCTOVC = new List<int?>();
            var paymentBenePWSD = new List<int?>();

            paymentBeneOpct.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_EVER_PAID")?.Total);
            paymentBeneOpct.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_INELIGIBLE")?.Total);
            paymentBeneOpct.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_ON_PAYROLL")?.Total);
            paymentBeneOpct.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_ON_PAYROLL_FIRST")?.Total);

            paymentBeneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_EVER_PAID")?.Total);
            paymentBeneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_INELIGIBLE")?.Total);
            paymentBeneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_ON_PAYROLL")?.Total);
            paymentBeneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_ON_PAYROLL_FIRST")?.Total);

            paymentBenePWSD.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_EVER_PAID")?.Total);
            paymentBenePWSD.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_INELIGIBLE")?.Total);
            paymentBenePWSD.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_ON_PAYROLL")?.Total);
            paymentBenePWSD.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_ON_PAYROLL_FIRST")?.Total);


            var beneOPCT = new List<int?>();
            var beneCTOVC = new List<int?>();
            var benePWSDCT = new List<int?>();

            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_EVER_EXITED")?.Total);
            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_EXITED")?.Total);
            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_ENROLLED")?.Total);
            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_ACCOUNTOPENED")?.Total);
            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_REGISTERED")?.Total);
            beneOPCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "BENE_CARDED")?.Total);

            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_EVER_EXITED")?.Total);
            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_EXITED")?.Total);
            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_ENROLLED")?.Total);
            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_ACCOUNTOPENED")?.Total);
            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_REGISTERED")?.Total);
            beneCTOVC.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "BENE_CARDED")?.Total);


            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_EVER_EXITED")?.Total);
            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_EXITED")?.Total);
            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_ENROLLED")?.Total);
            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_ACCOUNTOPENED")?.Total);
            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_REGISTERED")?.Total);
            benePWSDCT.Add(indicatorsSum.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "BENE_CARDED")?.Total);




            var Payment = db.PeriodicPaymentIndicators
                .Where(x => x.MonitoringPeriodId == defaultReportingPeriod.Id)
                .GroupBy(g => new {
                    Programme = g.Programme.Code,
                    Indicator = g.Indicator.Code
                }).Select(s => new DashBoardPaymentVM {
                    Programme = s.Key.Programme ?? "",
                    Indicator = s.Key.Indicator ?? "",
                    AmountWithin = s.Sum(x => x.AmountWithin),
                    AmountWithout = s.Sum(x => x.AmountWithout),
                    AmountTotal = s.Sum(x => x.AmountWithout + x.AmountWithin),
                }).ToList();

            var PaymentOPCT = new List<decimal?>();
            var PaymentCTOVC = new List<decimal?>();
            var PaymentPWSDCT = new List<decimal?>();


            PaymentOPCT.Add(Payment.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "PAYMENT_DISBURSEMENT")?.AmountTotal);
            PaymentCTOVC.Add(Payment.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "PAYMENT_DISBURSEMENT")?.AmountTotal);
            PaymentPWSDCT.Add(Payment.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "PAYMENT_DISBURSEMENT")?.AmountTotal);
             
            PaymentOPCT.Add(Payment.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "FUNDS_REQUESTED")?.AmountTotal);
            PaymentCTOVC.Add(Payment.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "FUNDS_REQUESTED")?.AmountTotal);
            PaymentPWSDCT.Add(Payment.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "FUNDS_REQUESTED")?.AmountTotal);

            PaymentOPCT.Add(Payment.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "TOTAL_PAYMENT_DISBURSEMENT")?.AmountTotal);
            PaymentCTOVC.Add(Payment.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "TOTAL_PAYMENT_DISBURSEMENT")?.AmountTotal);
            PaymentPWSDCT.Add(Payment.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "TOTAL_PAYMENT_DISBURSEMENT")?.AmountTotal);

            PaymentOPCT.Add(Payment.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "TOTAL_FUNDS_REQUESTED")?.AmountTotal);
            PaymentCTOVC.Add(Payment.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "TOTAL_FUNDS_REQUESTED")?.AmountTotal);
            PaymentPWSDCT.Add(Payment.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "TOTAL_FUNDS_REQUESTED")?.AmountTotal);


            var indicatorsComplaints = db.PeriodicConstituencyBeneIndicators
                .Where(x => x.MonitoringPeriodId == defaultReportingPeriod.Id && x.Indicator.Code.Contains("COMPLAINTS"))
                .GroupBy(g => new {
                    Programme = g.Programme.Code,
                    Indicator = g.Indicator.Code
                }).Select(s => new DashBoardVM {
                    Programme = s.Key.Programme ?? "",
                    Indicator = s.Key.Indicator ?? "",
                    Male = s.Sum(x => x.Male),
                    Female = s.Sum(x => x.Female),
                    Total = s.Sum(x => x.Total),
                }).ToList();

            var complaintsBeneOpct = new List<int?>();
            var complaintsBeneCTOVC = new List<int?>();
            var complaintsBenePWSD = new List<int?>();

            complaintsBeneOpct.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "COMPLAINTS_CLOSED_OUTSIDE")?.Total);
            complaintsBeneOpct.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "COMPLAINTS_CLOSED_WITHIN")?.Total);
            complaintsBeneOpct.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "COMPLAINTS_OPEN_OUTSIDE")?.Total);
            complaintsBeneOpct.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "COMPLAINTS_OPEN_WITHIN")?.Total);

            complaintsBeneCTOVC.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "COMPLAINTS_CLOSED_OUTSIDE")?.Total);
            complaintsBeneCTOVC.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "COMPLAINTS_CLOSED_WITHIN")?.Total);
            complaintsBeneCTOVC.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "COMPLAINTS_OPEN_OUTSIDE")?.Total);
            complaintsBeneCTOVC.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "COMPLAINTS_OPEN_WITHIN")?.Total);

            complaintsBenePWSD.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "COMPLAINTS_CLOSED_OUTSIDE")?.Total);
            complaintsBenePWSD.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "COMPLAINTS_CLOSED_WITHIN")?.Total);
            complaintsBenePWSD.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "COMPLAINTS_OPEN_OUTSIDE")?.Total);
            complaintsBenePWSD.Add(indicatorsComplaints.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "COMPLAINTS_OPEN_WITHIN")?.Total);

            var indicatorsupdates = db.PeriodicConstituencyBeneIndicators
                .Where(x => x.MonitoringPeriodId == defaultReportingPeriod.Id && x.Indicator.Code.Contains("UPDATES"))
                .GroupBy(g => new {
                    Programme = g.Programme.Code,
                    Indicator = g.Indicator.Code
                }).Select(s => new DashBoardVM {
                    Programme = s.Key.Programme ?? "",
                    Indicator = s.Key.Indicator ?? "",
                    Male = s.Sum(x => x.Male),
                    Female = s.Sum(x => x.Female),
                    Total = s.Sum(x => x.Total),
                }).ToList();

            var updatesBeneOpct = new List<int?>();
            var updatesBeneCTOVC = new List<int?>();
            var updatesBenePWSD = new List<int?>();

            updatesBeneOpct.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "UPDATES_CLOSED_OUTSIDE")?.Total);
            updatesBeneOpct.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "UPDATES_CLOSED_WITHIN")?.Total);
            updatesBeneOpct.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "UPDATES_OPEN_OUTSIDE")?.Total);
            updatesBeneOpct.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "OPCT" && x.Indicator == "UPDATES_OPEN_WITHIN")?.Total);

            updatesBeneCTOVC.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "UPDATES_CLOSED_OUTSIDE")?.Total);
            updatesBeneCTOVC.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "UPDATES_CLOSED_WITHIN")?.Total);
            updatesBeneCTOVC.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "UPDATES_OPEN_OUTSIDE")?.Total);
            updatesBeneCTOVC.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "CT-OVC" && x.Indicator == "UPDATES_OPEN_WITHIN")?.Total);

            updatesBenePWSD.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "UPDATES_CLOSED_OUTSIDE")?.Total);
            updatesBenePWSD.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "UPDATES_CLOSED_WITHIN")?.Total);
            updatesBenePWSD.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "UPDATES_OPEN_OUTSIDE")?.Total);
            updatesBenePWSD.Add(indicatorsupdates.SingleOrDefault(x => x.Programme == "PwSD-CT" && x.Indicator == "UPDATES_OPEN_WITHIN")?.Total);




            var list = new {
                Period = defaultReportingPeriod,
                PaymentBeneOPCT = paymentBeneOpct,
                PaymentBeneCTOVC = paymentBeneCTOVC,
                PaymentBenePWSD = paymentBenePWSD,
                BeneOPCT = beneOPCT,
                BeneCTOVC = beneCTOVC,
                BenePWSD = benePWSDCT,
                updatesBeneOPCT = updatesBeneOpct,
                updatesBeneCTOVC = updatesBeneCTOVC,
                updatesBenePWSD = updatesBenePWSD,
                complaintsBeneOPCT = complaintsBeneOpct,
                complaintsBeneCTOVC = complaintsBeneCTOVC,
                complaintsBenePWSD = complaintsBenePWSD,
                PaymentOPCT = PaymentOPCT,
                PaymentCTOVC = PaymentCTOVC,
                PaymentPWSD = PaymentPWSDCT,

            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

    public class GroupByProgrammeAndIndicator
    {
        public string Programme { get; set; }
        public string Indicator { get; set; }

    }

    public class DashBoardPaymentVM
    {

        public float Percentage { get; set; }
        public decimal AmountTotal { get; set; }
        public decimal AmountWithin { get; set; }
        public decimal AmountWithout { get; set; }

        public string Programme { get; set; }
        public string Psp { get; set; }
        public string Indicator { get; set; }
    }

    public class DashBoardVM
    {
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
        public string Programme { get; set; }
        public string Indicator { get; set; }
    }
    public class DashboardsViewModel
    {
        public int OpenComplaints { get; set; }
        public int ClosedComplaints { get; set; }
        public int OpenUpdates { get; set; }
        public int ClosedUpdates { get; set; }
        public int OpenEFCs { get; set; }
        public int ClosedEFCs { get; set; }
        public int Psps { get; set; }
        public int paymentBeneCycles { get; set; }
    }
}