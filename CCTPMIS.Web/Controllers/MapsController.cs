﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Services;
using ClosedXML;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    public class MapsController : Controller
    {
        protected readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        public MapsController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        public ActionResult Index()
        {
           
            var vm = new MapsViewModel();
            vm.Indicators = db.Indicators.Where(i => i.Code.Contains("BENE") || i.Code.Contains("UPDATED") || i.Code.Contains("COMPLAINTS")   ).ToList();
            vm.Counties = db.Counties.ToList();
            vm.MonitoringPeriods = db.MonitoringPeriods.OrderByDescending(c => c.Id).ToList();
            vm.Programmes = db.Programmes.ToList();
            return User.UserGroupHasRight("PSP-PAYMENT:VIEW")
                ? (ActionResult)RedirectToAction("Index", "Default", new { area = "Psp" })
                : View(vm);
        }

        [HttpPost]
        public JsonResult Beneficiaries(MapsFilterViewModel vm)
        {
            vm.MonitoringPeriodId = vm.MonitoringPeriodId ??  db.MonitoringPeriods.FirstOrDefault(x => x.IsActive)?.Id;
            vm.IndicatorId = vm.IndicatorId ?? db.Indicators.FirstOrDefault(x => x.Code == "BENE_EVER_PAID")?.Id;

            var data = db.PeriodicConstituencyBeneIndicators.Where(x => x.IndicatorId == vm.IndicatorId && x.MonitoringPeriodId== vm.MonitoringPeriodId)
                .Include(x => x.Constituency.County).ToList();
           
            var mapData = data.GroupBy(x=>x.Constituency.County.Name).Select(i => new string[]
            {
                i.Key,
                i.Sum(x=>x.Total).ToString()
            });
            return Json(mapData, JsonRequestBehavior.AllowGet);
        }

    }


    public class MapsFilterViewModel
    {
        public int? IndicatorId { get; set; }
        public int? CountyId { get; set; }
        public int? MonitoringPeriodId { get; set; }
        public int? ProgrammeId { get; set; }
    }
    public class MapsViewModel
    {
        public IEnumerable<Indicator> Indicators { get; set; }
        public IEnumerable<County> Counties { get; set; }
        public IEnumerable<MonitoringPeriod> MonitoringPeriods { get; set; }
        public IEnumerable<Programme> Programmes { get; set; }

    }
}