﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Payment;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    public class ReportsExportController : Controller
    {
        public readonly IGenericService GenericService;
        protected readonly IEmailService EmailService;
        private ApplicationDbContext db = new ApplicationDbContext();
        //   private ApplicationDbContext db = new ApplicationDbContext();

        public ReportsExportController(IGenericService genericService, EmailService emailService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [Route("ReportsExport/enrolment/{title?}/{name?}/{url}")]
        public ActionResult Enrolment(EnrolmentListViewModel vm, string title, string name, string url)
        {
            var spName = "RptGetEnrolment";
            vm.ReportTypeId = vm.ReportTypeId ?? 1;
            var parameterNames = "@HhEnrolmentPlanId,@ProgrammeId,@AccountOpenedId,@CardIssuedId,@WithCaregiverId,@ReportTypeId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "HhEnrolmentPlanId",
                            vm.HhEnrolmentPlanId == null ? (object)DBNull.Value : vm.HhEnrolmentPlanId)
                }
                ,
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ProgrammeId",
                            vm.ProgrammeId == null ? (object)DBNull.Value : vm.ProgrammeId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "AccountOpenedId",
                            vm.AccountOpenedId == null ? (object)DBNull.Value : vm.AccountOpenedId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "CardIssuedId",
                            vm.CardIssuedId == null ? (object)DBNull.Value : vm.CardIssuedId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "WithCaregiverId",
                            vm.WithCaregiverId == null ? (object)DBNull.Value : vm.WithCaregiverId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ReportTypeId",
                            vm.ReportTypeId == null ? (object)DBNull.Value : vm.ReportTypeId)
                }
            };
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 20;

            if (vm.ReportTypeId == 1)
            {
                vm.IDetails = GenericService.GetManyBySp<EnrolmentBenViewModel>(spName, parameterNames, parameterList);
            }
            else
            {
                vm.ISummaries = GenericService.GetManyBySp<ReportSummaryViewModel>(spName, parameterNames, parameterList);
            }

            ViewBag.Name = name.Replace("-", " ");
            ViewBag.Url = url.Replace("_", "/");
            ViewBag.Title = title;
            return View(vm);
        }

        [Route("ReportsExport/PayrollStatement/{title?}/{name?}/{url}")]
        public ActionResult PayrollStatement(PayrollStatementListViewModel vm, string title, string name, string url)
        {
            vm.ReportTypeId = vm.ReportTypeId ?? 1;
            var spName = "RptGetPayrollStatement";

            var parameterNames = "@ProgrammeId,@PaymentCycleId,@CreditStatusId,@PaymentTypeId,@ReportTypeId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ProgrammeId",
                            vm.ProgrammeId == null ? (object)DBNull.Value : vm.ProgrammeId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentCycleId",
                            vm.PaymentCycleId == null ? (object)DBNull.Value : vm.PaymentCycleId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "CreditStatusId",
                            vm.CreditStatusId == null ? (object)DBNull.Value : vm.CreditStatusId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentTypeId",
                            vm.PaymentTypeId == null ? (object)DBNull.Value : vm.PaymentTypeId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ReportTypeId",
                            vm.ReportTypeId == null ? (object)DBNull.Value : vm.ReportTypeId)
                }
            };
            var page = vm.Page ?? 1;
            var pageSize = vm.PageSize ?? 20;

            if (vm.ReportTypeId == 1)
            {
                vm.IDetails = GenericService.GetManyBySp<PayrollStatementDetailsViewModel>(spName, parameterNames, parameterList);
            }
            else
            {
                vm.ISummaries = GenericService.GetManyBySp<ReportSummaryViewModel>(spName, parameterNames, parameterList);
            }
            ViewBag.ProgrammeId = new SelectList(db.Programmes.ToList(), "Id", "Code", vm.ProgrammeId);

            ViewBag.Name = name.Replace("-", " ");
            ViewBag.Url = url.Replace("_", "/");
            ViewBag.Title = title;
            return View(vm);
        }

        [Route("ReportsExport/PrePayrollExceptions/{title?}/{name?}/{url}")]
        public ActionResult PrePayrollExceptions(PrePayrollExceptionsListViewModel vm, string title, string name, string url)
        {
            vm.ReportTypeId = vm.ReportTypeId ?? 1;
            var spName = "RptGetPrePayrollException";
            var parameterNames = "@ProgrammeId,@PaymentCycleId,@CreditStatusId,@PaymentTypeId,@StatusId,@ExceptionCategoryId,@ReportTypeId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ProgrammeId",
                            vm.ProgrammeId == null ? (object)DBNull.Value : vm.ProgrammeId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentCycleId",
                            vm.PaymentCycleId == null ? (object)DBNull.Value : vm.PaymentCycleId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "CreditStatusId",
                            vm.CreditStatusId == null ? (object)DBNull.Value : vm.CreditStatusId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "PaymentTypeId",
                            vm.PaymentTypeId == null ? (object)DBNull.Value : vm.PaymentTypeId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ExceptionCategoryId",
                            vm.ExceptionCategoryId == null ? (object)DBNull.Value : vm.ExceptionCategoryId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "StatusId",
                            vm.StatusId == null ? (object)DBNull.Value : vm.StatusId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ReportTypeId",
                            vm.ReportTypeId == null ? (object)DBNull.Value : vm.ReportTypeId)
                }
            };

            if (vm.ReportTypeId == 1)
            {
                vm.IDetails = GenericService.GetManyBySp<PrePayrollExceptionsViewModel>(spName, parameterNames, parameterList);
            }
            else
            {
                vm.ISummaries = GenericService.GetManyBySp<ReportSummaryViewModel>(spName, parameterNames, parameterList);
            }

            ViewBag.Name = name.Replace("-", " ");
            ViewBag.Url = url.Replace("_", "/");
            ViewBag.Title = title;
            return View(vm);
        }

        [Route("ReportsExport/MonthlyActivity/{title?}/{name?}/{url}")]
        public ActionResult MonthlyActivity(MonthlyActivityListViewModel vm, string title, string name, string url)
        {
            vm.ReportTypeId = vm.ReportTypeId ?? 1;
            var spName = "RptGetMonthlyActivity";
            var parameterNames = "@ProgrammeId,@ActivityMonthId,@UniqueWithdrawalsId,@ProofOfLifeId,@DormancyId,@ReportTypeId";
            var parameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ProgrammeId",
                            vm.ProgrammeId == null ? (object)DBNull.Value : vm.ProgrammeId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ActivityMonthId",
                            vm.ActivityMonthId == null ? (object)DBNull.Value : vm.ActivityMonthId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "UniqueWithdrawalsId",
                            vm.UniqueWithdrawalsId == null ? (object)DBNull.Value : vm.UniqueWithdrawalsId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ProofOfLifeId",
                            vm.ProofOfLifeId == null ? (object)DBNull.Value : vm.ProofOfLifeId)
                }
                , new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "DormancyId",
                            vm.DormancyId == null ? (object)DBNull.Value : vm.DormancyId)
                }, new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "ReportTypeId",
                            vm.ReportTypeId == null ? (object)DBNull.Value : vm.ReportTypeId)
                }
            };

            if (vm.ReportTypeId == 1)
            {
                vm.IDetails = GenericService.GetManyBySp<MonthlyActivityViewModel>(spName, parameterNames, parameterList);
            }
            else
            {
                vm.ISummaries = GenericService.GetManyBySp<ReportSummaryViewModel>(spName, parameterNames, parameterList);
            }

            ViewBag.Name = name.Replace("-", " ");
            ViewBag.Url = url.Replace("_", "/");
            ViewBag.Title = title;
            return View(vm);
        }
    }
}