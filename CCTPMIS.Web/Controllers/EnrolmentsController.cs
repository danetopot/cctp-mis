﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    public class EnrolmentsController : Controller
    {
        //  private readonly ApplicationDbContext Db;

        // ReSharper disable once InconsistentNaming
        private readonly IEmailService EmailService;

        // ReSharper disable once InconsistentNaming
        private readonly IGenericService GenericService;

        public EnrolmentsController(IGenericService genericService, IEmailService emailService, ApplicationDbContext db)
        {
            GenericService = genericService;
            EmailService = emailService;
        }

        public static DataTable CreateDataTable(IEnumerable source)
        {
            var table = new DataTable();
            int index = 0;
            var properties = new List<PropertyInfo>();
            foreach (var obj in source)
            {
                if (index == 0)
                {
                    foreach (var property in obj.GetType().GetProperties())
                    {
                        if (Nullable.GetUnderlyingType(property.PropertyType) != null)
                        {
                            continue;
                        }

                        properties.Add(property);
                        table.Columns.Add(new DataColumn(property.Name, property.PropertyType));
                    }
                }

                object[] values = new object[properties.Count];
                for (int i = 0; i < properties.Count; i++)
                {
                    values[i] = properties[i].GetValue(obj);
                }

                table.Rows.Add(values);
                index++;
            }

            return table;
        }

        //[GroupCustomAuthorize(Name = "ENROLMENT:APPROVAL")]
        //public ActionResult Approve(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var householdEnrolmentPlan = GenericService.GetOne<HouseholdEnrolmentPlan>(
        //        x => x.Id == id,
        //        "ApvByUser,CreatedByUser,ModifiedByUser,Status,EnrolmentGroup,RegGroup,Programme");

        //    if (householdEnrolmentPlan == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(householdEnrolmentPlan);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:APPROVAL")]
        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult Approve(HouseholdEnrolmentPlan model)
        //{
        //    var userId = int.Parse(User.Identity.GetUserId());
        //    var parameterNames = "@Id,@UserId";
        //    var parameterList = new List<ParameterEntity>
        //                            {
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "Id",
        //                                                model.Id)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "UserId",
        //                                                userId)
        //                                    }
        //                            };
        //    var newModel = GenericService.GetOneBySp<SpIntVm>("ApproveEnrolmentPlan", parameterNames, parameterList);

        //    // TempData["ViewModel"] = JsonConvert.SerializeObject(newModel);
        //    TempData["MESSAGE"] = " The Enrolment successfully Approved.  Kindly Proceed to Generate the Psp File";
        //    TempData["KEY"] = "success";

        //    return RedirectToAction("Index");
        //}

        [HttpGet]
        [ClientErrorHandler]
        [AllowAnonymous]
        public ActionResult BeneForEnrolment(int id, int regGroupId)
        {
            var sp = "GetBeneForEnrolment";
            var spParameters = "@ProgrammeId,@RegGroupId";
            var spParameterList = new List<ParameterEntity>
                                      {
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "ProgrammeId",
                                                          id)
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "RegGroupId",
                                                          regGroupId)
                                              }
                                      };

            var hhEPvm = GenericService.GetOneBySp<HouseholdEnrolmentPlanViewModel>(sp, spParameters, spParameterList);

            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }

        //[GroupCustomAuthorize(Name = "ENROLMENT:ENTRY")]
        //public ActionResult Create()
        //{
        //    ViewBag.ProgrammeId = new SelectList(GenericService.Get<Programme>(), "Id", "Name");
        //    ViewBag.RegGroupId = new SelectList(
        //        GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "Registration Group"),
        //        "Id",
        //        "Description");
        //    ViewBag.EnrolmentGroupId = new SelectList(
        //        GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "Enrolment Group"),
        //        "Id",
        //        "Description");

        //    return View();
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:ENTRY")]
        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult Create(HouseholdEnrolmentPlan model)
        //{
        //    var userId = int.Parse(User.Identity.GetUserId());
        //    var parameterNames =
        //        "@Id,@ProgrammeId,@RegGroupId,@RegGroupHhs,@BeneHhs,@ExpPlanEqualShare,@ExpPlanPovertyPrioritized,@EnrolmentNumbers,@EnrolmentGroupId,@UserId";
        //    var parameterList = new List<ParameterEntity>
        //                            {
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "Id",
        //                                                DBNull.Value)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ProgrammeId",
        //                                                model.ProgrammeId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "RegGroupId",
        //                                                model.RegGroupId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "RegGroupHhs",
        //                                                model.RegGroupHhs)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "BeneHhs",
        //                                                model.BeneHhs)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ExpPlanEqualShare",
        //                                                model.ExpPlanEqualShare)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ExpPlanPovertyPrioritized",
        //                                                model
        //                                                    .ExpPlanPovertyPrioritized)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentNumbers",
        //                                                model.EnrolmentNumbers)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentGroupId",
        //                                                model.EnrolmentGroupId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "UserId",
        //                                                userId)
        //                                    }
        //                            };

        //    var newModel = GenericService.GetOneBySp<SpIntVm>("AddEditEnrolmentPlan", parameterNames, parameterList);
        //    // TempData["ViewModel"] = JsonConvert.SerializeObject(newModel);
        //    TempData["MESSAGE"] = " The Enrolment was  successfully generated. The file must be approved!";
        //    TempData["KEY"] = "success";
        //    return RedirectToAction("Index");
        //}



        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var householdEnrolmentPlan = GenericService.GetOne<HouseholdEnrolmentPlan>(
        //        x => x.Id == id,
        //        "ApvByUser,CreatedByUser,ModifiedByUser,Status,EnrolmentGroup,RegGroup,Programme");

        //    if (householdEnrolmentPlan == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    return View(householdEnrolmentPlan);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public FileResult Download(int id, int? fileDownloadId)
        //{
        //    var file = GenericService.GetOne<FileCreation>(x => x.Id == id);
        //    var fileName = $"{WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]}{file.Name}";
        //    Response.AppendHeader("content-disposition", "attachment; filename=" + file.Name);
        //    Response.ContentType = "application/octet-stream";
        //    Response.WriteFile(fileName);
        //    Response.Flush();
        //    Response.End();
        //    return null;
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:MODIFIER")]
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var model = GenericService.GetOne<HouseholdEnrolmentPlan>(x => x.Id == id.Value);
        //    if (model == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    ViewBag.ProgrammeId = new SelectList(GenericService.Get<Programme>(), "Id", "Name", model.ProgrammeId);
        //    ViewBag.RegGroupId = new SelectList(
        //        GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "Registration Group"),
        //        "Id",
        //        "Description",
        //        model.RegGroupId);
        //    ViewBag.EnrolmentGroupId = new SelectList(
        //        GenericService.Get<SystemCodeDetail>(x => x.SystemCode.Code == "Enrolment Group"),
        //        "Id",
        //        "Description",
        //        model.EnrolmentGroupId);

        //    return View(model);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:MODIFIER")]
        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult Edit(HouseholdEnrolmentPlan model)
        //{
        //    var userId = int.Parse(User.Identity.GetUserId());
        //    var parameterNames =
        //        "@Id,@ProgrammeId,@RegGroupId,@RegGroupHhs,@BeneHhs,@ExpPlanEqualShare,@ExpPlanPovertyPrioritized,@EnrolmentNumbers,@EnrolmentGroupId,@UserId";
        //    var parameterList = new List<ParameterEntity>
        //                            {
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "Id",
        //                                                model.Id)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ProgrammeId",
        //                                                model.ProgrammeId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "RegGroupId",
        //                                                model.RegGroupId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "RegGroupHhs",
        //                                                model.RegGroupHhs)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "BeneHhs",
        //                                                model.BeneHhs)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ExpPlanEqualShare",
        //                                                model.ExpPlanEqualShare)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "ExpPlanPovertyPrioritized",
        //                                                model
        //                                                    .ExpPlanPovertyPrioritized)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentNumbers",
        //                                                model.EnrolmentNumbers)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentGroupId",
        //                                                model.EnrolmentGroupId)
        //                                    },
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "UserId",
        //                                                userId)
        //                                    }
        //                            };

        //    var newModel = GenericService.GetOneBySp<SpIntVm>("AddEditEnrolmentPlan", parameterNames, parameterList);
        //    TempData["MESSAGE"] = " The Enrolment was   was successfully Edited! ";
        //    TempData["KEY"] = "success";
        //    return RedirectToAction("Index");
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public ActionResult Export(int? id)
        //{
        //    var spName = "GetIndividualHHEnrolmentDetails";
        //    var parameterNames = "@HHenrolmentPlanId";
        //    var parameterList = new List<ParameterEntity>();
        //    if (id == null)
        //    {
        //        parameterList.Add(
        //            new ParameterEntity
        //            {
        //                ParameterTuple = new Tuple<string, object>(
        //                        "HHenrolmentPlanId",
        //                        DBNull.Value)
        //            });
        //    }
        //    else
        //    {
        //        parameterList.Add(
        //            new ParameterEntity { ParameterTuple = new Tuple<string, object>("HHenrolmentPlanId", id.Value) });
        //    }

        //    var newModel = GenericService.GetManyBySp<EnroledHhIndividual>(spName, parameterNames, parameterList);
        //    var dt = CreateDataTable(newModel);
        //    var wb = new XLWorkbook();
        //    wb.Worksheets.Add(dt, "Enrolled Beneficiaries");
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    Response.AddHeader("content-disposition",
        //        $"attachment;filename=EnroledBeneficiaries_{DateTime.Now:yy-MM-dd}.xlsx");

        //    var myMemoryStream = new MemoryStream();

        //    wb.SaveAs(myMemoryStream);
        //    myMemoryStream.WriteTo(Response.OutputStream);
        //    Response.Flush();
        //    Response.End();

        //    return View();
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public async Task<ActionResult> Files(int? page)
        //{
        //    var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
        //    var pageNum = page ?? 1;

        //    var plans = (await GenericService.GetAsync<FileCreation>(
        //                     x => x.Type.Code == "ENROLMENT",
        //                     x => x.OrderByDescending(y => y.Id),
        //                     "FileDownloads").ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
        //    return View(plans);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public async Task<ActionResult> Index(int? page)
        //{
        //    var pageSize = int.Parse(WebConfigurationManager.AppSettings["DEFAULT_PAGESIZE"]);
        //    var pageNum = page ?? 1;

        //    var plans = (await GenericService.GetAsync<HouseholdEnrolmentPlan>(
        //                         null,
        //                         x => x.OrderByDescending(y => y.Id),
        //                         "ApvByUser,CreatedByUser,ModifiedByUser,Status,EnrolmentGroup,RegGroup,Programme")
        //                     .ConfigureAwait(true)).ToPagedList(pageNum, pageSize);
        //    return View(plans);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public ActionResult Report(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var spName = "GetAllEnrolmentPlan";
        //    var parameterNames = "@EnrolmentPlanId";
        //    var parameterList = new List<ParameterEntity>
        //                            {
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentPlanId",
        //                                                id)
        //                                    }
        //                            };

        //    var results = new ApplicationDbContext().MultipleResults().With<EnrolmentProgress>()
        //        .With<BankEnrolmentProgress>().Execute(spName, parameterNames, parameterList);

        //    return View(results);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:VIEW")]
        //public ActionResult ReportByCounties(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var spName = "GetAllEnrolmentPlan";
        //    var parameterNames = "@EnrolmentPlanId";
        //    var parameterList = new List<ParameterEntity>
        //                            {
        //                                new ParameterEntity
        //                                    {
        //                                        ParameterTuple =
        //                                            new Tuple<string, object>(
        //                                                "EnrolmentPlanId",
        //                                                id)
        //                                    }
        //                            };

        //    var results = new ApplicationDbContext().MultipleResults().With<EnrolmentProgress>()
        //        .With<BankEnrolmentProgress>().Execute(spName, parameterNames, parameterList);

        //    return View(results);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:EXPORT")]
        //[HttpGet]
        //public async Task<ActionResult> ResendEnrolmentFile(int id)
        //{
        //    var fileService = new FileService();
        //    var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == id).ConfigureAwait(false);
        //    var model = new ResendEnrolmentFileViewModel
        //    {
        //        Id = file.Id,
        //        FileName = file.Name,
        //        UserId = User.Identity.GetUserId<int>()
        //    };
        //    return View(model);
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:EXPORT")]
        //[HttpPost, ValidateAntiForgeryToken]
        //public async Task<ActionResult> ResendEnrolmentFile(int id, ResendEnrolmentFileViewModel newModel)
        //{
        //    try
        //    {
        //        var fileId = id;
        //        if (fileId > 0)
        //        {
        //            var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == fileId)
        //                           .ConfigureAwait(false);

        //            // get the PSP User List
        //            var psps = await GenericService.GetAsync<Psp>(x => x.IsActive, null, "User").ConfigureAwait(false);
        //            foreach (var psp in psps)
        //            {
        //                await EmailService.SendAsync(
        //                    new PspEnrolmentFileEmail
        //                    {
        //                        FirstName = psp.User.DisplayName,
        //                        To = psp.User.Email,
        //                        PspName = psp.Name,
        //                        FileName = file.Name,
        //                        Subject = "Enrolment File  Ready for Carding - Reshared",
        //                        Title = "Enrolment File  Ready for Carding - Reshared",
        //                        FileChecksum = file.FileChecksum,
        //                        DateCreated = file.CreatedOn
        //                    }).ConfigureAwait(true);
        //            }

        //            // TempData["ViewModel"] = JsonConvert.SerializeObject(psps);
        //            TempData["MESSAGE"] =
        //                " A Enrolment File for all  approved plans Was Successfully send  to all ACTIVE PSPs.";
        //            TempData["KEY"] = "success";

        //            return RedirectToAction("Index");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["MESSAGE"] =
        //            "An Error Occured Prior to the Sharing of the File with the PSPs : " + ex.Message;
        //        TempData["KEY"] = "danger";
        //        return RedirectToAction("Index");
        //    }

        //    return RedirectToAction("Index");
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:EXPORT")]
        //public ActionResult ShareWithPsp()
        //{
        //    return View();
        //}

        //[GroupCustomAuthorize(Name = "ENROLMENT:EXPORT")]
        //[HttpPost, ValidateAntiForgeryToken]
        //public async Task<ActionResult> ShareWithPsp(int id)
        //{
        //    try
        //    {
        //        var conString = new ApplicationDbContext().Database.Connection.ConnectionString;
        //        SqlConnectionStringBuilder connection = new SqlConnectionStringBuilder(conString);

        //        var userId = int.Parse(User.Identity.GetUserId());
        //        var parameterNames = "@FilePath,@DBServer,@DBName,@DBUser,@DBPassword,@UserId";
        //        var parameterList = new List<ParameterEntity>
        //                                {
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "FilePath",
        //                                                    WebConfigurationManager
        //                                                        .AppSettings[
        //                                                            "DIRECTORY_SHARED_FILES"])
        //                                        },
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "DBServer",
        //                                                    connection
        //                                                        .DataSource)
        //                                        },
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "DBName",
        //                                                    connection
        //                                                        .InitialCatalog)
        //                                        },
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "DBUser",
        //                                                    connection.UserID)
        //                                        },
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "DBPassword",
        //                                                    connection.Password)
        //                                        },
        //                                    new ParameterEntity
        //                                        {
        //                                            ParameterTuple =
        //                                                new Tuple<string, object>(
        //                                                    "UserId",
        //                                                    userId)
        //                                        }
        //                                };
        //        var newModel = GenericService.GetOneBySp<SPOutput>(
        //            "GenerateEnrolmentFile",
        //            parameterNames,
        //            parameterList);
        //        var fileId = newModel.FileCreationId;
        //        if (fileId > 0)
        //        {
        //            var fileService = new FileService();
        //            var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == fileId)
        //                           .ConfigureAwait(false);
        //            try
        //            {
        //                file.FileChecksum = fileService.GetChecksum(file.FilePath + file.Name);
        //                GenericService.Update(file);

        //                var storedProcedureShare = "FlagFilesToShare";
        //                var parameterNamesShare = "@FileTypeCode";
        //                var parameterListShare =
        //                    new List<ParameterEntity>
        //                        {
        //                            new ParameterEntity
        //                                {
        //                                    ParameterTuple =
        //                                        new Tuple<string, object>(
        //                                            "FileTypeCode",
        //                                            "ENROLMENT")
        //                                }
        //                        };

        //                var newModelx = GenericService.GetOneBySp<SPOutput>(
        //                    storedProcedureShare,
        //                    parameterNamesShare,
        //                    parameterListShare);

        //                // get the PSP User List
        //                var psps = await GenericService.GetAsync<Psp>(x => x.IsActive, null, "User")
        //                               .ConfigureAwait(false);
        //                foreach (var psp in psps)
        //                {
        //                    await EmailService.SendAsync(
        //                        new PspEnrolmentFileEmail
        //                        {
        //                            FirstName = psp.User.DisplayName,
        //                            To = psp.User.Email,
        //                            PspName = psp.Name,
        //                            FileName = file.Name,
        //                            Subject = "Enrolment File  Ready for Carding",
        //                            Title = "Enrolment File  Ready for Carding",
        //                            FileChecksum = file.FileChecksum,
        //                            DateCreated = file.CreatedOn
        //                        }).ConfigureAwait(true);
        //                }

        //                // TempData["ViewModel"] = JsonConvert.SerializeObject(newModel);
        //                TempData["MESSAGE"] =
        //                    " A Enrolment File for all  approved plans Was Generated Successfully Approved. The same has been communicated to the Active PSPs ";
        //                TempData["KEY"] = "success";

        //                return RedirectToAction("Index");
        //            }
        //            catch (Exception ex)
        //            {
        //                TempData["MESSAGE"] = "An Error Occured Prior to the Sharing of the File with the PSPs : "
        //                                      + ex.Message;
        //                TempData["KEY"] = "danger";
        //                return RedirectToAction("Index");
        //            }
        //        }
        //    }
        //    catch (SqlException exc)
        //    {
        //        // TempData["ViewModel"] = JsonConvert.SerializeObject(exc);
        //        TempData["MESSAGE"] = " There was no Enrolment File Generated. " + exc.Message;
        //        TempData["KEY"] = "danger";
        //    }
        //    catch (Exception excs)
        //    {
        //        // TempData["ViewModel"] = JsonConvert.SerializeObject(excs);
        //        TempData["MESSAGE"] = " There was no Enrolment File Generated. " + excs.Message;
        //        TempData["KEY"] = "danger";
        //        return RedirectToAction("Index");
        //    }

        //    return RedirectToAction("Index");
        //}

        //public ActionResult Summary()
        //{
        //    var spName = "GetAllEnrolmentPlan";
        //    var parameterNames = string.Empty;
        //    var parameterList = new List<ParameterEntity>();
        //    var results = new ApplicationDbContext().MultipleResults().With<EnrolmentProgress>()
        //        .With<BankEnrolmentProgress>().Execute(spName, parameterNames, parameterList);
        //    return View(results);
        //}

        public ActionResult Verify(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var model = GenericService.GetOne<FileCreation>(x => x.Id == id);
            return View(model);
        }

        [HttpGet]
        [ClientErrorHandler]
        public ActionResult VerifyCheckSum(int? userId, string fileChecksum, int fileCreationId)
        {
            var userId2 = int.Parse(User.Identity.GetUserId());
            var sp = "BeneEnrolFileDownloaded";
            var spParameters = "@FileCreationId,@FileCheckSum,@UserId";
            var spParameterList = new List<ParameterEntity>
                                      {
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCreationId",
                                                          fileCreationId)
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCheckSum",
                                                          fileChecksum.Trim())
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "UserId",
                                                          userId2)
                                              }
                                      };

            var hhEPvm = GenericService.GetOneBySp<PassWordVm>(sp, spParameters, spParameterList);

            // var psp = await GenericService.GetOneAsync<Psp>(x => x.UserId == userId2, "User").ConfigureAwait(false);
            // var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == userId2, "User").ConfigureAwait(false);

            /*
            await EmailService.SendAsync(
                    new PspEnrolmentVerifiedFileEmail
                    {
                        FirstName = fileDownload.DownloadedByUser.DisplayName,
                        To = fileDownload.DownloadedByUser.Email,
                        // PspName = fileDownload.DownloadedByUser.Psps.First().Name,
                        FileName = fileDownload.FileCreation.Name,
                        Subject = $"Enrolment File  Access Password",
                        Title = $"Enrolment File  Access Password",
                        FileChecksum = fileDownload.FileCreation.FileChecksum,
                        DateCreated = fileDownload.FileCreation.CreatedOn
                    })
                .ConfigureAwait(true);

                */
            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}