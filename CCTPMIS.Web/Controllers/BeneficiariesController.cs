﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Business.Statics;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    public class BeneficiariesController : Controller
    {

        protected readonly IGenericService GenericService;
        private ApplicationDbContext db = new ApplicationDbContext();
        public BeneficiariesController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        public ActionResult Index()
        {
            return User.UserGroupHasRight("PSP-PAYMENT:VIEW")
                ? (ActionResult)RedirectToAction("Details", "Default", new { area = "Psp" })
                : View();
        }


        [HttpPost]
        public JsonResult FirstChart()
        {
            var BeneIndicatorFirst = db.Indicators.FirstOrDefault(x => x.Code == "BENE_EVER_PAID");
            var defaultReportingPeriod = db.MonitoringPeriods.FirstOrDefault(x => x.IsActive);
            var data = db.PeriodicConstituencyBeneIndicators.Where(x => x.IndicatorId == BeneIndicatorFirst.Id).Include(x => x.Constituency).ToList();
            var Counties = db.Counties.Select(x => new IdName { Id = x.Id, Name = x.Name });
            var Programmes = db.Programmes.Select(x => new ProgName { Id = x.Id, Name = x.Name });
            double scaleTotal = data.Sum(d => d.Total);
            scaleTotal = (int)(Math.Ceiling((decimal)scaleTotal / 1000) * 1000);
            var list = new {
                Male = data.Sum(d => d.Male),
                Female = data.Sum(d => d.Female),
                Total = data.Sum(d => d.Total),
                Indicators = db.Indicators.Where(i => i.Name.Contains("BENE")),
                Programmes ,
                Periods = db.MonitoringPeriods.OrderByDescending(x => x.Id),
                Counties,
                ScaleTotal = scaleTotal
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RefreshData(DashboardsFilterViewModel vm)
        {

            Expression<Func<PeriodicConstituencyBeneIndicator, bool>> filter = x=>x.MonitoringPeriodId == vm.MonitoringPeriodId && x.IndicatorId == vm.IndicatorId;
            Expression<Func<PeriodicConstituencyBeneIndicator, bool>> filterPlus = null;

            if (vm.CountyId != null)
            {
                filterPlus = x => x.Constituency.CountyId == vm.CountyId;
                filter = filter.And(filterPlus);
            }
            if (vm.ProgrammeId != null)
            {
                filterPlus = x => x.ProgrammeId == vm.ProgrammeId;
                filter = filter.And(filterPlus);
            }

            var data = db.PeriodicConstituencyBeneIndicators.Where(filter).Include(x => x.Constituency).ToList();

            var Counties = db.Counties.Select(x => new IdName { Id = x.Id, Name = x.Name });
            var Programmes = db.Programmes.Select(x => new ProgName { Id = x.Id, Name = x.Name });
            double scaleTotal = data.Sum(d => d.Total);
            scaleTotal = (int)(Math.Ceiling((decimal)scaleTotal / 1000) * 1000);
            var list = new {
                Male = data.Sum(d => d.Male),
                Female = data.Sum(d => d.Female),
                Total = data.Sum(d => d.Total),

                Indicators = db.Indicators.Where(i => i.Name.Contains("BENE")),
                Programmes,
                Periods = db.MonitoringPeriods.OrderByDescending(x => x.Id),
                Counties,
              
                ScaleTotal = scaleTotal
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

    public class IdName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ProgName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class DashboardsFilterViewModel
    {
        public int MonitoringPeriodId { get; set; }
        public int IndicatorId { get; set; }
        public int? CountyId { get; set; }
        public int? PeriodId { get; set; }
        public byte? ProgrammeId { get; set; }

    }


}