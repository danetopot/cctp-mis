﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.Mvc;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;

namespace CCTPMIS.Web.Controllers
{
    //[Authorize]
    //[AuthorizeSingleLogin]
    public class ServicesController : Controller
    {
        protected readonly IEmailService EmailService;

        protected readonly IGenericService GenericService;

        public ServicesController(IGenericService genericService, EmailService emailService)
        {
            GenericService = genericService;
            this.EmailService = emailService;
        }

        [Authorize]
        public FileResult Download(int id)
        {
            var file = GenericService.GetOne<FileCreation>(x => x.Id == id);
            var fileName = $"{WebConfigurationManager.AppSettings["DIRECTORY_SHARED_FILES"]}{file.Name}";
            Response.AppendHeader("content-disposition", "attachment; filename=" + file.Name);
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(fileName);
            Response.Flush();
            Response.End();
            return null;
        }

       [Authorize]

        // GET: Services
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Verify(int id)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var model = GenericService.GetOne<FileCreation>(x => x.Id == id);
            return View(model);
        }

        [HttpGet]
        [ClientErrorHandler]
        public ActionResult VerifyCheckSum(int? userId, string fileChecksum, int fileCreationId)
        {
            var userId2 = int.Parse(User.Identity.GetUserId());
            var sp = "BeneEnrolFileDownloaded";
            var spParameters = "@FileCreationId,@FileCheckSum,@UserId";
            var spParameterList = new List<ParameterEntity>
                                      {
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCreationId",
                                                          fileCreationId)
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "FileCheckSum",
                                                          fileChecksum.Trim())
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "UserId",
                                                          userId2)
                                              }
                                      };

            var hhEPvm = GenericService.GetOneBySp<PassWordVm>(sp, spParameters, spParameterList);

            // var psp = await GenericService.GetOneAsync<Psp>(x => x.UserId == userId2, "User").ConfigureAwait(false);
            // var file = await GenericService.GetOneAsync<FileCreation>(x => x.Id == userId2, "User").ConfigureAwait(false);

            /*
            await EmailService.SendAsync(
                    new PspEnrolmentVerifiedFileEmail
                    {
                        FirstName = fileDownload.DownloadedByUser.DisplayName,
                        To = fileDownload.DownloadedByUser.Email,
                        // PspName = fileDownload.DownloadedByUser.Psps.First().Name,
                        FileName = fileDownload.FileCreation.Name,
                        Subject = $"Enrolment File  Access Password",
                        Title = $"Enrolment File  Access Password",
                        FileChecksum = fileDownload.FileCreation.FileChecksum,
                        DateCreated = fileDownload.FileCreation.CreatedOn
                    })
                .ConfigureAwait(true);

                */
            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }
    }
}