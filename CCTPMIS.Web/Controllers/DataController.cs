﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.CaseManagement;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Models.Reports;
using CCTPMIS.Models.SingleRegistry;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    public class DataController : Controller
    {
        protected readonly IGenericService GenericService;
        protected readonly ISingleRegistryService SingleRegistryService;
        public DataController()
        {

        }
        public DataController(IGenericService genericService, ISingleRegistryService singleRegistryService)
        {
            GenericService = genericService;
            SingleRegistryService = singleRegistryService;
        }

        [HttpGet]

        public async Task<JsonResult> GetCounties()
        {
            var counties = await GenericService.GetAsync<County>();
            return Json(counties, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]

        public async Task<JsonResult> GetDateRange(string id)
        {
            var sp = "GetDateRange";
            var spParameters = "@DateRange";
            var spParameterList = new List<ParameterEntity>
            {
                new ParameterEntity
                {
                    ParameterTuple =
                        new Tuple<string, object>(
                            "DateRange",
                            id)
                },
            };
            var hhEPvm = GenericService.GetOneBySp<DateRange>(sp, spParameters, spParameterList);

            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]

        public async Task<JsonResult> GetConstituencies(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {


                if (id.Contains(","))
                {
                    var countyIds = new List<int>(id.Split(',').Select(int.Parse));
                    var constituencies = (await GenericService.GetAsync<Constituency>(x => countyIds.Contains(x.CountyId))
                       .ConfigureAwait(false)).Select(t => new {t.Id, t.Code, t.Name, t.CountyId });
                    return Json(constituencies, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var realId = int.Parse(id);
                    var constituencies = (  (await GenericService.GetAsync<Constituency>(x => x.CountyId == realId).ConfigureAwait(false)).Select(t=>new { t.Id, t.Code,t.Name,t.CountyId})
                        );

                    return Json(constituencies, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]

        public async Task<JsonResult> GetSubLocations(int id)
        {
            var subLocations = await GenericService.GetAsync<SubLocation>(x => x.ConstituencyId == id);

            return Json(subLocations, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult Get(int id, int regGroupId)
        {
            var sp = "GetBeneForEnrolment";
            var spParameters = "@ProgrammeId,@RegGroupId";
            var spParameterList = new List<ParameterEntity>
                                      {
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "ProgrammeId",
                                                          id)
                                              },
                                          new ParameterEntity
                                              {
                                                  ParameterTuple =
                                                      new Tuple<string, object>(
                                                          "RegGroupId",
                                                          regGroupId)
                                              }
                                      };

            var hhEPvm = GenericService.GetOneBySp<HouseholdEnrolmentPlanViewModel>(sp, spParameters, spParameterList);

            return Json(hhEPvm, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public async Task<JsonResult> GetPersonDetails(int id, int? mode)
        {;
            if (mode == 1)
            {
                Expression<Func<HouseholdMember, bool>> filter = x => x.Person.Id == id;
                var results_extra = (await GenericService.GetSearchableQueryable(filter, x => x.OrderByDescending(y => y.Id), "Relationship,MemberRole,Person,Person.Sex,Status").ConfigureAwait(false));
                return Json(results_extra, JsonRequestBehavior.AllowGet);
            }


            var results = (await GenericService.GetOneAsync<Person>(x => x.Id == id)
                .ConfigureAwait(false));

            return Json(results, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public async Task<JsonResult> GetUserGroupType(int id)
        {

            var results = (await GenericService.GetOneAsync<UserGroup>(x => x.Id == id, "UserGroupType")
                .ConfigureAwait(false));
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetUserRole(int id)
        {

            var spName = "GetUserRole";
            var parameterNames = "@UserId";
            var parameterList = new List<ParameterEntity>
            {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("UserId",id)},
                };

            var userRoles = GenericService.GetManyBySp<UserRole>(spName, parameterNames, parameterList).ToList();


            return Json(userRoles, JsonRequestBehavior.AllowGet);
        }




        //SecReciDoB/
        [HttpGet]
        public async Task<JsonResult> GetCaregiver(string id)
        {
            var progOfficer = User.GetProgOfficerSummary();
            var userId = User.Identity.GetUserId();
            var cgIPRSData = new IprsCacheVm();
            try
            {
                var spName = "IPRSCheckmember";
                var parameterNames = "@IdNumber";
                var parameterList = new List<ParameterEntity>
                {
                    new ParameterEntity{ParameterTuple =new Tuple<string, object>("IdNumber",id)},
                };

                var ipCaches = GenericService.GetManyBySp<IprsCacheVm>(spName, parameterNames, parameterList).ToList();

                if (ipCaches.Any())
                {
                    cgIPRSData = ipCaches.First();
                }
                else
                {


                    if (string.IsNullOrEmpty(cgIPRSData.ID_Number))
                    {
                        var IprsList = new List<IprsCache>();
                        var iprsCache = new IprsCache();

                        var login = new LoginVm {
                            Password = WebConfigurationManager.AppSettings["SR_PASSWORD"],
                            UserName = WebConfigurationManager.AppSettings["SR_USERNAME"]
                        };
                        var auth = await SingleRegistryService.Login(login);
                        if (auth.TokenAuth != null)
                        {

                            var cg = new VerificationSrPostVm {
                                TokenCode = auth.TokenAuth,
                                IDNumber = id,
                                Names = ""
                            };
                            var hhdIprs = await SingleRegistryService.IprsVerification(cg);
                            if (string.IsNullOrEmpty(hhdIprs.ID_Number))
                            {
                                return Json(cgIPRSData, JsonRequestBehavior.AllowGet);
                            }

                            if (!DateTime.TryParse(hhdIprs.Date_of_Birth, result: out var dateTime))
                            {
                                return Json(cgIPRSData, JsonRequestBehavior.AllowGet);
                            }

                            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(hhdIprs, iprsCache);
                            IprsList.Add(iprsCache);
                            iprsCache = new IprsCache();

                            IprsList.ForEach(x => x.DateCached = DateTime.Now);
                            var dt = Statics.ToDataTable(IprsList);
                            var db = new ApplicationDbContext();
                            using (var bulkCopy = new SqlBulkCopy(db.Database.Connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                            {
                                foreach (DataColumn col in dt.Columns)
                                {
                                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                                }

                                bulkCopy.BulkCopyTimeout = 600;
                                bulkCopy.DestinationTableName = "temp_IPRSCache";
                                await bulkCopy.WriteToServerAsync(dt).ConfigureAwait(false);
                                Thread.Sleep(1000);
                            }

                            spName = "ImportBulkIPRS";
                            parameterNames = "";
                            parameterList = new List<ParameterEntity> { };
                            GenericService.GetOneBySp<SPOutput>(spName, parameterNames, parameterList);

                            spName = "IPRSCheckmember";
                            parameterNames = "@IdNumber";
                            parameterList = new List<ParameterEntity>
                        {
                            new ParameterEntity{ParameterTuple =new Tuple<string, object>("IdNumber",id)},
                        };
                            ipCaches = GenericService.GetManyBySp<IprsCacheVm>(spName, parameterNames, parameterList).ToList();
                            if (ipCaches.Any())
                            {
                                cgIPRSData = ipCaches.First();
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {

            }

            return Json(cgIPRSData, JsonRequestBehavior.AllowGet);
        }



        public async Task<ActionResult> GetCaseTypeCategories(int id)
        {
            var results = (await GenericService.GetAsync<SystemCodeDetail>(x => x.SystemCodeId == id)
                .ConfigureAwait(false)).Select(x => new { x.Id, x.Code, x.Description });
            return Json(results, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> GetCMRequirements(int id)
        {
            var results = (await GenericService.GetOneAsync<CaseCategory>(x => x.Id == id)
                .ConfigureAwait(false));
            return Json(results, JsonRequestBehavior.AllowGet);
        }

    }
}