﻿using System.Web.Mvc;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    //[AuthorizeSingleLogin]
    public class MessagesController : Controller
    {
        protected readonly IGenericService GenericService;

        public MessagesController(IGenericService genericService)
        {
            GenericService = genericService;
        }

        // GET: Messages
        public ActionResult Index()
        {
            return View();
        }
    }
}