﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CaptchaMvc.HtmlHelpers;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.Account;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Models.Email;
using CCTPMIS.Services;
using Elmah;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace CCTPMIS.Web.Controllers
{


    [Authorize]
    public class AccountController : Controller
    {
        private const string XsrfKey = "XsrfId";

        private ApplicationSignInManager _signInManager;

        private ApplicationUserManager _userManager;

        private ApplicationDbContext db = new ApplicationDbContext();

        private IEmailService EmailService;

        private ILogService LogService;

        private IGenericService GenericService;

        public AccountController(GenericService uow)
        {
            GenericService = uow;
        }

        public AccountController(
            ApplicationUserManager userManager,
            ApplicationSignInManager signInManager,
            IGenericService uow,
            EmailService emailService,
            LogService logService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            GenericService = uow;
            EmailService = emailService;
            LogService = logService;

            UserManager.MaxFailedAccessAttemptsBeforeLockout = 5;
            UserManager.UserLockoutEnabledByDefault = true;

            UserManager.DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(60);

            UserManager.UserValidator =
                new UserValidator<ApplicationUser, int>(UserManager) {
                    AllowOnlyAlphanumericUserNames = false,
                    RequireUniqueEmail = true
                };

            // Configure validation logic for passwords
            UserManager.PasswordValidator = new PasswordValidator {
                RequiredLength = 8,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }

            private set
            {
                _signInManager = value;
            }
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                // return   new ApplicationUserManager(new ApplicationUserStore(HttpContext.GetOwinContext().Get<ApplicationDbContext>()));
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [AllowAnonymous]
        public ActionResult AccountLocked(string id)
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ChangePassword(int id)
        {
            if (id <= 0)
            {
                return HttpNotFound();
            }

            var user = GenericService.GetOne<ApplicationUser>(x => x.Id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            var model = new ChangePasswordViewModel();

            // (cfg => cfg.ValidateInlineMaps = false)
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, model);

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (model.NewPassword == model.OldPassword)
            {
                ViewBag.Error = "The New Password must not be the current password!";
                return View(model);
            }

            if (!ModelState.IsValid)
            {


                return View(model);
            }


            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == model.Id);
            var databefore = JsonConvert.SerializeObject(user);

            var result = await UserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                try
                {
                    var hashedPassword = UserManager.PasswordHasher.HashPassword(model.NewPassword);
                    user.PasswordChangeDate = DateTime.Now;
                    user.PasswordHash = hashedPassword;
                    user.IsActive = true;
                    GenericService.AddOrUpdate(user);

                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,

                        TableName = "User",
                        ModuleRightCode = "SYSTEM:CHANGE PASSWORD",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                        WasSuccessful = true,
                        Description = "Change Password Success"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    TempData["KEY"] = "success";
                    TempData["MESSAGE"] = "Password was successfully changed";

                    return RedirectToAction("Login");

                }
                catch (Exception exc)
                {
                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "USER PROFILE:CHANGE PASSWORD",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                        WasSuccessful = false,
                        Description = "Change Password Failed"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "An error occured and the password was not changed ";
                    ErrorLog.GetDefault(null).Log(new Error(exc));
                }
                await EmailService.SendAsync(
                    new PasswordChangeSuccessEmail {
                        FirstName = user.Email,
                        To = user.Email,
                        Subject = "Password Changed Successfully",
                        Title = "Password Changed Successfully"
                    }).ConfigureAwait(true);
                return View(model);
            }

            AddErrors(result);
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int? userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("ConfirmError");
            }

            try
            {

                var user = await GenericService.GetByIdAsync<ApplicationUser>(userId).ConfigureAwait(false);

                var result = await UserManager.ConfirmEmailAsync(userId.Value, code);

                if (result.Succeeded)
                {
                    user = await GenericService.GetByIdAsync<ApplicationUser>(userId).ConfigureAwait(false);

                    var databefore = JsonConvert.SerializeObject(user);
                    user.LockoutEnabled = true;
                    user.LoginDate = DateTime.Now;
                    user.IsActive = true;
                    user.PasswordChangeDate = null;

                    GenericService.Update(user);


                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "SYSTEM:CHANGE PASSWORD",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                        WasSuccessful = true,
                        Description = "Confirm Email Success"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    TempData["MESSAGE"] = "The Email  was successfully confirmed";
                    TempData["key"] = "success";
                    return View(result.Succeeded ? "ConfirmEmail" : "ConfirmError");
                }

                TempData["MESSAGE"] = $"The Email was not confirmed. ";
                TempData["key"] = "danger";
                AddErrors(result);
                return View(result.Succeeded ? "ConfirmEmail" : "ConfirmError");
            }
            catch (Exception ex)
            {
                TempData["MESSAGE"] = "The Email was not confirmed.";
                TempData["key"] = "danger";

                ErrorSignal.FromCurrentContext().Raise(ex);
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = userId,
                    TableName = "User",
                    ModuleRightCode = "SYSTEM:CONFIRM EMAIL",
                    Record = $"[]",
                    WasSuccessful = true,
                    Description = "Confirm Email Failed"
                };
                LogService.AuditTrail(auditTrailVm);
                return View("ConfirmError");
            }

        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // generate the reset code
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id).ConfigureAwait(true);
                var validity = int.Parse((await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "PWDRESETVALIDITYMINS").ConfigureAwait(false)).Description);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code }, Request.Url.Scheme);

                try
                {


                    // save the reset code for logs purposes
                    var passwordReset = new PasswordReset {
                        RequestTime = DateTime.Now,
                        ResetCode = code,
                        ValidityPeriod = validity,
                        UserId = user.Id
                    };

                    GenericService.Create(passwordReset);

                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "PasswordReset",
                        ModuleRightCode = "SYSTEM:RESET PASSWORD",
                        Record = $"{JsonConvert.SerializeObject(user)}",
                        WasSuccessful = false,
                        Description = "Password Reset Success"
                    };
                    LogService.AuditTrail(auditTrailVm);

                    await EmailService.SendAsync(
                    new ForgotPasswordEmail {
                        FirstName = model.Email,
                        To = model.Email,
                        ValidityPeriod = validity,
                        ConfirmationLink = callbackUrl,
                        Subject = "Forgot your Password"
                    }).ConfigureAwait(true);


                }
                catch (Exception e)
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = "An error Occurred. ";

                    ErrorSignal.FromCurrentContext().Raise(e);
                }
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var model = new LoginViewModel {
                //    RecaptchaKey =
                //                        GenericService
                //                            .GetOne<SystemCodeDetail>(x => x.Code == "RECAPTCHAKEY")
                //                            .Description
            };

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            var isCaptchaValid = this.IsCaptchaValid("Captcha is valid");
            if (!isCaptchaValid)
            {
                ModelState.AddModelError(string.Empty, "Confirm you are not a bot. Fill the Captcha correctly.");
                return View(model);
            }


            var user = await UserManager.FindByNameAsync(model.Email).ConfigureAwait(false);
            var databefore = JsonConvert.SerializeObject(user);
            if (user != null)
            {
                var validCredentials = await UserManager.FindAsync(model.Email, model.Password).ConfigureAwait(false);

                // await UserManager.UpdateSecurityStampAsync(user.Id);

                // When a user is lockedout, this check is done to ensure that even if the credentials are valid
                // the user can not login until the lockout duration has passed
                if (await UserManager.IsLockedOutAsync(user.Id).ConfigureAwait(false))
                {
                    ModelState.AddModelError(
                        string.Empty,
                        "Your account has been locked due to multiple failed login attempts. Contact System Administrator ");
                }

                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    ModelState.AddModelError("", "You need to confirm your email. Check on the welcome email and Confirm your email address.");
                    return View(model);
                }

                // if user is subject to lockouts and the credentials are invalid
                // record the failure and check if user is lockedout and display message, otherwise,
                // display the number of attempts remaining before lockout
                else if (await UserManager.GetLockoutEnabledAsync(user.Id) && validCredentials == null)
                {
                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "SYSTEM:LOGIN",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                        WasSuccessful = true,
                        Description = "Login Success"
                    };
                    LogService.AuditTrail(auditTrailVm);
                    // Record the failure which also may cause the user to be locked out
                    await UserManager.AccessFailedAsync(user.Id);
                    string message;
                    if (await UserManager.IsLockedOutAsync(user.Id))
                    {
                        message = "Your account has been locked out due to multiple failed login attempts. Contact System Administrator";
                    }
                    else
                    {
                        var accessFailedCount = await UserManager.GetAccessFailedCountAsync(user.Id);
                        var attemptsLeft = 3 - accessFailedCount;
                        if (attemptsLeft > 0)
                        {
                            message = $"Invalid credentials. You have {attemptsLeft} more attempt(s) before your account gets locked out.";
                        }
                        else
                        {
                            UserManager.SetLockoutEndDate(
                                user.Id,
                                DateTime.UtcNow.AddDays(
                                    int.Parse(
                                        (await GenericService
                                             .GetOneAsync<SystemCodeDetail>(x => x.Code == "ACCINACTIVEDAYS")
                                             .ConfigureAwait(false)).Description)));

                            auditTrailVm = new AuditTrailVm {
                                UserId = $"{user.Id}",
                                Key1 = user.Id,
                                TableName = "User",
                                ModuleRightCode = "SYSTEM:LOGIN",
                                Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                                WasSuccessful = true,
                                Description = "User Lock out"
                            };
                            LogService.AuditTrail(auditTrailVm);


                            message =
                                "Your account has been locked out due to multiple failed login attempts. Contact System Administrator";



                        }
                    }

                    ModelState.AddModelError(string.Empty, message);
                }
                else if (validCredentials == null)
                {
                    ModelState.AddModelError(string.Empty, "Invalid credentials. Please try again.");
                }
                else
                {
                    // This doesn't count login failures towards account lockout
                    // To enable password failures to trigger account lockout, change to shouldLockout: true
                    var result = await SignInManager.PasswordSignInAsync(
                                     model.Email,
                                     model.Password,
                                     model.RememberMe,
                                     shouldLockout: true).ConfigureAwait(true);

                    switch (result)
                    {
                        case SignInStatus.Success:
                            {
                                await UserManager.ResetAccessFailedCountAsync(user.Id);

                                var auditTrailVm = new AuditTrailVm {
                                    UserId = $"{user.Id}",
                                    Key1 = user.Id,
                                    TableName = "User",
                                    ModuleRightCode = "SYSTEM:LOGIN",
                                    Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                                    WasSuccessful = true,
                                    Description = "User Lock out"
                                };

                                LogService.AuditTrail(auditTrailVm);
                                var passwordResetDays = int.Parse((await GenericService.GetOneAsync<SystemCodeDetail>(x => x.Code == "PWDEXPIRYDAYS").ConfigureAwait(false)).Description);
                                if (user.PasswordChangeDate == null || ((DateTime)user.PasswordChangeDate).AddDays(passwordResetDays)
                                    < DateTime.Now)
                                {
                                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                                    return RedirectToAction("ChangePassword", new { id = user.Id });
                                }

                                // Check Last Activity Date
                                var accountIdleDays = int.Parse(
                                    (await GenericService
                                         .GetOneAsync<SystemCodeDetail>(x => x.Code == "ACCINACTIVEDAYS")
                                         .ConfigureAwait(false)).Description);
                                if (user.ActivityDate != null
                                    && ((DateTime)user.ActivityDate).AddDays(accountIdleDays) < DateTime.Now)
                                {
                                    user.LockoutEndDateUtc = DateTime.Now.AddYears(
                                        int.Parse(
                                            (await GenericService
                                                 .GetOneAsync<SystemCodeDetail>(x => x.Code == "ACCINACTIVEDAYS")
                                                 .ConfigureAwait(false)).Description));



                                    return RedirectToAction("AccountLocked", new { id = user.Id });
                                }

                                user.LoginDate = DateTime.Now;
                                user.IsActive = true;
                                user.SecurityStamp = Guid.NewGuid().ToString();

                                GenericService.AddOrUpdate(user);


                                auditTrailVm = new AuditTrailVm {
                                    UserId = $"{user.Id}",
                                    Key1 = user.Id,
                                    TableName = "User",
                                    ModuleRightCode = "SYSTEM:LOGIN",
                                    Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                                    WasSuccessful = true,
                                    Description = "User Lock out"
                                };

                                LogService.AuditTrail(auditTrailVm);

                                Session[$"USER_{user.Id}"] = null;

                                var redirectLink = User.UserGroupHasRight("PSP-PROFILE:VIEW") ? Url.Action("Details", "Default", new { area = "Psp" }) : "~/";
                                return Redirect(returnUrl ?? redirectLink);
                            }

                        case SignInStatus.LockedOut:
                            return View(!user.IsActive ? "Deactivated" : "Lockout");

                        case SignInStatus.RequiresVerification:
                            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });

                        case SignInStatus.Failure:
                            //    model.RecaptchaKey = recaptchaKey;
                            return View(model);

                        default:
                            {
                                //    model.RecaptchaKey = recaptchaKey;
                                return View(model);
                            }
                    }
                    // }
                }
            }

            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            // model.RecaptchaKey = recaptchaKey;
            return View(model);
        }

        [HttpGet]
        public ActionResult LogOff(string id)
        {
            return RedirectToAction("Index", "Home");
        }
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {


            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                var auditTrailVm = new AuditTrailVm {
                    UserId = $"{userId}",
                    Key1 = int.Parse(userId),
                    TableName = "User",
                    ModuleRightCode = "SYSTEM:LOG OFF",
                    Record = $"[]",
                    WasSuccessful = true,
                    Description = "User Log Off"
                };

                LogService.AuditTrail(auditTrailVm);

                Session[$"USER_{userId}"] = null;
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            }

            TempData["MESSAGE"] = "You have successfully logged out. As an added privacy measure, " +
                "please close all Tabs and Browsers after each session to clear your Cache/Cookies.";

            TempData["KEY"] = "info";

            return RedirectToAction("Login", "Account");
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("ResetPasswordError") : View();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {


                var user = await UserManager.FindByNameAsync(model.Email);
                var databefore = JsonConvert.SerializeObject(user);
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    return RedirectToAction("ResetPasswordError", "Account");
                }

                var passwordReset = await GenericService
                                        .GetOneAsync<PasswordReset>(x => x.UserId == user.Id && x.ResetCode == model.Code)
                                        .ConfigureAwait(false);

                var passBefore = JsonConvert.SerializeObject(user);


                if (passwordReset == null || passwordReset.RequestTime.AddHours(passwordReset.ValidityPeriod) < DateTime.Now
                                          || passwordReset.IsReset || passwordReset.ResetTime != null)
                {
                    ViewBag.Error = "The Code was not found or has expired";

                    return RedirectToAction("ResetPasswordError", "Account");
                }

                var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password).ConfigureAwait(true);
                if (result.Succeeded)
                {
                    user = await UserManager.FindByNameAsync(model.Email);

                    passwordReset.IsReset = true;
                    passwordReset.ResetTime = DateTime.Now;
                    GenericService.Update(passwordReset);

                    var dbUser = await GenericService.GetFirstAsync<ApplicationUser>(x => x.Id == user.Id).ConfigureAwait(false);

                    dbUser.PasswordChangeDate = DateTime.Now;
                    dbUser.IsActive = true;
                    dbUser.LoginDate = DateTime.Now;
                    dbUser.AccessFailedCount = 0;
                    dbUser.LockoutEndDateUtc = null;
                    dbUser.IsLocked = false;

                    GenericService.Update(dbUser);
                    var auditTrailVm = new AuditTrailVm {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "SYSTEM:CHANGE PASSWORD",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)},{passBefore},{passwordReset}]",
                        WasSuccessful = true,
                        Description = "Password Reset Success"
                    };

                    LogService.AuditTrail(auditTrailVm);



                    await EmailService.SendAsync(
                        new PasswordChangeSuccessEmail {
                            FirstName = user.DisplayName,
                            To = user.Email,
                            Subject = "Password Reset Successfully",
                            Title = "Password Reset Successfully"
                        }).ConfigureAwait(false);

                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }

                AddErrors(result);
            }
            catch (Exception ex)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred during the Password Change Process";
                ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));
            }
            return View();
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordError(string code, string userId)
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                    db.Dispose();
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }

        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }

            public string RedirectUri { get; set; }

            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }

                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        public ActionResult SendCode(string returnurl)
        {
            //throw new NotImplementedException();

            return View();
        }
    }
}