﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Statics;
using CCTPMIS.Models.Users;
using CCTPMIS.Services;

namespace CCTPMIS.Web.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            var model = new Pin
            {

            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(Pin model)
        {
           // var enumeratorService = new EnumeratorService(new ApplicationDbContext());
           // model.HashedPassword = enumeratorService.GeneratePin(4);
            model.HashedPassword = EasyMD5.Hash(model.Password);
            
            return View(model);
        }

    }
}