﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Model;
using CCTPMIS.Models.Account;
using CCTPMIS.Models.AuditTrail;
using CCTPMIS.Services;
using Elmah;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;

namespace CCTPMIS.Web.Controllers
{
    [Authorize]
    //[AuthorizeSingleLogin]
    public class ManageController : Controller
    {
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        public GenericService GenericService;
        private ILogService LogService;
        private ApplicationSignInManager _signInManager;

        private ApplicationUserManager _userManager;

        // public ManageController()
        // {
        // }
        public ManageController(
            ApplicationUserManager userManager,
            ApplicationSignInManager signInManager,
            LogService logService,
            GenericService genericService)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            GenericService = genericService;
            LogService = logService;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,

            ChangePasswordSuccess,

            SetTwoFactorSuccess,

            SetPasswordSuccess,

            RemoveLoginSuccess,

            RemovePhoneSuccess,

            Error
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }

            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }

            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        /*
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(
                           User.Identity.GetUserId<int>(),
                           model.Number);
            if (UserManager.SmsService != null)
            {
                var message =
                    new IdentityMessage { Destination = model.Number, Body = "Your security code is: " + code };
                await UserManager.SmsService.SendAsync(message);
            }

            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }
        */
        public async Task<ActionResult> Avatar()
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == userId).ConfigureAwait(false);
            var model = new AvatarUploadViewModel
            {
                Avatar = user.DisplayAvatar,
                ProfileImage = user.DisplayName,
                DisplayAvatar = user.DisplayAvatar
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Avatar(AvatarUploadViewModel model)
        {
            try
            {


                var fakeUserId = User.Identity.GetUserId();
                if (model.Upload != null && model.Upload.ContentLength > 0)
                {
                    var originalavatar = fakeUserId + model.Upload.FileName;
                    var resizedavatar = fakeUserId + '_' + model.Upload.FileName;

                    var uploadDir = "~/uploads/Images/";
                    var uploadDirPath = Server.MapPath(uploadDir);
                    var uploadoriginalavatarFilePath = Path.Combine(Server.MapPath(uploadDir), originalavatar);
                    var uploadresizedavatarFilePath = Path.Combine(Server.MapPath(uploadDir), resizedavatar);

                    if (!Directory.Exists(uploadDirPath))
                    {
                        Directory.CreateDirectory(uploadDirPath);
                    }

                    model.Upload.SaveAs(uploadoriginalavatarFilePath);
                    GetCroppedAvatar(uploadoriginalavatarFilePath, uploadresizedavatarFilePath, 75, false);

                    var realUserId = int.Parse(fakeUserId);

                    // update user with avatar
                    var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == realUserId)
                                   .ConfigureAwait(false);
                    var databefore = JsonConvert.SerializeObject(user);

                    user.Avatar = resizedavatar;
                    GenericService.Update(user);


                    var auditTrailVm = new AuditTrailVm
                    {
                        UserId = $"{user.Id}",
                        Key1 = user.Id,
                        TableName = "User",
                        ModuleRightCode = "SYSTEM:CHANGE AVATAR",
                        Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                        WasSuccessful = true
                    };

                    LogService.AuditTrail(auditTrailVm);

                    model.Avatar = user.DisplayAvatar;
                    model.ProfileImage = user.DisplayName;
                    model.DisplayAvatar = user.DisplayAvatar;
                    TempData["MESSAGE"] = " Avatar Uploaded Successfully";
                    TempData["Type"] = "success";
                    return View(model);
                }
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred. Avatar wa not uploaded.";
                ErrorSignal.FromCurrentContext().Raise(e);
            }

            return View(model);
        }

        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var result = await UserManager.ChangePasswordAsync(
                                 User.Identity.GetUserId<int>(),
                                 model.OldPassword,
                                 model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                    var databefore = JsonConvert.SerializeObject(user);
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);


                        var dataAfter = JsonConvert.SerializeObject(user);
                        var auditTrailVm = new AuditTrailVm
                        {
                            UserId = $"{user.Id}",
                            Key1 = user.Id,
                            TableName = "User",
                            ModuleRightCode = "SYSTEM:CHANGE PASSWORD",
                            Record = $"[{databefore},{dataAfter}]",
                            WasSuccessful = true
                        };

                        LogService.AuditTrail(auditTrailVm);

                    }

                    return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
                }

                AddErrors(result);

            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred. We could not change the Password";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return View(model);
        }

        public async Task<ActionResult> EditProfile()
        {
            var userId = int.Parse(User.Identity.GetUserId());
            var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == userId).ConfigureAwait(true);
            var model = new EditUserContactViewModel();
            new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(user, model);
            return View(model);
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfile(EditUserContactViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TempData["KEY"] = "danger";
                    TempData["MESSAGE"] = GetErrorListFromModelState(ModelState);
                    return View(model);
                }
                var user = await GenericService.GetOneAsync<ApplicationUser>(x => x.Id == model.Id).ConfigureAwait(false);
                var databefore = JsonConvert.SerializeObject(user);
                new MapperConfiguration(cfg => cfg.ValidateInlineMaps = false).CreateMapper().Map(model, user);

                GenericService.Update(user);


                var auditTrailVm = new AuditTrailVm
                {
                    UserId = $"{user.Id}",
                    Key1 = user.Id,
                    TableName = "User",
                    ModuleRightCode = "SYSTEM:EDIT PROFILE",
                    Record = $"[{databefore},{JsonConvert.SerializeObject(user)}]",
                    WasSuccessful = true
                };

                LogService.AuditTrail(auditTrailVm);

                TempData["KEY"] = "success";
                TempData["MESSAGE"] = "User Phone Number updated successfully.";
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred. The User was not updated";
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return View(model);
        }
        /*  */
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {

            try
            {
                await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), true);
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
            }
            catch (Exception e)
            {
                TempData["KEY"] = "danger";
                TempData["MESSAGE"] = "An error Occurred. Two factor Authentication was not enabled.";
                ErrorSignal.FromCurrentContext().Raise(e);
            }

            return RedirectToAction("Index", "Manage");
        }

        public void GetCroppedAvatar(string destination, string destinationThumb, int width, bool delete)
        {
            var fs = new FileStream(destination, FileMode.Open, FileAccess.Read);
            var img = Image.FromStream(fs);
            img = HardCropImage(img, width);
            img.Save(destinationThumb);
            fs.Close();
            if (delete)
            {
                System.IO.File.Delete(destination);
            }
        }

        public Image HardCropImage(Image img, int newHeight)
        {
            var percentH = img.Height / (float)newHeight;
            var bmp = new Bitmap((int)(img.Height / percentH), newHeight);
            var g = Graphics.FromImage(bmp);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(img, 0, 0, bmp.Width, bmp.Height);
            g.Dispose();
            return bmp;
        }

        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage = message == ManageMessageId.ChangePasswordSuccess
                                        ?
                                        "Your password has been changed."
                                        : message == ManageMessageId.SetPasswordSuccess
                                            ? "Your password has been set."
                                            : message == ManageMessageId.SetTwoFactorSuccess
                                                ? "Your two-factor authentication provider has been set."
                                                : message == ManageMessageId.Error
                                                    ? "An error has occurred."
                                                    : message == ManageMessageId.AddPhoneSuccess
                                                        ? "Your phone number was added."
                                                        : message == ManageMessageId.RemovePhoneSuccess
                                                            ? "Your phone number was removed."
                                                            : string.Empty;

            var userId = User.Identity.GetUserId<int>();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered =
                                    await AuthenticationManager.TwoFactorBrowserRememberedAsync(
                                        User.Identity.GetUserId<int>().ToString()),
                User = await GenericService
                                           .GetOneAsync<ApplicationUser>(x => x.Id == userId)
                                           .ConfigureAwait(true)
            };
            if (ViewBag.StatusMessage.Length > 0)
            {
                TempData["KEY"] = "success";
                TempData["MESSAGE"] = ViewBag.StatusMessage;
            }
            return View(model);
        }

        /*
      // POST: /Manage/DisableTwoFactorAuthentication
      [HttpPost]
      [ValidateAntiForgeryToken]
      public async Task<ActionResult> DisableTwoFactorAuthentication()
      {
          await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), false);
          var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
          if (user != null)
          {
              await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
          }

          return RedirectToAction("Index", "Manage");
      }
      */

        /*
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(
                provider,
                Url.Action("LinkLoginCallback", "Manage"),
                User.Identity.GetUserId<int>().ToString());
        }

        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(
                                XsrfKey,
                                User.Identity.GetUserId<int>().ToString());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }

            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId<int>(), loginInfo.Login);
            return result.Succeeded
                       ? RedirectToAction("ManageLogins")
                       : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage = message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed." :
                                    message == ManageMessageId.Error ? "An error has occurred." : string.Empty;
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user == null)
            {
                return View("Error");
            }

            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId<int>());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes()
                .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel { CurrentLogins = userLogins, OtherLogins = otherLogins });
        }

        [HttpPost]
        public ActionResult RememberBrowser()
        {
            var rememberBrowserIdentity = AuthenticationManager.CreateTwoFactorRememberBrowserIdentity(
                 User.Identity.GetUserId<int>().ToString());
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, rememberBrowserIdentity);
            return RedirectToAction("Index", "Manage");
        }

        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync(
                             User.Identity.GetUserId<int>(),
                             new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }

            return RedirectToAction("ManageLogins", new { Message = message });
        }

        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId<int>(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
            if (user != null)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            }

            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }

                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(
                           User.Identity.GetUserId<int>(),
                           phoneNumber);

            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null
                       ? View("Error")
                       : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await UserManager.ChangePhoneNumberAsync(
                             User.Identity.GetUserId<int>(),
                             model.PhoneNumber,
                             model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError(string.Empty, "Failed to verify phone");
            return View(model);
        }
        */
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PasswordHash != null;
            }

            return false;
        }

        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values from error in state.Errors select error.ErrorMessage;
            var delimiter = " ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }

            return false;
        }
    }
}