﻿namespace CCTPMIS.Web
{
    using System;
    using System.Threading;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {

            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling
                = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            //  AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.InitializeMappings();
           

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
          //  Session["Browser"] = HttpContext.Current.Request.Browser;

        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            if (ex is ThreadAbortException)
                return;

            //var Logger = new LogService(genericService:Ge);
            //try
            //{
            //    Logger.FileLog(MisKeys.ERROR, $"{ex.Message} \n {ex.InnerException.Message}", "Exception");
            //}
            //catch (Exception exception)
            //{
            //    Logger.FileLog(MisKeys.ERROR, $"{ex.Message} \n {exception.Message}", "Exception");
            //}
           
            //Response.Redirect("/");
            
        }


    }
}