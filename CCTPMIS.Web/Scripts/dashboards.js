﻿$(window).on('load', function () {
    setTimeout(function () {
        $('#app-site').addClass('hide hidden');
        $('#app-site').remove();
    }, 200);


});





var app = angular.module("clientApp", ["ngSanitize", "angularUtils.directives.dirPagination"]);

app.controller('clientCtrl', ['$scope', '$http', '$filter', function (scope, http, $filter) {
    scope.count = 1;
    scope.total = 1;
    scope.indicator = 1;
    scope.paymentIndicator = 0;
    scope.county = 'All';
    scope.programme = 'All';
    scope.categoryId = '0';
    scope.itemsPerPage = 20;

    var root = $('#RootUrl').val();

    scope.sourceofFundsChanged = function () {

        // scope.data.PieChart.Male = male;
        //  scope.data.PieChart.Female = female;

        BeneficiaryChats(scope.data);


    };
    // End Function
    scope.paymentsSourceOfFundsChanged = function () {

        for (var i = 0; i < scope.data.Payments.length; i++) {

            var item = scope.data.Payments[i];
            //male += (item.Male);
            //female += (item.Female);
            //EntitlementAmount += (item.EntitlementAmount);
            //PBUFees += (item.ServiceCharge);
            //WithdrawalCharges += (item.WithdrawalCharges);
            //BeneficiaryTotals += (item.BeneficiaryNumber);

        }

        scope.data.PieChart.Male = male;
        scope.data.PieChart.Female = female;
        PaymentsChats(scope.data);


    };// End Function

    scope.refreshCaseData = function () {
        var url = root + 'Cases/RefreshData';


        if (scope.period !== 0) {
            showLoader();
            http.post(url, { MonitoringPeriodId: scope.period, IndicatorId: scope.indicator, CountyId: scope.county, ProgrammeId: scope.programme }).success(function (data) {
                scope.data = data;
                scope.allBeneficiaries = data.Beneficiaries;
                if (scope.donor === 'All') {
                    BeneficiaryChats(scope.data);
                }
                else {
                    scope.sourceofFundsChanged();
                }

                hideLoader();
            }).error(ajaxError);
        }

    }



    scope.refreshBeneficiaryData = function () {
        var url = root + 'Beneficiaries/RefreshData';


        if (scope.period !== 0) {
            showLoader();
            http.post(url, { MonitoringPeriodId: scope.period, IndicatorId: scope.indicator, CountyId: scope.county, ProgrammeId: scope.programme }).success(function (data) {
                scope.data = data;
                scope.allBeneficiaries = data.Beneficiaries;
                if (scope.donor === 'All') {
                    BeneficiaryChats(scope.data);
                }
                else {
                    scope.sourceofFundsChanged();
                }

                hideLoader();
            }).error(ajaxError);
        }

    }

    scope.refreshPaymentData = function () {
        var url = root + 'Payment/RefreshData';

        if (scope.period !== 0) {
            showLoader();
            http.post(url, { IndicatorId: scope.paymentIndicator, PeriodId: scope.period }).success(
                function (data) {
                    scope.data = data;
                    scope.allPayments = data.Payments;
                    if (scope.donor === 'All') {
                        PaymentsChats(scope.data);
                    } else {
                        scope.paymentsSourceOfFundsChanged();
                    }

                    hideLoader();
                }).error(ajaxError);
        }


    }

    scope.refreshComplaintData = function () {
        var url = root + 'Complaints/RefreshData';
        scope.donor = 'All';
        showLoader();
        http.post(url, { IndicatorId: scope.paymentIndicator, PaymentCycleId: scope.period }).success(function (data) {
            scope.data = data;
            scope.allComplaints = data.Complaints;
            ComplaintsCharts(scope.data);
            hideLoader();
        }).error(ajaxError);
    }
    scope.indexCount = function (newPageNumber) {
        alert();
        scope.serial = newPageNumber * 10 - 9;

    }

}]).filter('inArray', function ($filter) {
    return function (list, arrayFilter, element) {
        if (arrayFilter) {
            return $filter("filter")(list, function (listItem) {
                return arrayFilter.indexOf(listItem[element]) !== -1;
            });
        }
    };
});


// var colors = ['#006091', '#7B99A9', '#374E5A', '#839118', '#9C8C21', '#98918F'];
var colors = ['#5B9BD4', '#EC7C30', '#9B9B9B', '#A5A5A5'];
var colorsb = ['#9B9B9B', '#FFC000', '#EC7C30', '#5B9BD4'];

function BeneficiaryChats(data) {

    var period = $("#reporting-period option:selected").text();
    period = period === 'Select' ? '' : period;
    var indicator = $("#indicator option:selected").val();
    var title = $("#indicator option:selected").text() + ' ' + period;



    $('.lbltitle').text(title);
    var max = parseInt(data.Total);

    Highcharts.chart('totals-barchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: title,
            /* style: {
                 fontWeight: 'bold',
                 fontSize: '14px'
             }*/
        },
        colors: colors,
        xAxis: {
            crosshair: true,
            categories: ['']
        },
        yAxis: {
            min: 0,
            max: max,
            labels: {
                formatter: function () {
                    return this.value / 1000;
                }
            },
            title: {
                text: 'in thousands'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            //x: -30,
            verticalAlign: 'bottom',
            y: 5,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: ' ',
            pointFormat: '<b>{series.name}</b>: {point.y}'
        },
        plotOptions: {
            column: {
                pointWidth: 65,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray',
                    formatter: function () {
                        return (this.y / 1000).toFixed(2);
                    }
                }
            }
        },
        series: [{
            name: 'Male',
            data: [data.Male]
        }, {
            name: 'Female',
            data: [data.Female]
        }, {
            name: 'Totals',
            data: [data.Total]
        }]
    });

    Highcharts.chart('totals-piechart', {
        chart: {
            type: 'pie'
        },
        title: {
            text: '% ' + title
        },
        colors: colors,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: [
                {
                    name: 'Male',
                    y: data.Male
                },
                {
                    name: 'Female',
                    y: data.Female
                }
            ]
        }]
    });


    setTimeout(function () {
        $('.highcharts-container').addClass('hvr-glow');
    }, 2000);
}

function ComplaintsCharts(data) {

    $('.lbltitle').text(title);
    var max = parseInt($('#max').val());

    var title = $("#indicator option:selected").text() + ' ' + period;

    Highcharts.chart('totals-barchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        colors: colors,
        xAxis: {

            crosshair: true,
            categories: ['']
        },
        yAxis: {
            min: 0,
            // max: max,
            title: {
                text: ''
            },
            // tickInterval: 100,
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            //x: -30,
            verticalAlign: 'bottom',
            y: 5,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: ' ',
            pointFormat: '<b>{series.name}: </b>{point.y}<br>'
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000',
                    // color: colors[0],
                    style: {
                        fontWeight: 'bold',
                        fontSize: '12px',
                        fontFamily: "'Maven Pro', sans-serif"
                    },
                    formatter: function () {
                        return this.y + '';
                    }
                }
            }
        },
        series: [{
            name: 'Registered',
            data: [data.Registered]
        }, {
            name: 'Resolved',
            data: [data.Resolved]
        }, {
            name: 'Totals',
            data: [data.Total]
        }]
    });

    Highcharts.chart('totals-piechart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: $("#period option:selected").val() === '0' ? '% Total of number of complaints to-date' : 'Total of number of complaints in the reporting period ' + $("#period option:selected").text()
        },
        colors: colors,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: [
                {
                    name: 'Registered',
                    y: data.Registered
                },
                {
                    name: 'Resolved',
                    y: data.Resolved
                }
            ]
        }]
    });

    setTimeout(function () {
        $('.highcharts-container').addClass('hvr-glow');
    }, 2000);

}
function PaymentsChats(data) {
    var period = $("#reporting-period option:selected").text();
    period = period === 'Select' ? '' : period;

    var title = $("#indicator option:selected").text() + ' ' + period;

    $('.lbltitle').text(title);
    var max = parseInt($('#max').val());

    Highcharts.chart('totals-barchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        colors: colors,
        xAxis: {
            crosshair: true,
            categories: ['']
        },
        yAxis: {
            min: 0,
            max: max,
            title: {
                text: 'in billions (KES)'
            },
            labels: {
                formatter: function () {
                    return this.value / 1000000000;
                }
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            y: 5,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '<b>Total: </b>{point.y}<br>'
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray',
                    formatter: function () {
                        return (this.y / 1000000000).toFixed(2);
                    }
                }
            }
        },
        series: [{
            name: 'Amount Within period',
            data: [data.BarChart.AmountWithin]
        }, {
            name: 'Amount Outside Period',
            data: [data.BarChart.AmountWithout]
        }, {
            name: 'Total Amount ',
            data: [data.BarChart.AmountTotal]
        }]
    });
    Highcharts.chart('totals-piechart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '% ' + title
        },
        colors: colors,
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: [
                {
                    name: 'Amount Within',
                    y: data.BarChart.AmountWithout
                },
                {
                    name: 'Amount Outside',
                    y: data.BarChart.AmountWithin
                }
            ]
        }]
    });
    setTimeout(function () {
        $('.highcharts-container').addClass('hvr-glow');
    }, 2000);
}





$(document).ready(function () {

    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        },
        credits: {
            enabled: false
        }
    });
    $(".btn-export").click(function () {

        var scope = angular.element(document.querySelector('body')).scope();
        scope.$apply(function (filter) {
            scope.OldPagination = scope.itemsPerPage;
            scope.itemsPerPage = '';

            setTimeout(function () {
                var fileName = $('.lbltitle').text();
                $(".table").table2excel({
                    // exclude CSS class
                    exclude: ".noExl,.ng-hide",
                    name: fileName + " Data",
                    filename: fileName//do not include extension
                });
                var scope = angular.element(document.querySelector('body')).scope();
                scope.$apply(function (filter) {
                    scope.itemsPerPage = scope.OldPagination;
                });
            }, 100);
        });

    });
    $("body").on('click', 'dir-pagination-controls a', function () {
        $('.to-top').click();
        $('html,body').animate({
            scrollTop: $(".panel-body").offset().top
        });

    });

});
function showLoader() {
    $('.ajax-loader ').fadeIn();
}

function hideLoader() {
    $('.ajax-loader ').fadeOut();
}
var ajaxError = function (object) {
    console.log(object);
    console.log("An error has occured processing your request.");
    hideLoader();
};
var ajaxAlways = function (object) {
    hideLoader();
};