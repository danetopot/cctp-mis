﻿(function () {
    "use strict";

    var fileId = 1, isCrypto = false;
    if ((typeof File !== 'undefined') && !File.prototype.slice) {
        if (File.prototype.webkitSlice) {
            File.prototype.slice = File.prototype.webkitSlice;
        }

        if (File.prototype.mozSlice) {
            File.prototype.slice = File.prototype.mozSlice;
        }
    }
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob || !File.prototype.slice) {
        alert('File APIs are not fully supported in this browser. Please use latest Mozilla Firefox or Google Chrome.');
    }

    isCrypto = window.crypto && window.crypto.subtle && window.crypto.subtle.digest;

    function hashFile(file, workers) {
        var i, bufferSize, block, threads, reader, blob, handleHashBlock, handleLoadBlock;

        handleLoadBlock = function (event) {
            for (i = 0; i < workers.length; i += 1) {
                threads += 1;
                workers[i].postMessage({
                    'message': event.target.result,
                    'block': block
                });
            }
        };
        handleHashBlock = function (event) {
            threads -= 1;

            if (threads === 0) {
                if (block.end !== file.size) {
                    block.start += bufferSize;
                    block.end += bufferSize;

                    if (block.end > file.size) {
                        block.end = file.size;
                    }
                    reader = new FileReader();
                    reader.onload = handleLoadBlock;
                    blob = file.slice(block.start, block.end);

                    reader.readAsArrayBuffer(blob);
                }
            }
        };
        bufferSize = 64 * 16 * 1024;
        block = {
            'file_size': file.size,
            'start': 0
        };

        block.end = bufferSize > file.size ? file.size : bufferSize;
        threads = 0;

        for (i = 0; i < workers.length; i += 1) {
            workers[i].addEventListener('message', handleHashBlock);
        }
        reader = new FileReader();
        reader.onload = handleLoadBlock;
        blob = file.slice(block.start, block.end);

        reader.readAsArrayBuffer(blob);
    }

    function handleCryptoProgress(id, total, loaded) {
        $(id + ' .bar').css('width', loaded * 100 / total + '%');
    }

    function handleFileSelect(event) {
        $("#dangerCheckSum").addClass("hidden");
        $("#successCheckSum").addClass("hidden");
        $("#verificationresults").removeClass("hidden");

        event.stopPropagation();
        event.preventDefault();

        var i, output, files, file, workers, worker, reader, cryptoFiles, cryptoAlgos, maxCryptoFileSize = 500 * 1024 * 1024;

        files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
        output = [];
        cryptoFiles = [];

        for (i = 0; i < files.length; i += 1) {
            file = files[i];
            workers = [];
            cryptoAlgos = [];

            if (isCrypto && file.size < maxCryptoFileSize) {
                cryptoAlgos.push({ id: "#sha256_file_hash_" + fileId, name: "SHA-256" });
            }

            if (isCrypto && cryptoAlgos.length > 0) {
                cryptoFiles.push({ file: file, algos: cryptoAlgos });
            }

            hashFile(file, workers);
            fileId += 1;
        }

        if (isCrypto) {
            handleCryptoFiles(cryptoFiles);
        }

        document.getElementById('list').innerHTML = '<table class="table table-striped table-hover">' + output.join('') + '</table>';
    }

    function handleCryptoFiles(cryptoFiles) {
        var cryptoFile, handleCryptoFile, handleCryptoBlock, reader;

        cryptoFile = cryptoFiles.pop();

        handleCryptoBlock = function (data, algos) {
            var algo = algos.pop();

            if (algo) {
                window.crypto.subtle.digest({ name: algo.name }, data)
                    .then(function (hash) {
                        var hexString = '', hashResult = new Uint8Array(hash);

                        for (var i = 0; i < hashResult.length; i++) {
                            hexString += ("00" + hashResult[i].toString(16)).slice(-2);
                        }

                        ////console.log(hexString);
                        $(algo.id).parent().html(hexString);

                        var curentScript = $("#ServerSha256").val().toUpperCase();
                        hexString = hexString.toUpperCase();
                       // ("#dangerCheckSum").html("");
                        //$("#successCheckSum").html("");
                        $("#FileCheckSum").html(hexString);
                        $("#FileName").html($("#EnrolmentFile").val());
                        if (curentScript === hexString) {
                            $("#dangerCheckSum").addClass("hidden");
                            $("#successCheckSum").removeClass("hidden");
                            $("#CheckSum").val(hexString);

                            $.ajax({
                                url: "/psp/files/verifychecksum",
                                dataType: "json",
                                crossDomain: true,
                                data:
                                    {
                                        UserId: parseInt($("#UserId").val()),
                                        FileCheckSum: $("#CheckSum").val(),
                                        FileCreationId: parseInt($("#FileCreationId").val())
                                    },
                                beforeSend: function () {
                                },
                                success: function (json) {
                                    $("#dangerCheckSum").addClass("hidden");
                                    $("#successCheckSumInner").html("This is the File Password \n <strong> " + json.FilePassword + "</strong>\n");
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    $("#dangerCheckSumInner").html(xhr.responseText + "\r\n");
                                    $("#dangerCheckSum").removeClass("hidden");
                                    $("#successCheckSum").addClass("hidden");
                                },

                                complete: function () {
                                }
                            });
                        } else {
                            $("#successCheckSum").addClass("hidden");
                            $("#dangerCheckSum").removeClass("hidden");
                        }
                        handleCryptoBlock(data, algos);
                    })
                    .catch(function (error) {
                        console.error(error);
                    });
            } else {
                handleCryptoFiles(cryptoFiles);
            }
        };

        handleCryptoFile = function (file, cryptoAlgos) {
            reader = new FileReader();

            reader.onprogress = (function (cryptoAlgos) {
                var algos = cryptoAlgos;

                return function (event) {
                    var i;

                    for (i = 0; i < algos.length; i++) {
                        handleCryptoProgress(algos[i].id, event.total, event.loaded);
                    }
                }
            })(cryptoAlgos);

            reader.onload = (function (cryptoAlgos) {
                var algos = cryptoAlgos;

                return function (event) {
                    handleCryptoBlock(event.target.result, algos);
                }
            })(cryptoAlgos);

            reader.readAsArrayBuffer(file);
        };

        if (cryptoFile) {
            handleCryptoFile(cryptoFile.file, cryptoFile.algos);
        }
    }

    document.getElementById('EnrolmentFile').addEventListener('change', handleFileSelect, false);
}());