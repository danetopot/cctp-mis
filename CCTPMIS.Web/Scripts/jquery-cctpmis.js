﻿var popup = '<div class="modal-dialog modal-default">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
    '<h4 class="modal-title text-center">Loading....</h4>' +
    "</div>" +
    '<div class="modal-body">' +
    '<p class="text-center"> <i class="fa  fa-cog fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> </p>' +
    "</div>" +
    '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div>';


(function ($) {
    $(window).on('load', function () { $.force_appear(); }); //Attaches load event
    $(window).trigger('load'); //Triggers load event manually.
});

(function ($) {


    $(document).on("change", "#CountyId.filter",
        function (e) {
            var working = "<span id ='spin' ><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></span>";

            if ($(this).val() !== "") {
                $.ajax({
                    url: "/Data/GetConstituencies/" + $(this).val(),
                    dataType: "json",
                    crossDomain: true,

                    beforeSend: function () {
                        $(this).parent().append(working);
                        $("#ConstituencyId").html("");
                        //$("#ConstituencyId").append("<option> Select </option>");
                    },
                    success: function (json) {
                        var data = json;
                        //console.log(data);

                        $(data).map(function () {

                            $("#ConstituencyId").append($('<option></option>').val(this.Id).html(this.Name));
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    },

                    complete: function () {
                        $("#spin").remove();
                    }
                });
            }
        });

    $('.btn-reset').on('click',
        function (e) {
            e.preventDefault();
            $('.filter-list').val('').trigger('change');
            $('.filter-list option').removeAttr('selected');
            $('.select-searchable').val('').trigger('change');
            $('.select-searchable option').removeAttr('selected');

            $(':input').each(function () {
                var type = this.type;
                var tag = this.tagName.toLowerCase(); // normalize case
                // to reset the value attr of text inputs,
                // password inputs, fileUpload and textareas
                if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'file')
                    this.value = "";
                // checkboxes and radios need to have their checked state cleared
                //else if (type == 'radio')
                //    this.checked = false;
                else if (type == 'checkbox')
                    this.checked = false;
                // select elements need to have their 'selectedIndex' property set to -1
                // (this works for both single and multiple select elements)
                else if (tag == 'select') {
                    this.selectedIndex = 0;
                }
            });
            $(':input.multiple').each(function () {
                this.selectedIndex = -1;
                $('.multiselect-selected-text').text('None selected');
            });
            return false;
        });

    $(document).on("change", "#ConstituencyId.filterSL, .filterSL",
        function (e) {
            var working = "<span id ='spin' ><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></span>";

            if ($(this).val() !== "") {
                $.ajax({
                    url: "/Data/GetSubLocations/" + $(this).val(),
                    dataType: "json",
                    crossDomain: true,

                    beforeSend: function () {
                        $(this).parent().append(working);
                        $("#SubLocationId").html("");
                        $("#SubLocationId").append("<option> Select </option>");

                        //UpdatedSubLocationId

                        $("#UpdatedSubLocationId").html("");
                        $("#UpdatedSubLocationId").append("<option> Select </option>");

                    },
                    success: function (json) {
                        var data = json;
                        //console.log(data);

                        $(data).map(function () {

                            $("#SubLocationId").append($('<option></option>').val(this.Id).html(this.Name));
                            $("#UpdatedSubLocationId").append($('<option></option>').val(this.Id).html(this.Name));
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    },

                    complete: function () {
                        $("#spin").remove();
                    }
                });
            }
        });


    $('[data-toggle="tooltip"]').tooltip({

    });

    var start = new Date();
    var end = new Date(new Date().setYear(start.getFullYear() + 1));



    $(document).on("change", "#DateRange",
        function (e) {
            var working = "<span id ='spin' ><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></span>";

            if ($(this).val() !== "") {
                $.ajax({
                    url: "/Data/GetDateRange/" + $(this).val(),
                    dataType: "json",
                    crossDomain: true,

                    beforeSend: function () {
                        $(this).parent().append(working);
                    },
                    success: function (json) {
                        var data = json;

                        $(data).map(function () {

                            $("#StartDate").val(json.StartDate);
                            $("#EndDate").val(json.EndDate);
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    },

                    complete: function () {
                        $("#spin").remove();
                    }
                });
            }
        });


    $(function () {
        $('.input-daterange #Start').datetimepicker(
            {
                format: "YYYY-MM-DD"
            });
        $('.input-daterange #End').datetimepicker({

            useCurrent: false,
            format: "YYYY-MM-DD"
        });
        $(".input-daterange #Start").on("dp.change", function (e) {
            $('.input-daterange #End').data("DateTimePicker").minDate(e.date);
        });
        $(".input-daterange #End").on("dp.change", function (e) {
            $('.input-daterange #Start').data("DateTimePicker").maxDate(e.date);
        });
    });



    $(function () {
        $('.death-date').datetimepicker(
            {
                format: "YYYY-MM-DD",
                maxDate: new Date()
            });
        $('.death-notif-date').datetimepicker({

            useCurrent: false,
            format: "YYYY-MM-DD",
            maxDate: new Date()
        });

        $(".death-date").on("dp.change", function (e) {
            $('.death-notif-date').data("DateTimePicker").minDate(e.date);
        });
        $(".death-notif-date").on("dp.change", function (e) {
            $('.death-date').data("DateTimePicker").maxDate(e.date);
        });
    });








    $(function () {

        $(' .ReceiveDate').datetimepicker({
            format: "YYYY-MM-DD",
            useCurrent: false,
            maxDate: new Date()

        });

        $(' #StartDate,#DobStartDate').datetimepicker({
            format: "YYYY-MM-DD"
        });



        $(' #EndDate,#DobEndDate').datetimepicker({
            format: "YYYY-MM-DD",
            useCurrent: false //Important! See issue #1075

        });
        $(" #StartDate").on("dp.change", function (e) {
            $(' #EndDate').data("DateTimePicker").minDate(e.date);
        });
        $(" #EndDate").on("dp.change", function (e) {
            $(' #StartDate').data("DateTimePicker").maxDate(e.date);
        });


        $(" #DobStartDate").on("dp.change", function (e) {
            $(' #DobEndDate').data("DateTimePicker").minDate(e.date);
        });
        $(" #DobEndDate").on("dp.change", function (e) {
            $(' #DobStartDate').data("DateTimePicker").maxDate(e.date);
        });


        $('#yearpicker').datetimepicker({
            viewMode: 'years',
            format: 'YYYY',
            useCurrent: false
        });

        $('#monthpicker').datetimepicker({
            format: "YYYY-MM",
            viewMode: 'months',
            dayViewHeaderFormat: 'YYYY',
            useCurrent: false
        });


        $(".death-date").datetimepicker({
            format: "YYYY-MM-DD",
            dayViewHeaderFormat: 'YYYY-MM-DD',
            useCurrent: false,
            maxDate: new Date()
        });
        $(".date-of-birth").datetimepicker({
            format: "YYYY-MM-DD",
            dayViewHeaderFormat: 'YYYY-MM-DD',
            useCurrent: false,
            maxDate: new Date()
        });

        var end = new Date(new Date().setYear(start.getFullYear() + 1));


        $(".date-of-birth-cg").datetimepicker({
            format: "YYYY-MM-DD",
            dayViewHeaderFormat: 'YYYY-MM-DD',
            useCurrent: false,
            maxDate: end
        });
    });


    $(window).scroll(function () {
        var scrollTop = $("body").scrollTop();
        if (scrollTop < 50) {
            $("#showBtnGroup").removeClass('dropdown').addClass('dropup');
        } else {
            $("#showBtnGroup").removeClass('dropup').addClass('dropdown');
        }
    });


    var query = String(document.location).split('?');
    $(".nav-tabs a")
        .each(function () {

            if (IsLocalMenuParent(query[0], this.href)) {
                $(this).parent().addClass("active");

                $(this).addClass("menuactive");
            }
        });

    $(".nav-tabs ").each(function () {
        $(this).next().addClass(" m-tb-30 ");
    });


    $("#side-nav a")
        .each(function () {
            if (IsLocalMenuParent(query[0], this.href)) {
                $(this).addClass("active");
                $(this).parent().addClass("active");
                if ($(this).parents(2).hasClass("has-sub")) {
                    $(this).parents(3).addClass("active");
                }
            }
        });
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    $('.equal-heights').equalHeights();
    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log)
                $("#dataFileLabel").text(log);
        }
    });

    if (!document.getElementById("treeData")) {

        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal',
            increaseArea: '0%', // optional
            indeterminateClass: 'indeterminate'
        });
        /*
        $("input[type=checkbox],input[type=radio]").each(function () {
            var $onclick = $(this).attr("onclick");
            var $iCheckName = $(this).attr("name");
            //console.log($iCheckName);
            
            var $buttonTrigger = "input[name=\"" + $iCheckName + "\"";
            var $this = $(this);
            if ($onclick != undefined) {
                if ($onclick.length > 0) {
                    $($buttonTrigger).on("ifChecked", function (event) {
                        $(this).trigger("click");
                        $(this).prop("checked", true);
                        $(this).click();
                    });
                }
            }
        });
        */

    }


    /*

    $(".main-menu li ul li a")
        .each(function () {
            //     if (this.href.indexOf(window.location.href)) {
            if (this.href === window.location.href) {
                $(this).addClass("menu-active");
                $(this).parent().addClass("active");

                if ($(this).parents(2).hasClass("has-sub")) {
                    $(this).parents(3).addClass("active");
                }
            }
        });
        */
    $('#side-nav').metisMenu(
        {
            toggle: false
        });

    $(function () {
        $('[data-toggle="popover"]').popover({ trigger: 'hover' });
        $('[data-toggle="tooltip"]').tooltip();


        $(document).on("click", ".modal-link", function (e) {
            e.preventDefault();
            $("#popup").html(popup);
            $('#popup').load($(this).attr("href"));
            $('#popup').modal('show');
            $('#popup').modal({ keyboard: false });
        });

        $(document).on("click", ".tr-link", function (e) {
            e.preventDefault();
           
            var childTD = $(this).closest("tr").next().find("td");
            childTD.load($(this).attr("href"));
            $(this).closest("tr").next().removeClass("hidden");
            $(this).closest("tr").next().show("slow");

        });


        $(document).on("click",
            ".closex",
            function(e) {
                $(this).closest("tr").addClass("hidden");
                $(this).closest("div.row").hide("slow");
                //$(this).closest("div.row").remove();

            });


    });

    $(".mobile-menu-icon").click(function (event) {
        event.preventDefault();
    });
    $(".btn-disabled").click(function (e) {
        e.preventDefault();
        swal({
            title: "Sorry",
            text: "You cannot approve an item that you created or modified ! For each transaction, there must be at least two individuals necessary for its completion.",
            animation: "slide-from-top",
            type: 'info',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });
    });
    $(".btn-ver-pyl-disabled").click(function (e) {
        e.preventDefault();
        swal({
            title: "Sorry",
            text: "You cannot Verify Payroll  that you Generated created!",
            animation: "slide-from-top",
            type: 'info',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });
    });
    $(".btn-pc-approve").click(function (e) {
        e.preventDefault();

        swal({
            title: "Sorry",
            text: "You cannot approve a payment cycle  that you created!",
            animation: "slide-from-top",
            type: 'info',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });
    });
    var $window = $(window), $container = $('div.page-container');
    var $isCollapsed = false;

    $(".sidebar-collapse-icon").click(function (event) {
        event.preventDefault();
        $container.toggleClass('sidebar-collapsed').toggleClass('can-resize');
        $(".page-container").addClass("sidebar-collapsing");
        if ($isCollapsed === true) {
            $(".page-sidebar, .sidebar-fixed").animate({
                width: 280
            }, function () {
                $(".page-container").removeClass("sidebar-collapsing");
                createCookie('sidebar', 'open');
            });
        }
        else {
            $(".page-sidebar, .sidebar-fixed").animate({
                width: 66
            }, function () {
                $(".page-container").removeClass("sidebar-collapsing");
                createCookie('sidebar', 'closed');
            });
        }
        $isCollapsed = !$isCollapsed;
    });

    if (readCookie('sidebar') === 'closed') {
        $(".sidebar-collapse-icon").click();
    }

    if ($container.hasClass('sidebar-collapsed')) {
        $isCollapsed = true;
        $(".page-sidebar, .sidebar-fixed").animate({
            width: 66
        });
    }
    $window.resize(function resize() {
        var windowWidth = $window.outerWidth();
        if (windowWidth < 951 && windowWidth > 767) {
            if ($container.hasClass('can-resize') === false) {
                $container.addClass('sidebar-collapsed');
                $(".page-sidebar, .sidebar-fixed").animate({
                    width: 66
                });
                $isCollapsed = true;
            }
        } else if (windowWidth < 767) {
            $container.removeClass('sidebar-collapsed');
            $container.removeClass('can-resize');
        } else {
            if ($container.hasClass('can-resize') === false && $isCollapsed === false) {
                $container.removeClass('sidebar-collapsed');
            }
        }
    }).trigger('resize');
    $('body').on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="reload"]', function (ev) {
        ev.preventDefault();

        var $this = $(this).closest('.panel');

        $this.block({
            message: '',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '50%'
            },
            overlayCSS: { backgroundColor: '#FFF' }
        });
        $this.addClass('reloading');

        setTimeout(function () {
            $this.unblock();
            $this.removeClass('reloading');
        }, 900);
    }).on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="close"]', function (ev) {
        ev.preventDefault();

        var $this = $(this);
        var $panel = $this.closest('.panel');

        $panel.fadeOut(500, function () {
            $panel.remove();
        });
    }).on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="collapse"]', function (ev) {
        ev.preventDefault();

        var $this = $(this),
            $panel = $this.closest('.panel'),
            $body = $panel.children('.panel-body, .table'),
            doCollapse = !$panel.hasClass('panel-collapse');

        if ($panel.is('[data-collapsed="1"]')) {
            $panel.attr('data-collapsed', 0);
            $body.hide();
            doCollapse = false;
        }

        if (doCollapse) {
            $body.slideUp('normal');
            $panel.addClass('panel-collapse');
        }
        else {
            $body.slideDown('normal');
            $panel.removeClass('panel-collapse');
        }
    });

    // removeable-list -- remove parent elements
    var $removalList = $(".removeable-list");
    $(".removeable-list .remove").each(function () {
        var $this = $(this);
        $this.click(function (event) {
            event.preventDefault();

            var $parent = $this.parent('li');
            $parent.slideUp(500, function () {
                $parent.delay(3000).remove();

                if ($removalList.find("li").length === 0) {
                    $removalList.html('<li class="text-danger"><p>All items has been deleted.</p></li>');
                }
            });
        });
    });

    var $filterBtn = $(".toggle-filter");
    var $filterBoxId = $filterBtn.attr('data-block-id');
    var $filterBox = $('#' + $filterBoxId);

    if ($filterBox.hasClass('visible-box')) {
        $filterBtn.parent('li').addClass('active');
    }

    $filterBtn.click(function (event) {
        event.preventDefault();

        if ($filterBox.hasClass('visible-box')) {
            $filterBtn.parent('li').removeClass('active');
            $filterBox.removeClass('visible-box').addClass('hidden-box').slideUp();
        } else {
            $filterBtn.parent('li').addClass('active');
            $filterBox.removeClass('hidden-box').addClass('visible-box').slideDown();
        }
    });

    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
    });

    $('.startdate').datetimepicker({
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('.enddate').val() ? $('.enddate').val() : false
            });
        },
        timepicker: false
    });
    $('.enddate').datetimepicker({
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('.startdate').val() ? $('.startdate').val() : false
            });
        },
        timepicker: false
    });

    $(document).on("change", "#UserGroupId.ug",
        function (e) {
            var working = "<span id ='spin' ><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></span>";

            if ($(this).val() !== "") {
                $.ajax({
                    url: "/Data/GetUserGroupType/" + $(this).val(),
                    dataType: "json",
                    crossDomain: true,

                    beforeSend: function () {
                        $(this).parent().append(working);
                    },
                    success: function (json) {
                        var data = json;
                        if (data.UserGroupType.Code === "Internal") {

                            $("#NationalIdNo").addClass("required");
                            $("#StaffNo").addClass("required");

                            $("#NationalIdNo").parent().parent().removeClass("hidden");
                            $("#StaffNo").parent().parent().removeClass("hidden");

                        } else {

                            $("#NationalIdNo").removeClass("required");
                            $("#StaffNo").removeClass("required");

                            $("#NationalIdNo").parent().parent().addClass("hidden");
                            $("#StaffNo").parent().parent().addClass("hidden");

                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    },

                    complete: function () {
                        $("#spin").remove();
                    }
                });
            }
        });






})(jQuery);

function showTooltip(x, y, contents) {
    var $windowWidth = $(window).width();
    var leftValue = x + 20;
    var toolTipWidth = 160;
    if ($windowWidth < (leftValue + toolTipWidth)) {
        leftValue = x - 32 - toolTipWidth;
    }

    $('<div id="flotTip" > ' + contents + ' </div>').css({
        top: y - 16,
        left: leftValue,
        position: 'absolute',
        padding: '5px 10px',
        border: '1px solid #111111',
        'min-width': toolTipWidth,
        background: '#ffffff',
        background: '-moz-linear-gradient(top,  #ffffff 0%, #f9f9f9 100%)',
        background: '-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f9f9f9))',
        background: '-webkit-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: '-o-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: '-ms-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: 'linear-gradient(to bottom,  #ffffff 0%,#f9f9f9 100%)',
        '-webkit-border-radius': '5px',
        '-moz-border-radius': '5px',
        'border-radius': '5px',
        'z-index': '100'
    }).appendTo('body').fadeIn();
}

/*
 * This function will remove its parent element
 *
 * @param $eleObj
 * @param $parentEle
 */

function removeElement($ele, $parentEle) {
    var $this = $($ele);
    $this.parent($parentEle).css({
        opacity: '0'
    });
}

//Create Cookie Function
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else
        var eexpires = "";
    document.cookie = name + "=" + value + eexpires + "; path=/";
}

//Read Cookie Function
function readCookie(name) {
    var nameEq = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEq) === 0) return c.substring(nameEq.length, c.length);
    }
    return null;
}

//Erase Cookie Function
function eraseCookie(name) {
    createCookie(name, "", -1);
}

$("#popup").on('hidden.bs.modal', function () {
    $(this).data('bs.modal', null);
});

$('#popup').on('hidden', function () {
    $(this).data('modal', null);
});

$(document).on("submit", "#PrePayrollForm", function (e) {
    e.preventDefault();
    if ($("#PaymentCycleId").val() === "") {
        swal({
            title: "Payment Cycle Programme Details is Required",
            text: "Kindly select the Payment Cycle Programme Details to run the Pre-payroll!",
            animation: "slide-from-top",
            type: 'warning',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });
    } else {
        $('#popup').data('bs.modal', null);
        $('#popup').load("/payments/Prepayrolls/Run/" + $("#PaymentCycleId").val());
        $('#popup').modal('show');
        $('#popup').modal({ keyboard: false });
    }
});

function StripNormalizeLink(menuLink) {
    menuLink = String(menuLink).split('?')[0];
    menuLink = menuLink
        .replace(/1|2|3|4|5|6|7|8|9|0|/gi, "")
        .replace(/details|summary|edit|create|delete|close|approve|page|approve|/gi, "");
    return menuLink;
}


function IsLocalMenuParent(windowLocation, aHref) {
    windowLocation = StripNormalizeLink(windowLocation);
    aHref = StripNormalizeLink(aHref);
    if (windowLocation === aHref)
        //console.log(windowLocation + "<>" + aHref);
        return windowLocation === aHref;
}


$(document).on("submit", "#FundsRequestForm", function (e) {
    e.preventDefault();
    if ($("#PaymentCycleId").val() === "") {
        swal({
            title: "Payment Cycle  is Required",
            text: "Kindly select the Payment Cycle  Generate Funds Request For!",
            animation: "slide-from-top",
            type: 'warning',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });
    } else {
        $('#popup').data('bs.modal', null);
        $('#popup').load("/payments/FundsRequests/Run/" + $("#PaymentCycleId").val());
        $('#popup').modal('show');
        $('#popup').modal({ keyboard: false });
    }
});


$(function () {


    $('.wizard-steps li a').on('click',
        function (e) {
            window.location = $(this).attr('href');
        });

if ($(".form-paginated")[0]) {
        $('.pagination li:not(.active) a').on('click', function (e) {
            var page = $.urlParam($(this).attr('href'), 'page');
            if (page !== null) {
                $('#pagination-page').val(page);
                $('.form-paginated').trigger('submit');
            }
            e.preventDefault();
            return false;
        });
    }
	/*
    if (document.getElementById("pagination-page")) {
        $('.pagination li:not(.active) a').on('click',
            function(e) {

                var page = $.urlParam($(this).attr('href'), 'page');

                if (page !== null) {
                    $('#pagination-page').val(page);
                    $('.form-paginated').trigger('submit');
                }
                e.preventDefault();
                return false;
            });

    }
*/
    $('.btn-print').on('click',
        function (e) {
            window.print();
            e.preventDefault();
        });

    $(document).on("submit", "form.form-paginated", function () {

        $('.panel-toggle').collapse({
            toggle: false
        });

    });

});

$.urlParam = function (url, shows) {
    var results = new RegExp('[\\?&]' + shows + '=([^&#]*)').exec(url);
    if (!results) { return 0; } return results[1] || 0;
}