var app = angular.module("clientApp", [ "ngSanitize", "angularUtils.directives.dirPagination"]);
app.controller('clientCtrl', ['$scope', '$http', '$filter', function (scope, http, $filter) {
    scope.count = 1;
    scope.total = 1;
    scope.indicator = 1;

    scope.donor = 'All';
    scope.categoryId = '0';
    scope.period = '';
    scope.itemsPerPage = 20;
    scope.refreshData = function () {
        scope.indicator = parseInt(scope.indicator);
        var url = root + 'Maps/Beneficiaries';
        if (scope.indicator == 5 || scope.indicator === 6 || scope.indicator === 7) {
            url = root + 'Maps/Payments';
        }
        else if (scope.indicator === 10 || scope.indicator === 11) {
            url = root + 'Maps/Cases';
        }
        if (scope.period !== 0) {
            showLoader();
            http.post(url, { IndicatorId: scope.indicator, PaymentCycleId: scope.period, DonorId: scope.donor }).success(function (data) {
                scope.data = data;
                scope.indicator = parseInt(scope.indicator);
                if (scope.indicator < 4 || scope.indicator === 8 || scope.indicator === 9) {
                    BeneficiariesMaps(scope.data);
                } else if (scope.indicator === 5 || scope.indicator === 6 || scope.indicator === 7) {
                    PaymentsMaps(scope.data);
                } else {
                    ComplaintsMaps(scope.data);
                }

                //  hideLoader();
            }).error(ajaxError);
        }
    }

    var root = $('#RootUrl').val();
}]).filter('inArray', function ($filter) {
    return function (list, arrayFilter, element) {
        if (arrayFilter) {
            return $filter("filter")(list, function (listItem) {
                return arrayFilter.indexOf(listItem[element]) != -1;
            });
        }
    };
});

var rootUrl = $('#RootUrl').val();

function BeneficiariesMaps(data) {
    $('#app-site').fadeOut();
    var period = $("#reporting-period option:selected").text();
    period = period === 'Select' ? '' : period;

    var title = period;

    Highcharts.mapChart('map-container', {
        title: {
            text: title
        },
        //subtitle: {
        //    text: "Test Subtite"
        //},

        mapNavigation: {
            enabled: true,
            buttons: {
                zoomIn: {
                    onclick: function () { this.mapZoom(0.1); }
                },
                zoomOut: {
                    onclick: function () { this.mapZoom(10); }
                }
            }
        },
        tooltip: {
               headerFormat: '',
             pointFormat: '{point.properties.COUNTY}' + '<br>' +
                  'Beneficiaries: {point.value}' 
        },
        colorAxis: {
            tickPixelInterval: 100,
            min: 0,
            minColor: '#d9f0a3',
            maxColor: '#005a32'
        },
        series: [{
            //data: data,
            mapData: JSON.parse($('#geojson').val()),
            joinBy: [ 'CONCODE','CAWCODE' ],
            keys: ["CONCODE", 'value'],
            //name: 'Map Data',
            states: {
                hover: {
                    color: '#a4edba'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.properties.CONSTITUEN}'
            }
        }]
    });
}




  /*
  function PaymentsMaps(data) {
      $('.ajax-loader ').fadeOut();
      var period = $("#reporting-period option:selected").text();
      period = period === 'Select' ? '' : period;
  
      var title = $("#indicators option:selected").val() === '5'
          ? "Total Volume of Grants Paid by end of" + ' ' + period
          : $("#indicators option:selected").text() + ' ' + period;
  
      Highcharts.mapChart('map-container', {
          title: {
              text: title
          },
          //subtitle: {
          //    text: "Test Subtite"
          //},
  
          mapNavigation: {
              enabled: true,
              buttonOptions: {
                  verticalAlign: 'top'
              }
          },
          tooltip: {
              headerFormat: '',
              pointFormat: '{point.properties.DISTRICT}' + '<br>' +
                  'UGX: {point.value}'
          },
          colorAxis: {
              tickPixelInterval: 100,
              min: 0,
              minColor: '#d9f0a3',
              maxColor: '#005a32'
          },
          series: [{
              data: data,
              mapData: JSON.parse($('#geojson').val()),
              joinBy: ["D_CODE", 0],
              keys: ["D_CODE", 'value'],
              name: 'Districts Data',
              states: {
                  hover: {
                      color: '#a4edba'
                  }
              },
              dataLabels: {
                  enabled: true,
                  format: '{point.properties.DISTRICT}'
              }
          }]
      });
  }
  function ComplaintsMaps(data) {
      $('.ajax-loader ').fadeOut();
      var period = $("#reporting-period option:selected").text();
      period = period === 'Select' ? '' : period;
  
      var title = $("#indicators option:selected").val() === '5'
          ? "Total Volume of Grants Paid by end of" + ' ' + period
          : $("#indicators option:selected").text() + ' ' + period;
  
      Highcharts.mapChart('map-container', {
          title: {
              text: title
          },
          //subtitle: {
          //    text: "Test Subtite"
          //},
  
          mapNavigation: {
              enabled: true,
              buttonOptions: {
                  verticalAlign: 'top'
              }
          },
          tooltip: {
              headerFormat: '',
              pointFormat: ' ' + '<br>' +
                  'No. of Compaints: {point.value}'
          },
          colorAxis: {
              tickPixelInterval: 100,
              min: 0,
              minColor: '#d9f0a3',
              maxColor: '#005a32'
          },
          series: [{
              data: data,
              mapData: JSON.parse($('#geojson').val()),
              joinBy: ["D_CODE", 0],
              keys: ["D_CODE", 'value'],
              name: 'Districts Data',
              states: {
                  hover: {
                      color: '#a4edba'
                  }
              },
              dataLabels: {
                  enabled: true,
                  format: '{point.properties.DISTRICT}'
              }
          }]
      });
  }
  */
  function showLoader() {
      $('.ajax-loader ').show();
  }
  
  function hideLoader() {
      $('.ajax-loader ').fadeOut();
  }
  var ajaxError = function (object) {
      console.log("An error has occured processing your request.");
      hideLoader();
  };
  var ajaxAlways = function (object) {
      hideLoader();
  };