﻿using System;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Models;
using CCTPMIS.Services;

namespace CCTPMIS.PspApi
{
    using System.Net.Http.Formatting;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.InitializeMappings();
            //GlobalConfiguration.Configuration.Formatters.Clear();
            //GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
            //GlobalConfiguration.Configuration.Formatters.Add(new  FormUrlEncodedMediaTypeFormatter());

        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            if (ex is ThreadAbortException)
                return;

            var Logger = new LogService(new GenericService(new GenericRepository<ApplicationDbContext>()));
            try
            {
                Logger.FileLog(MisKeys.ERROR, $"{ex.Message} \n {ex.InnerException?.Message}", "Exception");
            }
            catch (Exception exception)
            {
                Logger.FileLog(MisKeys.ERROR, $"{ex.Message} \n {exception.Message}", "Exception");
            }

            //Response.Redirect("/");
        }

    }
}