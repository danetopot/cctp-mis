﻿using System;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Cors;
using System.Web.Http;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Repositories;
using CCTPMIS.Services;
using Microsoft.Owin.Cors;

using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;

[assembly: OwinStartup(typeof(CCTPMIS.PspApi.Startup))]

namespace CCTPMIS.PspApi
{


    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(new CorsOptions()
            {
                PolicyProvider = new CorsPolicyProvider()
                {
                    PolicyResolver = request => request.Path.StartsWithSegments(new PathString("/Token")) ?
                                                    Task.FromResult(new CorsPolicy
                                                    {
                                                        AllowAnyOrigin = true
                                                    }) :
                                                    Task.FromResult<CorsPolicy>(null)
                }
            });
            //var webApiConfiguration = new HttpConfiguration();
            //webApiConfiguration.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional, controller = "values" });
            //app.UseNinjectMiddleware(CreateKernel);
                //.UseNinjectWebApi(webApiConfiguration);
            ConfigureAuth(app);
        }

        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            kernel.Bind<ApplicationDbContext>().ToSelf().InRequestScope();
            kernel.Bind<IGenericRepository>().To<GenericRepository<ApplicationDbContext>>().InRequestScope();
            kernel.Bind<IGenericService>().To<GenericService>().InRequestScope();
            kernel.Bind<ILogService>().ToConstructor(_ => new LogService(new GenericService(new GenericRepository<ApplicationDbContext>())));

            kernel.Load(Assembly.GetExecutingAssembly());
            return kernel;
        }
    }


}