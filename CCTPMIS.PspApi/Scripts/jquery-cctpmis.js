﻿(function ($) {
    $(window).on('load', function () { $.force_appear(); }); //Attaches load event
    $(window).trigger('load'); //Triggers load event manually.
});

(function ($) {

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log)
                $("#dataFileLabel").text(log);
        }

    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '0%' // optional
    });

    $(".main-menu li ul li a")
        .each(function () {

       //     if (this.href.indexOf(window.location.href)) {
            if (this.href === window.location.href) {
                $(this).addClass("menu-active");
                $(this).parent().addClass("active");

                if ($(this).parents(2).hasClass("has-sub")) {
                    $(this).parents(3).addClass("active");
                }
            }
        });



    $('#side-nav').metisMenu();

    $(function () {
        $('[data-toggle="popover"]').popover({ trigger: 'hover' });
        $('[data-toggle="tooltip"]').tooltip();

        $(document).on("click", ".modal-link", function (e) {
            e.preventDefault();

            $('#popup').load($(this).attr("href"));
            $('#popup').modal('show');
            $('#popup').modal({ keyboard: false });
        });
    });

    $(".mobile-menu-icon").click(function (event) {
        event.preventDefault();
    });
    $(".btn-disabled").click(function (e) {
        e.preventDefault();

        swal({
            title: "Sorry",
            text: "You cannot approve an Enrolment Plan that you created!",
            animation: "slide-from-top",
            type: 'info',
            allowEscapeKey: true,
            showConfirmButton: true,
            html: true,
            confirmButtonColor: "#3C8DBC",
            //timer: 2000,
            //confirmButtonText: "Cool",
            cancelButtonText: "OK",
            allowOutsideClick: true
        });

    });
    var $window = $(window), $container = $('div.page-container');
    var $isCollapsed = false;

    $(".sidebar-collapse-icon").click(function (event) {
        event.preventDefault();
        $container.toggleClass('sidebar-collapsed').toggleClass('can-resize');
        $(".page-container").addClass("sidebar-collapsing");
        if ($isCollapsed === true) {
            $(".page-sidebar, .sidebar-fixed").animate({
                width: 280
            }, function () {
                $(".page-container").removeClass("sidebar-collapsing");
                createCookie('sidebar', 'open');

            });
        }
        else {
            $(".page-sidebar, .sidebar-fixed").animate({
                width: 66
            }, function () {
                $(".page-container").removeClass("sidebar-collapsing");
                createCookie('sidebar', 'closed');
            });
        }
        $isCollapsed = !$isCollapsed;
    });

    if (readCookie('sidebar') ==='closed') {
        $(".sidebar-collapse-icon").click();
    }


    if ($container.hasClass('sidebar-collapsed')) {
        $isCollapsed = true;
        $(".page-sidebar, .sidebar-fixed").animate({
            width: 66
        });
    }
    $window.resize(function resize() {

        var windowWidth = $window.outerWidth();
        if (windowWidth < 951 && windowWidth > 767) {
            if ($container.hasClass('can-resize') === false) {
                $container.addClass('sidebar-collapsed');
                $(".page-sidebar, .sidebar-fixed").animate({
                    width: 66
                });
                $isCollapsed = true;
            }
        } else if (windowWidth < 767) {
            $container.removeClass('sidebar-collapsed');
            $container.removeClass('can-resize');
        } else {
            if ($container.hasClass('can-resize') === false && $isCollapsed === false) {
                $container.removeClass('sidebar-collapsed');
            }
        }

    }).trigger('resize');
    $('body').on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="reload"]', function (ev) {
        ev.preventDefault();

        var $this = $(this).closest('.panel');

        $this.block({
            message: '',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '50%'
            },
            overlayCSS: { backgroundColor: '#FFF' }
        });
        $this.addClass('reloading');

        setTimeout(function () {
            $this.unblock();
            $this.removeClass('reloading');
        }, 900);
    }).on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="close"]', function (ev) {
        ev.preventDefault();

        var $this = $(this);
        var $panel = $this.closest('.panel');

        $panel.fadeOut(500, function () {
            $panel.remove();
        });
    }).on('click', '.panel > .panel-heading > .panel-tool-options li > a[data-rel="collapse"]', function (ev) {
        ev.preventDefault();

        var $this = $(this),
            $panel = $this.closest('.panel'),
            $body = $panel.children('.panel-body, .table'),
            doCollapse = !$panel.hasClass('panel-collapse');

        if ($panel.is('[data-collapsed="1"]')) {
            $panel.attr('data-collapsed', 0);
            $body.hide();
            doCollapse = false;
        }

        if (doCollapse) {
            $body.slideUp('normal');
            $panel.addClass('panel-collapse');
        }
        else {
            $body.slideDown('normal');
            $panel.removeClass('panel-collapse');
        }
    });

    // removeable-list -- remove parent elements
    var $removalList = $(".removeable-list");
    $(".removeable-list .remove").each(function () {
        var $this = $(this);
        $this.click(function (event) {
            event.preventDefault();

            var $parent = $this.parent('li');
            $parent.slideUp(500, function () {
                $parent.delay(3000).remove();

                if ($removalList.find("li").length === 0) {
                    $removalList.html('<li class="text-danger"><p>All items has been deleted.</p></li>');
                }
            });
        });
    });

    var $filterBtn = $(".toggle-filter");
    var $filterBoxId = $filterBtn.attr('data-block-id');
    var $filterBox = $('#' + $filterBoxId);

    if ($filterBox.hasClass('visible-box')) {
        $filterBtn.parent('li').addClass('active');
    }

    $filterBtn.click(function (event) {
        event.preventDefault();

        if ($filterBox.hasClass('visible-box')) {
            $filterBtn.parent('li').removeClass('active');
            $filterBox.removeClass('visible-box').addClass('hidden-box').slideUp();
        } else {
            $filterBtn.parent('li').addClass('active');
            $filterBox.removeClass('hidden-box').addClass('visible-box').slideDown();
        }
    });

    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
    });

    // Add slideUp animation to Bootstrap dropdown when collapsing.
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
    });



    $('.startdate').datetimepicker({
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('.enddate').val() ? $('.enddate').val() : false
            });
        },
        timepicker: false
    });
    $('.enddate').datetimepicker({
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('.startdate').val() ? $('.startdate').val() : false
            });
        },
        timepicker: false
    });


})(jQuery);

function showTooltip(x, y, contents) {
    var $windowWidth = $(window).width();
    var leftValue = x + 20;
    var toolTipWidth = 160;
    if ($windowWidth < (leftValue + toolTipWidth)) {
        leftValue = x - 32 - toolTipWidth;
    }

    $('<div id="flotTip" > ' + contents + ' </div>').css({
        top: y - 16,
        left: leftValue,
        position: 'absolute',
        padding: '5px 10px',
        border: '1px solid #111111',
        'min-width': toolTipWidth,
        background: '#ffffff',
        background: '-moz-linear-gradient(top,  #ffffff 0%, #f9f9f9 100%)',
        background: '-webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#f9f9f9))',
        background: '-webkit-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: '-o-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: '-ms-linear-gradient(top,  #ffffff 0%,#f9f9f9 100%)',
        background: 'linear-gradient(to bottom,  #ffffff 0%,#f9f9f9 100%)',
        '-webkit-border-radius': '5px',
        '-moz-border-radius': '5px',
        'border-radius': '5px',
        'z-index': '100'
    }).appendTo('body').fadeIn();
}

/*
 * This function will remove its parent element
 *
 * @param $eleObj
 * @param $parentEle
 */

function removeElement($ele, $parentEle) {
    var $this = $($ele);
    $this.parent($parentEle).css({
        opacity: '0'
    });
}



//Create Cookie Function
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else
        var eexpires = "";
    document.cookie = name + "=" + value + eexpires + "; path=/";
}

//Read Cookie Function
function readCookie(name) {
    var nameEq = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEq) === 0) return c.substring(nameEq.length, c.length);
    }
    return null;
}

//Erase Cookie Function
function eraseCookie(name) {
    createCookie(name, "", -1);
}
