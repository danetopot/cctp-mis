﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using CCTPMIS.Business.Context;
using CCTPMIS.Business.Interfaces;
using CCTPMIS.Models;
using CCTPMIS.Models.Api;
using CCTPMIS.Models.Enrolment;
using CCTPMIS.Services;
using Microsoft.AspNet.Identity;


namespace CCTPMIS.PspApi.Controllers
{
    using System.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class PaymentController : ApiController
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IGenericService GenericService;
        private readonly LogService LogService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericService"></param>
        /// <param name="logService"></param>
        public PaymentController(IGenericService genericService, LogService logService)
        {
            GenericService = genericService;
            LogService = logService;
        }


        /// <summary>
        /// Payroll Credit Transaction Update - This method is used to update the status of the payroll transfer transaction following a payroll instruction
        /// </summary>
        /// <param name="model"></param>
        /// <returns name="apiFeedback">   </returns>
        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Payment/CreditReport")]
        public IHttpActionResult PostCreditReport(CreditReportViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            LogService.FileLog(MisKeys.CREDITREPORT, Newtonsoft.Json.JsonConvert.SerializeObject(model), userId.ToString());
            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, description, userId.ToString());
                return Ok(apiFeedback);
            }
            try
            {
                var parameterNames = "@PaymentCycleId,@EnrolmentNo,@BankCode,@BranchCode,@AccountNo,@AccountName,@AmountTransferred,@WasTransferSuccessful,@TrxNo,@TrxDate,@TrxNarration,@UserId";
                var parameterList = new List<ParameterEntity>
                      {
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("PaymentCycleId",model.PaymentCycleId)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("EnrolmentNo",model.EnrolmentNo)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("BankCode",model.BankCode)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("BranchCode",model.BranchCode)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("AccountNo",model.AccountNo)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("AccountName",model.AccountName)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("AmountTransferred",model.AmountTransferred)},
                        new ParameterEntity{ParameterTuple =new Tuple<string, object>("WasTransferSuccessful ",model.WasTransferSuccessful)},
                        !string.IsNullOrEmpty(model.TrxNo)?
                            new ParameterEntity
                                {
                                    ParameterTuple =new Tuple<string,object>("TrxNo",model.TrxNo)
                                }:
                            new ParameterEntity
                                       {
                                           ParameterTuple =new Tuple<string,object>("TrxNo",DBNull.Value)
                                       },
                        model.TrxDate != null? 
                            new ParameterEntity
                                {
                                    ParameterTuple =new Tuple<string,object>("TrxDate",model.TrxDate)
                                }:
                            new ParameterEntity
                                {
                                    ParameterTuple =new Tuple<string,object>("TrxDate",DBNull.Value)
                                },
                        !string.IsNullOrEmpty(model.TrxNarration)?
                            new ParameterEntity
                                {
                                    ParameterTuple =new Tuple<string,object>("TrxNarration",model.TrxNarration)
                                }:
                            new ParameterEntity
                                {
                                    ParameterTuple =new Tuple<string,object>("TrxNarration",DBNull.Value)
                                },
                        new ParameterEntity
                            {
                                ParameterTuple =new Tuple<string, object>("UserId",userId)
                            }};

                var spIntVm = GenericService.GetOneBySp<ApiStatus>("PSPPayrollTrx", parameterNames, parameterList);

                apiFeedback = new ApiStatus
                {
                    StatusId = spIntVm.StatusId,
                    Description = spIntVm.Description
                };

            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "Error - " + e.Message


                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, e.Message + e.Message, userId.ToString());
            }
            return Ok(apiFeedback);

        }


        /// <summary>
        /// Payroll Credit Transaction Finalization -  This method is used to finalize the update process of payroll transfer transactions following a payroll instruction
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(PayrolApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Payment/CreditReport/Finalize")]
        public IHttpActionResult PostCreditReportFinalize(CreditReportFinalizeViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            LogService.FileLog(MisKeys.CREDITREPORTFINALIZE, Newtonsoft.Json.JsonConvert.SerializeObject(model), userId.ToString());
            PayrolApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = -1,
                    NoOfRecs = 0,
                    Description = description
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, description, userId.ToString());
                return Ok(apiFeedback);
            }
            try
            {
                var parameterNames = "@PaymentCycleId,@BankCode,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "PaymentCycleId",
                                                            model.PaymentCycleId)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "BankCode",
                                                            model.BankCode)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };

                var payrolApiStatus = GenericService.GetOneBySp<PayrolApiStatus>("PSPFinalizePayrollTrx", parameterNames, parameterList);
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = payrolApiStatus.StatusId,
                    Description = payrolApiStatus.Description,
                    NoOfRecs = payrolApiStatus.NoOfRecs,
                };

            }
            catch (Exception e)
            {
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = -1,
                    NoOfRecs = 0,
                    Description = "Error - " + e.Message
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, e.Message + e.Message, userId.ToString());
            }
            return Ok(apiFeedback);

        }



        /// <summary>
        /// Monthly Account Activity Update
        /// This method is used to update the beneficiary account monthly activities including but not limited to payment withdrawal, dormancy, inactivity, proof of life, due for clawback with respective amount etc.
        /// </summary>
        /// <param name="model"> Payroll Transaction Model from the banks </param>
        /// <returns></returns>

        [ResponseType(typeof(ApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Payment/ActivityReport")]
        public IHttpActionResult PostActivityReport(AccountActivityReportViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            LogService.FileLog(MisKeys.ACTIVITYREPORT, Newtonsoft.Json.JsonConvert.SerializeObject(model), userId.ToString());
            ApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = description
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.ACTIVITYREPORT, description, userId.ToString());
                return Ok(apiFeedback);
            }
            try
            {
                var parameterNames = "@MonthNo,@Year,@BankCode,@BranchCode,@EnrolmentNo,@AccountNo,@AccountName,@AccountOpenedOn,@HadUniqueWdl,@UniqueWdlTrxNo,@UniqueWdlDate,@HadBeneBiosVerified,@IsDormant,@DormancyDate,@IsDueForClawback,@ClawbackAmount,@UserId";
                var parameterList = new List<ParameterEntity>();
                {
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("MonthNo", model.MonthNo) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("Year", model.Year) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("BankCode", model.BankCode) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("BranchCode", model.BranchCode) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("EnrolmentNo", model.EnrolmentNo) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("AccountNo", model.AccountNo) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("AccountName", model.AccountName) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("AccountOpenedOn", model.AccountOpenedOn) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("HadUniqueWdl", model.HadUniqueWdl) });
                    parameterList.Add(!string.IsNullOrEmpty(model.UniqueWdlTrxNo) ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("UniqueWdlTrxNo", model.UniqueWdlTrxNo) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("UniqueWdlTrxNo", DBNull.Value) });
                    parameterList.Add(model.UniqueWdlDate != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("UniqueWdlDate", model.UniqueWdlDate) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("UniqueWdlDate", DBNull.Value) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("HadBeneBiosVerified", model.HadBeneBiosVerified) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("IsDormant", model.IsDormant) });
                    parameterList.Add(model.DormancyDate != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("DormancyDate", model.DormancyDate) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("DormancyDate", DBNull.Value) });                    
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("IsDueForClawback", model.IsDueForClawback) });                    
                     parameterList.Add(model.ClawbackAmount != null ? new ParameterEntity { ParameterTuple = new Tuple<string, object>("ClawbackAmount", model.ClawbackAmount) } : new ParameterEntity { ParameterTuple = new Tuple<string, object>("ClawbackAmount", DBNull.Value) });
                    parameterList.Add(new ParameterEntity { ParameterTuple = new Tuple<string, object>("UserId", userId) });
                }
                var spIntVm = GenericService.GetOneBySp<PayrolApiStatus>("PSPBeneAccountMonthlyActivity", parameterNames, parameterList);
                apiFeedback = new ApiStatus
                {
                    StatusId = spIntVm.StatusId,
                    Description = spIntVm.Description
                };

            }
            catch (Exception e)
            {
                apiFeedback = new ApiStatus
                {
                    StatusId = -1,
                    Description = "Error - " + e.Message

                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, e.Message + e.Message, userId.ToString());

            }
            return Ok(apiFeedback);
        }




        /// <summary>
        /// Monthly Account Activity Finalization
        /// This method is used to finalize the update process of monthly acc transactions following a payroll instruction
        ///
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ResponseType(typeof(PayrolApiStatus))]
        [HttpPost]
        [Authorize]
        [Route("api/Payment/ActivityReport/Finalize")]
        public IHttpActionResult PostActivityReportFinalize(AccountActivityReportFinalizeViewModel model)
        {
            var userId = int.Parse(User.Identity.GetUserId());
            LogService.FileLog(MisKeys.ACTIVITYREPORTFINALIZE, Newtonsoft.Json.JsonConvert.SerializeObject(model), userId.ToString());
            PayrolApiStatus apiFeedback;
            if (!ModelState.IsValid)
            {
                var description = $" {GetErrorListFromModelState(ModelState)} - The Posted Data has been rejected";
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = -1,
                    NoOfRecs = 0,
                    Description = description
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.ACTIVITYREPORTFINALIZE, description, userId.ToString());
                return Ok(apiFeedback);
            }
            try
            {
                var parameterNames = "@MonthNo,@Year,@BankCode,@UserId";
                var parameterList = new List<ParameterEntity>
                                        {
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "MonthNo",
                                                            model.MonthNo)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "Year",
                                                            model.Year)
                                                },
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "BankCode",
                                                            model.BankCode)
                                                }
                                            ,
                                            new ParameterEntity
                                                {
                                                    ParameterTuple =
                                                        new Tuple<string, object>(
                                                            "UserId",
                                                            userId)
                                                }
                                        };
                var payrolApiStatus = GenericService.GetOneBySp<PayrolApiStatus>("PSPFinalizeBeneAccountMonthlyActivity", parameterNames, parameterList);
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = payrolApiStatus.StatusId,
                    Description = payrolApiStatus.Description,
                    NoOfRecs = payrolApiStatus.NoOfRecs,
                };
            }
            catch (Exception e)
            {
                apiFeedback = new PayrolApiStatus
                {
                    StatusId = -1,
                    NoOfRecs = 0,
                    Description = "Error - " + e.Message
                };
                LogService.FileLog(MisKeys.ERROR + "-" + MisKeys.CREDITREPORT, e.Message + e.Message, userId.ToString());
            }
            return Ok(apiFeedback);
        }     
        private string GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;
            var delimiter = @" ";
            var errorList = query.ToList();
            return errorList.Aggregate((i, j) => i + delimiter + j);
        }
    }
}

