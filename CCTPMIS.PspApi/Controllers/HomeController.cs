﻿using System.Web.Mvc;

namespace CCTPMIS.PspApi.Controllers
{
    /// <summary>
    /// Home Modules
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "CCTPMIS PSP API DOCUMENTATION ";

            return View();
        }
    }
}
