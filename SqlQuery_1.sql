﻿
ALTER TABLE CaseCategory ALTER COLUMN DOCUMENTSTOATTACH VARCHAR(512) 

ALTER TABLE CaseCategory ALTER COLUMN  ReportingProcess VARCHAR(512) 
GO




IF NOT OBJECT_ID('Source') IS NULL	
DROP TABLE Source
GO

CREATE TABLE Source(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_Source PRIMARY KEY (Id)
)
GO

insert into Source(Name,[Description])
SELECT T1.[Name],T1.[Description] FROM (
SELECT '70 Plus Registration' as [Name], '70 plus Inua Jamii Registration' as [Description]
UNION
SELECT 'Legacy MIS Migration', '2018/2019 Legacy MIS beneficiaries Migration'
UNION
SELECT 'CCTP-MIS Registration', 'CCTP-MIS Registration Modules'
) T1
LEFT JOIN Source T2 ON T1.Name = T2.[NAME] WHERE T2.ID IS NULL



go