﻿namespace CCTPMIS.Business.Repositories
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.SqlClient;
    using System.Linq;

    using Interfaces;

    public static class MultipleResultSets
    {
        public static MultipleResultSetWrapper MultipleResults(this DbContext db)
        {
            return new MultipleResultSetWrapper(db);
        }

        public class MultipleResultSetWrapper
        {
            public List<Func<IObjectContextAdapter, DbDataReader, IEnumerable>> _resultSet;

            public List<Func<IObjectContextAdapter, DbDataReader, IEnumerable>> _resultSets;

            private readonly string _storedProcedure;

            private readonly DbContext db;

            public MultipleResultSetWrapper(DbContext db)
            {
                this.db = db;
                _resultSets = new List<Func<IObjectContextAdapter, DbDataReader, IEnumerable>>();
            }

            // public MultipleResultSetWrapper WithProperties(List<TResult> withProperties = null)
            // {
            // var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
            // .Where(type => !String.IsNullOrEmpty(type.Namespace))
            // .Where(type => type.BaseType != null && type.BaseType.IsGenericType
            // && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

            // foreach (var withProperty in withProperties)
            // {
            // this.With<withProperty>();
            // }

            // return this;
            // }
            public List<IEnumerable> Execute(
                string storedProcedure,
                string parameterNames = null,
                List<ParameterEntity> parameterValues = null,
                List<dynamic> withs = null)
            {
                var results = new List<IEnumerable>();

                using (var connection = db.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "EXEC " + storedProcedure + "  " + parameterNames;

                    SqlParameter[] parameters = new SqlParameter[parameterValues.Count];
                    for (var i = 0; i <= parameterValues.Count - 1; i++)
                    {
                        parameters[i] = new SqlParameter(
                            parameterValues[i].ParameterTuple.Item1,
                            parameterValues[i].ParameterTuple.Item2);
                    }

                    if (parameters.Any())
                        command.Parameters.AddRange(parameters);

                    using (var reader = command.ExecuteReader())
                    {
                        var adapter = (IObjectContextAdapter)db;
                        foreach (var resultSet in _resultSets)
                        {
                            results.Add(resultSet(adapter, reader));
                            reader.NextResult();
                        }
                    }

                    return results;
                }
            }

            public MultipleResultSetWrapper With<TResult>()
            {
                _resultSets.Add((adapter, reader) => adapter.ObjectContext.Translate<TResult>(reader).ToList());

                return this;
            }

            public MultipleResultSetWrapper WithSingle<TResult>()
            {
                _resultSets.Add((adapter, reader) => adapter.ObjectContext.Translate<TResult>(reader).ToList());

                return this;
            }
        }
    }
}