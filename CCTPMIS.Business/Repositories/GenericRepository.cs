﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CCTPMIS.Business.Interfaces;

namespace CCTPMIS.Business.Repositories
{


    public class GenericRepository<TContext> : IGenericRepository
        where TContext : DbContext
    {
        protected readonly TContext Context;

        public GenericRepository(TContext context)
        {
            Context = context;
        }

        public GenericRepository()
        {
        }

        public void AddRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class
        {
            Context.Set<TEntity>().AddRange(entities);
        }

        public virtual void Create<TEntity>(TEntity entity)
            where TEntity : class
        {
            Context.Set<TEntity>().Add(entity);
        }


        public virtual void AddOrUpdate<TEntity>(TEntity entity)
            where TEntity : class
        {
            Context.Set<TEntity>().AddOrUpdate(entity);
        }
        public virtual void Delete<TEntity>(object id)
            where TEntity : class
        {
            var entity = Context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity)
            where TEntity : class
        {
            var dbSet = Context.Set<TEntity>();
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }

            dbSet.Remove(entity);
        }

        public virtual IEnumerable<TEntity> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual IEnumerable<TEntity> GetAll<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(null, orderBy, includeProperties, skip, take).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            return await GetQueryable<TEntity>(null, orderBy, includeProperties, skip, take).ToListAsync();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            return await GetQueryable<TEntity>(filter, orderBy, includeProperties, skip, take).ToListAsync();
        }


        public virtual async Task<IQueryable<TEntity>> GetSearchableQueryable<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            return  GetQueryable<TEntity>(filter, orderBy, includeProperties, skip, take);
        }

        public virtual TEntity GetById<TEntity>(object id)
            where TEntity : class
        {
            return Context.Set<TEntity>().Find(id);
        }

        public virtual Task<TEntity> GetByIdAsync<TEntity>(object id)
            where TEntity : class
        {
            return Context.Set<TEntity>().FindAsync(id);
        }

        public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter).Count();
        }

        public virtual Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter).CountAsync();
        }

        public virtual bool GetExists<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter).Any();
        }

        public virtual Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter).AnyAsync();
        }

        public virtual TEntity GetFirst<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter, orderBy, includeProperties).FirstOrDefault();
        }

        public virtual async Task<TEntity> GetFirstAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return await GetQueryable(filter, orderBy, includeProperties).FirstOrDefaultAsync().ConfigureAwait(true);
        }

        public IEnumerable<TEntity> GetManyBySp<TEntity>(
            string procName,
            string parameterNames = null,
            List<ParameterEntity> parameterValues = null)
            where TEntity : class
        {
            if (parameterValues == null)
            {
                return Context.Database.SqlQuery<TEntity>(procName).ToList();
            }

            var storedprocParams = new object[parameterValues.Count];
            for (var i = 0; i <= parameterValues.Count - 1; i++)
            {
                storedprocParams[i] = new SqlParameter(
                    parameterValues[i].ParameterTuple.Item1,
                    parameterValues[i].ParameterTuple.Item2);
            }

            return Context.Database.SqlQuery<TEntity>(procName + ' ' + parameterNames, storedprocParams).ToList();
        }

        public List<IEnumerable> GetMultipleResultSet<TResult>(
            string procName,
            string parameterNames = null,
            List<ParameterEntity> parameterValues = null,
            Func<DbDataReader, TResult> mapEntities = null)
            where TResult : class
        {
            return Context.MultipleResults().Execute(procName, parameterNames, parameterValues);
        }

        public virtual TEntity GetOne<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = "")
            where TEntity : class
        {
            return GetQueryable<TEntity>(filter, null, includeProperties).SingleOrDefault();
        }

        public virtual Task<TEntity> GetOneAsync<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            string includeProperties = null)
            where TEntity : class
        {
            return GetQueryable(filter, null, includeProperties).SingleOrDefaultAsync();
        }

        public TEntity GetOneBySp<TEntity>(
            string procName,
            string parameterNames = null,
            List<ParameterEntity> parameterValues = null)
            where TEntity : class
        {
            if (parameterValues == null)
            {
                return Context.Database.SqlQuery<TEntity>(procName).Single();
            }

            var storedprocParams = new object[parameterValues.Count];
            for (var i = 0; i <= parameterValues.Count - 1; i++)
            {
                storedprocParams[i] = new SqlParameter(
                    parameterValues[i].ParameterTuple.Item1,
                    parameterValues[i].ParameterTuple.Item2);
            }

            TEntity tEntity = null;
            try
            {

                // log the Sp called
                tEntity = Context.Database.SqlQuery<TEntity>(procName + ' ' + parameterNames, storedprocParams)
                    .Single();
            }
            catch (SqlException e)
            {
                var errorMessages = e.Message;
                throw new Exception(e.Message);
            }

            return tEntity;
        }

        public virtual IEnumerable<TEntity> GetWithRawSql<TEntity>(string query, params object[] parameters)
            where TEntity : class
        {
            var tEntity = Context.Database.SqlQuery<TEntity>(query, parameters).ToList();

            return tEntity;
        }
        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : class
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }


        public virtual int Save()
        {
            try
            {
                return Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
                return 0;
            }
        }

        public virtual Task SaveAsync()
        {
            try
            {
                return Context.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }
            return Task.FromResult(0);
        }

        public virtual void Update<TEntity>(TEntity entity)
            where TEntity : class
        {
            //entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = modifiedBy;
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,

            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(
                new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return query;
        }


        public virtual IQueryable<TResult> GetSet<TResult, TKey, TEntity>(
            Expression<Func<TEntity, TResult>> firstSelector,
            Expression<Func<TResult, TKey>> orderBy,
            Expression<Func<TEntity, bool>> filter = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            var predicates = new List<Expression<Func<TEntity, bool>>> { filter };
            var entities = GetQueryable(filter);
            if (skip != null && take != null)
            {
                return predicates
                    .Aggregate(entities, (current, predicate) => current.Where(predicate))
                    .Select(firstSelector)
                    .OrderBy(orderBy)
                .Skip(skip.Value)
                .Take(take.Value);

            }
            else
            {
                return predicates
                    .Aggregate(entities, (current, predicate) => current.Where(predicate)) 
                    .Select(firstSelector)
                    .OrderBy(orderBy);

            }
        }

        public virtual IQueryable<TReturn> GetGroupedSet<TResult, TKey, TGroup, TReturn, TEntity>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TResult>> firstSelector, Expression<Func<TResult, TKey>> orderSelector, Func<TResult, TGroup> groupSelector, Func<IGrouping<TGroup, TResult>, TReturn> selector, int? skip, int? take) where TEntity : class
        {
            var predicates = new List<Expression<Func<TEntity, bool>>> { filter };
            var entities = GetQueryable(filter);

            if (skip != null && take != null)
            {
                return predicates
                    .Aggregate(entities, (current, predicate) => current.Where(predicate))
                    .Select(firstSelector)
                    .OrderBy(orderSelector)
                    .GroupBy(groupSelector)
                    .Skip(skip.Value)
                    .Take(take.Value)
                    .Select(selector).AsQueryable();
            }
            else
            {
                return predicates
                    .Aggregate(entities, (current, predicate) => current.Where(predicate))
                    .Select(firstSelector)
                    .OrderBy(orderSelector)
                    .GroupBy(groupSelector)?
                    .Select(selector)?.AsQueryable();
            }
        }


        public virtual IList<TReturn> GetGrouped<TResult, TKey, TGroup, TReturn, TEntity>(Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TResult>> firstSelector, Expression<Func<TResult, TKey>> orderSelector, Func<TResult, TGroup> groupSelector, Func<IGrouping<TGroup, TResult>, TReturn> selector, int? skip, int? take) where TEntity : class
        {
            var predicates = new List<Expression<Func<TEntity, bool>>> { filter };
            var entities = GetQueryable(filter);

            if (skip != null && take != null)
            {
                return predicates
            .Aggregate(entities, (current, predicate) => current.Where(predicate))
            .Select(firstSelector)
            .OrderBy(orderSelector)
            .GroupBy(groupSelector)
            .Skip(skip.Value)
            .Take(take.Value)
            .Select(selector)
            .ToList();
            }
            else
            {
                return predicates
.Aggregate(entities, (current, predicate) => current.Where(predicate))
.Select(firstSelector)
.OrderBy(orderSelector)
.GroupBy(groupSelector)
.Select(selector)
.ToList();
            }
        }

        public virtual void ThrowEnhancedValidationException(DbEntityValidationException e)
        {
            var errorMessages = e.EntityValidationErrors.SelectMany(x => x.ValidationErrors)
                .Select(x => x.ErrorMessage);
            var fullErrorMessage = string.Join("; ", errorMessages);
            var exceptionMessage = string.Concat(e.Message, " The validation errors are: ", fullErrorMessage);
            throw new DbEntityValidationException(exceptionMessage, e.EntityValidationErrors);
        }




        private bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Context?.Dispose();
        }


        public static IQueryable<EntityWithCount<TEntity>> GetWithTotal<TEntity>(IQueryable<TEntity> entities, int page, int pageSize) where TEntity : class
        {
            return entities
                .Select(e => new EntityWithCount<TEntity> { Entity = e, Count = entities.Count() })
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
        }



    }

    public class EntityWithCount<T> where T : class
    {
        public T Entity { get; set; }
        public int Count { get; set; }
    }

    public static class PagingHelpers
    {
        /// <summary>
        /// Gets a paged list of entities with the total appended to each row in the resultset. This is a faster way of doing things than using 2 seperate queries
        /// </summary>
        /// <typeparam name="T">The type of the entity, can be inferred by the type of queryable you pass</typeparam>
        /// <param name="entities">A queryable of entities</param>
        /// <param name="page">the 1 indexed page number you are interested in, cannot be zero or negative</param>
        /// <param name="pageSize">the size of the page you want, cannot be zero or negative</param>
        /// <returns>A queryable of the page of entities with counts appended.</returns>
        public static IQueryable<EntityWithCount<T>> GetPageWithTotal<T>(this IQueryable<T> entities, int page, int pageSize) where T : class
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities");
            }

            return entities
                .Select(e => new EntityWithCount<T> { Entity = e, Count = entities.Count() })
                .GetPage(page, pageSize);
        }

        /// <summary>
        /// Gets a paged list of entities
        /// </summary>
        /// <typeparam name="T">The type of the entity, can be inferred by the type of queryable you pass</typeparam>
        /// <param name="entities">A queryable of entities</param>
        /// <param name="page">the 1 indexed page number you are interested in, cannot be zero or negative</param>
        /// <param name="pageSize">the size of the page you want, cannot be zero or negative</param>
        /// <returns>A queryable of the page of entities.</returns>
        public static IQueryable<T> GetPage<T>(this IQueryable<T> entities, int page, int pageSize)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities");
            }
            if (page < 1)
            {
                throw new ArgumentException("Must be positive", "page");
            }
            if (pageSize < 1)
            {
                throw new ArgumentException("Must be positive", "pageSize");
            }
            return entities
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
        }
    }

    public static class PredicateBuilder
    {


        public static Expression<Func<X, Y>> Compose<X, Y, Z>(this Expression<Func<Z, Y>> outer, Expression<Func<X, Z>> inner)
        {
            
            return Expression.Lambda<Func<X, Y>>(
                SubstExpressionVisitor.Replace(outer.Body, outer.Parameters[0], inner.Body),
                inner.Parameters[0]);
        }

        public static Expression<Func<T, bool>> Begin<T>(bool value = false)
        {
            if (value)
            {
                return parameter => true; //value cannot be used in place of true/false
            }

            return parameter => false;
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
        {
            // return CombineLambdas(left, right, ExpressionType.AndAlso);
            if (b == null)
            {
                return a;
            }

            ParameterExpression p = a.Parameters[0];

            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[b.Parameters[0]] = p;

            Expression body = Expression.AndAlso(a.Body, visitor.Visit(b.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
        {
            //   return CombineLambdas(left, right, ExpressionType.OrElse);

            if (b == null)
            {
                return a;
            }

            ParameterExpression p = a.Parameters[0];

            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[b.Parameters[0]] = p;

            Expression body = Expression.OrElse(a.Body, visitor.Visit(b.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }


        private static bool IsExpressionBodyConstant<T>(Expression<Func<T, bool>> left)
        {
            return left.Body.NodeType == ExpressionType.Constant;
        }



        private static Expression<Func<T, bool>> CombineLambdas<T>(this Expression<Func<T, bool>> left,
            Expression<Func<T, bool>> right, ExpressionType expressionType)
        {
            //Remove expressions created with Begin<T>()
            if (IsExpressionBodyConstant(left))
            {
                return (right);
            }

            ParameterExpression p = left.Parameters[0];

            SubstExpressionVisitor visitor = new SubstExpressionVisitor();
            visitor.subst[right.Parameters[0]] = p;

            Expression body = Expression.MakeBinary(expressionType, left.Body, visitor.Visit(right.Body));
            return Expression.Lambda<Func<T, bool>>(body, p);
        }
        internal class SubstExpressionVisitor : ExpressionVisitor
        {
            public Dictionary<Expression, Expression> subst = new Dictionary<Expression, Expression>();
            private ParameterExpression _parameter;
            private Expression _replacement;

            public SubstExpressionVisitor()
            {

            }
            public SubstExpressionVisitor(ParameterExpression parameter, Expression replacement)
            {
                _parameter = parameter;
                _replacement = replacement;
            }
            protected override Expression VisitParameter(ParameterExpression node)
            {
                return subst.TryGetValue(node, out var newValue) ? newValue : node;
            }


            public static Expression Replace(Expression expression, ParameterExpression parameter, Expression replacement)
            {
                return new SubstExpressionVisitor(parameter, replacement).Visit(expression);
            }

        }
    }
}