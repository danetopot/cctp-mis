﻿namespace CCTPMIS.Business.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using Context;

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly DbContext Context = null;

        private readonly DbSet<TEntity> _dbSet;

        public Repository(ApplicationDbContext context)
        {
            Context = context;
            var dbContext = context as DbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        // Editing :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void AddOrUpdate(TEntity entity)
        {
            _dbSet.AddOrUpdate(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public TEntity Find(int? id)
        {
            return _dbSet.Find(id);
        }

        public TEntity Find(string id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<TEntity> FindAll(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null,
            int? page = default(int?),
            int? pageSize = default(int?))
        {
            return GetQueryable(filter, orderBy, includes, includeProperties, page, pageSize);
        }

        public TEntity First(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null)
        {
            return GetQueryable(filter, orderBy, includes, includeProperties).First();
        }

        public IEnumerable<TEntity> GetAll(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null,
            int? page = default(int?),
            int? pageSize = default(int?))
        {
            return GetQueryable(filter, orderBy, includes, includeProperties, page, pageSize).ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null,
            int? page = default(int?),
            int? pageSize = default(int?))
        {
            return await GetQueryableAsync(filter, orderBy, includes, includeProperties, page, pageSize);
        }

        public IQueryable<TEntity> Queryable()
        {
            return _dbSet;
        }

        public void Remove(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public TEntity SingleOrDefault(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null)
        {
            return GetQueryable(filter, orderBy, includes, includeProperties).SingleOrDefault();
        }

        public void Update(TEntity entity)
        {
            var dbEntry = Context.Entry(entity);
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            var id = dbEntry.Property("Id").CurrentValue.ToString();
            //var originalValues = _dbSet.Find(id);
            string changes = "";
            // string oldValues = "{";
            foreach (var property in dbEntry.OriginalValues.PropertyNames)
            {
                var originalValue = dbEntry.GetDatabaseValues().GetValue<object>(property);
                var currentValue = dbEntry.Property(property).CurrentValue;

                if (!Equals(originalValue, currentValue))
                {
                    changes += property + ": " + originalValue + " -> " + currentValue + "<br>";
                }
            }
        }

        public IEnumerable<TEntity> Where(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null)
        {
            return GetQueryable(filter, orderBy, includes, includeProperties).ToList();
        }

        internal IQueryable<TEntity> GetQueryable(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null,
            int? page = null,
            int? pageSize = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (includes != null)
            {
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }

            //include for any relations
            if (includeProperties != null)
                foreach (var includeProperty in includeProperties.Split(
                    new char[] { ',' },
                    StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }

            return query;
        }

        internal async Task<IEnumerable<TEntity>> GetQueryableAsync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            string includeProperties = null,
            int? page = null,
            int? pageSize = null)
        {
            return await GetQueryable(filter, orderBy, includes, includeProperties, page, pageSize).ToListAsync();
        }
    }
}