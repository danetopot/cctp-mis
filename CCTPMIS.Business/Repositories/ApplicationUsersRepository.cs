﻿namespace CCTPMIS.Business.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Context;
    using Model;

    using Microsoft.AspNet.Identity;

    public class ApplicationUsersRepository
    {
        private ApplicationDbContext _context;

        public ApplicationUsersRepository(ApplicationDbContext context, UserManager<ApplicationUser, int> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        private UserManager<ApplicationUser, int> _userManager { get; set; }

        public async Task<bool> AddAsync(ApplicationUser user, string password)
        {
            var newUser = await addAsync(user, password);
            return newUser != null;
        }

        public async Task<bool> ChangePassword(int userId, string password)
        {
            await _userManager.RemovePasswordAsync(userId);
            var result = await _userManager.AddPasswordAsync(userId, password);
            return result.Succeeded;
        }

        public async Task<bool> DeleteAsync(string username)
        {
            var user = await GetByUserNameAsync(username);

            if (user != null)
                return (await _userManager.DeleteAsync(user)).Succeeded;
            return false;
        }

        public async Task<ApplicationUser> GetAsync(string username)
        {
            return Build(await GetByUserNameAsync(username));
        }

        public IEnumerable<ApplicationUser> Get(int pageSize, int pageCount)
        {
            var users = _context.Users

                // .Where(u => tenantManagerUsers.Contains(u) && !adminUsers.Contains(u))
                .OrderBy(p => p.UserName).Skip(pageSize * pageCount).Take(pageSize).ToList();

            return users.Select(u => Build(u)).ToList();
        }

        public async Task<ApplicationUser> GetByIdAsync(int id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return user;
        }

        public async Task<ApplicationUser> GetByUserNameAsync(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            return user;
        }

        public async Task<bool> UpdateAsync(ApplicationUser user)
        {
            var existingUser = await GetByIdAsync(user.Id);
            existingUser.FirstName = user.FirstName;
            existingUser.MiddleName = user.MiddleName;
            existingUser.Surname = user.Surname;
            existingUser.PhoneNumber = user.PhoneNumber;
            existingUser.Email = user.Email;
            existingUser.UserName = user.UserName;

            var updateResult = await _userManager.UpdateAsync(existingUser);

            return updateResult.Succeeded;
        }

        private async Task<ApplicationUser> addAsync(ApplicationUser user, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded)
                return user;
            return null;
        }

        private ApplicationUser Build(ApplicationUser userNew)
        {
            var user = new ApplicationUser
                           {
                               Id = userNew.Id,
                               FirstName = userNew.FirstName,
                               MiddleName = userNew.MiddleName,
                               Surname = userNew.Surname,
                               Email = userNew.Email,
                               UserName = userNew.UserName,
                               PhoneNumber = userNew.PhoneNumber
                           };
            return user;
        }
    }
}