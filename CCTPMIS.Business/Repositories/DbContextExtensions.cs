﻿namespace CCTPMIS.Business.Repositories
{
    using System.Data.Entity;
    using System.Data.SqlClient;

    public static class DbContextExtensions
    {
        public static MultiResultSetReader MultiResultSetSqlQuery(
            this DbContext context,
            string query,
            params SqlParameter[] parameters)
        {
            return new MultiResultSetReader(context, query, parameters);
        }
    }
}