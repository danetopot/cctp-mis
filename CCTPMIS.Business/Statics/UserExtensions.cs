﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CCTPMIS.Business.Statics
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using Context;
    using Microsoft.AspNet.Identity;

    
    public static class UserExtensions
    {
        private static readonly ApplicationDbContext Db = new ApplicationDbContext();

        public static bool UserGroupHasRight(this IPrincipal user, String role)
        {
            if (!user.Identity.IsAuthenticated) return false;
            var userId = int.Parse(user.Identity.GetUserId());
            if (HttpContext.Current.Session[$"USER_{userId}"] != null)
                return HttpContext.Current.Session[$"USER_{userId}"] is List<string> data && data.Contains(role);
            else
            {
                var data = Db.Database.SqlQuery<string>(
                    ";Exec GetUserGroupRights @UserId",
                    new SqlParameter("UserId", userId)).ToList();
                HttpContext.Current.Session[$"USER_{userId}"] = data;
                return data.Contains(role);
            }
        }

        public static UserSummary GetUserSummary(this IPrincipal user)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return new UserSummary();
            var userId = int.Parse(HttpContext.Current.User.Identity.GetUserId());
            if (HttpContext.Current.Session[$"USER_SUMMARY_{userId}"] != null)
                return HttpContext.Current.Session[$"USER_SUMMARY_{userId}"] as UserSummary;
            var data = Db.Database.SqlQuery<UserSummary>(
                ";Exec GetUserProfile @UserId",
                new SqlParameter("UserId", userId)).First();
            HttpContext.Current.Session[$"USER_SUMMARY_{userId}"] = data;
            return data;
        }


        public static ProgOfficerSummary GetProgOfficerSummary(this IPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
                return new ProgOfficerSummary();
            var userId = int.Parse(user.Identity.GetUserId());
            if (HttpContext.Current.Session[$"USER_PO_SUMMARY_{userId}"] != null)
            {
                return HttpContext.Current.Session[$"USER_PO_SUMMARY_{userId}"] as ProgOfficerSummary;
            }
            var data = Db.Database.SqlQuery<ProgOfficerSummary>(
                ";Exec GetProgOfficerSummary @UserId",
                new SqlParameter("UserId", userId)).FirstOrDefault();
            HttpContext.Current.Session[$"USER_PO_SUMMARY_{userId}"] = data;
            return data;
        }

    }
}