﻿namespace CCTPMIS.Business.Statics
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProgOfficerSummary
    {
        public int? CountyId { get; set; }
        public int? ConstituencyId { get; set; }
        public int? UserId { get; set; }
    }

    public class UserSummary
    {
        public string Avatar { get; set; }

        [Display(Name = "Avatar")]
        public string DisplayAvatar => string.IsNullOrEmpty(Avatar) ? "avatar.png" : Avatar;

        [Display(Name = "Display Names")]
        public string DisplayName => $"{FirstName} {MiddleName}";

        public string Email { get; set; }

        public string FirstName { get; set; }

        [Display(Name = "Full names")]
        public string FullName => $"{FirstName} {MiddleName} {Surname}";
        public string MiddleName { get; set; }
        [Display(Name = "Name")]
        public string Name => $"{FirstName}  {MiddleName}";
        public string PhoneNumber { get; set; }
        public List<string> Roles { get; set; }
        public string Surname { get; set; }
        public int UserGroupId { get; set; }
        public int? UserId { get; set; }
    }
}