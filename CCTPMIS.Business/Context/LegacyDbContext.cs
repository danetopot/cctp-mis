﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using CCTPMIS.Business.Legacy;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CCTPMIS.Business.Context
{
    public class LegacyDbContext : IdentityDbContext
    {
        public LegacyDbContext() : base($"LegacyConnection")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 18000;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
        }

        public DbSet<CaseGrievance> CaseGrievances { get; set; }
        public DbSet<CaseUpdate> CaseUpdates { get; set; }

        public DbSet<HhMembers> HhMembers { get; set; }
        public DbSet<HhRegistration> HhRegistration { get; set; }
        public DbSet<Payment> Payments { get; set; }
        //public DbSet<ExceptionsCTOVC> ExceptionsCTOVCS { get; set; }
        //public DbSet<ExceptionsDSD> ExceptionsDSDs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}