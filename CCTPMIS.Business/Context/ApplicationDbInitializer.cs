﻿namespace CCTPMIS.Business.Context
{
    using System.Data.Entity;

    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        public static void InitializeIdentityForEF(ApplicationDbContext db)
        {

        }

        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }
    }
}