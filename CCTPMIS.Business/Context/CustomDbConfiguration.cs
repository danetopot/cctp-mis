﻿namespace CCTPMIS.Business.Context
{
    using System.Data.Entity;
    using System.Data.Entity.SqlServer;

    public class CustomDbConfiguration : DbConfiguration
    {
        public CustomDbConfiguration()
        {
            SetMigrationSqlGenerator(SqlProviderServices.ProviderInvariantName, () => new CustomSqlGenerator());
        }
    }
}