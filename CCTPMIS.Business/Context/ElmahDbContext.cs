﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using CCTPMIS.Business.Model;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CCTPMIS.Business.Context
{
    public class ElmahDbContext : IdentityDbContext
    {
        public ElmahDbContext() : base($"ElmahConnection")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 18000;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
            
        }

        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<ELMAH_Error> ELMAH_Errors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}