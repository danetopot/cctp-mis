﻿namespace CCTPMIS.Business.Context
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model;

    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly

    // This is useful if you do not want to tear down the database each time you run the application.
    // public class ApplicationDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    // This example shows you how to create a new database if the Model changes

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserLogin,
        ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationDbContext() : base($"DefaultConnection")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 18000;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
            Configuration.EnsureTransactionsForFunctionsAndCommands = true;

        }

        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<BeneAccountMonthlyActivity> BeneAccountMonthlyActivities { get; set; }
        public DbSet<BeneAccountMonthlyActivityDetail> BeneAccountMonthlyActivityDetails { get; set; }
        public DbSet<BeneAccountPSPMonthlyActivity> BeneAccountPSPMonthlyActivities { get; set; }

        public DbSet<BeneficiaryAccount> BeneficiaryAccounts { get; set; }
        //
        public DbSet<BeneficiaryPaymentCard> BeneficiaryPaymentCards { get; set; }

        public DbSet<BulkTransferDetail> BulkTransferDetails { get; set; }
        public DbSet<BulkTransfer> BulkTransfers { get; set; }
        public DbSet<CaseCategory> CaseCategories { get; set; }

        public DbSet<ChangeNote> ChangeNotes { get; set; }
        public DbSet<Change> Changes { get; set; }
        public DbSet<ComplaintDocument> ComplaintDocuments { get; set; }
        public DbSet<ComplaintNote> ComplaintNotes { get; set; }
        public DbSet<Complaint> Complaints { get; set; }
        public DbSet<ComValListingAccept> ComValListingAccepts { get; set; }
        public DbSet<ComValListingPlanHH> ComValListingPlanHHs { get; set; }
        public DbSet<Constituency> Constituencies { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<CountyDistrict> CountyDistricts { get; set; }
        public DbSet<DbBackup> DbBackups { get; set; }
        public DbSet<District> Districts { get; set; }

        public DbSet<Division> Divisions { get; set; }
        public DbSet<EnumeratorDevice> EnumeratorDevices { get; set; }
        public DbSet<EnumeratorLocation> EnumeratorLocations { get; set; }
        public DbSet<Enumerator> Enumerators { get; set; }

        public DbSet<EFCDocument> EFCDocuments { get; set; }
        public DbSet<EFCNote> EFCNotes { get; set; }
        public DbSet<EFC> EFCs { get; set; }

        public DbSet<ExpansionPlanDetail> ExpansionPlanDetails { get; set; }
        public DbSet<ExpansionPlanMaster> ExpansionPlanMasters { get; set; }
        public DbSet<ExpansionPlan> ExpansionPlans { get; set; }
        public DbSet<FileCreation> FileCreations { get; set; }
        public DbSet<FileDownload> FileDownloads { get; set; }
        public DbSet<FundsRequestDetail> FundsRequestDetails { get; set; }
        public DbSet<FundsRequest> FundsRequests { get; set; }
        public DbSet<GeoMaster> GeoMasters { get; set; }
        public DbSet<GroupRight> GroupRights { get; set; }
        public DbSet<HouseholdEnrolmentPlan> HouseholdEnrolmentPlans { get; set; }
        public DbSet<HouseholdEnrolment> HouseholdEnrolments { get; set; }
        public DbSet<HouseholdMember> HouseholdMembers { get; set; }
        public DbSet<Household> Households { get; set; }
        public DbSet<HouseholdSubLocation> HouseholdSubLocations { get; set; }
        public DbSet<HouseholdVillageElder> HouseholdVillageElders { get; set; }
        public DbSet<HouseholdVillage> HouseholdVillages { get; set; }
        public DbSet<IprsCache> IprsCaches { get; set; }
        public DbSet<ListingAccept> ListingAccepts { get; set; }
        public DbSet<ListingPlanHH> ListingPlanHHs { get; set; }
        public DbSet<ListingPlanProgramme> ListingPlanProgrammes { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<ModuleRight> ModuleRights { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<MonthlyAccountActivity> MonthlyAccountActivities { get; set; }
        public DbSet<PasswordReset> PasswordResets { get; set; }
        public DbSet<PaymentAccountActivityMonth> PaymentAccountActivityMonths { get; set; }
        public DbSet<PaymentAdjustment> PaymentAdjustments { get; set; }
        public DbSet<PaymentCardBiometrics> PaymentCardBiometrics { get; set; }
        public DbSet<PaymentCycleDetail> PaymentCycleDetails { get; set; }
        public DbSet<PaymentCycle> PaymentCycles { get; set; }
        public DbSet<PaymentEnrolmentGroup> PaymentEnrolmentGroups { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentZone> PaymentZones { get; set; }
        public DbSet<Payroll> Payrolls { get; set; }
        public DbSet<PersonPension> PersonPensions { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonSocialAssistance> PersonSocialAssistances { get; set; }
        public DbSet<PrepayrollDuplicateId> PrepayrollDuplicateIds { get; set; }
        public DbSet<PrepayrollIneligible> PrepayrollIneligibles { get; set; }
        public DbSet<PrepayrollInvalidId> PrepayrollInvalidIds { get; set; }
        public DbSet<PrepayrollInvalidPaymentAccount> PrepayrollInvalidPaymentAccounts { get; set; }
        public DbSet<PrepayrollInvalidPaymentCard> PrepayrollInvalidPaymentCards { get; set; }
        public DbSet<Prepayroll> Prepayrolls { get; set; }
        public DbSet<PrepayrollSuspicious> PrepayrollSuspiciouses { get; set; }
        public DbSet<ProgrammeOfficer> ProgrammeOfficers { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<PspBranch> PspBranches { get; set; }
        public DbSet<Psp> Psps { get; set; }
        public DbSet<ReconciliationDetail> ReconciliationDetails { get; set; }
        public DbSet<Reconciliation> Reconciliations { get; set; }


        #region Registration
        public DbSet<HouseholdRegAccept> HouseholdRegAccepts { get; set; }
        public DbSet<HouseholdRegCharacteristic> HouseholdRegCharacteristics { get; set; }
        public DbSet<HouseholdRegMemberDisability> HouseholdRegMemberDisabilities { get; set; }
        public DbSet<HouseholdRegMember> HouseholdRegMembers { get; set; }
        public DbSet<HouseholdRegProgramme> HouseholdRegProgramme { get; set; }
        public DbSet<HouseholdReg> HouseholdRegs { get; set; }
        #endregion

        #region Recertification
        public DbSet<HouseholdRecAccept> HouseholdRecAccepts { get; set; }
        public DbSet<HouseholdRecCharacteristic> HouseholdRecCharacteristics { get; set; }
        public DbSet<HouseholdRecMemberDisability> HouseholdRecMemberDisabilities { get; set; }
        public DbSet<HouseholdRecMember> HouseholdRecMembers { get; set; }
        public DbSet<HouseholdRecProgramme> HouseholdRecProgramme { get; set; }
        public DbSet<HouseholdRec> HouseholdRecs { get; set; }
        #endregion

        public DbSet<RegistrationHhCv> RegistrationHhCvs { get; set; }
        public DbSet<RegistrationHh> RegistrationHhs { get; set; }
        public DbSet<SubLocation> SubLocations { get; set; }
        public DbSet<SystemCodeDetail> SystemCodeDetails { get; set; }

        public DbSet<SystemCode> SystemCodes { get; set; }


        public DbSet<TargetPlan> TargetPlans { get; set; }

        public DbSet<TargetPlanProgramme> TarPlanProgrammes { get; set; }

        public DbSet<UserGroupProfile> UserGroupProfiles { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<Village> Villages { get; set; }

        public DbSet<WardLocation> WardLocations { get; set; }
        public DbSet<Ward> Wards { get; set; }


        public DbSet<ListingException> ListingExceptions { get; set; }
        public DbSet<ComValListingException> ComValListingExceptions { get; set; }


        #region M & E
        public DbSet<MonitoringReport> MonitoringReports { get; set; }
        public DbSet<PeriodicPaymentIndicator> PeriodicPaymentIndicators { get; set; }
        public DbSet<PeriodicConstituencyBeneIndicator> PeriodicConstituencyBeneIndicators { get; set; }
        public DbSet<MonitoringPeriod> MonitoringPeriods { get; set; }
        public DbSet<Indicator> Indicators { get; set; }

        public DbSet<MonitoringReportCategory> MonitoringReportCategories { get; set; }

        #endregion M & E
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        // Override OnModelsCreating:
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Ignore<ListingExceptionChild>();
            // RENAME TABLES
            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<ApplicationRole>().ToTable("Role");
            modelBuilder.Entity<ApplicationUserRole>().ToTable("UserRole");
            modelBuilder.Entity<ApplicationUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<ApplicationUserLogin>().ToTable("UserLogin");

            // ADD INDICES
            modelBuilder.Entity<Prepayroll>().HasIndex(p => new { p.PaymentCycleId, p.HhId });
            modelBuilder.Entity<PaymentAdjustment>()
                .HasIndex(p => new { p.PaymentCycleId, p.HhId, p.AdjustmentTypeId });
            modelBuilder.Entity<PrepayrollInvalidId>().HasIndex(p => new { p.PaymentCycleId, p.HhId, p.PersonId });
            modelBuilder.Entity<PrepayrollIneligible>().HasIndex(p => new { p.PaymentCycleId, p.HhId, });
            modelBuilder.Entity<PrepayrollDuplicateId>().HasIndex(p => new { p.PaymentCycleId, p.HhId, p.PersonId });
            modelBuilder.Entity<SystemCodeDetail>().HasIndex(p => new { p.SystemCodeId, p.Code });
            modelBuilder.Entity<ModuleRight>().HasIndex(mr => new { mr.ModuleId, mr.RightId }).IsUnique();
            modelBuilder.Entity<ApplicationUser>().HasIndex(mr => new { mr.UserName }).IsUnique();

            // RENAME COLUMN NAMES
            modelBuilder.Entity<ApplicationUser>().Property(c => c.PhoneNumber).HasColumnName("MobileNo");
            modelBuilder.Entity<ApplicationUser>().Property(c => c.PhoneNumberConfirmed)
                .HasColumnName("MobileNoConfirmed");

            // IGNORE IDENTITY COLUMNS
            modelBuilder.Entity<ApplicationUser>().Ignore(c => c.TwoFactorEnabled);

            // ADJUST OR REMOVE CONVENTIONS
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<HouseholdSubLocation>().HasIndex(mr => new { mr.HhId, mr.GeoMasterId, mr.SubLocationId }).IsUnique();



        }


    }
}