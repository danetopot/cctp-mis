﻿namespace CCTPMIS.Business.Context
{
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;

    public class EmailIService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }
}