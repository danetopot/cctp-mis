﻿namespace CCTPMIS.Business.Context
{
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.SqlServer;
    using System.Linq;

    public class CustomSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(AddForeignKeyOperation addForeignKeyOperation)
        {
            addForeignKeyOperation.Name = GetForeignKeyName(
                addForeignKeyOperation.PrincipalTable,
                addForeignKeyOperation.DependentTable,
                addForeignKeyOperation.DependentColumns.ToArray());
            base.Generate(addForeignKeyOperation);
        }

        protected override void Generate(DropForeignKeyOperation dropForeignKeyOperation)
        {
            dropForeignKeyOperation.Name = GetForeignKeyName(
                dropForeignKeyOperation.PrincipalTable,
                dropForeignKeyOperation.DependentTable,
                dropForeignKeyOperation.DependentColumns.ToArray());
            base.Generate(dropForeignKeyOperation);
        }

        protected override void Generate(CreateTableOperation createTableOperation)
        {
            createTableOperation.PrimaryKey.Name = GetPrimaryKeyName(createTableOperation.Name);
            base.Generate(createTableOperation);
        }

        protected override void Generate(AddPrimaryKeyOperation addPrimaryKeyOperation)
        {
            addPrimaryKeyOperation.Name = GetPrimaryKeyName(addPrimaryKeyOperation.Table);
            base.Generate(addPrimaryKeyOperation);
        }

        protected override void Generate(DropPrimaryKeyOperation dropPrimaryKeyOperation)
        {
            dropPrimaryKeyOperation.Name = GetPrimaryKeyName(dropPrimaryKeyOperation.Table);
            base.Generate(dropPrimaryKeyOperation);
        }

        private static string GetForeignKeyName(
            string primaryKeyTable,
            string foreignKeyTable,
            params string[] foreignTableFields)
        {
            var fieldList = foreignTableFields.Aggregate(string.Empty, (current, field) => current + ("_" + field));
            return "FK_" + foreignKeyTable.Replace("dbo.", string.Empty) + "_"
                   + primaryKeyTable.Replace("dbo.", string.Empty) + fieldList;
        }

        private static string GetPrimaryKeyName(string primaryKeyTable)
        {
            return "PK_" + primaryKeyTable.Replace("dbo.", string.Empty);
        }
    }
}