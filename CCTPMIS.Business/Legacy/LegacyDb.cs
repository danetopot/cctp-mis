﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTPMIS.Business.Legacy
{


    public class ActiveCTOVCBeneficiaries
    {
        public string ProgrammeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Sex { get; set; }

        public string DateOfBirth { get; set; }

        public int? Age { get; set; }

        public string IDNo { get; set; }

        public int NoOfHouseholdMembers { get; set; }

        public int NoOfMaleOVC { get; set; }

        public int NoOfFemaleOVC { get; set; }

        public string CountyName { get; set; }

        public string DistrictName { get; set; }

        public string DivisionName { get; set; }

        public string LocationName { get; set; }

        public string SubLocationName { get; set; }

        public string ConstituencyName { get; set; }

        public string ResolvedIDNo { get; set; }

        public int HasInvalid { get; set; }

        public bool? IPRS_IDNoExists { get; set; }

        public bool? IPRS_FirstNameExists { get; set; }

        public bool? IPRS_MiddleNameExists { get; set; }

        public bool? IPRS_SurnameExists { get; set; }

        public bool? IPRS_DoBMatches { get; set; }

        public bool? IPRS_DoBYearMatches { get; set; }

        public bool? IPRS_SexMatches { get; set; }

        public string SubLocationCode { get; set; }

        public int? TargetingId { get; set; }

        public bool? Exited { get; set; }

        public int? TargetingDetailId { get; set; }
    }

    public class ActiveOPCTBeneficiaries
    {
        public int ProgrammeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string IDNumber { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CGFirstName { get; set; }

        public string CGMiddleName { get; set; }

        public string CGSurname { get; set; }

        public string CGIDNumber { get; set; }

        public string CGSex { get; set; }

        public DateTime? CGDateOfBirth { get; set; }

        public string County_Name { get; set; }

        public string Constituency_Name { get; set; }

        public string District_Name { get; set; }

        public string Division_Name { get; set; }

        public string Location_Name { get; set; }

        public string SubLocation_Name { get; set; }

        public string ResolvedIDNo { get; set; }

        public int HasInvalid1 { get; set; }

        public string ResolvedCGIDNo { get; set; }

        public int HasInvalid2 { get; set; }

        public bool? IPRS_IDNoExists { get; set; }

        public bool? IPRS_FirstNameExists { get; set; }

        public bool? IPRS_MiddleNameExists { get; set; }

        public bool? IPRS_SurnameExists { get; set; }

        public bool? IPRS_DoBMatches { get; set; }

        public bool? IPRS_DoBYearMatches { get; set; }

        public bool? IPRS_SexMatches { get; set; }

        public bool? IPRS_CGIDNoExists { get; set; }

        public bool? IPRS_CGFirstNameExists { get; set; }

        public bool? IPRS_CGMiddleNameExists { get; set; }

        public bool? IPRS_CGSurnameExists { get; set; }

        public bool? IPRS_CGDoBMatches { get; set; }

        public bool? IPRS_CGDoBYearMatches { get; set; }

        public bool? IPRS_CGSexMatches { get; set; }

        public string SubLocationCode { get; set; }

        public long? MemberCode { get; set; }

        public bool? Exited { get; set; }

        public long? CGMemberCode { get; set; }
    }

    public class ActiveOPCTBeneficiariesUPDATED
    {
        public int ProgrammeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string IDNumber { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CGFirstName { get; set; }

        public string CGMiddleName { get; set; }

        public string CGSurname { get; set; }

        public string CGIDNumber { get; set; }

        public string CGSex { get; set; }

        public DateTime? CGDateOfBirth { get; set; }

        public string County_Name { get; set; }

        public string Constituency_Name { get; set; }

        public string District_Name { get; set; }

        public string Division_Name { get; set; }

        public string Location_Name { get; set; }

        public string SubLocation_Name { get; set; }

        public string ResolvedIDNo { get; set; }

        public int HasInvalid1 { get; set; }

        public string ResolvedCGIDNo { get; set; }

        public int HasInvalid2 { get; set; }

        public bool? IPRS_IDNoExists { get; set; }

        public bool? IPRS_FirstNameExists { get; set; }

        public bool? IPRS_MiddleNameExists { get; set; }

        public bool? IPRS_SurnameExists { get; set; }

        public bool? IPRS_DoBMatches { get; set; }

        public bool? IPRS_DoBYearMatches { get; set; }

        public bool? IPRS_SexMatches { get; set; }

        public bool? IPRS_CGIDNoExists { get; set; }

        public bool? IPRS_CGFirstNameExists { get; set; }

        public bool? IPRS_CGMiddleNameExists { get; set; }

        public bool? IPRS_CGSurnameExists { get; set; }

        public bool? IPRS_CGDoBMatches { get; set; }

        public bool? IPRS_CGDoBYearMatches { get; set; }

        public bool? IPRS_CGSexMatches { get; set; }

        public string SubLocationCode { get; set; }

        public long? MemberCode { get; set; }

        public bool? Exited { get; set; }

        public long? CGMemberCode { get; set; }
    }

    public class ActivePwSDBeneficiaries
    {
        public int ProgrammeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string IDNumber { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CGFirstName { get; set; }

        public string CGMiddleName { get; set; }

        public string CGSurname { get; set; }

        public string CGIDNumber { get; set; }

        public string CGSex { get; set; }

        public DateTime? CGDateOfBirth { get; set; }

        public string County_Name { get; set; }

        public string Constituency_Name { get; set; }

        public string District_Name { get; set; }

        public string Division_Name { get; set; }

        public string Location_Name { get; set; }

        public string SubLocation_Name { get; set; }

        public string ResolvedIDNo { get; set; }

        public int HasInvalid1 { get; set; }

        public string ResolvedCGIDNo { get; set; }

        public int HasInvalid2 { get; set; }

        public bool? IPRS_IDNoExists { get; set; }

        public bool? IPRS_FirstNameExists { get; set; }

        public bool? IPRS_MiddleNameExists { get; set; }

        public bool? IPRS_SurnameExists { get; set; }

        public bool? IPRS_DoBMatches { get; set; }

        public bool? IPRS_DoBYearMatches { get; set; }

        public bool? IPRS_SexMatches { get; set; }

        public bool? IPRS_CGIDNoExists { get; set; }

        public bool? IPRS_CGFirstNameExists { get; set; }

        public bool? IPRS_CGMiddleNameExists { get; set; }

        public bool? IPRS_CGSurnameExists { get; set; }

        public bool? IPRS_CGDoBMatches { get; set; }

        public bool? IPRS_CGDoBYearMatches { get; set; }

        public bool? IPRS_CGSexMatches { get; set; }

        public string SubLocationCode { get; set; }

        public long? MemberCode { get; set; }

        public bool? Exited { get; set; }

        public long? CGMemberCode { get; set; }
    }

    public class ActivePwSDBeneficiariesUPDATED
    {
        public int ProgrammeNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string IDNumber { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CGFirstName { get; set; }

        public string CGMiddleName { get; set; }

        public string CGSurname { get; set; }

        public string CGIDNumber { get; set; }

        public string CGSex { get; set; }

        public DateTime? CGDateOfBirth { get; set; }

        public string County_Name { get; set; }

        public string Constituency_Name { get; set; }

        public string District_Name { get; set; }

        public string Division_Name { get; set; }

        public string Location_Name { get; set; }

        public string SubLocation_Name { get; set; }

        public string ResolvedIDNo { get; set; }

        public int HasInvalid1 { get; set; }

        public string ResolvedCGIDNo { get; set; }

        public int HasInvalid2 { get; set; }

        public bool? IPRS_IDNoExists { get; set; }

        public bool? IPRS_FirstNameExists { get; set; }

        public bool? IPRS_MiddleNameExists { get; set; }

        public bool? IPRS_SurnameExists { get; set; }

        public bool? IPRS_DoBMatches { get; set; }

        public bool? IPRS_DoBYearMatches { get; set; }

        public bool? IPRS_SexMatches { get; set; }

        public bool? IPRS_CGIDNoExists { get; set; }

        public bool? IPRS_CGFirstNameExists { get; set; }

        public bool? IPRS_CGMiddleNameExists { get; set; }

        public bool? IPRS_CGSurnameExists { get; set; }

        public bool? IPRS_CGDoBMatches { get; set; }

        public bool? IPRS_CGDoBYearMatches { get; set; }

        public bool? IPRS_CGSexMatches { get; set; }

        public string SubLocationCode { get; set; }

        public long? MemberCode { get; set; }

        public bool? Exited { get; set; }

        public long? CGMemberCode { get; set; }
    }

    public class CaseGrievance
    {
        public int Id { get; set; }

        public int CaseId { get; set; }

        public int SourceProgrammeId { get; set; }

        public string SourceProgramme { get; set; }

        public string SerialNo { get; set; }

        public int? ProgrammeId { get; set; }

        public string Programme { get; set; }

        public int GrievanceTypeId { get; set; }

        public string GrievanceTypeName { get; set; }

        public string GrievanceDescription { get; set; }

        public string CaseHistory { get; set; }

        public string Constituency { get; set; }

        public string SubLocation { get; set; }

        public string Village { get; set; }

        public string Community { get; set; }

        public int? HhId { get; set; }

        public int? HhRegistrationId { get; set; }

        public string IsBeneficiary { get; set; }

        public string ComplainantType { get; set; }

        public string ComplainantName { get; set; }

        public string ComplainantSex { get; set; }

        public string ComplainantAge { get; set; }

        public string ComplainantTel { get; set; }

        public DateTime ReceiptOn { get; set; }

        public string ReceivedBy { get; set; }

        public string ReceiptLevel { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? VerifiedOn { get; set; }

        public string VerifiedBy { get; set; }

        public string ActionNotes { get; set; }

        public string CaseStatus { get; set; }

        public DateTime? FeedbackOn { get; set; }

        public string Resolution { get; set; }

        public DateTime? ResolvedOn { get; set; }

        public string ResolvedBy { get; set; }

        public DateTime? ClosedOn { get; set; }

        public string ClosedBy { get; set; }
    }

    public class CaseUpdate
    {
        public int Id { get; set; }

        public int CaseId { get; set; }

        public int SourceProgrammeId { get; set; }

        public string SourceProgramme { get; set; }

        public string UpdateType { get; set; }

        public string UpdateName { get; set; }

        public string UpdateReason { get; set; }

        public DateTime UpdateDate { get; set; }

        public int? HhId { get; set; }

        public int? HhRegistrationId { get; set; }

        public int? OldCGMemberId { get; set; }

        public int? NewCGMemberId { get; set; }

        public int? OldCGHhMemberId { get; set; }

        public int? NewCGHhMemberId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string IDNumber { get; set; }

        public string MarritalStatus { get; set; }

        public string EducationLevel { get; set; }

        public string AttendanceSchool { get; set; }

        public string EducationChangeReason { get; set; }

        public string IsChronicallyIll { get; set; }

        public string IsDisabled { get; set; }

        public string AttendanceHeathCentre { get; set; }

        public string HealthChangeReason { get; set; }

        public string Relationship { get; set; }

        public string OldSubLocation { get; set; }

        public string NewSubLocation { get; set; }

        public string PSPBranch { get; set; }

        public string PSP { get; set; }

        public string ReplacePaymentCard { get; set; }

        public string PSPChangeReason { get; set; }

        public string AlternativeRecipientName { get; set; }

        public string AlternativeRecipientIDNumber { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ApprovedOn { get; set; }

        public string ApprovedBy { get; set; }

        public string ActionNotes { get; set; }
    }

    public class HhMembers
    {
        public int Id { get; set; }

        public long MemberId { get; set; }

        public int HhId { get; set; }

        public int HhRegistrationId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Sex { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string BirthCertNo { get; set; }

        public string NationalIdNo { get; set; }

        public string MarritalStatus { get; set; }

        public string EducationLevel { get; set; }

        public string AttendanceSchool { get; set; }

        public string IllnessType { get; set; }

        public string DisabilityType { get; set; }

        public string NCPWDNo { get; set; }

        public string AttendanceHeathCentre { get; set; }

        public string Relationship { get; set; }

        public string OccupationType { get; set; }

        public bool IsExited { get; set; }
    }

    public class HhRegistration
    {
        public int Id { get; set; }
        [Display(Name ="Households")]
        public int HhId { get; set; }
        [Display(Name = "Programme ID")]

        public int ProgrammeId { get; set; }

        public string Programme { get; set; }
        [Display(Name = "Programme Number")]

        public string ProgrammeNumber { get; set; }

        public string County { get; set; }
        [Display(Name = "Sub County")]

        public string Constituency { get; set; }

        public string District { get; set; }

        public string Division { get; set; }

        public string Location { get; set; }
        [Display(Name = "Sub Location")]

        public string SubLocation { get; set; }

        public string Village { get; set; }
        [Display(Name = "Nearest Physical Landmark")]

        public string NearestLandmark { get; set; }
        [Display(Name = "The Household owns Real Estate")]

        public string HasRealEstate { get; set; }
        [Display(Name = "Household living in own House")]

        public string IsLivingInOwnHouse { get; set; }

        [Display(Name = "Rent Amount")]

        public decimal? RentAmount { get; set; }
        [Display(Name = "Zebu Cattle")]


        public int ZebuCattle { get; set; }

        [Display(Name = "Hybrid Number")]

        public int HybridCattle { get; set; }

        public int Sheep { get; set; }

        public int Goats { get; set; }

        public int Pigs { get; set; }

        public int Camels { get; set; }

        public int? Poultry { get; set; }
        [Display(Name = "Number of Meals per Week")]

        public int? NoMealPerWeek { get; set; }

        [Display(Name = "Meals Per day")]

        public int? MealsPerDay { get; set; }

        [Display(Name = "Reasons for Skipping  Meal")]

        public string ReasonForNoMeal { get; set; }
        [Display(Name = "Number of meal per Month")]

        public int? NoMealPerMonth { get; set; }

        [Display(Name = "Wall Material")]

        public string WallMaterial { get; set; }
        [Display(Name = "Floor Material")]
        public string FloorMaterial { get; set; }
        [Display(Name = "Roof Material")]
        public string RoofMaterial { get; set; }
        [Display(Name = "Waste Disposal Amount")]
        public string ToiletType { get; set; }
        [Display(Name = "Water Source ")]
        public string WaterSource { get; set; }
        [Display(Name = "Lighting Source")]
        public string LightingSource { get; set; }
        [Display(Name = "Fuel Source")]
        public string FuelSource { get; set; }
        [Display(Name = "Land Size")]
        public int? LandSize { get; set; }
        [Display(Name = "Other Programmes Benefiting")]
        public string OtherProg { get; set; }
        [Display(Name = "Other Programme Benefit types")]
        public string OtherProgBenefitType { get; set; }
        [Display(Name = "Other Programme Benefit Frequency")]
        public string OtherProgBenefitFrequency { get; set; }
        [Display(Name = "Other Programme Benefit Amount")]
        public decimal? OtherProgBenefitAmount { get; set; }
        [Display(Name = "Other Programme Other benefits")]
        public string OtherProgOtherBenefits { get; set; }
        [Display(Name = "Enrolment Status")]
        public string EnrolmentStatus { get; set; }
        [Display(Name = "Enrolment Date")]
        public DateTime? EnrolmentDate { get; set; }
        [Display(Name = "Enrolment Reason")]
        public string EnrolmentReason { get; set; }
        [Display(Name = "Approval Date")]
        public DateTime? ApprovalDate { get; set; }

        [Display(Name = "Branch")]
        public string PSPBranch { get; set; }
        [Display(Name ="Bank")]
        public string PSP { get; set; }

        public string Inactive { get; set; }

        public string CapturedBy { get; set; }

        public string ApprovedBy { get; set; }

        public string EnrolledBy { get; set; }

        public double? PovertyScore { get; set; }


        public ICollection<HhMembers> HhMembers { get; set; }
        public ICollection<CaseUpdate> CaseUpdates { get; set; }
        public ICollection<CaseGrievance> CaseGrievances { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<ExceptionsDSD> ExceptionsDSDS { get; set; }
        public ICollection<ExceptionsCTOVC> ExceptionsCTOVCS { get; set; }

    }

    public class IPRS_CTOVC_CG
    {
        public string Programme { get; set; }

        public double? ProgrammeNumber { get; set; }

        public double? IDNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }

    public class IPRS_OPCT_Bene
    {
        public string Programme { get; set; }

        public double? ProgrammeNumber { get; set; }

        public double? IDNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }

    public class IPRS_OPCT_CG
    {
        public string Programme { get; set; }

        public double? ProgrammeNumber { get; set; }

        public double? IDNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }

    public class IPRS_PwSD_Bene
    {
        public string Programme { get; set; }

        public double? ProgrammeNumber { get; set; }

        public double? IDNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }

    public class IPRS_PwSD_CG
    {
        public string Programme { get; set; }

        public double? ProgrammeNumber { get; set; }

        public double? IDNumber { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }

    public class IPRS_Rerun1
    {
        public string Programme { get; set; }

        public string First_Name { get; set; }

        public string Other_Name { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public double? ID_Number { get; set; }

        public string Date_of_Birth { get; set; }
    }

    public class IPRS_Rerun2
    {
        public string Programme { get; set; }

        public string First_Name { get; set; }

        public string Other_Name { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public double? ID_Number { get; set; }

        public string Date_of_Birth { get; set; }
    }

    public class Payment
    {
        public int Id { get; set; }

        public int PaymentId { get; set; }

        public int HhId { get; set; }

        public int HhRegistrationId { get; set; }

        public int CycleId { get; set; }

        public string CycleName { get; set; }

        public DateTime CycleStartDate { get; set; }

        public DateTime CycleEndDate { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string Approved { get; set; }

        public string Notes { get; set; }

        public string PSPBranch { get; set; }

        public string PSP { get; set; }

        public string PrimaryRecipientName { get; set; }

        public string PrimaryRecipientNationalIdNo { get; set; }

        public string SecondaryRecipientName { get; set; }

        public string SecondaryRecipientNationalIdNo { get; set; }

        public decimal Amount { get; set; }

        public decimal PaidAmount { get; set; }

        public DateTime? PaidDate { get; set; }

        public string PaidTrxNo { get; set; }

        public string GeneratedBy { get; set; }
    }

    public class ExceptionsCTOVC
    {
        public int Id { get; set; }
        public int HhRegistrationId { get; set; }
        public double? ProgrammeNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string Sex { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string IDNo { get; set; }
        public string NoOfHouseholdMembers { get; set; }
        public string NoOfMaleOVC { get; set; }
        public string NoOfFemaleOVC { get; set; }
        public string CountyName { get; set; }
        public string DistrictName { get; set; }
        public string DivisionName { get; set; }
        public string LocationName { get; set; }
        public string SubLocationName { get; set; }
        public string ConstituencyName { get; set; }
        public string EXCEPTION { get; set; }
        public string DUPLICATE { get; set; }
        public string SUSPICIOUS_AMOUNT { get; set; }
        public string AMOUNT { get; set; }
        public string OVCFirstName { get; set; }
        public string OVCMiddleName { get; set; }
        public string OVCSurname { get; set; }
        public string OVCDateOfBirth { get; set; }
        public string NoOfOccurence { get; set; }
        public string EnrollmentDate { get; set; }
        public string SelectionDate { get; set; }
        public string TargetingDate { get; set; }
        public string InterviewDate { get; set; }
        public string MemberFirstName { get; set; }
        public string MemberMiddleName { get; set; }
        public string MemberSurname { get; set; }
        public string MemberDateOfBirth { get; set; }
        public string MemberSex { get; set; }
   


    }
    public class ExceptionsDSD
    {
        public int Id { get; set; }
        public int HhRegistrationId { get; set; }
        public double? ProgrammeNumber { get; set; }
        public string Register_No { get; set; }
        public string Programme { get; set; }
        public string BeneficiaryNames { get; set; }
        public string BeneficiaryIDNo { get; set; }
        public string CaregiverNames { get; set; }
        public string CaregiverIDNo { get; set; }
        public string Enrolled { get; set; }
        public string Inactive { get; set; }
        public string Sublocation_Name { get; set; }
        public string Location_Name { get; set; }
        public string Division_Name { get; set; }
        public string District_Name { get; set; }
        public string County_Name { get; set; }
        public string Constituency_Name { get; set; }
        public string MEMBERS { get; set; }
        public string EXCEPTION { get; set; }
        public string Enrollment_Date { get; set; }
        public string Entry_Date { get; set; }
        public string Interview_Date { get; set; }
        public string UserMapped_Constituency { get; set; }
    }

}
