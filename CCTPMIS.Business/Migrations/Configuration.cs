namespace CCTPMIS.Business.Migrations
{
    using System.Data.Entity.Migrations;

    using Context;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            // This method will be called after migrating to the latest version.

            // You can use the DbSet<T>.AddOrUpdate() helper extension method
            // to avoid creating duplicate seed data.

            // var sql =    File.ReadAllText(   HostingEnvironment.MapPath("~/Content/DataInitializer/Regional/xx.json"));

            // context.Database.ExecuteSqlCommand(sql);
        }



    }
}