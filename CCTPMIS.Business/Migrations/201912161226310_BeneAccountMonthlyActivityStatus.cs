namespace CCTPMIS.Business.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BeneAccountMonthlyActivityStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BeneAccountPSPMonthlyActivity",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    BeneAccountMonthlyActivityId = c.Int(nullable: false),
                    PspId = c.Byte(nullable: false),
                    StatusId = c.Int(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    CreatedOn = c.DateTime(nullable: false),
                    ModifiedBy = c.Int(),
                    ModifiedOn = c.DateTime(),
                    ClosedBy = c.Int(),
                    ClosedOn = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BeneAccountMonthlyActivity", t => t.BeneAccountMonthlyActivityId)
                .ForeignKey("dbo.User", t => t.ClosedBy)
                .ForeignKey("dbo.User", t => t.CreatedBy)
                .ForeignKey("dbo.User", t => t.ModifiedBy)
                .ForeignKey("dbo.Psp", t => t.PspId)
                .ForeignKey("dbo.SystemCodeDetail", t => t.StatusId)
                .Index(t => t.BeneAccountMonthlyActivityId)
                .Index(t => t.PspId)
                .Index(t => t.StatusId)
                .Index(t => t.ClosedBy)
                .Index(t => t.CreatedBy)
                .Index(t => t.ModifiedBy);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "StatusId", "dbo.SystemCodeDetail");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "Psp_Id", "dbo.Psp");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "ModifiedBy", "dbo.User");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "CreatedBy", "dbo.User");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "ClosedBy", "dbo.User");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "BeneAccountMonthlyActivityId", "dbo.BeneAccountMonthlyActivity");
            DropForeignKey("dbo.BeneAccountPSPMonthlyActivity", "ApvBy", "dbo.User");


            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "Psp_Id" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "ClosedBy" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "ApvBy" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "ModifiedBy" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "CreatedBy" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "StatusId" });
            DropIndex("dbo.BeneAccountPSPMonthlyActivity", new[] { "BeneAccountMonthlyActivityId" });


            DropTable("dbo.BeneAccountPSPMonthlyActivity");
        }
    }
}
