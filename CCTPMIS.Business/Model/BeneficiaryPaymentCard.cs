﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class BeneficiaryPaymentCard
    {
        [Display(Name = "Approved By ")]
        public int? ApvBy { get; set; }

        [ForeignKey("ApvBy")]
        public ApplicationUser ApvByUser { get; set; }
        [Display(Name = "Approved On ")]
        public DateTime? ApvOn { get; set; }
        [Display(Name = "Account ID ")]
        public int BeneAccountId { get; set; }

        [ForeignKey("BeneAccountId")]
        public BeneficiaryAccount BeneficiaryAccount { get; set; }
        [Display(Name = "Created By ")]
        public int CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }
        [Display(Name = "Created On ")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "ID ")]
        public int Id { get; set; }
        
        [StringLength(30), Display(Name = "Mobile number #1")]
        public string MobileNo1 { get; set; }

        [StringLength(30), Display(Name = "Mobile number #2")]
        public string MobileNo2 { get; set; }
        [Display(Name = "Modified By ")]
        public int? ModifiedBy { get; set; }

        [ForeignKey("ModifiedBy")]
        public ApplicationUser ModifiedByUser { get; set; }
        [Display(Name = "Modified On ")]
        public DateTime? ModifiedOn { get; set; }
        [Display(Name = "Masked Card No. ")]
        [StringLength(50)]
        public string PaymentCardNo { get; set; }
        [Display(Name = "Recipient Date Of Birth ")]
        public DateTime PriReciDoB { get; set; }
        
        [StringLength(50), Required, Display(Name = "Primary Recipient First Name")]
        public string PriReciFirstName { get; set; }
        [Display(Name = "Primary Recipient ID ")]
        public int PriReciId { get; set; }

        [StringLength(50), Display(Name = "Primary Recipient Middle Name")]
        public string PriReciMiddleName { get; set; }

        [StringLength(30), Required, Display(Name = "Primary Recipient National Id Number")]
        public string PriReciNationalIdNo { get; set; }

        [ForeignKey("PriReciId")]
        public Person PriReciPerson { get; set; }

        [ForeignKey("PriReciSexId")]
        public SystemCodeDetail PriReciSex { get; set; }
        [Display(Name = "Primary Recipient Sex ID")]
        public int PriReciSexId { get; set; }

        [StringLength(50), Required, Display(Name = "Primary Recipient Surname")]
        public string PriReciSurname { get; set; }
        [Display(Name = "Primary Recipient Date Of Birth")]
        public DateTime? SecReciDoB { get; set; }

        [StringLength(50), Display(Name = "Secondary Recipient First Name")]
        public string SecReciFirstName { get; set; }
        [Display(Name = "Secondary Recipient ID")]
        public int? SecReciId { get; set; }

        [StringLength(50), Display(Name = "Secondary Recipient Middle Name")]
        public string SecReciMiddleName { get; set; }

        [StringLength(30), Display(Name = "Secondary Recipient National Id Number")]
        public string SecReciNationalIdNo { get; set; }

        [ForeignKey("SecReciId")]
        public Person SecReciPerson { get; set; }

        [ForeignKey("SecReciSexId")]
        public SystemCodeDetail SecReciSex { get; set; }
        [Display(Name = "Secondary Recipient Sex")]
        public int? SecReciSexId { get; set; }

        [StringLength(50), Display(Name = "Secondary Recipient Surname")]
        public string SecReciSurname { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Required]
        [Display(Name = "Payment Card Status")]
        public int StatusId { get; set; }
    }
}