﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class ProgrammeOfficer:CreateApproveFields
    {
        public int Id { get; set; }
        [Display(Name = "Officer")]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User  { get; set; }
        [Display(Name="County")]
        public int? CountyId { get; set; }
        public County County { get; set; }
        [Display(Name = "Sub County")]
        public int? ConstituencyId { get; set; }
        public Constituency Constituency { get; set; }
        [Display(Name = "Deactivated By")]
        public int? DeactivatedBy { get; set; }
        [ForeignKey("DeactivatedBy")]
        public ApplicationUser DeactivatedByUser { get; set; }
        public DateTime? DeactivatedOn { get; set; }
        [Display(Name = "Deactivated Approved By ")]
        public int? DeactivatedApvBy { get; set; }
        [ForeignKey("DeactivatedApvBy")]
        public ApplicationUser DeactivatedApvByUser { get; set; }
        [Display(Name = "Deactivated Approved On")]
        public DateTime? DeactivatedApvOn { get; set; }
        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }

    }
}