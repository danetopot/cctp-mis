﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{  
    public class BeneficiaryAccountActivity
    {
        [Key, Column(Order = 1)]
        [Display(Name = "Account ")]
        public int BeneAccountId { get; set; }

        [ForeignKey("BeneAccountId")]
        public BeneficiaryAccount BeneficiaryAccount { get; set; }
        [Display(Name = "Has Unique Transaction ")]
        public bool HasUniqueTrx { get; set; }

        [ForeignKey("MonthId")]
        public SystemCodeDetail Month { get; set; }
        [Display(Name = "Month ")]
        [Key, Column(Order = 2)]
        public int MonthId { get; set; }
        [Display(Name = "Year ")]
        [Key, Column(Order = 3)]
        public short Year { get; set; }
    }
}