﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRegCharacteristic
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Household Members")]
        public int HouseholdMembers { get; set; }
        [Display(Name = "(2.01) How many habitable ROOMS does this dwelling unit contain?")]
        public int HabitableRooms { get; set; }
        [Display(Name = "(2.02)  If owner occupied,")]
        public int IsOwnedId { get; set; }
        [Display(Name = "(2.02)  TENURE status of the dwelling unit and/or surrounding terrain/land")]
        public int TenureStatusId { get; set; }
        [Display(Name = "(2.03)  Dominant CONSTRUCTION MATERIAL of the main Dwelling unit ROOF")]
        public int RoofMaterialId { get; set; }
        [Display(Name = "(2.04)  Dominant CONSTRUCTION MATERIAL of the main Dwelling unit WALL ")]
        public int WallMaterialId { get; set; }
        [Display(Name = "(2.05)  Dominant CONSTRUCTION MATERIAL of the main Dwelling unit FLOOR")]
        public int FloorMaterialId { get; set; }
        [Display(Name = "(2.06)	 The dwelling unit is at RISK of: ")]
        public int DwellingUnitRiskId { get; set; }
        [Display(Name = "(2.07)Main source of WATER:	")]
        public int WaterSourceId { get; set; }
        [Display(Name = "(2.08) Main mode of HUMAN WASTE DISPOSAL:")]
        public int ToiletTypeId { get; set; }
        [Display(Name = "(2.09) Main type of COOKING FUEL:")]
        public int CookingFuelSourceId { get; set; }
        [Display(Name = "(2.10)  Main type of LIGHTING FUEL:")]
        public int LightingSourceId { get; set; }
        [Display(Name = "(2.11) Does the household OWN any of the following items? Television")]
        public int IsTelevisionId { get; set; }
        [Display(Name = "(2.12) Does the household OWN any of the following items? Motor cycle")]
        public int IsMotorcycleId { get; set; }
        [Display(Name = "(2.13) Does the household OWN any of the following items? TukTuk")]
        public int IsTukTukId { get; set; }
        [Display(Name = "(2.14)Does the household OWN any of the following items? Refrigerator ")]
        public int IsRefrigeratorId { get; set; }
        [Display(Name = "(2.15) Does the household OWN any of the following items? Car")]
        public int IsCarId { get; set; }
        [Display(Name = "(2.16) Does the household OWN any of the following items? Mobile Phone")]
        public int IsMobilePhoneId { get; set; }
        [Display(Name = "(2.17) Does the household OWN any of the following items? Bicycle")]
        public int IsBicycleId { get; set; }
        [Display(Name = "(2.18) How many   Exotic Cattle are currently owned")]
        public int ExoticCattle { get; set; }
        [Display(Name = "(2.19)  How many  Indigenous Cattle are currently owned")]
        public int IndigenousCattle { get; set; }
        [Display(Name = "(2.20)  How many  Sheep are currently owned")]
        public int Sheep { get; set; }
        [Display(Name = "(2.21) How many  Goats are currently owned")]
        public int Goats { get; set; }
        [Display(Name = "(2.22) How many  Camels are currently owned")]
        public int Camels { get; set; }
        [Display(Name = "(2.23) How many  Donkeys are currently owned")]
        public int Donkeys { get; set; }
        [Display(Name = "(2.24) How many  Pigs are currently owned")]
        public int Pigs { get; set; }
        [Display(Name = "(2.25) How many  Chicken are currently owned")]
        public int Chicken { get; set; }
        [Display(Name = "(2.27)How many DEATHS occurred in this household in the last 12 months?")]
        public int? Deaths { get; set; }
        [Display(Name = "(2.26)How many LIVE BIRTHS occurred in this household in the last 12 months?")]
        public int? LiveBirths { get; set; }
        [Display(Name = "(2.28) Currently, the CONDITIONS of your household are:")]
        public int HouseholdConditionId { get; set; }
        [Display(Name = "(2.29) In the past 7 days, did anyone in this household cut the size of the meals or skip meals because of the lack of enough money?")]
        public int IsSkippedMealId { get; set; }
        [Display(Name = "(2.30) Is anyone in this household receiving benefits from any of the following National Safety Net Programmes?")]
        public int NsnpProgrammesId { get; set; }
        [Display(Name = "(2.31) 	Is anyone in this household receiving benefits from any other Social Assistance Programme or any other external support?")]
        public int OtherProgrammesId { get; set; }
        [Display(Name = "Other Programme Names")]
        public string OtherProgrammeNames { get; set; }
        [Display(Name = "(2.33)	 What type of BENEFIT do you receive?")]
        public int? BenefitTypeId { get; set; }
        [Display(Name = "(2.34)		How MUCH was the benefit in the last receipt?")]
        public decimal? LastReceiptAmount { get; set; }
        [Display(Name = "(2.40)	Specify IN-KIND of benefit:	")]
        public string InKindBenefit { get; set; }
        
        public SystemCodeDetail IsOwned { get; set; }
        public SystemCodeDetail TenureStatus { get; set; }
        public SystemCodeDetail RoofMaterial { get; set; }
        public SystemCodeDetail WallMaterial { get; set; }
        public SystemCodeDetail FloorMaterial { get; set; }
        public SystemCodeDetail DwellingUnitRisk { get; set; }
        public SystemCodeDetail WaterSource { get; set; }
        public SystemCodeDetail ToiletType { get; set; }
        public SystemCodeDetail CookingFuelSource { get; set; }
        public SystemCodeDetail LightingSource { get; set; }
        public SystemCodeDetail IsTelevision { get; set; }
        public SystemCodeDetail IsMotorcycle { get; set; }
        public SystemCodeDetail IsTukTuk { get; set; }
        public SystemCodeDetail IsRefrigerator { get; set; }
        public SystemCodeDetail IsCar { get; set; }
        public SystemCodeDetail IsMobilePhone { get; set; }
        public SystemCodeDetail IsBicycle { get; set; }
        public SystemCodeDetail HouseholdCondition { get; set; }
        public SystemCodeDetail IsSkippedMeal { get; set; }
        public SystemCodeDetail NsnpProgrammes { get; set; }
        public SystemCodeDetail OtherProgrammes { get; set; }
        public SystemCodeDetail BenefitType { get; set; }
    }
}