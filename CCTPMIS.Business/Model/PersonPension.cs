﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PersonPension
    {
        [StringLength(128)]
        public string Description { get; set; }
        [Display(Name = "House hold ")]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        [Key, Column(Order = 2)]
        public Household Household { get; set; }
        [Display(Name = "is Government on Pension ")]
        public bool IsGovPension { get; set; } = false;
        [Display(Name = "Pension Amount ")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        public decimal PensionAmount { get; set; } = 0;

        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        [Display(Name = "Person ")]
        [Key, Column(Order = 1)]
        public int PersonId { get; set; }
    }
}