﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class BeneAccountPSPMonthlyActivity : CreateModifyFields
    {
        [Key]
        public int Id { get; set; }

        [Column(Order = 1)]
        public int BeneAccountMonthlyActivityId { get; set; }

        [ForeignKey("BeneAccountMonthlyActivityId")]
        public BeneAccountMonthlyActivity BeneAccountMonthlyActivity { get; set; }

        public Psp Psp { get; set; }

        [Display(Name = "PSP")]
        public byte PspId { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Closed By")]
        public int? ClosedBy { get; set; }

        [Display(Name = "Closed On")]
        public DateTime? ClosedOn { get; set; }

        [ForeignKey("ClosedBy")]
        public ApplicationUser ClosedByUser { get; set; }
    }
}
