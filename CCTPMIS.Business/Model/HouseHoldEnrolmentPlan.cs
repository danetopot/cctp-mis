﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdEnrolmentPlan : CreateModifyFields
    {
        [Display(Name = "Approved By")]
        public int? ApvBy { get; set; }

        [ForeignKey("ApvBy")]
        public ApplicationUser ApvByUser { get; set; }

        [Display(Name = "Approved On")]
        public DateTime? ApvOn { get; set; }

        [Display(Name = "Beneficiaries HouseHolds")]
        public int BeneHhs { get; set; }

        [Display(Name = "Enrolled Households")]
        public int EnrolledHH => BeneHhs;

        [ForeignKey("EnrolmentGroupId")]
        public SystemCodeDetail EnrolmentGroup { get; set; }

        [Display(Name = "Enrolment Group ")]
        public int EnrolmentGroupId { get; set; }

        [Display(Name = "Enrolment Numbers")]
        public int EnrolmentNumbers { get; set; }

        [Display(Name = "Expansion Plan Equal Share")]
        public int ExpPlanEqualShare { get; set; }

        [Display(Name = "Expansion Plan Poverty Prioritized")]
        public int ExpPlanPovertyPrioritized { get; set; }

        [ForeignKey("FileCreationId")]
        public FileCreation FileCreation { get; set; }
        [Display(Name = "Enrolment File")]
        public int? FileCreationId { get; set; }

        public ICollection<HouseholdEnrolment> HouseholdEnrolments { get; set; }

        public int? Id { get; set; }

        [Display(Name = "Maximum Number to Enrol")]
        public int MaxToEnrol => ExpPlanEqualShare + ExpPlanPovertyPrioritized;

        [ForeignKey("ProgrammeId")]
        public Programme Programme { get; set; }

        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        [Display(Name = "Read For Enrolment")]
        public int ReadyForEnrolment => RegGroupHhs;

        [ForeignKey("RegGroupId")]
        public SystemCodeDetail RegGroup { get; set; }

        [Display(Name = "HouseHolds to Enroll")]
        public int RegGroupHhs { get; set; }

        [Display(Name = "Registration Group")]
        public int RegGroupId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }
    }
}