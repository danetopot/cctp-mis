﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    public class RegAccept :RegAcceptCommon
    {
        public ICollection<RegistrationHh> RegistrationHhs { get; set; }
    }
}