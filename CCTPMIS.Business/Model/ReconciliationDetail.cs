﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class ReconciliationDetail : CreateModifyFields
    {

        [Key, Column(Order = 1)]
        public int ReconciliationId { get; set; }
        public Reconciliation Reconciliation { get; set; }

        [Key, Column(Order = 2)]
        public byte PspId { get; set; }
        public Psp Psp { get; set; }

        public string OpeningBalanceDiffNarration { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:#,##0}")]
        public decimal OpeningBalance { get; set; }
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal OpeningBalanceSystem { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal CrFundsRequests { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal CrFundsRequestsStatement { get; set; }

        public string CrFundsRequestsDiffNarration { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal CrClawBacks { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal CrClawBacksStatement { get; set; }

        public string CrClawBacksDiffNarration { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal DrPayments { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal DrPaymentsStatement { get; set; }
        public string DrPaymentsDiffNarration { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal DrCommissions { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal DrCommissionsStatement { get; set; }
        public string DrCommissionsDiffNarration { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal Balance { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0,00#}")]
        public decimal BalanceStatement { get; set; }
        public string BalanceDiffNarration { get; set; }
        public int BankStatementFileId { get; set; }
        public FileCreation BankStatementFile { get; set; }

        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }

    }

    public class RegistrationHH
    {
        public int Id { get; set; }
        public string UniqueId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public bool IsFeatured { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int RegistrationGroupId { get; set; }
        public int FinancialYearId { get; set; }
        public int EnumeratorId { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HHdNationalIdNo { get; set; }
        public int HHdSexId { get; set; }
        public DateTime HHdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public DateTime CgDoB { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public DateTime DwellingStartDate { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }

    }


}