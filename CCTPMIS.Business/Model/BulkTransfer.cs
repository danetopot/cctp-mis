﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class BulkTransfer:CreateApproveFields
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Description")]
        [Required][DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Bulk Transfer File")]
        public int? FileId { get; set; }
        [ForeignKey("FileId")]
        public FileCreation FileCreation { get; set; }

        public ICollection<BulkTransferDetail> BulkTransferDetails { get; set; }
    }
}