﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ApplicationGroupUser
    {
        [Key, Column(Order = 2)]
        public int ApplicationUserId { get; set; }
        [Key, Column(Order = 1)]
        public int ApplicationGroupId { get; set; }
        public ApplicationGroupRole ApplicationGroupRole { get; set; }

        public ApplicationUser User { get; set; }
    }
}