﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class Location : CreateModifyFields
    {
        [Display(Name = "Location Code")]
        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        [JsonIgnore]
        public Division Division { get; set; }

        [Display(Name = "Division")]
        public int DivisionId { get; set; }

        [Display(Name = "Location")]
        public int Id { get; set; }

        [Display(Name = "Location Name")]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public PaymentZone PaymentZone { get; set; }

        [Display(Name = "Payment Zone")]
        public short? PaymentZoneId { get; set; }

        [JsonIgnore]
        public ICollection<SubLocation> SubLocations { get; set; }

        [JsonIgnore]
        public ICollection<WardLocation> WardLocations { get; set; }


        // public string Constituency => $"{Division.CountyDistrict.County.Constituencies}";
    }
}