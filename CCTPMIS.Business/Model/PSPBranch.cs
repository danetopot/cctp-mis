﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PspBranch : CreateModifyFields
    {
        [StringLength(20)]
        [Index(IsClustered = false, IsUnique = true, Order = 2)]
        [Display(Name = "Branch Code")]
        public string Code { get; set; }

        [Display(Name = "Branch ID")]
        public short Id { get; set; }

        [Display(Name = "Active Status")]
        public bool IsActive { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        [StringLength(100)]
        [Required]
        [Display(Name = "Branch Name")]
        public string Name { get; set; }

        public Psp Psp { get; set; }

        [Display(Name = "PSP")]
        [Index(IsClustered = false, IsUnique = true, Order = 1)]
        public byte PspId { get; set; }

        public SubLocation SubLocation { get; set; }

        // [Display(Name = "Geo Cordinate")]
        // public string GeoCordinate => $"[{Longitude},{Latitude}] ";
        [Display(Name = "Sub Location")]
        public int? SubLocationId { get; set; }
    }
}