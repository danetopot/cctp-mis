﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class BulkTransferDetail
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Change")]
        public int ChangeId { get; set; }
        [Display(Name = "Bulk Transfer")]
        public int BulkTransferId { get; set; }

        public BulkTransfer BulkTransfer { get; set; }
        public Change Change { get; set; }
    }
}