﻿
using System.Xml.Serialization;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public class CaseReportHistory : CreateModifyFields
    {
        [ForeignKey("Id")]
        [XmlIgnore]
        public virtual CaseReport CaseReport { get; set; }

        [Display(Name = "Case Report")]
        public int Id { get; set; }

        [Display(Name = "Relationship")]
        public int? RelationshipId { get; set; }

        [ForeignKey("RelationshipId")]
        [XmlIgnore]
        public SystemCodeDetail Relationship { get; set; }

        [Display(Name = "Person")]
        public int? PersonId { get; set; }

        [ForeignKey("PersonId")]
        [XmlIgnore]
        public Person Person { get; set; }

        [Display(Name = "First Name ")]
        [StringLength(50),]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string MiddleName { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50),]
        public string Surname { get; set; }

        [Display(Name = "Sex")]
        public int? SexId { get; set; }

        [ForeignKey("SexId")]
        [XmlIgnore]
        public SystemCodeDetail Sex { get; set; }

        [StringLength(50)]
        [Display(Name = "Birth Cert.")]
        public string BirthCertNo { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No.")]
        public string NationalIdNo { get; set; }

        [Display(Name = "Mobile #1 ")]
        [StringLength(20)]
        public string MobileNo1 { get; set; }

        [Display(Name = "Mobile #2 ")]
        [StringLength(20)]
        public string MobileNo2 { get; set; }

        [ForeignKey("SubLocationId")]
        [XmlIgnore]
        public SubLocation SubLocation { get; set; }

        [Display(Name = "SubLocation")]
        public int? SubLocationId { get; set; }
        
        [Display(Name = "Relationship")]
        public int? UpdatedRelationshipId { get; set; }

        [ForeignKey("UpdatedRelationshipId")][XmlIgnore]
        public SystemCodeDetail UpdatedRelationship { get; set; }

        [Display(Name = "Person")]
        public int? UpdatedPersonId { get; set; }

        [ForeignKey("UpdatedPersonId")]
        [XmlIgnore]
        public Person UpdatedPerson { get; set; }

        [Display(Name = "First Name ")]
        [StringLength(50)]
        public string UpdatedFirstName { get; set; }

        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string UpdatedMiddleName { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50)]
        public string UpdatedSurname { get; set; }

        [Display(Name = "Sex")]
        public int? UpdatedSexId { get; set; }

        [ForeignKey("SexId")]
        [XmlIgnore]
        public SystemCodeDetail UpdatedSex { get; set; }

        [StringLength(50)]
        [Display(Name = "Birth Cert.")]
        public string UpdatedBirthCertNo { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No. ")]
        public string UpdatedNationalIdNo { get; set; }

        [Display(Name = "Mobile #1")]
        [StringLength(20)]
        public string UpdatedMobileNo1 { get; set; }

        [Display(Name = "Mobile #2")]
        [StringLength(20)]
        public string UpdatedMobileNo2 { get; set; }

        [ForeignKey("SubLocationId")]
        [XmlIgnore]
        public SubLocation UpdatedSubLocation { get; set; }

        [Display(Name = "SubLocation")]
        public int? UpdatedSubLocationId { get; set; }
    }

}