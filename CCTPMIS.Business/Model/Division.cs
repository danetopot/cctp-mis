﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class Division : CreateModifyFields
    {
        [Display(Name = "Division Code")]
        [Required]
        [StringLength(20)]
        public string Code { get; set; }

        public CountyDistrict CountyDistrict { get; set; }

        [Display(Name = "County District")]
        public int CountyDistrictId { get; set; }

        [Display(Name = "Division")]
        public int Id { get; set; }

        [JsonIgnore]
        public ICollection<Location> Locations { get; set; }

        [Display(Name = "Division Name")]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
    }
}