﻿
namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Newtonsoft.Json;

    public class CaseIssue : CreateModifyFields
    {
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("CaseTypeId")]
        public SystemCodeDetail CaseType { get; set; }

        [Display(Name = "Case Type")]
        public int CaseTypeId { get; set; }

        [ForeignKey("CaseCategoryId")]
        public SystemCodeDetail CaseCategory { get; set; }

        [Display(Name = "Case Category")]
        public int CaseCategoryId { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Case Issue")]
        [RegularExpression("^[A-Za-z0-9- ]*$", ErrorMessage = "Invalid!")]
        public string Code { get; set; }

        [StringLength(100)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("UpdateEffectId")]
        public SystemCodeDetail UpdateEffect { get; set; }

        [Display(Name = "Change Effect")]
        public int? UpdateEffectId { get; set; }

        [Display(Name = "Maximum Limit")]
        public byte MaxLimit { get; set; }

        [Display(Name = "Resolution Days")]
        public byte SLADays { get; set; }

        [Display(Name = "Escalations")]
        public int Escalations { get { return (this.CaseFlows == null) ? 0 : this.CaseFlows.Count; } }

        [JsonIgnore]
        public ICollection<CaseFlow> CaseFlows { get; set; }

        

    }

}