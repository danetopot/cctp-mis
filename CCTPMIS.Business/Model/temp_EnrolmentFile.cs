﻿using System;

namespace CCTPMIS.Business.Model
{
    public class temp_EnrolmentFile
    {
        public int? EnrolmentNo { get; set; }
        public string ProgrammeNo { get; set; }
        public string BeneFirstName { get; set; }
        public string BeneMiddleName { get; set; }
        public string BeneSurname { get; set; }
        public string BeneIDNo { get; set; }
        public string BeneSex { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string CGFirstName { get; set; }
        public string CGMiddleName { get; set; }
        public string CGSurname { get; set; }
        public string CGIDNo { get; set; }
        public string CGSex { get; set; }
        public DateTime? CGDoB { get; set; }
        public string MobileNo1 { get; set; }
        public string MobileNo2 { get; set; }
        public string County { get; set; }
        public string Constituency { get; set; }
        public string District { get; set; }
        public string Division { get; set; }
        public string Location { get; set; }
        public string SubLocation { get; set; }
    }
}