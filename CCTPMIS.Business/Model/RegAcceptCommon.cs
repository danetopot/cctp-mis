﻿using System;

namespace CCTPMIS.Business.Model
{
    public class RegAcceptCommon
    {
        public int Id { get; set; }
        public int AccById { get; set; }
        public DateTime AccDate { get; set; }
        public int? AccApvById { get; set; }
        public DateTime? AccApvDate { get; set; }
        public int ReceivedHHs { get; set; }
        public string BatchName { get; set; }
        public int ConstituencyId { get; set; }
        public int TargetPlanId { get; set; }
        public TargetPlan TargetPlan { get; set; }
        public Constituency Constituency { get; set; }
        public ApplicationUser AccBy { get; set; }
        public ApplicationUser AccApvBy { get; set; }

    }
}