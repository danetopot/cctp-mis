﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdSubLocation
    {
        public GeoMaster GeoMaster { get; set; }

        [Key, Column(Order = 2)]
        [Display(Name = "GeoMaster")]
        public int GeoMasterId { get; set; }
        [Display(Name = "Household")]
        [Key, Column(Order = 1)]
        public int HhId { get; set; }

        
        public SubLocation SubLocation { get; set; }
        [Display(Name = "Sub Location")]
        [Key, Column(Order = 3)]
        public int SubLocationId { get; set; }
    }
}