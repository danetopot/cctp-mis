﻿namespace CCTPMIS.Business.Model
{
    public enum WorkProcessType
    {
        Export
    }
    public class PaymentCardBiometrics
    {      
        [Required]
        public string PriReciRT { get; set; }
        [Required]
        public string PriReciRI { get; set; }
        [Required]
        public string PriReciRMF { get; set; }
        [Required]
        public string PriReciRRF { get; set; }
        [Required]
        public string PriReciRP { get; set; }
        [Required]
        public string PriReciLT { get; set; }
        [Required]
        public string PriReciLI { get; set; }
        [Required]
        public string PriReciLMF { get; set; }
        [Required]
        public string PriReciLRF { get; set; }
        [Required]
        public string PriReciLP { get; set; }

        public string SecReciRT { get; set; }
        public string SecReciRI { get; set; }
        public string SecReciRMF { get; set; }
        public string SecReciRRF { get; set; }
        public string SecReciRP { get; set; }
        public string SecReciLT { get; set; }
        public string SecReciLI { get; set; }
        public string SecReciLMF { get; set; }
        public string SecReciLRF { get; set; }
        public string SecReciLP { get; set; }
    }

    public class Payment
    {
        public int PaymentCycleId { get; set; }
        public int HhId { get; set; }
        public decimal TransferAmount { get; set; }
        public string TrxNo { get; set; }
        public DateTime? TrxDate { get; set; }
    }
 public class BeneficiaryAccountActivity
    {
        public int BeneAccountId { get; set; }
        public int MonthId { get; set; }
        public smallint BeneAccountId { get; set; }
         public bool UniqueTrx { get; set; }


    }
}