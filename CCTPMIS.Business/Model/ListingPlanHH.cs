﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class ListingPlanHH
    {
        [Display(Name = "Targeting ID")]
        public int Id { get; set; }
        [Display(Name = "Reference No.")]
        public string UniqueId { get; set; }
        [Display(Name = "Start Time")]
        public DateTime? StartTime { get; set; }
        [Display(Name = "End Time")]
        public DateTime? EndTime { get; set; }
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }
        [Display(Name = "Registration Date")]
        public DateTime RegDate { get; set; }
        [Display(Name = "Sub Location ")]
        public int SubLocationId { get; set; }
        public SubLocation SubLocation { get; set; }
        [Display(Name = "Location ")]
        public int LocationId { get; set; }
        public  Location  Location { get; set; }

        public int Years { get; set; }
        public int Months { get; set; }
        [Display(Name = "Target Plan")]
        public int TargetPlanId { get; set; }

        public TargetPlan TargetPlan { get; set; }
        [Display(Name = "Enumerator")]
        public int EnumeratorId { get; set; }
        public Enumerator Enumerator { get; set; }
        [Display(Name = "First Name")]
        public string BeneFirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string BeneMiddleName { get; set; }
        [Display(Name = "Surname")]
        public string BeneSurname { get; set; }
        [Display(Name = "National ID No. ")]
        public string BeneNationalIdNo { get; set; }
        [Display(Name = "Phone Number ")]
        public string BenePhoneNumber { get; set; }
        [Display(Name = "Full Name")]
        public string BeneFullName => $"{BeneFirstName}  {BeneMiddleName} {BeneSurname}";

        public SystemCodeDetail BeneSex { get; set; }
        [Display(Name = "Sex ")]
        public int? BeneSexId { get; set; }
        [Display(Name = "Date Of Birth ")]
        public DateTime? BeneDoB { get; set; }
        [Display(Name = "First name")]
        public string CgFirstName { get; set; }
            [Display(Name = "Middle Name ")]
        public string CgMiddleName { get; set; }
        [Display(Name = " Surname")]
        public string CgSurname { get; set; }
        [Display(Name = "Full Name ")]
        public string CgFullName => $"{CgFirstName}  {CgMiddleName} {CgSurname}";
        [Display(Name = "National ID No. ")]
        public string CgNationalIdNo { get; set; }
        [Display(Name = " Phone Number")]
        public string CgPhoneNumber { get; set; }
        [Display(Name = "Sex ")]
        public int CgSexId { get; set; }
        public SystemCodeDetail CgSex { get; set; }
        [Display(Name = "Date Of Birth ")]
        public DateTime CgDoB { get; set; }
        [Display(Name = "Total Household Members ")]
        public int HouseholdMembers { get; set; }
        [Display(Name = "Status ")]
        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }
        [Display(Name = "Village")]
        public string Village { get; set; }
        [Display(Name = "Physical Address ")]
        public string PhysicalAddress { get; set; }
        [Display(Name = "Nearest Religious Building ")]
        public string NearestReligiousBuilding { get; set; }
        [Display(Name = " Nearest School ")]
        public string NearestSchool { get; set; }
         
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
        [Display(Name = "Sync Enumerator ")]
        public int SyncEnumeratorId { get; set; }
        [Display(Name = "Sync Date ")]
        public DateTime SyncDate { get; set; }
        [Display(Name = "Enumerator Devicwe ")]
        public int EnumeratorDeviceId { get; set; }
        public EnumeratorDevice EnumeratorDevice { get; set; }
        public Enumerator SyncEnumerator { get; set; }

        public Programme Programme { get; set; }
        [Display(Name = "Batch ID ")]
        public int? ListingAcceptId { get; set; }
        public ListingAccept ListingAccept { get; set; }
        [Display(Name = "Geo Coordinates ")]
        public string GeoPosition => $"[ {Longitude}, {Latitude}]";
        [Display(Name = "Form No: ")]
        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        [Display(Name = "Reference No ")]
        public string ReferenceNo => $" Targeting No.: #{Id}".ToUpper();
        [Display(Name = "Location ")]
        public string LocationName => $"{Location?.Name}";
        [Display(Name = "Programme Name ")]
        public string ProgrammeName => $"{Programme?.Name}";
        [Display(Name = "Sub Location ")]
        public string SubLocationName => $"{SubLocation?.Name}";
        [Display(Name="Duration of Residence")]
        public string DurationDisplay => $"{Years} Years, {Months} Months";
        [Display(Name = "Sex Name ")]
        public string CgSexName => $"{CgSex?.Description}";
        [Display(Name = " Sex Name")]
        public string BeneSexName => $"{BeneSex?.Description}";
        [Display(Name = "App Version")]
        public string AppVersion { get; set; }
        [Display(Name = "App Build ")]
        public string AppBuild { get; set; }
        [Display(Name = "Reject Reason ")]
        public string RejectReason { get; set; }
        [Display(Name = " Reject By")]
        public int? RejectById { get; set; }
        [Display(Name = "Reject Date ")]
        public DateTime? RejectDate { get; set; }


        [Display(Name = "County")]
        public string County => $"{Location.Division.CountyDistrict.County.Name}";
        [Display(Name = "SubCounty")]
        public string SubCounty => $"{SubLocation.Constituency.Name}";
    }
}