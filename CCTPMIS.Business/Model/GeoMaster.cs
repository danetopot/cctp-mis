﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class GeoMaster : CreateModifyFields
    {
        public ICollection<County> Counties { get; set; }

        [Display(Name = "Description", Description = "Short description about the Geo Master"), Required]
        [StringLength(100)]
        public string Description { get; set; }

        public ICollection<District> Districts { get; set; }

        [Display(Name = "Geo Master", Description = "Geo Master ID")]
        public int Id { get; set; }

        [Display(Name = "Default GeoMaster", Description = "Flag to set the Default GeoMaster in the System")]
        public bool IsDefault { get; set; }

        [Display(Name = "Geo Master", Description = "The name of the Geo Master")]
        [StringLength(20), Required]
        [Index(IsUnique = true, IsClustered = false)]
        public string Name { get; set; }
    }
}