﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace CCTPMIS.Business.Model
{
    public class TargetingCommon
    {
        public int Id { get; set; }
        [Required, StringLength(36)]
        public string UniqueId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public byte ProgrammeId { get; set; }
        [JsonIgnore]
        public Programme Programme { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int SubLocationId { get; set; }
        public int LocationId { get; set; }
        [JsonIgnore]
        public Location Location { get; set; }
        [JsonIgnore]
        public SubLocation SubLocation { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int RegPlanId { get; set; }
        [JsonIgnore]
        //public RegPlan RegPlan { get; set; }
        public int EnumeratorId { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        [JsonIgnore]
        public SystemCodeDetail Status { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }

        [Display(Name = "Batch")]
        public int? TarAcceptId { get; set; }

        [Display(Name = "Batch")]
        public int? TarAcceptCvId { get; set; }
        [JsonIgnore]
        public Enumerator Enumerator { get; set; }

    }
}