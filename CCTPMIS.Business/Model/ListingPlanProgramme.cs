﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class ListingPlanProgramme
    {
        [Key, Column(Order = 1)]
        public int TargetPlanId { get; set; }         
        [Key, Column(Order = 2)]
        public byte ProgrammeId { get; set; }

        public Programme Programme { get; set; }

        [ForeignKey("TargetPlanId")]
        public TargetPlan TargetPlan { get; set; }
    }
}