﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRecProgramme
    {
        [Key, Column(Order = 1)]
        [Display(Name = "Household Registration ")]
        public int HouseholdRecId { get; set; }
        [Key, Column(Order = 2)]
        [Display(Name = "Programme")]
        public int ProgrammeId { get; set; }

        public SystemCodeDetail Programme { get; set; }
        public HouseholdRec HouseholdRec { get; set; }
    }
}