﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FileDownload
    {
        [Display(Name = "File Downloaded By")]
        public int DownloadedBy { get; set; }

        [ForeignKey("DownloadedBy")]
        public ApplicationUser DownloadedByUser { get; set; }

        [Display(Name = "File Downloaded On")]
        public DateTime DownloadedOn { get; set; }

        [StringLength(64)]
        [Display(Name = "File Check Sum")]
        public string FileChecksum { get; set; }

        [ForeignKey("FileCreationId")]
        public FileCreation FileCreation { get; set; }

        [Display(Name = "File")]
        public int FileCreationId { get; set; }

        public int Id { get; set; }
    }
}