﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNet.Identity.EntityFramework;

    //public class Login
    //{
    //    public int Id { get; set; }

    //    [Required]
    //    public int UserId { get; set; }

    //    [Required]
    //    public string SessionId { get; set; }

    //    [Required]
    //    public DateTime Date { get; set; }
    //}
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>
    {
        [StringLength(50)]
        [Required]
        public string Description { get; set; }

        [StringLength(30)]
        [Required]
        public new string Name { get; set; }
    }
}