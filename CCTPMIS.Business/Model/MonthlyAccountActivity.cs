﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class MonthlyAccountActivity
    {

        [Key, Column(Order = 1)]
        public int MonthId { get; set; }
        [ForeignKey("MonthId")]
        public SystemCodeDetail Month { get; set; }
        [Key, Column(Order = 2)]
        public int YearId { get; set; }
        [ForeignKey("YearId")]
        public SystemCodeDetail Year { get; set; }

        [Key, Column(Order = 3)]
        public int BeneAccountId { get; set; }

        public BeneficiaryAccount BeneficiaryAccount { get; set; }

        public bool HadUniqueWdl { get; set; }

        public string UniqueWdlTrxNo { get; set; }

        public DateTime? UniqueWdlDate { get; set; }

        public bool HadBeneBiosVerified { get; set; }

        public bool IsDormant { get; set; }

        public DateTime? DormancyDate { get; set; }

        public bool IsDueForClawback { get; set; }

        public decimal? ClawbackAmount { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }

        public DateTime CreatedOn { get; set; }
       
        public int? ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public ApplicationUser ModifiedByUser { get; set; }


        public DateTime? ModifiedOn { get; set; }

        public int? ApvBy { get; set; }
        [ForeignKey("ApvBy")]
        public ApplicationUser ApvByUser { get; set; }
        public DateTime? ApvOn { get; set; }

    }
}