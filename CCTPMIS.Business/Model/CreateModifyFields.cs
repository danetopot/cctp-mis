﻿using System.Xml.Serialization;

namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    public class CreateModifyFields
    {
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        [ForeignKey("CreatedBy")]
        [XmlIgnore]
        public ApplicationUser CreatedByUser { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Modified By")]
        public int? ModifiedBy { get; set; }

        [JsonIgnore]
        [ForeignKey("ModifiedBy")]
        [XmlIgnore]
        public ApplicationUser ModifiedByUser { get; set; }

        [Display(Name = "Modified On")]
        public DateTime? ModifiedOn { get; set; }
    }
}