﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    public class WardLocation : CreateModifyFields
    {
        [JsonIgnore]
        public Location Location { get; set; }

        [Display(Name = "Location")]
        [Key, Column(Order = 2)]
        public int LocationId { get; set; }

        [JsonIgnore]
        public Ward Ward { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Ward")]
        public int WardId { get; set; }
    }
}