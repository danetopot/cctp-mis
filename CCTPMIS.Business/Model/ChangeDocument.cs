﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class ChangeDocument
    {
        [Display(Name = "Change Document")]
        public int Id { get; set; }
        [Display(Name = "Change")]
        public int ChangeId { get; set; }
        [Display(Name = "Created By")]
        public int CreatedById { get; set; }
        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "File Path")]
        public string FilePath { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public SystemCodeDetail Category { get; set; }

        [ForeignKey("CreatedById")]
        public ApplicationUser CreatedBy { get; set; }
        //public Change Change { get; set; }

    }
}