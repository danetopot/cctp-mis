﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FundsRequestDetail
    {
        [Display(Name = "Commission Amount  ")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal CommissionAmount { get; set; }
        [Display(Name = "Entitlement Amount")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal EntitlementAmount { get; set; } = 0;

        public FundsRequest FundsRequest { get; set; }

        [Display(Name = "Funds Request ")]
        [Key, Column(Order = 1)]
        public int FundsRequestId { get; set; }

        [Display(Name = "Other Amount  ")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal OtherAmount { get; set; }

        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES 0,00#.00}")]
        public decimal TotalAmount => (OtherAmount + EntitlementAmount + CommissionAmount);
        [Display(Name = "Total Beneficiaries")]
        public int PayrollHhs { get; set; }

        public Programme Programme { get; set; }

        [Display(Name = "Programme")]
        [Key, Column(Order = 2)]
        public byte ProgrammeId { get; set; }

        public Psp Psp { get; set; }
        [Display(Name = "PSP")]
        [Key, Column(Order =3)]
        public byte PspId { get; set; }

     
    }
}