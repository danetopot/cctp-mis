﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class PaymentAccountActivityMonth
    {
        [Key, Column(Order =1)]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }
        public PaymentCycle PaymentCycle { get; set; }
        [Key, Column(Order = 2)]
        [Display(Name = "Month")]
        public int MonthId { get; set; }
        
        [ForeignKey("MonthId")]
        public SystemCodeDetail Month { get; set; }
        [Key, Column(Order = 3)]
        [Display(Name = "Year")]
        public int Year { get; set; }

        
    }
}