﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class CountyDistrict : CreateModifyFields
    {
        [JsonIgnore]
        public County County { get; set; }

        [Display(Name = "County Id")]
        public int CountyId { get; set; }

        [JsonIgnore]
        public District District { get; set; }

        [Display(Name = "District Id")]
        public int DistrictId { get; set; }

        public ICollection<Division> Divisions { get; set; }

        public int Id { get; set; }

        public string Name => $"{County?.Name}-{District?.Name}";
    }
}