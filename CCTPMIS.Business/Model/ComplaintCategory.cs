﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class ComplaintCategory
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Resolution SLAs")]
        public int SLADays { get; set; }
        [Display(Name = "IS Active")]
        public bool IsActive { get; set; }
    }
}