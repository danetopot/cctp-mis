﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Newtonsoft.Json;

    public class CaseFlow : CreateModifyFields
    {
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("CaseIssueId")]
        public CaseIssue CaseIssue { get; set; }

        [Display(Name = "Case Issue")]
        public int CaseIssueId { get; set; }

        [Display(Name = "Level No.")]
        public byte StepNo { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Level Name")]
        [RegularExpression("^[A-Za-z0-9- ]*$", ErrorMessage = "Invalid!")]
        public string StepName { get; set; }

        [StringLength(100)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Case Issue Description!")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("UserGroupId")]
        public UserGroup UserGroup { get; set; }

        [Display(Name = "User Group")]
        public int UserGroupId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [Display(Name = "Can Resolve")]
        public bool CanResolve { get; set; }

        [Display(Name = "Docs")]
        public int Docs { get { return (this.CaseFlowDocs == null) ? 0 : this.CaseFlowDocs.Count; } }

        [JsonIgnore]
        public ICollection<CaseFlowDoc> CaseFlowDocs { get; set; }

    }

}