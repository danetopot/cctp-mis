﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class EnumeratorDevice
    {
        public int Id { get; set; }
        [Display(Name = "Device ID")]
        public string DeviceId { get; set; }
        [Display(Name = "Model")]
        public string DeviceModel { get; set; }
        [Display(Name = "Manufacturer")]
        public string DeviceManufacturer { get; set; }
        [Display(Name = "Device Name")]
        public string DeviceName { get; set; }
        public string Version { get; set; }
        [Display(Name = "Version No.")]
        public string VersionNumber { get; set; }
       
        public string Platform { get; set; }
        [Display(Name = "Device Type")]
        public string Idiom { get; set; }
        [Display(Name = "Device")]
        public bool IsDevice { get; set; }
        [Display(Name = "Enumerator")]
        public int EnumeratorId { get; set; }
    }
}