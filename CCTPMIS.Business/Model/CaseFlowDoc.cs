﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CaseFlowDoc : CreateModifyFields
    {
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("CaseFlowId")]
        public CaseIssue CaseFlow { get; set; }

        [Display(Name = "Case Flow")]
        public int CaseFlowId { get; set; }

        [ForeignKey("SuppDocId")]
        public SystemCodeDetail SuppDoc { get; set; }

        [Display(Name = "Supporting Doc")]
        public int SuppDocId { get; set; }
    }

}