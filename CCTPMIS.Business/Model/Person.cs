﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Person : CreateModifyFields
    {
        [StringLength(50)]
        [Display(Name = "Birth Certificate Number ")]
        public string BirthCertNo { get; set; }
        [Display(Name = "Display Name ")]
        public string DisplayName => $"{FirstName}  {MiddleName}  {Surname}";
        [Display(Name = " Date Of Birth")]
        public DateTime Dob { get; set; }

        public string DateOfBirth => $"{Dob:yyyy-MM-dd}";

        [Display(Name = "First Name ")]
        [StringLength(50), Required]
        public string FirstName { get; set; }
        [Display(Name = "ID ")]
        public int Id { get; set; }
        [Display(Name = "Middle Name ")]
        [StringLength(50)]
        public string MiddleName { get; set; }
        [Display(Name = "Mobile Number #1 ")]
        [StringLength(20)]
        public string MobileNo1 { get; set; }
        [Display(Name = "Is Mobile Confirmed ")]
        public bool MobileNo1Confirmed { get; set; } = false;
        [Display(Name = "Mobile Number #2 ")]
        [StringLength(20)]
        public string MobileNo2 { get; set; }
        [Display(Name = "Mobile Number #2 Confirmed ")]
        public bool MobileNo2Confirmed { get; set; } = false;

        [Display(Name = "Name ")]
        public string Name => $"{FirstName}  {MiddleName}";

        [StringLength(30)]
        [Display(Name = "National ID Number ")]
        public string NationalIdNo { get; set; }

        [StringLength(50)]
        [Display(Name = "Reference ID")]
        public string RefId { get; set; }

        [ForeignKey("SexId")]
        public SystemCodeDetail Sex { get; set; }
        [Display(Name = "Sex ")]
        public int SexId { get; set; }

        [Display(Name = "Surname ")]
        [StringLength(50), Required]
        public string Surname { get; set; }

        public byte? SourceId { get; set; }
    }
}