﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdReg
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Sub Location")]
        public int SubLocationId { get; set; }
        [Display(Name = " ")]
        public SubLocation SubLocation { get; set; }
        [Display(Name = "Unique Id ")]
        public string UniqueId { get; set; }
        [Display(Name = "Location ")]
        public int LocationId { get; set; }

        public  Location  Location { get; set; }
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }
   
        public Programme Programme { get; set; }
        [Display(Name = "Village ")]
        public string Village { get; set; }
        [Display(Name = "Status ")]
        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }
        [Display(Name = "Years ")]
        public int Years { get; set; }
        [Display(Name = "Months ")]
        public int Months { get; set; }
        [Display(Name = "Nearest Religious Building ")]
        public string NearestReligiousBuilding { get; set; }
        [Display(Name = "Nearest School ")]
        public string NearestSchool { get; set; }
        [Display(Name = "Physical Address ")]
        public string PhysicalAddress { get; set; }
        [Display(Name = "Start Time ")]
        public DateTime StartTime { get; set; }
        [Display(Name = "End Time ")]
        public DateTime EndTime { get; set; }
        [Display(Name = "Registration Date ")]
        public DateTime RegistrationDate { get; set; }
        [Display(Name = "Sync Up Date ")]
        public DateTime SyncUpDate { get; set; }
        [Display(Name = "Download Date ")]
        public DateTime DownloadDate { get; set; }
        [Display(Name = "Reg Date I")]
        public DateTime? RegDate1 { get; set; }
        [Display(Name = "Reg Date II ")]
        public DateTime? RegDate2 { get; set; }
        [Display(Name = "Reg Date III")]
        public DateTime? RegDate3 { get; set; }
        [Display(Name = "Longitude ")]
        public double? Longitude { get; set; }
        [Display(Name = "Latitude ")]
        public double? Latitude { get; set; }
        [Display(Name = "Target Plan")]
        public int TargetPlanId { get; set; }
     
        public TargetPlan TargetPlan { get; set; }
        [Display(Name = "Enumerator ")]
        public int EnumeratorId { get; set; }
        public Enumerator Enumerator { get; set; }

        [Display(Name = "Sync Enumerator")]
        public int SyncEnumeratorId { get; set; }
        public Enumerator SyncEnumerator { get; set; }

        [Display(Name = "Interview Status")]
        public int InterviewStatusId { get; set; }
        [Display(Name = "Interview Result")]
        public int InterviewResultId { get; set; }
        [Display(Name = "Household Registration Batch ")]
        public int? HouseholdRegAcceptId { get; set; }

        public SystemCodeDetail InterviewStatus { get; set; }
        public SystemCodeDetail InterviewResult { get; set; }
        public HouseholdRegAccept HouseholdRegAccept { get; set; }

        [Display(Name = "Enumerator Device")]
        public int EnumeratorDeviceId { get; set; }
        public EnumeratorDevice EnumeratorDevice { get; set; }

        [Display(Name = "Sync Date")]
        public DateTime SyncDate { get; set; }
        [Display(Name = "App Version")]
        public string AppVersion { get; set; }
        [Display(Name = "App Build")]
        public string AppBuild { get; set; }

        [Display(Name = "PMT Score")]
        public decimal? PMTScore { get; set; }


        [ForeignKey("Id")]
        public HouseholdRegCharacteristic HouseholdRegCharacteristic { get; set; }
        public ICollection<HouseholdRegMember> HouseholdRegMembers { get; set; }
        public ICollection<HouseholdRegMemberDisability> HouseholdRegMemberDisabilities { get; set; }
        public ICollection<HouseholdRegProgramme> HouseholdRegProgrammes { get; set; }

        /*
        public string CgId { get; set; }
        public string BeneId { get; set; }  */

        public string GeoPosition => $"[ {Longitude}, {Latitude}]";
        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        public string ReferenceNo => $" Targeting No.: #{Id}".ToUpper();
        public string LocationName => $"{Location?.Name}";
        public string ProgrammeName => $"{Programme?.Name}";
        public string SubLocationName => $"{SubLocation?.Name}";

        [Display(Name = "Duration in Residence")]
        public string DurationDisplay => $"{Years} Years, {Months} Months";
    }
}