﻿using System;

namespace CCTPMIS.Business.Model
{
    public class RegException
    {
        public int Id { get; set; }
        public string HHdName { get; set; }
        public int HHdSex { get; set; }
        public string HHdDob { get; set; }
        public int HHdNationalIdNo { get; set; }
        public int IPRS_IDNo { get; set; }
        public string IPRS_Name { get; set; }
        public int IPRS_Sex { get; set; }
        public DateTime IPRS_DoB { get; set; }
        public bool HHD_IDNoExists { get; set; }
        public bool HHD_FirstNameExists { get; set; }
        public bool HHD_MiddleNameExists { get; set; }
        public bool HHD_SurnameExists { get; set; }
        public bool HHD_DoBMatch { get; set; }
        public bool HHD_DoBYearMatch { get; set; }
        public bool HHD_SexMatch { get; set; }
        public string CgName { get; set; }
        public int CgSex { get; set; }
        public DateTime CgDob { get; set; }
        public string CgNationalIdNo { get; set; }
        public string IPRS_CG_IDNo { get; set; }
        public string IPRS_CG_Name { get; set; }
        public string IPRS_CG_Sex { get; set; }
        public DateTime IPRS_CG_DoB { get; set; }
        public bool CG_IDNoExists { get; set; }
        public bool CG_FirstNameExists { get; set; }
        public bool CG_MiddleNameExists { get; set; }
        public bool CG_SurnameExists { get; set; }
        public bool CG_DoBMatch { get; set; }
        public bool CG_DoBYearMatch { get; set; }
        public bool CG_SexMatch { get; set; }
        public int RegPlanId { get; set; }
        public DateTime DateValidated { get; set; }
    }
}