﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PaymentCycleDetail:CreateApproveFields
    {
         

        [ForeignKey("DuplicateIdActionsFileId")]
        public FileCreation DuplicateIdActionsFile { get; set; }

        public int? DuplicateIdActionsFileId { get; set; }

        [Display(Name = "Enrolled House Holds")]
        public int EnrolledHHs { get; set; }

        [ForeignKey("FileCreationId")]
        public FileCreation FileCreation { get; set; }

        [Display(Name = "File Creation")]
        public int? FileCreationId { get; set; }


        [Display(Name = "Exceptions File")]
        public int? ExceptionsFileId { get; set; }

        // public int? DeletedBy { get; set; }

        // [ForeignKey("DeletedBy")]
        // public ApplicationUser DeletedByUser { get; set; }

        // public DateTime? DeletedOn { get; set; }
        [ForeignKey("IneligibleBeneficiaryActionsFileId")]
        public FileCreation IneligibleBeneficiaryActionsFile { get; set; }

        public int? IneligibleBeneficiaryActionsFileId { get; set; }

        [ForeignKey("InvalidIdActionsFileId")]
        public FileCreation InvalidIdActionsFile { get; set; }

        public int? InvalidIdActionsFileId { get; set; }

        [ForeignKey("InvalidPaymentAccountActionsFileId")]
        public FileCreation InvalidPaymentAccountActionsFile { get; set; }

        public int? InvalidPaymentAccountActionsFileId { get; set; }

        [ForeignKey("InvalidPaymentCardActionsFileId")]
        public FileCreation InvalidPaymentCardActionsFile { get; set; }

        public int? InvalidPaymentCardActionsFileId { get; set; }

         
        public ICollection<PaymentAdjustment> PaymentAdjustments { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Key, Column(Order = 1)]
        [Index(IsUnique = true, IsClustered = false, Order = 1)]
        public int PaymentCycleId { get; set; }

        // [ForeignKey("PaymentCycleId,ProgrammeId")]
        public ICollection<PaymentEnrolmentGroup> PaymentEnrolmentGroups { get; set; }

        [ForeignKey("PaymentStageId")]
        public SystemCodeDetail PaymentStage { get; set; }

        [Display(Name = "Payment Stage")]
        public int PaymentStageId { get; set; }

        [Display(Name = "Payroll Approved  By")]
        public int? PayrollApvBy { get; set; }

        [ForeignKey("PayrollApvBy")]
        public ApplicationUser PayrollApvByUser { get; set; }

        [Display(Name = "Payroll Approved  On")]
        public DateTime? PayrollApvOn { get; set; }

        [Display(Name = "Payroll  By")]
        public int? PayrollBy { get; set; }

        [ForeignKey("PayrollBy")]
        public ApplicationUser PayrollByUser { get; set; }

        [Display(Name = "Payroll Executed  By")]
        public int? PayrollExBy { get; set; }

        [ForeignKey("PayrollExBy")]
        public ApplicationUser PayrollExByUser { get; set; }

        [Display(Name = "Payroll Verified  By")]
        public int? PayrollExConfBy { get; set; }

        [ForeignKey("PayrollExConfBy")]
        public ApplicationUser PayrollExConfByUser { get; set; }
        [Display(Name = "Payroll Exchanged Confirmed By")]
        public DateTime? PayrollExConfOn { get; set; }

        [Display(Name = "Payroll Exchanged  On")]
        public DateTime? PayrollExOn { get; set; }

        [Display(Name = "Payroll  On")]
        public DateTime? PayrollOn { get; set; }

        [Display(Name = "Payroll Verified  By")]
        public int? PayrollVerBy { get; set; }

        [ForeignKey("PayrollVerBy")]
        public ApplicationUser PayrollVerByUser { get; set; }

        [Display(Name = "Payroll Verified  On")]
        public DateTime? PayrollVerOn { get; set; }
        [Display(Name = "Post payroll Approved By")]
        public int? PostPayrollApvby { get; set; }

        [ForeignKey("PostPayrollApvby")]
        public ApplicationUser PostPayrollApvbyUser { get; set; }
        [Display(Name = "Post payroll Approved By")]
        public DateTime? PostPayrollApvOn { get; set; }
        [Display(Name = "Post payroll Approved By")]
        public int? PostPayrollBy { get; set; }

        [ForeignKey("PostPayrollBy")]
        public ApplicationUser PostPayrollByUser { get; set; }

        public DateTime? PostPayrollOn { get; set; }

        [Display(Name = "Prepayroll Approved By")]
        public int? PrepayrollApvBy { get; set; }

        [ForeignKey("PrepayrollApvBy")]
        public ApplicationUser PrepayrollApvByUser { get; set; }

        [Display(Name = "Prepayroll Approved On")]
        public DateTime? PrepayrollApvOn { get; set; }

        [Display(Name = "Prepayroll By")]
        public int? PrePayrollBy { get; set; }

        [ForeignKey("PrePayrollBy")]
        public ApplicationUser PrePayrollByUser { get; set; }

        [Display(Name = "Prepayroll On")]
        public DateTime? PrePayrollOn { get; set; }

        // [ForeignKey("PaymentCycleId,ProgrammeId")]
        public ICollection<Prepayroll> Prepayrolls { get; set; }

        public Programme Programme { get; set; }

        [Display(Name = "Programme")]
        [Key, Column(Order = 2)]
        [Index(IsUnique = true, IsClustered = false, Order = 2)]
        public byte ProgrammeId { get; set; }
        [Display(Name = "Reconciled Approved By")]
        public int? ReconciledApvBy { get; set; }

        [ForeignKey("ReconciledApvBy")]
        public ApplicationUser ReconciledApvByUser { get; set; }
        [Display(Name = "Reconciled  Approved On")]
        public DateTime? ReconciledApvOn { get; set; }
        [Display(Name = "Reconciled   By")]
        public int? ReconciledBy { get; set; }

        [ForeignKey("ReconciledBy")]
        public ApplicationUser ReconciledByUser { get; set; }
        [Display(Name = "Reconciled On")]
        public DateTime? ReconciledOn { get; set; }

        [ForeignKey("SuspiciousPaymentActionsFileId")]
        public FileCreation SuspiciousPaymentActionsFile { get; set; }
        [Display(Name = "Suspicious Payment Actions File Id")]
        public int? SuspiciousPaymentActionsFileId { get; set; }





        public int? FundsRequestBy { get; set; }

        [ForeignKey("FundsRequestBy")]
        public ApplicationUser FundsRequestByUser { get; set; }

        public DateTime? FundsRequestOn { get; set; }


        public int? FundsRequestApvBy { get; set; }

        [ForeignKey("FundsRequestApvBy")]
        public ApplicationUser FundsRequestApvByUser { get; set; }

        public DateTime? FundsRequestApvOn { get; set; }




    }
}