﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdVillage
    {
        public GeoMaster GeoMaster { get; set; }

        [Key, Column(Order = 2)]
        public int GeoMasterId { get; set; }

        [Key, Column(Order = 1)]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        public Village Village { get; set; }

        [Key, Column(Order = 3)]
        public int VillageId { get; set; }
    }
}