﻿namespace CCTPMIS.Business.Model
{
    public class temp_PayrollFile
    {

        public int PaymentCycleId { get; set; }

        public string PaymentCycle { get; set; }

        public int EnrolmentNo { get; set; }

        public string ProgrammeNo { get; set; }

        public int BankId { get; set; }

        public string BankCode { get; set; }

        public string BranchCode { get; set; }

        public string AccountNo { get; set; }

        public string AccountName { get; set; }

        public decimal Amount { get; set; }

        public string BeneficiaryIDNo { get; set; }

        public string BeneficiaryContact { get; set; }

        public string CaregiverIDNo { get; set; }

        public string CaregiverContact { get; set; }

        public string SubLocation { get; set; }

        public string Location { get; set; }

        public string Division { get; set; }

        public string District { get; set; }

        public string County { get; set; }

        public string Constituency { get; set; }

    }
}