﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ExpansionPlan : CreateModifyFields
    {
        [Display(Name = "Categorical Households")]
        public int CategoricalHHs { get; set; }

        public ICollection<ExpansionPlanDetail> ExpansionPlanDetails { get; set; }

        public ExpansionPlanMaster ExpansionPlanMaster { get; set; }

        [Display(Name = "Expansion Plan Master")]
        public short ExpansionPlanMasterId { get; set; }

        [Display(Name = "Expansion Plan")]
        public int Id { get; set; }

        public Location Location { get; set; }

        [Display(Name = "Location")]
        public int LocationId { get; set; }

        [Display(Name = "Poverty Head Count Percentage")]
        public double  PovertyHeadCountPerc { get; set; } =0;

        [Display(Name = "Scaleup Equal Share")]
        public int ScaleupEqualShare { get; set; }

        [Display(Name = "Scaleup Poverty Prioritized")]
        public int ScaleupPovertyPrioritized { get; set; }
    }
}