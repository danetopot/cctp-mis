﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class BeneficiaryAccount
    {
        [StringLength(100), Required]
        [Display(Name = "Account Name ")]
        public string AccountName { get; set; }
        [Display(Name = " Account Number")]
        [StringLength(50), Required]
        public string AccountNo { get; set; }
        [Display(Name = "Expiry Date ")]
        public DateTime ExpiryDate { get; set; }
        [Display(Name = "Enrolment No. ")]
        public int HhEnrolmentId { get; set; }

        [ForeignKey("HhEnrolmentId")]
        public HouseholdEnrolment HouseholdEnrolment { get; set; }
        [Display(Name = " Account ID")]
        public int Id { get; set; }
        [Display(Name = "Account Opened Date ")]
        public DateTime OpenedOn { get; set; }

        [Display(Name = "Account Submitted Date ")]
        public DateTime? DateAdded { get; set; }

        public PspBranch PspBranch { get; set; }
        [Display(Name = " PSP Branch")]
        [Required]
        public short PspBranchId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }
        [Display(Name = " Account Status")]
        [Required]
        public int StatusId { get; set; }
    }
}