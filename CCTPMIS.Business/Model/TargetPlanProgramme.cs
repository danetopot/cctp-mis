﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class TargetPlanProgramme
    {
        [Key, Column(Order = 1)]
        public int TargetPlanId { get; set; }
        [Key, Column(Order = 2)]
        public int ListingPlanId { get; set; }
        [Key, Column(Order = 3)]
        public byte ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        //[ForeignKey("TargetPlanId")]
        //public TargetPlan TargetPlan { get; set; }
        [ForeignKey("ListingPlanId")]
        public TargetPlan ListingPlan { get; set; }
    }
}