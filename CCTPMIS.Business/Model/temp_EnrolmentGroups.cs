﻿namespace CCTPMIS.Business.Model
{
    public class temp_EnrolmentGroups
    {
        public int PaymentCycleId { get; set; }
        public int ProgrammeId { get; set; }
        public int EnrolmentGroupId { get; set; }
    }
}