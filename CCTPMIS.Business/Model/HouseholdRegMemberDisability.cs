﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRegMemberDisability
    {
        [Key, Column(Order = 3)]
        [Display(Name = "Household Registration Member")]
        public string HouseholdRegMemberId { get; set; }
        [Key, Column(Order = 1)]
        [Display(Name = "Disability ")]
        public int DisabilityId { get; set; }
        [Key, Column(Order = 2)]
        [Display(Name = "Household Registration ")]
        public int HouseholdRegId { get; set; }
        public SystemCodeDetail Disability { get; set; }
        public HouseholdRegMember HouseholdRegMember { get; set; }

    }
}