﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class Change : CreateApproveFields
    {
        [Display(Name = "#")]
        public int Id { get; set; }
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        [Display(Name = "Household Enrolment No.")]
        public int HhEnrolmentId { get; set; }

        [ForeignKey("HhEnrolmentId")]
        public HouseholdEnrolment HhEnrolment { get; set; }
        [Display(Name = "Type of Update")]
        public int ChangeTypeId { get; set; }
        public CaseCategory ChangeType { get; set; }
        [Display(Name = "Serial No.")]
        public int? SerialNo { get; set; }
        [Display(Name = "Reported By ")]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }
        [Display(Name = "Reporter's Name")]
        public string ReportedByName { get; set; }
        [Display(Name = "Reporter's national ID Number")]
        public string ReportedByIdNo { get; set; }
        [Display(Name = "Reporter's Phone Number")]
        public string ReportedByPhone { get; set; }
        [Display(Name = "Reporter's Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }
        [Display(Name = "Update Date")]
        public DateTime? ChangeDate { get; set; }
        [Display(Name = "Received By")]
        public string ReceivedBy { get; set; }
        [Display(Name = "Received On")]
        public DateTime ReceivedOn { get; set; }

        public string Designation { get; set; }

        [Display(Name = "Confirmed On")]
        public DateTime? ConfirmedOn { get; set; }
        [Display(Name = "Confirmed By")]
        public int? ConfirmedBy { get; set; }
        [Display(Name = "Verified By")]
        public int? VerifiedBy { get; set; }
        [ForeignKey("VerifiedBy")]
        public ApplicationUser VerifiedByUser { get; set; }
        [Display(Name = "Verified On")]
        public DateTime? VerifiedOn { get; set; }
        [Display(Name = "Rejected By")]
        public int? RejectedBy { get; set; }
        [Display(Name = "Rejected On")]
        public DateTime? RejectedOn { get; set; }
        [Display(Name = "Closed By")]
        public int? ClosedBy { get; set; }

        [ForeignKey("ClosedBy")]
        public ApplicationUser ClosedByUser { get; set; }

        [Display(Name = "Closed On")]
        public DateTime? ClosedOn { get; set; }
        [Display(Name = "Escalated By")]
        public int? EscalatedBy { get; set; }
        [ForeignKey("EscalatedBy")]
        public ApplicationUser EscalatedByUser { get; set; }
        [Display(Name = "Escalated On")]
        public DateTime? EscalatedOn { get; set; }
        [Display(Name = "Bulk Transfer By")]
        public int? BulkTransferBy { get; set; }
        [ForeignKey("BulkTransferBy")]
        public ApplicationUser BulkTransferByUser { get; set; }

        [Display(Name = "Bulk Transfer On")]
        public DateTime? BulkTransferOn { get; set; }

        [Display(Name = "Approved By")]
        public int? ApprovedBy { get; set; }
        [ForeignKey("ApprovedBy")]
        public ApplicationUser ApprovedByUser { get; set; }
        [Display(Name = "Approved On")]
        public DateTime? ApprovedOn { get; set; }
        public string BeneFullName { get; set; }
        public string BeneNationalIdNo { get; set; }
        public string BenePhoneNumber { get; set; }
        public int? BeneSexId { get; set; }
        public SystemCodeDetail BeneSex { get; set; }
        public DateTime? BeneDoB { get; set; }
        public string CgFullName { get; set; }
        public string CgNationalIdNo { get; set; }
        public string CgPhoneNumber { get; set; }
        public int? CgSexId { get; set; }
        public SystemCodeDetail CgSex { get; set; }
        public DateTime? CgDoB { get; set; }
        [Display(Name = "Death Date")]
        public DateTime? DeathDate { get; set; }
        [Display(Name = "Death Notification Date")]
        public DateTime? DeathNotificationDate { get; set; }
        [Display(Name = "Status")]
        public int ChangeStatusId { get; set; }
        [Display(Name = "Source of Update")]
        public int ChangeSourceId { get; set; }
        [Display(Name = "Escalation Level")]
        public int ChangeLevelId { get; set; }
        public SystemCodeDetail ChangeLevel { get; set; }
        public SystemCodeDetail ChangeStatus { get; set; }
        public SystemCodeDetail ChangeSource { get; set; }
        [Display(Name = "Member Relationship")]
        public int? RelationshipId { get; set; }
        [Display(Name = "Member Role")]
        public int? UpdatedRoleId { get; set; }
        public SystemCodeDetail Relationship { get; set; }
        public SystemCodeDetail UpdatedRole { get; set; }
        [Display(Name = "Sub Location")]
        public int? SubLocationId { get; set; }
        [Display(Name = "Member")]
        public int? PersonId { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Surname")]
        public string Surname { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Sex")]
        public int? SexId { get; set; }
        public SystemCodeDetail Sex { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime? DoB { get; set; }
        [Display(Name = "Birth Certificate Number")]
        public string BirthCertNo { get; set; }
        [Display(Name = "National Id Number")]
        public string NationalIdNo { get; set; }
        [Display(Name = "Relationship")]
        public int? UpdatedRelationshipId { get; set; }
        public SystemCodeDetail UpdatedRelationship { get; set; }
        [Display(Name = "Member")]
        public int? UpdatedPersonId { get; set; }
        public Person UpdatedPerson { get; set; }

        public Person Person { get; set; }
        [Display(Name = "First Name")]
        public string UpdatedFirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string UpdatedMiddleName { get; set; }
        [Display(Name = "Surname")]
        public string UpdatedSurname { get; set; }
        [Display(Name = "Sex")]
        public int? UpdatedSexId { get; set; }
        public SystemCodeDetail UpdatedSex { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime? UpdatedDoB { get; set; }
        [Display(Name = "Birth Certification Number")]
        public string UpdatedBirthCertNo { get; set; }
        [Display(Name = " National Id Number")]
        public string UpdatedNationalIdNo { get; set; }
        [Display(Name = "SubLocation")]
        public int? UpdatedSubLocationId { get; set; }
        public SubLocation UpdatedSubLocation { get; set; }
        [Display(Name = "Constituency")]
        public int ConstituencyId { get; set; }

        [Display(Name = "Mobile Number #1")]
        public string UpdatedMobileNo1 { get; set; }
        [Display(Name = "Mobile Number #2")]
        public string UpdatedMobileNo2 { get; set; }


        public Constituency Constituency { get; set; }

        public ICollection<ChangeNote> ChangeNotes { get; set; }
        public ICollection<ChangeDocument> ChangeDocuments { get; set; }

    }
}