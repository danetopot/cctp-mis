﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRecCharacteristic
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "ID")]
        public int HouseholdMembers { get; set; }
        [Display(Name = "Habitable Rooms")]
        public int HabitableRooms { get; set; }
        [Display(Name = "Is Owned")]
        public int IsOwnedId { get; set; }
        [Display(Name = "Tenure Status")]
        public int TenureStatusId { get; set; }
        [Display(Name = "Roof Material ")]
        public int RoofMaterialId { get; set; }
        [Display(Name = "Wall Material")]
        public int WallMaterialId { get; set; }
        [Display(Name = "Floor Material")]
        public int FloorMaterialId { get; set; }
        [Display(Name = "Dwelling Unit Risk")]
        public int DwellingUnitRiskId { get; set; }
        [Display(Name = "Water Source")]
        public int WaterSourceId { get; set; }
        [Display(Name = "Toilet Type")]
        public int ToiletTypeId { get; set; }
        [Display(Name = "Cooking Fuel Source")]
        public int CookingFuelSourceId { get; set; }
        [Display(Name = "Lighting Source")]
        public int LightingSourceId { get; set; }
        [Display(Name = "Has Television")]
        public int IsTelevisionId { get; set; }
        [Display(Name = "Has Motor cycle")]
        public int IsMotorcycleId { get; set; }
        [Display(Name = "Has TukTuk")]
        public int IsTukTukId { get; set; }
        [Display(Name = "Has Refrigerator ")]
        public int IsRefrigeratorId { get; set; }
        [Display(Name = "Has Car")]
        public int IsCarId { get; set; }
        [Display(Name = "Has Mobile Phone")]
        public int IsMobilePhoneId { get; set; }
        [Display(Name = "Has Bicycle")]
        public int IsBicycleId { get; set; }
        [Display(Name = "Exotic Cattle")]
        public int ExoticCattle { get; set; }
        [Display(Name = "Indigenous Cattle")]
        public int IndigenousCattle { get; set; }
        [Display(Name = "Sheep")]
        public int Sheep { get; set; }
        [Display(Name = "Goats")]
        public int Goats { get; set; }
        [Display(Name = "Camels")]
        public int Camels { get; set; }
        [Display(Name = "Donkeys")]
        public int Donkeys { get; set; }
        [Display(Name = "Pigs")]
        public int Pigs { get; set; }
        [Display(Name = "Chicken")]
        public int Chicken { get; set; }
        [Display(Name = "Deaths")]
        public int? Deaths { get; set; }
        [Display(Name = "LiveBirths")]
        public int? LiveBirths { get; set; }
        [Display(Name = "Household Condition")]
        public int HouseholdConditionId { get; set; }
        [Display(Name = "hAS Skipped Meal ")]
        public int IsSkippedMealId { get; set; }
        [Display(Name = "NSNP Programmes")]
        public int NsnpProgrammesId { get; set; }
        [Display(Name = "Other Programmes")]
        public int OtherProgrammesId { get; set; }
        [Display(Name = "Other Programme Names")]
        public string OtherProgrammeNames { get; set; }
        [Display(Name = "Benefit Type ")]
        public int? BenefitTypeId { get; set; }
        [Display(Name = "Last Receipt Amount")]
        public decimal? LastReceiptAmount { get; set; }
        [Display(Name = "In Kind Benefit")]
        public string InKindBenefit { get; set; }
        


        public SystemCodeDetail IsOwned { get; set; }
        public SystemCodeDetail TenureStatus { get; set; }
        public SystemCodeDetail RoofMaterial { get; set; }
        public SystemCodeDetail WallMaterial { get; set; }
        public SystemCodeDetail FloorMaterial { get; set; }
        public SystemCodeDetail DwellingUnitRisk { get; set; }
        public SystemCodeDetail WaterSource { get; set; }
        public SystemCodeDetail ToiletType { get; set; }
        public SystemCodeDetail CookingFuelSource { get; set; }
        public SystemCodeDetail LightingSource { get; set; }
        public SystemCodeDetail IsTelevision { get; set; }
        public SystemCodeDetail IsMotorcycle { get; set; }
        public SystemCodeDetail IsTukTuk { get; set; }
        public SystemCodeDetail IsRefrigerator { get; set; }
        public SystemCodeDetail IsCar { get; set; }
        public SystemCodeDetail IsMobilePhone { get; set; }
        public SystemCodeDetail IsBicycle { get; set; }
        public SystemCodeDetail HouseholdCondition { get; set; }
        public SystemCodeDetail IsSkippedMeal { get; set; }
        public SystemCodeDetail NsnpProgrammes { get; set; }
        public SystemCodeDetail OtherProgrammes { get; set; }
        public SystemCodeDetail BenefitType { get; set; }
    }
}