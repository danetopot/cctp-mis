﻿namespace CCTPMIS.Business.Model
{
    using System;

    using Context;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationRoleStore : RoleStore<ApplicationRole, int, ApplicationUserRole>,
                                        IQueryableRoleStore<ApplicationRole, int>,
                                        IRoleStore<ApplicationRole, int>,
                                        IDisposable
    {
        public ApplicationRoleStore()
            : base(new ApplicationDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}