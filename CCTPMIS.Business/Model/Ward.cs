﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class Ward : CreateModifyFields
    {
        [StringLength(20)]
        [Required]
        [Display(Name = "Ward Code")]
        public string Code { get; set; }

        [JsonIgnore]
        public Constituency Constituency { get; set; }

        [Required]
        [Display(Name = "Constituency")]
        public int ConstituencyId { get; set; }

        [Display(Name = "Ward ID")]
        public int Id { get; set; }

        [StringLength(30)]
        [Required]
        [Display(Name = "Ward Name")]
        public string Name { get; set; }

        [JsonIgnore]
        public ICollection<WardLocation> WardLocations { get; set; }
    }
}