﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class Enumerator:CreateModifyFields
    {
        public int Id { get; set; }
        public int ConstituencyId { get; set; }
        public Constituency Constituency { get; set; }

        public string SecurityStamp { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }

        [Display(Name = "Full names")]
        public string FullName => $"{FirstName} {MiddleName} {Surname}";

        public string NationalIdNo { get; set; }
        public string MobileNo { get; set; }
        public bool IsActive { get; set; }      
        public DateTime? LoginDate { get; set; }
        public DateTime? ActivityDate { get; set; }
        public int AccessFailedCount { get; set; } = 0;
        public bool IsLocked { get; set; }
        public int? DeactivatedBy { get; set; }
        public DateTime? DeactivatedOn { get; set; }

        public DateTime? PasswordChangeDate { get; set; }
        public bool LockoutEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }


        public ICollection<EnumeratorLocation> EnumeratorLocations { get; set; }

    }
}