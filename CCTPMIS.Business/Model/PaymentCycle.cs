﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PaymentCycle : CreateModifyFields
    {
        [Display(Name = "Payment Cycle")]
        public string CycleName => $"{FinancialYear?.Description}  : {FromMonth?.Description} - {ToMonth?.Description}";

        [Display(Name = "Payment Cycle Months")]
        public string CycleMonthName => $"{FromMonth?.Description}    -    {ToMonth?.Description}";

        [Display(Name = "Payment Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(128)]
        [RegularExpression("^[A-Za-z0-9 .,]*$", ErrorMessage = "Invalid Payment cycle name!")]
        public string Description { get; set; }

        [ForeignKey("FinancialYearId")]
        public SystemCodeDetail FinancialYear { get; set; }

        [Display(Name = "Financial Year")]
        [Index(IsUnique = true, IsClustered = false, Order = 2)]
        public int FinancialYearId { get; set; }

        [ForeignKey("FromMonthId")]
        public SystemCodeDetail FromMonth { get; set; }

        [Display(Name = "From Month")]
        [Index(IsUnique = true, IsClustered = false, Order = 1)]
        public int FromMonthId { get; set; }

        [Display(Name = "Payment Cycle")]
        public int Id { get; set; }

        public ICollection<PaymentCycleDetail> PaymentCycleDetails { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [ForeignKey("ToMonthId")]
        public SystemCodeDetail ToMonth { get; set; }

        [Display(Name = "To Month")]
        public int ToMonthId { get; set; }
        public ICollection<PaymentAccountActivityMonth> PaymentAccountActivityMonths { get; set; }
        public int? ExceptionsFileId { get; set; }
        [ForeignKey("ExceptionsFileId")]
        public FileCreation ExceptionsFile { get; set; }


        public ICollection<Prepayroll> Prepayrolls { get; set; }
        public ICollection<Payroll> Payrolls { get; set; }

    }
}