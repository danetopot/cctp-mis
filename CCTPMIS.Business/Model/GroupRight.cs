﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class GroupRight
    {
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }

        [Display(Name = "Created on")]
        public DateTime CreatedOn { get; set; }

        public ModuleRight ModuleRight { get; set; }

        [Display(Name = "Module Right")]
        [Key, Column(Order = 2)]
        public int ModuleRightId { get; set; }

        public UserGroup UserGroup { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "User Group")]
        public int UserGroupId { get; set; }
    }
}