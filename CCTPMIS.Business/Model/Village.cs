﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class Village : CreateModifyFields
    {
        [Display(Name = "Ward Code")]
        [StringLength(20), Required]
        public string Code { get; set; }

        [Display(Name = "Ward ID")]
        public int Id { get; set; }

        [Display(Name = "Ward Name")]
        [StringLength(30), Required]
        public string Name { get; set; }

        [JsonIgnore]
        public SubLocation SubLocation { get; set; }

        [Display(Name = "Sub Location")]
        public int SubLocationId { get; set; }
    }
}