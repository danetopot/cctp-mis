﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Household : CreateModifyFields
    {
        [Display(Name = "Household ID")]
        public int Id { get; set; }

        public Programme Programme { get; set; }
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        [StringLength(50)]
        [Display(Name = "Reference ID")]
        public string RefId { get; set; }

        public SystemCodeDetail RegGroup { get; set; }
        [Display(Name = "Registration group")]
        public int RegGroupId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [StringLength(50)]
        public string Village { get; set; }

        public Source Source { get; set; }  
        public byte? SourceId { get; set; }

        public int? TokenId { get; set; }


        [ForeignKey("HhId")]
        public ICollection<HouseholdMember> HouseholdMembers { get; set; }

        [ForeignKey("HhId")]
        public ICollection<HouseholdSubLocation> HouseholdSubLocation { get; set; }
        [ForeignKey("HhId")]
        public ICollection<HouseholdEnrolment> HouseholdEnrolment { get; set; }

    }
}