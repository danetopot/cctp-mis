﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdMember : CreateModifyFields
    {
        [Display(Name = "Household ID")]
        [Index("IX_HouseholdMember", 1, IsClustered = false)]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        public int Id { get; set; }

        [ForeignKey("MemberRoleId")]
        public SystemCodeDetail MemberRole { get; set; }
        [Display(Name = "Role")]
        public int MemberRoleId { get; set; }

        public Person Person { get; set; }
        [Display(Name = "Person")]
        [Index("IX_HouseholdMember", 2, IsClustered = false)]
        public int PersonId { get; set; }

        [ForeignKey("RelationshipId")]
        public SystemCodeDetail Relationship { get; set; }
        [Display(Name = "Relationship")]
        [Index("IX_HouseholdMember", 3, IsClustered = false)]
        public int RelationshipId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }
        [Display(Name = "Status")]
        [Index("IX_HouseholdMember", 4, IsClustered = false)]
        public int StatusId { get; set; }
    }
}