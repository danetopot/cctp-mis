﻿namespace CCTPMIS.Business.Model
{
    public class ChangeType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}