﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUserClaim : IdentityUserClaim<int>
    {
        [StringLength(256)]
        public override string ClaimType { get; set; }

        [StringLength(256)]
        public override string ClaimValue { get; set; }
    }
}