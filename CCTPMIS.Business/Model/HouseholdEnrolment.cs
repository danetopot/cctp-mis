﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdEnrolment
    {
        [StringLength(7), Required]
        public string BeneProgNoPrefix { get; set; }
        [Display(Name = "Household Enrolment Plan")]
        public int HhEnrolmentPlanId { get; set; }
        [Display(Name = "Household")]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        [ForeignKey("HhEnrolmentPlanId")]
        public HouseholdEnrolmentPlan HouseholdEnrolmentPlan { get; set; }
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Programme Number")]
        public int ProgrammeNo { get; set; }

        public string ProgrammeNumber => $"{BeneProgNoPrefix}{ProgrammeNo:D6}";


        public ICollection<BeneficiaryAccount> BeneficiaryAccounts { get; set; }
    }
}