﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Prepayroll
    {
        [Display(Name = "Arrears Amount")]
        public decimal AdjustmentAmount { get; set; } = 0;

        [Display(Name = "Beneficiary Card Id")]
        public int? BeneAccountId { get; set; }

        [Display(Name = "Beneficiary Date Of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BeneDoB { get; set; }

        [Display(Name = "Beneficiary Name")]
        public string BeneFullName => $"{BeneFirstName} {BeneMiddleName} {BeneSurname}";

        [ForeignKey("BeneAccountId")]
        public BeneficiaryAccount BeneficiaryAccount { get; set; }

        [StringLength(50)]
        [Display(Name = "Beneficiary First Name")]
        public string BeneFirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "Beneficiary Middle Name")]
        public string BeneMiddleName { get; set; }

        [StringLength(30)]
        [Display(Name = "Beneficiary National ID Number")]
        public string BeneNationalIdNo { get; set; }

        [Display(Name = "Beneficiary Member ID")]
        public int BenePersonId { get; set; }

        [ForeignKey("BeneSexId")]
        public SystemCodeDetail BeneSex { get; set; }

        [Display(Name = "Beneficiary Sex")]
        public int BeneSexId { get; set; }

        [StringLength(50)]
        [Display(Name = "Beneficiary Surname")]
        public string BeneSurname { get; set; }

        [Display(Name = "Caregiver Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? CgdoB { get; set; }
        [Display(Name = "Caregiver   Name")]
        public string CgFullName => $"{CgFirstName} {CgMiddleName} {CgSurname}";
        [StringLength(50)]
        [Display(Name = "Caregiver First Name")]
        public string CgFirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "Caregiver Middle Name")]
        public string CgMiddleName { get; set; }

        [StringLength(30)]
        [Display(Name = "Caregiver National Id")]
        public string CgNationalIdNo { get; set; }

        [Display(Name = "Caregiver Member Id")]
        public int? CgPersonId { get; set; }

        [ForeignKey("CgSexId")]
        public SystemCodeDetail CgSex { get; set; }

        [Display(Name = "Caregiver Sex")]
        public int? CgSexId { get; set; }

        [StringLength(50)]
        [Display(Name = "Caregiver Surname")]
        public string CgSurname { get; set; }

        [Display(Name = "Continuous Account Inactivity")]
        public byte ConseAccInactivity { get; set; }

        [Display(Name = "Entitlement Amount")]
        public decimal EntitlementAmount { get; set; } = 0;

        [Display(Name = "House Hold")]
        [Key, Column(Order = 3)]
        public int HhId { get; set; }

        [ForeignKey("HhStatusId")]
        public SystemCodeDetail HhStatus { get; set; }

        [ForeignKey("BenePaymentCardId")]
        public BeneficiaryPaymentCard BeneficiaryPaymentCard { get; set; }
        [Display(Name = "Payment Card Number")]
        public int? BenePaymentCardId { get; set; }
        [Display(Name ="Household Status")]
        public int HhStatusId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        //[ForeignKey("PaymentCycleId,ProgrammeId,HhId")]
        //public ICollection<Payment> Payments { get; set; }

        public PaymentZone PaymentZone { get; set; }

        [Display(Name = "Payment Zone Commission Amount")]
        public decimal PaymentZoneCommAmt { get; set; }

        [Display(Name = "Payment Zone")]
        public short PaymentZoneId { get; set; }

        [Display(Name = "Can  Receive Payment")]
        public bool PriReciCanReceivePayment { get; set; } = true;

        public Programme Programme { get; set; }

        [Key, Column(Order = 2)]
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        public SubLocation SubLocation { get; set; }

        [Display(Name = "Sub Location")]
        public int SubLocationId { get; set; }

        [Display(Name = "Total House Hold Members")]
        public int TotalHhMembers { get; set; }


        public ICollection<PrepayrollIneligible> PrepayrollIneligibles { get; set; }
        public ICollection<PrepayrollDuplicateId> PrepayrollDuplicateIds { get; set; }
        public ICollection<PrepayrollInvalidId> PrepayrollInvalidIds { get; set; }
        public ICollection<PrepayrollInvalidPaymentAccount> PrepayrollInvalidPaymentAccounts { get; set; }
        public ICollection<PrepayrollInvalidPaymentCard> PrepayrollInvalidPaymentCards { get; set; }
        public ICollection<PrepayrollSuspicious> PrepayrollSuspicious { get; set; }



    }
}