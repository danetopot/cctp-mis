﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace CCTPMIS.Business.Model
{
    public static class IdentityExtensions
    {
        public static string GetDisplayName(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }
            var ci = identity as ClaimsIdentity;
            if (ci != null)
            {
                var name = ci.FindFirstValue("DisplayName");
                if (string.IsNullOrEmpty(name))
                    name = "Test User";
                return name;
            }
            return "TestUser";
        }
    }
}