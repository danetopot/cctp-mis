﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCTPMIS.Business.Model
{
    public class PeriodicPaymentIndicator
    {
        [Key, Column(Order =1)]
        public int MonitoringPeriodId { get; set; }
         
        [Key, Column(Order = 3)]
        public byte ProgrammeId { get; set; }
        [Key, Column(Order = 2)]
        public int IndicatorId { get; set; }
        public byte PspId { get; set; }
        public decimal AmountTotal => AmountWithin + AmountWithout;
        public decimal AmountWithin { get; set; }
        public double PercentWithin { get; set; }

        public double PercentTotal => PercentWithin + PercentWithout;
        public decimal AmountWithout { get; set; }
        public double PercentWithout { get; set; }

        public Programme Programme { get; set; }
        public Psp Psp { get; set; }
        public MonitoringPeriod MonitoringPeriod { get; set; }
        public Indicator Indicator { get; set; }
    }

    public class PeriodicConstituencyBeneIndicator
    {
        [Key, Column(Order = 1)]
        public int MonitoringPeriodId { get; set; }
        [Key, Column(Order = 2)]
        public int ConstituencyId { get; set; }
        [Key, Column(Order = 3)]
        public byte ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        [Key, Column(Order = 4)]
        public int IndicatorId { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Total { get; set; }
        public Indicator Indicator { get; set; }
        public MonitoringPeriod MonitoringPeriod { get; set; }
        public Constituency Constituency { get; set; }
     }
    public class MonitoringPeriod
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartMonthId { get; set; }
        public int EndMonthId { get; set; }
        public int Year { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
    }
    public class Indicator
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
    }
     
     

}
