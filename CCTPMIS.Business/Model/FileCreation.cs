﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FileCreation
    {
        [Display(Name = "File Created By ")]
        public int CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }

        [Display(Name = "File Created On")]
        public DateTime CreatedOn { get; set; }

        // [Display(Name = "File Approved By")]
        // public int? ApprovedBy { get; set; }

        // [Display(Name = "File Approved On")]
        // public DateTime? ApprovedOn { get; set; }

        // [ForeignKey("ApprovedBy")]
        // public ApplicationUser ApprovedByUser { get; set; }
        [ForeignKey("CreationTypeId")]
        public SystemCodeDetail CreationType { get; set; }

        [Display(Name = "File Create Type")]
        public int CreationTypeId { get; set; } /*  WHETHER AN UPLOAD, SYSTEM GENERATION   */

        [StringLength(64)]
        [Display(Name = "File Check Sum")]
        public string FileChecksum { get; set; }

        public ICollection<FileDownload> FileDownloads { get; set; }

        [StringLength(64)]
        [Display(Name = "File Password")]
        public string FilePassword { get; set; }

        [StringLength(128)]
        [Required]
        [Display(Name = "File path")]
        public string FilePath { get; set; }

        [Display(Name = "File")]
        public int Id { get; set; }

        public bool? IsShared { get; set; } = false;

        [Display(Name = "File Name")]
        [StringLength(128)]
        [Required]
        public string Name { get; set; }

        public ApplicationUser TargetUser { get; set; }

        public int? TargetUserId { get; set; }

        [ForeignKey("TypeId")]
        public SystemCodeDetail Type { get; set; }

        [Display(Name = "File Type")]
        public int TypeId { get; set; } /*  --WHETHER ENROLMENT, PAYMENT etc  */
    }
}