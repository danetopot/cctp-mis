﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class RegistrationHhCv : RegCommon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public new int Id { get; set; }

        public int CvStatusId { get; set; }
        public int CvEnumeratorId { get; set; }
        public int CvSyncEnumeratorId { get; set; }
        public DateTime CvSyncUpDate { get; set; }
        public DateTime CvUpdateDate { get; set; }
        public DateTime CvSyncDownDate { get; set; }
        public int? RegAcceptCvId { get; set; }
        public int RegistrationHhId { get; set; }
    }
}