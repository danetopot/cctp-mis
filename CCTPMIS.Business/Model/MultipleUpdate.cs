﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class EFC : CreateApproveFields
    {
        [DisplayName("EFC")]
        public int Id { get; set; }
        [DisplayName("EFC Type")]
        public int? EFCTypeId { get; set; }
        public SystemCodeDetail EFCType { get; set; }

        [DisplayName("Serial No.")]
        public int? SerialNo { get; set; }
        [DisplayName("Reported By Name")]
        public string ReportedByName { get; set; }
        [DisplayName("Reported By National ID No")]
        public string ReportedByIdNo { get; set; }
        [DisplayName("Reported By Phone Number")]
        public string ReportedByPhone { get; set; }
        [DisplayName("Reported By Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }

        [DisplayName("Reported By")]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }
        [DisplayName("Date Received")]
        public DateTime? ReportedDate { get; set; }
         
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Closed Date")]
        public DateTime? ClosedOn { get; set; }

        [DisplayName("Closed By")]
        public int? ClosedBy { get; set; }

        [ForeignKey("ClosedBy")]
        public ApplicationUser ClosedByUser { get; set; }

        [DisplayName("Received By")]
        public string ReceivedBy { get; set; }

        public string Regarding { get; set; }
       

        public string Designation { get; set; }
        [DisplayName("Source")]
        public int? SourceId { get; set; }
        [DisplayName("Sub County")]
        public int ConstituencyId { get; set; }
        [DisplayName("Status")]
        public int  StatusId { get; set; }

        public SystemCodeDetail Status { get; set; }

        public Constituency Constituency { get; set; }

        public SystemCodeDetail Source { get; set; }
        public ICollection<EFCNote> EFCNotes { get; set; }
        public ICollection<EFCDocument> EFCDocuments { get; set; }
    }
}