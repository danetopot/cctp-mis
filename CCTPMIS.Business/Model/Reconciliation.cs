﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace CCTPMIS.Business.Model
{
    public class Reconciliation : CreateApproveFields
    {

        public int Id { get; set; }

        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        [Display(Name = "Status")]
        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "PSP Reconciliations ")]
        public ICollection<ReconciliationDetail> ReconciliationDetails { get; set; }




        [Display(Name = "Finalized By")]
        public int? FinalizedBy { get; set; }

        [JsonIgnore]
        [ForeignKey("FinalizedBy")]
        [XmlIgnore]
        public ApplicationUser FinalizedByUser { get; set; }

        [Display(Name = "Finalized On")]
        public DateTime? FinalizedOn { get; set; }
    }
}