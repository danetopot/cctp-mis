﻿using System;

namespace CCTPMIS.Business.Model
{
    public class temp_Exceptions
    {

        public int PaymentCycleId { get; set; }

        public string PaymentCycle { get; set; }

        public int ProgrammeId { get; set; }

        public string Programme { get; set; }

        public int EnrolmentNo { get; set; }

        public string ProgrammeNo { get; set; }

        public string BeneFirstName { get; set; }

        public string BeneMiddleName { get; set; }

        public string BeneSurname { get; set; }

        public DateTime BeneDoB { get; set; }

        public string BeneSex { get; set; }

        public string BeneNationalIDNo { get; set; }

        public bool PriReciCanReceivePayment { get; set; }

        public bool IsInvalidBene { get; set; }

        public bool IsDuplicateBene { get; set; }

        public bool IsIneligibleBene { get; set; }

        public string CGFirstName { get; set; }

        public string CGMiddleName { get; set; }

        public string CGSurname { get; set; }

        public DateTime? CGDoB { get; set; }

        public string CGSex { get; set; }

        public string CGNationalIDNo { get; set; }

        public bool IsInvalidCG { get; set; }

        public bool IsDuplicateCG { get; set; }

        public bool IsIneligibleCG { get; set; }

        public string HhStatus { get; set; }

        public bool IsIneligibleHh { get; set; }

        public string AccountNumber { get; set; }

        public bool IsInvalidAccount { get; set; }

        public bool IsDormantAccount { get; set; }

        public string PaymentCardNumber { get; set; }

        public bool IsInvalidPaymentCard { get; set; }

        public string PaymentZone { get; set; }

        public decimal PaymentZoneCommAmt { get; set; }

        public byte ConseAccInactivity { get; set; }

        public decimal EntitlementAmount { get; set; }

        public decimal AdjustmentAmount { get; set; }

        public bool IsSuspiciousAmount { get; set; }

        public string SubLocation { get; set; }

        public string Location { get; set; }

        public string Division { get; set; }

        public string District { get; set; }

        public string County { get; set; }

        public string Constituency { get; set; }

        public bool? WasTrxSuccessful { get; set; }

        public string TrxNarration { get; set; }

    }
}