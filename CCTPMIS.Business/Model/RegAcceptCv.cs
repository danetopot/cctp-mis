﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    public class RegAcceptCv : RegAcceptCommon
    {
        public ICollection<RegistrationHhCv> RegistrationHhCvs { get; set; }

    }
}