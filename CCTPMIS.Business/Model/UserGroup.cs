﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class UserGroup
    {
        [Display(Name = "Created By")]
        public int? CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }

        [Display(Name = "Created On"), Required]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "User Group Id")]
        public int Id { get; set; }

        [Display(Name = "Modified By")]
        public int? ModifiedBy { get; set; }

        [ForeignKey("ModifiedBy")]
        public ApplicationUser ModifiedByUser { get; set; }

        [Display(Name = "Modified On")]
        public DateTime? ModifiedOn { get; set; }

        [Display(Name = "User Group Name")]
        [Required, StringLength(20)]

        [RegularExpression("^[-_ ,!.A-Za-z0-9]*$", ErrorMessage = "Invalid! Check, for any special characters. ")]
        public string Name { get; set; }

        public UserGroupProfile UserGroupProfile { get; set; }

        [Display(Name = "User Profile")]
        public int UserGroupProfileId { get; set; }

        [Display(Name = "User Group Type")]
        public int UserGroupTypeId { get; set; }

        public SystemCodeDetail UserGroupType { get; set; }
    }

    public class UserRole
    {
        public string Role { get; set; }
    }


}