﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Programme : CreateModifyFields
    {
        [ForeignKey("BeneficiaryTypeId")]
        public SystemCodeDetail BeneficiaryType { get; set; }

        [Display(Name = "Beneficiary Type")]
        public int BeneficiaryTypeId { get; set; }

        [StringLength(10), Required]
        [Index(IsUnique = true)]
        [Display(Name = "Beneficiary Programme Number Prefix")]
        public string BeneProgNoPrefix { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Programme Code")]
        [RegularExpression("^[A-Za-z0-9-]*$", ErrorMessage = "Invalid!")]
        public string Code { get; set; }

        [Display(Name = "Entitlement Amount")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        public decimal EntitlementAmount { get; set; }

        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [StringLength(100)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Programme Name!")]
        [Display(Name = "Programme Name")]
        public string Name { get; set; }

        [ForeignKey("PaymentFrequencyId")]
        public SystemCodeDetail PaymentFrequency { get; set; }

        [Display(Name = "Payment Frequency")]
        public int PaymentFrequencyId { get; set; }

        [ForeignKey("PrimaryRecipientId")]
        public SystemCodeDetail PrimaryRecipient { get; set; }

        [Display(Name = "Primary Recipient")]
        public int PrimaryRecipientId { get; set; }
        [Display(Name = "Primary Recipient Can receive payment")]
        public bool PriReciCanReceivePayment { get; set; } = true;

        [ForeignKey("SecondaryRecipientId")]
        public SystemCodeDetail SecondaryRecipient { get; set; }

        [Display(Name = "Secondary Recipient")]
        public int? SecondaryRecipientId { get; set; }

        [Display(Name = "Secondary Recipient is Mandatory")]
        public bool SecondaryRecipientMandatory { get; set; }
    }
}