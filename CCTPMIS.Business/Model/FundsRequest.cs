﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class FundsRequest
    {
        public FileCreation FileCreation { get; set; }

        [Display(Name = "Funds Request File ")]
        public int? FileCreationId { get; set; }

        public ICollection<FundsRequestDetail> FundsRequestDetails { get; set; }

        [Display(Name = "Funds Request ")]
        public int Id { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Display(Name = "Payment Cycle ")]
        public int PaymentCycleId { get; set; }
    }
}