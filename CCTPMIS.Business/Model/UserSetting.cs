﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class UserSetting
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [Required]
        public string Key { get; set; }



        [DataType(DataType.MultilineText), Required]
        public string Value { get; set; }
    }
}