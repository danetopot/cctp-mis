﻿namespace CCTPMIS.Business.Model
{
    public class temp_ExceptionTypes
    {
        public int PaymentCycleId { get; set; }
        public int ProgrammeId { get; set; }
        public int ExceptionTypeId { get; set; }
        public string ExceptionTypeCode { get; set; }
    }
}