﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class UserGroupProfile
    {
        [Display(Name = "Description")]
        [Required, StringLength(100)]
        [DataType(DataType.MultilineText)]
        [RegularExpression("^[-_ ,!.A-Za-z0-9]*$", ErrorMessage = "Invalid! Check, for any special characters. ")]
        public string Description { get; set; }

        [Display(Name = "User Profile Id")]
        public int Id { get; set; }

        [Required, StringLength(20)]
        [Display(Name = "User Profile Name")]
        [RegularExpression("^[-_ ,!.A-Za-z0-9]*$", ErrorMessage = "Invalid! Check, for any special characters. ")]
        public string Name { get; set; }

        public ICollection<UserGroup> UserGroups { get; set; }
    }
}