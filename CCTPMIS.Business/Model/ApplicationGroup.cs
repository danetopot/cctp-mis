﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class ApplicationGroup
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<ApplicationGroupRole> ApplicationGroupRoles { get; set; }
        public virtual ICollection<ApplicationGroupUser> ApplicationGroupUsers { get; set; }
    }
}