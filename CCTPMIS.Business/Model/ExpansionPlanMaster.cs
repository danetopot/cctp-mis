﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    public class ExpansionPlanMaster : CreateModifyFields
    {
        [Display(Name = "Expansion Plan Master Code")]
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string Code { get; set; }

        [Display(Name = "Expansion Plan Master Description ")]
        [StringLength(128)]
        public string Description { get; set; }

        [Display(Name = "Effective From Date")]
        public DateTime EffectiveFromDate { get; set; }

        [Display(Name = "Effective To Date")]
        public DateTime EffectiveToDate { get; set; }

        [JsonIgnore]
        public ICollection<ExpansionPlan> ExpansionPlans { get; set; }

        [Display(Name = "Expansion Plan Master")]
        public short Id { get; set; }

        public Programme Programme { get; set; }

        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }
    }
}