﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PersonSocialAssistance
    {
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        [Display(Name = "benefit Amount ")]
        public decimal BenefitAmount { get; set; } = 0;

        [ForeignKey("BenefitTypeId")]
        public SystemCodeDetail BenefitType { get; set; }
        [Display(Name = "Benefit Type ")]
        [Key, Column(Order = 3)]
        public int BenefitTypeId { get; set; }
        [Display(Name = "Benefit Description ")]
        [StringLength(128)]
        public string Description { get; set; }
        [Display(Name = "Other SP Programme ID ")]
        [ForeignKey("OtherSpProgrammeId")]
        public SystemCodeDetail OtherSpProgramme { get; set; }
        [Display(Name = "Other SP Programme ID ")]
        [Key, Column(Order = 2)]
        public int OtherSpProgrammeId { get; set; }
        
        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        [Display(Name = " Person")]
        [Key, Column(Order = 1)]
        public int PersonId { get; set; }
    }
}