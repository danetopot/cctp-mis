﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class BeneAccountMonthlyActivity
    {
        [Display(Name="ID")]
        public int Id { get; set; }
        [Display(Name = "Month ")]
        public int MonthId { get; set; }
        [ForeignKey("MonthId")]
        public SystemCodeDetail Month { get; set; }

        [Display(Name = "Year")]
        public short Year { get; set; }
        [Display(Name = "Status")]
        public int StatusId { get; set; }
        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }
        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "Modified By")]
        public int? ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public ApplicationUser ModifiedByUser { get; set; }
        [Display(Name = "Modified On")]
        public DateTime? ModifiedOn { get; set; }
        [Display(Name = "Approved By")]
        public int? ApvBy { get; set; }
        [ForeignKey("ApvBy")]
        public ApplicationUser ApvByUser { get; set; }
        [Display(Name = "Approved On")]
        public DateTime? ApvOn { get; set; }

        //public int? FileCreationId { get; set; }
        [Display(Name = "Closed By")]
        public int? ClosedBy { get; set; }
        [Display(Name = "Closed On")]
        public DateTime? ClosedOn { get; set; }

        [ForeignKey("ClosedBy")]
        public ApplicationUser ClosedByUser { get; set; }


        [Display(Name = "Month Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Month End Date")]
        public DateTime EndDate { get; set; }
    }
}