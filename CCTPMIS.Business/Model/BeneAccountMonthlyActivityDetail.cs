﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class BeneAccountMonthlyActivityDetail :CreateModifyFields
    {
        [Key, Column(Order = 1)]
        public int BeneAccountMonthlyActivityId { get; set; }

        [ForeignKey("BeneAccountMonthlyActivityId")]
        public BeneAccountMonthlyActivity BeneAccountMonthlyActivity { get; set; }

        [Key, Column(Order = 2)]
        public int BeneAccountId { get; set; }
        [ForeignKey("BeneAccountId")]
        public BeneficiaryAccount BeneficiaryAccount { get; set; }

        [Display(Name = "Had Unique Withdrawal")]
        public bool HadUniqueWdl { get; set; }
        [Display(Name = "Unique Withdrawal Transaction No.")]
        public string UniqueWdlTrxNo { get; set; }
        [Display(Name = "Unique Withdrawal Date")]
        public DateTime? UniqueWdlDate { get; set; }
        [Display(Name = "Had Biometrics Verified")]
        public bool HadBeneBiosVerified { get; set; }
        [Display(Name = "Is Dormant")]
        public bool IsDormant { get; set; }
        [Display(Name = "Dormancy Date")]
        public DateTime? DormancyDate { get; set; }
        [Display(Name = "Due for Claw Back")]
        public bool IsDueForClawback { get; set; }
        [Display(Name = "Claw Back Amount")]
        public decimal? ClawbackAmount { get; set; }      
    }
}