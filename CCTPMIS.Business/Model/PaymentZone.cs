﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class PaymentZone : CreateModifyFields
    {
        [Display(Name = "Commision")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        public decimal Commission { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Payment Zone Description ")]
        public string Description { get; set; }

        [Display(Name = "#")]
        public short Id { get; set; }

        [Display(Name = "Is Percent ? ")]
        public bool IsPerc { get; set; }

        public ICollection<Location> Locations { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Payment Zone Name ")]
        public string Name { get; set; }
    }
}