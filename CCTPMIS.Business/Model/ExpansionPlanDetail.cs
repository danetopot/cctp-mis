﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class ExpansionPlanDetail : CreateModifyFields
    {
        public int EnrolledHHs { get; set; }

        public ExpansionPlan ExpansionPlan { get; set; }

        public int ExpansionPlanId { get; set; }

        [ForeignKey("FinancialYearId")]
        public SystemCodeDetail FinancialYear { get; set; }

        public int FinancialYearId { get; set; }

        public int Id { get; set; }

        public int ScaleupEqualShare { get; set; }

        public int ScaleupPovertyPrioritized { get; set; }
    }
}