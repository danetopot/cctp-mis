﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUser : IdentityUser<int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        [Display(Name = "Last Activity Date")]
        public DateTime? ActivityDate { get; set; }

        [Display(Name = "Avatar"), StringLength(70)]
        public string Avatar { get; set; }

        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Deactivate Date")]
        public DateTime? DeactivateDate { get; set; }

        [RegularExpression("^[-_.A-Za-z0-9 ,]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Department"),  StringLength(100)]
        public string Department { get; set; }

        [Display(Name = "Avatar")]
        public string DisplayAvatar => string.IsNullOrEmpty(Avatar) ? "avatar.png" : Avatar;

        [Display(Name = "Display Names")]
        public string DisplayName => $"{FirstName} {MiddleName}";

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address"), Required, StringLength(100), MaxLength(100)]
        public override string Email { get; set; }

        [Display(Name = "Is Email Address Confirmed")]
        public override bool EmailConfirmed { get; set; }

        //   [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets")]
        [Display(Name = "First Name"), Required, StringLength(50)]
        public string FirstName { get; set; }

        [Display(Name = "Full names")]
        public string FullName => $"{FirstName} {MiddleName} {Surname}";

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "User ID")]
        public override int Id { get; set; }

        [Display(Name = "Activity Status")]
        public bool IsActive { get; set; }

        [Display(Name = "Locked Status")]
        public bool IsLocked { get; set; }

        [Display(Name = "Login Date")]
        public DateTime? LoginDate { get; set; }

        // [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Only Alphabets")]
        [Display(Name = "Middle Name"), StringLength(50)]
        public string MiddleName { get; set; }

        [Display(Name = "Modified By")]
        public int? ModifiedBy { get; set; }

        [Display(Name = "Modified On")]
        public DateTime? ModifiedOn { get; set; }

        [Display(Name = "Name")]
        public string Name => $"{FirstName}  {MiddleName}";

        // [RegularExpression("^[-_.A-Za-z0-9]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Organization"),  StringLength(100)]
        public string Organization { get; set; }

        [Display(Name = "Last Password Changed Date")]
        public DateTime? PasswordChangeDate { get; set; }

        // [Display(Name = "Access Failed Count")]
        // public override int AccessFailedCount { get; set; }
        [StringLength(256)]
        public override string PasswordHash { get; set; }

        [Display(Name = "Phone Number"), Required, StringLength(20)]
        public override string PhoneNumber { get; set; }

        [Display(Name = "Is Phone Number Confirmed")]
        public override bool PhoneNumberConfirmed { get; set; }

        //[RegularExpression("^[-_.A-Za-z0-9]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Position"), Required, StringLength(100)]
        public string Position { get; set; }

        [StringLength(256)]
        public override string SecurityStamp { get; set; }

        // [RegularExpression("^[-_.A-Za-z]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Surname"), Required, StringLength(50)]
        public string Surname { get; set; }

        public UserGroup UserGroup { get; set; }

        [Display(Name = "User Group")]
        public int UserGroupId { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Username"), Required, StringLength(100), MaxLength(100)]
        public override string UserName { get; set; }

        // [RegularExpression("^[-_.A-Za-z0-9]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "Staff Number"), StringLength(10)]
        public string StaffNo { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid!")]
        [Display(Name = "National ID No."), StringLength(10)]
        public string NationalIdNo { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie)
                .ConfigureAwait(false);
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(
            UserManager<ApplicationUser, int> manager,
            string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType).ConfigureAwait(false);

            // Add custom user claims here
            userIdentity.AddClaim(new Claim("DisplayName", DisplayName));
            return userIdentity;
        }

    }
}