﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class PasswordReset
    {
        [Display(Name = "ID")]
        public long Id { get; set; }

        [Display(Name = "Has the Code been Used?")]
        public bool IsReset { get; set; }

        [Display(Name = "Time Requested ")]
        public DateTime RequestTime { get; set; }

        [Display(Name = "Reset Code")]
        [StringLength(128)]
        public string ResetCode { get; set; }

        [Display(Name = "Reset Time ")]
        public DateTime? ResetTime { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }

        [Display(Name = "Validity Of Code (HRs)")]
        public int ValidityPeriod { get; set; }
    }
}