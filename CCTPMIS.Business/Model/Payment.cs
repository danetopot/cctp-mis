﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Payment
    {
        [Display(Name = "Household")]
        [Key, Column(Order = 3)]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        [Display(Name = "Programme")]
        public PaymentCycle PaymentCycle { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        public Programme Programme { get; set; }
        [Display(Name = "Programme")]
        [Key, Column(Order = 2)]

        public byte ProgrammeId { get; set; }
        [Display(Name = "Transaction Amount")]

        public decimal? TrxAmount { get; set; }
        [Display(Name = "Transaction Date")]

        public DateTime? TrxDate { get; set; }
        [Display(Name = "Transaction Narration")]

        [StringLength(128)]
        public string TrxNarration { get; set; }
        [Display(Name = "Transaction Number")]

        [StringLength(50)]
        public string TrxNo { get; set; }

        [Display(Name = "Transaction Successful")]
        public bool WasTrxSuccessful { get; set; }

        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [ForeignKey("PaymentCycleId,ProgrammeId,HhId")]
        public Payroll Payroll { get; set; }

        public string TrxStatus
        {
            get
            {
                if (WasTrxSuccessful)
                {
                    return "YES";
                }
                return "NO";
            }
        }
    }
}