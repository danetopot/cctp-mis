﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class RegPlanx 
    {
        public int Id { get; set; }

        [StringLength(30)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Exercise Name!")]
        [Display(Name = "Exercise Name")]
        public string Name { get; set; }

        [StringLength(128)]
        [DataType(DataType.MultilineText)]
        [Required]
        [RegularExpression("^[A-Za-z0-9 .,! ]*$", ErrorMessage = "Invalid Exercise Description!")]
        [Display(Name = "Exercise Description")]
        public string Description { get; set; }

        [Display(Name = "Plan Duration")] public string DateRange => $"{Start:D} - {End:D}";

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date")]
        public DateTime Start { get; set; }

        [Required]
        [Display(Name = "End Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime End { get; set; }



        [Required]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public SystemCodeDetail Category { get; set; }
        [Required] [Display(Name = "Stage")] public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }

        [Display(Name = "Closed By")] public int? ClosedBy { get; set; }

        [ForeignKey("ClosedBy")] public ApplicationUser ClosedByUser { get; set; }

        [Display(Name = "Closed On")] public DateTime? ClosedOn { get; set; }

        [Display(Name = "Community Validation List File")]
        public int? ValidationFileId { get; set; }

        public FileCreation ValidationFile { get; set; }
        [Display(Name = "Exceptions File")] public int? ExceptionsFileId { get; set; }

        public FileCreation ExceptionsFile { get; set; }
        public ICollection<RegAccept> Accepts { get; set; }

        [Display(Name = "Exceptions File")] public int? PostRegExceptionsFileId { get; set; }
        public int? FinalizeBy { get; set; }
        [ForeignKey("FinalizeBy")] public ApplicationUser FinalizeByUser { get; set; }

        public DateTime? FinalizeOn { get; set; }
        public int? FinalizeApvBy { get; set; }
        [ForeignKey("FinalizeApvBy")] public ApplicationUser FinalizeApvByUser { get; set; }
        public DateTime? FinalizeApvOn { get; set; }
        public int? ExceptionBy { get; set; }
        [ForeignKey("ExceptionBy")] public ApplicationUser ExceptionByUser { get; set; }

        public DateTime? ExceptionOn { get; set; }
        public int? ValBy { get; set; }
        [ForeignKey("ValBy")] public ApplicationUser ValByUser { get; set; }
        public DateTime? ValOn { get; set; }
        public int? ComValBy { get; set; }
        public DateTime? ComValOn { get; set; }
        public int? ComValApvBy { get; set; }
        public DateTime? ComValApvOn { get; set; }
        public int? PostRegExceptionBy { get; set; }
        public DateTime? PostRegExceptionOn { get; set; }

        public FileCreation PostRegExceptionsFile { get; set; }

        public ICollection<TargetPlanProgramme> TargetPlanProgrammes { get; set; }
        public ICollection<ListingPlanProgramme> ListingPlanProgrammes { get; set; }
        public ICollection<RegistrationHh> RegistrationHhs { get; set; }
        public ICollection<RegistrationHhCv> RegistrationHhCvs { get; set; }
    }
}