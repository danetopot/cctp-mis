﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class DbBackup
    {
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "File Path")]
        [StringLength(128)]
        public string FilePath { get; set; }

        [Display(Name = "Backup Id")]
        public int Id { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser User { get; set; }
    }
}