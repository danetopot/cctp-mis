﻿namespace CCTPMIS.Business.Model
{
    using System;

    using Context;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUserStore :
        UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim
        >,
        IUserStore<ApplicationUser, int>,
        IDisposable
    {
        public ApplicationUserStore()
            : this(new ApplicationDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}