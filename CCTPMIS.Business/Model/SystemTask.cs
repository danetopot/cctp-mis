﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class SystemTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Parent")]
        public int? ParentTaskId { get; set; }

        [DisplayName("Task Name")]
        public string TaskName { get; set; }

        public SystemTask Parent { get; set; }

        [ForeignKey("ParentTaskId")]
        public ICollection<SystemTask> Children { get; set; }
    }
}