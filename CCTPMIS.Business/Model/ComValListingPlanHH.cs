﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{

    public class ComValListingException : ListingExceptionChild
    {
        [Key]
        public int Id { get; set; }
    }

    public class ListingException : ListingExceptionChild
    {
        [Key]
        public int Id { get; set; }
    }

    
    public class ListingExceptionChild
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Id")]
        public ListingPlanHH ListingPlanHh { get; set; }
        public string BeneName { get; set; }
        public string BeneSex { get; set; }
        public string BeneDob { get; set; }
        public string BeneNationalIdNo { get; set; }
        public long? IPRS_IDNo { get; set; }
        public string IPRS_Name { get; set; }
        public string IPRS_Sex { get; set; }
        public DateTime? IPRS_DoB { get; set; }
        public bool Bene_IDNoExists { get; set; }
        public bool Bene_FirstNameExists { get; set; }
        public bool Bene_MiddleNameExists { get; set; }
        public bool Bene_SurnameExists { get; set; }
        public bool Bene_DoBMatch { get; set; }
        public bool Bene_DoBYearMatch { get; set; }
        public bool Bene_SexMatch { get; set; }
        public string CgName { get; set; }
        public string CgSex { get; set; }
        public DateTime CgDob { get; set; }
        public string CgNationalIdNo { get; set; }
        public string IPRS_CG_IDNo { get; set; }
        public string IPRS_CG_Name { get; set; }
        public string IPRS_CG_Sex { get; set; }
        public DateTime? IPRS_CG_DoB { get; set; }
        public bool CG_IDNoExists { get; set; }
        public bool CG_FirstNameExists { get; set; }
        public bool CG_MiddleNameExists { get; set; }
        public bool CG_SurnameExists { get; set; }
        public bool CG_DoBMatch { get; set; }
        public bool CG_DoBYearMatch { get; set; }
        public bool CG_SexMatch { get; set; }
        public int TargetPlanId { get; set; }
        public TargetPlan TargetPlan { get; set; }
        public DateTime DateValidated { get; set; }
    }

    public class ComValListingPlanHH
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Targeting ID")]
        public new int Id { get; set; }
        [Display(Name = "Reference No. ")]
        public string UniqueId { get; set; }
        [Display(Name = "Start Time ")]
        public DateTime? StartTime { get; set; }
        [Display(Name = "End Time ")]
        public DateTime? EndTime { get; set; }
        [Display(Name = "Programme ")]
        public byte ProgrammeId { get; set; }
        [Display(Name = "Registration Date ")]
        public DateTime RegDate { get; set; }
        [Display(Name = "SubLocation ")]
        public int SubLocationId { get; set; }
        public SubLocation SubLocation { get; set; }
        [Display(Name = "Location ")]
        public int LocationId { get; set; }
        public Location Location { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        [Display(Name = "Targeting Plan ")]
        public int TargetPlanId { get; set; }

        public TargetPlan TargetPlan { get; set; }
        [Display(Name = "Enumerator ")]
        public int EnumeratorId { get; set; }
        public Enumerator Enumerator { get; set; }
        [Display(Name = "First Name ")]
        public string BeneFirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string BeneMiddleName { get; set; }
        [Display(Name = "Surname ")]
        public string BeneSurname { get; set; }
        [Display(Name = "National ID No. ")]
        public string BeneNationalIdNo { get; set; }
        [Display(Name = "Phone Number ")]
        public string BenePhoneNumber { get; set; }
        [Display(Name = "Full Name ")]
        public string BeneFullName => $"{BeneFirstName}  {BeneMiddleName} {BeneSurname}";
        public SystemCodeDetail BeneSex { get; set; }
        [Display(Name = "Sex ")]
        public int? BeneSexId { get; set; }
        [Display(Name = "Date Of Birth ")]
        public DateTime? BeneDoB { get; set; }
        [Display(Name = "First Name ")]
        public string CgFirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string CgMiddleName { get; set; }
        [Display(Name = "Surname ")]
        public string CgSurname { get; set; }
        [Display(Name = "Full Name ")]
        public string CgFullName => $"{CgFirstName}  {CgMiddleName} {CgSurname}";
        [Display(Name = "Nationasl ID No. ")]
        public string CgNationalIdNo { get; set; }
        [Display(Name = "Phone No. ")]
        public string CgPhoneNumber { get; set; }
        [Display(Name = "Sex ")]
        public int CgSexId { get; set; }
        public SystemCodeDetail CgSex { get; set; }
        [Display(Name = "Date of Birth ")]
        public DateTime CgDoB { get; set; }
        [Display(Name = "Total Household Members ")]
        public int HouseholdMembers { get; set; }
        [Display(Name = "Status ")]
        public int StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }
        public string Village { get; set; }
        [Display(Name = "Physical Address  ")]
        public string PhysicalAddress { get; set; }
        [Display(Name = "Nearest Religious Building  ")]
        public string NearestReligiousBuilding { get; set; }
        [Display(Name = " Nearest School ")]
        public string NearestSchool { get; set; }
     
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        [Display(Name = "Sync Enumerator   ")]
        public int SyncEnumeratorId { get; set; }
        [Display(Name = "Sync Date  ")]
        public DateTime SyncDate { get; set; }
        [Display(Name = "Enumerator Device    ")]
        public int EnumeratorDeviceId { get; set; }
        public EnumeratorDevice EnumeratorDevice { get; set; }
        public Enumerator SyncEnumerator { get; set; }
        public Programme Programme { get; set; }

        [Display(Name = "App Version  ")]
        public string AppVersion { get; set; }
        [Display(Name = "App Build  ")]
        public string AppBuild { get; set; }
        [Display(Name = "Community Validation Date  ")]
        public DateTime? ComValDate { get; set; }
        [Display(Name = "Download Date  ")]
        public DateTime DownloadDate { get; set; }
        [Display(Name = "Community Validation Batch  ")]
        public int? ComValListingAcceptId { get; set; }
        public ComValListingAccept ComValListingAccept { get; set; }

        [ForeignKey("Id")]
        public ListingPlanHH ListingPlanHH { get; set; }
        [Display(Name = "Geo Coordinates  ")]
        public string GeoPosition => $"[ {Longitude}, {Latitude}]";
        [Display(Name = "Form No  ")]
        public string FormNo => $"Form No: #{UniqueId}".ToUpper();
        [Display(Name = "Reference No  ")]
        public string ReferenceNo => $" Targeting No.: #{Id}".ToUpper();
        [Display(Name = "Location ")]
        public string LocationName => $"{Location?.Name}";
        [Display(Name = "Programme  ")]
        public string ProgrammeName => $"{Programme?.Name}";
        [Display(Name = "Sub Location  ")]
        public string SubLocationName => $"{SubLocation?.Name}";

        [Display(Name = "Duration in Residence")]
        public string DurationDisplay => $"{Years} Years, {Months} Months";
        [Display(Name = " Caregiver Sex ")]
        public string CgSexName => $"{CgSex?.Description}";
        [Display(Name = " Beneficiary Sex ")]
        public string BeneSexName => $"{BeneSex?.Description}";

    }
}