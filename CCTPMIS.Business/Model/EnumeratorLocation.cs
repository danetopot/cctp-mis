﻿using Newtonsoft.Json;

namespace CCTPMIS.Business.Model
{
    public class EnumeratorLocation
    {
        public int Id { get; set; }
        public int EnumeratorId { get; set; }      
        public int LocationId { get; set; }
        [JsonIgnore]
        public Enumerator Enumerator { get; set; }
        [JsonIgnore]
        public Location Location { get; set; }
        public bool IsActive { get; set; }
    }
}
