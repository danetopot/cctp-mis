﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{

    public class ELMAH_Error
    {
        public Guid ErrorId { get; set; }
        public string Application { get; set; }
        public string Host { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string User { get; set; }
        public int StatusCode { get; set; }
        public DateTime TimeUtc { get; set; }
        [Key]
        public int Sequence { get; set; }
        public string AllXml { get; set; }
    }



    public class AuditTrail
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "USER ID")]
        public int? UserId { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Module")]
        public string Module { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Date Logged")]
        public DateTime LogTime { get; set; }
        [Display(Name = "IP Address")]
        public string IPAddress { get; set; }
        [Display(Name = "Mac Address")]
        public string MACAddress { get; set; }
        [Display(Name = "IMEI")]
        public string IMEI { get; set; }
        [Display(Name = "Module Right")]
        public int ModuleRightId { get; set; }
        [Display(Name = "Affected Table")]
        public int TableId { get; set; }
        [Display(Name = "Key 1")]
        public int Key1 { get; set; }
        [Display(Name = "Key 2")]
        public int? Key2 { get; set; }
        [Display(Name = "Key 3")]
        public int? Key3 { get; set; }
        [Display(Name = "Key 4")]
        public int? Key4 { get; set; }
        [Display(Name = "Key 5")]
        public int? Key5 { get; set; }
        [Display(Name = "Record")]
        public string Record { get; set; }
        [Display(Name = "Was Successful")]
        public bool? WasSuccessful { get; set; }

        [ForeignKey("TableId")]
        public LogTable Table { get; set; }
    }


    public class AuditTrailxx
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime LogTime { get; set; }
        public string IPAddress { get; set; }
        public string MACAddress { get; set; }
        public string IMEI { get; set; }
        public int ModuleRightId { get; set; }
        public int TableId { get; set; }
        [ForeignKey("TableId")]
        public LogTable Table { get; set; }


        public int Key1 { get; set; }
        public int? Key2 { get; set; }
        public int? Key3 { get; set; }
        public int? Key4 { get; set; }
        public int? Key5 { get; set; }
        public string Record { get; set; }
        public bool WasSuccessful { get; set; }
    }

    public class LogTable
    {
        public int Id { get; set; }
        public int? ModuleRightId { get; set; }
        public string TableName { get; set; }
    }


}