﻿using System;

namespace CCTPMIS.Business.Model
{
    public class RegistrationHhx
    {
        public int  Id { get; set; }

        public int LocalId { get; set; }
   
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        public bool IsFeatured { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime SyncTime { get; set; }
        public int SubLocationId { get; set; }

        public SubLocation SubLocation { get; set; }
        public int LocationId { get; set; }
        public  Location  Location { get; set; }
        public int Years { get; set; }
        public int Months { get; set; }
        public int RegistrationGroupId { get; set; }
        public SystemCodeDetail RegistrationGroup { get; set; }
        public int FinancialYearId { get; set; }

        public SystemCodeDetail FinancialYear { get; set; }
        public int EnumeratorId { get; set; }
        public Enumerator Enumerator { get; set; }
        public string HHdFirstName { get; set; }
        public string HHdMiddleName { get; set; }
        public string HHdSurname { get; set; }
        public string HHdNationalIdNo { get; set; }
        public int HHdSexId { get; set; }
        public SystemCodeDetail HHdSex { get; set; }
        public DateTime HHdDoB { get; set; }
        public string CgFirstName { get; set; }
        public string CgMiddleName { get; set; }
        public string CgSurname { get; set; }
        public string CgNationalIdNo { get; set; }
        public int CgSexId { get; set; }
        public SystemCodeDetail CgSex { get; set; }
        public DateTime CgDoB { get; set; }
        public int HouseholdMembers { get; set; }
        public int StatusId { get; set; }
        public string Village { get; set; }
        public string PhysicalAddress { get; set; }
        public DateTime DwellingStartDate { get; set; }
        public string NearestReligiousBuilding { get; set; }
        public string NearestSchool { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int? SyncEnumeratorId { get; set; }
        public  Enumerator SyncEnumerator { get; set; }

        public string DeviceId { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceManufacturer { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public string VersionNumber { get; set; }
        public string AppVersion { get; set; }
        public string AppBuild { get; set; }
        public string Platform { get; set; }
        public string Idiom { get; set; }
        public bool IsDevice { get; set; }

        public string GeoPosition => $"{Longitude},{Latitude}";
        public string Duration => $"{Years} Years,{Months} Months";
    }
}