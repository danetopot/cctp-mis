﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class District : CreateModifyFields
    {
        [Required]
        [Display(Name = "District Code")]
        [StringLength(20)]
        public string Code { get; set; }

        public virtual ICollection<CountyDistrict> CountyDistricts { get; set; }

        public GeoMaster GeoMaster { get; set; }

        [Display(Name = "Geo Master Id")]
        [Required]
        public int GeoMasterId { get; set; }

        [Display(Name = "District Id")]
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "District Name")]
        public string Name { get; set; }
    }
}