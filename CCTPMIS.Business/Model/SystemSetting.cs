﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class SystemSetting
    {
        public int Id { get; set; }

        [Required]
        public string Key { get; set; }

        [DataType(DataType.MultilineText), Required]
        public string Value { get; set; }
    }
}