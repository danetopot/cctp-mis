﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class ComValListingAccept
    {
        [Display(Name = "# ID")]
        public int Id { get; set; }
        [Display(Name = "Accepted By")]
        public int AcceptById { get; set; }
        public ApplicationUser AcceptBy { get; set; }
        [Display(Name = "Accepted Date")]
        public DateTime AcceptDate { get; set; }
        [Display(Name = "Approved By")]
        public int? AcceptApvById { get; set; }
        public ApplicationUser AcceptApvBy { get; set; }
        [Display(Name = "Approved Date")]
        public DateTime? AcceptApvDate { get; set; }
        [Display(Name = "Total Households")]
        public int ReceivedHHs { get; set; }
        [Display(Name = "Batch Name")]
        public string BatchName { get; set; }
        [Display(Name = "Constituency ")]
        public int ConstituencyId { get; set; }
        public Constituency Constituency { get; set; }
        [Display(Name = "Is Validated ")]
        public bool IsValidated { get; set; }
        [Display(Name = "Pending Validation ")]
        public int PendingValidation { get; set; }
        [Display(Name = "Targeting Plan ")]
        public int TargetPlanId { get; set; }
        public TargetPlan TargetPlan { get; set; }

        public ICollection<ComValListingPlanHH> ComValListingPlanHHs { get; set; }
    }

    

    
}