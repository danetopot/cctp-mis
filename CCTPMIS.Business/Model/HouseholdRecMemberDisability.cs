﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRecMemberDisability
    {
        [Key, Column(Order = 3)]
        [Display(Name = "Household Registration Member")]
        public string HouseholdRecMemberId { get; set; }
        [Key, Column(Order = 1)]
        [Display(Name = "Disability ")]
        public int DisabilityId { get; set; }
        [Key, Column(Order = 2)]
        [Display(Name = "Household Registration ")]
        public int HouseholdRecId { get; set; }
        public SystemCodeDetail Disability { get; set; }
        public HouseholdRecMember HouseholdRecMember { get; set; }

    }
}