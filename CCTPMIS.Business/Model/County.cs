﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using Newtonsoft.Json;

    public class County : CreateModifyFields
    {
        [Required]
        [Display(Name = "County Code", Description = "County code defined by Bureau of Statistics")]
        [StringLength(20)]
        public string Code { get; set; }

        [JsonIgnore]
        public ICollection<Constituency> Constituencies { get; set; }

        [JsonIgnore]
        public ICollection<CountyDistrict> CountyDistricts { get; set; }

        [JsonIgnore]
        public GeoMaster GeoMaster { get; set; }

        [Required]
        [Display(Name = "GeoMaster", Description = "The name of the Geo Master")]
        public int GeoMasterId { get; set; }

        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        //[RegularExpression("^[a-zA-Z0-9\\W]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        [Display(Name = "County", Description = "The name of the county")]
        public string Name { get; set; }
    }
}