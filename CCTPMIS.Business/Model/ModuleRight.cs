﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ModuleRight
    {
        [Required, StringLength(100), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Module Right ")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Module Module { get; set; }

        [Display(Name = "Module  ")]
        public byte ModuleId { get; set; }

        [Display(Name = "  Right ")]
        public int RightId { get; set; }

        [ForeignKey("RightId")]
        public SystemCodeDetail SystemCodeDetail { get; set; }
    }
}