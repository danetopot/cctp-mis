﻿namespace CCTPMIS.Business.Model
{
    public class temp_ExceptionActions
    {
        public int PaymentCycleId { get; set; }
        public int ProgrammeId { get; set; }
        public int HhId { get; set; }
        public int? PersonId { get; set; }
    }
}