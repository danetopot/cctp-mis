﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PaymentEnrolmentGroup
    {
        [ForeignKey("EnrolmentGroupId")]
        public SystemCodeDetail EnrolmentGroup { get; set; }

        [Key, Column(Order = 2)]
        [Display(Name = "Enrolment Group")]
        public int EnrolmentGroupId { get; set; }

        [Display(Name = "Payment Amount")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        public decimal PaymentAmount { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        public Programme Programme { get; set; }

        [Key, Column(Order = 3),]
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }
    }
}