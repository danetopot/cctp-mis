﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CreateApproveFields :CreateModifyFields
    {
        [Display(Name = "Approved By")]
        public int? ApvBy { get; set; }

        [ForeignKey("ApvBy")]
        public ApplicationUser ApvByUser { get; set; }

        [Display(Name = "Approved On")]
        public DateTime? ApvOn { get; set; }

         
    }
}