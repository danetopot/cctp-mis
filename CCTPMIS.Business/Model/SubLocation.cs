﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;


    public class SubLocation : CreateModifyFields
    {
        [StringLength(20)]
        [Required]
        [Display(Name = "Sub Location Code")]
        public string Code { get; set; }

        [JsonIgnore]
        public Constituency Constituency { get; set; }

        [Display(Name = "Constituency")]
        public int ConstituencyId { get; set; }

        [Display(Name = "Sub Location")]
        public int Id { get; set; }

        [ForeignKey("LocalityId")]
        public SystemCodeDetail Locality { get; set; }

        [Display(Name = "Locality")]
        public int LocalityId { get; set; }

        [JsonIgnore]
        public Location Location { get; set; }

        [Display(Name = "Location")]
        public int LocationId { get; set; }

        [StringLength(30)]
        [Required]
        [Display(Name = "Sub Location Name")]
        public string Name { get; set; }
    }


     

}