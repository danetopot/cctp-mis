﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class Complaint : CreateApproveFields
    {
        public int Id { get; set; }
        [DisplayName("Programme")]
        public byte ProgrammeId { get; set; }
        public Programme Programme { get; set; }
        [DisplayName("Programme Number")]
        public string ProgrammeNo => $"{Programme?.BeneProgNoPrefix}{HhEnrolmentId:D6}";
        [DisplayName("Household Enrolment ID")]
        public int HhEnrolmentId { get; set; }
        public HouseholdEnrolment HhEnrolment { get; set; }
        [DisplayName("Complaint Type")]
        public int? ComplaintTypeId { get; set; }
        public CaseCategory ComplaintType { get; set; }

        [DisplayName("Complaint Escalation Level")]
        public int? ComplaintLevelId { get; set; }
        public SystemCodeDetail ComplaintLevel { get; set; }


        [DisplayName("Serial No.")]
        public int? SerialNo { get; set; }
        [DisplayName("Reported By Name")]
        public string ReportedByName { get; set; }
        [DisplayName("Reported By National ID No")]
        public string ReportedByIdNo { get; set; }
        [DisplayName("Reported By Phone Number")]
        public string ReportedByPhone { get; set; }
        [DisplayName("Reported By Sex")]
        public int? ReportedBySexId { get; set; }
        public SystemCodeDetail ReportedBySex { get; set; }

        [DisplayName("Reported By")]
        public int ReportedByTypeId { get; set; }
        public SystemCodeDetail ReportedByType { get; set; }
        [DisplayName("Complaint Date")]
        public DateTime? ComplaintDate { get; set; }

        public DateTime ReceivedOn { get; set; }
        [DisplayName("Resolved Date")]
        public DateTime? ResolvedOn { get; set; }

        [DisplayName("Status")]
        public int ComplaintStatusId { get; set; }

        public SystemCodeDetail ComplaintStatus { get; set; }
        [DisplayName("Resolved By")]
        public int? ResolvedBy { get; set; }

        [ForeignKey("ResolvedBy")]
        public ApplicationUser ResolvedByUser { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Closed Date")]
        public DateTime? ClosedOn { get; set; }

        [DisplayName("Closed By")]
        public int? ClosedBy { get; set; }

        [ForeignKey("ClosedBy")]
        public ApplicationUser ClosedByUser { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Verified Date")]
        public DateTime? VerifiedOn { get; set; }

        [DisplayName("Verified By")]
        public int? VerifiedBy { get; set; }

        [ForeignKey("VerifiedBy")]
        public ApplicationUser VerifiedByUser { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Escalated Date")]
        public DateTime? EscalatedOn { get; set; }

        [DisplayName("Escalated By")]
        public int? EscalatedBy { get; set; }

        [ForeignKey("EscalatedBy")]
        public ApplicationUser EscalatedByUser { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Escalated2 Date")]
        public DateTime? Escalated2On { get; set; }

        [DisplayName("Escalated2 By")]
        public int? Escalated2By { get; set; }

        [ForeignKey("Escalated2By")]
        public ApplicationUser Escalated2ByUser { get; set; }

        [DisplayName("Received By")]
        public string ReceivedBy { get; set; }
        public string Designation { get; set; }
        [DisplayName("Source")]
        public int? SourceId { get; set; }

        public int ConstituencyId { get; set; }

        public Constituency Constituency { get; set; }

        public SystemCodeDetail Source { get; set; }
        public ICollection<ComplaintNote> ComplaintNotes { get; set; }
        public ICollection<ComplaintDocument> ComplaintDocuments { get; set; }
    }
}
