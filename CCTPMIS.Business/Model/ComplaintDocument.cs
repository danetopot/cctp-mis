﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class ComplaintDocument
    {

        public int Id { get; set; }
        [DisplayName("Complaint ")]
        public int ComplaintId { get; set; }

        [DisplayName("File Name")]
        public string FileName { get; set; }

         
        [DisplayName("Added By")]
        public int CreatedById { get; set; }

        [ForeignKey("CreatedById")]
        public ApplicationUser CreatedBy { get; set; }

        [DisplayName("Category")]
        public int CategoryId { get; set; }
        public SystemCodeDetail Category { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Date Added")]
        public DateTime? CreatedOn { get; set; }

        public string FilePath { get; set; }
         
    }
}