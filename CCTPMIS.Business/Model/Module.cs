﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Module
    {
        public ICollection<Module> ChildModules { get; set; }

        [Required, StringLength(100), DataType(DataType.MultilineText)]
        [Display(Name = "Description ")]
        public string Description { get; set; }

        [Display(Name = "Module #")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        public ICollection<ModuleRight> ModuleRights { get; set; }

        [Required, StringLength(30)]
        [Display(Name = " Module Name")]
        public string Name { get; set; }

        [ForeignKey("ParentModuleId")]
        public Module ParentModule { get; set; }

        [Display(Name = "Parent Module ")]
        public byte? ParentModuleId { get; set; }
    }
}