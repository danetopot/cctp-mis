﻿namespace CCTPMIS.Business.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using Newtonsoft.Json;

    public class Psp : CreateModifyFields
    {
        [Display(Name = "PSP Code"), StringLength(20), Index(IsClustered = false, IsUnique = true)]
        public string Code { get; set; }

        [Display(Name = "PSP ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [Display(Name = "Active Status")]
        public bool IsActive { get; set; }

        [Display(Name = "PSP Name")]
        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Primary Contact"), StringLength(100), Required]
        public string PrimaryContact { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        [Display(Name = "Primary Email Address")]
        public string PrimaryEmail { get; set; }

        [Display(Name = "Primary Telephone I"), StringLength(20), Required]
        public string PrimaryTel1 { get; set; }

        [Display(Name = "Primary Telephone II"), StringLength(20)]
        public string PrimaryTel2 { get; set; }

        [JsonIgnore]
        public ICollection<PspBranch> PspBranches { get; set; }

        [JsonIgnore]
        public ApplicationUser User { get; set; }

        [Display(Name = "Associated User Account")]
        public int UserId { get; set; }
    }
}