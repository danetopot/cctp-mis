﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRegAccept
    {
        public int Id { get; set; }
        [Display(Name = "Accepted By")]
        public int AcceptById { get; set; }
        [Display(Name = "Accepted Date")]
        public DateTime AcceptDate { get; set; }
        [Display(Name = "Accept Approved By")]
        public int? AcceptApvById { get; set; }
        [Display(Name = "Accept Approved Date")]
        public DateTime? AcceptApvDate { get; set; }
        [Display(Name = "Received HHs")]
        public int ReceivedHHs { get; set; }
        [Display(Name = "Batch Name")]
        public string BatchName { get; set; }
        [Display(Name = "Constituency")]
        public int ConstituencyId { get; set; }
        [Display(Name = "Targeting Plan")]
        public int TargetPlanId { get; set; }
        [Display(Name = "Pending Validation")]
        public int PendingValidation { get; set; }
        [Display(Name = "Is Validated")]
        public bool IsValidated { get; set; }
         
        public ApplicationUser AcceptBy { get; set; }
        public ApplicationUser AcceptApvBy { get; set; }
        public Constituency Constituency { get; set; }
        public TargetPlan TargetPlan { get; set; }

    }
}