﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ApplicationGroupRole
    {
        [Key, Column(Order =1)]
        public int ApplicationGroupId { get; set; }
        [Key, Column(Order = 2)]
        public int ApplicationRoleId { get; set; }

        public ApplicationGroup ApplicationGroup { get; set; }

        public ApplicationRole ApplicationRole { get; set; }
    }
}