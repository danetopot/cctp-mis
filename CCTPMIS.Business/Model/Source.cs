﻿namespace CCTPMIS.Business.Model
{
    public class Source
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}