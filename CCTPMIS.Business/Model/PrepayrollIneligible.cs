﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PrepayrollIneligible
    {
        [Display(Name = "Is Actioned")]
        public bool Actioned { get; set; }

        [Display(Name = "Actioned Approved By")]
        public int? ActionedApvBy { get; set; }

        [ForeignKey("ActionedApvBy")]
        public ApplicationUser ActionedApvByUser { get; set; }

        [Display(Name = "Actioned Approved On")]
        public DateTime? ActionedApvOn { get; set; }

        [Display(Name = "Actioned By")]
        public int? ActionedBy { get; set; }

        [ForeignKey("ActionedBy")]
        public ApplicationUser ActionedByUser { get; set; }

        [Display(Name = "Actioned On")]
        public DateTime? ActionedOn { get; set; }

        [ForeignKey("ExceptionTypeId")]
        public SystemCodeDetail ExceptionType { get; set; }

        [Key, Column(Order = 5)]
        public int ExceptionTypeId { get; set; }

        [Key, Column(Order = 3)]
        [Display(Name = "House Hold")]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        // [Key, Column(Order = 4)]
        // [Display(Name = "Member Id")]
        // public int MemberId { get; set; }
        [StringLength(128)]
        [Required]
        public string Notes { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        public PaymentCycleDetail PaymentCycleDetail { get; set; }

        [Display(Name = "Payment Cycle")]
        [Key, Column(Order = 1)]
        public int PaymentCycleId { get; set; }

        public Programme Programme { get; set; }

        [Key, Column(Order = 2)]
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }


        public Payment Payment { get; set; }

        [ForeignKey("PaymentCycleId,ProgrammeId,HhId")]
        public Prepayroll Prepayroll { get; set; }

    }
}