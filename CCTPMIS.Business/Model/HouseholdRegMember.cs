﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class HouseholdRegMember
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Key, Column(Order = 1)]
        [Display(Name = "Household Registration ")]
        public int HouseholdRegId { get; set; }
        [Key, Column(Order = 3)]
        [Display(Name = "Member ")]
        public string MemberId { get; set; }
        [Display(Name = "Care Giver ")]
        public string CareGiverId { get; set; }
        [Display(Name = "Spouse")]
        public string SpouseId { get; set; }
        [Display(Name = "First Name ")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name ")]
        public string MiddleName { get; set; }
        [Display(Name = "Surname ")]
        public string Surname { get; set; }

        public string FullName => $"{FirstName} {MiddleName} {Surname}";
        [Display(Name = "Identification Type")]
        public int IdentificationTypeId { get; set; }
        [Display(Name = "Identification Number")]
        public string IdentificationNumber { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Relationship ")]
        public int RelationshipId { get; set; }
        [Display(Name = "Spouse In Household")]
        public int? SpouseInHouseholdId { get; set; }
        [Display(Name = "Sex ")]
        public int SexId { get; set; }
        [Display(Name = "Date Of Birth ")]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Marital Status")]
        public int MaritalStatusId { get; set; }
        [Display(Name = "Chronic Illness Status ")]
        public int? ChronicIllnessStatusId { get; set; }
        [Display(Name = "Disability Care Status ")]
        public int? DisabilityCareStatusId { get; set; }
        [Display(Name = "Disability Type ")]
        public int? DisabilityTypeId { get; set; }
        [Display(Name = "Education Level ")]
        public int? EducationLevelId { get; set; }
        [Display(Name = "Father Alive Status ")]
        public int? FatherAliveStatusId { get; set; }
        [Display(Name = "Formal Job Ngo ")]
        public int? FormalJobNgoId { get; set; }
        [Display(Name = "Learning Status ")]
        public int? LearningStatusId { get; set; }
        [Display(Name = "Work Type ")]
        public int? WorkTypeId { get; set; }
        [Display(Name = "Is Mother Alive")]
        public int? MotherAliveStatusId { get; set; }


        public SystemCodeDetail IdentificationType { get; set; }
        public SystemCodeDetail Relationship { get; set; }
        public SystemCodeDetail SpouseInHousehold { get; set; }
        public SystemCodeDetail Sex { get; set; }
        public SystemCodeDetail MaritalStatus { get; set; }
        public SystemCodeDetail ChronicIllnessStatus { get; set; }
        public SystemCodeDetail DisabilityCareStatus { get; set; }
        public SystemCodeDetail DisabilityType { get; set; }
        public SystemCodeDetail EducationLevel { get; set; }
        public SystemCodeDetail FatherAliveStatus { get; set; }
        public SystemCodeDetail FormalJobNgo { get; set; }
        public SystemCodeDetail LearningStatus { get; set; }
        public SystemCodeDetail WorkType { get; set; }
        public SystemCodeDetail MotherAliveStatus { get; set; }


    }
}