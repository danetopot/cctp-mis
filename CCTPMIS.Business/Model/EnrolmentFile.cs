﻿using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class EnrolmentFile
    {
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "ID")]
        public int Id { get; set; }
    }
}