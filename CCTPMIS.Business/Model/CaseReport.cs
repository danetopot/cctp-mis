﻿
namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class CaseReport : CreateModifyFields
    {
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(20)]
        [Display(Name = "Serial No.")]
        [RegularExpression("^[A-Za-z0-9-]*$", ErrorMessage = "Invalid!")]
        public string SerialNo { get; set; }

        [ForeignKey("ProgrammeId")]
        public Programme Programme { get; set; }

        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        public string BeneProgNoPrefix { get; set; }

        [Display(Name = "Prog. No.")]
        public int? ProgrammeNo { get; set; }

        [Display(Name = "Relation")]
        public int? MemberRoleId { get; set; }

        [ForeignKey("MemberRoleId")]
        public SystemCodeDetail MemberRole { get; set; }

        [Display(Name = "Person")]
        public int? PersonId { get; set; }

        [ForeignKey("PersonId")]
        public Person Person { get; set; }

        [StringLength(100)]
        [RegularExpression("^[A-Za-z0-9 ]*$", ErrorMessage = "Invalid Name")]
        [Display(Name = "Names")]
        public string Names { get; set; }

        [Display(Name = "Sex")]
        public int? SexId { get; set; }

        [ForeignKey("SexId")]
        public SystemCodeDetail Sex { get; set; }

        [StringLength(30)]
        [Display(Name = "ID No.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid!")]
        public string IdNo { get; set; }

        [StringLength(20)]
        [Display(Name = "Mobile No.")]
        [RegularExpression("^[0-9+]*$", ErrorMessage = "Invalid!")]
        public string MobileNo { get; set; }

        [ForeignKey("CaseIssueId")]
        public CaseIssue CaseIssue { get; set; }

        [Display(Name = "Case Type")]
        public int CaseIssueId { get; set; }

        [StringLength(4000)]
        [Required]
        [Display(Name = "Notes")]
        [RegularExpression("^[A-Za-z0-9- ]*$", ErrorMessage = "Invalid!")]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [ForeignKey("CountyId")]
        public County County { get; set; }

        [Display(Name = "County")]
        public int? CountyId { get; set; }

        [ForeignKey("ConstituencyId")]
        public Constituency Constituency { get; set; }

        [Display(Name = "Constituency")]
        public int? ConstituencyId { get; set; }

        [ForeignKey("SubLocationId")]
        public SubLocation SubLocation { get; set; }

        [Display(Name = "SubLocation")]
        public int? SubLocationId { get; set; }

        [StringLength(20)]
        [Display(Name = "Received By")]
        [RegularExpression("^[A-Za-z- ]*$", ErrorMessage = "Invalid!")]
        public string ReceivedBy { get; set; }

        [StringLength(20)]
        [Display(Name = "Designation")]
        [RegularExpression("^[A-Za-z0-9-]*$", ErrorMessage = "Invalid!")]
        public string ReceivedByDesig { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Received On")]
        public DateTime ReceivedOn { get; set; }

        [ForeignKey("FileCreationId")]
        public FileCreation FileCreation { get; set; }

        [Display(Name = "File Creation")]
        public int? FileCreationId { get; set; }

        [Display(Name = "Latest Action")]
        public int CaseActionId { get; set; }

        [ForeignKey("CaseActionId")]
        public SystemCodeDetail CaseAction { get; set; }

        [Display(Name = "Next Workflow Stage")]
        public int CaseFlowId { get; set; }

        [ForeignKey("CaseFlowId")]
        public SystemCodeDetail CaseFlow { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [ForeignKey("StatusId")]
        public SystemCodeDetail Status { get; set; }

        public virtual CaseReportHistory CaseReportHistory { get; set; }

        public string BeneProgNo { get { return this.BeneProgNoPrefix + this.ProgrammeNo; } }
    }

}