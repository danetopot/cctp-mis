﻿namespace CCTPMIS.Business.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PaymentAdjustment
    {
        [Display(Name = "Adjustment Amount")]
        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        public decimal AdjustmentAmount { get; set; }

        [ForeignKey("AdjustmentTypeId")]
        public SystemCodeDetail AdjustmentType { get; set; }

        [Key, Column(Order = 4)]
        [Display(Name = "Adjustment Type")]
        public int AdjustmentTypeId { get; set; }

        [DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:KES #.00}")]
        [Display(Name = "Amount Adjustment")]
        public decimal AmountAdjusted { get; set; } = 0;

        [Display(Name = "Created By")]
        public int CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        public ApplicationUser CreatedByUser { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Key, Column(Order = 3)]
        [Display(Name = "House Hold")]
        public int HhId { get; set; }

        [Display(Name = "Notes")]
        [StringLength(128)]
        public string Notes { get; set; }

        public PaymentCycle PaymentCycle { get; set; }

        [Key, Column(Order = 1)]
        [Display(Name = "Payment Cycle")]
        public int PaymentCycleId { get; set; }

        public Programme Programme { get; set; }

        [Key, Column(Order = 2)]
        [Display(Name = "Programme")]
        public byte ProgrammeId { get; set; }

        [Display(Name = "Reference Number")]
        [StringLength(50)]
        public string RefNo { get; set; }
    }
}