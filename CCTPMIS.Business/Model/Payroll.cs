﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{
    public class Payroll
    {
        [Key,Column(Order = 1)]
        [Display(Name = "Payment Cycle ")]
        public int PaymentCycleId { get; set; }
        [Key, Column(Order = 2)]
        [Display(Name = "Programme ")]
        public byte ProgrammeId { get; set; }
        [Key, Column(Order = 3)]
        [Display(Name = "Household ")]
        public int HhId { get; set; }
        [Display(Name = "Payment Amount ")]
        public decimal PaymentAmount { get; set; }
        

        public PaymentCycle PaymentCycle { get; set; }

        public Programme Programme { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }


        [ForeignKey("PaymentCycleId,ProgrammeId,HhId")]
        public Prepayroll Prepayroll { get; set; }

    }
}