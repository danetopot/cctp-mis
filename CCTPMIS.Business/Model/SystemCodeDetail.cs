﻿using System.Collections.Generic;

namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class SystemCodeDetail : CreateModifyFields
    {
        [Required, StringLength(20)]
        [Display(Name = "Parameter Code")]
        [Index("IX_SystemCodeDetail_Code", IsClustered = false)]
        public string Code { get; set; }

        [Required, StringLength(100), DataType(DataType.MultilineText)]
        [Index("IX_SystemCodeDetail_Description", IsClustered = false)]
        [Display(Name = "Parameter Valur")]
        public string Description { get; set; }

        [Display(Name = "Parameter")]
        public int Id { get; set; }

        [Display(Name = "Order Number"), DataType(DataType.Text)]
        public byte OrderNo { get; set; }

        public SystemCode SystemCode { get; set; }

        [Display(Name = "General Parameter Id")]
        public int SystemCodeId { get; set; }

        public int IsActive { get; set; }

        //public ICollection<CaseCategory> CaseCategory { get; set; }
    }
}