﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class PaymentCardBiometrics
    {
        [ForeignKey("BenePaymentCardId")]
        public BeneficiaryPaymentCard BeneficiaryPaymentCard { get; set; }

        [Key]
        public int BenePaymentCardId { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciLI { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciLMF { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciLP { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciLRF { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciLT { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciRI { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciRMF { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciRP { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciRRF { get; set; }

        // ReSharper disable once InconsistentNaming
        [Required]
        public byte[] PriReciRT { get; set; }

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciLI { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciLMF { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciLP { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciLRF { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciLT { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciRI { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciRMF { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciRP { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciRRF { get; set; } = null;

        // ReSharper disable once InconsistentNaming
        public byte[] SecReciRT { get; set; } = null;
    }
}