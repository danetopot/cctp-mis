﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CCTPMIS.Business.Model
{
    public class IprsCache
    {
        
        public long Serial_Number { get; set; }
        public string First_Name { get; set; }
        public string Surname { get; set; }
        public string Middle_Name { get; set; }
        [Key]
        public string ID_Number { get; set; }
        public string Gender { get; set; }
        public string Date_of_Birth { get; set; }
        public string Date_of_Issue { get; set; }
        public string Place_of_Birth { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public DateTime DateCached { get; set; }       
    }
}