﻿namespace CCTPMIS.Business.Model
{
    public class temp_Payroll
    {
        public int PaymentCycleId { get; set; }
        public int ProgrammeId { get; set; }
        public int HhId { get; set; }
        public decimal PaymentAmount { get; set; }
    }
}