﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCTPMIS.Business.Model
{

    public class MonitoringReportCategory : CreateApproveFields
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    
    }
    public class MonitoringReport : CreateApproveFields
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Category")]
        public int? MonitoringReportCategoryId { get; set; }
        public MonitoringReportCategory MonitoringReportCategory { get; set; }
        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        public SystemCodeDetail Status { get; set; }
        public string FileName { get; set; }
        public string  Name { get; set; }
        public string FilePath { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }

        [Display(Name = "Approved By")]
        public int? VerifiedBy { get; set; }

        [ForeignKey("VerifiedBy")]
        public ApplicationUser VerifiedByUser { get; set; }

        [Display(Name = "Verified On")]
        public DateTime? VerifiedOn { get; set; }
    }

    public class CaseCategory : CreateApproveFields
    {


        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Type of Case")]
        public int CaseTypeId { get; set; }
        public SystemCodeDetail CaseType { get; set; }

        [Display(Name = "Case Category Name")]
        public string Name { get; set; }

        [Display(Name = "Case Category Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Case Category Code")]
        public string Code { get; set; }

        [Display(Name = "Documents To Attach")]
        [DataType(DataType.MultilineText)]
        public string DocumentsToAttach { get; set; }

        [Display(Name = "Reporting Process")]
        [DataType(DataType.MultilineText)]
        public string ReportingProcess { get; set; }


        [Display(Name = "SLA Duration of Service")]
        public int Timeline { get; set; }

        [Display(Name = "Type of Days")]
        public int TypeofDayId { get; set; }

        public SystemCodeDetail TypeofDay { get; set; }

    }
}