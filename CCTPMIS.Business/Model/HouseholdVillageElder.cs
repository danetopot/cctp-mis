﻿namespace CCTPMIS.Business.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class HouseholdVillageElder
    {
        [Key]
        public int HhId { get; set; }

        [ForeignKey("HhId")]
        public Household Household { get; set; }

        [StringLength(20)]
        [Required]
        public string MobileNo { get; set; }

        public bool MobileNoConfirmed { get; set; } = false;

        [StringLength(100)]
        [Required]
        public string Name { get; set; }
    }
}