﻿namespace CCTPMIS.Business.Interfaces
{
    public interface IEntity : IModifiableEntity
    {
        object Id { get; set; }

        /*   DateTime CreatedDate { get; set; }
           DateTime? ModifiedDate { get; set; }
           int CreatedBy { get; set; }
           int? ModifiedBy { get; set; }
           byte[] Version { get; set; }
           */
    }

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }
}