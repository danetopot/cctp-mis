﻿namespace CCTPMIS.Business.Interfaces
{
    using System;

    public class ParameterEntity
    {
        public Tuple<string, object> ParameterTuple { get; set; }
    }
}