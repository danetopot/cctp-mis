﻿
SELECT T1.*, T2.[Description] AS BeneficiaryType, T3.[Description] AS PrimaryRecipient, T4.[Description] AS SecondaryRecipient  FROM Programme T1
INNER JOIN SystemCodeDetail T2 ON T1.BeneficiaryTypeId = T2.Id
INNER JOIN SystemCodeDetail T3 ON T1.PrimaryRecipientId = T3.Id
LEFT JOIN SystemCodeDetail T4 ON T1.SecondaryRecipientId =T4.Id
---2-------

SELECT T1.Id, T1.BeneProgNoPrefix, T1.Code, T1.[Name], T2.[Description] AS BeneficiaryType, T3.[Description] AS PrimaryRecipient, 
T4.[Description] AS SecondaryRecipient, T5.[Description] AS PaymentFrequency, T1.EntitlementAmount  FROM Programme T1
INNER JOIN SystemCodeDetail T2 ON T1.BeneficiaryTypeId = T2.Id
INNER JOIN SystemCodeDetail T3 ON T1.PrimaryRecipientId = T3.Id
LEFT JOIN SystemCodeDetail T4 ON T1.SecondaryRecipientId =T4.Id
LEFT JOIN SystemCodeDetail T5 ON T1.PaymentFrequencyId =T5.Id
ORDER BY T1.BeneProgNoPrefix




SELECT COUNT(T1.Id) FROM Household T1 WHERE T1.SourceId = 2



SELECT COUNT(T1.Id) AS Households, T1.RegGroupId, T2.Description, T1.CreatedOn FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  WHERE t1.RegGroupId>105
GROUP BY T1.RegGroupId, T2.Description, T1.CreatedOn
ORDER BY t1.CreatedOn


SELECT COUNT(T1.Id) AS Households FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  WHERE t1.RegGroupId>105

SELECT COUNT(T1.Id) AS Households, t1.ProgrammeId FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  WHERE t1.RegGroupId>105
GROUP BY ProgrammeId

 

 

SELECT COUNT(T1.Id) AS Households, T1.ProgrammeId, T3.Name FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  
INNER JOIN  Programme T3 ON T3.Id = T1.ProgrammeId
WHERE t1.RegGroupId>105
GROUP BY ProgrammeId, Name



SELECT COUNT(T1.Id) AS Households, T1.ProgrammeId, T3.Code 'PROGRAMME', T4.Code 'STATUS' FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  
INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id  
INNER JOIN  Programme T3 ON T3.Id = T1.ProgrammeId
WHERE T1.RegGroupId>105
GROUP BY ProgrammeId, T3.Code, T4.Code
ORDER BY t3.Code







;with x as (
SELECT t1.* FROM Household  T1 
 LEFT JOIN WITHCAREGIVERS T2 ON T1.Id = T2.Hhid  
 where t2.HhId is null AND T1.RegGroupId>105
)

select T4.Code 'STATUS', T3.Code 'PROGRAMME', COUNT( distinct x.Id) 'Households' from x 
INNER JOIN SystemCodeDetail T4 ON x.StatusId = T4.Id  
INNER JOIN Programme T3 ON X.ProgrammeId = T3.Id
GROUP BY T4.Code,T3.Code














