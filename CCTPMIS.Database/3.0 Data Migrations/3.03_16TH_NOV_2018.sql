﻿


select  count(T3.Id) MEMBERS, T3.RefId  from Household T1 
inner join HouseholdMember T2  ON T1.Id = T2.HhId
INNER JOIN Person T3 ON T2.PersonId=T3.Id
INNER JOIN SystemCodeDetail T4 ON T2.MemberRoleId = T4.Id  INNER JOIN SystemCodeDetail T5 ON T2.RelationshipId = T5.Id  
INNER JOIN SystemCodeDetail T6 ON T1.StatusId = T6.Id  INNER JOIN Programme T7 ON T1.ProgrammeId = T7.Id  and t1.RegGroupId>105 
GROUP BY T3.RefId
ORDER BY MEMBERS DESC








;WITH x AS (
SELECT DISTINCT  t1.* FROM Household  T1 
 LEFT JOIN WITHCAREGIVERS T2 ON T1.Id = T2.Hhid  
 WHERE t2.HhId IS NULL AND T1.RegGroupId>105
)

SELECT T4.Code 'STATUS', T3.Code 'PROGRAMME', COUNT( DISTINCT x.Id) 'Households' FROM x 
INNER JOIN SystemCodeDetail T4 ON x.StatusId = T4.Id  
INNER JOIN Programme T3 ON X.ProgrammeId = T3.Id
GROUP BY T4.Code,T3.Code



SELECT DISTINCT  t1.* FROM Household  T1    
 LEFT JOIN WITHCAREGIVERS T2 ON T1.Id = T2.Hhid  AND T1.RegGroupId>105 AND T1.ProgrammeId=5
 WHERE t2.HhId IS NULL 
 ORDER BY ProgrammeId DESC


SELECT * FROM Household T1 INNER JOIN HouseholdMember T2 ON T1.Id = T2.HhId	



--D:\DBFiles

SELECT COUNT(1) FROM PaymentCardBiometrics







SELECT COUNT(T1.Id) from Household T1 
INNER JOIN (
SELECT COUNT(PersonId) 'CGs', HhId from HouseholdMember T1  INNER JOIN SystemCodeDetail T2 ON T2.Id  = T1.MemberRoleId  AND T2.Code='BENEFICIARY'
 GROUP BY HhId
) T2 ON T1.Id = T2.HhId --AND CGs=0
AND T1.RegGroupId>105 and t1.ProgrammeId=5 



SELECT COUNT(T1.Id) AS Households FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  WHERE t1.RegGroupId>105

SELECT COUNT(T1.Id) AS Households, t1.ProgrammeId FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  WHERE t1.RegGroupId>105
GROUP BY ProgrammeId

 

 
SELECT COUNT(T1.Id) AS Households, T1.ProgrammeId, T3.Code 'PROGRAMME', T4.Code 'STATUS' FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  
INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id  
INNER JOIN  Programme T3 ON T3.Id = T1.ProgrammeId
WHERE T1.RegGroupId>105
GROUP BY ProgrammeId, T3.Code, T4.Code
ORDER BY t3.Code





;with x as (
SELECT t1.* FROM Household  T1 
 LEFT JOIN WITHCAREGIVERS T2 ON T1.Id = T2.Hhid  
 where t2.HhId is null AND T1.RegGroupId>105
)

select T4.Code 'STATUS', T3.Code 'PROGRAMME', COUNT( distinct x.Id) 'Households' from x 
INNER JOIN SystemCodeDetail T4 ON x.StatusId = T4.Id  
INNER JOIN Programme T3 ON X.ProgrammeId = T3.Id
GROUP BY T4.Code,T3.Code








;with y as (select *  from Person where Surname!=''  and FirstName!='' and MiddleName!='')
, Z as
( select ROW_NUMBER() over( partition by FirstName, MiddleName,SURNAME,SexId,DoB  order by id desc) as RN,* FROM y )
select * from Z where RN>1 order by RN desc


SELECT * FROM Person WHERE NationalIdNo='14469806'
















SELECT * FROM	IPRS_CTOVC_CG UNION
;WITH X AS  (
SELECT  DISTINCT  Programme, IDNumber, FirstName, MiddleName, Surname, Gender, DateOfBirth  FROM (
SELECT * FROM	IPRS_CTOVC_CG UNION
SELECT * FROM	IPRS_OPCT_Bene UNION
SELECT * FROM	IPRS_OPCT_CG UNION
SELECT * FROM	IPRS_PwSD_Bene UNION
SELECT * FROM 	IPRS_PwSD_CG  ) T1 WHERE IDNumber IS NOT NULL
), 

Z AS( SELECT ROW_NUMBER() OVER( PARTITION BY  IDNUMBER  ORDER BY IDNUMBER DESC) AS RN,* FROM X )
SELECT * INTO CCTPMISPAYMENTS.DBO.IPRSCACHE FROM Z WHERE RN=1



SELECT    Programme, IDNumber, FirstName, MiddleName, Surname, Gender, DateOfBirth  FROM (
SELECT * FROM	IPRS_CTOVC_CG UNION
SELECT * FROM	IPRS_OPCT_Bene UNION
SELECT * FROM	IPRS_OPCT_CG UNION
SELECT * FROM	IPRS_PwSD_Bene UNION
SELECT * FROM 	IPRS_PwSD_CG  ) T1 WHERE IDNumber ='36403093'






SELECT * FROM	IPRS_OPCT_Bene UNION
SELECT * FROM	IPRS_OPCT_CG UNION
SELECT * FROM	IPRS_PwSD_Bene UNION
SELECT * FROM 	IPRS_PwSD_CG  



ALTER DATABASE [CCTPMISPayments] SET RECOVERY SIMPLE
 DBCC SHRINKFILE ('CCTP-MIS_log', 1)

 
USE [LegacyDB]
ALTER DATABASE [LegacyDB] SET RECOVERY SIMPLE
DBCC SHRINKFILE (N'LegacyDB_log' , 0, TRUNCATEONLY)
GO


DBCC SHRINKFILE (N'LegacyDB_log' , 0, TRUNCATEONLY)
GO

USE CCTPMISPAYMENTS
GO

SELECT * FROM PERSON WHERE  DoB>GETDATE()





USE CCTPMISPayments
GO


--; with x as (
SELECT 
   T2.HhId
  ,CONCAT(T3.FirstName, ' ' ,CASE WHEN(T3.MiddleName<>'') THEN T3.MiddleName ELSE '' END, ' ',T3.Surname) AS PERSON_NAME  
  ,T3.DoB AS PERSON_DOB
  ,T3.NationalIdNo AS PERSON_NATIONALIDNO

  ,T4.Code AS 'MEMBER ROLE'
  ,T5.FirstName+CASE WHEN(T5.MiddleName<>'') THEN ' ' ELSE '' END+T5.MiddleName+CASE WHEN(T5.Surname<>'') THEN ' ' ELSE '' END+T5.Surname AS IPRS_Name  
  ,T5.IdNumber AS IPRS_NATIONALID
  ,T5.Gender AS IPRS_SEX
  ,T5.DateOfBirth AS IPRS_DOB
  ,T7.Code AS 'HOUSEHOLD STATUS'
  ,CONVERT(BIT,ISNULL(T5.IDNumber,0)) AS IDNO_Exists  
  ,CASE WHEN(T3.FirstName IN(T5.FirstName,T5.MiddleName,T5.Surname)) THEN 1 ELSE 0 END AS FirstNameExists  
  ,CASE WHEN(T3.MiddleName IN(T5.FirstName,T5.MiddleName,T5.Surname)) THEN 1 ELSE 0 END AS MiddleNameExists  
  ,CASE WHEN(T3.Surname IN(T5.FirstName,T5.MiddleName,T5.Surname)) THEN 1 ELSE 0 END AS SurnameExists  
  ,CASE WHEN(T3.DoB=T5.DateOfBirth) THEN 1 ELSE 0 END AS DoBMatch  
  ,CASE WHEN(YEAR(T3.DoB)=YEAR(T5.DateOfBirth)) THEN 1 ELSE 0 END AS DoBYearMatch  
  ,CASE WHEN(T6.Code=T5.Gender) THEN 1 ELSE 0 END AS SexMatch   
 , CASE WHEN (ISNUMERIC(T3.NATIONALIDnO)=0) THEN 0 ELSE 1 END AS VALIDID
   FROM Household T1 
INNER JOIN HouseholdMember T2 ON T1.Id = T2.HhId AND T1.RegGroupId>105 AND T1.ProgrammeId=5
INNER JOIN Person T3 ON T2.PersonId = T3.Id
INNER JOIN SystemCodeDetail T6 ON T3.SexId = T6.Id  
INNER JOIN SystemCodeDetail T4 ON T2.MemberRoleId = T4.Id  AND T4.Code IN ('BENEFICIARY','CAREGIVER')
INNER JOIN SystemCodeDetail T7 ON T1.StatusId = T7.Id
LEFT OUTER JOIN (SELECT DISTINCT * FROM IPRSCACHE) T5 ON RTRIM(LTRIM(T3.NationalIdNo)) =  RTRIM(LTRIM(T5.IDNumber))  

ORDER BY t1.Id

