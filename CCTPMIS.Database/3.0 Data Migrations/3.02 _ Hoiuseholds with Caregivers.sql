﻿DECLARE @BeneficiaryRoleId INT 
DECLARE @CaregiverRoleId INT
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT
DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SELECT COUNT(T1.Id) 'Households',  T5.Code 'Programme' from Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  AND T1.RegGroupId>105
INNER JOIN HouseholdMember T3 ON T1.Id = T3.HhId 
INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id  AND T4.Id = @CgRoleId
INNER JOIN  Programme T5 ON  T1.ProgrammeId =T5.Id 
GROUP BY  T5.Code





SELECT T1.Code  FROM  Programme T1
LEFT JOIN Household T2 ON T1.Id = T2.ProgrammeId
LEFT JOIN HouseholdMember T3 ON T2.Id = T3.HhId
LEFT JOIN (SELECT HHID, COUNT(HHID) 'Members' FROM HouseholdMember GROUP BY HhId) T4 ON T4.HhId = T2.Id



;with x as (
SELECT T1.HHID, COUNT(T1.Id) 'Members' FROM HouseholdMember T1
INNER JOIN Household T2 ON T1.HhId = T2.Id AND T2.RegGroupId>105
 GROUP BY T1.HhId
)
select * from x ORDER BY Members ASC--where Members = 1



-----------------------------------------------

DECLARE @BeneficiaryRoleId INT 
DECLARE @CaregiverRoleId INT
DECLARE @SysCode VARCHAR(30)
DECLARE @SysDetailCode VARCHAR(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT
DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode




SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  
 T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

;WITH X AS (
SELECT T3.*,T6.FirstName,T6.MiddleName,T6.Surname,T6.SexId,T6.DoB from Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  AND T1.RegGroupId>105
INNER JOIN HouseholdMember T3 ON T1.Id = T3.HhId 
INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id  AND T4.Id = @CgRoleId
INNER JOIN  Programme T5 ON  T1.ProgrammeId =T5.Id 
INNER JOIN Person T6 ON T3.PersonId = T6.Id  AND T3.StatusId = @MemberStatusId
) ,
Z as
( select ROW_NUMBER() over( partition by HhId  order by  HhId desc) as RN,* FROM X
)
select HhId, max(RN) from Z-- where RN>1
group by HhId --order by RN desc 




--------------------------------------------------------


DECLARE @BeneficiaryRoleId INT 
DECLARE @CaregiverRoleId INT
DECLARE @SysCode VARCHAR(30)
DECLARE @SysDetailCode VARCHAR(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT
DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

 
 ; WITH X AS (
     
SELECT  T1.*, T3.HhId, T3.MemberRoleId,T4.Code 'ROLE', T5.Code 'PROGRAMME' FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  AND T1.RegGroupId>105
INNER JOIN HouseholdMember T3 ON T1.Id = T3.HhId 
INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id  AND T4.Id = @BeneficiaryRoleId
INNER JOIN  Programme T5 ON  T1.ProgrammeId =T5.Id 
--GROUP BY  T5.Code, T1.StatusId
 ),
 Z AS
(
SELECT ROW_NUMBER() OVER( PARTITION BY HhId  ORDER BY HhId DESC) AS RN,* FROM X
)

SELECT COUNT(DISTINCT HhId) 'Members', PROGRAMME FROM Z WHERE RN=1

GROUP BY PROGRAMME



DECLARE @BeneficiaryRoleId INT 
DECLARE @CaregiverRoleId INT
DECLARE @SysCode VARCHAR(30)
DECLARE @SysDetailCode VARCHAR(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT
DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

 

SELECT  T1.*, T3.HhId,T4.Code 'ROLE', T5.Code 'PROGRAMME' FROM Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  AND T1.RegGroupId>105
INNER JOIN HouseholdMember T3 ON T1.Id = T3.HhId 
INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id  AND T4.Id = @BeneficiaryRoleId
INNER JOIN  Programme T5 ON  T1.ProgrammeId =T5.Id 
--GROUP BY  T5.Code, T4.Code



DECLARE @BeneficiaryRoleId INT 
DECLARE @CaregiverRoleId INT
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT
DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode




SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  
 T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



;
SELECT T1.Id, T3.Id, T3.RelationshipId, T3.MemberRoleId, T4.Code,T7.Code from Household T1 
INNER JOIN SystemCodeDetail T2 ON T1.RegGroupId = T2.Id  AND T1.RegGroupId>105
INNER JOIN HouseholdMember T3 ON T1.Id = T3.HhId 
INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id  AND T4.Id = @CgRoleId
INNER JOIN SystemCodeDetail T7 ON T3.RelationshipId = T7.Id  
INNER JOIN  Programme T5 ON  T1.ProgrammeId =T5.Id  AND T5.Id=2
INNER JOIN Person T6 ON T3.PersonId = T6.Id  AND T3.StatusId = @MemberStatusId





