USE [CCTP-MIS]

GO
DECLARE @SysCode varchar(20)
DECLARE @SysDetailCode varchar(20)
DECLARE @SystemCodeDetailId1 int
DECLARE @SystemCodeDetailId2 int
DECLARE @SystemCodeDetailId3 int
DECLARE @ChangeTypeId int
DECLARE @UserId int


SET @SysCode='HHStatus'
SET @SysDetailCode='EX'
SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
SET @SysDetailCode='ENRLPSP'
SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
SET @SysDetailCode='PSPCARDED'
SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SELECT @ChangeTypeId=Id FROM CaseCategory WHERE Code='HH_EXIT'

DROP TABLE temp_BeneStatus

CREATE TABLE temp_BeneStatus(
		EnrolmentNo1 int, 
		EnrolmentNo2 int, 
		HhId1 int, 
		HhId2 int, 
		BeneFirstName varchar(50),
		BeneMiddleName varchar(50),
		BeneSurName varchar(50),
		BeneIdNo varchar(50),
		HHStatus1 varchar(50),
		HHStatus2 varchar(50),
		PSPStatus1 varchar(50),
		PSPStatus2 varchar(50),
		On350Error int,
		ExitedOnDeath int
	);


DELETE FROM temp_BeneStatus

INSERT INTO temp_BeneStatus
SELECT 
T1.EnrolmentNo1, T2.EnrolmentNo2, T1.HhId1, T2.HhId2,T1.BeneFirstName, T1.BeneMiddleName, T1.BeneSurName, T1.BeneIdNo,T1.HHStatus1, T2.HHStatus2,
CASE 
	WHEN T1.AccountNo IS NOT NULL AND T1.PaymentCardNo IS NULL THEN 'ENRLPSP'
	WHEN T1.PaymentCardNo IS NOT NULL THEN 'PSPCARDED'
	ELSE NULL
END AS PSPStatus1,
CASE 
	WHEN T2.AccountNo IS NOT NULL AND T2.PaymentCardNo IS NULL THEN 'ENRLPSP'
	WHEN T2.PaymentCardNo IS NOT NULL THEN 'PSPCARDED'
	ELSE NULL
END AS PSPStatus2,
T2.On350Error,
T2.ExitedOnDeath
FROM 
(
SELECT T1.Id HhId1,T6.Id EnrolmentNo1, T5.FirstName BeneFirstName, T5.MiddleName BeneMiddleName, T5.Surname BeneSurName, T5.NationalIdNo BeneIdNo, T2.Code AS HHStatus1, T7.AccountNo, T8.PaymentCardNo
    FROM Household  T1
        INNER JOIN SystemCodeDetail T2 ON T1.StatusId = T2.Id
        INNER JOIN HouseholdMember T3 ON T3.HhId = T1.Id
        INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id AND T4.Code = 'BENEFICIARY' AND T2.Code='1' AND T1.ProgrammeId =1
        INNER JOIN Person T5 ON T5.ID = T3.PersonId
        LEFT JOIN HouseholdEnrolment T6 ON T6.HhId = T1.Id
		LEFT JOIN BeneficiaryAccount T7 ON T6.Id=T7.HhEnrolmentId
		LEFT JOIN BeneficiaryPaymentCard T8 ON T7.Id=T8.BeneAccountId
) T1  
INNER JOIN (
SELECT T1.Id HhId2, T5.NationalIdNo BeneIdNo, T2.Code HHStatus2, T6.Id EnrolmentNo2, T7.AccountNo, T8.PaymentCardNo,
CASE WHEN T7.Id IS NULL AND T1.StatusId IN (@SystemCodeDetailId2, @SystemCodeDetailId3) THEN 1 ELSE 0 END AS On350Error,
CASE WHEN T9.Id IS NULL THEN 0 ELSE 1 END AS ExitedOnDeath
    FROM Household  T1
        INNER JOIN SystemCodeDetail T2 ON T1.StatusId = T2.Id
        INNER JOIN HouseholdMember T3 ON T3.HhId = T1.Id
        INNER JOIN SystemCodeDetail T4 ON T3.MemberRoleId = T4.Id AND T4.Code = 'BENEFICIARY' AND T2.Code<>'1' AND T1.ProgrammeId =1
        INNER JOIN Person T5 ON T5.ID = T3.PersonId
        LEFT JOIN HouseholdEnrolment T6 ON T6.HhId = T1.Id
		LEFT JOIN BeneficiaryAccount T7 ON T6.Id=T7.HhEnrolmentId
		LEFT JOIN BeneficiaryPaymentCard T8 ON T7.Id=T8.BeneAccountId
		LEFT JOIN Change T9 ON T6.Id=T9.HhEnrolmentId AND T9.ChangeTypeId=@ChangeTypeId AND ISNULL(T9.EscalatedBy,0)>0
) T2 ON T1.BeneIdNo = T2.BeneIdNo




/*
1. New Category - Beneficiaries without Enrolment# on Status1 Household but PSPCARDED / ENRLPSP. We shall exit status1 Household
*/
UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN  T2.HHStatus2 IN('ENRLPSP','PSPCARDED') AND T2.EnrolmentNo1 IS NULL  AND T2.On350Error <=0 THEN @SystemCodeDetailId1
		ELSE '1'
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.HHStatus2 IN('ENRLPSP','PSPCARDED') AND T2.EnrolmentNo1 IS NULL






/*
2. As earlier proposed, for this category we will exit status1 households. 
However if  PSPCARDED is on 350 error, AND on status1 was either PSPCARDED / ENRLPSP. 
Then we shall updated status1 accordingly and exit the PSPCARDED on 350 Error.
*/
UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN  T2.HHStatus2 IN('ENRLPSP','PSPCARDED') AND T2.On350Error <=0 THEN @SystemCodeDetailId1
		ELSE '1'
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.HHStatus2 IN('ENRLPSP','PSPCARDED') AND T2.On350Error <=0





/*
3. As earlier proposed, for this category we will exit status1 households. 
However if ENRLPSP is on 350 Error, AND on status1 was either PSPCARDED / ENRLPSP. 
Then we shall update status1 accordingly and exit the ENRLPSP on 350 error.
*/
UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN  T2.On350Error > 0 AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED') THEN @SystemCodeDetailId1
		--ELSE (SELECT Id FROM SystemCodeDetail WHERE Code=T2.PSPStatus1)
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId2=T1.Id
WHERE T2.On350Error > 0 AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED')

UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN  T2.On350Error > 0 AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED') THEN (CASE WHEN T2.PSPStatus1='ENRLPSP' THEN @SystemCodeDetailId2 ELSE @SystemCodeDetailId3 END)
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.On350Error > 0 AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED')





/*
4. If household with status 1 has an account (ENRLPSP)/ or a payment carded (PSPCARDED), 
we will exit the ENRL household and update the household with status1 accordingly. 
If status1 household  does not have an account(ENRL), We will exit it and retain the new ENRL household.  
*/

UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN T2.HHStatus2='ENRL' AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED') THEN @SystemCodeDetailId1
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId2=T1.Id
WHERE T2.HHStatus2='ENRL' AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED')

UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN T2.HHStatus2='ENRL' AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED') THEN (CASE WHEN T2.PSPStatus1='ENRLPSP' THEN @SystemCodeDetailId2 ELSE @SystemCodeDetailId3 END)
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.HHStatus2='ENRL' AND T2.PSPStatus1 IN('ENRLPSP','PSPCARDED')




/*
5. We need to eliminate all status1 households, so the proposed solution still stands.
However if the reason of exit is Death of Beneficiary, the corresponding household should also be exited. 
At the end of the update process, there will be no status1 households.
*/



UPDATE T1 
SET T1.StatusId=@SystemCodeDetailId1
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.HHStatus2='EX' AND T2.HHStatus1='1' AND ISNULL(T2.ExitedOnDeath, 0) > 0


UPDATE T1 
SET T1.StatusId=
	CASE 
		WHEN T2.PSPStatus1 IN('ENRLPSP','PSPCARDED') THEN (CASE WHEN T2.PSPStatus1='ENRLPSP' THEN @SystemCodeDetailId2 ELSE @SystemCodeDetailId3 END)
	END
FROM  Household T1 
INNER JOIN temp_BeneStatus T2 ON T2.HhId1=T1.Id
WHERE T2.HHStatus2='EX' AND T2.HHStatus1='1' AND ISNULL(T2.ExitedOnDeath, 0) <= 0


