
ALTER PROC [dbo].[ProcessPrepayroll]
	@PaymentCycleId int
   ,@ProgrammeId int
   ,@UserId int
AS
BEGIN
	DECLARE @ActiveHhs int
	DECLARE @TargetHhs int
	DECLARE @HhStatus_EnrolledProgCode varchar(20)
	DECLARE @HhStatus_EnrolledPSPCode varchar(20)
	DECLARE @HhStatus_PSPCardedCode varchar(20)
	DECLARE @HhStatus_OnPayrollCode varchar(20)
	DECLARE @HhStatus_OnSuspensionCode varchar(20)
	DECLARE @SysSetting_PSPACCDORMANCY int
	DECLARE @DormancyFromDate datetime
	DECLARE @DormancyToDate datetime
	DECLARE @BeneficiaryTypeId int
	DECLARE @PriReciCanReceivePayment bit
	DECLARE @SecondaryRecipientMandatory bit
	DECLARE @BeneficiaryType_INDIVIDUAL int
	DECLARE @ExceptionType_INVALIDBENEID int
	DECLARE @ExceptionType_INVALIDCGID int
	DECLARE @ExceptionType_DUPLICATEBENEIDIN int
	DECLARE @ExceptionType_DUPLICATEBENEIDACC int
	DECLARE @ExceptionType_DUPLICATECGIDIN int
	DECLARE @ExceptionType_DUPLICATECGIDACC int
	DECLARE @ExceptionType_INVALIDACC int
	DECLARE @ExceptionType_INVALIDCARD int
	DECLARE @ExceptionType_SUSPICIOUSAMT int
	DECLARE @ExceptionType_SUSPICIOUSDORMANCY int
	DECLARE @ExceptionType_INELIGIBLEBENE int
	DECLARE @ExceptionType_INELIGIBLECG int
	DECLARE @ExceptionType_INELIGIBLESUS int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(128)

	DECLARE @FileName nvarchar(128)
	DECLARE @FileType nvarchar(10)
	DECLARE @NoOfRows int

	SET @HhStatus_EnrolledProgCode = 'ENRL'
	SET @HhStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HhStatus_PSPCardedCode = 'PSPCARDED'
	SET @HhStatus_OnPayrollCode = 'ONPAY'
	SET @HhStatus_OnSuspensionCode = 'SUS'

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLDRAFT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='PREPAYROLLFINAL'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ActiveHhs=COUNT(T1.Id),@TargetHhs=SUM(CASE WHEN(T6.EnrolmentGroupId>0) THEN 1 ELSE 0 END),@SystemCodeDetailId3=MAX(T3.PaymentStageId)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
					  INNER JOIN PaymentCycleDetail T3 ON T1.ProgrammeId=T3.ProgrammeId
					  INNER JOIN HouseholdEnrolment T4 ON T1.Id=T4.HhId
					  INNER JOIN HouseholdEnrolmentPlan T5 ON T4.HhEnrolmentPlanId=T5.Id
					  LEFT JOIN PaymentEnrolmentGroup T6 ON T3.PaymentCycleId=T6.PaymentCycleId AND T3.ProgrammeId=T6.ProgrammeId AND T5.EnrolmentGroupId=T6.EnrolmentGroupId								  
	WHERE T3.PaymentCycleId=@PaymentCycleId AND T3.ProgrammeId=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode,@HhStatus_OnSuspensionCode)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycle WHERE Id=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF	@SystemCodeDetailId3 NOT IN(@SystemCodeDetailId1,@SystemCodeDetailId2)
		SET @ErrorMsg='Once the Prepayroll has been APPROVED one CANNOT run the Prepayroll again'	
	ELSE IF ISNULL(@TargetHhs,0)=0
		SET @ErrorMsg='No target households'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='System Settings'
	SET @SysDetailCode='PSPACCDORMANCY'
	SELECT @SysSetting_PSPACCDORMANCY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='Beneficiary Type'
	SET @SysDetailCode='INDIVIDUAL'
	SELECT @BeneficiaryType_INDIVIDUAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Exception Type'
	SET @SysDetailCode='INVALIDBENEID'
	SELECT @ExceptionType_INVALIDBENEID=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INVALIDCGID'
	SELECT @ExceptionType_INVALIDCGID=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysDetailCode='DUPLICATEBENEIDIN'
	SELECT @ExceptionType_DUPLICATEBENEIDIN=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='DUPLICATEBENEIDACC'
	SELECT @ExceptionType_DUPLICATEBENEIDACC=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='DUPLICATECGIDIN'
	SELECT @ExceptionType_DUPLICATECGIDIN=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='DUPLICATECGIDACC'
	SELECT @ExceptionType_DUPLICATECGIDACC=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='INVALIDACC'
	SELECT @ExceptionType_INVALIDACC=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INVALIDCARD'
	SELECT @ExceptionType_INVALIDCARD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='SUSPICIOUSAMT'
	SELECT @ExceptionType_SUSPICIOUSAMT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='SUSPICIOUSDORMANCY'
	SELECT @ExceptionType_SUSPICIOUSDORMANCY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='INELIGIBLEBENE'
	SELECT @ExceptionType_INELIGIBLEBENE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INELIGIBLECG'
	SELECT @ExceptionType_INELIGIBLECG=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INELIGIBLESUS'
	SELECT @ExceptionType_INELIGIBLESUS=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @BeneficiaryTypeId=T1.BeneficiaryTypeId
		  ,@PriReciCanReceivePayment=T1.PriReciCanReceivePayment
		  ,@SecondaryRecipientMandatory=T1.SecondaryRecipientMandatory
		  ,@DormancyToDate=CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code,0)+' '+CONVERT(varchar(4),CASE WHEN(T4.Code<7) THEN 1 ELSE 0 END+CONVERT(bigint,T5.Code)))
	FROM Programme T1 INNER JOIN PaymentCycleDetail T2 ON T1.Id=T2.ProgrammeId
					  INNER JOIN PaymentCycle T3 ON T2.PaymentCycleId=T3.Id
					  INNER JOIN SystemCodeDetail T4 ON T3.FromMonthId=T4.Id
					  INNER JOIN SystemCodeDetail T5 ON T3.FinancialYearId=T5.Id
	WHERE T2.PaymentCycleId=@PaymentCycleId AND T2.ProgrammeId=@ProgrammeId

	SET @DormancyFromDate=DATEADD(MM,-@SysSetting_PSPACCDORMANCY,@DormancyToDate)

	BEGIN TRAN

	DELETE T1 FROM Prepayroll T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollDuplicateID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidPaymentAccount T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidPaymentCard T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollIneligible T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollSuspicious T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;

	INSERT INTO Prepayroll(PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSexId,BeneNationalIDNo,PriReciCanReceivePayment,CGPersonId,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSexId,CGNationalIDNo,TotalHhMembers,HhStatusId,BeneAccountId,BenePaymentCardId,SubLocationId,PaymentZoneId,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount)
	SELECT @PaymentCycleId,T3.Id AS ProgrammeId,T1.Id AS HhId,T4.PersonId AS BenePersonId,T4.FirstName AS BeneFirstName,T4.MiddleName AS BeneMiddleName,T4.Surname AS BeneSurname,T4.DoB AS BeneDoB,T4.SexId AS BeneSexId,T4.NationalIdNo AS BeneNationalIDNo,T3.PriReciCanReceivePayment,T5.PersonId AS CGPersonId,T5.FirstName AS CGFirstName,T5.MiddleName AS CGMiddleName,T5.Surname AS CGSurname,T5.DoB AS CGDoB,T5.SexId AS CGSexId,T5.NationalIdNo AS CGNationalIDNo,T6.TotalHhMembers,T1.StatusId AS HhStatusId,T14.Id AS BeneAccountId,T15.Id AS BenePaymentCardId,T7.SubLocationId,T8.Id AS PaymentZoneId,CASE WHEN(T8.IsPerc=1) THEN ((T3.EntitlementAmount+T10.AdjustmentAmount)*T8.Commission)/100 ELSE T8.Commission END AS PaymentZoneCommAmt,ISNULL(T9.BeneAccDormancy,0) AS ConseAccInactivity,T3.EntitlementAmount,ISNULL(T10.AdjustmentAmount,0) AS AdjustmentAmount
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
					  INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					  INNER JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T1.NationalIdNo
									FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
												   INNER JOIN Household T3 ON T2.HhId=T3.Id
												   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
									WHERE T2.StatusId=@SystemCodeDetailId1
								) T4 ON T1.Id=T4.HhId AND T4.RowId=1
					  LEFT JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T1.NationalIdNo
									FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
												   INNER JOIN Household T3 ON T2.HhId=T3.Id
												   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.SecondaryRecipientId
									WHERE T2.StatusId=@SystemCodeDetailId1
								) T5 ON T1.Id=T5.HhId AND T5.RowId=1
					  LEFT JOIN(
									SELECT HhId,COUNT(PersonId) AS TotalHhMembers
									FROM HouseholdMember
									WHERE StatusId=@SystemCodeDetailId1
									GROUP BY HhId
								) T6 ON T1.Id=T6.HhId
					  LEFT JOIN(
									SELECT T1.HhId,T1.SubLocationId,T2.LocationId,T4.PaymentZoneId
									FROM HouseholdSubLocation T1 INNER JOIN SubLocation T2 ON T1.SubLocationId=T2.Id
																 INNER JOIN GeoMaster T3 ON T1.GeoMasterId=T3.Id
																 INNER JOIN Location T4 ON T2.LocationId=T4.Id
									WHERE T3.IsDefault=1
								) T7 ON T1.Id=T7.HhId	
					  LEFT JOIN PaymentZone T8 ON T7.PaymentZoneId=T8.Id
					  LEFT JOIN (
									SELECT T3.HhId,SUM(CASE(T1.HasUniqueTrx) WHEN 1 THEN 1 ELSE 0 END) AS BeneAccDormancy
									FROM BeneficiaryAccountActivity T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
																	   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
																	   INNER JOIN SystemCodeDetail T4 ON T1.MonthId=T4.Id
									WHERE CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code,0)+' '+CONVERT(varchar(4),T1.[Year]))>@DormancyFromDate
										AND CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code,0)+' '+CONVERT(varchar(4),T1.[Year]))<=@DormancyFromDate
									GROUP BY T3.HhId
								) T9 ON T1.Id=T9.HhId
					  LEFT JOIN (
									SELECT PaymentCycleId,HhId,SUM(AdjustmentAmount) AS AdjustmentAmount
									FROM PaymentAdjustment
									GROUP BY PaymentCycleId,HhId
								) T10 ON T10.PaymentCycleId=@PaymentCycleId AND T1.Id=T10.HHId
					  INNER JOIN HouseholdEnrolment T11 ON T1.Id=T11.HhId
					  INNER JOIN HouseholdEnrolmentPlan T12 ON T11.HhEnrolmentPlanId=T12.Id
					  INNER JOIN PaymentEnrolmentGroup T13 ON T13.PaymentCycleId=@PaymentCycleId AND T12.EnrolmentGroupId=T13.EnrolmentGroupId	
					  LEFT JOIN BeneficiaryAccount T14 ON T11.Id=T14.HhEnrolmentId AND T14.StatusId=@SystemCodeDetailId2 AND T14.ExpiryDate>GETDATE()
					  LEFT JOIN BeneficiaryPaymentCard T15 ON T14.Id=T15.BeneAccountId AND T15.StatusId=@SystemCodeDetailId3
	WHERE T3.Id=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
		AND T14.Id<=486640

	INSERT INTO PrepayrollInvalidID(PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,@ExceptionType_INVALIDBENEID
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND @PriReciCanReceivePayment=1 AND ISNUMERIC(BeneNationalIDNo)<>1
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,@ExceptionType_INVALIDCGID
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND CGPersonId>1 AND ISNUMERIC(CGNationalIDNo)<>1
	
	INSERT INTO PrepayrollDuplicateID(PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.BenePersonId,@ExceptionType_DUPLICATEBENEIDIN
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo,COUNT(T1.HhId) NoOfHhs
							FROM (
									SELECT T6.NationalIdNo,T1.Id AS HhId
									FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
														INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
														INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId 
														INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T3.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
														INNER JOIN Person T6 ON T5.PersonId=T6.Id
									WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode) AND ISNUMERIC(T6.NationalIDNo)=1
								) T1
							GROUP BY CONVERT(bigint,T1.NationalIdNo)
							HAVING COUNT(T1.HhId)>1
						) T2 ON CONVERT(bigint,T1.BeneNationalIDNo)=T2.ResolvedIDNo
	UNION
	SELECT T1.PaymentCycleId,ProgrammeId,T1.HhId,T1.BenePersonId,@ExceptionType_DUPLICATEBENEIDACC
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T6.NationalIdNo
							FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
												INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
												INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
												INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T3.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
												INNER JOIN Person T6 ON T5.PersonId=T6.Id
							WHERE NOT(T3.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL) AND ISNUMERIC(T6.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
							GROUP BY T6.NationalIdNo
						) T2 ON CONVERT(bigint,T1.BeneNationalIDNo)=CONVERT(bigint,T2.NationalIdNo)
	WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)
	UNION
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.CGPersonId,@ExceptionType_DUPLICATECGIDIN
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,CGNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo,COUNT(T1.HhId) NoOfHhs
							FROM (
									SELECT T6.NationalIdNo,T1.Id AS HhId
									FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
														INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
														INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId
														INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T3.SecondaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
														INNER JOIN Person T6 ON T5.PersonId=T6.Id
									WHERE T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode) AND ISNUMERIC(T6.NationalIDNo)=1
								) T1
							GROUP BY CONVERT(bigint,T1.NationalIdNo)
							HAVING COUNT(T1.HhId)>1
						) T2 ON CONVERT(bigint,T1.CGNationalIDNo)=T2.ResolvedIDNo
	UNION
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.CGPersonId,@ExceptionType_DUPLICATECGIDACC
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,CGNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T6.NationalIdNo
							FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
												INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
												INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
												INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T3.SecondaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
												INNER JOIN Person T6 ON T5.PersonId=T6.Id
							WHERE NOT(T3.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL) AND ISNUMERIC(T6.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
							GROUP BY T6.NationalIdNo
						) T2 ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T2.NationalIdNo)
	WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)

	INSERT INTO PrepayrollInvalidPaymentAccount(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INVALIDACC
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BeneAccountId,0)<=0 

	INSERT INTO PrepayrollInvalidPaymentCard(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INVALIDCARD
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePaymentCardId,0)<=0

	INSERT INTO PrepayrollIneligible(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INELIGIBLEBENE
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePersonId,0)<=0
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INELIGIBLECG
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND @SecondaryRecipientMandatory=1 AND ISNULL(CGPersonId,0)<=0
	UNION
	SELECT T1.PaymentCycleId,ProgrammeId,T1.HhId,@ExceptionType_INELIGIBLESUS
	FROM Prepayroll T1 INNER JOIN SystemCodeDetail T2 ON T1.HhStatusId=T2.Id
	WHERE PaymentCycleId=@PaymentCycleId AND T2.Code=@HhStatus_OnSuspensionCode

	INSERT INTO PrepayrollSuspicious(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_SUSPICIOUSDORMANCY
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ConseAccInactivity>=@SysSetting_PSPACCDORMANCY
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_SUSPICIOUSAMT
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND AdjustmentAmount<>0

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLFINAL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.EnrolledHhs=@ActiveHHs
	   ,T1.PrePayrollBy=@UserId
	   ,T1.PrePayrollOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		SELECT 1 AS NoRows
	END
	--EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
END
