alter proc GetDisputedAccounts
as

begin
;WITH X AS (
select * from PspEnrolmentLog
UNION

select * from PspEnrolmentLogV1
UNION

select * from PspEnrolmentLogV2
 )


 SELECT DISTINCT 
 T1.EnrolmentNo ,
 T1.BankCode 'Attempting PSP ',
 T1.BranchCode 'Attempting PSP Branch', 
 T1.AccountName 'Attempted Account Name',
 T1.AccountNo 'Attempted Account No',
 T1.TokenId 'Attempted TOKen',
    T4.Code 'Locked By PSP ',
    T3.Code 'Locked By PSP Branch',
   T2.AccountName 'Locked Account No',
   T2.AccountNo 'Locked Account Name'

   FROM X T1 
 LEFT JOIN BeneficiaryAccount T2 ON  CONVERT(INT,T1.EnrolmentNo ) = T2.HhEnrolmentId
 INNER JOIN PSPBranch T3 ON T2.PSPBranchId = T3.Id
 INNER JOIN PSP  T4 ON T3.PSPId = T4.Id
 INNER JOIN HouseholdEnrolment T5 ON T2.HhEnrolmentId = T5.Id
 INNER JOIN Household T6 ON T6.Id= T5.HhId
 WHERE T3.Code<>T1.BranchCode AND T4.Code<>T1.BankCode
 end
 go 


 GetDisputedAccounts