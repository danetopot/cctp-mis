
USE [CCTP-MIS]
GO

DECLARE @STAGE INT  = 0

IF @STAGE = 0
BEGIN

    SELECT 'IMPORT ALL SUB LOCATIONS AND LOCATIONS MAPPING FROM OUTSIDE INTO INSIDE'

  update SubLocation set code = '424020205' where code ='kivaani'
END


IF @STAGE = 1
BEGIN
    SELECT T1.*
    INTO WrongHouseholdSubLocation
    FROM HouseholdSubLocation T1
        INNER JOIN SubLocation T2 ON T1.SubLocationId = T2.Id
            AND T2.CreatedOn >='2018-11-21'
END

IF @STAGE = 2
BEGIN
    UPDATE T1 SET T1.SubLocationId = T5.Id
    FROM HouseholdSubLocation T1
        INNER JOIN SubLocation T2 ON T1.SubLocationId = T2.Id AND T2.CreatedOn >='2018-11-21'
        INNER JOIN ExtraSublocations T3 ON ltrim(rtrim(T3.SublocationCode)) = T2.Code
        INNER JOIN SubLocation T5 ON T5.Code = ltrim(rtrim(T3.Subloc_Code))
            
END

IF @STAGE = 3
BEGIN

-- UPDATE ALL THE SUBLOCATIONS THAT HAVE BEEN CHANGED FOR THE HOUSEHOLD
           UPDATE T1 
        SET T1.SubLocationId = T2.SubLocationId
        FROM Prepayroll T1 
        inner join HouseholdSubLocation T2 ON T1.HhId = T2.HhId  AND T1.SubLocationId <> T2.SubLocationId 
   
END

IF @STAGE = 4
BEGIN
    SELECT 'IMPORT  THE FINAL PAYMENT ZONES INTO CCTPMIS '
-- THETABLE NAME IS FinalLocationPaymentZone

END

IF @STAGE = 5
BEGIN
    UPDATE T1 
SET T1.PaymentZoneId = t2.PaymentZoneId
FROM [Location]  T1
        INNER JOIN FinalLocationPaymentZone T2 ON T1.Code = t2.LocationCode
END

IF @STAGE = 6
BEGIN
    UPDATE T1 
SET T1.PaymentZoneId = T4.PaymentZoneId,
T1.PaymentZoneCommAmt = T5.Commission
FROM Prepayroll  T1
       -- INNER JOIN HouseholdSubLocation T2 ON T1.SubLocationId = T2.SubLocationId
        INNER JOIN SubLocation T3 ON T3.Id = T1.SubLocationId
        INNER JOIN [Location] T4 ON T4.Id = T3.LocationId
        INNER JOIN PaymentZone T5 ON T5.Id = T4.PaymentZoneId
END
 

 