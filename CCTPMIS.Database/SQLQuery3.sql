
ALTER PROC [dbo].[GetBeneficiariesWithNoAccounts]
AS
BEGIN
SELECT
T2.Id 'EnrolmentNo'
,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
,T4.Code 'Programme', CONCAT(T7.FirstName, ' ', T7.MiddleName, ' ',T7.Surname) 'Beneficiary Name', T7.NationalIdNo,
T13.County,T13.Constituency,T13.Location,T13.SubLocation

FROM Household T1 
INNER JOIN HouseholdMember T6 ON T1.Id=T6.HhId
INNER JOIN Person T7 ON T7.Id=T6.PersonId
INNER JOIN HouseholdEnrolment T2 ON T2.HhId= T1.Id
INNER JOIN HouseholdSubLocation T3 ON T3.HhId=T2.Id
INNER JOIN Programme T4 ON T4.Id = T1.ProgrammeId AND T4.PrimaryRecipientId = T6.MemberRoleId
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,
				   T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
					FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
										INNER JOIN Division T3 ON T2.DivisionId=T3.Id
										INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
										INNER JOIN District T5 ON T4.DistrictId=T5.Id
										INNER JOIN County T6 ON T4.CountyId=T6.Id
										INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
										INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
				) T13 ON T13.SubLocationId=T3.SubLocationId AND T3.GeoMasterId=T13.GeoMasterId
LEFT JOIN BeneficiaryAccount T5 ON T5.HhEnrolmentId= T2.Id
WHERE T5.AccountName IS NULL  
ORDER BY 
END
GO

EXEC [dbo].[GetBeneficiariesWithNoAccounts]