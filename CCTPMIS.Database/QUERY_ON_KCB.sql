-- what the PSP is sending 

;with xx as 
(
SELECT * FROM PspEnrolmentLog
union 
SELECT * FROM PspEnrolmentLogV1
union 
SELECT * FROM PspEnrolmentLogv2
 )
 SELECT DISTINCT 
      [EnrolmentNo]
      ,[PriReciFirstName]
      ,ISNULL(PriReciMiddleName,'') AS PriReciMiddleName  
      ,[PriReciSurname]
      ,[PriReciNationalIdNo]
      ,[PriReciSex]
      ,[PriReciDoB]
	  ,ISNULL([SecReciFirstName],'') AS [SecReciFirstName]  
	  ,ISNULL([SecReciSurname],'') AS [SecReciSurname]  
	  ,ISNULL([SecReciNationalIdNo],'') AS [SecReciNationalIdNo]  
	  ,ISNULL([SecReciSex],'') AS [SecReciSex]  
	  ,ISNULL([SecReciDoB],'') AS [SecReciDoB]  
 
      ,[MobileNo2]
      ,[AccountName]
      ,[AccountNo]
     -- ,[DateAdded]
      ,T1.[TokenId]
      ,[BankCode]
      ,[BranchCode]
      ,[AccountOpenedOn]
      ,[PaymentCardNo]
	   FROM xx T1 
 INNER JOIN HouseholdEnrolment T2 ON T1.EnrolmentNo = T2.Id
 INNER JOIN HOUSEHOLD T3 ON T2.HhId  = T3.Id AND T1.BankCode='01' --and t1.TokenId <> t3.TokenId
  
 AND 
 T2.Id IN ( 1,2,3 )
     -- put HHENrolmentID  here
    



-- DATA THAT WAS ON ENROLMENT FILE, EXPECTED FORMAT DEPENDING 
 SELECT T2.Id AS EnrolmentNo
 ,T4.CODE PROGRAMME
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS PriFirstName
		,T6.MiddleName AS PriMiddleName
		,T6.Surname AS PriSurname
		,T6.NationalIdNo AS PriIDNo
		,T7.Code AS PriSex
		,T6.DoB AS PriDoB
		,ISNULL(T9.FirstName,'') AS SecReciFirstName
		,ISNULL(T9.MiddleName,'') AS SecReciMiddleName
		,ISNULL(T9.Surname,'') AS SecReciSurname
		,ISNULL(T9.NationalIdNo,'') AS SecReciIDNo
		,ISNULL(T10.Code,'') AS SecReciSex
		,ISNULL(T9.DoB,'') AS SecReciDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T3.TokenId
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

											  AND
 T2.Id IN ( 1,2,3 )

ORDER BY T2.Id

--- ENROLLED AND BY WHO

SELECT  T3.NAME,T2.NAME,  T1.HhEnrolmentId, T1.AccountNo, T1.AccountName  FROM BeneficiaryAccount T1 
INNER JOIN PSPBranch T2 ON T1.PSPBranchId = T2.Id
INNER JOIN PSP T3 ON T3.ID = T2.PSPId
AND 


 T1. HhEnrolmentId IN ( 1,2,3 )