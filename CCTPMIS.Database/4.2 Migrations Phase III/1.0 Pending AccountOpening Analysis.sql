DECLARE @MIG_STAGE tinyint
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int
DECLARE @StartDate Datetime
DECLARE @EndDate Datetime
DECLARE @SysCode varchar(20)
DECLARE @SysDetailCode varchar(20)
DECLARE @FileName varchar(128)
DECLARE @FileExtension varchar(5)
DECLARE @FileCompression varchar(5)
DECLARE @FilePathName varchar(128)
DECLARE @SQLStmt varchar(8000)
DECLARE @FileExists bit
DECLARE @FileIsDirectory bit
DECLARE @FileParentDirExists bit
DECLARE @DatePart_Day char(2)
DECLARE @DatePart_Month char(2)
DECLARE @DatePart_Year char(4)
DECLARE @DatePart_Time char(4)
DECLARE @FileCreationId int
DECLARE @FilePassword nvarchar(64)
DECLARE @NoOfRows int
DECLARE @ErrorMsg varchar(128)
DECLARE @SystemCodeDetailId1 int
DECLARE @SystemCodeDetailId2 int
DECLARE @MaxTokenId int, @PrevMaxTokenId INT
DECLARE @UserId int

SET @UserId = 1
select @MaxTokenId = MAX(TokenId)
from Household

set @PrevMaxTokenId = 690624
DECLARE 
	 @FilePath varchar(128)
	,@DBServer varchar(30)
	,@DBName varchar(30)
	,@DBUser varchar(30)
	,@DBPassword varchar(30)

SELECT
    @FilePath = 'E:\\Shared\\',
    @DBName='CCTP-MIS',
    @DBPassword='SAU@70+',
    @DBUser='CCTP-MIS',
    @DBServer='LOCALHOST'

SELECT @StartDate = '2018-11-20', @EndDate = '2018-12-30'

SET @MIG_STAGE = 1

IF(@MIG_STAGE = -1)
BEGIN
 

 -- CALL THE NEW STORED PROC


END

IF @MIG_STAGE = 0
BEGIN
    SELECT T2.Id AS EnrolmentNo
		, T4.Code AS Programme
		, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		, T6.FirstName AS BeneFirstName
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
    INTO temp_PendingAccountOpening
    FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
        INNER JOIN Household T3 ON T2.HhId=T3.Id
        INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
        INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
        INNER JOIN Person T6 ON T5.PersonId=T6.Id
        INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
        LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
        LEFT JOIN Person T9 ON T8.PersonId=T9.Id
        LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
        INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
        INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
        INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
        FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
            INNER JOIN Division T3 ON T2.DivisionId=T3.Id
            INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
            INNER JOIN District T5 ON T4.DistrictId=T5.Id
            INNER JOIN County T6 ON T4.CountyId=T6.Id
            INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
            INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
        LEFT JOIN BeneficiaryAccount T14 ON T2.ID= T14.HhEnrolmentId
    where T1.CreatedOn BETWEEN @StartDate AND @EndDate and t14.Id is null

    SET @FileName='PENDING_PHASE_01_'


    SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
    SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
    SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
    SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
    SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
    SET @FilePathName=@FilePath+@FileName
    SET @FileExtension='.csv'


    SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_TokenExport" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
    EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

END

IF @MIG_STAGE = 1
BEGIN
    SELECT T2.HhEnrolmentPlanId, T2.HhId, T2.Id HhEnrolmentId, T3.ProgrammeId, T3.TokenId, GETDATE() CreatedOn, @UserId CreatedBy
    INTO HouseholdPendingAccountOpening
    FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
        INNER JOIN Household T3 ON T2.HhId=T3.Id
        INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
        INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
        INNER JOIN Person T6 ON T5.PersonId=T6.Id
        INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
        LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
        LEFT JOIN Person T9 ON T8.PersonId=T9.Id
        LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
        INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
        INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
        INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
        FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
            INNER JOIN Division T3 ON T2.DivisionId=T3.Id
            INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
            INNER JOIN District T5 ON T4.DistrictId=T5.Id
            INNER JOIN County T6 ON T4.CountyId=T6.Id
            INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
            INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
        LEFT JOIN BeneficiaryAccount T14 ON T2.ID= T14.HhEnrolmentId
    where T1.CreatedOn BETWEEN @StartDate AND @EndDate and t14.Id is null


END




IF @MIG_STAGE = 2
BEGIN
    ;
    WITH
        TokenGenerator
        as
        (
            select
                T3.Id AS HhId, ROW_NUMBER() over (order by T6.DoB) As TokenNo
            from Household T3
                INNER JOIN HouseholdPendingAccountOpening T1 ON T1.HhId = T3.Id
                INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
                INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
        )
        update Household set TokenId = TokenNo + @MaxTokenId from TokenGenerator
        left join Household h on h.Id = TokenGenerator.HhId


END


IF @MIG_STAGE = 3
BEGIN
    IF OBJECT_ID(N'temp_TokenExport') IS NOT NULL
			DROP TABLE temp_TokenExport;

    SET @FileName='TokenRegen_'



    SELECT
        T4.Code Programme
		, REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.TokenId)))+CONVERT(varchar(6),T3.TokenId) AS TokenId
		, T6.FirstName AS BeneFirstName
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, convert(float,T6.NationalIdNo) AS BeneIDNo
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
    into temp_TokenExport
    FROM Household T3 -- ON T2.HhId=T3.Id
        INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
        INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
        INNER JOIN Person T6 ON T5.PersonId=T6.Id
        INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
        LEFT JOIN HouseholdMember T8 ON T3.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
        LEFT JOIN Person T9 ON T8.PersonId=T9.Id
        LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
        INNER JOIN HouseholdSubLocation T11 ON T3.Id=T11.HhId
        INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
        INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
        FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
            INNER JOIN Division T3 ON T2.DivisionId=T3.Id
            INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
            INNER JOIN District T5 ON T4.DistrictId=T5.Id
            INNER JOIN County T6 ON T4.CountyId=T6.Id
            INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
            INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
    WHERE ISNULL(T3.TokenId,0)>@PrevMaxTokenId

    SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
    SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
    SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
    SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
    SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
    SET @FilePathName=@FilePath+@FileName
    SET @FileExtension='.csv'


    SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_TokenExport" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
    EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;


END
