

DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW varchar(20)
DECLARE @SysRightCode_ENTRY varchar(20)
DECLARE @SysRightCode_MODIFIER varchar(20)
DECLARE @SysRightCode_DELETION varchar(20)
DECLARE @SysRightCode_APPROVAL varchar(20)
DECLARE @SysRightCode_EXPORT varchar(20)
DECLARE @SysRightCode_DOWNLOAD varchar(20)
DECLARE @SysRightCode_UPLOAD varchar(20)

DECLARE @SysRightId_VIEW int
DECLARE @SysRightId_ENTRY int
DECLARE @SysRightId_MODIFIER int
DECLARE @SysRightId_DELETION int
DECLARE @SysRightId_APPROVAL int
DECLARE @SysRightCode_FINALIZE int
DECLARE @SysRightCode_VERIFY int
DECLARE @SysRightId_EXPORT int
DECLARE @SysRightId_DOWNLOAD int
DECLARE @SysRightId_UPLOAD int

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD


SET @ModuleCode='Enrolment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)
	 ,(@ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL)
	 ,(@ModuleId,@SysRightId_EXPORT,@SysRightCode_EXPORT)

SET @ModuleCode='PSP-Enrolment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD)
	 ,(@ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD)

SET @ModuleCode='Programmes'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='Donors'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='PSPs'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='PSPBranches'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='GeoUnits'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='General Parameters'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='Expansion Plans'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='User Groups'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='Users'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='Database Backups'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)


SET @ModuleCode='Payment Cycle'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION)

SET @ModuleCode='Payment Detail'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)
	 ,(@ModuleId,@SysRightCode_APPROVAL,@SysRightCode_APPROVAL)

SET @ModuleCode='Pre-Payroll'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)

	 ,(@ModuleId,@SysRightId_ENTRY,'RUN & GENERATE EXCEPTIONS')
	 ,(@ModuleId,@SysRightId_DELETION,'RE-RUN')
	 ,(@ModuleId,@SysRightId_MODIFIER,'ACTION')	--FOR ACTIONING

	 ,(@ModuleId,@SysRightCode_FINALIZE,@SysRightCode_FINALIZE)
	 ,(@ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL)
	 ,(@ModuleId,@SysRightCode_DOWNLOAD,@SysRightCode_DOWNLOAD)

SET @ModuleCode='Request For Funds'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)

	 ,(@ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY)
	 ,(@ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER)

	 ,(@ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL)
	 ,(@ModuleId,@SysRightCode_UPLOAD,@SysRightCode_UPLOAD)
	 ,(@ModuleId,@SysRightCode_DOWNLOAD,@SysRightCode_DOWNLOAD)

SET @ModuleCode='Payroll'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_ENTRY,'PROCESS')
	 ,(@ModuleId,@SysRightCode_VERIFY,@SysRightCode_VERIFY)
	 ,(@ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL)
	 ,(@ModuleId,@SysRightId_EXPORT,'SHARE')
	 ,(@ModuleId,@SysRightCode_DOWNLOAD,@SysRightCode_DOWNLOAD)

SET @ModuleCode='PSP-Payment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
VALUES(@ModuleId,@SysRightId_VIEW,@SysRightCode_VIEW)
	 ,(@ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD)
	 ,(@ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD)

INSERT INTO GroupRight(UserGroupId,ModuleRightId,CreatedBy,CreatedOn)
SELECT T1.Id AS UserGroupId,T2.Id AS ModuleRightId,1 AS CreatedBy,GETDATE() AS CreatedOn
FROM UserGroup T1 CROSS JOIN ModuleRight T2
WHERE T1.Name='System'
