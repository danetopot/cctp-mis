

/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO MIGRATE DSD DATA INTO THE NEW DSD WEB MIS
	CREATED BY: JOAB SELELYA (SENIOR ASSOCIATE - MIS EXPERT, DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH OCTOBER, 2015
   MODIFIED ON: 

	NB: THE MIGRATION SOURCE DB TABLE IS CALLED MigrationData_Base

	TO DO:
	======


*/


/*


DROP TABLE MigrationData_Base
GO

DROP TABLE MigrationData_Targets
GO

*/




DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int

SET @MIG_STAGE = 0

IF @MIG_STAGE = 0	--0. DEFAULT CREDENTIALS
BEGIN
	DECLARE @UserGroupId int

	INSERT INTO Module(Name, [Description],ParentModuleId)
	SELECT T1.Name,T1.[Description],T1.ParentModuleId
	FROM (SELECT 1 AS ModuleNo,'Dashboard' AS Name,'Current summary project achievement' AS [Description],NULL AS ParentModuleId
		  UNION
		  SELECT 2,'Trend Analysis','Analyzes the trend over time',NULL
		  UNION
		  SELECT 3,'Maps','Project elements geo-locations',NULL
		  UNION
		  SELECT 4,'Registration','Potential Beneficiary Registration',NULL
		  UNION
		  SELECT 5,'Targeting','Registereed Beneficiary Targeting',NULL
		  UNION
		  SELECT 6,'Enrolment','Programme Beneficiary Enrolment',NULL
		  UNION
		  SELECT 7,'Payment','Beneficiary Payment',NULL
		  UNION
		  SELECT 8,'Payment Cycle','Programme Beneficiary Payment Cycle',7
		  UNION
		  SELECT 9,'Pre-Payroll','Beneficiary Pre-payroll Audit',7
		  UNION
		  SELECT 10,'Request For Funds','Request For Payment Cycle Funds',7
		  UNION
		  SELECT 11,'Payroll','Beneficiary Payroll',7
		  UNION
		  SELECT 12,'PSP Report','Payment Service Provider Report',7
		  UNION
		  SELECT 13,'PSP-Movement','Beneficiary Account Withdrawal Activity',7
		  UNION
		  SELECT 14,'Post-Payroll','Post-payroll Audit',7
		  UNION
		  SELECT 15,'Recertification','Programme Beneficiary Recertification',NULL
		  UNION
		  SELECT 16,'Fund Management','Payment Cycle Funds Management',NULL
		  UNION
		  SELECT 17,'Case Management','Beneficiary Case Management',NULL
		  UNION
		  SELECT 18,'Monitoring & Evaluation','Programme Performance Monitoring',NULL
		  UNION
		  SELECT 19,'Administration','System Setup',NULL
		  UNION
		  SELECT 20,'Programmes','Safety Net Programmes',19
		  UNION
		  SELECT 21,'Donors','Actors supporting the Safety Net Programmes',19
		  UNION
		  SELECT 22,'PSPs','Providers of payment services',19
		  UNION
		  SELECT 23,'PSPBranches','Branches for providers of payment services',19
		  UNION
		  SELECT 24,'GeoUnits','Geographical Locations',19
		  UNION
		  SELECT 25,'General Parameters','System user-defined codes',19
		  UNION
		  SELECT 26,'Expansion Plans','Programme targeting expansion plans',19
		  UNION
		  SELECT 27,'User Groups','System user groups',19
		  UNION
		  SELECT 28,'Users','System users',19
		  UNION
		  SELECT 29,'Database Backups','System database backups',19
		  ) T1

	INSERT INTO UserGroupProfile(Name,[Description])
	VALUES('SYS','System')
		 ,('SAU','Social Assistance Unit')
		 ,('CCCS','County Coordinator Children Services')
		 ,('CCSD','County Coordinator Social Development')
		 ,('SCCSO','Sub-County Children Services Officer')
		 ,('SDO','Social Development Officer')
		 ,('LO','Liason Officer')
		 ,('MA','Ministry Accounting')
		 ,('MICT','Ministry ICT')
		 ,('NCPwDO','National Council of Persons with Disability Officer')

	INSERT INTO UserGroup(UserGroupProfileId,Name,CreatedBy,CreatedOn)
	Values ( 1,'System',0,GETDATE()),
	( 2,'SAU Head',0,GETDATE()),
	( 2,'System',0,GETDATE())


	SET @UserGroupId=IDENT_CURRENT('UserGroup')

	INSERT INTO [User](UserGroupId,email,emailConfirmed,[Password],FirstName,MiddleName,Surname,Organization,Department,Position,MobileNo,MobileNoConfirmed,IsActive,DeactivateDate,CreatedBy,CreatedOn)
	SELECT @UserGroupId AS UserGroupId,'system@system.com' AS Email,0 AS EmailConfirmed,'*****' AS [Password],'System' AS FirstName,'' AS MiddleName,'' AS Surname,'' AS Organization,'' AS Department,'' AS Position,'' AS MobileNo,0 AS MobileNoConfirmed,1 AS IsActive,NULL AS DeactivateDate,0 AS CreatedBy,GETDATE()


	SELECT * FROM Module
	SELECT * FROM UserGroup
	SELECT * FROM [User]
END



IF @MIG_STAGE = 1	--1. GENERAL SETUP
BEGIN	
	SET @Code='System Settings'
	SET @Description='The System Settings'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='HHStatus'
	SET @Description='The household status in the programme'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Stages'
	SET @Description='The payment/payroll stages'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Frequency'
	SET @Description='The frequency of programme beneficiary payment'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Payment Adjustment'
	--SET @Description='The payment/payroll adjustments'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='GeoUnit'
	SET @Description='The geographical units available in the system'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Locality'
	SET @Description='Whether a location is considered to be rural or urban'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Sex'
	SET @Description='The sex of a person'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;
	
	SET @Code='Target Group'
	SET @Description='Associates a beneficiary with a particular enrolment group'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Member Role'
	SET @Description='The household member role'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Relationship'
	SET @Description='How a member relates to the beneficiary'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='System Right'
	SET @Description='MIS system right'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Financial Period'
	SET @Description='Programme Financial Period'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Livelihood'
	--SET @Description='Various kinds of livelihoods'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Ailment'
	--SET @Description='Various kinds of ailements'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Disability'
	--SET @Description='Various kinds of ailements'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Education Level'
	--SET @Description='Various highest levels of education'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Level'
	--SET @Description='Various levels from which a grievance is reported'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Type'
	--SET @Description='Various types of grievances'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Reporting Channel'
	--SET @Description='The medium used to relay information'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Complainant Type'
	--SET @Description='Various types of grievance complainants'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Resolution'
	--SET @Description='All that is done to reported grievances'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Age Bracket'
	--SET @Description='A persons age bracket'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Illiteracy Reason'
	--SET @Description='Reasons for not attending school'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
			
	--SET @Code='Orphanhood'
	--SET @Description='Various types of orphanhood statuses'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
	
	--SET @Code='Marital Status'
	--SET @Description='Marital statuses'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
	
	--SET @Code='Household Item'
	--SET @Description='Household Items'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Wall Material'
	--SET @Description='Wall Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Floor Material'
	--SET @Description='Floor Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Roof Material'
	--SET @Description='Roof Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Toilet Type'
	--SET @Description='Toilet Type'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Water Source'
	--SET @Description='Water Source'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Lighting Source'
	--SET @Description='Lighting Source'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Cooking Fuel'
	--SET @Description='Cooking Fuel'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Other Programme'
	--SET @Description='Other Programme'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Frequency'
	--SET @Description='Frequency'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Income'
	--SET @Description='Income'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Expenditure'
	--SET @Description='Expenditure'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Food Type'
	--SET @Description='Food Type'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Skipped Meals Range'
	--SET @Description='Skipped Meals Range'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Savings Storage'
	--SET @Description='How savings gets stored'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Calendar Months'
	SET @Description='The twelve calendar months'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Change Type'
	--SET @Description='The type of household update'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Change Justification'
	--SET @Description='The reason for household update'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


	---------------SYSTEM SETTINGS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='System Settings'
	SET @DetailCode='PWDEXPIRY'
	SET @Description='Login Password Lifespan Days'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ACCINACTIVEDAYS'
	SET @Description='The Maximum Days An A Login Can Remain Inactive Before Auto-Disabled'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECAPTCHASECRET'
	SET @Description='Login Recaptcha Secret'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECAPTCHAKEY'
	SET @Description='Login Recaptcha Key'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PWDRESETVALIDITY'
	SET @Description='Login Password Reset Link Lifespan Minutes'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1



	---------------HOUSEHOLD PROGRAMME STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='HHStatus'
	SET @DetailCode='REG'
	SET @Description='Registered'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGAPPR'
	SET @Description='Registration Approved'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGREJ'
	SET @Description='Registration Rejected'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='VALPASS'
	SET @Description='Registration Validation Passed'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='VALFAIL'
	SET @Description='Registration Validation Failed'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENROLLED'
	SET @Description='Enrolled and successfully carded by PSP'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ONPAYROLL'
	SET @Description='On Payroll'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='SUSPENDED'
	SET @Description='On Payroll Suspension'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='GRADUATED'
	SET @Description='Graduated'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='EXITED'
	SET @Description='Exited'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------PAYMENT/PAYROLL PROGRAMME STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Status'
	SET @DetailCode='CREATION'
	SET @Description='Created'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PREPAYROLL'
	SET @Description='Pre-Payroll Audit'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='FUNDSREQUEST'
	SET @Description='Funds Request'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='GENERATION'
	SET @Description='Generated'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='GENERATIONVAL'
	SET @Description='Validated'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENT'
	SET @Description='Payment'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENTSRECON'
	SET @Description='Payment Reconciled'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='FUNDSRECON'
	SET @Description='Funds Reconciled'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CLOSED'
	SET @Description='Closed'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




	---------------PAYMENT FREQUENCY-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Frequency'
	SET @DetailCode='BIMONTHLY'
	SET @Description='Bi-monthly'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	-------------SYSTEM RIGHTS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='System Right'
	SET @DetailCode='VIEW'
	SET @Description='Data View'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENTRY'
	SET @Description='Data Entry'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='MODIFIER'
	SET @Description='Data Modifier'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DELETION'
	SET @Description='Data Deletion'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='APPROVAL'
	SET @Description='Data Approval'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='EXPORT'
	SET @Description='Data Export'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DOWNLOAD'
	SET @Description='Data Download'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='UPLOAD'
	SET @Description='Data Upload'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------PAYMENT/PAYROLL ADJUSTMENTS-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Payment Adjustment'
	--SET @DetailCode='ARREARS'
	--SET @Description='Arrears/uncollected Amount'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='PENALTY'
	--SET @Description='Penalty Amount'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='COMPLIMENTARY'
	--SET @Description='Complimentary Payment Amount'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='OFFSET'
	--SET @Description='Payment Offset Amount'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-------------GEOUNITS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='GeoUnit'
	SET @DetailCode='PROVINCE'
	SET @Description='Province'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='COUNTY'
	SET @Description='County'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CONSTITUENCY'
	SET @Description='Constituency'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='WARD'
	SET @Description='Ward'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DISTRICT'
	SET @Description='District'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DIVISION'
	SET @Description='Division'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='LOCATION'
	SET @Description='Location'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUBLOCATION'
	SET @Description='Sub-Location'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------LOCALITY-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Locality'
	SET @DetailCode='0'
	SET @Description='Rural Area'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='1'
	SET @Description='Urban Area'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='2'
	SET @Description='Nairobi Area'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------SEX-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Sex'
	SET @DetailCode='M'
	SET @Description='Male'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='F'
	SET @Description='Female'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------MEMBER ROLE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Member Role'
	SET @DetailCode='MEMBER'
	SET @Description='Household member'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CAREGIVER'
	SET @Description='Caregiver'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='BENEFICIARY'
	SET @Description='Beneficiary'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------RELATIONSHIP-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Relationship'
	INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	SELECT @Id,T1.RelationshipId,T1.RelationshipName,ROW_NUMBER() OVER(ORDER BY RelationshipId),1,GETDATE()
	FROM [DCS].dbo.Relationship T1
	UNION
	SELECT @Id,(SELECT COUNT(RelationshipId) FROM [DCS].dbo.Relationship )+1,'Beneficiary',(SELECT COUNT(RelationshipId) FROM [DCS].dbo.Relationship )+1,1,GETDATE()

	---------------LIVELIHOOD-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Livelihood'
	--SET @DetailCode='Farming'
	--SET @Description='Farming'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Employment'
	--SET @Description='Employment'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Livelihood'
	--SET @DetailCode='Casual'
	--SET @Description='Casual'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------GRIEVANCE LEVEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Level'
	--SET @DetailCode='DISTRICT'
	--SET @Description='District'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='COUNTY'
	--SET @Description='County'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='NATIONAL'
	--SET @Description='National'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------GRIEVANCE TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.GrievanceTypeId,T1.GrievanceTypeName,ROW_NUMBER() OVER(ORDER BY GrievanceTypeId),1,GETDATE()
	--FROM [DCS].dbo.GrievanceType T1

	---------------REPORTING CHANNEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Reporting Channel'
	--SET @DetailCode='TELEPHONE'
	--SET @Description='via Telephone'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='TEXT'
	--SET @Description='via Short Text Message'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='LETTER'
	--SET @Description='via Letter'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='EMAIL'
	--SET @Description='via Email'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='IN-PERSON'
	--SET @Description='in Person'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------COMPLAINANT TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Complainant Type'
	--SET @DetailCode='INDIVIDUAL'
	--SET @Description='Individual / Household complainant'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='COMMUNITY'
	--SET @Description='Community Complainant'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='SENATE'
	--SET @Description='Senate Complainant'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='PARLIAMENT'
	--SET @Description='Parliament Complainant'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------GRIEVANCE RESOLUTION-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Resolution'
	--SET @DetailCode='REFERRAL'
	--SET @Description='Refferal'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='ACTION'
	--SET @Description='Other Action'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='APPEAL'
	--SET @Description='Appeal'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='RESOLVE'
	--SET @Description='Resolve'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------AGE BRACKET-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Age Bracket'
	--SET @DetailCode='0-5'
	--SET @Description='0 - 5 years'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='6-18'
	--SET @Description='6 - 18 years'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='19-24'
	--SET @Description='19 - 24 years'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='25-34'
	--SET @Description='25 - 34 years'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='35-44'
	--SET @Description='35 - 44 years'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='45-54'
	--SET @Description='45 - 54 years'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='55-64'
	--SET @Description='55 - 64 years'
	--SET @OrderNo=7
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='65-74'
	--SET @Description='65 - 74 years'
	--SET @OrderNo=8
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='75-84'
	--SET @Description='75 - 84 years'
	--SET @OrderNo=9
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='>84'
	--SET @Description='Above 84 years'
	--SET @OrderNo=10
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------AILMENT TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Ailment'
	--SET @DetailCode='Tuberculosis'
	--SET @Description='TB'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------DISABILITY-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Disability'
	--SET @DetailCode='Mental Disable'
	--SET @Description='Mental Disability'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Physical Disable'
	--SET @Description='Physical Disability'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------EDUCATION LEVEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Education Level'
	--SET @DetailCode='Form 4'
	--SET @Description='Form 4'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------ILLITERACY REASON-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Illiteracy Reason'
	--SET @DetailCode='Lack of schools'
	--SET @Description='Lack of schools'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------ORPHANHOOD-------------

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan None'
	--SET @Description='None'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan Father'
	--SET @Description='Father'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan Mother'
	--SET @Description='Mother'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Orphan Both'
	--SET @Description='Both'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------MARITAL STATUS-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Marital Status'
	--SET @DetailCode='Never Married'
	--SET @Description='Never Married'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Married'
	--SET @Description='Married'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Divorced'
	--SET @Description='Divorced'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------HOUSEHOLD ITEM-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Household Item'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=10

	-----------------WALL TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Wall Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------FLOOR TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Floor Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------ROOF TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Roof Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------TOILET TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Toilet Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=1

	-----------------WATER SOURCE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Water Source'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=2

	-----------------LIGHTING SOURCE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Lighting Source'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=3

	-----------------COOKING FUEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Cooking Fuel'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=4

	---------------OTHER PROGRAMME-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Other Programme'
	SET @DetailCode='CT-OVC'
	SET @Description='CT-OVC'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='OPCT'
	SET @Description='OPCT'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PwSD-CT'
	SET @Description='PwSD-CT'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='HSNP'
	SET @Description='HSNP'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='WFP'
	SET @Description='WFP'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------FREQUENCY-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Frequency'
	--SET @DetailCode='Daily'
	--SET @Description='Daily'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Weekly'
	--SET @Description='Weekly'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Fortnightly'
	--SET @Description='Fortnightly'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Monthly'
	--SET @Description='Monthly'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Bi-Monthly'
	--SET @Description='Bi-Monthly'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Quarterly'
	--SET @Description='Quarterly'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Half-Yearly'
	--SET @Description='Half-Yearly'
	--SET @OrderNo=7
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Annually'
	--SET @Description='Annually'
	--SET @OrderNo=8
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------INCOME-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Income'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=8

	-----------------EXPENDITURE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Expenditure'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=9

	-----------------FOOD TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Food Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=11

	-----------------SKIPPED MEALS RANGE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Skipped Meals Range'
	--SET @DetailCode='1 - 2'
	--SET @Description='1 - 2'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='3 - 4'
	--SET @Description='3 - 4'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='5 - 6'
	--SET @Description='5 - 6'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='7 And Above'
	--SET @Description='7 And Above'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------SAVINGS STORAGE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Savings Storage'
	--SET @DetailCode='Bank'
	--SET @Description='Cash in the bank'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------CALENDAR MONTHS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Calendar Months'
	SET @DetailCode='1'
	SET @Description='January'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='2'
	SET @Description='February'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='3'
	SET @Description='March'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='4'
	SET @Description='April'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='5'
	SET @Description='May'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='6'
	SET @Description='June'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='7'
	SET @Description='July'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='8'
	SET @Description='August'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='9'
	SET @Description='September'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='10'
	SET @Description='October'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='11'
	SET @Description='November'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='12'
	SET @Description='December'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------CHANGE TYPES-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Change Type'
	--SET @DetailCode='Dissolution'
	--SET @Description='Dissolution'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Caregiver'
	--SET @Description='Caregiver'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Exlusion'
	--SET @Description='Exlusion'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Correction'
	--SET @Description='Correction'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Graduation'
	--SET @Description='Graduation'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Reversal'
	--SET @Description='Reversal'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------CHANGE JUSTIFICATION-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Change Justification'
	--SET @DetailCode='Death'
	--SET @Description='Deceased'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Fraud'
	--SET @Description='Fraud'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SELECT * FROM SystemCode ORDER BY Id
	SELECT * FROM SystemCodeDetail ORDER BY Id
	SELECT * FROM UserGroupProfile ORDER BY Id
END



IF @MIG_STAGE = 2	--2. GEO-SPATIAL MAPPING
BEGIN

	INSERT INTO GeoMaster(Name,[Description],CreatedBy,CreatedOn)
	SELECT 'Census 2009 Standard','Census 2009 Geographic Location Mapping',1 AS CreatedBy,GETDATE() AS CreatedOn
	;

	INSERT INTO County(GeoMasterId,Code,Name,CreatedBy,CreatedOn)
	SELECT DISTINCT 1 AS GeoMasterId,T1.CountyCode,T1.CountyName,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 LEFT JOIN County T3 ON T1.CountyCode COLLATE DATABASE_DEFAULT=T3.Code COLLATE DATABASE_DEFAULT
	WHERE T3.Id IS NULL
	ORDER BY T1.CountyCode
	;

	INSERT INTO Constituency(Code,Name,CountyId,CreatedBy,CreatedOn)
	SELECT DISTINCT T1.ConstituencyCode,T1.ConstituencyName,T2.Id AS CountyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 INNER JOIN County T2 ON T1.CountyCode COLLATE DATABASE_DEFAULT=T2.Code COLLATE DATABASE_DEFAULT
				LEFT JOIN Constituency T4 ON T1.ConstituencyCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
	WHERE T4.Id IS NULL
	ORDER BY T1.ConstituencyCode
	;

	INSERT INTO District(GeoMasterId,Code,Name,CreatedBy,CreatedOn)
	SELECT DISTINCT 1 AS GeoMasterId,T1.DistrictCode,T1.DistrictName,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 LEFT JOIN District T4 ON T1.DistrictCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
	WHERE T4.Id IS NULL
	ORDER BY T1.DistrictCode
	;

	INSERT INTO CountyDistrict(CountyId,DistrictId,CreatedBy,CreatedOn)
	SELECT DISTINCT T2.Id AS CountyId,T4.Id AS DistrictId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 INNER JOIN County T2 ON T2.GeoMasterId=1 AND T1.CountyCode COLLATE DATABASE_DEFAULT=T2.Code COLLATE DATABASE_DEFAULT
				 INNER JOIN District T4 ON T4.GeoMasterId=1 AND T1.DistrictCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
				 LEFT JOIN CountyDistrict T5 ON T2.Id=T5.CountyId AND T4.Id=T5.DistrictId
	WHERE T5.CountyId IS NULL
	;

	INSERT INTO Division(Code,Name,CountyDistrictId,CreatedBy,CreatedOn)
	SELECT DISTINCT T1.DivisionCode,T1.DivisionName,T6.Id AS CountyDistrictId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 INNER JOIN District T2 ON T1.DistrictCode COLLATE DATABASE_DEFAULT=T2.Code COLLATE DATABASE_DEFAULT
			    INNER JOIN County T3 ON T1.CountyCode COLLATE DATABASE_DEFAULT=T3.Code COLLATE DATABASE_DEFAULT
				INNER JOIN CountyDistrict T6 ON T2.Id=T6.DistrictId AND T3.Id=T6.CountyId
				LEFT JOIN Division T5 ON T1.DivisionCode COLLATE DATABASE_DEFAULT=T5.Code COLLATE DATABASE_DEFAULT
	WHERE T5.Id IS NULL
	ORDER BY T1.DivisionCode
	;

	INSERT INTO Location(Code,Name,DivisionId,CreatedBy,CreatedOn)
	SELECT DISTINCT T1.LocationCode,T1.LocationName,T2.Id AS DivisionId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 INNER JOIN Division T2 ON T1.DivisionCode COLLATE DATABASE_DEFAULT=T2.Code COLLATE DATABASE_DEFAULT
				INNER JOIN CountyDistrict T7 ON T2.CountyDistrictId=T7.Id
				INNER JOIN District T3 ON T7.DistrictId=T3.Id AND T1.DistrictCode COLLATE DATABASE_DEFAULT=T3.Code COLLATE DATABASE_DEFAULT
			    INNER JOIN County T4 ON T7.CountyId=T4.Id AND T1.CountyCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
				LEFT JOIN Location T6 ON T1.LocationCode COLLATE DATABASE_DEFAULT=T6.Code COLLATE DATABASE_DEFAULT
	WHERE T6.Id IS NULL
	ORDER BY T1.LocationCode
	;
		
	INSERT INTO SubLocation(Code,Name,LocalityId,LocationId,ConstituencyId,CreatedBy,CreatedOn)
	SELECT DISTINCT T1.SubLocationCode,T1.SubLocationName,T8.Id AS Category,T2.Id AS LocationId,T7.Id AS ConstituencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM (	SELECT CONVERT(varchar(50),CONVERT(int,TT1.SUBLID)) AS SubLocationCode,TT1.SUBLOCATION AS SubLocationName,CONVERT(varchar(50),CONVERT(int,TT1.LOCID)) AS LocationCode,TT1.LOCATION AS LocationName,CONVERT(varchar(50),TT1.DIVID) AS DivisionCode,TT1.DIVISION AS DivisionName,CONVERT(varchar(50),TT1.DISTID) AS DistrictCode,TT1.DISTRICT AS DistrictName,CONVERT(varchar(50),TT1.COUNTID) AS CountyCode,TT1.[COUNTY (KNBS)] AS CountyName,TT1.COUNTY AS CountyName2,CONVERT(varchar(50),TT1.CONSTID) AS ConstituencyCode,TT1.CONSTNAME AS ConstituencyName,CONVERT(varchar(50),TT1.PROVID) AS ProvinceCode,TT1.PROVINCE AS ProvinceName,CASE WHEN(TT2.SUBLID IS NULL) THEN CASE(TT1.STATUSID) WHEN 2 THEN 1 ELSE 0 END ELSE 2 END AS StatusId,STATUS AS Status,TT1.TOWN AS Town
			FROM [GeoLocations].dbo.GeoLocationsCensus2009 TT1 LEFT JOIN (SELECT SUBLID FROM [GeoLocations].dbo.GeoLocationsCensus2009 WHERE COUNTID=101) TT2 ON TT1.SUBLID=TT2.SUBLID
		   ) T1 INNER JOIN Location T2 ON T1.LocationCode COLLATE DATABASE_DEFAULT=T2.Code COLLATE DATABASE_DEFAULT
				INNER JOIN Division T3 ON T1.DivisionCode COLLATE DATABASE_DEFAULT=T3.Code COLLATE DATABASE_DEFAULT AND T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T10 ON T3.CountyDistrictId=T10.Id
				INNER JOIN District T4 ON T10.DistrictId=T4.Id AND T1.DistrictCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
			    INNER JOIN County T5 ON T10.CountyId=T5.Id AND T1.CountyCode COLLATE DATABASE_DEFAULT=T5.Code COLLATE DATABASE_DEFAULT
				INNER JOIN Constituency T7 ON T1.ConstituencyCode COLLATE DATABASE_DEFAULT=T7.Code COLLATE DATABASE_DEFAULT AND T5.Id=T7.CountyId
				INNER JOIN (SELECT TT1.Id,TT1.Code
							FROM SystemCodeDetail TT1 INNER JOIN SystemCode TT2 ON TT1.SystemCodeId=TT2.Id
							WHERE TT2.Code='Locality'
							) T8 ON CONVERT(varchar(10),T1.StatusId)=T8.Code
				LEFT JOIN SubLocation T9 ON T1.SubLocationCode COLLATE DATABASE_DEFAULT=T9.Code COLLATE DATABASE_DEFAULT
	WHERE T9.Id IS NULL
	ORDER BY T1.SubLocationCode
	
	SELECT T7.Code AS ConstituencyCode,T7.Name AS ConstituencyName,T2.Code AS CountyCode,T2.Name AS CountyName,T3.Code AS DistrictCode,T3.Name AS DistrictName,T4.Code AS DivisionCode,T4.Name AS DivisionName,T5.Code AS LocationCode,T5.Name AS LocationName,T6.Code AS SubLocationCode,T6.Name AS SubLocationName,T8.Code AS LocalityCode,T8.[Description] AS LocalityDescription
	FROM County T2 LEFT JOIN CountyDistrict T1 ON T2.Id=T1.CountyId
					LEFT JOIN District T3 ON T1.DistrictId=T3.Id
					LEFT JOIN Division T4 ON T1.Id=T4.CountyDistrictId
					LEFT JOIN Location T5 ON T4.Id=T5.DivisionId
					LEFT JOIN SubLocation T6 ON T5.Id=T6.LocationId
					LEFT JOIN Constituency T7 ON T6.ConstituencyId=T7.Id
					LEFT JOIN SystemCodeDetail T8 ON T6.LocalityId=T8.Id
	ORDER BY CountyCode,DistrictCode,DivisionCode,LocationCode,SubLocationCode,ConstituencyCode
END


