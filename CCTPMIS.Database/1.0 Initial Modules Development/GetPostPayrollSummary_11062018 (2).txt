IF NOT OBJECT_ID('GetPostPayrollSummary') IS NULL	DROP PROC GetPostPayrollSummary
GO
CREATE PROC GetPostPayrollSummary
	@PaymentCycleId int
AS
BEGIN
	SELECT COUNT(T1.HhId) AS TotalPaymentHhs
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 1 ELSE 0 END) AS FailedPayments
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentExceptionsHhs
		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs
		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs
		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentCardHhs
		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PaymentIneligibleHhs
		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentSuspiciousHhs
		,SUM(T1.TrxAmount) AS TotalPaymentAmount
	FROM Payment T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId
END
GO
