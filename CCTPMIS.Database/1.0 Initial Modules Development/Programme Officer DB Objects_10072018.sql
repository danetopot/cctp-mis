

IF NOT OBJECT_ID('ProgrammeOfficer') IS NULL	DROP TABLE ProgrammeOfficer
GO

CREATE TABLE ProgrammeOfficer(
	Id int NOT NULL IDENTITY(1,1)
   ,UserId int NOT NULL
   ,CountyId int NULL
   ,ConstituencyId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,DeactivatedBy int NULL
   ,DeactivatedOn datetime NULL
   ,DeactivatedApvBy int NULL
   ,DeactivatedApvOn datetime NULL
   ,CONSTRAINT PK_ProgrammeOfficer PRIMARY KEY (Id)
   ,CONSTRAINT FK_ProgrammeOfficer_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_County_CountyId FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_DeactivatedBy FOREIGN KEY (DeactivatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_DeactivatedApvBy FOREIGN KEY (DeactivatedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO





IF NOT OBJECT_ID('AddEditProgrammeOfficer') IS NULL	DROP PROC AddEditProgrammeOfficer
GO
CREATE PROC AddEditProgrammeOfficer
	@Id int=NULL
   ,@ProgrammeOfficerUserId int
   ,@CountyId int
   ,@ConstituencyId int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@ProgrammeOfficerUserId)
		SET @ErrorMsg='Please specify valid ProgrammeOfficerUserId parameter'
	ELSE IF ISNULL(@CountyId,0)<>0 AND NOT EXISTS(SELECT 1 FROM County WHERE Id=@CountyId)
		SET @ErrorMsg='Please specify valid CountyId parameter'
	ELSE IF ISNULL(@ConstituencyId,0)<>0 AND NOT EXISTS(SELECT 1 FROM Constituency T1 INNER JOIN County T2 ON T1.CountyId=T2.Id WHERE T1.Id=@ConstituencyId AND T2.Id=@CountyId)
		SET @ErrorMsg='Please specify valid ConstituencyId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='Once the Programme Officer location has been approved it cannot be modified. Deactivate and make a new assignment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.UserId=@ProgrammeOfficerUserId
		   ,T1.CountyId=@CountyId
		   ,T1.ConstituencyId=@ConstituencyId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ProgrammeOfficer T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO ProgrammeOfficer(UserId,CountyId,ConstituencyId,CreatedBy,CreatedOn)
		SELECT @ProgrammeOfficerUserId,@CountyId,@ConstituencyId,@UserId,GETDATE()
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('ApproveProgrammeOfficer') IS NULL	DROP PROC ApproveProgrammeOfficer
GO
CREATE PROC ApproveProgrammeOfficer
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='The specified programme officer id location has already been approved'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('DeactivateProgrammeOfficer') IS NULL	DROP PROC DeactivateProgrammeOfficer
GO
CREATE PROC DeactivateProgrammeOfficer
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='The specified programme officer id location is not active'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedBy>0)
		SET @ErrorMsg='The specified programme officer id has already been deactivated'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.DeactivatedBy=@UserId
	   ,T1.DeactivatedOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('ApproveDeactivateProgrammeOfficer') IS NULL	DROP PROC ApproveDeactivateProgrammeOfficer
GO
CREATE PROC ApproveDeactivateProgrammeOfficer
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedBy>0)
		SET @ErrorMsg='The specified programme officer id is not ready for deactivation approval'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedApvBy>0)
		SET @ErrorMsg='The specified programme officer id deactivation has already been approved'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.DeactivatedApvBy=@UserId
	   ,T1.DeactivatedApvOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO
