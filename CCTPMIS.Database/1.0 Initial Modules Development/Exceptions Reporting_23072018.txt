
IF NOT OBJECT_ID('GetPrepayrollAudit') IS NULL	DROP PROC GetPrepayrollAudit
GO
IF NOT OBJECT_ID('GetPrepayrollSummary') IS NULL	DROP PROC GetPrepayrollSummary
GO
CREATE PROC GetPrepayrollSummary
	@PaymentCycleId int
   ,@ProgrammeId tinyint
AS
BEGIN
	SELECT MAX(T2.ExceptionsFileId) AS ExceptionsFileId,MAX(T2.EnrolledHHs) AS EnroledHhs,COUNT(T1.HhId) AS EnrolmentGroupHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN 1 ELSE 0 END) AS PayrollHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.EntitlementAmount,0) ELSE 0 END) AS PayrollEntitlementAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.AdjustmentAmount,0) ELSE 0 END) AS PayrollAdjustmentAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN (ISNULL(T1.EntitlementAmount,0)+ISNULL(T1.AdjustmentAmount,0)) ELSE 0 END) AS PayrollAmount
		--EXCEPTION:INVALID NATIONAL ID
		,SUM(CASE WHEN(TT3.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidNationalIDHhs
		,SUM(CASE WHEN(TT3.PaymentCycleId>0 AND TT3.ExceptionTypeCode='INVALIDBENEID') THEN 1 ELSE 0 END) AS InvalidNationalIDHhs_BENE
		,SUM(CASE WHEN(TT3.PaymentCycleId>0 AND TT3.ExceptionTypeCode='INVALIDCGID') THEN 1 ELSE 0 END) AS InvalidNationalIDHhs_CG
		,SUM(CASE WHEN(TA3.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidNationalIDHhs
		,SUM(CASE WHEN(TA3.PaymentCycleId>0 AND TA3.ExceptionTypeCode='INVALIDBENEID') THEN 1 ELSE 0 END) AS ActionedInvalidNationalIDHhs_BENE
		,SUM(CASE WHEN(TA3.PaymentCycleId>0 AND TA3.ExceptionTypeCode='INVALIDBENEID') THEN 1 ELSE 0 END) AS ActionedInvalidNationalIDHhs_CG
		--EXCEPTION:DUPLICATE NATIONAL ID
		,SUM(CASE WHEN(TT4.PaymentCycleId>0) THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs
		,SUM(CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs_BENEIN
		,SUM(CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs_BENEACC
		,SUM(CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATECGIDIN') THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs_CGIN
		,SUM(CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATECGIDACC') THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs_CGACC
		,SUM(CASE WHEN(TA4.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs
		,SUM(CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs_BENEIN
		,SUM(CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs_BENEACC
		,SUM(CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATECGIDIN') THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs_CGIN
		,SUM(CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATECGIDACC') THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs_CGACC
		--EXCEPTION:INVALID PAYMENT ACCOUNT
		,SUM(CASE WHEN(TT5.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidPaymentAccountHhs
		,SUM(CASE WHEN(TA5.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidPaymentAccountHhs
		--EXCEPTION:INVALID PAYMENT CARD
		,SUM(CASE WHEN(TT6.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidPaymentCardHhs
		,SUM(CASE WHEN(TA6.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidPaymentCardHhs
		--EXCEPTION:INELIGIBLE
		,SUM(CASE WHEN(TT7.PaymentCycleId>0) THEN 1 ELSE 0 END) AS IneligibleHhs
		,SUM(CASE WHEN(TT7.PaymentCycleId>0 AND TT7.ExceptionTypeCode='INELIGIBLEBENE') THEN 1 ELSE 0 END) AS IneligibleHhs_BENE
		,SUM(CASE WHEN(TT7.PaymentCycleId>0 AND TT7.ExceptionTypeCode='INELIGIBLECG') THEN 1 ELSE 0 END) AS IneligibleHhs_CG
		,SUM(CASE WHEN(TA7.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedIneligibleHhs
		,SUM(CASE WHEN(TA7.PaymentCycleId>0 AND TA7.ExceptionTypeCode='INELIGIBLEBENE') THEN 1 ELSE 0 END) AS ActionedIneligibleHhs_BENE
		,SUM(CASE WHEN(TA7.PaymentCycleId>0 AND TA7.ExceptionTypeCode='INELIGIBLECG') THEN 1 ELSE 0 END) AS ActionedIneligibleHhs_CG
		--EXCEPTION:SUSPICIOUS PAYMENT
		,SUM(CASE WHEN(TT8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS SuspiciousPaymentHhs
		,SUM(CASE WHEN(TA8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedSuspiciousPaymentHhs
		,MAX(CASE WHEN(TA3.PaymentCycleId>0
					   OR TA4.PaymentCycleId>0
					   OR TA5.PaymentCycleId>0
					   OR TA6.PaymentCycleId>0
					   OR TA7.PaymentCycleId>0
					   OR TA8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS PayrollHasBeenActioned
	FROM Prepayroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedApvBy IS NULL) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HHId
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT3 ON T1.PaymentCycleId=TT3.PaymentCycleId AND T1.ProgrammeId=TT3.ProgrammeId AND T1.HhId=TT3.HHId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedApvBy>0) TA3 ON T1.PaymentCycleId=TA3.PaymentCycleId AND T1.ProgrammeId=TA3.ProgrammeId AND T1.HhId=TA3.HHId
					   
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedApvBy IS NULL) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HHId
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT4 ON T1.PaymentCycleId=TT4.PaymentCycleId AND T1.ProgrammeId=TT4.ProgrammeId AND T1.HhId=TT4.HHId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedApvBy>0) TA4 ON T1.PaymentCycleId=TA4.PaymentCycleId AND T1.ProgrammeId=TA4.ProgrammeId AND T1.HhId=TA4.HHId
					   
					   LEFT JOIN PrepayrollInvalidPaymentAccount T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId AND T5.ActionedApvBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentAccount TT5 ON T1.PaymentCycleId=TT5.PaymentCycleId AND T1.ProgrammeId=TT5.ProgrammeId AND T1.HhId=TT5.HhId
								LEFT JOIN PrepayrollInvalidPaymentAccount TA5 ON T1.PaymentCycleId=TA5.PaymentCycleId AND T1.ProgrammeId=TA5.ProgrammeId AND T1.HhId=TA5.HhId AND TA5.ActionedApvBy>0
					   
					   LEFT JOIN PrepayrollInvalidPaymentCard T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId AND T6.ActionedApvBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentCard TT6 ON T1.PaymentCycleId=TT6.PaymentCycleId AND T1.ProgrammeId=TT6.ProgrammeId AND T1.HhId=TT6.HhId
								LEFT JOIN PrepayrollInvalidPaymentCard TA6 ON T1.PaymentCycleId=TA6.PaymentCycleId AND T1.ProgrammeId=TA6.ProgrammeId AND T1.HhId=TA6.HhId AND TA6.ActionedApvBy>0
					   
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId AND T7.ActionedApvBy IS NULL
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT7 ON T1.PaymentCycleId=TT7.PaymentCycleId AND T1.ProgrammeId=TT7.ProgrammeId AND T1.HhId=TT7.HhId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedApvBy FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TA7 ON T1.PaymentCycleId=TA7.PaymentCycleId AND T1.ProgrammeId=TA7.ProgrammeId AND T1.HhId=TA7.HhId AND TA7.ActionedApvBy>0
					   
					   LEFT JOIN PrepayrollSuspicious T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.HhId=T8.HhId AND T8.ActionedApvBy IS NULL
							LEFT JOIN PrepayrollSuspicious TT8 ON T1.PaymentCycleId=TT8.PaymentCycleId AND T1.ProgrammeId=TT8.ProgrammeId AND T1.HhId=TT8.HhId
								LEFT JOIN PrepayrollSuspicious TA8 ON T1.PaymentCycleId=TA8.PaymentCycleId AND T1.ProgrammeId=TA8.ProgrammeId AND T1.HhId=TA8.HhId AND TA8.ActionedApvBy>0
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
END
GO




IF NOT OBJECT_ID('GetPostPayrollSummary') IS NULL	DROP PROC GetPostPayrollSummary
GO
CREATE PROC GetPostPayrollSummary
	@PaymentCycleId int
AS
BEGIN
	SELECT COUNT(T8.HhId) AS TotalPayrollHhs
		,SUM(T8.PaymentAmount) AS TotalPayrollAmount
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments
		,SUM(T1.TrxAmount) AS SuccessfulPaymentsAmount
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 1 ELSE 0 END) AS FailedPayments
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN T8.PaymentAmount ELSE 0 END) AS FailedPaymentsAmount
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentExceptionsHhs

		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs
		,SUM(CASE WHEN(T2.HhId>1 AND T2.ExceptionTypeCode='INVALIDBENEID') THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs_BENE
		,SUM(CASE WHEN(T2.HhId>1 AND T2.ExceptionTypeCode='INVALIDCGID') THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs_CG

		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_BENEIN
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_BENEACC
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATECGIDIN') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_CGIN
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATECGIDACC') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_CGAC

		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentCardHhs

		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PaymentIneligibleHhs
		,SUM(CASE WHEN(T6.HhId>1 AND T2.ExceptionTypeCode='INELIGIBLEBENE') THEN 1 ELSE 0 END) AS PaymentIneligibleHhs_BENE
		,SUM(CASE WHEN(T6.HhId>1 AND T2.ExceptionTypeCode='INELIGIBLECG') THEN 1 ELSE 0 END) AS PaymentIneligibleHhs_CG

		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentSuspiciousHhs
	FROM Payment T1 LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
					INNER JOIN Payroll T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.HhId=T8.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId
END
GO