IF NOT OBJECT_ID('AddEditPaymentCycle') IS NULL	DROP PROC AddEditPaymentCycle
GO
CREATE PROC AddEditPaymentCycle
	@Id int=NULL
   ,@ProgrammeId smallint
   ,@Description nvarchar(128)
   ,@FinancialYearId int
   ,@FromMonthId int
   ,@ToMonthId int
   ,@EnrolmentGroupsXML XML
   ,@FilePath nvarchar(128)
   ,@HasSupportingDoc bit=0
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentEnrolmentGroup TABLE(
		Id int
	   ,EnrolmentGroupId int
	   ,PaymentAmount money
	)

	DECLARE @HHStatus_EnrolledProgCode varchar(20)
	DECLARE @HHStatus_EnrolledPSPCode varchar(20)
	DECLARE @HHStatus_PSPCardedCode varchar(20)
	DECLARE @HHStatus_OnPayrollCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @CalendarMonthCode varchar(20)
	DECLARE @EnrolmentGroupCode varchar(20)
	DECLARE @PaymentCycleStageCode varchar(20)
	DECLARE @PaymentCycleStageCode_PAYMENTCYCLEAPV varchar(20)
	DECLARE @PaymentCycleStageId int
	DECLARE @PayableHHs int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @Exists bit
	DECLARE @NoOfRows int
	DECLARE @EnrolmentGroups1 varchar(128), @EnrolmentGroups2 varchar(128)
	DECLARE @ErrorMsg varchar(128)



	SET @HHStatus_EnrolledProgCode = 'ENRL'
	SET @HHStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HHStatus_PSPCardedCode = 'PSPCARDED'
	SET @HHStatus_OnPayrollCode = 'ONPAY'
	SET @FinancialYearCode = 'Financial Year'
	SET @CalendarMonthCode = 'Calendar Months'
	SET @EnrolmentGroupCode = 'Enrolment Group'
	SET @PaymentCycleStageCode = 'Payment Stage'
	SET @PaymentCycleStageCode_PAYMENTCYCLEAPV = 'PAYMENTCYCLEAPV'

	INSERT INTO @tblPaymentEnrolmentGroup(Id,EnrolmentGroupId,PaymentAmount)
	SELECT T1.Id,T1.EnrolmentGroupId,PaymentAmount
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(EnrolmentGroupId)[1]','int') AS EnrolmentGroupId
			  ,U.R.value('(PaymentAmount)[1]','money') AS PaymentAmount
		FROM @EnrolmentGroupsXML.nodes('EnrolmentGroups/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.EnrolmentGroupId=T2.Id
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@EnrolmentGroupCode

	SELECT @EnrolmentGroups1=COALESCE(@EnrolmentGroups1+','+CONVERT(varchar(20),T2.EnrolmentGroupId),@EnrolmentGroups1) FROM PaymentCycle T1 INNER JOIN PaymentEnrolmentGroup T2 ON T1.Id=T2.PaymentCycleId WHERE T1.ProgrammeId=@ProgrammeId AND T1.FinancialYearId=@FinancialYearId AND T1.FromMonthId=@FromMonthId AND T1.ToMonthId=@ToMonthId AND @Id=0 ORDER BY T2.EnrolmentGroupId ASC
	SELECT @EnrolmentGroups2=COALESCE(@EnrolmentGroups2+','+CONVERT(varchar(20),EnrolmentGroupId),@EnrolmentGroups1) FROM @tblPaymentEnrolmentGroup ORDER BY EnrolmentGroupId ASC
	SELECT @PaymentCycleStageId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@PaymentCycleStageCode AND T1.Code=@PaymentCycleStageCode_PAYMENTCYCLEAPV
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId
	SELECT @ProgrammeId=Id FROM Programme WHERE Id=@ProgrammeId

	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	IF ISNULL(@Description,'')=''
		SET @ErrorMsg='Please specify valid Description parameter'
	IF NOT EXISTS(SELECT 1 FROM @tblPaymentEnrolmentGroup)
		SET @ErrorMsg='Please specify valid EnrolmentGroupsXML parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@FinancialYearId AND T2.Code=@FinancialYearCode)
		SET @ErrorMsg='Please specify valid FinancialYear parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@FromMonthId AND T2.Code=@CalendarMonthCode)
		SET @ErrorMsg='Please specify valid FromMonth parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@ToMonthId AND T2.Code=@CalendarMonthCode)
		SET @ErrorMsg='Please specify valid ToMonth parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@Id,0)=0 AND @EnrolmentGroups1=@EnrolmentGroups2
		SET @ErrorMsg='The Payment Cycle already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @PayableHHs=COUNT(T1.Id)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
	WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HHStatus_EnrolledProgCode,@HHStatus_EnrolledPSPCode,@HHStatus_PSPCardedCode,@HHStatus_OnPayrollCode)

	BEGIN TRAN

	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.[Description]=@Description
		   ,T1.FinancialYearId=@FinancialYearId
		   ,T1.FromMonthId=@FromMonthId
		   ,T1.ToMonthId=@ToMonthId
		   ,T1.EnrolledHHs=@PayableHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PaymentCycle T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCycle(ProgrammeId,[Description],FinancialYearId,FromMonthId,ToMonthId,EnrolledHHs,PaymentStageId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@Description,@FinancialYearId,@FromMonthId,@ToMonthId,@PayableHHs,@PaymentCycleStageId,@UserId,GETDATE()

		SELECT @Id=Id FROM PaymentCycle WHERE ProgrammeId=@ProgrammeId AND FinancialYearId=@FinancialYearId AND FromMonthId=@FromMonthId AND ToMonthId=@ToMonthId
	END
	
	DELETE T1
	FROM PaymentEnrolmentGroup T1 
	WHERE T1.PaymentCycleId=@Id

	INSERT INTO PaymentEnrolmentGroup(PaymentCycleId,EnrolmentGroupId,PaymentAmount)
	SELECT @Id,EnrolmentGroupId,PaymentAmount
	FROM @tblPaymentEnrolmentGroup

	--RECORDING THE FILE
	IF @HasSupportingDoc=1
	BEGIN
		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @FileName='SUPPORT'+'_'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@FromMonthId)+'-'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@ToMonthId)+'_'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SET @FileExtension='.pdf'

		INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
		SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2 AND CreatedBy=@UserId

		UPDATE T1
		SET T1.FileCreationId=@FileCreationId
		FROM PaymentCycle T1
		WHERE T1.Id=@Id
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows,@Id AS PaymentCycleId,T2.Id AS PaymentCycleStageId,T2.[Description] AS PaymentCycleStage,@FileName+@FileExtension AS SupportingDoc
		FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id
		WHERE T1.Id=@Id
	END
END
GO



IF NOT OBJECT_ID('ApprovePaymentCycle') IS NULL	DROP PROC ApprovePaymentCycle
GO
CREATE PROC ApprovePaymentCycle
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycle WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycle T1
	WHERE T1.Id=@Id
	
	SELECT @@ROWCOUNT AS NoOfRows
END
GO



IF NOT OBJECT_ID('DeletePaymentCycle') IS NULL	DROP PROC DeletePaymentCycle
GO
CREATE PROC DeletePaymentCycle
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @PaymentCycleStatusCode_PAYMENTCYCLEAPV varchar(20)
	DECLARE @PaymentCycleStatusCode_PREPAYROLL varchar(20)
	DECLARE @PaymentCycleStatusCode_PREPAYROLLAPV varchar(20)
	DECLARE @ErrorMsg varchar(128)

	SET @PaymentCycleStatusCode_PAYMENTCYCLEAPV='PREPAYROLL'
	SET @PaymentCycleStatusCode_PREPAYROLL='PREPAYROLL'
	SET @PaymentCycleStatusCode_PREPAYROLLAPV='PREPAYROLLAPV'
	SELECT @Id=Id FROM PaymentCycle WHERE Id=@Id
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF NOT EXISTS(SELECT 1 FROM PaymentCycle WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id WHERE T1.Id=@Id AND T2.Code NOT IN(@PaymentCycleStatusCode_PAYMENTCYCLEAPV,@PaymentCycleStatusCode_PREPAYROLL,@PaymentCycleStatusCode_PREPAYROLLAPV))
		SET @ErrorMsg='Once the Payment Cycle Funds have been requested it cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	UPDATE T1
	SET T1.DeletedBy=@UserId
	   ,T1.DeletedOn=GETDATE() 
	FROM PaymentCycle T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
END
GO