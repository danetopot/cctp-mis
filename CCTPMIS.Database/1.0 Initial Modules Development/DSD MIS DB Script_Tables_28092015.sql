
/*
	 1. Identification
	 2. Screener
	 3. Bio-Data
	 4. Education
	 5. Dwelling
	 6. Assets
	 7. Business
	 8. Expenditure
	 9. Income
	10. Meals
	11. Control
	12. Summary


	CODES
	=====
	 1. Gender
	 2. Marrital Status
	 3. Illness Type
	 4. Disability Type
	 5. Target Group
	 6. Education Level
	 7. Occupation Category
	 8. Orphanhood
	 9. Member Relationship
	10. Non-Schooling Reasons
	11. Household Items
	12. Wall Construction Material
	13. Floor Construction Material
	14. Roof Construction Material
	15. Toilet Type
	16. Drinking Water Source
	17. Lighting Source
	18. Cooking Fuel Source
	19. Household Status
	20. Monthly Rental Range
	21. Livelihood Source
	22. Savings Storage Type
	23. Frequency
	24. Household Expenditure Type
	25. Household Income Type
	26. Household Nutrition Type
	27. Locality (of Rural or Urban only) 
	
*/

IF NOT OBJECT_ID('GrievanceAction') IS NULL	DROP TABLE GrievanceAction
GO

IF NOT OBJECT_ID('Grievance') IS NULL	DROP TABLE Grievance
GO

IF NOT OBJECT_ID('Payment') IS NULL	DROP TABLE Payment
GO

IF NOT OBJECT_ID('PrepayrollAudit') IS NULL	DROP TABLE PrepayrollAudit
GO

IF NOT OBJECT_ID('PaymentAdjustment') IS NULL	DROP TABLE PaymentAdjustment
GO

IF NOT OBJECT_ID('PaymentTargetGroup') IS NULL	DROP TABLE PaymentTargetGroup
GO

IF NOT OBJECT_ID('PaymentCycle') IS NULL	DROP TABLE PaymentCycle
GO

IF NOT OBJECT_ID('ExpansionPlan') IS NULL	DROP TABLE ExpansionPlan
GO

IF NOT OBJECT_ID('HouseholdNutrition') IS NULL	DROP TABLE HouseholdNutrition
GO

IF NOT OBJECT_ID('HouseholdIncome') IS NULL	DROP TABLE HouseholdIncome
GO

IF NOT OBJECT_ID('HouseholdExpenditure') IS NULL	DROP TABLE HouseholdExpenditure
GO

IF NOT OBJECT_ID('HouseholdBusiness') IS NULL	DROP TABLE HouseholdBusiness
GO

IF NOT OBJECT_ID('HouseholdAssets') IS NULL	DROP TABLE HouseholdAssets
GO

IF NOT OBJECT_ID('HouseholdItems') IS NULL	DROP TABLE HouseholdItems
GO

IF NOT OBJECT_ID('HouseholdCharacteristics') IS NULL	DROP TABLE HouseholdCharacteristics
GO

IF NOT OBJECT_ID('HouseholdMembers') IS NULL	DROP TABLE HouseholdMembers
GO

IF NOT OBJECT_ID('Household') IS NULL	DROP TABLE Household
GO

IF NOT OBJECT_ID('Programme') IS NULL	DROP TABLE Programme
GO

IF NOT OBJECT_ID('Donor') IS NULL	DROP TABLE Donor
GO

IF NOT OBJECT_ID('PSP') IS NULL	DROP TABLE PSP
GO

IF NOT OBJECT_ID('PSPBranch') IS NULL	DROP TABLE PSPBranch
GO

IF NOT OBJECT_ID('SubLocation') IS NULL	DROP TABLE SubLocation
GO

IF NOT OBJECT_ID('Location') IS NULL	DROP TABLE Location
GO

IF NOT OBJECT_ID('Division') IS NULL	DROP TABLE Division
GO

IF NOT OBJECT_ID('District') IS NULL	DROP TABLE District
GO

IF NOT OBJECT_ID('Constituency') IS NULL	DROP TABLE Constituency
GO

IF NOT OBJECT_ID('County') IS NULL	DROP TABLE County
GO

IF NOT OBJECT_ID('Province') IS NULL	DROP TABLE Province
GO

IF NOT OBJECT_ID('UserGroupRight') IS NULL	DROP TABLE UserGroupRight
GO

IF NOT OBJECT_ID('Module') IS NULL	DROP TABLE Module
GO

IF NOT OBJECT_ID('SystemCodeDetail') IS NULL	DROP TABLE SystemCodeDetail
GO

IF NOT OBJECT_ID('SystemCode') IS NULL	DROP TABLE SystemCode
GO

IF NOT OBJECT_ID('User') IS NULL	DROP TABLE [User]
GO

IF NOT OBJECT_ID('UserGroup') IS NULL	DROP TABLE UserGroup
GO



IF NOT OBJECT_ID('fn_SystemCodeExist') IS NULL	DROP FUNCTION fn_SystemCodeExist
GO

/* REQUIRED

-- To allow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO 

*/

CREATE FUNCTION fn_SystemCodeExist(@SystemCode tinyint,@Value int)
RETURNS bit
AS
BEGIN
	DECLARE @RetVal bit
	IF EXISTS(SELECT 1 
			  FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId
			  WHERE UPPER(T2.Code)=(CASE(@SystemCode) WHEN 1 THEN 'Target Group'
													  WHEN 2 THEN 'Other Programme'
													  WHEN 3 THEN 'Locality'
													  ELSE '0'
								    END
								    ) AND T1.SystemCodeDetailId=@Value
			  )
		SET @RetVal=1
	ELSE
		SET @RetVal=0
	RETURN @RetVal
END
GO


IF NOT OBJECT_ID('fn_MonthName') IS NULL	DROP FUNCTION fn_MonthName
GO
CREATE FUNCTION fn_MonthName(@MonthId tinyint)
RETURNS varchar(30)
AS
BEGIN
	RETURN CASE(@MonthId)
				WHEN 1 THEN 'January'
				WHEN 2 THEN 'February'
				WHEN 3 THEN 'March'
				WHEN 4 THEN 'April'
				WHEN 5 THEN 'May'
				WHEN 6 THEN 'June'
				WHEN 7 THEN 'July'
				WHEN 8 THEN 'August'
				WHEN 9 THEN 'September'
				WHEN 10 THEN 'October'
				WHEN 11 THEN 'November'
				WHEN 12 THEN 'December'
				ELSE ''
			END
END
GO


CREATE TABLE UserGroup(
	UserGroupId int NOT NULL IDENTITY(1,1)
   ,UserGroupName varchar(20) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(1)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_UserGroup PRIMARY KEY (UserGroupId)
)
GO



CREATE TABLE [User](
	UserId int NOT NULL IDENTITY(1,1)
   ,UserGroupId int NOT NULL
   ,UserName nvarchar(20) NOT NULL
   ,[Password] nvarchar(128) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(1)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_User PRIMARY KEY (UserId)
   ,CONSTRAINT FK_User_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(UserGroupId) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT U_User_UserName UNIQUE (UserName)
)
GO



CREATE TABLE Module(
	ModuleId tinyint NOT NULL IDENTITY(1,1)
   ,ModuleName varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CanAddEdit bit NOT NULL DEFAULT(0)
   ,CanDelete bit NOT NULL DEFAULT(0)
   ,ParentModuleId tinyint
   ,CONSTRAINT PK_Module PRIMARY KEY (ModuleId)
)
GO



CREATE TABLE UserGroupRight(
	UserGroupId int NOT NULL
   ,ModuleId tinyint NOT NULL
   ,CanView bit NOT NULL DEFAULT(0)
   ,CanAddEdit bit NOT NULL DEFAULT(0)
   ,CanDelete bit NOT NULL DEFAULT(0)
   ,CONSTRAINT FK_UserGroupRight_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(UserGroupId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_UserGroupRight_Module FOREIGN KEY (ModuleId) REFERENCES Module(ModuleId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT U_UserGroupRight_UserGroupId_ModuleId UNIQUE (UserGroupId,ModuleId)
)
GO



CREATE TABLE SystemCode(
	SystemCodeId int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,IsUserMaintained bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_SystemCode PRIMARY KEY (SystemCodeId)
   ,CONSTRAINT U_SystemCode_Code UNIQUE (Code)
)
GO



CREATE TABLE SystemCodeDetail(
	SystemCodeDetailId int NOT NULL IDENTITY(1,1)
   ,SystemCodeId int NOT NULL
   ,DetailCode varchar(20) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,OrderNo tinyint NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SystemCodeDetail PRIMARY KEY (SystemCodeDetailId)
   ,CONSTRAINT FK_SystemCodeDetail_SystemCode FOREIGN KEY (SystemCodeId) REFERENCES SystemCode(SystemCodeId) ON UPDATE CASCADE
   ,CONSTRAINT U_SystemCodeDetail_SystemCodeId_DetailCode UNIQUE (SystemCodeId,DetailCode)
)
GO



CREATE TABLE Province(
	ProvinceId int NOT NULL IDENTITY(1,1)
   ,ProvinceCode varchar(20) NOT NULL
   ,ProvinceName varchar(30) NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Province PRIMARY KEY (ProvinceId)
   ,CONSTRAINT U_Province_ProvinceCode UNIQUE (ProvinceCode)
)
GO



CREATE TABLE County(
	CountyId int NOT NULL IDENTITY(1,1)
   ,CountyCode varchar(20) NOT NULL
   ,CountyName varchar(30) NOT NULL
   ,ProvinceId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_County PRIMARY KEY (CountyId)
   ,CONSTRAINT FK_County_Province FOREIGN KEY (ProvinceId) REFERENCES Province(ProvinceId) ON UPDATE CASCADE
   ,CONSTRAINT U_County_CountyCode UNIQUE (CountyCode)
)
GO



CREATE TABLE Constituency(
	ConstituencyId int NOT NULL IDENTITY(1,1)
   ,ConstituencyCode varchar(20) NOT NULL
   ,ConstituencyName varchar(30) NOT NULL
   ,CountyId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Constituency PRIMARY KEY (ConstituencyId)
   ,CONSTRAINT FK_Constituency_County FOREIGN KEY (CountyId) REFERENCES County(CountyId) ON UPDATE CASCADE
   ,CONSTRAINT U_Constituency_ConstituencyCode_CountyId UNIQUE (ConstituencyCode,CountyId)
)
GO



CREATE TABLE District(
	DistrictId int NOT NULL IDENTITY(1,1)
   ,DistrictCode varchar(20) NOT NULL
   ,DistrictName varchar(30) NOT NULL
   ,CountyId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_District PRIMARY KEY (DistrictId)
   ,CONSTRAINT FK_District_County FOREIGN KEY (CountyId) REFERENCES County(CountyId) ON UPDATE CASCADE
   ,CONSTRAINT U_District_DistrictCode_CountyId UNIQUE (DistrictCode,CountyId)
)
GO



CREATE TABLE Division(
	DivisionId int NOT NULL IDENTITY(1,1)
   ,DivisionCode varchar(20) NOT NULL
   ,DivisionName varchar(30) NOT NULL
   ,DistrictId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Division PRIMARY KEY (DivisionId)
   ,CONSTRAINT FK_Division_District FOREIGN KEY (DistrictId) REFERENCES District(DistrictId) ON UPDATE CASCADE
   ,CONSTRAINT U_Division_DivisionCode UNIQUE (DivisionCode)
)
GO



CREATE TABLE Location(
	LocationId int NOT NULL IDENTITY(1,1)
   ,LocationCode varchar(20) NOT NULL
   ,LocationName varchar(30) NOT NULL
   ,DivisionId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Location PRIMARY KEY (LocationId)
   ,CONSTRAINT FK_Location_Division FOREIGN KEY (DivisionId) REFERENCES Division(DivisionId) ON UPDATE CASCADE
   ,CONSTRAINT U_Location_LocationCode UNIQUE (LocationCode)
)
GO



CREATE TABLE SubLocation(
	SubLocationId int NOT NULL IDENTITY(1,1)
   ,SubLocationCode varchar(20) NOT NULL
   ,SubLocationName varchar(30) NOT NULL
   ,LocalityId int NOT NULL
   ,LocationId int NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NULL
   ,CreatedOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SubLocation PRIMARY KEY (SubLocationId)
   ,CONSTRAINT FK_SubLocation_Location FOREIGN KEY (LocationId) REFERENCES Location(LocationId) ON UPDATE CASCADE
   ,CONSTRAINT FK_SubLocation_Constituency FOREIGN KEY (ConstituencyId) REFERENCES Constituency(ConstituencyId) ON UPDATE NO ACTION
   ,CONSTRAINT FK_SubLocation_SystemCodeDetail FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(SystemCodeDetailId) ON UPDATE CASCADE
   ,CONSTRAINT U_SubLocation_SubLocationCode UNIQUE (SubLocationCode)
   ,CONSTRAINT C_SubLocation_Locality CHECK (dbo.fn_SystemCodeExist(3,LocalityId)=1)
)
GO



CREATE TABLE [Programme](
	ProgrammeId smallint NOT NULL IDENTITY(1,1)
   ,ProgrammeCode nvarchar(20) NOT NULL
   ,ProgrammeName nvarchar(128) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Programme PRIMARY KEY (ProgrammeId) 
   ,CONSTRAINT U_Programme_ProgrammeCode UNIQUE (ProgrammeCode)
)
GO



CREATE TABLE Donor(
	DonorId smallint NOT NULL IDENTITY(1,1)
   ,DonorCode nvarchar(20) NOT NULL
   ,DonorName nvarchar(128) NOT NULL
   ,TargetGroupId int NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Donor PRIMARY KEY (DonorId)
   ,CONSTRAINT U_Donor_DonorCode UNIQUE (DonorCode)
)
GO



CREATE TABLE PSP(
	PSPId smallint NOT NULL IDENTITY(1,1)
   ,PSPCode nvarchar(20) NOT NULL
   ,PSPName nvarchar(128) NOT NULL
   ,IsDefault bit NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSP PRIMARY KEY (PSPId)
   ,CONSTRAINT U_PSP_PSPCode UNIQUE (PSPCode)
)
GO



CREATE TABLE PSPBranch(
	PSPBranchId smallint NOT NULL IDENTITY(1,1)
   ,PSPId smallint NOT NULL
   ,PSPBranchCode nvarchar(20) NOT NULL
   ,PSPBranchName nvarchar(128) NOT NULL
   ,IsDefault bit NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSPBranch PRIMARY KEY (PSPBranchId)
   ,CONSTRAINT U_PSPBranch_PSPBranchCode UNIQUE (PSPBranchCode)
)
GO



CREATE TABLE Household(
	HHId int NOT NULL IDENTITY(1,1)
   ,ProgrammeId tinyint NOT NULL
   ,HHNo varchar(30)	--CONSIDER STANDARDIZING THIS DATA TYPE
   ,SerialNo int NULL
   ,CaptureDate datetime NOT NULL
   ,SubLocationId int NOT NULL
   ,Village varchar(50) NOT NULL
   ,Ward varchar(50) NOT NULL
   ,PhysicalAddress varchar(50) NULL
   ,NearestLandmark varchar(50) NULL
   ,LocalityId int NOT NULL
   ,Longitude float NULL
   ,Latitude float NULL
   ,Elevation float NULL
   ,TargetGroupId int NOT NULL
   --BENEFITING FROM OTHER PROGRAMME
   ,OtherProgId int NULL
   ,OtherProgBenefitFreqId int NULL
   ,OtherProgMonthlyBenefitAmount money NULL
   --HOUSEHOLD MEALS
   ,AverageDailyMealsCount tinyint NULL
   ,AverageWeeklyNoMealsCount tinyint NULL
   ,AverageWeeklyNoMealsReason varchar(50) NULL
   ,DonorId smallint NULL
   --PSP DETAILS
   ,PSPBranchId smallint NULL
   ,LoCNames varchar(50) NULL
   ,LoCNationalIDNo varchar(30) NULL
   ,LoCPhone varchar(30) NULL
   ,EnrolmentDate datetime NULL
   ,HHStatusId int NOT NULL
   ,TargetingObservations varchar(128) NULL
   ,Reminder varchar(128) NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,IsDeleted bit NOT NULL DEFAULT(0)
   ,CreatedOn datetime NOT NULL
   ,CreatedBy int NOT NULL
   ,ModifiedOn datetime NULL
   ,ModifiedBy int NULL
   ,CONSTRAINT PK_Household PRIMARY KEY (HHId)
   ,CONSTRAINT FK_Household_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(SubLocationId)
   ,CONSTRAINT FK_Household_SC_Locality FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_Household_SC_TargetGroup FOREIGN KEY (TargetGroupId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_Household_SC_OtherProg FOREIGN KEY (OtherProgId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_Household_SC_OtherProgBenefitFreq FOREIGN KEY (OtherProgBenefitFreqId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_Household_Donor FOREIGN KEY (DonorId) REFERENCES Donor(DonorId)
   ,CONSTRAINT FK_Household_PSPBranch FOREIGN KEY (PSPBranchId) REFERENCES PSPBranch(PSPBranchId)
   ,CONSTRAINT FK_Household_SC_HHStatus FOREIGN KEY (HHStatusId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
)
GO




--CONSIDER Photos AND FingerPrints
CREATE TABLE HouseholdMembers(
	MemberId int NOT NULL IDENTITY(1,1)
   ,HHId int NOT NULL
   ,MemberRoleId int NOT NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL
   ,SexId int NOT NULL
   ,RelationshipId int NOT NULL
   ,DoB datetime NULL
   ,BirthCertNo varchar(30)
   ,OrphanhoodTypeId int NOT NULL
   ,NationalIDNo varchar(30) NULL
   ,EducationLevelId int NOT NULL
   ,IlliteracyReasonId int NULL
   ,MaritalStatusId int NOT NULL
   ,IllnessTypeId int NOT NULL
   ,DisabilityTypeId int NOT NULL	--INCLUDES CHRONICALLY ILLNESS AND PHYSICAL DISABILITY
   ,NCPwDNo varchar(30) NULL
   ,Phone1 nvarchar(20)
   ,Phone2 nvarchar(20)
   ,ExternalMember bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_HouseholdMembers PRIMARY KEY (MemberId)
   ,CONSTRAINT FK_HouseholdMembers_Household FOREIGN KEY (HHId) REFERENCES Household(HHId)
   ,CONSTRAINT FK_HouseholdMembers_SC_MemberRole FOREIGN KEY (MemberRoleId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Sex FOREIGN KEY (SexId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Relationship FOREIGN KEY (RelationshipId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Orphanhood FOREIGN KEY (OrphanhoodTypeId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Education FOREIGN KEY (EducationLevelId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Illiteracy FOREIGN KEY (IlliteracyReasonId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_MaritalStatus FOREIGN KEY (MaritalStatusId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Illness FOREIGN KEY (IllnessTypeId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
   ,CONSTRAINT FK_HouseholdMembers_SC_Disability FOREIGN KEY (DisabilityTypeId) REFERENCES SystemCodeDetail(SystemCodeDetailId)
)
GO



IF NOT OBJECT_ID('HouseholdCharacteristics') IS NULL	DROP TABLE HouseholdCharacteristics
GO
CREATE TABLE HouseholdCharacteristics(
	HHId int NOT NULL
   ,WallMaterialId int NOT NULL
   ,FloorMaterialId int NOT NULL
   ,RoofMaterialId int NOT NULL
   ,ToiletTypeId int NOT NULL
   ,WaterSourceId int NOT NULL
   ,LightingSourceId int NOT NULL
   ,CookingFuelSourceId int NOT NULL
)
GO


--ENFORCE THAT ONLY ITEMS MORE THAN ZERO GETS CAPTURED
IF NOT OBJECT_ID('HouseholdItems')	IS NULL	DROP TABLE HouseholdItems
GO
CREATE TABLE HouseholdItems(
	HHId int NOT NULL
   ,HHItemTypeId int NOT NULL
   ,HHItemCount tinyint NOT NULL
)
GO


IF NOT OBJECT_ID('HouseholdAssets') IS NULL	DROP TABLE HouseholdAssets
GO
CREATE TABLE HouseholdAssets(
	HHId int NOT NULL
   ,OwnHouse bit NOT NULL DEFAULT(0)
   ,MonthlyRental money NULL
   ,HouseRooms tinyint NOT NULL DEFAULT(1)
   ,ZebuCattle tinyint NOT NULL DEFAULT(0)
   ,HybridCattle tinyint NOT NULL DEFAULT(0)
   ,Goats tinyint NOT NULL DEFAULT(0)
   ,Sheep tinyint NOT NULL DEFAULT(0)
   ,Pigs tinyint NOT NULL DEFAULT(0)
   ,Camels tinyint NOT NULL DEFAULT(0)
   ,Donkeys tinyint NOT NULL DEFAULT(0)
   ,Poultry tinyint NOT NULL DEFAULT(0)
   ,Rabbits tinyint NOT NULL DEFAULT(0)
   ,OwnFarmingLandSize tinyint NOT NULL DEFAULT(0)
   ,OwnGrazingLandSize tinyint NOT NULL DEFAULT(0)
   ,LivelihoodSourceId int NOT NULL
   ,PermanentIncome money NOT NULL DEFAULT(0.00)
   ,ContractIncome money NOT NULL DEFAULT(0.00)
   ,CasualIncome money NOT NULL DEFAULT(0.00)
   ,OtherIncome money NOT NULL DEFAULT(0.00)
)
GO



IF NOT OBJECT_ID('HouseholdBusiness') IS NULL DROP TABLE HouseholdBusiness
GO
CREATE TABLE HouseholdBusiness(
	HHId int NOT NULL
   ,BusinessType varchar(50) NOT NULL
   ,TotalEmployeesInclusive tinyint NOT NULL
   ,EstimatedBusinessValue money NOT NULL
   ,PremisesMonthlyRental money NOT NULL
   ,TotalStatutoryLevies money NOT NULL
   ,AverageMonthlyProfit money NOT NULL
   ,SavingsAmount money NOT NULL DEFAULT(0.00)
   ,SavingsStorageTypeId int NOT NULL
)
GO


--ENFORCE THAT ONLY EXPENSES GREATER THAN ZERO GETS CAPTURED
IF NOT OBJECT_ID('HouseholdExpenditure') IS NULL	DROP TABLE HouseholdExpenditure
GO
CREATE TABLE HouseholdExpenditure(
	HHId int NOT NULL
   ,HHExpenditureTypeId int NOT NULL
   ,HHExpenditureAmount money NOT NULL
)
GO



--ENFORCE THAT ONLY EXPENSES GREATER THAN ZERO GETS CAPTURED
IF NOT OBJECT_ID('HouseholdIncome') IS NULL	DROP TABLE HouseholdIncome
GO
CREATE TABLE HouseholdIncome(
	HHId int NOT NULL
   ,HHIncomeTypeId int NOT NULL
   ,HHIncomeAmount money NOT NULL
)
GO



IF NOT OBJECT_ID('HouseholdNutrition') IS NULL	DROP TABLE HouseholdNutrition
GO
CREATE TABLE HouseholdNutrition(
	HHId int NOT NULL
   ,HHNutritionTypeId int NOT NULL
   ,EatenRaw bit NOT NULL
   ,PkgUnitSize varchar(30) NOT NULL
   ,PkgUnitCost money NOT NULL
   ,WeeklyConsumptionFreq tinyint NOT NULL
   ,FoodSource varchar(30) NOT NULL
   ,ChoiceInfluence varchar(30) NOT NULL
   ,ModeOfTransport varchar(30) NOT NULL
)
GO



IF NOT OBJECT_ID('ExpansionPlan') IS NULL	DROP TABLE ExpansionPlan
GO
CREATE TABLE ExpansionPlan(
	ExpansionPlanId int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,LocationId int NOT NULL
   ,PovertyHeadCountPerc float --SMALL AREA ESTIMATE
   ,EligibleHHs int
   ,Scaleup_EqualShare int
   ,Scaleup_PovertyPrioritized int
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
)
GO



CREATE TABLE Grievance(
	GrievanceId int NOT NULL IDENTITY(1,1)
   ,SerialNo varchar(20) NULL
   ,GrievanceLevelId int NOT NULL
   ,GrievanceTypeId int NOT NULL
   ,GrievanceDesc nvarchar(512) NOT NULL
   ,ReportingDate datetime NOT NULL
   ,ReportedTo nvarchar(50) NULL
   ,ReportingChannelId int NOT NULL
   ,ReceiptDate datetime NOT NULL
   ,ComplainantTypeId int NOT NULL
   ,Names nvarchar(50) NULL
   ,SexId int NOT NULL
   ,AgeBracketId int NOT NULL
   ,IsBeneficiary bit NOT NULL DEFAULT(0)
   ,HHId int NULL
   ,CommunityName nvarchar(20)
   ,CountyId int NULL
   ,ConstituencyId int NULL
   ,DistrictId int NULL
   ,DivisionId int NULL
   ,LocationId int NULL
   ,SubLocationId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Grievance PRIMARY KEY (GrievanceId)
   ,CONSTRAINT U_Grievance_SerialNo UNIQUE(SerialNo)
)
GO



CREATE TABLE GrievanceAction(
	GrievanceActionId int NOT NULL IDENTITY(1,1)
   ,GrievanceId int NOT NULL
   ,GrievanceResolutionId int NOT NULL
   ,ResolutionDesc nvarchar(512) NOT NULL
   ,ResolutionDate datetime NOT NULL
   ,FeedbackDate datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeletedBy int NULL
   ,DeletedOn datetime NULL
   ,CONSTRAINT PK_GrievanceAction PRIMARY KEY (GrievanceActionId)
   ,CONSTRAINT FK_GrievanceAction_Grievance FOREIGN KEY (GrievanceId) REFERENCES Grievance(GrievanceId)
)
GO



CREATE TABLE DBBackup(
	DBBackupId int NOT NULL IDENTITY(1,1)
   ,FilePath nvarchar(128) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
)
GO



CREATE TABLE PaymentCycle(
	PaymentCycleId int NOT NULL IDENTITY(1,1)
   ,ProgrammeId smallint NOT NULL
   ,[Description] varchar(50) NOT NULL
   ,FromMonth tinyint NOT NULL
   ,FromYear smallint NOT NULL
   ,ToMonth tinyint NOT NULL
   ,ToYear smallint NOT NULL
   ,PaymentStatusId int NOT NULL
   ,IsDeleted bit NOT NULL DEFAULT(0)
   ,EnrolledHHs int NULL
   ,PrePayrollAuditBy int NULL
   ,PrePayrollAuditOn datetime NULL
   ,PaymentBy int NULL
   ,PaymentOn datetime NULL
   ,ReconciledBy int NULL
   ,ReconciledOn datetime NULL
   ,ClosedBy int NULL
   ,ClosedOn datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeletedBy int NULL
   ,DeletedOn datetime NULL
   ,CONSTRAINT PK_PaymentCycle PRIMARY KEY (PaymentCycleId)
   ,CONSTRAINT FK_PaymetCycle_Programme FOREIGN KEY (ProgrammeId) REFERENCES Programme(ProgrammeId) ON UPDATE CASCADE
)
GO




CREATE TABLE PaymentTargetGroup(
	PaymentTargetId int NOT NULL IDENTITY(1,1)
   ,PaymentCycleId int NOT NULL
   ,TargetGroupId int NOT NULL
   ,PaymentAmount money NOT NULL
   ,CONSTRAINT PK_PaymentTargetGroup PRIMARY KEY (PaymentTargetId)
   ,CONSTRAINT FK_PaymentTargetGroup_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(PaymentCycleId) ON UPDATE CASCADE
)
GO




CREATE TABLE PaymentAdjustment(
	PaymentAdjustmentId int NOT NULL IDENTITY(1,1)
   ,HHId int NOT NULL
   ,PaymentCycleId int NOT NULL
   ,AdjustmentTypeId int NOT NULL
   ,AdjustmentAmount money NOT NULL
   ,AmountAdjusted money NOT NULL DEFAULT(0.00)
   ,RefNo nvarchar(50) NULL
   ,Notes nvarchar(128) NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_PaymentAdjustment PRIMARY KEY (PaymentAdjustmentId)
   ,CONSTRAINT FK_PaymentAdjustment_Household FOREIGN KEY (HHId) REFERENCES Household(HHId) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymentAdjustment_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(PaymentCycleId) ON UPDATE CASCADE
)
GO




CREATE TABLE PrepayrollAudit(
	PrepayrollAuditId int NOT NULL IDENTITY(1,1)
   ,PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,BeneFirstName varchar(50) NOT NULL
   ,BeneMiddleName varchar(50) NULL
   ,BeneSurname varchar(50) NOT NULL
   ,BeneNationalIDNo varchar(30) NULL
   ,BeneNationalIDNoInvalid bit NOT NULL
   ,BeneNationalIDNoDuplicated bit NOT NULL
   ,CGFirstName varchar(50) NOT NULL
   ,CGMiddleName varchar(50) NULL
   ,CGSurname varchar(50) NOT NULL
   ,CGNationalIDNo varchar(30) NULL
   ,CGNationalIDNoInvalid bit NOT NULL
   ,CGNationalIDNoDuplicated bit NOT NULL
   ,SubLocationId int NOT NULL
   ,ConseUnpaidCyclesAmount money NOT NULL
   ,PaymentAdjustmentAmount money NOT NULL
   ,EntitlementAmount money NULL
   ,PaymentAmountSuspicious bit NULL
   ,Ineligible bit NULL
   ,IsException bit NULL
   ,IsActioned bit NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionNotes nvarchar(128) NULL
   --CONSIDER INCLUDING INFO THAT COULD CHANGE FROM PAYMENT CYCLE TO PAYMENT CYCLE SUCH AS NoOVCs,NoMembers etc
   ,CONSTRAINT PK_PrepayrollAudit PRIMARY KEY (PrepayrollAuditId)
   ,CONSTRAINT FK_PrepayrollAudit_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(PaymentCycleId) ON UPDATE CASCADE
   ,CONSTRAINT FK_PrepayrollAudit_Household FOREIGN KEY (HHId) REFERENCES Household(HHId) ON UPDATE CASCADE
   ,CONSTRAINT FK_PrepayrollAudit_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(SubLocationId) ON UPDATE NO ACTION
)
GO




CREATE TABLE Payment(
	PaymentId int NOT NULL IDENTITY(1,1)
   ,PrepayrollAuditId int NOT NULL
   ,PaidAmount money NULL
   ,ReceiverNames varchar(128) NULL
   ,ReceiverNationalIDNo varchar(30) NULL
   ,TrxNo varchar(30) NULL
   ,TrxDate datetime NULL
   ,CONSTRAINT PK_PaymentId PRIMARY KEY (PaymentId)
   ,CONSTRAINT FK_Payment_PrepayrollAudit FOREIGN KEY (PrepayrollAuditId) REFERENCES PrepayrollAudit(PrepayrollAuditId) ON UPDATE CASCADE
)
GO


