
/*
	   PURPOSE: CCTP MIS BENEFICIARY ACCOUNTS OPENED BY PSPS
	CREATED BY: JOAB SELELYA - MIS SPECIALIST (DEVELOPMENT PATHWAYS)
	CREATED ON: 18TH APRIL, 2018
*/
					  	
	IF NOT OBJECT_ID('tempdb..##tblEnrolmentAnalysis') IS NULL	DROP TABLE ##tblEnrolmentAnalysis;
	CREATE TABLE ##tblEnrolmentAnalysis(RowId bigint NOT NULL IDENTITY(1,1)
								      ,PSPId int
								      ,PSPName varchar(30)
								      ,CountyId int
								      ,County varchar(128)
								      ,AccountsOpened int
								      ,PaymentCardsIssued int
								      )
								   
	IF NOT OBJECT_ID('tempdb..##tblEnrolmentSummary') IS NULL	DROP TABLE ##tblEnrolmentSummary;
	CREATE TABLE ##tblEnrolmentSummary(RowId bigint NOT NULL IDENTITY(1,1)
								    ,CountyId int
								    ,TotalAccounts int
								    ,TotalPaymentCardsIssued int
								    )
								    
	DECLARE @tblPaymentCycles TABLE(PSPId int
								   ,PSPName varchar(48)
								   )
								   
	DECLARE @SQL varchar(8000)
	DECLARE @strPSP varchar(512)
	DECLARE @strCols varchar(512)

	
	--1. BUILDING THE PAYMENT NUMBERS STRING
	SELECT @strPSP=COALESCE(@strPSP+',['+T1.Name+']','['+T1.Name+']')
	FROM PSP T1
	
	SELECT @strCols=COALESCE(@strCols+',ISNULL(T2.['+T1.Name+'],0) ['+T1.Name+'-Accs],ISNULL(T3.['+T1.Name+'],0) ['+T1.Name+'-Cards]','ISNULL(T2.['+Name+'],0) ['+T1.Name+'-Accs],ISNULL(T3.['+T1.Name+'],0) ['+T1.Name+'-Cards]')
	FROM PSP T1
	
	--3. ANALYZING Enrolment (BENEFICIARY AND PaymentCardsIssued NUMBERS) BY CYCLE AND BY COUNTY
	INSERT INTO ##tblEnrolmentAnalysis(PSPId,PSPName,CountyId,County,AccountsOpened,PaymentCardsIssued)
	SELECT T1.Code,T1.Name,T8.CountyId,T8.CountyName,COUNT(T3.Id) AS AccountsOpened,SUM(CASE WHEN(ISNULL(T4.PaymentCardNo,'')<>'') THEN 1 ELSE 0 END) AS PaymentCardsIssued
	FROM PSP T1 INNER JOIN PSPBranch T2 ON T1.Id=T2.PSPId
				INNER JOIN BeneficiaryAccount T3 ON T2.Id=T3.PSPBranchId
				INNER JOIN BeneficiaryPaymentCard T4 ON T3.Id=T4.BeneAccountId
				INNER JOIN HouseholdEnrolment T5 ON T3.HhEnrolmentId=T5.Id
				INNER JOIN Household T6 ON T5.HhId=T6.Id
				INNER JOIN HouseholdSubLocation T7 ON T6.Id=T7.HhId
				INNER JOIN (
								SELECT T1.Id AS SubLocationId,T6.Id AS CountyId,T6.Code AS CountyCode,T6.Name AS CountyName
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
							) T8 ON T7.SubLocationId=T8.SubLocationId
	GROUP BY T1.Code,T1.Name,T8.CountyId,T8.CountyName
	


	--4. Enrolment SUMMARY
	INSERT INTO ##tblEnrolmentSummary(CountyId,TotalAccounts,TotalPaymentCardsIssued)
	SELECT T1.CountyId,SUM(T1.AccountsOpened) AS TotalAccounts,SUM(T1.PaymentCardsIssued) AS TotalPaymentCardsIssued
	FROM ##tblEnrolmentAnalysis T1
	GROUP BY T1.CountyId
	
	-->QUICK CONFIRMATION
	SELECT @strPSP
	SELECT @strCols

	SELECT T1.*
	FROM ##tblEnrolmentAnalysis T1
	ORDER BY T1.County
	--COMPUTE SUM(T1.AccountsOpened),SUM(T1.PaymentCardsIssued)


	
	--BUILDING THE QUERY STRING
	SET @SQL =' SELECT ROW_NUMBER() OVER(ORDER BY T1.Name) AS No,T1.Name AS County,'+@strCols+',ISNULL(T4.TotalAccounts,0) TotalAccs,ISNULL(T4.TotalPaymentCardsIssued,0) TotalCards'
			 +' FROM County T1 LEFT JOIN ('
										 +' SELECT *'
										 +' FROM ('
												 +'	SELECT CountyId,County,AccountsOpened,PSPName'
												 +'	FROM ##tblEnrolmentAnalysis'
											  +' ) AS T1'
										 +' PIVOT'
											  +' ('
												 +'	SUM(AccountsOpened)'
												 +'	FOR PSPName IN('+@strPSP+')'
											  +' ) AS T2'
									   +' ) T2 ON T1.Id=T2.CountyId'
							+' LEFT JOIN ('
										 +' SELECT *'
										 +' FROM ('
												 +'	SELECT CountyId,County,PaymentCardsIssued,PSPName'
												 +'	FROM ##tblEnrolmentAnalysis'
											  +' ) AS T1'
										 +' PIVOT'
											  +' ('
												 +'	SUM(PaymentCardsIssued)'
												 +'	FOR PSPName IN('+@strPSP+')'
											  +' ) AS T2'
									   +' ) T3 ON T1.Id=T3.CountyId'
							+' LEFT JOIN ##tblEnrolmentSummary T4 ON T1.Id=T4.CountyId'
			 +' ORDER BY T1.Name'

	PRINT @SQL
	PRINT @strCols
	
	EXEC(@SQL)


