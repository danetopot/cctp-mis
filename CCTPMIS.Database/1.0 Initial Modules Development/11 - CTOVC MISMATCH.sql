use [CCTP-MIS]
go

;with x as (
SELECT T1.IDnUMBER, t1.BeneficiaryName, T2.*
FROM LegacyDB.dbo.CONSOLIDATED_CLEAN T1
    LEFT JOIN LegacyDB.dbo.ActiveCTOVCBeneficiaries T2 ON   t1.IdNumber COLLATE DATABASE_DEFAULT   =  SUBSTRING(T2.ResolvedIDNo, PATINDEX('%[^0]%', T2.ResolvedIDNo+'.'), LEN(T2.ResolvedIDNo)) 
        COLLATE DATABASE_DEFAULT
WHERE T1.Programme = 'CT-OVC' --AND T2.IDNo IS  not  NULL
)
,Y AS (
SELECT  
		T4.Code Programme
		,REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.TokenId)))+CONVERT(varchar(6),T3.TokenId) AS TokenId
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,convert(varchar,T6.NationalIdNo) AS BeneIDNo
		,T7.Code AS BeneSex
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T14.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T14.ProgrammeNo)))+CONVERT(varchar(6),T14.ProgrammeNo) AS ProgrammeNo
		,T15.AccountName
		,T15.AccountNo
		 FROM  Household T3  -- ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id and T4.Code ='CT-OVC'
								   INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T3.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id

								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T3.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

										  LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T3.Id
										  LEFT JOIN BeneficiaryAccount T15 ON T14.Id = T15.HhEnrolmentId
 )

 SELECT *,
 (convert(int,X.IPRS_FirstNameExists) + convert(int,X.IPRS_MiddleNameExists) + convert(int,X.IPRS_SurnameExists)) 'Name match'
  FROM X LEFT JOIN Y ON X.IDNo COLLATE DATABASE_DEFAULT = Y.BeneIDNo COLLATE DATABASE_DEFAULT
   LEFT JOIN LegacyDB.DBO.VW_IPRS_SET Z ON X.IDNo = CONVERT(VARCHAR, Z.ID_Number)
  WHERE Y.BeneIDNo IS NULL  AND X.ResolvedIDNo IS NOT NULL
  --AND  (convert(int,X.IPRS_FirstNameExists) + convert(int,X.IPRS_MiddleNameExists) + convert(int,X.IPRS_SurnameExists))>0






-- SELECT 
-- Z.*,
-- X.*
----, Y.Programme
----,y.BeneFirstName
----,y.BeneMiddleName
----,y.BeneSurname
----,Y.BeneSex 
----,y.BeneIDNo
--,X.ProgrammeNumber 'Legacy Programme No'
--, CASE WHEN isnull(X.IDNo,'')<>'' THEN 'YES' ELSE 'NO' END AS 'IN ACTIVE BENE LIST'
--,isnull(y.ProgrammeNo,'') ProgrammeNo
--,case when isnull(y.TokenId,0)>0 then 'Yes' else 'No' end  as 'Migrated to CCTP'
--,case when isnull(y.ProgrammeNo,0)>0 then 'Yes' else 'No' end  as 'Enrolled By Programme'
--,case when isnull(y.AccountName,'')<>'' then 'Yes' else 'No' end as 'Enrolled By PSP'
--,isnull(y.AccountName,'') AccountName
--,isnull(y.AccountNo,'') AccountNo
--,y.County
--,y.Constituency
--,y.District
--,y.Division
--,y.Location
--,y.SubLocation
--,(convert(int,X.IPRS_FirstNameExists) + convert(int,X.IPRS_MiddleNameExists) + convert(int,X.IPRS_SurnameExists)) 'Name match'
--  FROM X LEFT JOIN Y ON X.IDNo COLLATE DATABASE_DEFAULT = Y.BeneIDNo COLLATE DATABASE_DEFAULT
--  WHERE Y.BeneIDNo IS NULL  AND X.ResolvedIDNo IS NOT NULL
  




SELECT  SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)), COUNT( SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber))) FROM  
CONSOLIDATED_CLEAN -- WHERE Programme='OPCT'
GROUP BY SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber))
HAVING COUNT(SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)))>1


SELECT * FROM CONSOLIDATED_CLEAN WHERE SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)) IN ('6384925',
'450634',
'6201842')





SELECT  SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)), COUNT( SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber))) FROM  ActiveOPCTBeneficiariesUPDATED -- WHERE Programme='OPCT'
GROUP BY SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber))
HAVING COUNT(SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)))>1

ALTER VIEW VW_IPRS_SET
AS

 SELECT DISTINCT  First_Name, ISNULL(Other_Name,'') Other_Name, Surname, Gender, ID_Number, Date_of_Birth FROM 
(
SELECT  First_Name, Other_Name, Surname, Gender, ID_Number, Date_of_Birth FROM  LegacyDB.dbo.IPRS_Rerun1
UNION
SELECT  First_Name, Other_Name, Surname, Gender, ID_Number, Date_of_Birth FROM  LegacyDB.dbo.IPRS_Rerun2
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname, Sex Gender, IDNumber, DoB FROM LegacyDB.dbo.IPRS_CTOVC
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname,   Gender, IDNumber, DateOfBirth  FROM LegacyDB.dbo.IPRS_CTOVC_CG
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname,   Gender, IDNumber, DateOfBirth  FROM LegacyDB.dbo.IPRS_OPCT_Bene
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname,   Gender, IDNumber, DateOfBirth  FROM LegacyDB.dbo.IPRS_OPCT_CG
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname,   Gender, IDNumber, DateOfBirth  FROM LegacyDB.dbo.IPRS_PwSD_Bene
UNION
SELECT  FirstName First_Name , MiddleName Other_Name , Surname,   Gender, IDNumber, DateOfBirth  FROM LegacyDB.dbo.IPRS_PwSD_CG
) T1


GO


