

/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB SPs FOR DATA MANIPULATION IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH FEBRUARY, 2018
   MODIFIED ON: 
	
	NOTE: IT REQUIRES AN EXISTING DATABASE
*/


IF NOT OBJECT_ID('AddEditPaymentCycle') IS NULL	DROP PROC AddEditPaymentCycle
GO
CREATE PROC AddEditPaymentCycle
	@Id int=NULL
   ,@ProgrammeId smallint
   ,@Description nvarchar(128)
   ,@FinancialPeriodId int
   ,@FromMonthId int
   ,@ToMonthId int
   ,@TargetGroupsXML XML
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentTargetGroup TABLE(
		Id int
	   ,TargetGroupId int
	   ,PaymentAmount money
	)

	DECLARE @HHStatus_EnrolledCode varchar(20)
	DECLARE @HHStatus_OnPayrollCode varchar(20)
	DECLARE @FinancialPeriodCode varchar(20)
	DECLARE @CalendarMonthCode varchar(20)
	DECLARE @TargetGroupCode varchar(20)
	DECLARE @PaymentCycleStageCode varchar(20)
	DECLARE @PaymentCycleStageCode_PREPAYROLL varchar(20)
	DECLARE @PaymentCycleStageId int
	DECLARE @PayableHHs int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @Exists bit
	DECLARE @NoOfRows int
	DECLARE @TargetGroups1 varchar(128), @TargetGroups2 varchar(128)


	SET @HHStatus_EnrolledCode = 'ENRL'
	SET @HHStatus_OnPayrollCode = 'ONPAY'
	SET @FinancialPeriodCode = 'Financial Period'
	SET @CalendarMonthCode = 'Calendar Months'
	SET @TargetGroupCode = 'Target Group'
	SET @PaymentCycleStageCode = 'Payment Status'
	SET @PaymentCycleStageCode_PREPAYROLL = 'PREPAYROLL'

	INSERT INTO @tblPaymentTargetGroup(Id,TargetGroupId,PaymentAmount)
	SELECT T1.Id,T1.TargetGroupId,PaymentAmount
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(TargetGroupId)[1]','int') AS TargetGroupId
			  ,U.R.value('(PaymentAmount)[1]','int') AS PaymentAmount
		FROM @TargetGroupsXML.nodes('TargetGroups/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.TargetGroupId=T2.Id
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@TargetGroupCode

	SELECT @TargetGroups1=COALESCE(@TargetGroups1+','+CONVERT(varchar(20),T2.TargetGroupId),@TargetGroups1) FROM PaymentCycle T1 INNER JOIN PaymentTargetGroup T2 ON T1.Id=T2.PaymentCycleId WHERE T1.ProgrammeId=@ProgrammeId AND T1.FinancialPeriodId=@FinancialPeriodId AND T1.FromMonthId=@FromMonthId AND T1.ToMonthId=@ToMonthId AND @Id=0 ORDER BY T2.TargetGroupId ASC
	SELECT @TargetGroups2=COALESCE(@TargetGroups2+','+CONVERT(varchar(20),TargetGroupId),@TargetGroups1) FROM @tblPaymentTargetGroup ORDER BY TargetGroupId ASC
	SELECT @PaymentCycleStageId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@PaymentCycleStageCode AND T1.Code=@PaymentCycleStageCode_PREPAYROLL
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId
	SELECT @ProgrammeId=Id FROM Programme WHERE Id=@ProgrammeId

	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	IF ISNULL(@Description,'')=''
		SET @ErrorMsg='Please specify valid Description parameter'
	IF NOT EXISTS(SELECT 1 FROM @tblPaymentTargetGroup)
		SET @ErrorMsg='Please specify valid TargetGroupsXML parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@FinancialPeriodId AND T2.Code=@FinancialPeriodCode)
		SET @ErrorMsg='Please specify valid FinancialPeriod parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@FromMonthId AND T2.Code=@CalendarMonthCode)
		SET @ErrorMsg='Please specify valid FromMonth parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@ToMonthId AND T2.Code=@CalendarMonthCode)
		SET @ErrorMsg='Please specify valid ToMonth parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@Id,0)=0 AND @TargetGroups1=@TargetGroups2
		SET @ErrorMsg='The Payment Cycle already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @PayableHHs=COUNT(T1.Id)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
	WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HHStatus_EnrolledCode,@HHStatus_OnPayrollCode)

	BEGIN TRAN

	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.[Description]=@Description
		   ,T1.FinancialPeriodId=@FinancialPeriodId
		   ,T1.FromMonthId=@FromMonthId
		   ,T1.ToMonthId=@ToMonthId
		   ,T1.EnrolledHHs=@PayableHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PaymentCycle T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCycle(ProgrammeId,[Description],FinancialPeriodId,FromMonthId,ToMonthId,EnrolledHHs,PaymentStageId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@Description,@FinancialPeriodId,@FromMonthId,@ToMonthId,@PayableHHs,@PaymentCycleStageId,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('PaymentCycle')
	END
	
	DELETE T1
	FROM PaymentTargetGroup T1 
	WHERE T1.PaymentCycleId=@Id

	INSERT INTO PaymentTargetGroup(PaymentCycleId,TargetGroupId,PaymentAmount)
	SELECT @Id,TargetGroupId,PaymentAmount
	FROM @tblPaymentTargetGroup


	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows,@Id AS PaymentCycleId,T2.Id AS PaymentCycleStageId,T2.[Description] AS PaymentCycleStage
		FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id
		WHERE T1.Id=@Id
	END
END
GO



IF NOT OBJECT_ID('DeletePaymentCycle') IS NULL	DROP PROC DeletePaymentCycle
GO
CREATE PROC DeletePaymentCycle
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @PaymentCycleStatusCode_PREPAYROLL varchar(20)
	DECLARE @PaymentCycleStatusCode_PREPAYROLLAPV varchar(20)
	DECLARE @ErrorMsg varchar(128)

	SET @PaymentCycleStatusCode_PREPAYROLL='PREPAYROLL'
	SET @PaymentCycleStatusCode_PREPAYROLLAPV='PREPAYROLLAPV'
	SELECT @Id=Id FROM PaymentCycle WHERE Id=@Id
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@Id,0)=0
		SET @ErrorMsg='The specified Payment Cycle does not exist'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id WHERE T1.Id=@Id AND T2.Code IN(@PaymentCycleStatusCode_PREPAYROLL,@PaymentCycleStatusCode_PREPAYROLLAPV))
		SET @ErrorMsg='Once the Payment Cycle Funds have been requested it cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	UPDATE T1
	SET T1.DeletedBy=@UserId
	   ,T1.DeletedOn=GETDATE() 
	FROM PaymentCycle T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetPaymentCycles
END
GO





IF NOT OBJECT_ID('UTILITY_SP_PWDGEN') IS NULL DROP PROC UTILITY_SP_PWDGEN
GO
CREATE PROCEDURE UTILITY_SP_PWDGEN
    @len int = 8,
    --@Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz.,#$%^&*-+_/=',
    @Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
    @output nvarchar(64) OUTPUT
AS
    SET NOCOUNT ON;
    SET @output = '';
    SELECT  @output += SUBSTRING(@Charset, FLOOR(ABS(CAST(CRYPT_GEN_RANDOM(8) AS BIGINT) / 9223372036854775808.5) * LEN(@Charset)) + 1, 1)
    FROM master.dbo.spt_values
    WHERE type = 'P' AND number < @len;
GO



IF NOT OBJECT_ID('ApproveEnrolmentPlan') IS NULL	DROP PROC ApproveEnrolmentPlan
GO
CREATE PROC ApproveEnrolmentPlan
	@Id int
   ,@UserId int
AS
BEGIN
	--TO DO LIST: 1. INCORPORATE WAITING LIST VALIDITY PERIOD
	DECLARE @tbl_EnrolmentHhAnalysis TABLE(
		Id int NOT NULL IDENTITY(1,1)
	   ,HhId int NOT NULL
	 --,PMTScore int NOT NULL
	   ,LocationId int NOT NULL
	)
	DECLARE @tbl_EnrolmentNumbers TABLE(
		Id int NOT NULL IDENTITY(1,1)
	   ,LocationId int NOT NULL
	   ,PovertyPerc float NOT NULL
	   ,RegGroupHhs int NOT NULL
	   ,BeneHhs int NOT NULL
	   ,ScaleupEqualShare int NOT NULL
	   ,ScaleupPovertyPrioritized int NOT NULL
	   ,StartsFromId int
	   ,EnrolmentNumbers int
	)
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @NumbersToEnroll int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='HHStatus'
	SET @SysDetailCode='VALPASS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='PSPCARDED'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='System Settings'
	SET @SysDetailCode='CURFINYEAR'
	SELECT @SystemCodeDetailId3=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='WAITLISTVALIDITYMONTHS'
	SELECT @SystemCodeDetailId4=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @NumbersToEnroll=EnrolmentNumbers FROM HouseholdEnrolmentPlan WHERE Id=@Id

	BEGIN TRAN

	INSERT INTO @tbl_EnrolmentHhAnalysis(HhId,LocationId)
	SELECT T1.Id,T5.LocationId
	FROM Household T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId AND T1.RegGroupId=T2.RegGroupId
					  INNER JOIN HouseholdSubLocation T3 ON T1.Id=T3.HhId
					  INNER JOIN GeoMaster T4 ON T3.GeoMasterId=T4.Id AND T4.IsDefault=1
					  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T3.SubLocationId=T5.SubLocationId AND T4.Id=T5.GeoMasterId		
					  LEFT JOIN (
									SELECT T3.ProgrammeId,T2.LocationId,T1.FinancialYearId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
									FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
																INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
																INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																								INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																								INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																								INNER JOIN District T5 ON T4.DistrictId=T5.Id
																								INNER JOIN County T6 ON T4.CountyId=T6.Id
																								INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																								INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																			WHERE T8.IsDefault=1
																			) T4 ON T2.LocationId=T4.LocationId	
								) T6 ON T1.ProgrammeId=T6.ProgrammeId AND T5.LocationId=T6.LocationId AND T6.FinancialYearId=@SystemCodeDetailId3				 					  
	WHERE T2.Id=@Id AND T1.StatusId=@SystemCodeDetailId1 --INCORPORATE WAITING LIST VALIDITY PERIOD HERE
	ORDER BY T6.PovertyHeadCountPerc,T5.LocationId--THIS IS WHERE YOU INCLUDE PMT ORDERING ALSO

	INSERT INTO @tbl_EnrolmentNumbers(LocationId,PovertyPerc,RegGroupHhs,BeneHhs,ScaleupEqualShare,ScaleupPovertyPrioritized,StartsFromId)
	SELECT T1.LocationId,ISNULL(T3.PovertyHeadCountPerc,0.00) AS PovertyPerc,T1.RegGroupHhs,ISNULL(T2.BeneHhs,0) AS BeneHhs,ISNULL(T3.ScaleupEqualShare,0) AS ScaleupEqualShare,ISNULL(T3.ScaleupPovertyPrioritized,0) AS ScaleupPovertyPrioritized,T4.StartsFromId
	FROM (
			SELECT T1.ProgrammeId,T5.LocationId,COUNT(T1.Id) AS RegGroupHhs
			FROM Household T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId AND T1.RegGroupId=T2.RegGroupId
							  INNER JOIN HouseholdSubLocation T3 ON T1.Id=T3.HhId
							  INNER JOIN GeoMaster T4 ON T3.GeoMasterId=T4.Id AND T4.IsDefault=1
							  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
										FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
															INNER JOIN Division T3 ON T2.DivisionId=T3.Id
															INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
															INNER JOIN District T5 ON T4.DistrictId=T5.Id
															INNER JOIN County T6 ON T4.CountyId=T6.Id
															INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										) T5 ON T3.SubLocationId=T5.SubLocationId AND T4.Id=T5.GeoMasterId
			WHERE T2.Id=@Id AND T1.StatusId=@SystemCodeDetailId1
			GROUP BY T1.ProgrammeId,T5.LocationId
		) T1 LEFT JOIN (
							SELECT T4.LocationId,COUNT(T1.Id) AS BeneHhs
							FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
												INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
												INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
															FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																				INNER JOIN District T5 ON T4.DistrictId=T5.Id
																				INNER JOIN County T6 ON T4.CountyId=T6.Id
																				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
															) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId					  
							WHERE T1.StatusId=@SystemCodeDetailId2
							GROUP BY T4.LocationId
						) T2 ON T1.LocationId=T2.LocationId 
				LEFT JOIN (
							SELECT T3.ProgrammeId,T2.LocationId,T1.FinancialYearId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
							FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
														INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
														INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																						INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																						INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																						INNER JOIN District T5 ON T4.DistrictId=T5.Id
																						INNER JOIN County T6 ON T4.CountyId=T6.Id
																						INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																						INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																	WHERE T8.IsDefault=1
																	) T4 ON T2.LocationId=T4.LocationId	
						) T3 ON T1.ProgrammeId=T3.ProgrammeId AND T1.LocationId=T3.LocationId AND T3.FinancialYearId=@SystemCodeDetailId3	
				LEFT JOIN (
							SELECT LocationId,MIN(Id) AS StartsFromId
							FROM @tbl_EnrolmentHhAnalysis
							GROUP BY LocationId
						  ) T4 ON T1.LocationId=T4.LocationId
	ORDER BY PovertyPerc,LocationId

	--FIRST PRIORITIZE ENROLMENT OF EQUAL SHARE NUMBERS
	SET @SystemCodeDetailId1=1
	WHILE @NumbersToEnroll>0 AND EXISTS(SELECT 1 FROM @tbl_EnrolmentNumbers WHERE Id=@SystemCodeDetailId1)
	BEGIN
		SELECT @SystemCodeDetailId2=CASE WHEN(ScaleupEqualShare+ScaleupPovertyPrioritized<=0) THEN RegGroupHhs
										 WHEN(ScaleupEqualShare-BeneHhs>0) THEN CASE WHEN(ScaleupEqualShare-BeneHhs>RegGroupHhs) THEN RegGroupHhs ELSE ScaleupEqualShare-BeneHhs END
										 ELSE 0 
								    END 
		FROM @tbl_EnrolmentNumbers 
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId2=CASE WHEN(@NumbersToEnroll>=@SystemCodeDetailId2) THEN @SystemCodeDetailId2 ELSE @NumbersToEnroll END
		SET @NumbersToEnroll=@NumbersToEnroll-@SystemCodeDetailId2
		
		UPDATE @tbl_EnrolmentNumbers
		SET EnrolmentNumbers=ISNULL(EnrolmentNumbers,0)+@SystemCodeDetailId2
		FROM @tbl_EnrolmentNumbers
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId1=@SystemCodeDetailId1+1
	END
	--THEN ENROLMENT OF POVERTY PRIORITIZED NUMBERS
	SET @SystemCodeDetailId1=1
	WHILE @NumbersToEnroll>0 AND EXISTS(SELECT 1 FROM @tbl_EnrolmentNumbers WHERE Id=@SystemCodeDetailId1)
	BEGIN
		SELECT @SystemCodeDetailId2=CASE WHEN(ScaleupEqualShare+ScaleupPovertyPrioritized<=0) THEN RegGroupHhs
										 WHEN((ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers)>0) THEN CASE WHEN((ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers)>(RegGroupHhs-EnrolmentNumbers)) THEN (RegGroupHhs-EnrolmentNumbers) ELSE (ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers) END
										 ELSE 0 
								    END 
		FROM @tbl_EnrolmentNumbers 
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId2=CASE WHEN(@NumbersToEnroll>=@SystemCodeDetailId2) THEN @SystemCodeDetailId2 ELSE @NumbersToEnroll END
		SET @NumbersToEnroll=@NumbersToEnroll-@SystemCodeDetailId2
		
		UPDATE @tbl_EnrolmentNumbers
		SET EnrolmentNumbers=ISNULL(EnrolmentNumbers,0)+@SystemCodeDetailId2
		FROM @tbl_EnrolmentNumbers
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId1=@SystemCodeDetailId1+1
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@SystemCodeDetailId1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.Id=@Id

	INSERT INTO HouseholdEnrolment(HhEnrolmentPlanId,HhId,BeneProgNoPrefix,ProgrammeNo)
	SELECT T1.HhEnrolmentPlanId,T1.HhId,T1.BeneProgNoPrefix,ISNULL(T2.MAXProgNo,0)+T1.RowId AS ProgrammeNo
	FROM (
			SELECT @Id AS HhEnrolmentPlanId,T1.HhId,T4.Id AS ProgrammeId,T4.BeneProgNoPrefix,ROW_NUMBER() OVER(PARTITION BY T4.Id ORDER BY T1.Id) AS RowId 
			FROM @tbl_EnrolmentHhAnalysis T1 INNER JOIN @tbl_EnrolmentNumbers T2 ON T1.LocationId=T2.LocationId 
											 INNER JOIN Household T3 ON T1.HhId=T3.Id
											 INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			WHERE T2.EnrolmentNumbers>0 AND T1.Id>=T2.StartsFromId AND T1.Id<(T2.StartsFromId+T2.EnrolmentNumbers) 
		) T1 LEFT JOIN (SELECT T3.Id AS ProgrammeId,MAX(ISNULL(T1.ProgrammeNo,0)) AS MAXProgNo
						FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id
												   INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id
						GROUP BY T3.Id
						) T2 ON T1.ProgrammeId=T2.ProgrammeId

	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRL'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T3
	SET T3.StatusId=@SystemCodeDetailId2
	FROM @tbl_EnrolmentHhAnalysis T1 INNER JOIN @tbl_EnrolmentNumbers T2 ON T1.LocationId=T2.LocationId 
									 INNER JOIN Household T3 ON T1.HhId=T3.Id
	WHERE T2.EnrolmentNumbers>0 AND T1.Id>=T2.StartsFromId AND T1.Id<(T2.StartsFromId+T2.EnrolmentNumbers) 

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('AddEditEnrolmentPlan') IS NULL	DROP PROC AddEditEnrolmentPlan
GO
CREATE PROC AddEditEnrolmentPlan
	@Id int=NULL
   ,@ProgrammeId int
   ,@RegGroupId int
   ,@RegGroupHhs int
   ,@BeneHhs int
   ,@ExpPlanEqualShare int
   ,@ExpPlanPovertyPrioritized int
   ,@EnrolmentNumbers int
   ,@EnrolmentGroupId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Id=@ProgrammeId)
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@RegGroupId AND T2.Code='Registration Group')
		SET @ErrorMsg='Please specify valid RegGroupId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@EnrolmentGroupId AND T2.Code='Enrolment Group')
		SET @ErrorMsg='Please specify valid EnrolmentGroupId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGENROLAPV'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.RegGroupId=@RegGroupId
		   ,T1.RegGroupHhs=@RegGroupHhs
		   ,T1.BeneHhs=@BeneHhs
		   ,T1.ExpPlanEqualShare=@BeneHhs
		   ,T1.ExpPlanPovertyPrioritized=@BeneHhs
		   ,T1.EnrolmentNumbers=@EnrolmentNumbers
		   ,T1.EnrolmentGroupId=@EnrolmentGroupId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM HouseholdEnrolmentPlan T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO HouseholdEnrolmentPlan(ProgrammeId,RegGroupId,RegGroupHhs,BeneHhs,ExpPlanEqualShare,ExpPlanPovertyPrioritized,EnrolmentNumbers,EnrolmentGroupId,StatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@RegGroupId,@RegGroupHhs,@BeneHhs,@ExpPlanEqualShare,@ExpPlanPovertyPrioritized,@EnrolmentNumbers,@EnrolmentGroupId,@SystemCodeDetailId,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('HouseholdEnrolmentPlan')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO
--exec AddEditEnrolmentPlan @Id=0,@ProgrammeId=1,@RegGroupId=103,@RegGroupHhs=553449,@BeneHhs=0,@ExpPlanEqualShare=0,@ExpPlanPovertyPrioritized=0,@EnrolmentNumbers=100,@EnrolmentGroupId=104,@UserId=1
--exec AddEditEnrolmentPlan @Id=1,@ProgrammeId=1,@RegGroupId=103,@RegGroupHhs=553449,@BeneHhs=0,@ExpPlanEqualShare=0,@ExpPlanPovertyPrioritized=0,@EnrolmentNumbers=500,@EnrolmentGroupId=104,@UserId=1
--EXEC ApproveEnrolmentPlan @Id=2,@UserId=1
--select * from householdenrolmentplan
--select * from FileCreation
--select * from household where statusid=16
--exec DeleteEnrolmentPlan @Id=5
--select * from HouseholdEnrolment




IF NOT OBJECT_ID('DeleteEnrolmentPlan') IS NULL	DROP PROC DeleteEnrolmentPlan
GO
CREATE PROC DeleteEnrolmentPlan
	@Id int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE Id=@Id AND ApvBy IS NOT NULL)
		SET @ErrorMsg='The specified enrolment batch has been approved and cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
END
GO



IF NOT OBJECT_ID('GenerateEnrolmentFile') IS NULL	DROP PROC GenerateEnrolmentFile
GO
CREATE PROC GenerateEnrolmentFile
	@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	DROP TABLE #FileResults

	IF OBJECT_ID('temp_EnrolmentFile') IS NOT NULL	DROP TABLE temp_EnrolmentFile;
	CREATE TABLE temp_EnrolmentFile(
		EnrolmentNo int
	   ,ProgrammeNo varchar(50)
	   ,BeneFirstName varchar(50)
	   ,BeneMiddleName varchar(50)
	   ,BeneSurname varchar(50)
	   ,BeneIDNo varchar(30)
	   ,BeneSex varchar(20)
	   ,CGFirstName varchar(50)
	   ,CGMiddleName varchar(50)
	   ,CGSurname varchar(50)
	   ,CGIDNo varchar(30)
	   ,CGSex varchar(20)
	   ,County varchar(30)
	   ,Constituency varchar(30)
	   ,District varchar(30)
	   ,Division varchar(30)
	   ,Location varchar(30)
	   ,SubLocation varchar(30)
	   );

	INSERT INTO temp_EnrolmentFile(EnrolmentNo,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneIDNo,BeneSex,CGFirstName,CGMiddleName,CGSurname,CGIDNo,CGSex,County,Constituency,District,Division,Location,SubLocation)
	SELECT T2.Id AS EnrolmentNo
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!
	WHERE T1.StatusId=@SystemCodeDetailId1

	IF NOT EXISTS(SELECT 1 FROM temp_EnrolmentFile)
		SET @ErrorMsg='There is no beneficiary selection approved for enrolment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SELECT @FileName='ENROLMENT_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_EnrolmentFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	--SELECT @SQLStmt
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	DROP TABLE temp_EnrolmentFile;
	
	--RECORDING THE FILE
	SET @SysCode='File Type'
	SET @SysDetailCode='ENROLMENT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SET @FileCreationId=IDENT_CURRENT('FileCreation')

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPRECEIPT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	   ,T1.FileCreationId=@FileCreationId
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.StatusId=@SystemCodeDetailId2

	SELECT @FileCreationId AS FileCreationId
	SET NOCOUNT OFF
END
GO
--EXEC GenerateEnrolmentFile @FilePath='C:\MINE\',@DBServer='.\SQL2012',@DBName='CCTP-MIS',@DBUser='sa',@DBPassword='sa@pass',@UserId=1




IF NOT OBJECT_ID('BeneEnrolFileDownloaded') IS NULL	DROP PROC BeneEnrolFileDownloaded
GO
CREATE PROC BeneEnrolFileDownloaded
	@FileCreationId int
   ,@FileChecksum varchar(64)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @RowCount int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Id=@FileCreationId AND FileChecksum=@FileChecksum)
		SET @ErrorMsg='Please specify valid FileCreationId corresponding FileCheksum'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF NOT EXISTS(SELECT 1 FROM FileDownload WHERE FileCreationId=@FileCreationId AND DownloadedBy=@UserId)
		INSERT INTO FileDownload(FileCreationId,FileChecksum,DownloadedBy,DownloadedOn)
		SELECT @FileCreationId,@FileChecksum,@UserId,GETDATE() AS DownloadedOn

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.FileCreationId=@FileCreationId

	SELECT @RowCount=@@ROWCOUNT

	IF @@ERROR>0 OR ISNULL(@RowCount,0)<=0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FilePassword
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT FilePassword FROM FileCreation WHERE Id=@FileCreationId
	END
END
GO




IF NOT OBJECT_ID('PSPEnrolBeneficiary') IS NULL	DROP PROC PSPEnrolBeneficiary
GO
CREATE PROC PSPEnrolBeneficiary
	@EnrolmentNo int
   ,@BankCode varchar(20)
   ,@BranchCode varchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AccountOpenedOn datetime
   ,@PriReciFirstName varchar(50)
   ,@PriReciMiddleName varchar(50)
   ,@PriReciSurname varchar(50)
   ,@PriReciNationalIdNo varchar(30)
   ,@PriReciSex char(1)
   ,@PriReciDoB datetime
   ,@SecReciFirstName varchar(50)
   ,@SecReciMiddleName varchar(50)
   ,@SecReciSurname varchar(50)
   ,@SecReciNationalIdNo varchar(30)
   ,@SecReciSex char(1)
   ,@SecReciDoB datetime
   ,@PaymentCardNo varchar(50)
   ,@MobileNo1 nvarchar(20)
   ,@MobileNo2 nvarchar(20)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @PriReciId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@PriReciSex
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PriReciId=T1.Id 
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.PrimaryRecipientId
	WHERE T1.FirstName=@PriReciFirstName AND T1.MiddleName=@PriReciMiddleName AND T1.Surname=@PriReciSurname AND T1.NationalIdNo=@PriReciNationalIdNo AND T1.DoB=@PriReciDoB AND T1.SexId=@SystemCodeDetailId3
		
	SELECT @SecReciId=T1.Id 
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=@SecReciMiddleName AND T1.Surname=@SecReciSurname AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.DoB=@SecReciDoB AND T1.SexId=@SystemCodeDetailId4
						
	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
			 
			 	   
	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo AND T2.StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF @AccountOpenedOn IS NULL OR NOT (@AccountOpenedOn<=GETDATE())
		SET @ErrorMsg='Please specify valid AccountOpenedOn parameter'
	ELSE IF ISNULL(@PriReciId,0)=0
		SET @ErrorMsg='Please specify valid Primary Recipient details'
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	ELSE IF RTRIM(LTRIM(ISNULL(@PaymentCardNo,'')))=''
		SET @ErrorMsg='Please specify valid PaymentCardNo parameter'
	ELSE IF ISNULL(@MobileNo1,'')<>'' AND (CASE WHEN(@MobileNo1 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo1 parameter'
	ELSE IF ISNULL(@MobileNo2,'')<>'' AND (CASE WHEN(@MobileNo2 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo2 parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM BeneficiaryAccount WHERE HhEnrolmentId=@EnrolmentNo AND ExpiryDate>GETDATE())
		SET @ErrorMsg='The household appears to have an existing account with a PSP'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SET @SysCode='System Settings'
	SET @SysDetailCode='BENEPSPRENEWALDATE'
	SELECT @AccountExpiryDate=CASE WHEN(ISNULL(T1.[Description],'')='' OR CONVERT(datetime,T1.[Description])<GETDATE()) THEN CONVERT(datetime,'30 June '+CONVERT(varchar(4),CASE WHEN(GETDATE()>CONVERT(datetime,'30 June '+CONVERT(varchar(4),YEAR(GETDATE())))) THEN YEAR(GETDATE())+1 ELSE YEAR(GETDATE()) END))
																													    ELSE CONVERT(datetime,T1.[Description])
							  END
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO BeneficiaryAccount(HhEnrolmentId,PSPBranchId,AccountNo,AccountName,OpenedOn,StatusId,ExpiryDate)
	SELECT @EnrolmentNo AS HhEnrolmentId
		,(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AS PSPBranchId
		,@AccountNo AS AccountNo
		,@AccountName AS AccountName
		,@AccountOpenedOn AS OpenedOn
		,@SystemCodeDetailId1 AS StatusId
		,@AccountExpiryDate AS ExpiryDate

	SET @AccountId=IDENT_CURRENT('BeneficiaryAccount')

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	INSERT INTO BeneficiaryPaymentCard(BeneAccountId,PriReciId,PriReciFirstname,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSexId,PriReciDoB,SecReciId,SecReciFirstname,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSexId,SecReciDoB,PaymentCardNo,MobileNo1,MobileNo2,StatusId)
	SELECT @AccountId,@PriReciId,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@SystemCodeDetailId3,@PriReciDoB,@SecReciId,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SystemCodeDetailId4,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@SystemCodeDetailId2
	
	SET @PaymentCardId=IDENT_CURRENT('BeneficiaryPaymentCard')

	SET @SysCode='HHStatus'
	SET @SysDetailCode='PSPCARDED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS BeneAccountId,NULL AS PaymentCardId,NULL AS PriReciId,NULL AS SecReciId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @AccountId AS BeneAccountId,@PaymentCardId AS PaymentCardId,@PriReciId AS PriReciId,@SecReciId AS SecReciId
	END
END
GO



IF NOT OBJECT_ID('AddEditPaymentCardBiometrics') IS NULL	DROP PROC AddEditPaymentCardBiometrics
GO
CREATE PROC AddEditPaymentCardBiometrics
	@BenePaymentCardId int
   ,@PriReciRT varbinary
   ,@PriReciRI varbinary
   ,@PriReciRMF varbinary
   ,@PriReciRRF varbinary
   ,@PriReciRP varbinary
   ,@PriReciLT varbinary
   ,@PriReciLI varbinary
   ,@PriReciLMF varbinary
   ,@PriReciLRF varbinary
   ,@PriReciLP varbinary
   ,@SecReciRT varbinary
   ,@SecReciRI varbinary
   ,@SecReciRMF varbinary
   ,@SecReciRRF varbinary
   ,@SecReciRP varbinary
   ,@SecReciLT varbinary
   ,@SecReciLI varbinary
   ,@SecReciLMF varbinary
   ,@SecReciLRF varbinary
   ,@SecReciLP varbinary
   ,@UserId int
AS
BEGIN
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM BeneficiaryPaymentCard WHERE Id=@BenePaymentCardId)
		SET @ErrorMsg='Please specify valid PaymentCardId'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@BenePaymentCardId,0)>0
	BEGIN
		UPDATE T1
		SET T1.PriReciRT=@PriReciRT
		   ,T1.PriReciRI=@PriReciRI
		   ,T1.PriReciRMF=@PriReciRMF
		   ,T1.PriReciRRF=@PriReciRRF
		   ,T1.PriReciRP=@PriReciRP
		   ,T1.PriReciLT=@PriReciLT
		   ,T1.PriReciLI=@PriReciLI
		   ,T1.PriReciLMF=@PriReciLMF
		   ,T1.PriReciLRF=@PriReciLRF
		   ,T1.PriReciLP=@PriReciLP
		   ,T1.SecReciRT=@SecReciRT
		   ,T1.SecReciRI=@SecReciRI
		   ,T1.SecReciRMF=@SecReciRMF
		   ,T1.SecReciRRF=@SecReciRRF
		   ,T1.SecReciRP=@SecReciRP
		   ,T1.SecReciLT=@SecReciLT
		   ,T1.SecReciLI=@SecReciLI
		   ,T1.SecReciLMF=@SecReciLMF
		   ,T1.SecReciLRF=@SecReciLRF
		   ,T1.SecReciLP=@SecReciLP
		   ,T1.CreatedBy=@UserId
		   ,T1.CreatedOn=GETDATE()
		FROM PaymentCardBiometrics T1
		WHERE T1.BenePaymentCardId=BenePaymentCardId
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCardBiometrics(BenePaymentCardId,PriReciRT,PriReciRI,PriReciRMF,PriReciRRF,PriReciRP,PriReciLT,PriReciLI,PriReciLMF,PriReciLRF,PriReciLP,SecReciRT,SecReciRI,SecReciRMF,SecReciRRF,SecReciRP,SecReciLT,SecReciLI,SecReciLMF,SecReciLRF,SecReciLP,CreatedBy,CreatedOn)
		SELECT @BenePaymentCardId,@PriReciRT,@PriReciRI,@PriReciRMF,@PriReciRRF,@PriReciRP,@PriReciLT,@PriReciLI,@PriReciLMF,@PriReciLRF,@PriReciLP,@SecReciRT,@SecReciRI,@SecReciRMF,@SecReciRRF,@SecReciRP,@SecReciLT,@SecReciLI,@SecReciLMF,@SecReciLRF,@SecReciLP,@UserId,GETDATE()
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO