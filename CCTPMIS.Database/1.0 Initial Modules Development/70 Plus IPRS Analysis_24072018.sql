	DECLARE @HhStatus_EnrolledProgCode varchar(20)
	DECLARE @HhStatus_EnrolledPSPCode varchar(20)
	DECLARE @HhStatus_PSPCardedCode varchar(20)
	DECLARE @HhStatus_OnPayrollCode varchar(20)
	DECLARE @HhStatus_OnSuspensionCode varchar(20)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @HhStatus_EnrolledProgCode = 'ENRL'
	SET @HhStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HhStatus_PSPCardedCode = 'PSPCARDED'
	SET @HhStatus_OnPayrollCode = 'ONPAY'
	SET @HhStatus_OnSuspensionCode = 'SUS'

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T1.EnrolmentNo
		,T1.BeneNationalIDNo,T1.BeneFirstName+CASE WHEN(T1.BeneMiddleName<>'') THEN ' ' ELSE '' END+T1.BeneMiddleName+CASE WHEN(T1.BeneSurname<>'') THEN ' ' ELSE '' END+T1.BeneSurname AS BeneName,T1.BeneSex,T1.BeneDoB
		,T2.IDNo AS IPRS_IDNo,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS IPRS_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_BeneSurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS IPRS_DoBMatches
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS IPRS_DoBYearMatches
		,CASE WHEN(T1.BeneSex=T2.Sex) THEN 1 ELSE 0 END AS IPRS_SexMatches
		,CONVERT(bit,ISNULL(T1.PSPId,0)) AS PSPEnrolled,T1.PSP AS Bank,T1.AccountNo,T1.AccountName,T1.OnJanFebPayroll,T1.PaidInJanFebPayroll
	FROM (
			SELECT T3.Name AS Programme,T1.Id AS HhId,T11.Id AS EnrolmentNo
				,T4.PersonId AS BenePersonId,ISNULL(T4.FirstName,'') AS BeneFirstName,ISNULL(T4.MiddleName,'') AS BeneMiddleName,ISNULL(T4.Surname,'') AS BeneSurname,T4.DoB AS BeneDoB,T4.SexId AS BeneSexId,T4.Sex AS BeneSex,T4.NationalIdNo AS BeneNationalIDNo,T3.PriReciCanReceivePayment
				,T5.PersonId AS CGPersonId,ISNULL(T5.FirstName,'') AS CGFirstName,ISNULL(T5.MiddleName,'') AS CGMiddleName,ISNULL(T5.Surname,'') AS CGSurname,T5.DoB AS CGDoB,T5.SexId AS CGSexId,T5.Sex AS CGSex,T5.NationalIdNo AS CGNationalIDNo
				,T6.TotalHhMembers,T1.StatusId AS HhStatusId,T14.Id AS BeneAccountId,T14.AccountNo,T14.AccountName,T15.Id AS BenePaymentCardId,T7.SubLocationId
				,T17.Id AS PSPId,T17.Name AS PSP,CONVERT(bit,ISNULL(T19.HhId,0)) AS OnJanFebPayroll,CONVERT(bit,ISNULL(T20.WasTrxSuccessful,0)) AS PaidInJanFebPayroll
			FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
							  INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
							  INNER JOIN (
											SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T5.Code AS Sex,T1.NationalIdNo
											FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
														   INNER JOIN Household T3 ON T2.HhId=T3.Id
														   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
														   INNER JOIN SystemCodeDetail T5 ON T1.SexId=T5.Id
											WHERE T2.StatusId=@SystemCodeDetailId1
										) T4 ON T1.Id=T4.HhId AND T4.RowId=1
							  LEFT JOIN (
											SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T5.Code AS Sex,T1.NationalIdNo
											FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
														   INNER JOIN Household T3 ON T2.HhId=T3.Id
														   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.SecondaryRecipientId
														   INNER JOIN SystemCodeDetail T5 ON T1.SexId=T5.Id
											WHERE T2.StatusId=@SystemCodeDetailId1
										) T5 ON T1.Id=T5.HhId AND T5.RowId=1
							  LEFT JOIN(
											SELECT HhId,COUNT(PersonId) AS TotalHhMembers
											FROM HouseholdMember
											WHERE StatusId=@SystemCodeDetailId1
											GROUP BY HhId
										) T6 ON T1.Id=T6.HhId
							  LEFT JOIN(
											SELECT T1.HhId,T1.SubLocationId,T2.LocationId,T4.PaymentZoneId
											FROM HouseholdSubLocation T1 INNER JOIN SubLocation T2 ON T1.SubLocationId=T2.Id
																		 INNER JOIN GeoMaster T3 ON T1.GeoMasterId=T3.Id
																		 INNER JOIN Location T4 ON T2.LocationId=T4.Id
											WHERE T3.IsDefault=1
										) T7 ON T1.Id=T7.HhId
							  INNER JOIN HouseholdEnrolment T11 ON T1.Id=T11.HhId
							  INNER JOIN HouseholdEnrolmentPlan T12 ON T11.HhEnrolmentPlanId=T12.Id
							  LEFT JOIN BeneficiaryAccount T14 ON T11.Id=T14.HhEnrolmentId AND T14.StatusId=@SystemCodeDetailId2
							  LEFT JOIN BeneficiaryPaymentCard T15 ON T14.Id=T15.BeneAccountId AND T15.StatusId=@SystemCodeDetailId3
							  LEFT JOIN PSPBranch T16 ON T14.PSPBranchId=T16.Id
							  LEFT JOIN PSP T17 ON T16.PSPId=T17.Id
							  LEFT JOIN Prepayroll T18 ON T14.Id=T18.BeneAccountId
							  LEFT JOIN Payroll T19 ON T18.PaymentCycleId=T19.PaymentCycleId AND T18.ProgrammeId=T19.ProgrammeId AND T18.HhId=T19.HhId
							  LEFT JOIN Payment T20 ON T19.PaymentCycleId=T20.PaymentCycleId AND T19.ProgrammeId=T20.ProgrammeId AND T19.HhId=T20.HhId
			WHERE T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
		) T1 LEFT JOIN (
							SELECT IDNo,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,CONVERT(datetime,DoB) AS DoB,Gender AS Sex
							FROM [IPRSLookUp_70Plus]
						) T2 ON CONVERT(bigint,T1.BeneNationalIDNo)=CONVERT(bigint,T2.IDNo)
			 LEFT JOIN (
							SELECT IDNo,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,CONVERT(datetime,DoB) AS DoB,Gender AS Sex
							FROM [IPRSLookUp_70Plus]
						) T3 ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)
