use LegacyDB
go

    ALTER TABLE  CONSOLIDATED_CLEAN ADD   ResolvedNationalNo BIGint null 
	go
    update CONSOLIDATED_CLEAN set ResolvedNationalNo =SUBSTRING(IdNumber, PATINDEX('%[^0]%', IdNumber+'.'), LEN(IdNumber)) 
    where Programme='OPCT'
	go

	--alter table ActiveOPCTBeneficiariesUPDATED  ADD   ResolvedNationalNo BIGint null 
	--go

   update ActiveOPCTBeneficiariesUPDATED set ResolvedNationalNo =try_CONVERT(bigint,SUBSTRING(ResolvedIDNo, PATINDEX('%[^0]%', ResolvedIDNo+'.'), LEN(ResolvedIDNo)) )
 


 
use LegacyDB
go

 alter table ActivePwSDBeneficiariesUPDATED  ADD   ResolvedNationalNo BIGint null 
	--go

   update ActivePwSDBeneficiariesUPDATED set ResolvedNationalNo =try_CONVERT(bigint,SUBSTRING(ResolvedIDNo, PATINDEX('%[^0]%', ResolvedIDNo+'.'), LEN(ResolvedIDNo)) )
 


 