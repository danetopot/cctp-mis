IF NOT OBJECT_ID('PSPPayrollTrx') IS NULL	DROP PROC PSPPayrollTrx
GO
CREATE PROC PSPPayrollTrx
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@EnrolmentNo int
   ,@BankCode nvarchar(20)
   ,@BranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AmountTransferred money
   ,@WasTrxSuccessful bit
   ,@TrxNo nvarchar(50)=NULL
   ,@TrxDate datetime=NULL
   ,@TrxNarration nvarchar(128)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @BeneHhId int
	DECLARE @BeneBankCode nvarchar(20)
	DECLARE @BeneBranchCode nvarchar(20)
	DECLARE @BeneAccountNo varchar(50)
	DECLARE @BeneAccountName varchar(100)
	DECLARE @PaymentAmount money
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int


	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @BeneHhId=T1.HhId,@BeneBankCode=T6.Code,@BeneBranchCode=T5.Code,@BeneAccountNo=T4.AccountNo,@BeneAccountName=T4.AccountName,@PaymentAmount=T1.PaymentAmount
	FROM Payroll T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
					INNER JOIN HouseholdEnrolment T3 ON T1.HhId=T3.HhId
					INNER JOIN BeneAccount T4 ON T2.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T2.Id=@EnrolmentNo
		 	   
	IF ISNULL(@BeneHhId,0)=0
		SET @ErrorMsg='Please specify valid combination of PaymentCycleId, ProgrammeId and EnrolmentNo parameters'
	IF @WasTrxSuccessful IS NULL
		SET @ErrorMsg='Please specify valid WasTrxSuccessful parameters'
	ELSE IF @BankCode<>@BeneBankCode
		SET @ErrorMsg='Please specify valid BankCode parameter for the beneficiary'
	ELSE IF @BranchCode<>@BeneBranchCode
		SET @ErrorMsg='Please specify valid BranchCode parameter for the beneficiary'
	ELSE IF @AccountNo<>@BeneAccountNo
		SET @ErrorMsg='Please specify valid AccountNo parameter for the beneficiary'
	ELSE IF @AccountName<>@BeneAccountName
		SET @ErrorMsg='Please specify valid AccountName parameter for the beneficiary'
	ELSE IF @PaymentAmount<>@AmountTransferred AND @AmountTransferred>0
		SET @ErrorMsg='Please specify valid AmountTranferred parameter for the beneficiary'
	ELSE IF ISNULL(@TrxNo,'')='' AND @AmountTransferred>0
		SET @ErrorMsg='Please specify valid TrxNo parameter'
	ELSE IF @TrxDate>GETDATE() AND @AmountTransferred>0
		SET @ErrorMsg='Please specify valid TrxDate parameter'
	ELSE IF ISNULL(@TrxNarration,'')='' AND ISNULL(@AmountTransferred,0)=0
		SET @ErrorMsg='Please specify TrxNarration TrxDate parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	IF EXISTS(SELECT 1 
			  FROM Payment T1
			  WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T1.HhId=@BeneHhId
			  )
	BEGIN
		UPDATE T1
		SET T1.WasTrxSuccessful=@WasTrxSuccessful
		   ,T1.TrxAmount=@AmountTransferred
		   ,T1.TrxNo=@TrxNo
		   ,T1.TrxDate=@TrxDate
		   ,T1.TrxNarration=@TrxNarration
		FROM Payment T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T1.HhId=@BeneHhId
	END
	ELSE
	BEGIN
		INSERT INTO Payment(PaymentCycleId,HhId,WasTrxSuccessful,TrxAmount,TrxNo,TrxDate,TrxNarration)
		SELECT @PaymentCycleId,@BeneHhId,@WasTrxSuccessful,@AmountTransferred,@TrxNo,@TrxDate,@TrxNarration
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS NoOfRows
	END
END
GO
