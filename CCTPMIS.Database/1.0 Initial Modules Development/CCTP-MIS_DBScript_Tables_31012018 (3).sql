/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB TABLES TO HOLD DATA IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH FEBRUARY, 2018
    UPDATED ON: 26TH FEBRUARY, 2018
	
	NOTE: IT REQUIRES AN EXISTING DATABASE	
*/
IF NOT OBJECT_ID('PrepayrollSuspicious') IS NULL	DROP TABLE PrepayrollSuspicious
GO

IF NOT OBJECT_ID('PrepayrollIneligible') IS NULL	DROP TABLE PrepayrollIneligible
GO

IF NOT OBJECT_ID('PrepayrollDuplicateID') IS NULL	DROP TABLE PrepayrollDuplicateID
GO

IF NOT OBJECT_ID('PrepayrollInvalidID') IS NULL	DROP TABLE PrepayrollInvalidID
GO

IF NOT OBJECT_ID('Prepayroll') IS NULL	DROP TABLE Prepayroll
GO

IF NOT OBJECT_ID('PaymentAdjustment') IS NULL	DROP TABLE PaymentAdjustment
GO

IF NOT OBJECT_ID('PaymentTargetGroup') IS NULL	DROP TABLE PaymentTargetGroup
GO

IF NOT OBJECT_ID('PaymentCycle') IS NULL	DROP TABLE PaymentCycle
GO

IF NOT OBJECT_ID('DBBackup') IS NULL	DROP TABLE DBBackup
GO

IF NOT OBJECT_ID('ExpansionPlanDetail') IS NULL	DROP TABLE ExpansionPlanDetail
GO

IF NOT OBJECT_ID('ExpansionPlan') IS NULL	DROP TABLE ExpansionPlan
GO

IF NOT OBJECT_ID('ExpansionPlanMaster') IS NULL	DROP TABLE ExpansionPlanMaster
GO

IF NOT OBJECT_ID('PSPBranch') IS NULL	DROP TABLE PSPBranch
GO

IF NOT OBJECT_ID('PSP') IS NULL	DROP TABLE PSP
GO

IF NOT OBJECT_ID('Programme') IS NULL	DROP TABLE Programme
GO

IF NOT OBJECT_ID('WardLocation') IS NULL	DROP TABLE WardLocation
GO

IF NOT OBJECT_ID('Village') IS NULL	DROP TABLE Village
GO

IF NOT OBJECT_ID('SubLocation') IS NULL	DROP TABLE SubLocation
GO

IF NOT OBJECT_ID('Location') IS NULL	DROP TABLE Location
GO

IF NOT OBJECT_ID('Division') IS NULL	DROP TABLE Division
GO

IF NOT OBJECT_ID('CountyDistrict') IS NULL	DROP TABLE CountyDistrict
GO

IF NOT OBJECT_ID('District') IS NULL	DROP TABLE District
GO

IF NOT OBJECT_ID('Ward') IS NULL	DROP TABLE Ward
GO

IF NOT OBJECT_ID('Constituency') IS NULL	DROP TABLE Constituency
GO

IF NOT OBJECT_ID('County') IS NULL	DROP TABLE County
GO

IF NOT OBJECT_ID('PaymentZone') IS NULL	DROP TABLE PaymentZone
GO

IF NOT OBJECT_ID('GeoMaster') IS NULL	DROP TABLE GeoMaster
GO

IF NOT OBJECT_ID('GroupRight') IS NULL	DROP TABLE GroupRight
GO

IF NOT OBJECT_ID('ModuleRight') IS NULL	DROP TABLE ModuleRight
GO

IF NOT OBJECT_ID('Module') IS NULL	DROP TABLE Module
GO

IF NOT OBJECT_ID('SystemCodeDetail') IS NULL	DROP TABLE SystemCodeDetail
GO

IF NOT OBJECT_ID('SystemCode') IS NULL	DROP TABLE SystemCode
GO

IF NOT OBJECT_ID('User') IS NULL	DROP TABLE [User]
GO

IF NOT OBJECT_ID('UserGroup') IS NULL	DROP TABLE UserGroup
GO

IF NOT OBJECT_ID('UserGroupProfile') IS NULL	DROP TABLE UserGroupProfile
GO


/* REQUIRED

-- To allow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO 

IF NOT OBJECT_ID('fn_SystemCodeExist') IS NULL	DROP FUNCTION fn_SystemCodeExist
GO

CREATE FUNCTION fn_SystemCodeExist(@SystemCode tinyint,@Value int)
RETURNS bit
AS
BEGIN
	DECLARE @RetVal bit
	IF EXISTS(SELECT 1 
			  FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId
			  WHERE UPPER(T2.Code)=(CASE(@SystemCode) WHEN 1 THEN 'Target Group'
													  WHEN 2 THEN 'Other Programme'
													  WHEN 3 THEN 'Locality'
													  ELSE '0'
								    END
								    ) AND T1.SystemCodeDetailId=@Value
			  )
		SET @RetVal=1
	ELSE
		SET @RetVal=0
	RETURN @RetVal
END
GO


IF NOT OBJECT_ID('fn_MonthName') IS NULL	DROP FUNCTION fn_MonthName
GO
CREATE FUNCTION fn_MonthName(@MonthId tinyint)
RETURNS varchar(30)
AS
BEGIN
	RETURN CASE(@MonthId)
				WHEN 1 THEN 'January'
				WHEN 2 THEN 'February'
				WHEN 3 THEN 'March'
				WHEN 4 THEN 'April'
				WHEN 5 THEN 'May'
				WHEN 6 THEN 'June'
				WHEN 7 THEN 'July'
				WHEN 8 THEN 'August'
				WHEN 9 THEN 'September'
				WHEN 10 THEN 'October'
				WHEN 11 THEN 'November'
				WHEN 12 THEN 'December'
				ELSE ''
			END
END
GO

*/



CREATE TABLE UserGroupProfile(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_UserGroupProfile PRIMARY KEY (Id)
)
GO



CREATE TABLE UserGroup(
	Id int NOT NULL IDENTITY(1,1)
   ,UserGroupProfileId int
   ,Name varchar(20) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_UserGroup PRIMARY KEY (Id)
   ,CONSTRAINT FK_UserGroup_UserGroupProfile FOREIGN KEY (UserGroupProfileId) REFERENCES UserGroupProfile(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO


CREATE TABLE [User](
	Id int NOT NULL IDENTITY(1,1)
   ,UserGroupId int NOT NULL
   ,Email nvarchar(100) NOT NULL
   ,EmailConfirmed bit NOT NULL DEFAULT(0)
   ,[Password] nvarchar(256) NOT NULL
   ,PasswordChangeDate datetime NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL
   ,Organization varchar(100) NOT NULL
   ,Department varchar(100) NOT NULL
   ,Position varchar(100) NOT NULL
   ,MobileNo nvarchar(20) NOT NULL
   ,MobileNoConfirmed bit NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,DeactivateDate datetime NULL
   ,LoginDate datetime NULL
   ,ActivityDate datetime NULL
   ,AccessFailedCount tinyint NOT NULL DEFAULT(0)
   ,IsLocked bit NOT NULL DEFAULT(0)
   ,LockedDate datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_User PRIMARY KEY (Id)
   ,CONSTRAINT FK_User_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT U_User_UserName UNIQUE (email)
)
GO



CREATE TABLE SystemCode(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,IsUserMaintained bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_SystemCode PRIMARY KEY (Id)
   ,CONSTRAINT U_SystemCode_Code UNIQUE (Code)
)
GO



CREATE TABLE SystemCodeDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,SystemCodeId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,OrderNo tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SystemCodeDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_SystemCodeDetail_SystemCode FOREIGN KEY (SystemCodeId) REFERENCES SystemCode(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT U_SystemCodeDetail_Code UNIQUE (SystemCodeId,Code)
)
GO



CREATE TABLE Module(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,ParentModuleId tinyint NULL
   ,CONSTRAINT PK_Module PRIMARY KEY (Id)
)
GO



CREATE TABLE ModuleRight(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,ModuleId tinyint NOT NULL
   ,RightId int NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_ModuleRight PRIMARY KEY (Id)
   ,CONSTRAINT FK_ModuleRight_Module FOREIGN KEY (ModuleId) REFERENCES Module(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_ModuleRight_SystemCodeDetail FOREIGN KEY (RightId) REFERENCES SystemCodeDetail(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE GroupRight(
	UserGroupId int NOT NULL
   ,ModuleRightId tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_GroupRight PRIMARY KEY (UserGroupId,ModuleRightId)
   ,CONSTRAINT FK_GroupRight_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_ModuleRight FOREIGN KEY (ModuleRightId) REFERENCES ModuleRight(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_User FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PaymentZone(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,Commission money NOT NULL
   ,IsPerc bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_PaymentZone PRIMARY KEY (Id)
)
GO



CREATE TABLE GeoMaster(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_GeoMaster PRIMARY KEY (Id)
   ,CONSTRAINT U_GeoMaster_Name UNIQUE (Name)
   ,CONSTRAINT FK_GeoMaster_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_GeoMaster_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE County(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_County PRIMARY KEY (Id)
   ,CONSTRAINT FK_County_GeoMaster FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Constituency(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CountyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Constituency PRIMARY KEY (Id)
   ,CONSTRAINT FK_Constituency_County FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Ward(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Ward PRIMARY KEY (Id)
   ,CONSTRAINT FK_Ward_Constituency FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE District(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_District PRIMARY KEY (Id)
   ,CONSTRAINT FK_District_GeoMaster FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE CountyDistrict(
	Id int NOT NULL IDENTITY(1,1)
   ,CountyId int NOT NULL
   ,DistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CountyDistrict PRIMARY KEY (Id)
   ,CONSTRAINT FK_CountyDistrict_County FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_District FOREIGN KEY (DistrictId) REFERENCES District(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Division(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CountyDistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Division PRIMARY KEY (Id)
   ,CONSTRAINT FK_Division_CountyDistrict FOREIGN KEY (CountyDistrictId) REFERENCES CountyDistrict(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Location(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,DivisionId int NOT NULL
   ,PaymentZoneId smallint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Location PRIMARY KEY (Id)
   ,CONSTRAINT FK_Location_Division FOREIGN KEY (DivisionId) REFERENCES Division(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Location_Zone FOREIGN KEY (PaymentZoneId) REFERENCES PaymentZone(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Location_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Location_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE SubLocation(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,LocalityId int NOT NULL
   ,LocationId int NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SubLocation PRIMARY KEY (Id)
   ,CONSTRAINT FK_SubLocation_Location FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_Constituency FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_SystemCodeDetail FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE WardLocation(
	WardId int NOT NULL
   ,LocationId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_WardLocation PRIMARY KEY (WardId,LocationId)
   ,CONSTRAINT FK_WardLocation_Ward FOREIGN KEY (WardId) REFERENCES Ward(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_Location FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Village(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,SubLocationId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Village PRIMARY KEY (Id)
   ,CONSTRAINT FK_Village_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Village_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Village_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE [Programme](
	Id smallint NOT NULL IDENTITY(1,1)
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,BeneficiaryTypeId int NOT NULL	--INDIVIDUAL OR HOUSEHOLD ENTITLEMENT
   ,IsActive bit NOT NULL DEFAULT(0)
   ,EntitlementAmount money NOT NULL DEFAULT(0)
   ,PrimaryRecipientId int NOT NULL
   ,PrimaryRecipientMandatory bit NOT NULL DEFAULT(0)
   ,SecondaryRecipientId int NOT NULL
   ,SecondaryRecipientMandatory bit NOT NULL DEFAULT(0)
   ,PaymentFrequencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Programme PRIMARY KEY (Id) 
   ,CONSTRAINT U_Programme_Code UNIQUE (Code)
   ,CONSTRAINT FK_Programme_SystemCodeDetail1 FOREIGN KEY (BeneficiaryTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail2 FOREIGN KEY (PrimaryRecipientId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail3 FOREIGN KEY (SecondaryRecipientId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail4 FOREIGN KEY (PaymentFrequencyId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PSP(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,CommissionTypeId int NOT NULL
   ,CommissionValue money NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,UserId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSP PRIMARY KEY (Id)
   ,CONSTRAINT U_PSP_Code UNIQUE (Code)
   ,CONSTRAINT FK_PSP_User1 FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User2 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User3 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail FOREIGN KEY (CommissionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PSPBranch(
	Id smallint NOT NULL IDENTITY(1,1)
   ,PSPId smallint NOT NULL
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,SubLocationId int NULL
   ,Latitude float NULL
   ,Longitude float NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSPBranch PRIMARY KEY (Id)
   ,CONSTRAINT FK_PSPBranch_PSP FOREIGN KEY (PSPId) REFERENCES PSP(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT U_PSPBranch_Code UNIQUE (PSPId,Code)
   ,CONSTRAINT FK_PSPBranch_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE ExpansionPlanMaster(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,ProgrammeId smallint NOT NULL
   ,EffectiveFromDate datetime NOT NULL
   ,EffectiveToDate datetime NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlanMaster PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlanMaster_Programme FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanMaste_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanMaster_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT U_ExpansionPlanMaster_Code UNIQUE (Code)
)
GO



CREATE TABLE ExpansionPlan(
	Id int NOT NULL IDENTITY(1,1)
   ,ExpansionPlanMasterId smallint NOT NULL
   ,LocationId int NOT NULL
   ,PovertyHeadCountPerc float --SMALL AREA ESTIMATE
   ,CategoricalHHs int
   ,ScaleupEqualShare int
   ,ScaleupPovertyPrioritized int
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlan PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlan_ExpansionPlanMaster FOREIGN KEY (ExpansionPlanMasterId) REFERENCES ExpansionPlanMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_Location FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE ExpansionPlanDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,ExpansionPlanId int NOT NULL
   ,FinancialYearId int NOT NULL
   ,EnrolledHHs int NOT NULL DEFAULT(0)
   ,ScaleupEqualShare int NOT NULL DEFAULT(0)
   ,ScaleupPovertyPrioritized int NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlanDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlanDetail_ExpansionPlan FOREIGN KEY (ExpansionPlanId) REFERENCES ExpansionPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_SystemCodeDetail FOREIGN KEY (FinancialYearId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE DBBackup(
	DBBackupId int NOT NULL IDENTITY(1,1)
   ,FilePath nvarchar(128) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT FK_DBBackup_User FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE PaymentCycle(
	Id int NOT NULL IDENTITY(1,1)
   ,ProgrammeId smallint NOT NULL
   ,[Description] varchar(50) NOT NULL
   ,FinancialPeriodId int NOT NULL
   ,FromMonth int NOT NULL
   ,ToMonth int NOT NULL
   ,PaymentStageId int NOT NULL
   ,EnrolledHHs int NULL
   ,PrePayrollBy int NULL
   ,PrePayrollOn datetime NULL
   ,PrepayrollApvBy int NULL
   ,PrepayrollApvOn datetime NULL
   ,PayrollBy int NULL
   ,PayrollOn datetime NULL
   ,PayrollVerBy int NULL
   ,PayrollVerOn datetime NULL
   ,PayrollApvBy int NULL
   ,PayrollApvOn datetime NULL
   ,PayrollExBy int NULL
   ,PayrollExOn datetime NULL
   ,PayrollExConfBy int NULL
   ,PayrollExConfOn datetime NULL
   ,PostPayrollBy int NULL
   ,PostPayrollOn datetime NULL
   ,PostPayrollApvby int NULL
   ,PostPayrollApvOn datetime NULL
   ,ReconciledBy int NULL
   ,ReconciledOn datetime NULL
   ,ReconciledApvBy int NULL
   ,ReconciledApvOn datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeletedBy int NULL
   ,DeletedOn datetime NULL
   ,CONSTRAINT PK_PaymentCycle PRIMARY KEY (Id)
   ,CONSTRAINT FK_PaymetCycle_Programme FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymetCycle_SystemCodeDetail1 FOREIGN KEY (FinancialPeriodId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_SystemCodeDetail2 FOREIGN KEY (FromMonth) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_SystemCodeDetail3 FOREIGN KEY (ToMonth) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_SystemCodeDetail4 FOREIGN KEY (PaymentStageId) REFERENCES SystemCodeDetail(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymetCycle_User1 FOREIGN KEY (PrePayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User2 FOREIGN KEY (PrepayrollApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User3 FOREIGN KEY (PayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User4 FOREIGN KEY (PayrollVerBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User5 FOREIGN KEY (PayrollApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User6 FOREIGN KEY (PayrollExBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User7 FOREIGN KEY (PayrollExConfBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User8 FOREIGN KEY (PostPayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User9 FOREIGN KEY (PostPayrollApvby) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User10 FOREIGN KEY (ReconciledBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User11 FOREIGN KEY (ReconciledApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User12 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User13 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymetCycle_User14 FOREIGN KEY (DeletedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION


)
GO



CREATE TABLE PaymentTargetGroup(
	PaymentTargetId int NOT NULL IDENTITY(1,1)
   ,PaymentCycleId int NOT NULL
   ,TargetGroupId int NOT NULL
   ,PaymentAmount money NOT NULL
   ,CONSTRAINT PK_PaymentTargetGroup PRIMARY KEY (PaymentTargetId)
   ,CONSTRAINT FK_PaymentTargetGroup_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE CASCADE
)
GO



CREATE TABLE PaymentAdjustment(
	PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,AdjustmentTypeId int NOT NULL
   ,AdjustmentAmount money NOT NULL
   ,AmountAdjusted money NOT NULL DEFAULT(0.00)
   ,RefNo nvarchar(50) NULL
   ,Notes nvarchar(128) NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_PaymentAdjustment PRIMARY KEY (PaymentCycleId,HHId,AdjustmentTypeId)
   --,CONSTRAINT FK_PaymentAdjustment_Household FOREIGN KEY (HHId) REFERENCES Household(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymentAdjustment_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAdjustment_SystemCodeDetail FOREIGN KEY (AdjustmentTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAdjustment_User FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO


 
CREATE TABLE Prepayroll(
    PaymentCycleId int NOT NULL
   ,HhId int NOT NULL
   ,BeneMemberId int NOT NULL
   ,BeneFirstName varchar(50) NOT NULL
   ,BeneMiddleName varchar(50) NULL
   ,BeneSurname varchar(50) NOT NULL
   ,BeneDoB datetime NOT NULL
   ,BeneSexId int NOT NULL
   ,BeneNationalIDNo varchar(30) NULL
   ,CanReceivePayment bit NOT NULL DEFAULT(0)
   ,CGMemberId int NOT NULL
   ,CGFirstName varchar(50) NULL
   ,CGMiddleName varchar(50) NULL
   ,CGSurname varchar(50) NOT NULL
   ,CGDoB datetime NULL
   ,CGSexId int NULL
   ,CGNationalIDNo varchar(30) NULL
   ,TotalHHMembers int NOT NULL
   ,SubLocationId int NOT NULL
   ,PaymentZoneId smallint NOT NULL
   ,PaymentZoneCommAmt money NOT NULL
   ,ConseAccInactivity tinyint NOT NULL
   ,EntitlementAmount money NULL
   ,AdjustmentAmount money NOT NULL
   ,CONSTRAINT PK_Prepayroll PRIMARY KEY (PaymentCycleId,HhId)
   ,CONSTRAINT FK_Prepayroll_PaymentCycle FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   --,CONSTRAINT FK_Prepayroll_Household FOREIGN KEY (HHId) REFERENCES Household(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   --,CONSTRAINT FK_Prepayroll_Member1 FOREIGN KEY (BeneMemberId) REFERENCES Member(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_Member2 FOREIGN KEY (BeneSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   --,CONSTRAINT FK_Prepayroll_Member3 FOREIGN KEY (CGMemberId) REFERENCES Member(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_Member4 FOREIGN KEY (CGSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_PaymentZone FOREIGN KEY (PaymentZoneId) REFERENCES PaymentZone(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollInvalidID(
    PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,MemberId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NOT NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollInvalidID PRIMARY KEY (PaymentCycleId,HhId,MemberId)
   ,CONSTRAINT FK_PrepayrollInvalidID_Prepayroll FOREIGN KEY (PaymentCycleId,HhId) REFERENCES Prepayroll(PaymentCycleId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollInvalidID_User1 FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollInvalidID_User2 FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollDuplicateID(
    PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,MemberId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NOT NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollDuplicateID PRIMARY KEY (PaymentCycleId,HhId,MemberId)
   ,CONSTRAINT FK_PrepayrollDuplicateID_Prepayroll FOREIGN KEY (PaymentCycleId,HhId) REFERENCES Prepayroll(PaymentCycleId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollDuplicateID_User1 FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollDuplicateID_User2 FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollIneligible(
    PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NOT NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollIneligible PRIMARY KEY (PaymentCycleId,HhId)
   ,CONSTRAINT FK_PrepayrollIneligible_Prepayroll FOREIGN KEY (PaymentCycleId,HhId) REFERENCES Prepayroll(PaymentCycleId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollIneligible_User1 FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollIneligible_User2 FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollSuspicious(
    PaymentCycleId int NOT NULL
   ,HHId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NOT NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollSuspicious PRIMARY KEY (PaymentCycleId,HhId)
   ,CONSTRAINT FK_PrepayrollSuspicious_Prepayroll FOREIGN KEY (PaymentCycleId,HhId) REFERENCES Prepayroll(PaymentCycleId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollSuspicious_User1 FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollSuspicious_User2 FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO


