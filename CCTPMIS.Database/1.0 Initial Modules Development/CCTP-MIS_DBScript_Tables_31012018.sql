


/*
	 1. Identification
	 2. Screener
	 3. Bio-Data
	 4. Education
	 5. Dwelling
	 6. Assets
	 7. Business
	 8. Expenditure
	 9. Income
	10. Meals
	11. Control
	12. Summary


	CODES
	=====
	 1. Gender
	 2. Marrital Status
	 3. Illness Type
	 4. Disability Type
	 5. Target Group
	 6. Education Level
	 7. Occupation Category
	 8. Orphanhood
	 9. Member Relationship
	10. Non-Schooling Reasons
	11. Household Items
	12. Wall Construction Material
	13. Floor Construction Material
	14. Roof Construction Material
	15. Toilet Type
	16. Drinking Water Source
	17. Lighting Source
	18. Cooking Fuel Source
	19. Household Status
	20. Monthly Rental Range
	21. Livelihood Source
	22. Savings Storage Type
	23. Frequency
	24. Household Expenditure Type
	25. Household Income Type
	26. Household Nutrition Type
	27. Locality (of Rural or Urban only) 
	
*/

IF NOT OBJECT_ID('DBBackup') IS NULL	DROP TABLE DBBackup
GO

IF NOT OBJECT_ID('ExpansionPlan') IS NULL	DROP TABLE ExpansionPlan
GO

IF NOT OBJECT_ID('PSPBranch') IS NULL	DROP TABLE PSPBranch
GO

IF NOT OBJECT_ID('PSP') IS NULL	DROP TABLE PSP
GO

IF NOT OBJECT_ID('Programme') IS NULL	DROP TABLE Programme
GO

IF NOT OBJECT_ID('WardLocation') IS NULL	DROP TABLE WardLocation
GO

IF NOT OBJECT_ID('SubLocation') IS NULL	DROP TABLE SubLocation
GO

IF NOT OBJECT_ID('Location') IS NULL	DROP TABLE Location
GO

IF NOT OBJECT_ID('Division') IS NULL	DROP TABLE Division
GO

IF NOT OBJECT_ID('CountyDistrict') IS NULL	DROP TABLE CountyDistrict
GO

IF NOT OBJECT_ID('District') IS NULL	DROP TABLE District
GO

IF NOT OBJECT_ID('Ward') IS NULL	DROP TABLE Ward
GO

IF NOT OBJECT_ID('Constituency') IS NULL	DROP TABLE Constituency
GO

IF NOT OBJECT_ID('County') IS NULL	DROP TABLE County
GO

IF NOT OBJECT_ID('GeoMaster') IS NULL	DROP TABLE GeoMaster
GO

IF NOT OBJECT_ID('GroupRight') IS NULL	DROP TABLE GroupRight
GO

IF NOT OBJECT_ID('ModuleRight') IS NULL	DROP TABLE ModuleRight
GO

IF NOT OBJECT_ID('Module') IS NULL	DROP TABLE Module
GO

IF NOT OBJECT_ID('SystemCodeDetail') IS NULL	DROP TABLE SystemCodeDetail
GO

IF NOT OBJECT_ID('SystemCode') IS NULL	DROP TABLE SystemCode
GO

IF NOT OBJECT_ID('User') IS NULL	DROP TABLE [User]
GO

IF NOT OBJECT_ID('UserGroup') IS NULL	DROP TABLE UserGroup
GO

IF NOT OBJECT_ID('UserGroupProfile') IS NULL	DROP TABLE UserGroupProfile
GO


/* REQUIRED

-- To allow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO 

IF NOT OBJECT_ID('fn_SystemCodeExist') IS NULL	DROP FUNCTION fn_SystemCodeExist
GO

CREATE FUNCTION fn_SystemCodeExist(@SystemCode tinyint,@Value int)
RETURNS bit
AS
BEGIN
	DECLARE @RetVal bit
	IF EXISTS(SELECT 1 
			  FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId
			  WHERE UPPER(T2.Code)=(CASE(@SystemCode) WHEN 1 THEN 'Target Group'
													  WHEN 2 THEN 'Other Programme'
													  WHEN 3 THEN 'Locality'
													  ELSE '0'
								    END
								    ) AND T1.SystemCodeDetailId=@Value
			  )
		SET @RetVal=1
	ELSE
		SET @RetVal=0
	RETURN @RetVal
END
GO


IF NOT OBJECT_ID('fn_MonthName') IS NULL	DROP FUNCTION fn_MonthName
GO
CREATE FUNCTION fn_MonthName(@MonthId tinyint)
RETURNS varchar(30)
AS
BEGIN
	RETURN CASE(@MonthId)
				WHEN 1 THEN 'January'
				WHEN 2 THEN 'February'
				WHEN 3 THEN 'March'
				WHEN 4 THEN 'April'
				WHEN 5 THEN 'May'
				WHEN 6 THEN 'June'
				WHEN 7 THEN 'July'
				WHEN 8 THEN 'August'
				WHEN 9 THEN 'September'
				WHEN 10 THEN 'October'
				WHEN 11 THEN 'November'
				WHEN 12 THEN 'December'
				ELSE ''
			END
END
GO

*/



CREATE TABLE UserGroupProfile(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_UserGroupProfile PRIMARY KEY (Id)
)
GO



CREATE TABLE UserGroup(
	Id int NOT NULL IDENTITY(1,1)
   ,UserGroupProfileId int
   ,Name varchar(20) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_UserGroup PRIMARY KEY (Id)
   ,CONSTRAINT FK_UserGroup_UserGroupProfile FOREIGN KEY (UserGroupProfileId) REFERENCES UserGroupProfile(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE [User](
	Id int NOT NULL IDENTITY(1,1)
   ,UserGroupId int NOT NULL
   ,email nvarchar(100) NOT NULL
   ,emailConfirmed bit NOT NULL DEFAULT(0)
   ,[Password] nvarchar(256) NOT NULL
   ,PasswordChangeDate datetime NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL
   ,Organization varchar(100) NOT NULL
   ,Department varchar(100) NOT NULL
   ,Position varchar(100) NOT NULL
   ,MobileNo nvarchar(20) NOT NULL
   ,MobileNoConfirmed bit NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,DeactivateDate datetime NULL
   ,LoginDate datetime NULL
   ,ActivityDate datetime NULL
   ,AccessFailedCount tinyint DEFAULT(0)
   ,IsLocked bit NOT NULL DEFAULT(0)
   ,LockedDate datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_User PRIMARY KEY (Id)
   ,CONSTRAINT FK_User_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT U_User_UserName UNIQUE (email)
)
GO



CREATE TABLE SystemCode(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,IsUserMaintained bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_SystemCode PRIMARY KEY (Id)
   ,CONSTRAINT U_SystemCode_Code UNIQUE (Code)
)
GO



CREATE TABLE SystemCodeDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,SystemCodeId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,OrderNo tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SystemCodeDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_SystemCodeDetail_SystemCode FOREIGN KEY (SystemCodeId) REFERENCES SystemCode(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT U_SystemCodeDetail_Code UNIQUE (SystemCodeId,Code)
)
GO



CREATE TABLE Module(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,ParentModuleId tinyint NULL
   ,CONSTRAINT PK_Module PRIMARY KEY (Id)
)
GO



CREATE TABLE ModuleRight(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,ModuleId tinyint NOT NULL
   ,RightId int NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_ModuleRight PRIMARY KEY (Id)
   ,CONSTRAINT FK_ModuleRight_Module FOREIGN KEY (ModuleId) REFERENCES Module(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_ModuleRight_SystemCodeDetail FOREIGN KEY (RightId) REFERENCES SystemCodeDetail(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE GroupRight(
	UserGroupId int NOT NULL
   ,ModuleRightId tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_GroupRight PRIMARY KEY (UserGroupId,ModuleRightId)
   ,CONSTRAINT FK_GroupRight_UserGroup FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_ModuleRight FOREIGN KEY (ModuleRightId) REFERENCES Module(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_User FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO




CREATE TABLE GeoMaster(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_GeoMaster PRIMARY KEY (Id)
   ,CONSTRAINT U_GeoMaster_Name UNIQUE (Name)
   ,CONSTRAINT FK_GeoMaster_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_GeoMaster_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE County(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_County PRIMARY KEY (Id)
   ,CONSTRAINT FK_County_GeoMaster FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Constituency(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CountyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Constituency PRIMARY KEY (Id)
   ,CONSTRAINT FK_Constituency_County FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Ward(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Ward PRIMARY KEY (Id)
   ,CONSTRAINT FK_Ward_Constituency FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE District(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_District PRIMARY KEY (Id)
   ,CONSTRAINT FK_District_GeoMaster FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE CountyDistrict(
	CountyId int NOT NULL
   ,DistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CountyDistrict PRIMARY KEY (CountyId,DistrictId)
   ,CONSTRAINT FK_CountyDistrict_County FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_District FOREIGN KEY (DistrictId) REFERENCES District(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Division(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,DistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Division PRIMARY KEY (Id)
   ,CONSTRAINT FK_Division_District FOREIGN KEY (DistrictId) REFERENCES District(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Location(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,DivisionId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Location PRIMARY KEY (Id)
   ,CONSTRAINT FK_Location_Division FOREIGN KEY (DivisionId) REFERENCES Division(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Location_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Location_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE SubLocation(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,LocalityId int NOT NULL
   ,LocationId int NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SubLocation PRIMARY KEY (Id)
   ,CONSTRAINT FK_SubLocation_Location FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_Constituency FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_SystemCodeDetail FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE WardLocation(
	WardId int NOT NULL
   ,LocationId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_WardLocation PRIMARY KEY (WardId,LocationId)
   ,CONSTRAINT FK_WardLocation_Ward FOREIGN KEY (WardId) REFERENCES Ward(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_Location FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PSP(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,CommissionTypeId int NOT NULL
   ,CommissionValue money NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,UserId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSP PRIMARY KEY (Id)
   ,CONSTRAINT U_PSP_Code UNIQUE (Code)
   ,CONSTRAINT FK_PSP_User1 FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User2 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User3 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PSPBranch(
	Id smallint NOT NULL IDENTITY(1,1)
   ,PSPId smallint NOT NULL
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSPBranch PRIMARY KEY (Id)
   ,CONSTRAINT FK_PSPBranch_PSP FOREIGN KEY (PSPId) REFERENCES PSP(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT U_PSPBranch_Code UNIQUE (PSPId,Code)
   ,CONSTRAINT FK_PSPBranch_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE DBBackup(
	DBBackupId int NOT NULL IDENTITY(1,1)
   ,FilePath nvarchar(128) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT FK_DBBackup_User FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO
