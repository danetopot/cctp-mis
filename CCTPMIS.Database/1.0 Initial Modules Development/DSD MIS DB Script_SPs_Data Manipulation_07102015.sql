

/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB SPs FOR DATA MANIPULATION FOR THE NRW DSD WEB MIS
	CREATED BY: JOAB SELELYA (NSNP MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH OCTOBER, 2014
   MODIFIED ON: 
	
	NOTE: IT REQUIRES AN EXISTING DATABASE
*/

IF NOT OBJECT_ID('AddEditUserGroup') IS NULL	DROP PROC AddEditUserGroup
GO
CREATE PROC AddEditUserGroup
	@UserGroupId int=NULL
   ,@UserGroupName varchar(20)
   ,@IsActive bit=NULL
   ,@UserGroupRightsXML XML
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @IsActive=ISNULL(@IsActive,1)

	IF ISNULL(@UserGroupName,'')=''
		SET @ErrorMsg='Please specify valid UserGroupName parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @UserGroupId>0
	BEGIN
		UPDATE T1
		SET T1.UserGroupName=@UserGroupName
		   ,T1.IsActive=@IsActive
		FROM UserGroup T1
		WHERE T1.UserGroupId=@UserGroupId
	END
	ELSE
	BEGIN
		INSERT INTO UserGroup(UserGroupName,IsActive)
		SELECT @UserGroupName,@IsActive

		SET @UserGroupId=IDENT_CURRENT('UserGroup')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	UPDATE T2
	SET T2.CanView=T1.CanView
	   ,T2.CanAddEdit=T1.CanAddEdit
	   ,T2.CanDelete=T1.CanDelete
	FROM (	SELECT U.R.value('(ModuleId)[1]','int') AS ModuleId
				,U.R.value('(CanView)[1]','bit') AS CanView
				,U.R.value('(CanAddEdit)[1]','bit') AS CanAddEdit
				,U.R.value('(CanDelete)[1]','bit') AS CanDelete
			FROM @UserGroupRightsXML.nodes('UserGroupRights/Record') AS U(R)
			) T1 INNER JOIN UserGroupRight T2 ON T2.UserGroupId=@UserGroupId AND T1.ModuleId=T2.ModuleId

	INSERT INTO UserGroupRight(UserGroupId,ModuleId,CanView,CanAddEdit,CanDelete)
	SELECT @UserGroupId AS UserGroupId,T1.ModuleId,T1.CanView,T1.CanAddEdit,T1.CanDelete
	FROM (	SELECT U.R.value('(ModuleId)[1]','int') AS ModuleId
				,U.R.value('(CanView)[1]','bit') AS CanView
				,U.R.value('(CanAddEdit)[1]','bit') AS CanAddEdit
				,U.R.value('(CanDelete)[1]','bit') AS CanDelete
			FROM @UserGroupRightsXML.nodes('UserGroupRights/Record') AS U(R)
			) T1 LEFT JOIN UserGroupRight T2 ON T2.UserGroupId=@UserGroupId AND T1.ModuleId=T2.ModuleId
	WHERE T2.UserGroupId IS NULL

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetUserGroups	

		EXEC GetModules
	END
END
GO



IF NOT OBJECT_ID('DeleteUserGroup') IS NULL	DROP PROC DeleteUserGroup
GO
CREATE PROC DeleteUserGroup
	@UserGroupId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM [User] WHERE UserGroupId=@UserGroupId)
		SET @ErrorMsg='The specified UserGroupId has dependant child records'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM UserGroup T1
	WHERE T1.UserGroupId=@UserGroupId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetUserGroups	

	EXEC GetModules
END
GO



IF NOT OBJECT_ID('AddEditUser') IS NULL	DROP PROC AddEditUser
GO
CREATE PROC AddEditUser
	@UserId int=NULL
   ,@UserGroupId int
   ,@UserName varchar(20)
   ,@Password nvarchar(128)
   ,@IsActive bit=NULL
   ,@IsLocked bit=NULL
   ,@CurrentUserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsActive=ISNULL(@IsActive,1)
	SET @IsLocked=ISNULL(@IsLocked,0)
	SELECT @CurrentUserId=UserId FROM [User] WHERE UserId=@CurrentUserId

	IF NOT EXISTS(SELECT 1 FROM UserGroup WHERE UserGroupId=@UserGroupId)
		SET @ErrorMsg='Please specify valid UserGroupId parameter'
	ELSE IF ISNULL(@UserName,'')=''
		SET @ErrorMsg='Please specify valid UserName parameter'
	ELSE IF ISNULL(@Password,'')=''
		SET @ErrorMsg='Please specify valid Password parameter'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	IF @UserId>0
	BEGIN
		UPDATE T1
		SET T1.UserGroupId=@UserGroupId
		   ,T1.UserName=@UserName
		   ,T1.[Password]=@Password
		   ,T1.IsActive=@IsActive
		   ,T1.ModifiedBy=@CurrentUserId
		   ,T1.ModifiedOn=GETDATE()
		FROM [User] T1
		WHERE T1.UserId=@UserId
	END
	ELSE
	BEGIN
		INSERT INTO [User](UserGroupId,UserName,[Password],IsActive,CreatedBy,CreatedOn)
		SELECT @UserGroupId,@UserName,@Password,@IsActive,@CurrentUserId,GETDATE()
	END

	SELECT @@ROWCOUNT AS NoOfRows

	EXEC GetUsers
END
GO



IF NOT OBJECT_ID('UpdateUser') IS NULL	DROP PROC UpdateUser
GO
CREATE PROC UpdateUser
	@UserName varchar(20)
   ,@CurrentPassword nvarchar(128)
   ,@NewPassword nvarchar(128)
   ,@ConfirmPassword nvarchar(128)
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE UPPER(UserName)=UPPER(@UserName))
		SET @ErrorMsg='Invalid UserName'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE UPPER(UserName)=UPPER(@UserName) AND IsActive=1)
		SET @ErrorMsg='The User is inactive'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE UserName=@UserName AND [Password] COLLATE Latin1_General_CS_AS=@CurrentPassword COLLATE Latin1_General_CS_AS)
		SET @ErrorMsg='Current Password mismatch'
	ELSE IF @NewPassword COLLATE Latin1_General_CS_AS<>@ConfirmPassword COLLATE Latin1_General_CS_AS
		SET @ErrorMsg='New and Confirm Password mismatch'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	UPDATE T1
	SET T1.[Password]=@NewPassword
	FROM [User] T1
	WHERE T1.UserName=@UserName

	SELECT @@ROWCOUNT AS NoOfRows
END
GO



IF NOT OBJECT_ID('DeleteUser') IS NULL	DROP PROC DeleteUser
GO
CREATE PROC DeleteUser
	@UserId int
AS
BEGIN
	DELETE T1
	FROM [User] T1
	WHERE T1.UserId=@UserId

	SELECT @@ROWCOUNT AS NoOfRows

	EXEC GetUsers
END
GO



IF NOT OBJECT_ID('SignInUser') IS NULL	DROP PROC SignInUser
GO
CREATE PROC SignInUser
	@UserName varchar(20)
   ,@Password nvarchar(128)
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@UserName,'')=''
		SET @ErrorMsg='Please specify valid UserName parameter'
	ELSE IF ISNULL(@Password,'')=''
		SET @ErrorMsg='Please specify valid Password parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT T1.UserId,T1.UserName,T2.UserGroupId,T2.UserGroupName
	FROM [User] T1 INNER JOIN UserGroup T2 ON T1.UserGroupId=T2.UserGroupId
	WHERE T1.UserName=@UserName AND T1.[Password] COLLATE  Latin1_General_CS_AS=@Password COLLATE  Latin1_General_CS_AS

END
GO



IF NOT OBJECT_ID('AddEditSystemCode') IS NULL	DROP PROC AddEditSystemCode
GO
CREATE PROC AddEditSystemCode
	@SystemCodeId int=NULL
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCode WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @SystemCodeId>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCode T1
		WHERE T1.SystemCodeId=@SystemCodeId
	END
	ELSE
	BEGIN
		INSERT INTO SystemCode(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END
GO



IF NOT OBJECT_ID('AddEditSystemCodeDetail') IS NULL	DROP PROC AddEditSystemCodeDetail
GO
CREATE PROC AddEditSystemCodeDetail
	@SystemCodeDetailId int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(20)=NULL
   ,@DetailCode varchar(20)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=SystemCodeId
		FROM SystemCode
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF ISNULL(@Description,'')=''
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SystemCodeId AND DetailCode=@DetailCode AND SystemCodeDetailId<>ISNULL(@SystemCodeDetailId,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@SystemCodeDetailId,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.DetailCode=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetail T1
		WHERE T1.SystemCodeDetailId=@SystemCodeDetailId
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetail(SystemCodeId,DetailCode,[Description],OrderNo,CreatedBy,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @SystemCodeDetailId=IDENT_CURRENT('SystemCodeDetail')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetSystemCodeDetails	
	END
END
GO



IF NOT OBJECT_ID('DeleteSystemCodeDetail') IS NULL	DROP PROC DeleteSystemCodeDetail
GO
CREATE PROC DeleteSystemCodeDetail
	@SystemCodeDetailId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	--IF EXISTS(SELECT 1 FROM Household T1 WHERE T1.PSPBranchId=@PSPBranchId)
	--	SET @ErrorMsg='The specified PSPBranch has dependant child records'
	--CONSIDER CHECKING DEPENDANT TABLES BASED ON SYSTEM CODE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM SystemCodeDetail T1
	WHERE T1.SystemCodeDetailId=@SystemCodeDetailId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetSystemCodeDetails
END
GO



IF NOT OBJECT_ID('AddEditTargeting') IS NULL	DROP PROC AddEditTargeting
GO
CREATE PROC AddEditTargeting
	@HHId int
   ,@ProgrammeId int
   ,@TargetGroupId int
   ,@CaptureDate datetime
   ,@SerialNo int
   ,@SubLocationId int
   ,@Village varchar(50)
   ,@Ward varchar(50)
   ,@LocalityId int
   ,@PhysicalAddress varchar(50)
   ,@NearestLandmark varchar(50)
   ,@Longitude float
   ,@Latitude float
   ,@Elevation float

   ,@HHMembersXML XML
   
   ,@HHItemsXML XML
   ,@HHCharacteristicsXML XML

   ,@HHAssetsXML XML

   ,@HHBusinessXML XML

   ,@HHIncomesXML XML
   ,@HHExpendituresXML XML

   ,@HHNutritionsXML XML
   ,@AverageDailyMealsCount tinyint
   ,@AverageWeeklyNoMealsCount tinyint
   ,@AverageWeeklyNoMealsReason varchar(50)

   ,@PSPBranchId int
   ,@LoCNames varchar(50)
   ,@LoCNationalIdNo varchar(30)
   ,@LoCPhone varchar(30)
   ,@TargetingObservations varchar(128)

   ,@HHStatusId int=NULL

   ,@UserId int
AS
BEGIN
	DECLARE @tblMembers TABLE(
		MemberId int
	   ,FirstName varchar(50)
	   ,MiddleName varchar(50)
	   ,Surname varchar(50)
	   ,MemberRoleId int
	   ,RelationshipId int
	   ,ExternalMember bit
	   ,BirthCertNo varchar(30)
	   ,NationalIdNo varchar(30)
	   ,DateOfBirth datetime
	   ,SexId int
	   ,EducationLevelId int
	   ,IlliteracyReasonId int
	   ,DisabilityTypeId int
	   ,NCPwDNo varchar(30)
	   ,IllnessTypeId int
	   ,OrphanhoodTypeId int
	   ,MaritalStatusId int
	   ,Phone1 varchar(20)
	   ,Phone2 varchar(20)
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHItems TABLE(
		HHItemTypeId int
	   ,HHItemCount tinyint
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHIncomes TABLE(
		HHIncomeTypeId int
	   ,HHIncomeAmount money
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHExpenditures TABLE(
		HHExpenditureTypeId int
	   ,HHExpenditureAmount money
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHNutrition TABLE(
		HHNutritionTypeId int
	   ,EatenRaw bit
	   ,PkgUnitSize varchar(30)
	   ,PkgUnitCost money
	   ,WeeklyConsumptionFreq tinyint
	   ,FoodSource varchar(30)
	   ,ChoiceInfluence varchar(30)
	   ,ModeOfTransport varchar(30)
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @ErrorMsg varchar(128)
   
    DECLARE @WallMaterialId int
		   ,@FloorMaterialId int
		   ,@RoofMaterialId int
		   ,@ToiletTypeId int
		   ,@WaterSourceId int
		   ,@LightingSourceId int
		   ,@CookingFuelId int

	DECLARE @OwnHouse bit
		   ,@MonthlyRental money
		   ,@HouseRooms tinyint
		   ,@FarmingLandSize tinyint
		   ,@GrazingLandSize tinyint
		   ,@ZebuCattle tinyint
		   ,@HybridCattle tinyint
		   ,@Goats tinyint
		   ,@Sheep tinyint
		   ,@Pigs tinyint
		   ,@Camels tinyint
		   ,@Donkeys tinyint
		   ,@Poultry tinyint
		   ,@Rabbits tinyint
		   ,@LivelihoodSourceId int
		   ,@PermanentIncome money
		   ,@ContractIncome money
		   ,@CasualIncome money
		   ,@OtherIncome money

	DECLARE @BusinessDescription varchar(50)
		   ,@TotalEmployees tinyint
		   ,@BusinessValue money
		   ,@PremisesMonthlyRental money
		   ,@StatutoryFees money
		   ,@AverageMonthlyProfit money
		   ,@SavingsStorageTypeId int
		   ,@SavingsAmount money
		   ,@OtherProgId tinyint
		   ,@OtherProgBenefitFreqId int
		   ,@OtherProgMonthlyBenefitAmount money

	DECLARE @HHStatusCode varchar(20)
	DECLARE @HHStatus_RegistrationCode varchar(20)
	DECLARE @HHStatus_RegistrationId int
	DECLARE @TargetGroupCode varchar(20)
	DECLARE @WallMaterialCode varchar(20)
	DECLARE @FloorMaterialCode varchar(20)
	DECLARE @RoofMaterialCode varchar(20)
	DECLARE @ToiletTypeCode varchar(20)
	DECLARE @WaterSourceCode varchar(20)
	DECLARE @LightingSourceCode varchar(20)
	DECLARE @CookingFuelCode varchar(20)
	DECLARE @LivelihoodSourceCode varchar(20)
	DECLARE @OtherProgCode varchar(20)
	DECLARE @FrequencyCode varchar(20)

	DECLARE @MemberRoleCode varchar(20)
	DECLARE @RelationshipCode varchar(20)
	DECLARE @SexCode varchar(20)
	DECLARE @EducationLevelCode varchar(20)
	DECLARE @IlliteracyReasonCode varchar(20)
	DECLARE @DisabilityCode varchar(20)
	DECLARE @IllnessCode varchar(20)
	DECLARE @OrphanhoodCode varchar(20)
	DECLARE @MaritalStatusCode varchar(20)

	SET @HHStatusCode='HHStatus'
	SET @HHStatus_RegistrationCode='0'
	SET @TargetGroupCode='Target Group'
	SET @WallMaterialCode='Wall Material'
	SET @FloorMaterialCode='Floor Material'
	SET @RoofMaterialCode='Roof Material'
	SET @ToiletTypeCode='Toilet Type'
	SET @WaterSourceCode='Water Source'
	SET @LightingSourceCode='Lighting Source'
	SET @CookingFuelCode='Cooking Fuel'
	SET @LivelihoodSourceCode='Livelihood'
	SET @OtherProgCode = 'Other Programme'
	SET @FrequencyCode = 'Frequency'

	SET @MemberRoleCode='Member Role'
	SET @RelationshipCode='Relationship'
	SET @SexCode='Sex'
	SET @EducationLevelCode='Education Level'
	SET @IlliteracyReasonCode='Illiteracy Reason'
	SET @DisabilityCode='Disability'
	SET @IllnessCode='Illness'
	SET @OrphanhoodCode='Orphanhood'
	SET @MaritalStatusCode='Marital Status'

	SELECT @HHStatus_RegistrationId=T1.SystemCodeDetailId
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@HHStatusCode
	WHERE T1.DetailCode=@HHStatus_RegistrationCode

	
	SELECT @WallMaterialId=T1.WallMaterialId
		  ,@FloorMaterialId=T1.FloorMaterialId
		  ,@RoofMaterialId=T1.RoofMaterialId
		  ,@ToiletTypeId=T1.ToiletTypeId
		  ,@WaterSourceId=T1.WaterSourceId
		  ,@LightingSourceId=T1.LightingSourceId
		  ,@CookingFuelId=T1.CookingFuelId
	FROM (
			SELECT U.R.value('(WallMaterialId)[1]','int') AS WallMaterialId
				,U.R.value('(FloorMaterialId)[1]','int') AS FloorMaterialId
				,U.R.value('(RoofMaterialId)[1]','int') AS RoofMaterialId
				,U.R.value('(ToiletTypeId)[1]','int') AS ToiletTypeId
				,U.R.value('(WaterSourceId)[1]','int') AS WaterSourceId
				,U.R.value('(LightingSourceId)[1]','int') AS LightingSourceId
				,U.R.value('(CookingFuelId)[1]','int') AS CookingFuelId
			FROM @HHCharacteristicsXML.nodes('HHCharacteristics/Record') AS U(R)
		) T1
		
	SELECT @OwnHouse=T1.OwnHouse
		  ,@MonthlyRental=T1.MonthlyRental
		  ,@HouseRooms=T1.HouseRooms
		  ,@FarmingLandSize=T1.FarmingLandSize
		  ,@GrazingLandSize=T1.GrazingLandSize
		  ,@ZebuCattle=T1.ZebuCattle
		  ,@HybridCattle=T1.HybridCattle
		  ,@Goats=T1.Goats
		  ,@Sheep=T1.Sheep
		  ,@Pigs=T1.Pigs
		  ,@Camels=T1.Camels
		  ,@Donkeys=T1.Donkeys
		  ,@Poultry=T1.Poultry
		  ,@Rabbits=T1.Rabbits
		  ,@LivelihoodSourceId=T1.LivelihoodSourceId
		  ,@PermanentIncome=T1.PermanentIncome
		  ,@ContractIncome=ISNULL(T1.ContractIncome,0)
		  ,@CasualIncome=ISNULL(T1.CasualIncome,0)
		  ,@OtherIncome=ISNULL(T1.OtherIncome,0)
	FROM (
			SELECT U.R.value('(OwnHouse)[1]','bit') AS OwnHouse
				,U.R.value('(MonthlyRental)[1]','money') AS MonthlyRental
				,U.R.value('(HouseRooms)[1]','tinyint') AS HouseRooms
				,U.R.value('(FarmingLandSize)[1]','tinyint') AS FarmingLandSize
				,U.R.value('(GrazingLandSize)[1]','tinyint') AS GrazingLandSize
				,U.R.value('(ZebuCattle)[1]','tinyint') AS ZebuCattle
				,U.R.value('(HybridCattle)[1]','tinyint') AS HybridCattle
				,U.R.value('(Goats)[1]','tinyint') AS Goats
				,U.R.value('(Sheep)[1]','tinyint') AS Sheep
				,U.R.value('(Pigs)[1]','tinyint') AS Pigs
				,U.R.value('(Camels)[1]','tinyint') AS Camels
				,U.R.value('(Donkeys)[1]','tinyint') AS Donkeys
				,U.R.value('(Poultry)[1]','tinyint') AS Poultry
				,U.R.value('(Rabbits)[1]','tinyint') AS Rabbits
				,U.R.value('(LivelihoodSourceId)[1]','int') AS LivelihoodSourceId
				,U.R.value('(PermanentIncome)[1]','money') AS PermanentIncome
				,U.R.value('(ContractIncome)[1]','money') AS ContractIncome
				,U.R.value('(CasualIncome)[1]','money') AS CasualIncome
				,U.R.value('(OtherIncome)[1]','money') AS OtherIncome
			FROM @HHAssetsXML.nodes('HHAssets/Record') AS U(R)
		) T1

	SELECT @BusinessDescription=T1.BusinessDescription
		  ,@TotalEmployees=T1.TotalEmployees
		  ,@BusinessValue=T1.BusinessValue
		  ,@PremisesMonthlyRental=T1.PremisesMonthlyRental
		  ,@StatutoryFees=T1.StatutoryFees
		  ,@AverageMonthlyProfit=T1.AverageMonthlyProfit
		  ,@SavingsStorageTypeId=T1.SavingsStorageTypeId
		  ,@SavingsAmount=T1.SavingsAmount
		  ,@OtherProgId=T1.OtherProgId
		  ,@OtherProgBenefitFreqId=T1.OtherProgBenefitFreqId
		  ,@OtherProgMonthlyBenefitAmount=T1.OtherProgMonthlyBenefitAmount
	FROM (
			SELECT U.R.value('(BusinessDescription)[1]','varchar(50)') AS BusinessDescription
				,U.R.value('(TotalEmployees)[1]','tinyint') AS TotalEmployees
				,U.R.value('(BusinessValue)[1]','money') AS BusinessValue
				,U.R.value('(PremisesMonthlyRental)[1]','money') AS PremisesMonthlyRental
				,U.R.value('(StatutoryFees)[1]','money') AS StatutoryFees
				,U.R.value('(AverageMonthlyProfit)[1]','money') AS AverageMonthlyProfit
				,U.R.value('(SavingsStorageTypeId)[1]','int') AS SavingsStorageTypeId
				,U.R.value('(SavingsAmount)[1]','money') AS SavingsAmount
				,U.R.value('(OtherProgId)[1]','int') AS OtherProgId
				,U.R.value('(OtherProgBenefitFreqId)[1]','int') AS OtherProgBenefitFreqId
				,U.R.value('(OtherProgMonthlyBenefitAmount)[1]','int') AS OtherProgMonthlyBenefitAmount
			FROM @HHBusinessXML.nodes('HHBusiness/Record') AS U(R)
		) T1

	SET @SerialNo=CASE WHEN(@SerialNo='') THEN NULL ELSE @SerialNo END
	SELECT @ProgrammeId=ProgrammeId FROM Programme WHERE ProgrammeCode=@ProgrammeId
	SET @Longitude=CASE WHEN(ISNULL(@Longitude,0)=0) THEN NULL ELSE @Longitude END
	SET @Latitude=CASE WHEN(ISNULL(@Latitude,0)=0) THEN NULL ELSE @Latitude END
	SET @Elevation=CASE WHEN(ISNULL(@Elevation,0)=0) THEN NULL ELSE @Elevation END

	SET @OtherProgId=CASE WHEN(ISNULL(@OtherProgId,0)=0) THEN NULL ELSE @OtherProgId END
	SET @OtherProgBenefitFreqId=CASE WHEN(ISNULL(@OtherProgBenefitFreqId,0)=0) THEN NULL ELSE @OtherProgBenefitFreqId END

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@TargetGroupCode AND T1.SystemCodeDetailId=@TargetGroupId)
		SET @ErrorMsg='Please specify valid TargetGroupId parameter'
	ELSE IF ISNULL(@SerialNo,'')<>'' AND EXISTS(SELECT 1 FROM Household WHERE SerialNo=@SerialNo AND ProgrammeId=@ProgrammeId AND HHId<>@HHId)
		SET @ErrorMsg='The Serial No. is already associated with another household'
	ELSE IF NOT EXISTS(SELECT 1 FROM SubLocation WHERE SubLocationId=@SubLocationId)
		SET @ErrorMsg='Please specify valid SubLocationId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@WallMaterialCode AND T1.SystemCodeDetailId=@WallMaterialId)
		SET @ErrorMsg='Please specify valid WallMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@FloorMaterialCode AND T1.SystemCodeDetailId=@FloorMaterialId)
		SET @ErrorMsg='Please specify valid FloorMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@RoofMaterialCode AND T1.SystemCodeDetailId=@RoofMaterialId)
		SET @ErrorMsg='Please specify valid RoofMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@ToiletTypeCode AND T1.SystemCodeDetailId=@ToiletTypeId)
		SET @ErrorMsg='Please specify valid ToiletTypeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@WaterSourceCode AND T1.SystemCodeDetailId=@WaterSourceId)
		SET @ErrorMsg='Please specify valid WaterSourceId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@LightingSourceCode AND T1.SystemCodeDetailId=@LightingSourceId)
		SET @ErrorMsg='Please specify valid LightingSourceId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@CookingFuelCode AND T1.SystemCodeDetailId=@CookingFuelId)
		SET @ErrorMsg='Please specify valid CookingFuelId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@LivelihoodSourceCode AND T1.SystemCodeDetailId=@LivelihoodSourceId)
		SET @ErrorMsg='Please specify valid LivelihoodSourceId parameter'
	ELSE IF @OtherProgId>0 AND NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@OtherProgCode AND T1.SystemCodeDetailId=@OtherProgId)
		SET @ErrorMsg='Please specify valid OtherProgId parameter'
	ELSE IF @OtherProgBenefitFreqId>0 AND NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@FrequencyCode AND T1.SystemCodeDetailId=@OtherProgBenefitFreqId)
		SET @ErrorMsg='Please specify valid OtherProgBenefitFreqId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch WHERE PSPBranchId=@PSPBranchId)
		SET @ErrorMsg='Please specify valid PSPBranchId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE UserId=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	INSERT INTO @tblMembers(MemberId,FirstName,MiddleName,Surname,MemberRoleId,RelationshipId,ExternalMember,BirthCertNo,NationalIdNo,DateOfBirth,SexId,EducationLevelId,IlliteracyReasonId,DisabilityTypeId,NCPwDNo,IllnessTypeId,OrphanhoodTypeId,MaritalStatusId,Phone1,Phone2,IsNew,IsForDeletion)
	SELECT T1.MemberId,T1.FirstName,T1.MiddleName,T1.Surname,T2.SystemCodeDetailId AS MemberRoleId,T4.SystemCodeDetailId AS RelationshipId,T1.ExternalMember,T1.BirthCertNo,T1.NationalIdNo,T1.DateOfBirth,T6.SystemCodeDetailId AS SexId,ISNULL(T8.SystemCodeDetailId,0) AS EducationLevelId,ISNULL(T18.SystemCodeDetailId,0) AS IlliteracyReasonId,ISNULL(T10.SystemCodeDetailId,0) AS DisabilityTypeId,NCPwDNo,ISNULL(T12.SystemCodeDetailId,0) AS IllnessTypeId,ISNULL(T14.SystemCodeDetailId,0) AS OrphanhoodTypeId,ISNULL(T16.SystemCodeDetailId,0) AS MaritalStatusId,Phone1,Phone2,IsNew,IsForDeletion
	FROM (
			SELECT U.R.value('(MemberId)[1]','int') AS MemberId
				,U.R.value('(FirstName)[1]','varchar(50)') AS FirstName
				,U.R.value('(MiddleName)[1]','varchar(50)') AS MiddleName
				,U.R.value('(Surname)[1]','varchar(50)') AS Surname
				,U.R.value('(MemberRoleId)[1]','varchar(50)') AS MemberRoleCode
				,U.R.value('(RelationshipId)[1]','int') AS RelationshipId
				,U.R.value('(ExternalMember)[1]','bit') AS ExternalMember
				,U.R.value('(BirthCertNo)[1]','varchar(30)') AS BirthCertNo
				,U.R.value('(NationalIDNo)[1]','varchar(30)') AS NationalIdNo
				,U.R.value('(DateOfBirth)[1]','datetime') AS DateOfBirth
				,U.R.value('(SexId)[1]','int') AS SexId
				,U.R.value('(EducationLevelId)[1]','int') AS EducationLevelId
				,U.R.value('(IlliteracyReasonId)[1]','int') AS IlliteracyReasonId
				,U.R.value('(DisabilityId)[1]','int') AS DisabilityTypeId
				,U.R.value('(NCPwDNo)[1]','varchar(30)') AS NCPwDNo
				,U.R.value('(IllnessId)[1]','int') AS IllnessTypeId
				,U.R.value('(OrphanhoodId)[1]','int') AS OrphanhoodTypeId
				,U.R.value('(MaritalStatusId)[1]','int') AS MaritalStatusId
				,U.R.value('(PhoneNo1)[1]','varchar(20)') AS Phone1
				,U.R.value('(PhoneNo2)[1]','varchar(20)') AS Phone2
				,U.R.value('(IsNew)[1]','bit') AS IsNew
				,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
			FROM @HHMembersXML.nodes('HHMembers/Record') AS U(R)
		) T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleCode=T2.DetailCode
			 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@MemberRoleCode
			 INNER JOIN SystemCodeDetail T4 ON T1.RelationshipId=T4.SystemCodeDetailId
			 INNER JOIN SystemCode T5 ON T4.SystemCodeId=T5.SystemCodeId AND T5.Code=@RelationshipCode
			 INNER JOIN SystemCodeDetail T6 ON T1.SexId=T6.SystemCodeDetailId
			 INNER JOIN SystemCode T7 ON T6.SystemCodeId=T7.SystemCodeId AND T7.Code=@SexCode
			 LEFT JOIN SystemCodeDetail T8 ON T1.EducationLevelId=T8.SystemCodeDetailId
			 LEFT JOIN SystemCode T9 ON T8.SystemCodeId=T9.SystemCodeId AND T9.Code=@EducationLevelCode
			 LEFT JOIN SystemCodeDetail T10 ON T1.DisabilityTypeId=T10.SystemCodeDetailId
			 LEFT JOIN SystemCode T11 ON T10.SystemCodeId=T11.SystemCodeId AND T11.Code=@DisabilityCode
			 LEFT JOIN SystemCodeDetail T12 ON T1.IllnessTypeId=T12.SystemCodeDetailId
			 LEFT JOIN SystemCode T13 ON T12.SystemCodeId=T13.SystemCodeId AND T13.Code=@IllnessCode
			 LEFT JOIN SystemCodeDetail T14 ON T1.OrphanhoodTypeId=T14.SystemCodeDetailId
			 LEFT JOIN SystemCode T15 ON T14.SystemCodeId=T15.SystemCodeId AND T15.Code=@OrphanhoodCode
			 INNER JOIN SystemCodeDetail T16 ON T1.MaritalStatusId=T16.SystemCodeDetailId
			 INNER JOIN SystemCode T17 ON T16.SystemCodeId=T17.SystemCodeId AND T17.Code=@MaritalStatusCode
			 LEFT JOIN SystemCodeDetail T18 ON T1.IlliteracyReasonId=T18.SystemCodeDetailId
			 LEFT JOIN SystemCode T19 ON T18.SystemCodeId=T19.SystemCodeId AND T19.Code=@IlliteracyReasonCode

	INSERT INTO @tblHHItems(HHItemTypeId,HHItemCount,IsNew,IsForDeletion)
	SELECT U.R.value('(ItemId)[1]','int') AS HHItemTypeId
		,U.R.value('(NoOfItems)[1]','tinyint') AS HHItemCount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHItemsXML.nodes('HHItems/Record') AS U(R)

	INSERT INTO @tblHHIncomes(HHIncomeTypeId,HHIncomeAmount,IsNew,IsForDeletion)
	SELECT U.R.value('(IncomeId)[1]','int') AS HHIncomeTypeId
		,U.R.value('(Amount)[1]','money') AS HHIncomeTypeAmount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHIncomesXML.nodes('HHIncomes/Record') AS U(R)

	INSERT INTO @tblHHExpenditures(HHExpenditureTypeId,HHExpenditureAmount,IsNew,IsForDeletion)
	SELECT U.R.value('(ExpenditureId)[1]','int') AS HHExpenditureTypeId
		,U.R.value('(Amount)[1]','money') AS HHExpenditureAmount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHExpendituresXML.nodes('HHExpenditures/Record') AS U(R)

	INSERT INTO @tblHHNutrition(HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport,IsNew,IsForDeletion)
	SELECT U.R.value('(FoodId)[1]','int') AS HHNutritionTypeId
		,U.R.value('(EatenRaw)[1]','bit') AS EatenRaw
		,U.R.value('(PackSize)[1]','varchar(30)') AS PkgUnitSize
		,U.R.value('(PackCost)[1]','money') AS PkgUnitCost
		,U.R.value('(WeeklyFreq)[1]','tinyint') AS WeeklyConsumptionFreq
		,U.R.value('(FoodSource)[1]','varchar(30)') AS FoodSource
		,U.R.value('(ChoiceInfluence)[1]','varchar(30)') AS ChoiceInfluence
		,U.R.value('(ModeOfTransport)[1]','varchar(30)') AS ModeOfTransport
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHNutritionsXML.nodes('HHNutritions/Record') AS U(R)

	IF @HHId>0
	BEGIN
		-----------------------------------------1. HOUSEHOLD, SOCIAL ASSISTANCE DATA, SUMMARY NUTRITION, OBSERVATIONS & PSP MAPPING DATA-----------------------------------------

		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.TargetGroupId=@TargetGroupId
		   ,T1.CaptureDate=@CaptureDate
		   ,T1.SerialNo=@SerialNo
		   ,T1.SubLocationId=@SubLocationId
		   ,T1.Village=@Village
		   ,T1.Ward=@Ward
		   ,T1.LocalityId=@LocalityId
		   ,T1.PhysicalAddress=@PhysicalAddress
		   ,T1.NearestLandmark=@NearestLandmark
		   ,T1.Longitude=@Longitude
		   ,T1.Latitude=@Latitude
		   ,T1.Elevation=@Elevation
		   ,T1.OtherProgId=@OtherProgId
		   ,T1.OtherProgBenefitFreqId=@OtherProgBenefitFreqId
		   ,T1.OtherProgMonthlyBenefitAmount=@OtherProgMonthlyBenefitAmount
		   ,T1.AverageDailyMealsCount=@AverageDailyMealsCount
		   ,T1.AverageWeeklyNoMealsCount=@AverageWeeklyNoMealsCount
		   ,T1.AverageWeeklyNoMealsReason=@AverageWeeklyNoMealsReason
		   ,T1.PSPBranchId=@PSPBranchId
		   ,T1.LoCNames=@LoCNames
		   ,T1.LoCNationalIDNo=@LoCNationalIdNo
		   ,T1.LoCPhone=@LoCPhone
		   ,T1.TargetingObservations=@TargetingObservations
		   ,T1.HHStatusId=ISNULL(@HHStatusId,T1.HHStatusId)
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Household T1
		WHERE T1.HHId=@HHId

		-----------------------------------------2. HOUSEHONLD MEMBERS DATA-----------------------------------------

		INSERT INTO HouseholdMembers(HHId,MemberRoleId,FirstName,MiddleName,Surname,SexId,RelationshipId,DoB,BirthCertNo,OrphanhoodTypeId,NationalIDNo,EducationLevelId,IlliteracyReasonId,MaritalStatusId,IllnessTypeId,DisabilityTypeId,NCPwDNo,Phone1,Phone2,ExternalMember)
		SELECT @HHId,T1.MemberRoleId,T1.FirstName,T1.MiddleName,T1.Surname,T1.SexId,T1.RelationshipId,T1.DateOfBirth,T1.BirthCertNo,T1.OrphanhoodTypeId,T1.NationalIdNo,T1.EducationLevelId,T1.IlliteracyReasonId,T1.MaritalStatusId,T1.IllnessTypeId,T1.DisabilityTypeId,T1.NCPwDNo,T1.Phone1,T1.Phone2,T1.ExternalMember
		FROM @tblMembers T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.MemberRoleId=T2.MemberRoleId
			,T1.FirstName=T2.FirstName
			,T1.MiddleName=T2.MiddleName
			,T1.Surname=T2.Surname
			,T1.SexId=T2.SexId
			,T1.RelationshipId=T2.RelationshipId
			,T1.DoB=T2.DateOfBirth
			,T1.OrphanhoodTypeId=T2.OrphanhoodTypeId
			,T1.NationalIDNo=T2.NationalIdNo
			,T1.EducationLevelId=T2.EducationLevelId
			,T1.IlliteracyReasonId=T2.IlliteracyReasonId
			,T1.MaritalStatusId=T2.MaritalStatusId
			,T1.IllnessTypeId=T2.IllnessTypeId
			,T1.DisabilityTypeId=T2.DisabilityTypeId
			,T1.NCPwDNo=T2.NCPwDNo
			,T1.Phone1=T2.Phone1
			,T1.Phone2=T2.Phone2
			,T1.ExternalMember=T2.ExternalMember
		FROM HouseholdMembers T1 INNER JOIN @tblMembers T2 ON T1.HHId=@HHId AND T1.MemberId=T2.MemberId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdMembers T1 INNER JOIN @tblMembers T2 ON T1.HHId=@HHId AND T1.MemberId=T2.MemberId
		WHERE T2.IsForDeletion=1
		
		-----------------------------------------3. HOUSEHOLD DWELLING DATA-----------------------------------------

		INSERT INTO HouseholdItems(HHId,HHItemTypeId,HHItemCount)
		SELECT @HHId,T1.HHItemTypeId,T1.HHItemCount
		FROM @tblHHItems T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.HHItemCount=T2.HHItemCount
		FROM HouseholdItems T1 INNER JOIN @tblHHItems T2 ON T1.HHId=@HHId AND T1.HHItemTypeId=T2.HHItemTypeId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdItems T1 INNER JOIN @tblHHItems T2 ON T1.HHId=@HHId AND T1.HHItemTypeId=T2.HHItemTypeId
		WHERE T2.IsForDeletion=1

		UPDATE T1
		SET T1.WallMaterialId=@WallMaterialId
		   ,T1.FloorMaterialId=@FloorMaterialId
		   ,T1.RoofMaterialId=@RoofMaterialId
		   ,T1.ToiletTypeId=@ToiletTypeId
		   ,T1.WaterSourceId=@WallMaterialId
		   ,T1.LightingSourceId=@LightingSourceId
		   ,T1.CookingFuelSourceId=@CookingFuelId
		FROM HouseholdCharacteristics T1
		WHERE T1.HHId=@HHId

		-----------------------------------------4. HOUSEHOLD ASSETS-----------------------------------------
		
		UPDATE T1
		SET T1.OwnHouse=@OwnHouse
		   ,T1.MonthlyRental=@MonthlyRental
		   ,T1.HouseRooms=@HouseRooms
		   ,T1.OwnFarmingLandSize=@FarmingLandSize
		   ,T1.OwnGrazingLandSize=@GrazingLandSize
		   ,T1.ZebuCattle=@ZebuCattle
		   ,T1.HybridCattle=@HybridCattle
		   ,T1.Goats=@Goats
		   ,T1.Sheep=@Sheep
		   ,T1.Pigs=@Pigs
		   ,T1.Camels=@Camels
		   ,T1.Donkeys=@Donkeys
		   ,T1.Poultry=@Poultry
		   ,T1.Rabbits=@Rabbits
		   ,T1.LivelihoodSourceId=@LivelihoodSourceId
		   ,T1.PermanentIncome=@PermanentIncome
		   ,T1.ContractIncome=@ContractIncome
		   ,T1.OtherIncome=@OtherIncome
		FROM HouseholdAssets T1
		WHERE T1.HHId=@HHId

		-----------------------------------------5. HOUSEHOLD BUSINESS & SOCIAL ASSISTANCE-----------------------------------------

		UPDATE T1
		SET T1.BusinessType=@BusinessDescription
		   ,T1.TotalEmployeesInclusive=@TotalEmployees
		   ,T1.EstimatedBusinessValue=@BusinessValue
		   ,T1.PremisesMonthlyRental=@PremisesMonthlyRental
		   ,T1.TotalStatutoryLevies=@StatutoryFees
		   ,T1.AverageMonthlyProfit=@AverageMonthlyProfit
		   ,T1.SavingsStorageTypeId=@SavingsStorageTypeId
		   ,T1.SavingsAmount=@SavingsAmount
		FROM HouseholdBusiness T1
		WHERE T1.HHId=@HHId

		-----------------------------------------6. HOUSEHOLD MONTHLY INCOME vs EXPENDITURE-----------------------------------------

		INSERT INTO HouseholdIncome(HHId,HHIncomeTypeId,HHIncomeAmount)
		SELECT @HHId,T1.HHIncomeTypeId,T1.HHIncomeAmount
		FROM @tblHHIncomes T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.HHIncomeAmount=T2.HHIncomeAmount
		FROM HouseholdIncome T1 INNER JOIN @tblHHIncomes T2 ON T1.HHId=@HHId AND T1.HHIncomeTypeId=T2.HHIncomeTypeId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdIncome T1 INNER JOIN @tblHHIncomes T2 ON T1.HHId=@HHId AND T1.HHIncomeTypeId=T2.HHIncomeTypeId
		WHERE T2.IsForDeletion=1

		INSERT INTO HouseholdExpenditure(HHId,HHExpenditureTypeId,HHExpenditureAmount)
		SELECT @HHId,T1.HHExpenditureTypeId,T1.HHExpenditureAmount
		FROM @tblHHExpenditures T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.HHExpenditureAmount=T2.HHExpenditureAmount
		FROM HouseholdExpenditure T1 INNER JOIN @tblHHExpenditures T2 ON T1.HHId=@HHId AND T1.HHExpenditureTypeId=T2.HHExpenditureTypeId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdExpenditure T1 INNER JOIN @tblHHExpenditures T2 ON T1.HHId=@HHId AND T1.HHExpenditureTypeId=T2.HHExpenditureTypeId
		WHERE T2.IsForDeletion=1

		-----------------------------------------7. HOUSEHOLD NUTRITION-----------------------------------------

		INSERT INTO HouseholdNutrition(HHId,HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport)
		SELECT @HHId,T1.HHNutritionTypeId,T1.EatenRaw,T1.PkgUnitSize,T1.PkgUnitCost,T1.WeeklyConsumptionFreq,T1.FoodSource,T1.ChoiceInfluence,T1.ModeOfTransport
		FROM @tblHHNutrition T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.EatenRaw=T2.EatenRaw
		   ,T1.PkgUnitSize=T2.PkgUnitSize
		   ,T1.PkgUnitCost=T2.PkgUnitCost
		   ,T1.WeeklyConsumptionFreq=T2.WeeklyConsumptionFreq
		   ,T1.FoodSource=T2.FoodSource
		   ,T1.ChoiceInfluence=T2.ChoiceInfluence
		   ,T1.ModeOfTransport=T2.ModeOfTransport
		FROM HouseholdNutrition T1 INNER JOIN @tblHHNutrition T2 ON T1.HHId=@HHId AND T1.HHNutritionTypeId=T2.HHNutritionTypeId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdNutrition T1 INNER JOIN @tblHHNutrition T2 ON T1.HHId=@HHId AND T1.HHNutritionTypeId=T2.HHNutritionTypeId
		WHERE T2.IsForDeletion=1

	END
	ELSE
	BEGIN
		-----------------------------------------1. HOUSEHOLD, SOCIAL ASSISTANCE DATA, SUMMARY NUTRITION, OBSERVATIONS & PSP MAPPING DATA-----------------------------------------

		INSERT INTO Household(ProgrammeId,TargetGroupId,CaptureDate,SerialNo,SubLocationId,Village,Ward,LocalityId,PhysicalAddress,NearestLandmark,Longitude,Latitude,Elevation,OtherProgId,OtherProgBenefitFreqId,OtherProgMonthlyBenefitAmount,AverageDailyMealsCount,AverageWeeklyNoMealsCount,AverageWeeklyNoMealsReason,PSPBranchId,LOCNames,LOCNationalIDNo,LOCPhone,TargetingObservations,HHStatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@TargetGroupId,@CaptureDate,@SerialNo,@SubLocationId,@Village,@Ward,@LocalityId,@PhysicalAddress,@NearestLandmark,@Longitude,@Latitude,@Elevation,@OtherProgId,@OtherProgBenefitFreqId,@OtherProgMonthlyBenefitAmount,@AverageDailyMealsCount,@AverageWeeklyNoMealsCount,@AverageWeeklyNoMealsReason,@PSPBranchId,@LoCNames,@LoCNationalIdNo,@LoCPhone,@TargetingObservations,ISNULL(@HHStatusId,@HHStatus_RegistrationId),@UserId,GETDATE()
	
		SET @HHId=IDENT_CURRENT('Household');
		-----------------------------------------2. HOUSEHONLD MEMBERS DATA-----------------------------------------

		INSERT INTO HouseholdMembers(HHId,MemberRoleId,FirstName,MiddleName,Surname,SexId,RelationshipId,DoB,BirthCertNo,OrphanhoodTypeId,NationalIDNo,EducationLevelId,IlliteracyReasonId,MaritalStatusId,IllnessTypeId,DisabilityTypeId,NCPwDNo,Phone1,Phone2,ExternalMember)
		SELECT @HHId,T1.MemberRoleId,T1.FirstName,T1.MiddleName,T1.Surname,T1.SexId,T1.RelationshipId,T1.DateOfBirth,T1.BirthCertNo,T1.OrphanhoodTypeId,T1.NationalIdNo,T1.EducationLevelId,T1.IlliteracyReasonId,T1.MaritalStatusId,T1.IllnessTypeId,T1.DisabilityTypeId,T1.NCPwDNo,T1.Phone1,T1.Phone2,T1.ExternalMember
		FROM @tblMembers T1
		WHERE T1.IsNew=1

		-----------------------------------------3. HOUSEHOLD DWELLING DATA-----------------------------------------

		INSERT INTO HouseholdItems(HHId,HHItemTypeId,HHItemCount)
		SELECT @HHId,T1.HHItemTypeId,T1.HHItemCount
		FROM @tblHHItems T1
		WHERE T1.IsNew=1

		INSERT INTO HouseholdCharacteristics(HHId,WallMaterialId,FloorMaterialId,RoofMaterialId,ToiletTypeId,WaterSourceId,LightingSourceId,CookingFuelSourceId)
		SELECT @HHId,@WallMaterialId,@FloorMaterialId,@RoofMaterialId,@ToiletTypeId,@WaterSourceId,@LightingSourceId,@CookingFuelId

		-----------------------------------------4. HOUSEHOLD ASSETS-----------------------------------------

		INSERT INTO HouseholdAssets(HHId,OwnHouse,MonthlyRental,HouseRooms,OwnFarmingLandSize,OwnGrazingLandSize,ZebuCattle,HybridCattle,Goats,Sheep,Pigs,Camels,Donkeys,Poultry,Rabbits,LivelihoodSourceId,PermanentIncome,ContractIncome,OtherIncome)
		SELECT @HHId,@OwnHouse,@MonthlyRental,@HouseRooms,@FarmingLandSize,@GrazingLandSize,@ZebuCattle,@HybridCattle,@Goats,@Sheep,@Pigs,@Camels,@Donkeys,@Poultry,@Rabbits,@LivelihoodSourceId,@PermanentIncome,@ContractIncome,@OtherIncome

		-----------------------------------------5. HOUSEHOLD BUSINESS-----------------------------------------

		INSERT INTO HouseholdBusiness(HHId,BusinessType,TotalEmployeesInclusive,EstimatedBusinessValue,PremisesMonthlyRental,TotalStatutoryLevies,AverageMonthlyProfit,SavingsStorageTypeId,SavingsAmount)
		SELECT @HHId,@BusinessDescription,@TotalEmployees,@BusinessValue,@PremisesMonthlyRental,@StatutoryFees,@AverageMonthlyProfit,@SavingsStorageTypeId,@SavingsAmount

		-----------------------------------------6. HOUSEHOLD MONTHLY INCOME vs EXPENDITURE-----------------------------------------

		INSERT INTO HouseholdIncome(HHId,HHIncomeTypeId,HHIncomeAmount)
		SELECT @HHId,T1.HHIncomeTypeId,T1.HHIncomeAmount
		FROM @tblHHIncomes T1
		WHERE T1.IsNew=1

		INSERT INTO HouseholdExpenditure(HHId,HHExpenditureTypeId,HHExpenditureAmount)
		SELECT @HHId,T1.HHExpenditureTypeId,T1.HHExpenditureAmount
		FROM @tblHHExpenditures T1
		WHERE T1.IsNew=1

		-----------------------------------------7. HOUSEHOLD NUTRITION-----------------------------------------

		INSERT INTO HouseholdNutrition(HHId,HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport)
		SELECT @HHId,T1.HHNutritionTypeId,T1.EatenRaw,T1.PkgUnitSize,T1.PkgUnitCost,T1.WeeklyConsumptionFreq,T1.FoodSource,T1.ChoiceInfluence,T1.ModeOfTransport
		FROM @tblHHNutrition T1
		WHERE T1.IsNew=1

	END

	SELECT 1 AS NoOfRows,@HHId AS HHId

	EXEC GetTargetings

END
GO



IF NOT OBJECT_ID('UpdateHousehold') IS NULL	DROP PROC UpdateHousehold
GO
CREATE PROC UpdateHousehold
	@HHId int
   ,@ChangeTypeId varchar(20)
   ,@ChangeJustificationId varchar(20)
   ,@ProgrammeId int
   ,@TargetGroupId int
   ,@CaptureDate datetime
   ,@SerialNo int
   ,@SubLocationId int
   ,@Village varchar(50)
   ,@Ward varchar(50)
   ,@LocalityId int
   ,@PhysicalAddress varchar(50)
   ,@NearestLandmark varchar(50)
   ,@Longitude float
   ,@Latitude float
   ,@Elevation float

   ,@HHMembersXML XML
   
   ,@HHItemsXML XML
   ,@HHCharacteristicsXML XML

   ,@HHAssetsXML XML

   ,@HHBusinessXML XML

   ,@HHIncomesXML XML
   ,@HHExpendituresXML XML

   ,@HHNutritionsXML XML
   ,@AverageDailyMealsCount tinyint
   ,@AverageWeeklyNoMealsCount tinyint
   ,@AverageWeeklyNoMealsReason varchar(50)

   ,@PSPBranchId int
   ,@LoCNames varchar(50)
   ,@LoCNationalIdNo varchar(30)
   ,@LoCPhone varchar(30)
   ,@TargetingObservations varchar(128)

   ,@UserId int
AS
BEGIN
	DECLARE @tblMembers TABLE(
		MemberId int
	   ,FirstName varchar(50)
	   ,MiddleName varchar(50)
	   ,Surname varchar(50)
	   ,MemberRoleId int
	   ,RelationshipId int
	   ,ExternalMember bit
	   ,BirthCertNo varchar(30)
	   ,NationalIdNo varchar(30)
	   ,DateOfBirth datetime
	   ,SexId int
	   ,EducationLevelId int
	   ,IlliteracyReasonId int
	   ,DisabilityTypeId int
	   ,NCPwDNo varchar(30)
	   ,IllnessTypeId int
	   ,OrphanhoodTypeId int
	   ,MaritalStatusId int
	   ,Phone1 varchar(20)
	   ,Phone2 varchar(20)
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHItems TABLE(
		HHItemTypeId int
	   ,HHItemCount tinyint
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHIncomes TABLE(
		HHIncomeTypeId int
	   ,HHIncomeAmount money
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHExpenditures TABLE(
		HHExpenditureTypeId int
	   ,HHExpenditureAmount money
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @tblHHNutrition TABLE(
		HHNutritionTypeId int
	   ,EatenRaw bit
	   ,PkgUnitSize varchar(30)
	   ,PkgUnitCost money
	   ,WeeklyConsumptionFreq tinyint
	   ,FoodSource varchar(30)
	   ,ChoiceInfluence varchar(30)
	   ,ModeOfTransport varchar(30)
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @ErrorMsg varchar(128)
   
    DECLARE @WallMaterialId int
		   ,@FloorMaterialId int
		   ,@RoofMaterialId int
		   ,@ToiletTypeId int
		   ,@WaterSourceId int
		   ,@LightingSourceId int
		   ,@CookingFuelId int

	DECLARE @OwnHouse bit
		   ,@MonthlyRental money
		   ,@HouseRooms tinyint
		   ,@FarmingLandSize tinyint
		   ,@GrazingLandSize tinyint
		   ,@ZebuCattle tinyint
		   ,@HybridCattle tinyint
		   ,@Goats tinyint
		   ,@Sheep tinyint
		   ,@Pigs tinyint
		   ,@Camels tinyint
		   ,@Donkeys tinyint
		   ,@Poultry tinyint
		   ,@Rabbits tinyint
		   ,@LivelihoodSourceId int
		   ,@PermanentIncome money
		   ,@ContractIncome money
		   ,@CasualIncome money
		   ,@OtherIncome money

	DECLARE @BusinessDescription varchar(50)
		   ,@TotalEmployees tinyint
		   ,@BusinessValue money
		   ,@PremisesMonthlyRental money
		   ,@StatutoryFees money
		   ,@AverageMonthlyProfit money
		   ,@SavingsStorageTypeId int
		   ,@SavingsAmount money
		   ,@OtherProgId tinyint
		   ,@OtherProgBenefitFreqId int
		   ,@OtherProgMonthlyBenefitAmount money

	DECLARE @HHStatusId int
	DECLARE @HHStatusCode varchar(20)
	DECLARE @HHStatus_RegistrationCode varchar(20)
	DECLARE @HHStatus_EnrolledCode varchar(20)
	DECLARE @HHStatus_ExitedCode varchar(20)
	DECLARE @HHStatus_RegistrationId int
	DECLARE @TargetGroupCode varchar(20)
	DECLARE @WallMaterialCode varchar(20)
	DECLARE @FloorMaterialCode varchar(20)
	DECLARE @RoofMaterialCode varchar(20)
	DECLARE @ToiletTypeCode varchar(20)
	DECLARE @WaterSourceCode varchar(20)
	DECLARE @LightingSourceCode varchar(20)
	DECLARE @CookingFuelCode varchar(20)
	DECLARE @LivelihoodSourceCode varchar(20)
	DECLARE @OtherProgCode varchar(20)
	DECLARE @FrequencyCode varchar(20)

	DECLARE @MemberRoleCode varchar(20)
	DECLARE @RelationshipCode varchar(20)
	DECLARE @SexCode varchar(20)
	DECLARE @EducationLevelCode varchar(20)
	DECLARE @IlliteracyReasonCode varchar(20)
	DECLARE @DisabilityCode varchar(20)
	DECLARE @IllnessCode varchar(20)
	DECLARE @OrphanhoodCode varchar(20)
	DECLARE @MaritalStatusCode varchar(20)

	DECLARE @ChangeType_DissolutionCode varchar(20)
	DECLARE @ChangeType_ExclusionCode varchar(20)
	DECLARE @ChangeType_GraduationCode varchar(20)
	DECLARE @ChangeType_ReversalCode varchar(20)

	SET @HHStatusCode='HHStatus'
	SET @HHStatus_RegistrationCode='0'
	SET @HHStatus_EnrolledCode='1'
	SET @HHStatus_ExitedCode='-1'
	SET @TargetGroupCode='Target Group'
	SET @WallMaterialCode='Wall Material'
	SET @FloorMaterialCode='Floor Material'
	SET @RoofMaterialCode='Roof Material'
	SET @ToiletTypeCode='Toilet Type'
	SET @WaterSourceCode='Water Source'
	SET @LightingSourceCode='Lighting Source'
	SET @CookingFuelCode='Cooking Fuel'
	SET @LivelihoodSourceCode='Livelihood'
	SET @OtherProgCode = 'Other Programme'
	SET @FrequencyCode = 'Frequency'

	SET @MemberRoleCode='Member Role'
	SET @RelationshipCode='Relationship'
	SET @SexCode='Sex'
	SET @EducationLevelCode='Education Level'
	SET @IlliteracyReasonCode='Illiteracy Reason'
	SET @DisabilityCode='Disability'
	SET @IllnessCode='Illness'
	SET @OrphanhoodCode='Orphanhood'
	SET @MaritalStatusCode='Marital Status'

	SET @ChangeType_DissolutionCode='Dissolution'
	SET @ChangeType_ExclusionCode='Exlusion'
	SET @ChangeType_GraduationCode='Graduation'
	SET @ChangeType_ReversalCode='Reversal'

	SELECT @HHStatus_RegistrationId=T1.SystemCodeDetailId
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@HHStatusCode
	WHERE T1.DetailCode=@HHStatus_RegistrationCode

	
	SELECT @WallMaterialId=T1.WallMaterialId
		  ,@FloorMaterialId=T1.FloorMaterialId
		  ,@RoofMaterialId=T1.RoofMaterialId
		  ,@ToiletTypeId=T1.ToiletTypeId
		  ,@WaterSourceId=T1.WaterSourceId
		  ,@LightingSourceId=T1.LightingSourceId
		  ,@CookingFuelId=T1.CookingFuelId
	FROM (
			SELECT U.R.value('(WallMaterialId)[1]','int') AS WallMaterialId
				,U.R.value('(FloorMaterialId)[1]','int') AS FloorMaterialId
				,U.R.value('(RoofMaterialId)[1]','int') AS RoofMaterialId
				,U.R.value('(ToiletTypeId)[1]','int') AS ToiletTypeId
				,U.R.value('(WaterSourceId)[1]','int') AS WaterSourceId
				,U.R.value('(LightingSourceId)[1]','int') AS LightingSourceId
				,U.R.value('(CookingFuelId)[1]','int') AS CookingFuelId
			FROM @HHCharacteristicsXML.nodes('HHCharacteristics/Record') AS U(R)
		) T1
		
	SELECT @OwnHouse=T1.OwnHouse
		  ,@MonthlyRental=T1.MonthlyRental
		  ,@HouseRooms=T1.HouseRooms
		  ,@FarmingLandSize=T1.FarmingLandSize
		  ,@GrazingLandSize=T1.GrazingLandSize
		  ,@ZebuCattle=T1.ZebuCattle
		  ,@HybridCattle=T1.HybridCattle
		  ,@Goats=T1.Goats
		  ,@Sheep=T1.Sheep
		  ,@Pigs=T1.Pigs
		  ,@Camels=T1.Camels
		  ,@Donkeys=T1.Donkeys
		  ,@Poultry=T1.Poultry
		  ,@Rabbits=T1.Rabbits
		  ,@LivelihoodSourceId=T1.LivelihoodSourceId
		  ,@PermanentIncome=T1.PermanentIncome
		  ,@ContractIncome=ISNULL(T1.ContractIncome,0)
		  ,@CasualIncome=ISNULL(T1.CasualIncome,0)
		  ,@OtherIncome=ISNULL(T1.OtherIncome,0)
	FROM (
			SELECT U.R.value('(OwnHouse)[1]','bit') AS OwnHouse
				,U.R.value('(MonthlyRental)[1]','money') AS MonthlyRental
				,U.R.value('(HouseRooms)[1]','tinyint') AS HouseRooms
				,U.R.value('(FarmingLandSize)[1]','tinyint') AS FarmingLandSize
				,U.R.value('(GrazingLandSize)[1]','tinyint') AS GrazingLandSize
				,U.R.value('(ZebuCattle)[1]','tinyint') AS ZebuCattle
				,U.R.value('(HybridCattle)[1]','tinyint') AS HybridCattle
				,U.R.value('(Goats)[1]','tinyint') AS Goats
				,U.R.value('(Sheep)[1]','tinyint') AS Sheep
				,U.R.value('(Pigs)[1]','tinyint') AS Pigs
				,U.R.value('(Camels)[1]','tinyint') AS Camels
				,U.R.value('(Donkeys)[1]','tinyint') AS Donkeys
				,U.R.value('(Poultry)[1]','tinyint') AS Poultry
				,U.R.value('(Rabbits)[1]','tinyint') AS Rabbits
				,U.R.value('(LivelihoodSourceId)[1]','int') AS LivelihoodSourceId
				,U.R.value('(PermanentIncome)[1]','money') AS PermanentIncome
				,U.R.value('(ContractIncome)[1]','money') AS ContractIncome
				,U.R.value('(CasualIncome)[1]','money') AS CasualIncome
				,U.R.value('(OtherIncome)[1]','money') AS OtherIncome
			FROM @HHAssetsXML.nodes('HHAssets/Record') AS U(R)
		) T1

	SELECT @BusinessDescription=T1.BusinessDescription
		  ,@TotalEmployees=T1.TotalEmployees
		  ,@BusinessValue=T1.BusinessValue
		  ,@PremisesMonthlyRental=T1.PremisesMonthlyRental
		  ,@StatutoryFees=T1.StatutoryFees
		  ,@AverageMonthlyProfit=T1.AverageMonthlyProfit
		  ,@SavingsStorageTypeId=T1.SavingsStorageTypeId
		  ,@SavingsAmount=T1.SavingsAmount
		  ,@OtherProgId=T1.OtherProgId
		  ,@OtherProgBenefitFreqId=T1.OtherProgBenefitFreqId
		  ,@OtherProgMonthlyBenefitAmount=T1.OtherProgMonthlyBenefitAmount
	FROM (
			SELECT U.R.value('(BusinessDescription)[1]','varchar(50)') AS BusinessDescription
				,U.R.value('(TotalEmployees)[1]','tinyint') AS TotalEmployees
				,U.R.value('(BusinessValue)[1]','money') AS BusinessValue
				,U.R.value('(PremisesMonthlyRental)[1]','money') AS PremisesMonthlyRental
				,U.R.value('(StatutoryFees)[1]','money') AS StatutoryFees
				,U.R.value('(AverageMonthlyProfit)[1]','money') AS AverageMonthlyProfit
				,U.R.value('(SavingsStorageTypeId)[1]','int') AS SavingsStorageTypeId
				,U.R.value('(SavingsAmount)[1]','money') AS SavingsAmount
				,U.R.value('(OtherProgId)[1]','int') AS OtherProgId
				,U.R.value('(OtherProgBenefitFreqId)[1]','int') AS OtherProgBenefitFreqId
				,U.R.value('(OtherProgMonthlyBenefitAmount)[1]','int') AS OtherProgMonthlyBenefitAmount
			FROM @HHBusinessXML.nodes('HHBusiness/Record') AS U(R)
		) T1

	SET @SerialNo=CASE WHEN(@SerialNo='') THEN NULL ELSE @SerialNo END
	SELECT @ProgrammeId=ProgrammeId FROM Programme WHERE ProgrammeCode=@ProgrammeId
	SET @Longitude=CASE WHEN(ISNULL(@Longitude,0)=0) THEN NULL ELSE @Longitude END
	SET @Latitude=CASE WHEN(ISNULL(@Latitude,0)=0) THEN NULL ELSE @Latitude END
	SET @Elevation=CASE WHEN(ISNULL(@Elevation,0)=0) THEN NULL ELSE @Elevation END

	SET @OtherProgId=CASE WHEN(ISNULL(@OtherProgId,0)=0) THEN NULL ELSE @OtherProgId END
	SET @OtherProgBenefitFreqId=CASE WHEN(ISNULL(@OtherProgBenefitFreqId,0)=0) THEN NULL ELSE @OtherProgBenefitFreqId END

	SELECT @HHStatusId=T1.SystemCodeDetailId
	FROM SystemCodeDetail T1 
	WHERE T1.DetailCode=CASE(@ChangeTypeId) WHEN @ChangeType_DissolutionCode THEN @HHStatus_ExitedCode
											WHEN @ChangeType_ExclusionCode THEN  @HHStatus_ExitedCode
											WHEN @ChangeType_GraduationCode THEN  @HHStatus_ExitedCode
											WHEN @ChangeType_ReversalCode THEN  @HHStatus_EnrolledCode
											ELSE NULL
						END		

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@TargetGroupCode AND T1.SystemCodeDetailId=@TargetGroupId)
		SET @ErrorMsg='Please specify valid TargetGroupId parameter'
	ELSE IF ISNULL(@SerialNo,'')<>'' AND EXISTS(SELECT 1 FROM Household WHERE SerialNo=@SerialNo AND ProgrammeId=@ProgrammeId AND HHId<>@HHId)
		SET @ErrorMsg='The Serial No. is already associated with another household'
	ELSE IF NOT EXISTS(SELECT 1 FROM SubLocation WHERE SubLocationId=@SubLocationId)
		SET @ErrorMsg='Please specify valid SubLocationId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@WallMaterialCode AND T1.SystemCodeDetailId=@WallMaterialId)
		SET @ErrorMsg='Please specify valid WallMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@FloorMaterialCode AND T1.SystemCodeDetailId=@FloorMaterialId)
		SET @ErrorMsg='Please specify valid FloorMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@RoofMaterialCode AND T1.SystemCodeDetailId=@RoofMaterialId)
		SET @ErrorMsg='Please specify valid RoofMaterialId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@ToiletTypeCode AND T1.SystemCodeDetailId=@ToiletTypeId)
		SET @ErrorMsg='Please specify valid ToiletTypeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@WaterSourceCode AND T1.SystemCodeDetailId=@WaterSourceId)
		SET @ErrorMsg='Please specify valid WaterSourceId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@LightingSourceCode AND T1.SystemCodeDetailId=@LightingSourceId)
		SET @ErrorMsg='Please specify valid LightingSourceId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@CookingFuelCode AND T1.SystemCodeDetailId=@CookingFuelId)
		SET @ErrorMsg='Please specify valid CookingFuelId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@LivelihoodSourceCode AND T1.SystemCodeDetailId=@LivelihoodSourceId)
		SET @ErrorMsg='Please specify valid LivelihoodSourceId parameter'
	ELSE IF @OtherProgId>0 AND NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@OtherProgCode AND T1.SystemCodeDetailId=@OtherProgId)
		SET @ErrorMsg='Please specify valid OtherProgId parameter'
	ELSE IF @OtherProgBenefitFreqId>0 AND NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@FrequencyCode AND T1.SystemCodeDetailId=@OtherProgBenefitFreqId)
		SET @ErrorMsg='Please specify valid OtherProgBenefitFreqId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch WHERE PSPBranchId=@PSPBranchId)
		SET @ErrorMsg='Please specify valid PSPBranchId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE UserId=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	INSERT INTO @tblMembers(MemberId,FirstName,MiddleName,Surname,MemberRoleId,RelationshipId,ExternalMember,BirthCertNo,NationalIdNo,DateOfBirth,SexId,EducationLevelId,IlliteracyReasonId,DisabilityTypeId,NCPwDNo,IllnessTypeId,OrphanhoodTypeId,MaritalStatusId,Phone1,Phone2,IsNew,IsForDeletion)
	SELECT T1.MemberId,T1.FirstName,T1.MiddleName,T1.Surname,T2.SystemCodeDetailId AS MemberRoleId,T4.SystemCodeDetailId AS RelationshipId,T1.ExternalMember,T1.BirthCertNo,T1.NationalIdNo,T1.DateOfBirth,T6.SystemCodeDetailId AS SexId,ISNULL(T8.SystemCodeDetailId,0) AS EducationLevelId,ISNULL(T18.SystemCodeDetailId,0) AS IlliteracyReasonId,ISNULL(T10.SystemCodeDetailId,0) AS DisabilityTypeId,NCPwDNo,ISNULL(T12.SystemCodeDetailId,0) AS IllnessTypeId,ISNULL(T14.SystemCodeDetailId,0) AS OrphanhoodTypeId,ISNULL(T16.SystemCodeDetailId,0) AS MaritalStatusId,Phone1,Phone2,IsNew,IsForDeletion
	FROM (
			SELECT U.R.value('(MemberId)[1]','int') AS MemberId
				,U.R.value('(FirstName)[1]','varchar(50)') AS FirstName
				,U.R.value('(MiddleName)[1]','varchar(50)') AS MiddleName
				,U.R.value('(Surname)[1]','varchar(50)') AS Surname
				,U.R.value('(MemberRoleId)[1]','varchar(50)') AS MemberRoleCode
				,U.R.value('(RelationshipId)[1]','int') AS RelationshipId
				,U.R.value('(ExternalMember)[1]','bit') AS ExternalMember
				,U.R.value('(BirthCertNo)[1]','varchar(30)') AS BirthCertNo
				,U.R.value('(NationalIDNo)[1]','varchar(30)') AS NationalIdNo
				,U.R.value('(DateOfBirth)[1]','datetime') AS DateOfBirth
				,U.R.value('(SexId)[1]','int') AS SexId
				,U.R.value('(EducationLevelId)[1]','int') AS EducationLevelId
				,U.R.value('(IlliteracyReasonId)[1]','int') AS IlliteracyReasonId
				,U.R.value('(DisabilityId)[1]','int') AS DisabilityTypeId
				,U.R.value('(NCPwDNo)[1]','varchar(30)') AS NCPwDNo
				,U.R.value('(IllnessId)[1]','int') AS IllnessTypeId
				,U.R.value('(OrphanhoodId)[1]','int') AS OrphanhoodTypeId
				,U.R.value('(MaritalStatusId)[1]','int') AS MaritalStatusId
				,U.R.value('(PhoneNo1)[1]','varchar(20)') AS Phone1
				,U.R.value('(PhoneNo2)[1]','varchar(20)') AS Phone2
				,U.R.value('(IsNew)[1]','bit') AS IsNew
				,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
			FROM @HHMembersXML.nodes('HHMembers/Record') AS U(R)
		) T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleCode=T2.DetailCode
			 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@MemberRoleCode
			 INNER JOIN SystemCodeDetail T4 ON T1.RelationshipId=T4.SystemCodeDetailId
			 INNER JOIN SystemCode T5 ON T4.SystemCodeId=T5.SystemCodeId AND T5.Code=@RelationshipCode
			 INNER JOIN SystemCodeDetail T6 ON T1.SexId=T6.SystemCodeDetailId
			 INNER JOIN SystemCode T7 ON T6.SystemCodeId=T7.SystemCodeId AND T7.Code=@SexCode
			 LEFT JOIN SystemCodeDetail T8 ON T1.EducationLevelId=T8.SystemCodeDetailId
			 LEFT JOIN SystemCode T9 ON T8.SystemCodeId=T9.SystemCodeId AND T9.Code=@EducationLevelCode
			 LEFT JOIN SystemCodeDetail T10 ON T1.DisabilityTypeId=T10.SystemCodeDetailId
			 LEFT JOIN SystemCode T11 ON T10.SystemCodeId=T11.SystemCodeId AND T11.Code=@DisabilityCode
			 LEFT JOIN SystemCodeDetail T12 ON T1.IllnessTypeId=T12.SystemCodeDetailId
			 LEFT JOIN SystemCode T13 ON T12.SystemCodeId=T13.SystemCodeId AND T13.Code=@IllnessCode
			 LEFT JOIN SystemCodeDetail T14 ON T1.OrphanhoodTypeId=T14.SystemCodeDetailId
			 LEFT JOIN SystemCode T15 ON T14.SystemCodeId=T15.SystemCodeId AND T15.Code=@OrphanhoodCode
			 INNER JOIN SystemCodeDetail T16 ON T1.MaritalStatusId=T16.SystemCodeDetailId
			 INNER JOIN SystemCode T17 ON T16.SystemCodeId=T17.SystemCodeId AND T17.Code=@MaritalStatusCode
			 LEFT JOIN SystemCodeDetail T18 ON T1.IlliteracyReasonId=T18.SystemCodeDetailId
			 LEFT JOIN SystemCode T19 ON T18.SystemCodeId=T19.SystemCodeId AND T19.Code=@IlliteracyReasonCode

	INSERT INTO @tblHHItems(HHItemTypeId,HHItemCount,IsNew,IsForDeletion)
	SELECT U.R.value('(ItemId)[1]','int') AS HHItemTypeId
		,U.R.value('(NoOfItems)[1]','tinyint') AS HHItemCount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHItemsXML.nodes('HHItems/Record') AS U(R)

	INSERT INTO @tblHHIncomes(HHIncomeTypeId,HHIncomeAmount,IsNew,IsForDeletion)
	SELECT U.R.value('(IncomeId)[1]','int') AS HHIncomeTypeId
		,U.R.value('(Amount)[1]','money') AS HHIncomeTypeAmount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHIncomesXML.nodes('HHIncomes/Record') AS U(R)

	INSERT INTO @tblHHExpenditures(HHExpenditureTypeId,HHExpenditureAmount,IsNew,IsForDeletion)
	SELECT U.R.value('(ExpenditureId)[1]','int') AS HHExpenditureTypeId
		,U.R.value('(Amount)[1]','money') AS HHExpenditureAmount
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHExpendituresXML.nodes('HHExpenditures/Record') AS U(R)

	INSERT INTO @tblHHNutrition(HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport,IsNew,IsForDeletion)
	SELECT U.R.value('(FoodId)[1]','int') AS HHNutritionTypeId
		,U.R.value('(EatenRaw)[1]','bit') AS EatenRaw
		,U.R.value('(PackSize)[1]','varchar(30)') AS PkgUnitSize
		,U.R.value('(PackCost)[1]','money') AS PkgUnitCost
		,U.R.value('(WeeklyFreq)[1]','tinyint') AS WeeklyConsumptionFreq
		,U.R.value('(FoodSource)[1]','varchar(30)') AS FoodSource
		,U.R.value('(ChoiceInfluence)[1]','varchar(30)') AS ChoiceInfluence
		,U.R.value('(ModeOfTransport)[1]','varchar(30)') AS ModeOfTransport
		,U.R.value('(IsNew)[1]','bit') AS IsNew
		,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
	FROM @HHNutritionsXML.nodes('HHNutritions/Record') AS U(R)

	IF @HHId>0
	BEGIN
		-----------------------------------------1. HOUSEHOLD, SOCIAL ASSISTANCE DATA, SUMMARY NUTRITION, OBSERVATIONS & PSP MAPPING DATA-----------------------------------------

		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.TargetGroupId=@TargetGroupId
		   ,T1.CaptureDate=@CaptureDate
		   ,T1.SerialNo=@SerialNo
		   ,T1.SubLocationId=@SubLocationId
		   ,T1.Village=@Village
		   ,T1.Ward=@Ward
		   ,T1.LocalityId=@LocalityId
		   ,T1.PhysicalAddress=@PhysicalAddress
		   ,T1.NearestLandmark=@NearestLandmark
		   ,T1.Longitude=@Longitude
		   ,T1.Latitude=@Latitude
		   ,T1.Elevation=@Elevation
		   ,T1.OtherProgId=@OtherProgId
		   ,T1.OtherProgBenefitFreqId=@OtherProgBenefitFreqId
		   ,T1.OtherProgMonthlyBenefitAmount=@OtherProgMonthlyBenefitAmount
		   ,T1.AverageDailyMealsCount=@AverageDailyMealsCount
		   ,T1.AverageWeeklyNoMealsCount=@AverageWeeklyNoMealsCount
		   ,T1.AverageWeeklyNoMealsReason=@AverageWeeklyNoMealsReason
		   ,T1.PSPBranchId=@PSPBranchId
		   ,T1.LoCNames=@LoCNames
		   ,T1.LoCNationalIDNo=@LoCNationalIdNo
		   ,T1.LoCPhone=@LoCPhone
		   ,T1.TargetingObservations=@TargetingObservations
		   ,T1.HHStatusId=ISNULL(@HHStatusId,T1.HHStatusId)
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Household T1
		WHERE T1.HHId=@HHId

		-----------------------------------------2. HOUSEHONLD MEMBERS DATA-----------------------------------------

		INSERT INTO HouseholdMembers(HHId,MemberRoleId,FirstName,MiddleName,Surname,SexId,RelationshipId,DoB,BirthCertNo,OrphanhoodTypeId,NationalIDNo,EducationLevelId,IlliteracyReasonId,MaritalStatusId,IllnessTypeId,DisabilityTypeId,NCPwDNo,Phone1,Phone2,ExternalMember)
		SELECT @HHId,T1.MemberRoleId,T1.FirstName,T1.MiddleName,T1.Surname,T1.SexId,T1.RelationshipId,T1.DateOfBirth,T1.BirthCertNo,T1.OrphanhoodTypeId,T1.NationalIdNo,T1.EducationLevelId,T1.IlliteracyReasonId,T1.MaritalStatusId,T1.IllnessTypeId,T1.DisabilityTypeId,T1.NCPwDNo,T1.Phone1,T1.Phone2,T1.ExternalMember
		FROM @tblMembers T1
		WHERE T1.IsNew=1

		UPDATE T1
		SET T1.MemberRoleId=T2.MemberRoleId
			,T1.FirstName=T2.FirstName
			,T1.MiddleName=T2.MiddleName
			,T1.Surname=T2.Surname
			,T1.SexId=T2.SexId
			,T1.RelationshipId=T2.RelationshipId
			,T1.DoB=T2.DateOfBirth
			,T1.OrphanhoodTypeId=T2.OrphanhoodTypeId
			,T1.NationalIDNo=T2.NationalIdNo
			,T1.EducationLevelId=T2.EducationLevelId
			,T1.IlliteracyReasonId=T2.IlliteracyReasonId
			,T1.MaritalStatusId=T2.MaritalStatusId
			,T1.IllnessTypeId=T2.IllnessTypeId
			,T1.DisabilityTypeId=T2.DisabilityTypeId
			,T1.NCPwDNo=T2.NCPwDNo
			,T1.Phone1=T2.Phone1
			,T1.Phone2=T2.Phone2
			,T1.ExternalMember=T2.ExternalMember
		FROM HouseholdMembers T1 INNER JOIN @tblMembers T2 ON T1.HHId=@HHId AND T1.MemberId=T2.MemberId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM HouseholdMembers T1 INNER JOIN @tblMembers T2 ON T1.HHId=@HHId AND T1.MemberId=T2.MemberId
		WHERE T2.IsForDeletion=1
		
		-----------------------------------------3. HOUSEHOLD DWELLING DATA-----------------------------------------

		--INSERT INTO HouseholdItems(HHId,HHItemTypeId,HHItemCount)
		--SELECT @HHId,T1.HHItemTypeId,T1.HHItemCount
		--FROM @tblHHItems T1
		--WHERE T1.IsNew=1

		--UPDATE T1
		--SET T1.HHItemCount=T2.HHItemCount
		--FROM HouseholdItems T1 INNER JOIN @tblHHItems T2 ON T1.HHId=@HHId AND T1.HHItemTypeId=T2.HHItemTypeId
		--WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		--DELETE T1
		--FROM HouseholdItems T1 INNER JOIN @tblHHItems T2 ON T1.HHId=@HHId AND T1.HHItemTypeId=T2.HHItemTypeId
		--WHERE T2.IsForDeletion=1

		--UPDATE T1
		--SET T1.WallMaterialId=@WallMaterialId
		--   ,T1.FloorMaterialId=@FloorMaterialId
		--   ,T1.RoofMaterialId=@RoofMaterialId
		--   ,T1.ToiletTypeId=@ToiletTypeId
		--   ,T1.WaterSourceId=@WallMaterialId
		--   ,T1.LightingSourceId=@LightingSourceId
		--   ,T1.CookingFuelSourceId=@CookingFuelId
		--FROM HouseholdCharacteristics T1
		--WHERE T1.HHId=@HHId

		-----------------------------------------4. HOUSEHOLD ASSETS-----------------------------------------
		
		--UPDATE T1
		--SET T1.OwnHouse=@OwnHouse
		--   ,T1.MonthlyRental=@MonthlyRental
		--   ,T1.HouseRooms=@HouseRooms
		--   ,T1.OwnFarmingLandSize=@FarmingLandSize
		--   ,T1.OwnGrazingLandSize=@GrazingLandSize
		--   ,T1.ZebuCattle=@ZebuCattle
		--   ,T1.HybridCattle=@HybridCattle
		--   ,T1.Goats=@Goats
		--   ,T1.Sheep=@Sheep
		--   ,T1.Pigs=@Pigs
		--   ,T1.Camels=@Camels
		--   ,T1.Donkeys=@Donkeys
		--   ,T1.Poultry=@Poultry
		--   ,T1.Rabbits=@Rabbits
		--   ,T1.LivelihoodSourceId=@LivelihoodSourceId
		--   ,T1.PermanentIncome=@PermanentIncome
		--   ,T1.ContractIncome=@ContractIncome
		--   ,T1.OtherIncome=@OtherIncome
		--FROM HouseholdAssets T1
		--WHERE T1.HHId=@HHId

		-----------------------------------------5. HOUSEHOLD BUSINESS & SOCIAL ASSISTANCE-----------------------------------------

		--UPDATE T1
		--SET T1.BusinessType=@BusinessDescription
		--   ,T1.TotalEmployeesInclusive=@TotalEmployees
		--   ,T1.EstimatedBusinessValue=@BusinessValue
		--   ,T1.PremisesMonthlyRental=@PremisesMonthlyRental
		--   ,T1.TotalStatutoryLevies=@StatutoryFees
		--   ,T1.AverageMonthlyProfit=@AverageMonthlyProfit
		--   ,T1.SavingsStorageTypeId=@SavingsStorageTypeId
		--   ,T1.SavingsAmount=@SavingsAmount
		--FROM HouseholdBusiness T1
		--WHERE T1.HHId=@HHId

		-----------------------------------------6. HOUSEHOLD MONTHLY INCOME vs EXPENDITURE-----------------------------------------

		--INSERT INTO HouseholdIncome(HHId,HHIncomeTypeId,HHIncomeAmount)
		--SELECT @HHId,T1.HHIncomeTypeId,T1.HHIncomeAmount
		--FROM @tblHHIncomes T1
		--WHERE T1.IsNew=1

		--UPDATE T1
		--SET T1.HHIncomeAmount=T2.HHIncomeAmount
		--FROM HouseholdIncome T1 INNER JOIN @tblHHIncomes T2 ON T1.HHId=@HHId AND T1.HHIncomeTypeId=T2.HHIncomeTypeId
		--WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		--DELETE T1
		--FROM HouseholdIncome T1 INNER JOIN @tblHHIncomes T2 ON T1.HHId=@HHId AND T1.HHIncomeTypeId=T2.HHIncomeTypeId
		--WHERE T2.IsForDeletion=1

		--INSERT INTO HouseholdExpenditure(HHId,HHExpenditureTypeId,HHExpenditureAmount)
		--SELECT @HHId,T1.HHExpenditureTypeId,T1.HHExpenditureAmount
		--FROM @tblHHExpenditures T1
		--WHERE T1.IsNew=1

		--UPDATE T1
		--SET T1.HHExpenditureAmount=T2.HHExpenditureAmount
		--FROM HouseholdExpenditure T1 INNER JOIN @tblHHExpenditures T2 ON T1.HHId=@HHId AND T1.HHExpenditureTypeId=T2.HHExpenditureTypeId
		--WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		--DELETE T1
		--FROM HouseholdExpenditure T1 INNER JOIN @tblHHExpenditures T2 ON T1.HHId=@HHId AND T1.HHExpenditureTypeId=T2.HHExpenditureTypeId
		--WHERE T2.IsForDeletion=1

		-----------------------------------------7. HOUSEHOLD NUTRITION-----------------------------------------

		--INSERT INTO HouseholdNutrition(HHId,HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport)
		--SELECT @HHId,T1.HHNutritionTypeId,T1.EatenRaw,T1.PkgUnitSize,T1.PkgUnitCost,T1.WeeklyConsumptionFreq,T1.FoodSource,T1.ChoiceInfluence,T1.ModeOfTransport
		--FROM @tblHHNutrition T1
		--WHERE T1.IsNew=1

		--UPDATE T1
		--SET T1.EatenRaw=T2.EatenRaw
		--   ,T1.PkgUnitSize=T2.PkgUnitSize
		--   ,T1.PkgUnitCost=T2.PkgUnitCost
		--   ,T1.WeeklyConsumptionFreq=T2.WeeklyConsumptionFreq
		--   ,T1.FoodSource=T2.FoodSource
		--   ,T1.ChoiceInfluence=T2.ChoiceInfluence
		--   ,T1.ModeOfTransport=T2.ModeOfTransport
		--FROM HouseholdNutrition T1 INNER JOIN @tblHHNutrition T2 ON T1.HHId=@HHId AND T1.HHNutritionTypeId=T2.HHNutritionTypeId
		--WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		--DELETE T1
		--FROM HouseholdNutrition T1 INNER JOIN @tblHHNutrition T2 ON T1.HHId=@HHId AND T1.HHNutritionTypeId=T2.HHNutritionTypeId
		--WHERE T2.IsForDeletion=1

	END
	ELSE
	BEGIN
		-----------------------------------------1. HOUSEHOLD, SOCIAL ASSISTANCE DATA, SUMMARY NUTRITION, OBSERVATIONS & PSP MAPPING DATA-----------------------------------------

		INSERT INTO Household(ProgrammeId,TargetGroupId,CaptureDate,SerialNo,SubLocationId,Village,Ward,LocalityId,PhysicalAddress,NearestLandmark,Longitude,Latitude,Elevation,OtherProgId,OtherProgBenefitFreqId,OtherProgMonthlyBenefitAmount,AverageDailyMealsCount,AverageWeeklyNoMealsCount,AverageWeeklyNoMealsReason,PSPBranchId,LOCNames,LOCNationalIDNo,LOCPhone,TargetingObservations,HHStatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@TargetGroupId,@CaptureDate,@SerialNo,@SubLocationId,@Village,@Ward,@LocalityId,@PhysicalAddress,@NearestLandmark,@Longitude,@Latitude,@Elevation,@OtherProgId,@OtherProgBenefitFreqId,@OtherProgMonthlyBenefitAmount,@AverageDailyMealsCount,@AverageWeeklyNoMealsCount,@AverageWeeklyNoMealsReason,@PSPBranchId,@LoCNames,@LoCNationalIdNo,@LoCPhone,@TargetingObservations,ISNULL(@HHStatusId,@HHStatus_RegistrationId),@UserId,GETDATE()
	
		SET @HHId=IDENT_CURRENT('Household');
		-----------------------------------------2. HOUSEHONLD MEMBERS DATA-----------------------------------------

		INSERT INTO HouseholdMembers(HHId,MemberRoleId,FirstName,MiddleName,Surname,SexId,RelationshipId,DoB,BirthCertNo,OrphanhoodTypeId,NationalIDNo,EducationLevelId,IlliteracyReasonId,MaritalStatusId,IllnessTypeId,DisabilityTypeId,NCPwDNo,Phone1,Phone2,ExternalMember)
		SELECT @HHId,T1.MemberRoleId,T1.FirstName,T1.MiddleName,T1.Surname,T1.SexId,T1.RelationshipId,T1.DateOfBirth,T1.BirthCertNo,T1.OrphanhoodTypeId,T1.NationalIdNo,T1.EducationLevelId,T1.IlliteracyReasonId,T1.MaritalStatusId,T1.IllnessTypeId,T1.DisabilityTypeId,T1.NCPwDNo,T1.Phone1,T1.Phone2,T1.ExternalMember
		FROM @tblMembers T1
		WHERE T1.IsNew=1

		-----------------------------------------3. HOUSEHOLD DWELLING DATA-----------------------------------------

		INSERT INTO HouseholdItems(HHId,HHItemTypeId,HHItemCount)
		SELECT @HHId,T1.HHItemTypeId,T1.HHItemCount
		FROM @tblHHItems T1
		WHERE T1.IsNew=1

		INSERT INTO HouseholdCharacteristics(HHId,WallMaterialId,FloorMaterialId,RoofMaterialId,ToiletTypeId,WaterSourceId,LightingSourceId,CookingFuelSourceId)
		SELECT @HHId,@WallMaterialId,@FloorMaterialId,@RoofMaterialId,@ToiletTypeId,@WaterSourceId,@LightingSourceId,@CookingFuelId

		-----------------------------------------4. HOUSEHOLD ASSETS-----------------------------------------

		INSERT INTO HouseholdAssets(HHId,OwnHouse,MonthlyRental,HouseRooms,OwnFarmingLandSize,OwnGrazingLandSize,ZebuCattle,HybridCattle,Goats,Sheep,Pigs,Camels,Donkeys,Poultry,Rabbits,LivelihoodSourceId,PermanentIncome,ContractIncome,OtherIncome)
		SELECT @HHId,@OwnHouse,@MonthlyRental,@HouseRooms,@FarmingLandSize,@GrazingLandSize,@ZebuCattle,@HybridCattle,@Goats,@Sheep,@Pigs,@Camels,@Donkeys,@Poultry,@Rabbits,@LivelihoodSourceId,@PermanentIncome,@ContractIncome,@OtherIncome

		-----------------------------------------5. HOUSEHOLD BUSINESS-----------------------------------------

		INSERT INTO HouseholdBusiness(HHId,BusinessType,TotalEmployeesInclusive,EstimatedBusinessValue,PremisesMonthlyRental,TotalStatutoryLevies,AverageMonthlyProfit,SavingsStorageTypeId,SavingsAmount)
		SELECT @HHId,@BusinessDescription,@TotalEmployees,@BusinessValue,@PremisesMonthlyRental,@StatutoryFees,@AverageMonthlyProfit,@SavingsStorageTypeId,@SavingsAmount

		-----------------------------------------6. HOUSEHOLD MONTHLY INCOME vs EXPENDITURE-----------------------------------------

		INSERT INTO HouseholdIncome(HHId,HHIncomeTypeId,HHIncomeAmount)
		SELECT @HHId,T1.HHIncomeTypeId,T1.HHIncomeAmount
		FROM @tblHHIncomes T1
		WHERE T1.IsNew=1

		INSERT INTO HouseholdExpenditure(HHId,HHExpenditureTypeId,HHExpenditureAmount)
		SELECT @HHId,T1.HHExpenditureTypeId,T1.HHExpenditureAmount
		FROM @tblHHExpenditures T1
		WHERE T1.IsNew=1

		-----------------------------------------7. HOUSEHOLD NUTRITION-----------------------------------------

		INSERT INTO HouseholdNutrition(HHId,HHNutritionTypeId,EatenRaw,PkgUnitSize,PkgUnitCost,WeeklyConsumptionFreq,FoodSource,ChoiceInfluence,ModeOfTransport)
		SELECT @HHId,T1.HHNutritionTypeId,T1.EatenRaw,T1.PkgUnitSize,T1.PkgUnitCost,T1.WeeklyConsumptionFreq,T1.FoodSource,T1.ChoiceInfluence,T1.ModeOfTransport
		FROM @tblHHNutrition T1
		WHERE T1.IsNew=1

	END

	SELECT 1 AS NoOfRows,@HHId AS HHId

	EXEC GetTargetings

END
GO



IF NOT OBJECT_ID('DeleteTargeting') IS NULL	DROP PROC DeleteTargeting
GO
CREATE PROC DeleteTargeting
	@HHId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @HHStatusCode varchar(20)
	DECLARE @HHStatus_EnrolledCode int

	SET @HHStatusCode='HHStatus'
	SET @HHStatus_EnrolledCode=1

	IF EXISTS(SELECT 1 
			  FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.HHStatusId=T2.SystemCodeDetailId 
								INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@HHStatusCode
			  WHERE HHId=@HHId AND CONVERT(int,T2.DetailCode)>=1)
		SET @ErrorMsg='Once a houeshold has been enrolled it can only be exited but not deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	--*IF THE HOUSEHOLD IS ALREADY A BENEFICIARY (ENROLLED) THEN THE RECORD CANNOT BE PURGED FOR HISTORICAL REASONS INSTEAD IT IS FLAGGED AS DELETED

	DELETE T1 FROM HouseholdNutrition T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdIncome T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdExpenditure T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdBusiness T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdAssets T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdItems T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdCharacteristics T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM HouseholdMembers T1 WHERE T1.HHId=@HHId
	DELETE T1 FROM Household T1 WHERE T1.HHId=@HHId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetTargetings	

END
GO



IF NOT OBJECT_ID('AddEditPaymentCycle') IS NULL	DROP PROC AddEditPaymentCycle
GO
CREATE PROC AddEditPaymentCycle
	@PaymentCycleId int=NULL
   ,@Description nvarchar(128)
   ,@ProgrammeCode nvarchar(40)
   ,@TargetGroupsXML XML
   ,@FromMonth tinyint
   ,@FromYear smallint
   ,@ToMonth tinyint
   ,@ToYear smallint
   ,@UserId int
AS
BEGIN
	DECLARE @tblTargetGroups TABLE(
		PaymentTargetId int
	   ,TargetGroupId int
	   ,PaymentAmount money
	   ,IsNew bit
	   ,IsForDeletion bit
	)

	DECLARE @HHStatus_EnrolledCode varchar(20)
	DECLARE @HHStatus_OnPayrollCode varchar(20)
	DECLARE @TargetGroupCode varchar(20)
	DECLARE @CalendarMonthsCode varchar(20)
	DECLARE @PaymentCycleStatusCode varchar(20)
	DECLARE @PaymentCycleStatusCode_CREATION varchar(20)
	DECLARE @PaymentCycleStatusId int
	DECLARE @ProgrammeId int
	DECLARE @PayableHHs int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @Exists bit
	DECLARE @NoOfRows int
	DECLARE @MaxPaymentPeriodMonths int
	DECLARE @TargetGroups1 varchar(128), @TargetGroups2 varchar(128)


	SET @HHStatus_EnrolledCode = '1'
	SET @HHStatus_OnPayrollCode = '2'
	SET @TargetGroupCode = 'Target Group'
	SET @PaymentCycleStatusCode = 'Payment Status'
	SET @PaymentCycleStatusCode_CREATION = 'CREATION'

	INSERT INTO @tblTargetGroups(PaymentTargetId,TargetGroupId,PaymentAmount,IsNew,IsForDeletion)
	SELECT T1.PaymentTargetId,T1.TargetGroupId,PaymentAmount,T1.IsNew,T1.IsForDeletion
	FROM (
		SELECT U.R.value('(PaymentTargetId)[1]','int') AS PaymentTargetId
			  ,U.R.value('(TargetGroupId)[1]','int') AS TargetGroupId
			  ,U.R.value('(PaymentAmount)[1]','int') AS PaymentAmount
			  ,U.R.value('(IsNew)[1]','int') AS IsNew
			  ,U.R.value('(IsForDeletion)[1]','int') AS IsForDeletion
		FROM @TargetGroupsXML.nodes('TargetGroups/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.TargetGroupId=T2.SystemCodeDetailId
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@TargetGroupCode

	SET @CalendarMonthsCode='Calendar Months'
	SET @MaxPaymentPeriodMonths=12

	SELECT @TargetGroups1=COALESCE(@TargetGroups1+','+CONVERT(varchar(20),T2.TargetGroupId),@TargetGroups1) FROM PaymentCycle T1 INNER JOIN PaymentTargetGroup T2 ON T1.PaymentCycleId=T2.PaymentCycleId WHERE T1.ProgrammeId=@ProgrammeId AND T1.FromMonth=@FromMonth AND T1.FromYear=@FromYear AND T1.ToMonth=@ToMonth AND ToYear=@ToYear AND @PaymentCycleId=0 ORDER BY T2.TargetGroupId ASC
	SELECT @TargetGroups2=COALESCE(@TargetGroups2+','+CONVERT(varchar(20),TargetGroupId),@TargetGroups1) FROM @tblTargetGroups ORDER BY TargetGroupId ASC
	SELECT @PaymentCycleStatusId=T1.SystemCodeDetailId FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@PaymentCycleStatusCode AND T1.DetailCode=@PaymentCycleStatusCode_CREATION
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @ProgrammeId=ProgrammeId FROM Programme WHERE ProgrammeCode=@ProgrammeCode

	IF ISNULL(@Description,'')=''
		SET @ErrorMsg='Please specify valid Description parameter'
	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	IF NOT EXISTS(SELECT 1 FROM @tblTargetGroups)
		SET @ErrorMsg='Please specify valid TargetGroupsXML parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId WHERE T1.DetailCode=@FromMonth AND T2.Code=@CalendarMonthsCode)
		SET @ErrorMsg='Please specify valid FromMonth parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId WHERE T1.DetailCode=@ToMonth AND T2.Code=@CalendarMonthsCode)
		SET @ErrorMsg='Please specify valid ToMonth parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@PaymentCycleId,0)=0 AND @TargetGroups1=@TargetGroups2
		SET @ErrorMsg='The Payment Cycle already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @PayableHHs=COUNT(T1.HHId)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.HHStatusId=T2.SystemCodeDetailId
	WHERE T1.ProgrammeId=@ProgrammeId AND T2.DetailCode IN(@HHStatus_EnrolledCode,@HHStatus_OnPayrollCode)

	BEGIN TRAN

	IF @PaymentCycleId>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.[Description]=@Description
		   ,T1.FromMonth=@FromMonth
		   ,T1.FromYear=@FromYear
		   ,T1.ToMonth=@ToMonth
		   ,T1.ToYear=@ToYear
		   ,T1.EnrolledHHs=@PayableHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PaymentCycle T1
		WHERE T1.PaymentCycleId=@PaymentCycleId

		INSERT INTO PaymentTargetGroup(PaymentCycleId,TargetGroupId,PaymentAmount)
		SELECT @PaymentCycleId,TargetGroupId,PaymentAmount
		FROM @tblTargetGroups
		WHERE IsNew=1

		UPDATE T1
		SET T1.TargetGroupId=T2.TargetGroupId
		   ,T1.PaymentAmount=T2.PaymentAmount
		FROM PaymentTargetGroup T1 INNER JOIN @tblTargetGroups T2 ON T1.PaymentTargetId=T2.PaymentTargetId
		WHERE T2.IsNew=0 AND T2.IsForDeletion=0

		DELETE T1
		FROM PaymentTargetGroup T1 INNER JOIN @tblTargetGroups T2 ON T1.PaymentTargetId=T2.PaymentTargetId
		WHERE T2.IsForDeletion=1
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCycle(ProgrammeId,[Description],FromMonth,FromYear,ToMonth,ToYear,EnrolledHHs,PaymentStatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@Description,@FromMonth,@FromYear,@ToMonth,@ToYear,@PayableHHs,@PaymentCycleStatusId,@UserId,GETDATE()

		SET @PaymentCycleId=IDENT_CURRENT('PaymentCycle')

		INSERT INTO PaymentTargetGroup(PaymentCycleId,TargetGroupId,PaymentAmount)
		SELECT @PaymentCycleId,TargetGroupId,PaymentAmount
		FROM @tblTargetGroups
		WHERE IsNew=1
	END
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows,@PaymentCycleId AS PaymentCycleId,T2.DetailCode AS PaymentCycleStatusId,T2.[Description] AS PaymentCycleStatus
		FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStatusId=T2.SystemCodeDetailId
		WHERE T1.PaymentCycleId=@PaymentCycleId
	END
END
GO



IF NOT OBJECT_ID('DeletePaymentCycle') IS NULL	DROP PROC DeletePaymentCycle
GO
CREATE PROC DeletePaymentCycle
	@PaymentCycleId int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @PaymentCycleStatusCode_CREATION varchar(20)
	DECLARE @PaymentCycleStatusCode_PREPAYROLL varchar(20)

	SET @PaymentCycleStatusCode_CREATION='CREATION'
	SET @PaymentCycleStatusCode_PREPAYROLL='PREPAYROLL'
	SELECT @PaymentCycleId=PaymentCycleId FROM PaymentCycle WHERE PaymentCycleId=@PaymentCycleId
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId

	IF ISNULL(@PaymentCycleId,0)=0
		SET @ErrorMsg='The specified Payment Cycle does not exist'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF NOT EXISTS(SELECT 1 FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStatusId=T2.SystemCodeDetailId WHERE PaymentCycleId=@PaymentCycleId AND T2.DetailCode IN(@PaymentCycleStatusCode_CREATION,@PaymentCycleStatusCode_PREPAYROLL))
		SET @ErrorMsg='Once the Payment Cycle Payroll has been processed it cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	UPDATE T1
	SET T1.IsDeleted=1
	   ,T1.DeletedBy=@UserId
	   ,T1.DeletedOn=GETDATE() 
	FROM PaymentCycle T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetPaymentCycles
END
GO


IF NOT OBJECT_ID('GeneratePaymentExceptionsFile') IS NULL	DROP PROC GeneratePaymentExceptionsFile
GO
CREATE PROC GeneratePaymentExceptionsFile
	@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@PaymentCycleId int
   ,@FilePathName nvarchar(256) OUTPUT
   ,@FileName nvarchar(50) OUTPUT
   ,@FileType nvarchar(10) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SQLStmt varchar(8000)
	DECLARE @PaymentCycleName varchar(50)
	DECLARE @ErrorMsg varchar(128)

	--VALIDATION FOR THE FILE PATH COMES HERE!
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	IF NOT OBJECT_ID('temp_PaymentExceptions') IS NULL	DROP TABLE temp_PaymentExceptions;
	CREATE TABLE temp_PaymentExceptions(
		ProgrammeNo int
	   ,BeneName varchar(128)
	   ,BeneIDNo varchar(30)
	   ,BeneIDNoIsInvalid varchar(10)
	   ,BeneIDNoIsDuplicated varchar(10)
	   ,CGName varchar(128)
	   ,CGIDNo varchar(30)
	   ,CGDNoIsInvalid varchar(10)
	   ,CGIDNoIsDuplicated varchar(10)
	   ,ConseUnpaidCyclesAmount money
	   ,PaymentAdjustmentAmount money
	   ,EntitlementAmount money
	   ,PaymentAmount money
	   ,PaymentIsSuspicious varchar(10)
	   ,PSP varchar(30)
	   ,County varchar(30)
	   ,Constituency varchar(30)
	   ,District varchar(30)
	   ,Division varchar(30)
	   ,Location varchar(30)
	   ,SubLocation varchar(30)
	   ,Ineligible varchar(10)
	   );

	INSERT INTO temp_PaymentExceptions(ProgrammeNo,BeneName,BeneIDNo,BeneIDNoIsInvalid,BeneIDNoIsDuplicated,CGName,CGIDNo,CGDNoIsInvalid,CGIDNoIsDuplicated,ConseUnpaidCyclesAmount,PaymentAdjustmentAmount,EntitlementAmount,PaymentAmount,PaymentIsSuspicious,PSP,County,Constituency,District,Division,Location,SubLocation,Ineligible)
	SELECT T2.HHNo AS ProgrammeNo
		,ISNULL(REPLACE(CASE WHEN(LEN(T1.BeneFirstName+' '+T1.BeneMiddleName+' '+T1.BeneSurname)>0) THEN (T1.BeneFirstName+' '+T1.BeneMiddleName+' '+T1.BeneSurname) ELSE '' END,'  ',' '),'')  AS BeneName
		,ISNULL(T1.BeneNationalIDNo,'') AS BeneIDNo
		,CASE WHEN(T1.BeneNationalIDNoInvalid IS NULL) THEN '' WHEN(T1.BeneNationalIDNoInvalid=0) THEN 'False' ELSE 'True' END AS BeneIDNoIsInvalid
		,CASE WHEN(T1.BeneNationalIDNoDuplicated IS NULL) THEN '' WHEN(T1.BeneNationalIDNoDuplicated=0) THEN 'False' ELSE 'True' END AS BeneIDNoIsDuplicated
		,ISNULL(REPLACE(CASE WHEN(LEN(T1.CGFirstName+' '+T1.CGMiddleName+' '+T1.CGSurname)>0) THEN (T1.CGFirstName+' '+T1.CGMiddleName+' '+T1.CGSurname) ELSE '' END,'  ',' '),'') AS CGName
		,ISNULL(T1.CGNationalIDNo,'') AS CGIDNo
		,CASE WHEN(T1.CGNationalIDNoInvalid IS NULL) THEN '' WHEN(T1.CGNationalIDNoInvalid=0) THEN 'False' ELSE 'True' END  AS CGDNoIsInvalid
		,CASE WHEN(T1.CGNationalIDNoDuplicated IS NULL) THEN '' WHEN(T1.CGNationalIDNoDuplicated=0) THEN 'False' ELSE 'True' END AS CGIDNoIsDuplicated
		,T1.ConseUnpaidCyclesAmount
		,T1.PaymentAdjustmentAmount
		,T1.EntitlementAmount
		,(T1.ConseUnpaidCyclesAmount+T1.EntitlementAmount+T1.PaymentAdjustmentAmount) AS PaymentAmount
		,CASE(T1.PaymentAmountSuspicious) WHEN 0 THEN 'False' ELSE 'True' END AS PaymentIsSuspicious
		,ISNULL(T4.PSPName,'') AS PSP
		,ISNULL(T5.CountyName,'') AS County
		,ISNULL(T5.ConstituencyName,'') AS Constituency
		,ISNULL(T5.DistrictName,'') AS District
		,ISNULL(T5.DivisionName,'') AS Division
		,ISNULL(T5.LocationName,'') AS Location
		,ISNULL(T5.SubLocationName,'') AS SubLocation
		,CASE(T1.Ineligible) WHEN 0 THEN 'False' ELSE 'True' END AS Ineligible
	FROM PrepayrollAudit T1 INNER JOIN Household T2 ON T1.HHId=T2.HHId
							INNER JOIN PSPBranch T3 ON T2.PSPBranchId=T3.PSPBranchId
							INNER JOIN PSP T4 ON T3.PSPId=T4.PSPId
							INNER JOIN (SELECT T1.SubLocationId,T1.SubLocationName,T2.LocationName,T3.DivisionName,T4.DistrictName,T5.CountyName,T6.ConstituencyName
										FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.LocationId
															INNER JOIN Division T3 ON T2.DivisionId=T3.DivisionId
															INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
															INNER JOIN County T5 ON T4.CountyId=T5.CountyId
															INNER JOIN Constituency T6 ON T1.ConstituencyId=T6.ConstituencyId
										) T5 ON T2.SubLocationId=T5.SubLocationId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.IsException=1
	ORDER BY T1.PrepayrollAuditId

	SELECT @PaymentCycleName=T2.ProgrammeCode+'-'+dbo.fn_MonthName(T1.FromMonth)+'-'+dbo.fn_MonthName(T1.ToMonth)+'-'+CONVERT(varchar(5),T1.ToYear)
	FROM PaymentCycle T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.ProgrammeId

	SET @FileName=@PaymentCycleName+'_Exceptions'
	SET @FileType='.csv'
	SET @FilePathName=@FilePathName+@FileName+@FileType

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SELECT * FROM temp_PaymentExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName +'"'
	--SELECT @SQLStmt
	--RETURN
	EXEC xp_cmdshell @SQLStmt;
	SET NOCOUNT OFF
END
GO




IF NOT OBJECT_ID('ProcessPrepayrollAudit') IS NULL	DROP PROC ProcessPrepayrollAudit
GO
CREATE PROC ProcessPrepayrollAudit
	@PaymentCycleId int
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@FilePath nvarchar(256)
   ,@DownloadPath nvarchar(256)
   ,@UserId int
AS
BEGIN
/*
	1. Update EnrolledHHs
	2. Insert into PrepayrollAudit then (i) check National ID No. validity (ii) check National ID No. duplication (iii) No. of consecutive uncollected cycles (iv) isException
	3. update PrepayrollAuditBy, PrepayrollAuditOn
	4. update paymentcycle status

*/
	IF NOT OBJECT_ID('temp_tblPrepayrollAudit') IS NULL	DROP TABLE temp_tblPrepayrollAudit;
	CREATE TABLE temp_tblPrepayrollAudit(
		HHId int NOT NULL
	   ,HHNo varchar(30) NULL
	   ,BeneFirstName varchar(50) NOT NULL
	   ,BeneMiddleName varchar(50) NULL
	   ,BeneSurname varchar(50) NOT NULL
	   ,BeneNationalIDNo varchar(30) NULL
	   ,BeneNationalIDNoInvalid bit NOT NULL
	   ,BeneNationalIDNoDuplicated bit NOT NULL
	   ,CGFirstName varchar(50) NOT NULL
	   ,CGMiddleName varchar(50) NULL
	   ,CGSurname varchar(50) NOT NULL
	   ,CGNationalIDNo varchar(30) NULL
	   ,CGNationalIDNoInvalid bit NOT NULL
	   ,CGNationalIDNoDuplicated bit NOT NULL
	   ,SubLocationId int NOT NULL
	   ,ConseUnpaidCyclesAmount money NOT NULL
	   ,PaymentAdjustmentAmount money NOT NULL
	   ,EntitlementAmount money NULL
	   ,PaymentAmountSuspicious bit NULL
	   ,Ineligible bit NULL
	);

	DECLARE @ErrorMsg varchar(128)
	DECLARE @PaymentCycleStatusCode varchar(20)
	DECLARE @PaymentCycleStatusCode_Prepayroll varchar(20)
	DECLARE @HHStatus_Enrolled varchar(20)
	DECLARE @HHStatus_OnPayroll varchar(20)
	DECLARE @MemberRoleCode varchar(20)
	DECLARE @MemberRoleCode_Caregiver varchar(20)
	DECLARE @MemberRoleCode_Beneficiary varchar(20)
	DECLARE @PaymentStatusCode_Closed varchar(20)
	DECLARE @MaxUnpaidCycles tinyint
	DECLARE @CycleEntitlementAmount money
	DECLARE @ActiveHHs int
	DECLARE @ReqBeneId bit
	DECLARE @ReqCG bit
	DECLARE @ReqCGId bit
	DECLARE @FileName nvarchar(128)
	DECLARE @FileType nvarchar(10)
	DECLARE @NoOfRows int

	SET @PaymentCycleStatusCode='Payment Status'
	SET @PaymentCycleStatusCode_Prepayroll='PREPAYROLL'
	SET @HHStatus_Enrolled='1'
	SET @HHStatus_OnPayroll='2'
	SET @MemberRoleCode='Member Role'
	SET @MemberRoleCode_Caregiver='CAREGIVER'
	SET @MemberRoleCode_Beneficiary='BENEFICIARY'
	SET @PaymentStatusCode_Closed='CLOSED'
	--THE FOLLOWING SHOULD BE IMPLEMENTED AS A SETTING ON PROG DEFINITION AND LATER INCLUDED AS PART OF PAYMENT CYCLE INFO
	SELECT @ReqBeneId=CASE(ProgrammeCode) WHEN 'OPCT' THEN 1 ELSE 0 END FROM Programme T1 INNER JOIN PaymentCycle T2 ON T1.ProgrammeId=T2.ProgrammeId WHERE T2.PaymentCycleId=@PaymentCycleId
	SELECT @ReqCG=CASE(ProgrammeCode) WHEN 'PwSD-CT' THEN 1 ELSE 0 END FROM Programme T1 INNER JOIN PaymentCycle T2 ON T1.ProgrammeId=T2.ProgrammeId WHERE T2.PaymentCycleId=@PaymentCycleId
	SELECT @ReqCGId=CASE(ProgrammeCode) WHEN 'PwSD-CT' THEN 1 ELSE 0 END FROM Programme T1 INNER JOIN PaymentCycle T2 ON T1.ProgrammeId=T2.ProgrammeId WHERE T2.PaymentCycleId=@PaymentCycleId

	SET @MaxUnpaidCycles=4
	SET @CycleEntitlementAmount=4000

	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @ActiveHHs=COUNT(HHId)
	FROM Household T1 INNER JOIN PaymentCycle T2 ON T1.ProgrammeId=T2.ProgrammeId
					  INNER JOIN SystemCodeDetail T3 ON T1.HHStatusId=T3.SystemCodeDetailId AND T3.DetailCode IN(@HHStatus_Enrolled,@HHStatus_OnPayroll)
	WHERE T2.PaymentCycleId=@PaymentCycleId

	INSERT INTO temp_tblPrepayrollAudit(HHId,HHNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneNationalIDNo,BeneNationalIDNoInvalid,BeneNationalIDNoDuplicated,CGFirstName,CGMiddleName,CGSurname,CGNationalIDNo,CGNationalIDNoInvalid,CGNationalIDNoDuplicated,SubLocationId,ConseUnpaidCyclesAmount,PaymentAdjustmentAmount,EntitlementAmount,PaymentAmountSuspicious,Ineligible)
	SELECT T1.HHId,T1.HHNo,ISNULL(T5.BeneFirstName,'') AS BeneFirstName,ISNULL(T5.BeneMiddleName,'') AS BeneMiddleName,ISNULL(T5.BeneSurname,'') AS BeneSurname,ISNULL(T5.BeneNationalIDNo,'') AS BeneNationalIDNo,CASE WHEN(T5.BeneNationalIDNo LIKE '%[^0-9-/]%') THEN 1 ELSE 0 END AS BeneNationalIDNoInvalid,CASE WHEN(T7.ResolvedNationalIDNo IS NULL) THEN 0 ELSE 1 END AS BeneNationalIDNoDuplicated,ISNULL(T6.CGFirstName,'') AS CGFirstName,ISNULL(T6.CGMiddleName,'') AS CGMiddleName,ISNULL(T6.CGSurname,'') AS CGSurname,ISNULL(T6.CGNationalIDNo,'') AS CGNationalIDNo,CASE WHEN(T6.CGNationalIDNo LIKE '%[^0-9-/]%') THEN 1 ELSE 0 END AS CGNationalIDNoInvalid,CASE WHEN(T8.ResolvedNationalIDNo IS NULL) THEN 0 ELSE 1 END AS CGNationalIDNoDuplicated,T11.SubLocationId,(ISNULL(T9.PaymentAmount,0)-ISNULL(T9.PaidAmount,0)) AS ConseUnpaidCyclesAmount,ISNULL(T10.AdjustmentAmount,0) AS AdjustmentAmount,T3.PaymentAmount AS EntitlementAmount,CASE WHEN(T9.HHId>0) THEN 1 WHEN(((ISNULL(T9.PaymentAmount,0)-ISNULL(T9.PaidAmount,0))+T3.PaymentAmount+T10.AdjustmentAmount)>(@CycleEntitlementAmount*@MaxUnpaidCycles)) THEN 1 ELSE 0 END AS PaymentAmountSuspicious
		,CASE WHEN(T5.HHId IS NULL) THEN 1
			  WHEN(@ReqBeneId=1 AND ISNULL(T5.BeneNationalIDNo,'')='') THEN 1
			  WHEN(@ReqCG=1 AND T6.HHId IS NULL) THEN 1
			  WHEN(@ReqCGId=1 AND ISNULL(T6.CGNationalIDNo,'')='') THEN 1
			  WHEN(T11.SubLocationId IS NULL) THEN 1
			  ELSE 0
		 END AS Ineligible
	FROM Household T1 INNER JOIN PaymentCycle T2 ON T1.ProgrammeId=T2.ProgrammeId AND T2.PaymentCycleId=@PaymentCycleId
					  INNER JOIN PaymentTargetGroup T3 ON T1.TargetGroupId=T3.TargetGroupId AND T2.PaymentCycleId=T3.PaymentCycleId
					  INNER JOIN SystemCodeDetail T4 ON T1.HHStatusId=T4.SystemCodeDetailId AND T4.DetailCode IN(@HHStatus_Enrolled,@HHStatus_OnPayroll)
					  LEFT JOIN (SELECT T1.HHId,T1.FirstName AS BeneFirstName,T1.MiddleName AS BeneMiddleName,T1.Surname AS BeneSurname,T1.NationalIDNo AS BeneNationalIDNo,CASE WHEN(ISNULL(T1.NationalIDNo,'')='') THEN '-1' WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END AS ResolvedBeneNationalIDNo
								 FROM HouseholdMembers T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleId=T2.SystemCodeDetailId AND T2.DetailCode=@MemberRoleCode_Beneficiary
														  INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@MemberRoleCode
								) T5 ON T1.HHId=T5.HHId
					  LEFT JOIN (SELECT T1.HHId,T1.FirstName AS CGFirstName,T1.MiddleName AS CGMiddleName,T1.Surname AS CGSurname,T1.NationalIDNo AS CGNationalIDNo,CASE WHEN(ISNULL(T1.NationalIDNo,'')='') THEN '-1' WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END AS ResolvedCGNationalIDNo
								 FROM HouseholdMembers T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleId=T2.SystemCodeDetailId AND T2.DetailCode=@MemberRoleCode_Caregiver
														  INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@MemberRoleCode
								 ) T6 ON T1.HHId=T6.HHId
					  LEFT JOIN(SELECT CASE WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END AS ResolvedNationalIDNo
								FROM HouseholdMembers T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleId=T2.SystemCodeDetailId AND T2.DetailCode IN(@MemberRoleCode_Beneficiary,@MemberRoleCode_Caregiver)
								WHERE T1.NationalIDNo<>''
								GROUP BY CASE WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END
								HAVING COUNT(T1.MemberId)>1
								) T7 ON T5.ResolvedBeneNationalIDNo=T7.ResolvedNationalIDNo
					  LEFT JOIN(SELECT CASE WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END AS ResolvedNationalIDNo
								FROM HouseholdMembers T1 INNER JOIN SystemCodeDetail T2 ON T1.MemberRoleId=T2.SystemCodeDetailId AND T2.DetailCode IN(@MemberRoleCode_Beneficiary,@MemberRoleCode_Caregiver)
								WHERE T1.NationalIDNo<>''
								GROUP BY CASE WHEN(REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-','') NOT LIKE '%[^0-9]%') THEN CONVERT(varchar,CONVERT(bigint,REPLACE(REPLACE(T1.NationalIDNo,'/',''),'-',''))) ELSE T1.NationalIDNo END
								HAVING COUNT(T1.MemberId)>1
								) T8 ON T6.ResolvedCGNationalIDNo=T8.ResolvedNationalIDNo
					  LEFT JOIN (SELECT T1.PrevPaymentCycle,T1.HHId,T1.PaymentAmount,T1.PaidAmount
								 FROM (
										SELECT ROW_NUMBER() OVER(PARTITION BY T1.HHId ORDER BY T1.PaymentCycleId DESC) AS PrevPaymentCycle,T1.HHId,(T1.EntitlementAmount+T1.ConseUnpaidCyclesAmount+T1.PaymentAdjustmentAmount) AS PaymentAmount,T2.PaidAmount
										FROM PrepayrollAudit T1 INNER JOIN Payment T2 ON T1.PrepayrollAuditId=T2.PrepayrollAuditId
																INNER JOIN PaymentCycle T3 ON T1.PaymentCycleId=T3.PaymentCycleId
																INNER JOIN SystemCodeDetail T4 ON T3.PaymentStatusId=T4.SystemCodeDetailId AND T4.DetailCode=@PaymentStatusCode_Closed
										) T1
								 WHERE T1.PrevPaymentCycle=1 AND (T1.PaymentAmount-T1.PaidAmount)>=(@CycleEntitlementAmount*@MaxUnpaidCycles)
								) T9 ON T1.HHId=T9.HHId
					  LEFT JOIN (SELECT PaymentCycleId,HhId,SUM(AdjustmentAmount) AS AdjustmentAmount
								 FROM PaymentAdjustment 
								 GROUP BY PaymentCycleId,HHId
								 )T10 ON T10.PaymentCycleId=@PaymentCycleId AND T1.HHId=T10.HhId
					  LEFT JOIN SubLocation T11 ON T1.SubLocationId=T11.SubLocationId
	WHERE T1.IsActive=1

	IF NOT EXISTS(SELECT 1 FROM temp_tblPrepayrollAudit)
	BEGIN
		SET @ErrorMsg='No eligible households'
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	INSERT INTO PrepayrollAudit(PaymentCycleId,HHId,BeneFirstName,BeneMiddleName,BeneSurname,BeneNationalIDNo,BeneNationalIDNoInvalid,BeneNationalIDNoDuplicated,CGFirstName,CGMiddleName,CGSurname,CGNationalIDNo,CGNationalIDNoInvalid,CGNationalIDNoDuplicated,SubLocationId,ConseUnpaidCyclesAmount,PaymentAdjustmentAmount,EntitlementAmount,PaymentAmountSuspicious,Ineligible,IsException)
	SELECT @PaymentCycleId,T1.HHId,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneNationalIDNo,T1.BeneNationalIDNoInvalid,T1.BeneNationalIDNoDuplicated,T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,T1.CGNationalIDNo,T1.CGNationalIDNoInvalid,T1.CGNationalIDNoDuplicated,T1.SubLocationId,T1.ConseUnpaidCyclesAmount,T1.PaymentAdjustmentAmount,T1.EntitlementAmount,T1.PaymentAmountSuspicious,T1.Ineligible,CASE WHEN(T1.BeneNationalIDNoInvalid=1 OR T1.BeneNationalIDNoDuplicated=1 OR T1.CGNationalIDNoInvalid=1 OR T1.CGNationalIDNoDuplicated=1 OR T1.PaymentAmountSuspicious=1 OR T1.Ineligible=1) THEN 1 ELSE 0 END AS IsException
	FROM temp_tblPrepayrollAudit T1 LEFT JOIN PrepayrollAudit T2 ON T1.HHId=T2.HHId AND T2.PaymentCycleId=@PaymentCycleId
	WHERE T2.PrepayrollAuditId IS NULL

	UPDATE T1
	SET T1.BeneFirstName=T2.BeneFirstName
	   ,T1.BeneMiddleName=T2.BeneMiddleName
	   ,T1.BeneSurname=T2.BeneSurname
	   ,T1.BeneNationalIDNo=T2.BeneNationalIDNo
	   ,T1.BeneNationalIDNoInvalid=T2.BeneNationalIDNoInvalid
	   ,T1.BeneNationalIDNoDuplicated=T2.BeneNationalIDNoDuplicated
	   ,T1.CGFirstName=T2.CGFirstName
	   ,T1.CGMiddleName=T2.CGMiddleName
	   ,T1.CGSurname=T2.CGSurname
	   ,T1.CGNationalIDNo=T2.CGNationalIDNo
	   ,T1.CGNationalIDNoInvalid=T2.CGNationalIDNoInvalid
	   ,T1.CGNationalIDNoDuplicated=T2.CGNationalIDNoDuplicated
	   ,T1.SubLocationId=T2.SubLocationId
	   ,T1.ConseUnpaidCyclesAmount=T2.ConseUnpaidCyclesAmount
	   ,T1.PaymentAdjustmentAmount=T2.PaymentAdjustmentAmount
	   ,T1.EntitlementAmount=T2.EntitlementAmount
	   ,T1.PaymentAmountSuspicious=T2.PaymentAmountSuspicious
	   ,T1.Ineligible=T2.Ineligible
	   ,T1.IsException=CASE WHEN(T2.BeneNationalIDNoInvalid=1 OR T2.BeneNationalIDNoDuplicated=1 OR T2.CGNationalIDNoInvalid=1 OR T2.CGNationalIDNoDuplicated=1 OR T2.PaymentAmountSuspicious=1 OR T2.Ineligible=1) THEN 1 ELSE 0 END
	FROM PrepayrollAudit T1 INNER JOIN temp_tblPrepayrollAudit T2 ON T1.PaymentCycleId=@PaymentCycleId AND T1.HHId=T2.HHId
	
	SET @NoOfRows=@@ROWCOUNT

	UPDATE T1
	SET T1.EnrolledHHs=@ActiveHHs
	   ,T1.PrepayrollAuditBy=@UserId
	   ,T1.PrepayrollAuditOn=GETDATE()
	   ,T1.PaymentStatusId=(SELECT SystemCodeDetailId FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_Prepayroll AND T2.Code=@PaymentCycleStatusCode)
	FROM PaymentCycle T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		EXEC GeneratePaymentExceptionsFile @DBServer=@DBServer,@DBName=@DBName,@DBUser=@DBUser,@DBPassword=@DBPassword,@PaymentCycleId=@PaymentCycleId,@FilePathName=@FilePath OUTPUT,@FileName=@FileName OUTPUT,@FileType=@FileType OUTPUT;
		SELECT @NoOfRows AS NoOfRows,T1.[Description] AS PaymentStatus,@DownloadPath+@FileName+@FileType AS DownloadPath,@FileName AS [FileName] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_Prepayroll AND T2.Code=@PaymentCycleStatusCode
	END
	EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
END
GO



IF NOT OBJECT_ID('ProcessExceptionsAction') IS NULL	DROP PROC ProcessExceptionsAction
GO
CREATE PROC ProcessExceptionsAction
	@ExceptionAction bit
   ,@ActionNotes nvarchar(128)
   ,@PrepayrollExceptionsXML XML
   ,@UserId int
AS
BEGIN
	IF NOT OBJECT_ID('temp_tblExceptionsAction') IS NULL	DROP TABLE temp_tblExceptionsAction;
	CREATE TABLE temp_tblExceptionsAction(
		PrepayrollAuditId int NOT NULL
	   ,ActionFlag bit NOT NULL
	);

	DECLARE @PaymentCycleId int
	DECLARE @PaymentCycleStatusCode varchar(20)
	DECLARE @PaymentCycleStatusCode_PrepayrollExceptions varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @PaymentCycleStatusCode='Payment Status'
	SET @PaymentCycleStatusCode_PrepayrollExceptions='PREPAYROLLEXCEPTIONS'

	INSERT INTO temp_tblExceptionsAction(PrepayrollAuditId,ActionFlag)
	SELECT T1.PrepayrollAuditId,T1.ActionFlag
	FROM (
		SELECT U.R.value('(PrepayrollAuditId)[1]','int') AS PrepayrollAuditId
			  ,U.R.value('(ActionFlag)[1]','bit') AS ActionFlag
		FROM @PrepayrollExceptionsXML.nodes('PrepayrollExceptions/Record') AS U(R)
	) T1 

	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @PaymentCycleId=MAX(T2.PaymentCycleId) FROM temp_tblExceptionsAction T1 INNER JOIN PrepayrollAudit T2 ON T1.PrepayrollAuditId=T2.PrepayrollAuditId

	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@PaymentCycleId,0)=0
		SET @ErrorMsg='Please specify valid prepayroll exceptions'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.IsActioned=@ExceptionAction
	   ,T1.ActionedBy=@UserId
	   ,T1.ActionedOn=GETDATE()
	   ,T1.ActionNotes=@ActionNotes
	FROM PrepayrollAudit T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PrepayrollAuditId=T2.PrepayrollAuditId
	WHERE T2.ActionFlag=1

	SET @NoOfRows=@@ROWCOUNT

	UPDATE T1
	SET T1.PaymentStatusId=(SELECT SystemCodeDetailId FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_PrepayrollExceptions AND T2.Code=@PaymentCycleStatusCode)
	FROM PaymentCycle T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows,T1.[Description] AS PaymentStatus FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_PrepayrollExceptions AND T2.Code=@PaymentCycleStatusCode
	END
	EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
	IF NOT OBJECT_ID('temp_tblExceptionsAction') IS NULL	DROP TABLE temp_tblExceptionsAction;
END
GO



IF NOT OBJECT_ID('GeneratePaymentFile') IS NULL	DROP PROC GeneratePaymentFile
GO
CREATE PROC GeneratePaymentFile
   @FilePath nvarchar(128)
  ,@PaymentCycleId int
AS
BEGIN


	DECLARE @SQLStmt varchar(8000)
	DECLARE @PaymentCycleName varchar(50)
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	--VALIDATION FOR THE FILE PATH COMES HERE!
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	CREATE TABLE #PaymentFile(
		HH_No int NOT NULL
	   ,BENEFICIARY_NAMES varchar(80)
	   ,BENEFICIARY_IDNO varchar(30)
	   ,CAREGIVER_NAMES varchar(80)
	   ,CAREGIVER_IDNO varchar(30)
	   ,CYCLE varchar(128)
	   ,SCHEDULED_AMOUNT money
	   ,BRANCH_CODE varchar(20)
	   ,BRANCH_NAME varchar(128)
	   );

	INSERT INTO #PaymentFile(HH_No,BENEFICIARY_NAMES,BENEFICIARY_IDNO,CAREGIVER_NAMES,CAREGIVER_IDNO,CYCLE,SCHEDULED_AMOUNT,BRANCH_CODE,BRANCH_NAME)
	SELECT T3.HHNo AS HH_No
		,REPLACE(CASE WHEN(LEN(T2.BeneFirstName+' '+T2.BeneMiddleName+' '+T2.BeneSurname)>0) THEN (T2.BeneFirstName+' '+T2.BeneMiddleName+' '+T2.BeneSurname) ELSE '' END,'  ',' ')  AS BENEFICIARY_NAMES
		,T2.BeneNationalIDNo AS BENEFICIARY_IDNO
		,REPLACE(CASE WHEN(LEN(T2.CGFirstName+' '+T2.CGMiddleName+' '+T2.CGSurname)>0) THEN (T2.CGFirstName+' '+T2.CGMiddleName+' '+T2.CGSurname) ELSE '' END,'  ',' ') AS CAREGIVER_NAMES
		,T2.CGNationalIDNo AS CAREGIVER_IDNO
		,T4.[Description] AS CYCLE
		,(T2.ConseUnpaidCyclesAmount+T2.EntitlementAmount+T2.PaymentAdjustmentAmount) AS SCHEDULED_AMOUNT
		,T5.PSPBranchCode AS BRANCH_CODE
		,T5.PSPBranchName AS BRANCH_NAME
	FROM Payment T1 INNER JOIN PrepayrollAudit T2 ON T1.PrepayrollAuditId=T2.PrepayrollAuditId
					INNER JOIN Household T3 ON T2.HHId=T3.HHId
					INNER JOIN PaymentCycle T4 ON T2.PaymentCycleId=T4.PaymentCycleId
					INNER JOIN PSPBranch T5 ON T3.PSPBranchId=T5.PSPBranchId
					INNER JOIN PSP T6 ON T5.PSPId=T6.PSPId
	WHERE T2.PaymentCycleId=@PaymentCycleId

	SELECT @PaymentCycleName=T2.ProgrammeCode+'-'+dbo.fn_MonthName(T1.FromMonth)+'-'+dbo.fn_MonthName(T1.ToMonth)+'-'+CONVERT(varchar(5),T1.ToYear)
	FROM PaymentCycle T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.ProgrammeId

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FilePath=@FilePath+@PaymentCycleName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time+'.xml'

	SET @SQLStmt='BCP "SELECT * FROM #PaymentFile FOR XML RAW(''RECORD''), ROOT(''PAYMENTS''), ELEMENTS, BINARY BASE64" QUERYOUT "'+ @FilePath +'" -S.\SQL2012 -dDSD-MIS -T -c -r -t'
	--SELECT @SQLStmt
	--RETURN
	EXEC xp_cmdshell @SQLStmt;

	DROP TABLE #PaymentFile;
END
GO




IF NOT OBJECT_ID('ProcessPayroll') IS NULL	DROP PROC ProcessPayroll
GO
CREATE PROC ProcessPayroll
	@PaymentCycleId int
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @PaymentCycleStatusCode varchar(20)
	DECLARE @PaymentCycleStatusCode_Payment varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @PaymentCycleStatusCode='Payment Status'
	SET @PaymentCycleStatusCode_Payment='PAYMENT'
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @NoOfRows=COUNT(PaymentCycleId) FROM PrepayrollAudit WHERE PaymentCycleId=@PaymentCycleId

	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@NoOfRows,0)=0
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter that has prepayroll audit information'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	INSERT INTO Payment(PrepayrollAuditId)
	SELECT T1.PrepayrollAuditId
	FROM PrepayrollAudit T1 LEFT JOIN Payment T2 ON T1.PrepayrollAuditId=T2.PrepayrollAuditId
	WHERE (T1.IsException=0 OR T1.IsActioned=1) AND T2.PaymentId IS NULL

	SET @NoOfRows=@@ROWCOUNT

	UPDATE T1
	SET T1.PaymentBy=@UserId
	   ,T1.PaymentOn=GETDATE()
	   ,T1.PaymentStatusId=(SELECT SystemCodeDetailId FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_Payment AND T2.Code=@PaymentCycleStatusCode)
	FROM PaymentCycle T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows,T1.[Description] AS PaymentStatus FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T1.DetailCode=@PaymentCycleStatusCode_Payment AND T2.Code=@PaymentCycleStatusCode
	END
	EXEC GetPaymentAudit @PaymentCycleId=@PaymentCycleId;
	EXEC GeneratePaymentFile @PaymentCycleId=@PaymentCycleId,@FilePath=@FilePath;
END
GO



IF NOT OBJECT_ID('AddEditProgramme') IS NULL	DROP PROC AddEditProgramme
GO
CREATE PROC AddEditProgramme
	@ProgrammeId smallint=NULL
   ,@ProgrammeCode nvarchar(20)
   ,@ProgrammeName nvarchar(128)
   ,@IsActive bit=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @Exists bit
	DECLARE @NoOfRows int

	SET @IsActive=ISNULL(@IsActive,1)
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @Exists=COUNT(ProgrammeId) FROM [Programme] WHERE ProgrammeCode=@ProgrammeCode

	IF ISNULL(@ProgrammeCode,'')=''
		SET @ErrorMsg='Please specify valid ProgrammeCode parameter'
	IF ISNULL(@ProgrammeName,'')=''
		SET @ErrorMsg='Please specify valid ProgrammeName parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@Exists,0)<>0 AND ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='The ProgrammeCode already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @ProgrammeId>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeCode=@ProgrammeCode
		   ,T1.ProgrammeName=@ProgrammeName
		   ,T1.IsActive=@IsActive
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM [Programme] T1
		WHERE T1.ProgrammeId=@ProgrammeId
	END
	ELSE
	BEGIN
		INSERT INTO [Programme](ProgrammeCode,ProgrammeName,IsActive,CreatedBy,CreatedOn)
		SELECT @ProgrammeCode,@ProgrammeName,@IsActive,@UserId,GETDATE()

		SET @ProgrammeId=IDENT_CURRENT('Programme')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetProgrammes	
	END
END
GO



IF NOT OBJECT_ID('DeleteProgramme') IS NULL	DROP PROC DeleteProgramme
GO
CREATE PROC DeleteProgramme
	@ProgrammeId smallint
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM Household WHERE ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The specified Programme has dependant child records'
	--CONSIDER CHECKING IN OTHER DEPENDANT TABLE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM [Programme] T1
	WHERE T1.ProgrammeId=@ProgrammeId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetProgrammes
END
GO



IF NOT OBJECT_ID('AddEditDonor') IS NULL	DROP PROC AddEditDonor
GO
CREATE PROC AddEditDonor
	@DonorId smallint=NULL
   ,@DonorCode nvarchar(20)
   ,@DonorName nvarchar(128)
   ,@TargetGroupId int
   ,@IsActive bit=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @Exists bit
	DECLARE @NoOfRows int

	SET @IsActive=ISNULL(@IsActive,1)
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	--VALIDATE FOR TargetGroupId
	SELECT @Exists=COUNT(DonorId) FROM Donor WHERE DonorCode=@DonorCode

	IF ISNULL(@DonorCode,'')=''
		SET @ErrorMsg='Please specify valid DonorCode parameter'
	IF ISNULL(@DonorName,'')=''
		SET @ErrorMsg='Please specify valid DonorName parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@Exists,0)<>0 AND ISNULL(@DonorId,0)=0
		SET @ErrorMsg='The DonorCode already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @DonorId>0
	BEGIN
		UPDATE T1
		SET T1.DonorCode=@DonorCode
		   ,T1.DonorName=@DonorName
		   ,T1.TargetGroupId=@TargetGroupId
		   ,T1.IsActive=@IsActive
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Donor T1
		WHERE T1.DonorId=@DonorId
	END
	ELSE
	BEGIN
		INSERT INTO Donor(DonorCode,DonorName,TargetGroupId,IsActive,CreatedBy,CreatedOn)
		SELECT @DonorCode,@DonorName,@TargetGroupId,@IsActive,@UserId,GETDATE()

		SET @DonorId=IDENT_CURRENT('Donor')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetDonors	
	END
END
GO



IF NOT OBJECT_ID('DeleteDonor') IS NULL	DROP PROC DeleteDonor
GO
CREATE PROC DeleteDonor
	@DonorId smallint
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM TargetGroup WHERE DonorId=@DonorId)
		SET @ErrorMsg='The specified Donor has dependant child records'
	--CONSIDER CHECKING IN OTHER DEPENDANT TABLE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM Donor T1
	WHERE T1.DonorId=@DonorId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetDonors
END
GO



IF NOT OBJECT_ID('AddEditPSP') IS NULL	DROP PROC AddEditPSP
GO
CREATE PROC AddEditPSP
	@PSPId smallint=NULL
   ,@PSPCode nvarchar(20)
   ,@PSPName nvarchar(128)
   ,@IsDefault bit=NULL
   ,@IsActive bit=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CodeExists bit
	DECLARE @DefaultExists bit
	DECLARE @NoOfRows int

	SET @IsActive=ISNULL(@IsActive,1)
	SET @IsDefault=ISNULL(@IsDefault,0)
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @CodeExists=COUNT(PSPId) FROM PSP WHERE PSPCode=@PSPCode
	SELECT @DefaultExists=COUNT(PSPId) FROM PSP WHERE ISNULL(IsActive,0)<>0 AND ISNULL(IsDefault,0)<>0 AND PSPId<>@PSPId

	IF ISNULL(@PSPCode,'')=''
		SET @ErrorMsg='Please specify valid PSPCode parameter'
	IF ISNULL(@PSPName,'')=''
		SET @ErrorMsg='Please specify valid PSPName parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@CodeExists,0)<>0 AND ISNULL(@PSPId,0)=0
		SET @ErrorMsg='The PSPCode already exists'
	IF ISNULL(@IsDefault,0)<>0 AND ISNULL(@IsActive,0)=0
		SET @ErrorMsg='You cannot deactivate a default PSP. First set another branch to be the default PSP'
	IF ISNULL(@IsDefault,0)<>0 AND ISNULL(@IsActive,0)=0
		SET @ErrorMsg='You cannot set an inactive PSP to be the default PSP'
	IF ISNULL(@DefaultExists,0)=0 AND ISNULL(@IsDefault,0)=0
		SET @ErrorMsg='One of the active PSPs must be the default PSP'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @PSPId>0
	BEGIN
		IF @IsDefault=1		--THERE SHOULD ONLY BE ONE DEFAULT PSP
			UPDATE T1 SET T1.IsDefault=0 FROM PSP T1 WHERE T1.PSPId<>@PSPId

		UPDATE T1
		SET T1.PSPCode=@PSPCode
		   ,T1.PSPName=@PSPName
		   ,T1.IsDefault=@IsDefault
		   ,T1.IsActive=@IsActive
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PSP T1
		WHERE T1.PSPId=@PSPId
	END
	ELSE
	BEGIN
		INSERT INTO PSP(PSPCode,PSPName,IsDefault,IsActive,CreatedBy,CreatedOn)
		SELECT @PSPCode,@PSPName,@IsDefault,@IsActive,@UserId,GETDATE()

		SET @PSPId=IDENT_CURRENT('PSP')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetPSPs	
	END
END
GO



IF NOT OBJECT_ID('DeletePSP') IS NULL	DROP PROC DeletePSP
GO
CREATE PROC DeletePSP
	@PSPId smallint
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM Household T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.PSPBranchId WHERE T2.PSPId=@PSPId)
		SET @ErrorMsg='The specified PSP has dependant child records'
	--CONSIDER CHECKING IN OTHER DEPENDANT TABLE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM PSP T1
	WHERE T1.PSPId=@PSPId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetPSPs
END
GO



IF NOT OBJECT_ID('AddEditPSPBranch') IS NULL	DROP PROC AddEditPSPBranch
GO
CREATE PROC AddEditPSPBranch
	@PSPBranchId smallint=NULL
   ,@PSPId smallint
   ,@PSPBranchCode nvarchar(20)
   ,@PSPBranchName nvarchar(128)
   ,@IsDefault bit=NULL
   ,@IsActive bit=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CodeExists bit
	DECLARE @DefaultExists bit
	DECLARE @NoOfRows int

	SET @IsActive=ISNULL(@IsActive,1)
	SET @IsDefault=ISNULL(@IsDefault,0)
	SELECT @PSPId=PSPId FROM PSP WHERE PSPId=@PSPId
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @CodeExists=COUNT(PSPBranchId) FROM PSPBranch WHERE PSPBranchCode=@PSPBranchCode
	SELECT @DefaultExists=COUNT(PSPId) FROM PSPBranch WHERE PSPId=@PSPId AND ISNULL(IsActive,0)<>0 AND ISNULL(IsDefault,0)<>0 AND PSPBranchId<>@PSPBranchId

	IF ISNULL(@PSPId,0)=0
		SET @ErrorMsg='Please specify valid PSPId parameter'
	IF ISNULL(@PSPBranchCode,'')=''
		SET @ErrorMsg='Please specify valid PSPBranchCode parameter'
	IF ISNULL(@PSPBranchName,'')=''
		SET @ErrorMsg='Please specify valid PSPBranchName parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@CodeExists,0)<>0 AND ISNULL(@PSPBranchId,0)=0
		SET @ErrorMsg='The PSPBranchCode already exists'
	IF ISNULL(@IsDefault,0)<>0 AND ISNULL(@IsActive,0)=0
		SET @ErrorMsg='You cannot deactivate a default PSP branch. First set another branch as the default PSP branch'
	IF ISNULL(@IsDefault,0)<>0 AND ISNULL(@IsActive,0)=0
		SET @ErrorMsg='You cannot set an inactive branch as the default PSP branch'
	IF ISNULL(@DefaultExists,0)=0 AND ISNULL(@IsDefault,0)=0
		SET @ErrorMsg='One of the active branches must be the default PSP branch'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @PSPBranchId>0
	BEGIN
		IF @IsDefault=1		--THERE SHOULD ONLY BE ONE DEFAULT PSPBranch
			UPDATE T1 SET T1.IsDefault=0 FROM PSPBranch T1 WHERE T1.PSPBranchId<>@PSPBranchId

		UPDATE T1
		SET T1.PSPId=@PSPId
		   ,T1.PSPBranchCode=@PSPBranchCode
		   ,T1.PSPBranchName=@PSPBranchName
		   ,T1.IsDefault=@IsDefault
		   ,T1.IsActive=@IsActive
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PSPBranch T1
		WHERE T1.PSPBranchId=@PSPBranchId
	END
	ELSE
	BEGIN
		INSERT INTO PSPBranch(PSPId,PSPBranchCode,PSPBranchName,IsDefault,IsActive,CreatedBy,CreatedOn)
		SELECT @PSPId,@PSPBranchCode,@PSPBranchName,@IsDefault,@IsActive,@UserId,GETDATE()

		SET @PSPBranchId=IDENT_CURRENT('PSPBranch')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetPSPBranches	
	END
END
GO



IF NOT OBJECT_ID('DeletePSPBranch') IS NULL	DROP PROC DeletePSPBranch
GO
CREATE PROC DeletePSPBranch
	@PSPBranchId smallint
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM Household T1 WHERE T1.PSPBranchId=@PSPBranchId)
		SET @ErrorMsg='The specified PSPBranch has dependant child records'
	--CONSIDER CHECKING IN OTHER DEPENDANT TABLE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM PSPBranch T1
	WHERE T1.PSPBranchId=@PSPBranchId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetPSPBranches
END
GO



IF NOT OBJECT_ID('AddEditGeoUnit') IS NULL	DROP PROC AddEditGeoUnit
GO
CREATE PROC AddEditGeoUnit
	@GeoUnit varchar(20)
   ,@GeoId int
   ,@GeoCode varchar(20)
   ,@GeoName varchar(30)
   ,@LocalityId int
   ,@ParentGeoId int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @LocalityCode varchar(20)
	DECLARE @GeoCodeExists bit
	DECLARE @GeoCode_PROVINCE varchar(20)
	DECLARE @GeoCode_COUNTY varchar(20)
	DECLARE @GeoCode_CONSTITUENCY varchar(20)
	DECLARE @GeoCode_DISTRICT varchar(20)
	DECLARE @GeoCode_DIVISION varchar(20)
	DECLARE @GeoCode_LOCATION varchar(20)
	DECLARE @GeoCode_SUBLOCATION varchar(20)

	SET @LocalityCode='Locality'
	SET @GeoCode_PROVINCE='PROVINCE'
	SET @GeoCode_COUNTY='COUNTY'
	SET @GeoCode_CONSTITUENCY='CONSTITUENCY'
	SET @GeoCode_DISTRICT='DISTRICT'
	SET @GeoCode_DIVISION='DIVISION'
	SET @GeoCode_LOCATION='LOCATION'
	SET @GeoCode_SUBLOCATION='SUBLOCATION'

	SET @GeoId=ISNULL(@GeoId,0)
	SET @GeoCodeExists=CASE(@GeoUnit) WHEN @GeoCode_PROVINCE THEN (SELECT COUNT(ProvinceId) FROM Province WHERE ProvinceCode=@GeoCode AND ProvinceId<>@GeoId)
									  WHEN @GeoCode_COUNTY THEN (SELECT COUNT(CountyId) FROM County WHERE CountyCode=@GeoCode AND CountyId<>@GeoId)
									  WHEN @GeoCode_CONSTITUENCY THEN (SELECT COUNT(ConstituencyId) FROM Constituency WHERE ConstituencyCode=@GeoCode AND ConstituencyId<>@GeoId)
									  WHEN @GeoCode_DISTRICT THEN (SELECT COUNT(DistrictId) FROM District WHERE DistrictCode=@GeoCode AND DistrictId<>@GeoId)
									  WHEN @GeoCode_DIVISION THEN (SELECT COUNT(DivisionId) FROM Division WHERE DivisionCode=@GeoCode AND DivisionId<>@GeoId)
									  WHEN @GeoCode_LOCATION THEN (SELECT COUNT(LocationId) FROM Location WHERE LocationCode=@GeoCode AND LocationId<>@GeoId)
									  WHEN @GeoCode_SUBLOCATION THEN (SELECT COUNT(SubLocationId) FROM SubLocation WHERE SubLocationCode=@GeoCode AND SubLocationId<>@GeoId)
									  ELSE -1
					    END
	SET @ParentGeoId=CASE(@GeoUnit) WHEN @GeoCode_COUNTY THEN (SELECT ProvinceId FROM Province WHERE ProvinceId=@ParentGeoId)
									WHEN @GeoCode_CONSTITUENCY THEN (SELECT CountyId FROM County WHERE CountyId=@ParentGeoId)
									WHEN @GeoCode_DISTRICT THEN (SELECT CountyId FROM County WHERE CountyId=@ParentGeoId)
									WHEN @GeoCode_DIVISION THEN (SELECT DistrictId FROM District WHERE DistrictId=@ParentGeoId)
									WHEN @GeoCode_LOCATION THEN (SELECT DivisionId FROM Division WHERE DivisionId=@ParentGeoId)
									WHEN @GeoCode_SUBLOCATION THEN (SELECT LocationId FROM Location WHERE LocationId=@ParentGeoId)
									ELSE -1
					 END

	SELECT @LocalityId=T1.SystemCodeDetailId FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code=@LocalityCode WHERE T1.SystemCodeDetailId=@LocalityId
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	
	IF ISNULL(@GeoCodeExists,0)=-1
		SET @ErrorMsg='Please specify valid GeoUnit parameter'
	IF ISNULL(@GeoCode,'')=''
		SET @ErrorMsg='Please specify valid GeoCode parameter'
	IF ISNULL(@GeoName,'')=''
		SET @ErrorMsg='Please specify valid GeoName parameter'
	IF ISNULL(@LocalityId,0)=0 AND @GeoUnit=@GeoCode_SUBLOCATION
		SET @ErrorMsg='Please specify valid LocalityId parameter'
	IF ISNULL(@ParentGeoId,0)=0
		SET @ErrorMsg='Please specify valid ParentGeoId parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@GeoCodeExists,0)>0
		SET @ErrorMsg='The GeoCode already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @GeoId>0
	BEGIN
		IF @GeoUnit=@GeoCode_PROVINCE
			UPDATE T1 SET T1.ProvinceCode=@GeoCode,T1.ProvinceName=@GeoName,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM Province T1 WHERE T1.ProvinceId=@GeoId
		
		IF @GeoUnit=@GeoCode_COUNTY
			UPDATE T1 SET T1.CountyCode=@GeoCode,T1.CountyName=@GeoName,T1.ProvinceId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM County T1 WHERE T1.CountyId=@GeoId
		
		IF @GeoUnit=@GeoCode_CONSTITUENCY
			UPDATE T1 SET T1.ConstituencyCode=@GeoCode,T1.ConstituencyName=@GeoName,T1.CountyId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM Constituency T1 WHERE T1.ConstituencyId=@GeoId
		
		IF @GeoUnit=@GeoCode_DISTRICT
			UPDATE T1 SET T1.DistrictCode=@GeoCode,T1.DistrictName=@GeoName,T1.CountyId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM District T1 WHERE T1.DistrictId=@GeoId
		
		IF @GeoUnit=@GeoCode_DIVISION
			UPDATE T1 SET T1.DivisionCode=@GeoCode,T1.DivisionName=@GeoName,T1.DistrictId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM Division T1 WHERE T1.DivisionId=@GeoId
		
		IF @GeoUnit=@GeoCode_LOCATION
			UPDATE T1 SET T1.LocationCode=@GeoCode,T1.LocationName=@GeoName,T1.DivisionId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM Location T1 WHERE T1.LocationId=@GeoId
		
		IF @GeoUnit=@GeoCode_SUBLOCATION
			UPDATE T1 SET T1.SubLocationCode=@GeoCode,T1.SubLocationName=@GeoName,T1.LocationId=@ParentGeoId,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM SubLocation T1 WHERE T1.SubLocationId=@GeoId
	END
	ELSE
	BEGIN
		IF @GeoUnit=@GeoCode_PROVINCE
			INSERT INTO Province(ProvinceCode,ProvinceName,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_COUNTY
			INSERT INTO County(CountyCode,CountyName,ProvinceId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_CONSTITUENCY
			INSERT INTO Constituency(ConstituencyCode,ConstituencyName,CountyId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_DISTRICT
			INSERT INTO District(DistrictCode,DistrictName,CountyId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_DIVISION
			INSERT INTO Division(DivisionCode,DivisionName,DistrictId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_LOCATION
			INSERT INTO Location(LocationCode,LocationName,DivisionId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()

		IF @GeoUnit=@GeoCode_SUBLOCATION
			INSERT INTO SubLocation(SubLocationCode,SubLocationName,LocationId,CreatedBy,CreatedOn)
			SELECT @GeoCode,@GeoName,@ParentGeoId,@UserId,GETDATE()
	END
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows

		EXEC GetGeoUnits @GeoUnit=@GeoUnit	
	END
END
GO




IF NOT OBJECT_ID('DeleteGeoUnit') IS NULL	DROP PROC DeleteGeoUnit
GO
CREATE PROC DeleteGeoUnit
	@GeoUnit varchar(20)
   ,@GeoId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @GeoCode_PROVINCE varchar(20)
	DECLARE @GeoCode_COUNTY varchar(20)
	DECLARE @GeoCode_CONSTITUENCY varchar(20)
	DECLARE @GeoCode_DISTRICT varchar(20)
	DECLARE @GeoCode_DIVISION varchar(20)
	DECLARE @GeoCode_LOCATION varchar(20)
	DECLARE @GeoCode_SUBLOCATION varchar(20)

	SET @GeoCode_PROVINCE='PROVINCE'
	SET @GeoCode_COUNTY='COUNTY'
	SET @GeoCode_CONSTITUENCY='CONSTITUENCY'
	SET @GeoCode_DISTRICT='DISTRICT'
	SET @GeoCode_DIVISION='DIVISION'
	SET @GeoCode_LOCATION='LOCATION'
	SET @GeoCode_SUBLOCATION='SUBLOCATION'

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY,@GeoCode_CONSTITUENCY,@GeoCode_DISTRICT,@GeoCode_DIVISION,@GeoCode_LOCATION,@GeoCode_SUBLOCATION)
		DELETE T1
		--SELECT T1.*
		FROM SubLocation T1 INNER JOIN (
										SELECT DISTINCT T1.*
										FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.LocationId
															INNER JOIN Division T3 ON T2.DivisionId=T3.DivisionId
															INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
															INNER JOIN Constituency T5 ON T1.ConstituencyId=T5.ConstituencyId
															INNER JOIN County T6 ON T4.CountyId=T6.CountyId
															INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
										WHERE (
												CASE(@GeoUnit) WHEN @GeoCode_SUBLOCATION THEN T1.SubLocationId 
															   WHEN @GeoCode_LOCATION THEN T2.LocationId
															   WHEN @GeoCode_DIVISION THEN T3.DivisionId
															   WHEN @GeoCode_DISTRICT THEN T4.DistrictId
															   WHEN @GeoCode_CONSTITUENCY THEN T5.ConstituencyId
															   WHEN @GeoCode_COUNTY THEN T6.CountyId
															   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
												END
											  )=@GeoId
										) T2 ON T1.SubLocationId=T2.SubLocationId

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY,@GeoCode_CONSTITUENCY,@GeoCode_DISTRICT,@GeoCode_DIVISION,@GeoCode_LOCATION)
		DELETE T1
		--SELECT T1.*
		FROM Location T1 INNER JOIN (
										SELECT DISTINCT T2.*
										FROM Location T2 INNER JOIN Division T3 ON T2.DivisionId=T3.DivisionId
														 INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
														 INNER JOIN County T6 ON T4.CountyId=T6.CountyId
														 INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
										WHERE (
												CASE(@GeoUnit) WHEN @GeoCode_LOCATION THEN T2.LocationId
															   WHEN @GeoCode_DIVISION THEN T3.DivisionId
															   WHEN @GeoCode_DISTRICT THEN T4.DistrictId
															   WHEN @GeoCode_COUNTY THEN T6.CountyId
															   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
												END
											  )=@GeoId
									) T2 ON T1.LocationId=T2.LocationId

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY,@GeoCode_CONSTITUENCY,@GeoCode_DISTRICT,@GeoCode_DIVISION)
		DELETE T1
		--SELECT T1.*
		FROM Division T1 INNER JOIN (
										SELECT DISTINCT T3.*
										FROM Division T3 INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
														 INNER JOIN County T6 ON T4.CountyId=T6.CountyId
														 INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
										WHERE (
												CASE(@GeoUnit) WHEN @GeoCode_DIVISION THEN T3.DivisionId
															   WHEN @GeoCode_DISTRICT THEN T4.DistrictId
															   WHEN @GeoCode_COUNTY THEN T6.CountyId
															   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
												END
											  )=@GeoId
									) T2 ON T1.DivisionId=T2.DivisionId

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY,@GeoCode_CONSTITUENCY,@GeoCode_DISTRICT)
		DELETE T1
		--SELECT T1.*
		FROM District T1 INNER JOIN (
										SELECT DISTINCT T4.*
										FROM District T4 INNER JOIN County T6 ON T4.CountyId=T6.CountyId
														 INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
										WHERE (
												CASE(@GeoUnit) WHEN @GeoCode_DISTRICT THEN T4.DistrictId
															   WHEN @GeoCode_COUNTY THEN T6.CountyId
															   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
												END
											  )=@GeoId
									) T2 ON T1.DistrictId=T2.DistrictId

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY,@GeoCode_CONSTITUENCY)
		DELETE T1
		--SELECT T1.*
		FROM Constituency T1 INNER JOIN (
											SELECT DISTINCT T5.*
											FROM Constituency T5 INNER JOIN County T6 ON T5.CountyId=T6.CountyId
																 INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
											WHERE (
													CASE(@GeoUnit) WHEN @GeoCode_CONSTITUENCY THEN T5.ConstituencyId
																   WHEN @GeoCode_COUNTY THEN T6.CountyId
																   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
													END
												  )=@GeoId
										) T2 ON T1.ConstituencyId=T2.ConstituencyId

	IF @GeoUnit IN(@GeoCode_PROVINCE,@GeoCode_COUNTY)
		DELETE T1
		--SELECT T1.*
		FROM County T1 INNER JOIN (
											SELECT DISTINCT T6.*
											FROM County T6 INNER JOIN Province T7 ON T6.ProvinceId=T7.ProvinceId
											WHERE (
													CASE(@GeoUnit) WHEN @GeoCode_COUNTY THEN T6.CountyId
																   WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
													END
												  )=@GeoId
									) T2 ON T1.CountyId=T2.CountyId

	IF @GeoUnit IN(@GeoCode_PROVINCE)
		DELETE T1
		--SELECT T1.*
		FROM Province T1 INNER JOIN (
											SELECT DISTINCT T7.*
											FROM Province T7
											WHERE (
													CASE(@GeoUnit) WHEN @GeoCode_PROVINCE THEN T7.ProvinceId
													END
												  )=@GeoId
									) T2 ON T1.ProvinceId=T2.ProvinceId

	SELECT 1 AS NoOfRows
	
	EXEC GetGeoUnits @GeoUnit=@GeoUnit
END
GO




IF NOT OBJECT_ID('AddEditExpansionPlan') IS NULL	DROP PROC AddEditExpansionPlan
GO
CREATE PROC AddEditExpansionPlan
	@ExpansionPlanId int
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@ProgrammeId tinyint
   ,@LocationId int
   ,@PovertyHeadCountPerc float
   ,@ActiveHHs int
   ,@Scaleup_EqualShare int
   ,@Scaleup_PovertyPrioritized int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @ExpansionCodeExists bit
	DECLARE @ExpansionPlanExists bit
	DECLARE @NoOfRows int

	SELECT @ProgrammeId=ProgrammeId FROM Programme WHERE ProgrammeId=@ProgrammeId
	SELECT @LocationId=LocationId FROM Location WHERE LocationId=@LocationId
	SELECT @UserId=UserId FROM [User] WHERE UserId=@UserId
	SELECT @ExpansionCodeExists=COUNT(ExpansionPlanId) FROM ExpansionPlan WHERE ProgrammeId=@ProgrammeId AND Code=@Code
	SELECT @ExpansionPlanExists=COUNT(ExpansionPlanId) FROM ExpansionPlan WHERE ProgrammeId=@ProgrammeId AND LocationId=@LocationId
	
	IF ISNULL(@Code,'')=''
		SET @ErrorMsg='Please specify valid Code parameter'
	IF ISNULL(@Description,'')=''
		SET @ErrorMsg='Please specify valid Description parameter'
	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	IF ISNULL(@LocationId,0)=0
		SET @ErrorMsg='Please specify valid LocationId parameter'
	IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@ExpansionCodeExists,0)<>0 AND ISNULL(@ExpansionPlanId,0)=0
		SET @ErrorMsg='The Expansion Code for the programme already exists'
	IF ISNULL(@ExpansionPlanExists,0)<>0 AND ISNULL(@ExpansionPlanId,0)=0
		SET @ErrorMsg='The Expansion Plan for the location already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @ExpansionPlanId>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.ProgrammeId=@ProgrammeId
		   ,T1.LocationId=@LocationId
		   ,T1.PovertyHeadCountPerc=@PovertyHeadCountPerc
		   ,T1.EligibleHHs=@ActiveHHs
		   ,T1.Scaleup_EqualShare=@Scaleup_EqualShare
		   ,T1.Scaleup_PovertyPrioritized=@Scaleup_PovertyPrioritized
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ExpansionPlan T1
		WHERE T1.ExpansionPlanId=@ExpansionPlanId
	END
	ELSE
	BEGIN
		INSERT INTO ExpansionPlan(Code,[Description],ProgrammeId,LocationId,PovertyHeadCountPerc,EligibleHHs,Scaleup_EqualShare,Scaleup_PovertyPrioritized,CreatedBy,CreatedOn)
		SELECT @Code,@Description,@ProgrammeId,@LocationId,@PovertyHeadCountPerc,@ActiveHHs,@Scaleup_EqualShare,@Scaleup_PovertyPrioritized,@UserId,GETDATE()

		SET @ExpansionPlanId=IDENT_CURRENT('ExpansionPlan')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetExpansionPlans	
	END
END
GO



IF NOT OBJECT_ID('DeleteExpansionPlan') IS NULL	DROP PROC DeleteExpansionPlan
GO
CREATE PROC DeleteExpansionPlan
	@ExpansionPlanId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM ExpansionPlan T1
	WHERE T1.ExpansionPlanId=@ExpansionPlanId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetExpansionPlans
END
GO



IF NOT OBJECT_ID('AddEditGrievance') IS NULL	DROP PROC AddEditGrievance
GO
CREATE PROC AddEditGrievance
	@GrievanceId int=NULL
   ,@SerialNo varchar(20)
   ,@GrievanceLevelId int
   ,@GrievanceTypeId int
   ,@GrievanceDesc nvarchar(512)
   ,@ReportingDate datetime
   ,@ReportedTo nvarchar(50)
   ,@ReportingChannelId int
   ,@ReceiptDate datetime
   ,@ComplainantTypeId int
   ,@Names nvarchar(50)
   ,@SexId int
   ,@AgeBracketId int
   ,@IsBeneficiary bit
   ,@HHNo nvarchar(64)
   ,@CommunityName nvarchar(20)
   ,@CountyId int
   ,@ConstituencyId int
   ,@DistrictId int
   ,@DivisionId int
   ,@LocationId int
   ,@SubLocationId int
   ,@GrievanceActionsXML XML
   ,@UserId int
AS
BEGIN
	DECLARE @GrievanceAction TABLE(GrievanceActionId int NULL
								  ,GrievanceId int NOT NULL
								  ,GrievanceResolutionId int NOT NULL
								  ,ResolutionDesc nvarchar(512) NOT NULL
								  ,ResolutionDate datetime NOT NULL
								  ,FeedbackDate datetime NULL
								  ,IsNew bit
								  ,IsForDeletion bit
								)
	DECLARE @GrievanceResolutionCode varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @HHId int
	DECLARE @NoOfRows int

	SET @GrievanceResolutionCode='Grievance Resolution'
	SET @SerialNo=CASE WHEN(@SerialNo='') THEN NULL ELSE @SerialNo END
	SET @ReportingDate=ISNULL(@ReportingDate,GETDATE())
	SET @ReceiptDate=ISNULL(@ReceiptDate,GETDATE())
	SET @IsBeneficiary=ISNULL(@IsBeneficiary,0)
	SELECT @HHId=HHId FROM Household WHERE HHNo=@HHNo
	SET @CountyId=CASE WHEN(ISNULL(@CountyId,0)<=0) THEN NULL ELSE @CountyId END
	SET @ConstituencyId=CASE WHEN(ISNULL(@ConstituencyId,0)<=0) THEN NULL ELSE @ConstituencyId END
	SET @DistrictId=CASE WHEN(ISNULL(@DistrictId,0)<=0) THEN NULL ELSE @DistrictId END
	SET @DivisionId=CASE WHEN(ISNULL(@DivisionId,0)<=0) THEN NULL ELSE @DivisionId END
	SET @LocationId=CASE WHEN(ISNULL(@LocationId,0)<=0) THEN NULL ELSE @LocationId END
	SET @SubLocationId=CASE WHEN(ISNULL(@SubLocationId,0)<=0) THEN NULL ELSE @SubLocationId END

	IF ISNULL(@SerialNo,'')<>'' AND EXISTS(SELECT 1 FROM Grievance WHERE SerialNo=@SerialNo AND GrievanceId<>@GrievanceId)
		SET @ErrorMsg='The Serial No. is already associated with another grievance'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Grievance Level' AND T1.SystemCodeDetailId=@GrievanceLevelId)
		SET @ErrorMsg='Please specify valid GrievanceLevelId parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Grievance Type' AND T1.SystemCodeDetailId=@GrievanceTypeId)
		SET @ErrorMsg='Please specify valid GrievanceTypeId parameter'
	IF ISNULL(@GrievanceDesc,'')=''
		SET @ErrorMsg='Please specify valid GrievanceDesc parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Reporting Channel' AND T1.SystemCodeDetailId=@ReportingChannelId)
		SET @ErrorMsg='Please specify valid ReportingChannelId parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Complainant Type' AND T1.SystemCodeDetailId=@ComplainantTypeId)
		SET @ErrorMsg='Please specify valid ComplainantTypeId parameter'
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Sex' AND T1.SystemCodeDetailId=@SexId)
		SET @ErrorMsg='Please specify valid SexId parameter'
	IF ISNULL(@AgeBracketId,0)>0 AND NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId AND T2.Code='Age Bracket' AND T1.SystemCodeDetailId=@AgeBracketId)
		SET @ErrorMsg='Please specify valid AgeBracketId parameter'
	IF @HHId<=0
		SET @ErrorMsg='Please specify valid HHNo parameter'
	IF @CountyId>0 AND NOT EXISTS(SELECT 1 FROM County WHERE CountyId=@CountyId)
		SET @ErrorMsg='Please specify valid CountyId parameter'
	IF @ConstituencyId>0 AND NOT EXISTS(SELECT 1 FROM Constituency WHERE ConstituencyId=@ConstituencyId)
		SET @ErrorMsg='Please specify valid ConstituencyId parameter'
	IF @DistrictId>0 AND NOT EXISTS(SELECT 1 FROM District WHERE DistrictId=@DistrictId)
		SET @ErrorMsg='Please specify valid DistrictID parameter'
	IF @DivisionId>0 AND NOT EXISTS(SELECT 1 FROM Division WHERE DivisionId=@DivisionId)
		SET @ErrorMsg='Please specify valid DivisionId parameter'
	IF @LocationId>0 AND NOT EXISTS(SELECT 1 FROM Location WHERE LocationId=@LocationId)
		SET @ErrorMsg='Please specify valid LocationId parameter'
	IF @SubLocationId>0 AND NOT EXISTS(SELECT 1 FROM SubLocation WHERE SubLocationId=@SubLocationId)
		SET @ErrorMsg='Please specify valid SubLocationId parameter'
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE UserId=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END



	IF NOT @GrievanceActionsXML IS NULL
	BEGIN
		INSERT INTO @GrievanceAction(GrievanceActionId,GrievanceId,GrievanceResolutionId,ResolutionDesc,ResolutionDate,FeedbackDate,IsNew,IsForDeletion)
		SELECT T1.GrievanceActionId,@GrievanceId AS GrievanceId,T2.SystemCodeDetailId AS GrievanceResolutionId,ResolutionDesc,ResolutionDate,CASE WHEN(T1.FeedbackDate='') THEN NULL ELSE T1.FeedbackDate END AS FeedbackDate,IsNew,IsForDeletion
		FROM (	SELECT U.R.value('(GrievanceActionId)[1]','int') AS GrievanceActionId
					,U.R.value('(GrievanceId)[1]','int') AS GrievanceId
					,U.R.value('(GrievanceResolutionId)[1]','varchar(20)') AS GrievanceResolutionCode
					,U.R.value('(ResolutionDesc)[1]','nvarchar(512)') AS ResolutionDesc
					,U.R.value('(ResolutionDate)[1]','datetime') AS ResolutionDate
					,U.R.value('(FeedbackDate)[1]','nvarchar(30)') AS FeedbackDate
					,U.R.value('(IsNew)[1]','bit') AS IsNew
					,U.R.value('(IsForDeletion)[1]','bit') AS IsForDeletion
				FROM @GrievanceActionsXML.nodes('GrievanceActions/Record') AS U(R)
			) T1 INNER JOIN SystemCodeDetail T2 ON T1.GrievanceResolutionCode=T2.DetailCode
				 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.SystemCodeId AND T3.Code=@GrievanceResolutionCode
	END

	BEGIN TRAN

	IF @GrievanceId>0
	BEGIN
		UPDATE T1
		SET T1.SerialNo=@SerialNo
		   ,T1.GrievanceLevelId=@GrievanceLevelId
		   ,T1.GrievanceTypeId=@GrievanceTypeId
		   ,T1.GrievanceDesc=@GrievanceDesc
		   ,T1.ReportingDate=@ReportingDate
		   ,T1.ReportedTo=@ReportedTo
		   ,T1.ReportingChannelId=@ReportingChannelId
		   ,T1.ReceiptDate=@ReceiptDate
		   ,T1.ComplainantTypeId=@ComplainantTypeId
		   ,T1.Names=@Names
		   ,T1.SexId=@SexId
		   ,T1.AgeBracketId=@AgeBracketId
		   ,T1.IsBeneficiary=@IsBeneficiary
		   ,T1.HHId=@HHId
		   ,T1.CommunityName=@CommunityName
		   ,T1.CountyId=@CountyId
		   ,T1.ConstituencyId=@ConstituencyId
		   ,T1.DistrictId=@DistrictId
		   ,T1.DivisionId=@DivisionId
		   ,T1.LocationId=@LocationId
		   ,T1.SubLocationId=@SubLocationId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Grievance T1
		WHERE T1.GrievanceId=@GrievanceId
	END
	ELSE
	BEGIN
		INSERT INTO Grievance(SerialNo,GrievanceLevelId,GrievanceTypeId,GrievanceDesc,ReportingDate,ReportedTo,ReportingChannelId,ReceiptDate,ComplainantTypeId,Names,SexId,AgeBracketId,IsBeneficiary,HHId,CommunityName,CountyId,ConstituencyId,DistrictId,DivisionId,LocationId,SubLocationId,CreatedBy,CreatedOn)
		SELECT @SerialNo,@GrievanceLevelId,@GrievanceTypeId,@GrievanceDesc,@ReportingDate,@ReportedTo,@ReportingChannelId,@ReceiptDate,@ComplainantTypeId,@Names,@SexId,@AgeBracketId,@IsBeneficiary,@HHId,@CommunityName,@CountyId,@ConstituencyId,@DistrictId,@DivisionId,@LocationId,@SubLocationId,@UserId,GETDATE()

		SET @GrievanceId=IDENT_CURRENT('Grievance')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	UPDATE T1
	SET T1.DeletedBy=@UserId
	   ,T1.DeletedOn=GETDATE()
	   ,T1.ModifiedBy=@UserId
	   ,T1.ModifiedOn=GETDATE()
	FROM GrievanceAction T1 INNER JOIN @GrievanceAction T2 ON T1.GrievanceActionId=T2.GrievanceActionId
	WHERE T2.IsForDeletion=1

	UPDATE T1
	SET T1.GrievanceResolutionId=T2.GrievanceResolutionId
	   ,T1.ResolutionDesc=T2.ResolutionDesc
	   ,T1.ResolutionDate=T2.ResolutionDate
	   ,T1.FeedbackDate=T2.FeedbackDate
	   ,T1.ModifiedBy=@UserId
	   ,T1.ModifiedOn=GETDATE()
	FROM GrievanceAction T1 INNER JOIN @GrievanceAction T2 ON T1.GrievanceActionId=T2.GrievanceActionId
	WHERE ISNULL(T2.IsForDeletion,0)=0 AND ISNULL(T2.IsNew,0)=0

	INSERT INTO GrievanceAction(GrievanceId,GrievanceResolutionId,ResolutionDesc,ResolutionDate,FeedbackDate,CreatedBy,CreatedOn)
	SELECT T1.GrievanceId,T1.GrievanceResolutionId,T1.ResolutionDesc,T1.ResolutionDate,T1.FeedbackDate,@UserId,GETDATE()
	FROM @GrievanceAction T1
	WHERE T1.IsNew=1

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		EXEC GetGrievances	
	END
END
GO



IF NOT OBJECT_ID('DeleteGrievance') IS NULL	DROP PROC DeleteGrievance
GO
CREATE PROC DeleteGrievance
	@GrievanceId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	DELETE T1
	FROM GrievanceAction T1
	WHERE T1.GrievanceId=@GrievanceId;

	DELETE T1
	FROM Grievance T1
	WHERE T1.GrievanceId=@GrievanceId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetGrievances		
END
GO



IF NOT OBJECT_ID('AddEditDBBackup') IS NULL	DROP PROC AddEditDBBackup
GO
CREATE PROC AddEditDBBackup
	@DBName nvarchar(30)
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @SQLStmt nvarchar(4000)
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(5)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE UserId=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CONVERT(char(2),DATEPART(hour,GETDATE()))+CONVERT(char(2),DATEPART(minute,GETDATE()))
	SET @FilePath=@FilePath+@DBName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time+'.bak'

	SET @SQLStmt='BACKUP DATABASE ['+@DBName+'] TO  DISK = N'''+@FilePath+''' WITH NOFORMAT, NOINIT,  NAME = N'''+@FilePath+''', SKIP, NOREWIND, NOUNLOAD,  STATS = 10'

	EXEC (@SQLStmt)

	IF @@ERROR>0
	BEGIN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		INSERT INTO DBBackup(FilePath,CreatedBy,CreatedOn)
		SELECT @FilePath,@UserId,GETDATE()

		SELECT 1 AS NoOfRows

		EXEC GetDBBackups	
	END


END
GO



