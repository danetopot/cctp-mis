/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB TABLES TO HOLD DATA IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 1ST MARCH, 2018
    UPDATED ON: 9TH MARCH, 2018
	
	NOTE: IT REQUIRES AN EXISTING DATABASE	
*/




IF NOT OBJECT_ID('GetBeneForEnrolment') IS NULL	DROP PROC GetBeneForEnrolment
GO
CREATE PROC GetBeneForEnrolment
	@ProgrammeId int
   ,@RegGroupId int
AS
BEGIN
	--TO DO LIST: 1. INCORPORATE WAITING LIST VALIDITY PERIOD!
	DECLARE @tbl_EnrolmentHhAnalysis TABLE(
		LocationId int NOT NULL
	   ,PovertyPerc float NOT NULL DEFAULT(0.00)
	   ,RegGroupHhs int NOT NULL DEFAULT(0)
	   ,BeneHhs int NOT NULL DEFAULT(0)
	   ,ExpPlanEqualShare int NOT NULL DEFAULT(0)
	   ,ExpPlanPovertyPrioritized int NOT NULL DEFAULT(0)
	)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode1 varchar(20)
	DECLARE @SysDetailCode2 varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE ProgrammeId=@ProgrammeId AND ApvBy IS NULL)
		SET @ErrorMsg='There is an existing enrolment batch for the programme which requires to be approved first'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='System Settings'
	SET @SysDetailCode1='CURFINYEAR'
	SET @SysDetailCode2='WAITLISTVALIDITYMONTHS'
	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1
	SELECT @SystemCodeDetailId2=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2

	SET @SysCode='HHStatus'
	SET @SysDetailCode1='VALPASS'
	SET @SysDetailCode2='PSPCARDED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2

	SELECT SUM(T1.RegGroupHhs) AS RegGroupHhs,SUM(T1.BeneHhs) AS BeneHhs,SUM(ScaleupEqualShare) AS ScaleupEqualShare,SUM(ScaleupPovertyPrioritized) AS ScaleupPovertyPrioritized
	FROM (
			SELECT T1.LocationId,T1.RegGroupHhs,ISNULL(T2.BeneHhs,0) AS BeneHhs,ISNULL(T3.PovertyHeadCountPerc,0.00) AS PovertyPerc,ISNULL(T3.ScaleupEqualShare,0) AS ScaleupEqualShare,ISNULL(T3.ScaleupPovertyPrioritized,0) AS ScaleupPovertyPrioritized
			FROM (
					SELECT T4.LocationId,COUNT(T1.Id) AS RegGroupHhs
					FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
									  INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
									  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
												 ) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId										 					  
					WHERE T1.ProgrammeId=@ProgrammeId AND T1.RegGroupId=@RegGroupId AND T1.StatusId=@SystemCodeDetailId3 --EVALUATE THE WAITING LIST VALIDITY PERIOD HERE!!! 
					GROUP BY T4.LocationId
				) T1 LEFT JOIN (
									SELECT T4.LocationId,COUNT(T1.Id) AS BeneHhs
									FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
														INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
														INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																						INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																						INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																						INNER JOIN District T5 ON T4.DistrictId=T5.Id
																						INNER JOIN County T6 ON T4.CountyId=T6.Id
																						INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																						INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																	) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId					  
									WHERE T1.StatusId=@SystemCodeDetailId4
									GROUP BY T4.LocationId
								) T2 ON T1.LocationId=T2.LocationId 
					 LEFT JOIN (
									SELECT T2.LocationId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
									FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
																INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
																INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																								INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																								INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																								INNER JOIN District T5 ON T4.DistrictId=T5.Id
																								INNER JOIN County T6 ON T4.CountyId=T6.Id
																								INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																								INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																			WHERE T8.IsDefault=1
																			) T4 ON T2.LocationId=T4.LocationId	
									WHERE T3.ProgrammeId=@ProgrammeId AND T1.FinancialYearId=@SystemCodeDetailId1
								) T3 ON T1.LocationId=T3.LocationId
			) T1
END
GO
--EXEC GetBeneForEnrolment @ProgrammeId=1,@RegGroupId=99



IF NOT OBJECT_ID('BeneEnrolmentStatus') IS NULL	DROP PROC BeneEnrolmentStatus
GO
CREATE PROC BeneEnrolmentStatus
	@NationalIdNo varchar(30)
   ,@UserId int
AS
BEGIN
	DECLARE @BeneAccountId int
	DECLARE @HhEnrolId int
	DECLARE @PSPCode varchar(30)
	DECLARE @PSPUserId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@NationalIdNo,'')=''
		SET @ErrorMsg='Please specify valid NationalIdNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T6.Id,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','ENRLPSP','PSPCARDED','ONPAY'))
							   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id
							   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1 AND T5.PrimaryRecipientId=T3.MemberRoleId
							   INNER JOIN Person T4 ON T3.PersonId=T4.Id 
							   LEFT JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
							   LEFT JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
							   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
							   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	WHERE T4.NationalIdNo=@NationalIdNo 

	IF ISNULL(@BeneAccountId,'')<>''
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
	ELSE IF ISNULL(@PSPUserId,0)>0 AND @PSPUserId<>@UserId
		SET @ErrorMsg=@PSPCode
	ELSE IF ISNULL(@HhEnrolId,0)=0
		SET @ErrorMsg='The beneficiary appears not to have been selected for enrolment into the programme'
	ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@NationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	ELSE IF EXISTS(SELECT 1 FROM [70PlusRegistration] WHERE OP_CONFIRM_ID_NO=@NationalIdNo AND RefId>523449)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	ELSE
		SELECT 0 AS StatusId,'The beneficiary is available for enrolment' AS [Description]	
END	
GO


--*
IF NOT OBJECT_ID('GetUserProfile') IS NULL	DROP PROC GetUserProfile
GO
CREATE PROCEDURE [dbo].[GetUserProfile]
    @UserId  int
 AS
   BEGIN  

      select u.FirstName, 
      u.MiddleName, 
      u.Surname, 
      u.Avatar,
       u.Email,
        u.MobileNo,
        u.UserGroupId
       from    [User] u 
        where u.Id = @UserId

     END

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetUserGroupRights]
    @UserId  int
 AS
   BEGIN  

   
     select UPPER( concat(m.Name,':', mr.[DESCRIPTION]) ) from Groupright r 
     inner join  ModuleRight mr on mr.Id = r.ModuleRightId
     inner join SystemCodeDetail scd on scd.Id = mr.RightId
     inner join UserGroup ug on ug.Id = r.UserGroupId
     inner join Module m on m.Id = mr.ModuleId
     inner join   dbo.[User]  u on u.UserGroupId = ug.Id
     and u.Id = @UserId

     END


GO



IF NOT OBJECT_ID('GetEnrolmentPspFiles') IS NULL	DROP PROC GetEnrolmentPspFiles
GO
CREATE PROCEDURE [dbo].[GetEnrolmentPspFiles]
     @UserId int
 AS
 BEGIN
     select 
     fc.Id, fc.[Name], fc.FilePath, fc.FileChecksum, fc.CreatedOn, fc.CreatedBy, fc.CreationTypeId, fc.TypeId,
     fd.Id as FileDownloadId, fd.FileCreationId, fd.FileChecksum as FileChecksum2 , fd.DownloadedBy, fd.DownloadedOn
     from FileCreation fc left outer  join FileDownload fd on fc.Id = fd.FileCreationId and fd.DownloadedBy = @UserId
						  INNER JOIN HouseholdEnrolmentPlan T3 ON fc.Id=T3.FileCreationId
	 WHERE fc.IsShared=1
     order by fc.Id desc
     END

GO



IF NOT OBJECT_ID('GetPaymentCycleOptions') IS NULL	DROP PROC GetPaymentCycleOptions
GO
CREATE PROC [dbo].[GetPaymentCycleOptions]
AS
BEGIN
    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)

    DECLARE @FromMonthId int
    DECLARE @ToMonthId int
    DECLARE @CurFromMonthId int
    DECLARE @CurToMonthId int

    DECLARE @FinancialYearId int
    DECLARE @FinancialYear varchar(30)
    DECLARE @FromMonth  varchar(30)
    DECLARE @ToMonth  varchar(30)

    IF EXISTS(	SELECT 1
    FROM PaymentCycle
    where StatusId
				    IN(SELECT T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
    WHERE T2.Code='Payment Status' AND T1.Code='PAYMENTOPEN')
			)
	BEGIN
        RAISERROR('There seems  to be having a running payment cycle which needs to be concluded first',16,1)
        RETURN
    END
	ELSE
	BEGIN




        SELECT @FromMonthId=T2.Id, @CurFromMonthId = CAST( T2.Code AS INT)
			  , @ToMonthId=T3.Id, @CurToMonthId = CAST( T3.Code AS INT)
              , @FinancialYearId = T1.FinancialYearId



        FROM PaymentCycle T1
            INNER JOIN SystemCodeDetail T2 ON T1.FromMonthId=T2.Id
            INNER JOIN SystemCodeDetail T3 ON T1.ToMonthId=T3.Id
            INNER JOIN SystemCodeDetail T4 ON T1.StatusId=T4.Id
        WHERE T1.Id=(SELECT MAX(Id)
        FROM PaymentCycle)


        PRINT @CurFromMonthId

        PRINT @CurToMonthId

        IF(ISNULL(@FinancialYearId,'' )='')

        SELECT @FinancialYearId =  T1.Id
        FROM SystemCodeDetail T1
        WHERE T1.Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

		IF EXISTS(SELECT 1 FROM PaymentCycle)
		BEGIN
			SELECT @FromMonthId=T3.Code+1
				  ,@ToMonthId=(T4.Code)+2 --BY DEFAULT THE PAYMENT CYCLES ARE BI-MONTHLY. ADDITIONAL LOGIC SHOULD TAKE CARE OF PROGRAMMES WITH VARYING FREQUENCIES
			FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T3 ON T1.FromMonthId=T3.Id
								 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
			WHERE T1.Id=(SELECT MAX(Id) FROM PaymentCycle)
		
			SELECT @FromMonthId=T1.Id,@FromMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CONVERT(varchar(30),CASE WHEN(@FromMonthId>12) THEN @FromMonthId-12 ELSE @FromMonthId END)
			SELECT @ToMonthId=T1.Id, @ToMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CONVERT(varchar(30),CASE WHEN(@ToMonthId>12) THEN @ToMonthId-12 ELSE @ToMonthId END)
		END
        SELECT @FinancialYear = [Description]
        FROM SystemCodeDetail
        WHERE Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

        SELECT @FromMonthId AS 'FromMonthId', @ToMonthId as 'ToMonthId', @FromMonth as 'FromMonth' , @ToMonth as 'ToMonth', @FinancialYearId as 'FinancialYearId', @FinancialYear as 'FinancialYear'


    END


END
GO



IF NOT OBJECT_ID('GetPrepayrollPaymentCycles') IS NULL	DROP PROC GetPrepayrollPaymentCycles
GO
CREATE proc [dbo].[GetPrepayrollPaymentCycles] (@Code VARCHAR(50) = NULL)

as
BEGIN
 
select CAST(PD.PaymentCycleId AS VARCHAR(5)) + '-'+CAST(PD.ProgrammeId AS VARCHAR(5))  AS "ID", 
-- P.*,F.Code, F.Code as 'FinancialYear',FM.Code 'FromMonth', TM.Code 'ToMonth', PR.Name 'Programme', PR.EntitlementAmount
F.Code +' '+ FM.[Description] +' '+ TM.[Description] + ' ' + PR.Name as 'Name'
 

 from PaymentCycleDetail PD    
INNER JOIN PaymentCycle P ON P.Id = PD.PaymentCycleId
INNER JOIN SystemCodeDetail F ON P.FinancialYearId = F.Id
INNER JOIN SystemCodeDetail FM ON P.FromMonthId = FM.Id
INNER JOIN SystemCodeDetail TM ON P.ToMonthId = TM.Id
INNER JOIN SystemCodeDetail PS ON PD.PaymentStageId = PS.Id AND PS.Code = @Code
INNER JOIN Programme PR ON PD.ProgrammeId = PR.Id




END

GO



IF NOT OBJECT_ID('GetPaymentCyclesDueForFundsRequest') IS NULL	DROP PROC GetPaymentCyclesDueForFundsRequest
GO
 CREATE PROC [dbo].[GetPaymentCyclesDueForFundsRequest]
	 
AS
BEGIN

	DECLARE @SysCode2 varchar(30)
	DECLARE @SysDetailCode2 varchar(30)
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @SystemCodeDetailId2 int



	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    SET @SysCode2='Payment Stage'
	SET @SysDetailCode2='FUNDSREQUEST'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode2 AND T1.Code=@SysDetailCode2



	SELECT  PC.ID, FY.[Description] +' ' + FM.[Description] +' ' + TM.[Description] AS  [Name]  FROM PaymentCycle PC
	LEFT JOIN SystemCodeDetail FM ON FM.Id = PC.FromMonthId
	LEFT JOIN SystemCodeDetail TM ON TM.Id = PC.ToMonthId
	LEFT JOIN SystemCodeDetail FY ON PC.FinancialYearId = FY.Id
	 WHERE PC.StatusId = @SystemCodeDetailId1 AND 
	PC.Id NOT IN (SELECT Id FROM FundsRequest) AND 
    PC.Id   IN (SELECT  PaymentCycleId FROM  PaymentCycleDetail WHERE PaymentStageId =@SystemCodeDetailId2  ) 
    	 
END
GO



IF NOT OBJECT_ID('GetIndividualHHEnrolmentDetails') IS NULL	DROP PROC GetIndividualHHEnrolmentDetails
GO
CREATE PROCEDURE  [dbo].[GetIndividualHHEnrolmentDetails]
    @HHenrolmentPlanId   int  = null
     
AS
   BEGIN 

 	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    SET @SysCode='Card Status'
	SET @SysDetailCode='-1'
	
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SELECT   T2.Id AS EnrolmentNo
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,BPC.PriReciFirstName AS BeneFirstName
		,BPC.PriReciMiddleName AS BeneMiddleName
		,BPC.PriReciSurname AS BeneSurname
		,BPC.PriReciNationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,BPC.PriReciDoB AS BeneDoB
		,ISNULL(BPC.SecReciFirstName,'') AS CGFirstName
		,ISNULL(BPC.SecReciMiddleName,'') AS CGMiddleName
		,ISNULL(BPC.SecReciSurname,'') AS CGSurname
		,ISNULL(BPC.SecReciNationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(BPC.SecReciDoB,'') AS CGDoB
		,ISNULL(BPC.MobileNo1,'') AS MobileNo1
		,ISNULL(BPC.MobileNo2,'') AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,P.[Name] AS 'BANK'
		,PB.[NAME] AS 'BankBranch'
		,CASE WHEN BPC.PaymentCardNo IS NULL THEN 'Account Opened' ELSE 'Account Opened,Payment Card Issued' END  AS 'Status'


	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
                                     INNER JOIN BeneficiaryAccount BA ON BA.HhEnrolmentId = T2.Id AND BA.StatusId=@SystemCodeDetailId1
                                     INNER JOIN PSPBranch PB ON BA.PSPBranchId = PB.Id
									 INNER JOIN PSP P ON PB.PSPId = P.ID
									 INNER JOIN BeneficiaryPaymentCard BPC ON BPC.BeneAccountId = BA.Id AND BPC.StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
								     INNER JOIN SystemCodeDetail T7 ON BPC.PriReciSexId=T7.Id
								     LEFT  JOIN SystemCodeDetail T10 ON BPC.SecReciSexId=T10.Id
  WHERE T1.Id=ISNULL(@HHenrolmentPlanId,0)

END
GO


--*


IF NOT OBJECT_ID('GetAllEnrolmentPlan') IS NULL	DROP PROC GetAllEnrolmentPlan
GO
CREATE PROC GetAllEnrolmentPlan
	@EnrolmentPlanId int=NULL
AS
BEGIN
	SELECT T1.Id,T2.Code AS Programme,T3.Code AS RegGroup,T1.RegGroupHhs,T1.BeneHhs,T1.ExpPlanEqualShare,T1.ExpPlanPovertyPrioritized,T1.EnrolmentNumbers,T4.Code AS EnrolmentGroup,T5.[Description] AS [Status]
	FROM HouseholdEnrolmentPlan T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
								   INNER JOIN SystemCodeDetail T3 ON T1.RegGroupId=T3.Id
								   INNER JOIN SystemCodeDetail T4 ON T1.EnrolmentGroupId=T4.Id
								   INNER JOIN SystemCodeDetail T5 ON T1.StatusId=T5.Id
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)

	SELECT T3.PSP,SUM(ISNULL(T3.AccountOpenedHhs,0)) AS AccountOpenedHhs,SUM(ISNULL(T3.CardedHhs,0)) AS CardedHhs
	FROM HouseholdEnrolmentPlan T1 LEFT JOIN (
												SELECT HhEnrolmentPlanId,COUNT(Id) AS EnrolledHhs
												FROM HouseholdEnrolment
												GROUP BY HhEnrolmentPlanId
											) T2 ON T1.Id=T2.HhEnrolmentPlanId
								   LEFT JOIN(
												SELECT T1.EnrolmentPlanId,T1.PSP,T1.AccountOpenedHhs,T2.CardedHhs
												FROM (
														SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.BeneAccountId) AS AccountOpenedHhs
														FROM (
																SELECT DISTINCT T3.Id AS EnrolmentPlanId,T1.Id AS BeneAccountId,T5.Id AS PSPId,T5.Name AS PSP
																FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
																						   INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
																						   INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id
																						   INNER JOIN PSP T5 ON T4.PSPId=T5.Id
															) T1
														GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
													) T1 LEFT JOIN (
																		SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.PaymentCard) AS CardedHhs
																		FROM (
																				SELECT DISTINCT T4.Id AS EnrolmentPlanId,T1.PriReciId AS PaymentCard,T6.Id AS PSPId,T6.Name AS PSP
																				FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
																											   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
																											   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id
																											   INNER JOIN PSPBranch T5 ON T2.PSPBranchId=T5.Id
																											   INNER JOIN PSP T6 ON T5.PSPId=T6.Id
																				WHERE ISNULL(T1.PaymentCardNo,'')<>''
																			) T1
																		GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
																	) T2 ON T1.EnrolmentPlanId=T2.EnrolmentPlanId AND T1.PSPId=T2.PSPId
											) T3 ON T1.Id=T3.EnrolmentPlanId
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)	
	GROUP BY T3.PSP							  
END
GO


IF NOT OBJECT_ID('GetPaymentCycleDetailOptions') IS NULL DROP PROC GetPaymentCycleDetailOptions
GO
CREATE PROC GetPaymentCycleDetailOptions
	@PaymentCycleId int
   ,@ProgrammeId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @EntitlementAmount money


	IF EXISTS(	SELECT 1
				FROM PaymentCycleDetail
				WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
					AND PaymentStageId NOT IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='Payment Stage' AND T1.Code='CLOSED')
			)
	BEGIN
		RAISERROR('The specified programme seems to be having a running payment cycle which needs to be conluded first',16,1)
		RETURN
	END

	
	SELECT T1.EntitlementAmount
	FROM Programme T1
	WHERE T1.Id=@ProgrammeId

	SET @SysCode='Enrolment Group'
	SELECT @EntitlementAmount=EntitlementAmount FROM Programme WHERE Id=@ProgrammeId

	SELECT DISTINCT T1.Id AS EnrolmentGroupId,T1.Code AS EnrolmentGroup,@EntitlementAmount AS EntitlementAmount
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
							 INNER JOIN HouseholdEnrolmentPlan T3 ON T1.Id=T3.EnrolmentGroupId AND T3.ApvBy IS NOT NULL
	WHERE T2.Code=@SysCode AND T3.ProgrammeId=@ProgrammeId
END
GO



IF NOT OBJECT_ID('GetPrepayrollAudit') IS NULL	DROP PROC GetPrepayrollAudit
GO
IF NOT OBJECT_ID('GetPrepayrollSummary') IS NULL	DROP PROC GetPrepayrollSummary
GO
CREATE PROC GetPrepayrollSummary
	@PaymentCycleId int
   ,@ProgrammeId tinyint
AS
BEGIN
	SELECT MAX(T2.ExceptionsFileId) AS ExceptionsFileId,MAX(T2.EnrolledHHs) AS EnroledHhs,COUNT(T1.HhId) AS EnrolmentGroupHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN 1 ELSE 0 END) AS PayrollHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.EntitlementAmount,0) ELSE 0 END) AS PayrollEntitlementAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.AdjustmentAmount,0) ELSE 0 END) AS PayrollAdjustmentAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN (ISNULL(T1.EntitlementAmount,0)+ISNULL(T1.AdjustmentAmount,0)) ELSE 0 END) AS PayrollAmount
		--EXCEPTION:INVALID NATIONAL ID
		,SUM(CASE WHEN(TT3.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidNationalIDHhs
		,SUM(CASE WHEN(TA3.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidNationalIDHhs
		--EXCEPTION:DUPLICATE NATIONAL ID
		,SUM(CASE WHEN(TT4.PaymentCycleId>0) THEN 1 ELSE 0 END) AS DuplicateNationalIDHhs
		,SUM(CASE WHEN(TA4.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedDuplicateNationalIDHhs
		--EXCEPTION:INVALID PAYMENT ACCOUNT
		,SUM(CASE WHEN(TT5.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidPaymentAccountHhs
		,SUM(CASE WHEN(TA5.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidPaymentAccountHhs
		--EXCEPTION:INVALID PAYMENT CARD
		,SUM(CASE WHEN(TT6.PaymentCycleId>0) THEN 1 ELSE 0 END) AS InvalidPaymentCardHhs
		,SUM(CASE WHEN(TA6.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedInvalidPaymentCardHhs
		--EXCEPTION:INELIGIBLE
		,SUM(CASE WHEN(TT7.PaymentCycleId>0) THEN 1 ELSE 0 END) AS IneligibleHhs
		,SUM(CASE WHEN(TA7.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedIneligibleHhs
		--EXCEPTION:SUSPICIOUS PAYMENT
		,SUM(CASE WHEN(TT8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS SuspiciousPaymentHhs
		,SUM(CASE WHEN(TA8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS ActionedSuspiciousPaymentHhs
		,MAX(CASE WHEN(TA3.PaymentCycleId>0
					   OR TA4.PaymentCycleId>0
					   OR TA5.PaymentCycleId>0
					   OR TA6.PaymentCycleId>0
					   OR TA7.PaymentCycleId>0
					   OR TA8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS PayrollHasBeenActioned
	FROM Prepayroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
					   LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy IS NULL) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HHId
							LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID) TT3 ON T1.PaymentCycleId=TT3.PaymentCycleId AND T1.ProgrammeId=TT3.ProgrammeId AND T1.HhId=TT3.HHId
								LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) TA3 ON T1.PaymentCycleId=TA3.PaymentCycleId AND T1.ProgrammeId=TA3.ProgrammeId AND T1.HhId=TA3.HHId
					   
					   LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy IS NULL) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HHId
							LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID) TT4 ON T1.PaymentCycleId=TT4.PaymentCycleId AND T1.ProgrammeId=TT4.ProgrammeId AND T1.HhId=TT4.HHId
								LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) TA4 ON T1.PaymentCycleId=TA4.PaymentCycleId AND T1.ProgrammeId=TA4.ProgrammeId AND T1.HhId=TA4.HHId
					   
					   LEFT JOIN PrepayrollInvalidPaymentAccount T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId AND T5.ActionedBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentAccount TT5 ON T1.PaymentCycleId=TT5.PaymentCycleId AND T1.ProgrammeId=TT5.ProgrammeId AND T1.HhId=TT5.HhId
								LEFT JOIN PrepayrollInvalidPaymentAccount TA5 ON T1.PaymentCycleId=TA5.PaymentCycleId AND T1.ProgrammeId=TA5.ProgrammeId AND T1.HhId=TA5.HhId AND TA5.ActionedBy>0
					   
					   LEFT JOIN PrepayrollInvalidPaymentCard T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId AND T6.ActionedBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentCard TT6 ON T1.PaymentCycleId=TT6.PaymentCycleId AND T1.ProgrammeId=TT6.ProgrammeId AND T1.HhId=TT6.HhId
								LEFT JOIN PrepayrollInvalidPaymentCard TA6 ON T1.PaymentCycleId=TA6.PaymentCycleId AND T1.ProgrammeId=TA6.ProgrammeId AND T1.HhId=TA6.HhId AND TA6.ActionedBy>0
					   
					   LEFT JOIN PrepayrollIneligible T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId AND T7.ActionedBy IS NULL
							LEFT JOIN PrepayrollIneligible TT7 ON T1.PaymentCycleId=TT7.PaymentCycleId AND T1.ProgrammeId=TT7.ProgrammeId AND T1.HhId=TT7.HhId
								LEFT JOIN PrepayrollIneligible TA7 ON T1.PaymentCycleId=TA7.PaymentCycleId AND T1.ProgrammeId=TA7.ProgrammeId AND T1.HhId=TA7.HhId AND TA7.ActionedBy>0
					   
					   LEFT JOIN PrepayrollSuspicious T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.HhId=T8.HhId AND T8.ActionedBy IS NULL
							LEFT JOIN PrepayrollSuspicious TT8 ON T1.PaymentCycleId=TT8.PaymentCycleId AND T1.ProgrammeId=TT8.ProgrammeId AND T1.HhId=TT8.HhId
								LEFT JOIN PrepayrollSuspicious TA8 ON T1.PaymentCycleId=TA8.PaymentCycleId AND T1.ProgrammeId=TA8.ProgrammeId AND T1.HhId=TA8.HhId AND TA8.ActionedBy>0
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
END
GO



IF NOT OBJECT_ID('GetRequestForFunds') IS NULL	DROP PROC GetRequestForFunds
GO
CREATE PROC GetRequestForFunds
	@PaymentCycleId int
AS
BEGIN
	SELECT CONVERT(int,ROW_NUMBER() OVER(ORDER BY T3.Code)) AS [No],T3.Code AS Programme,T4.Name AS PSP,SUM(T2.PayrollHhs) AS NoOfHouseholds,SUM(T2.EntitlementAmount+T2.OtherAmount) AS Amount,SUM(T2.CommissionAmount) AS Commission,SUM(T2.EntitlementAmount+T2.OtherAmount+T2.CommissionAmount) AS Total
	FROM FundsRequest T1 INNER JOIN FundsRequestDetail T2 ON T1.Id=T2.FundsRequestId
						 INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id
						 INNER JOIN PSP T4 ON T2.PSPId=T4.Id
	WHERE T1.PaymentCycleId=@PaymentCycleId
	GROUP BY T3.Code,T4.Name
END
GO


IF NOT OBJECT_ID('GetExceptionType') IS NULL	DROP PROC GetExceptionType
GO
IF NOT OBJECT_ID('GetActionableExceptionTypes') IS NULL	DROP PROC GetActionableExceptionTypes
GO
CREATE PROC GetActionableExceptionTypes
	@Category varchar(20)
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysCodeId int
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)

	SET @SysCode='Exception Type'
	SELECT @SysCodeId=Id FROM SystemCode WHERE Code=@SysCode
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'

	IF @Category='INVALIDID'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID)
	END

	IF @Category='DUPLICATEID'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC)
	END

	--IF @Category='INVALIDACC'
	--BEGIN
	--	SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDACC)
	--END

	IF @Category='INVALIDCARD'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDCARD)
	END

	IF @Category='SUSPICIOUS'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY)
	END

	IF @Category='INELIGIBLE'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INELIGIBLESUS)
	END


END
GO



IF NOT OBJECT_ID('GetPayrollSummary') IS NULL	DROP PROC GetPayrollSummary
GO
CREATE PROC GetPayrollSummary
	@PaymentCycleId int
   ,@ProgrammeId tinyint
AS
BEGIN
	SELECT COUNT(T1.HhId) AS TotalPayrollHhs
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollExceptionsHhs
		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidIDHhs
		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PayrollDuplicateIDHhs
		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentCardHhs
		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PayrollIneligibleHhs
		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollSuspiciousHhs
		,SUM(T1.PaymentAmount) AS TotalPayrollAmount
	FROM Payroll T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
END
GO



IF NOT OBJECT_ID('GetPayrollTrxSummary') IS NULL	DROP PROC GetPayrollTrxSummary
GO
CREATE PROC GetPayrollTrxSummary
	@PaymentCycleId int
AS
BEGIN
	SELECT T6.Name AS PSP,COUNT(T1.HhId) AS OnPayroll,COUNT(T2.HhId) AS ProcessedPayment,SUM(CASE WHEN(ISNULL(T2.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments,SUM(CASE WHEN(T2.WasTrxSuccessful=0) THEN 1 ELSE 0 END) AS FailedPayments,COUNT(T1.HhId)-COUNT(T2.HhId) AS PendingPayments
	FROM Payroll T1 LEFT JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T3.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
	WHERE T1.PaymentCycleId=@PaymentcycleId
	GROUP BY T6.Code,T6.Name
END
GO



IF NOT OBJECT_ID('GetFailedPayrollTrx') IS NULL	DROP PROC GetFailedPayrollTrx
GO
CREATE PROC GetFailedPayrollTrx
	@PaymentCycleId int
AS
BEGIN
	SELECT T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T7.Id AS EnrolmentNo,T6.Name AS Bank,T5.Name AS Branch
		,T4.AccountNo,T4.AccountName,T1.PaymentAmount,T2.TrxNarration AS FailedReason
	FROM Payroll T1 LEFT JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T3.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentcycleId AND ISNULL(T2.WasTrxSuccessful,0)=0
END
GO



IF NOT OBJECT_ID('GetPostPayrollSummary') IS NULL	DROP PROC GetPostPayrollSummary
GO
CREATE PROC GetPostPayrollSummary
	@PaymentCycleId int
AS
BEGIN
	SELECT COUNT(T1.HhId) AS TotalPaymentHhs
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 1 ELSE 0 END) AS FailedPayments
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentExceptionsHhs
		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs
		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs
		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentCardHhs
		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PaymentIneligibleHhs
		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentSuspiciousHhs
		,SUM(T1.TrxAmount) AS TotalPaymentAmount
	FROM Payment T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId
END
GO



IF NOT OBJECT_ID('GetBeneAccountMonthlyActivitySummary') IS NULL	DROP PROC GetBeneAccountMonthlyActivitySummary
GO
CREATE PROC GetBeneAccountMonthlyActivitySummary
	@MonthNo tinyint
   ,@Year int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int

	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T3.Code AS PSPCode,COUNT(T1.Id) AS TotalAccounts
		,SUM(CASE WHEN(T4.BeneAccountId>0) THEN 1 ELSE 0 END) AS ReportedAccounts
		,COUNT(T1.Id)-SUM(CASE WHEN(T4.BeneAccountId>0) THEN 1 ELSE 0 END) AS PendingAccounts
		,SUM(CASE WHEN(T4.HadUniqueWdl<>0) THEN 1 ELSE 0 END) AS HadUniqueWdl
		,SUM(CASE WHEN(T4.UniqueWdlTrxNo<>0) THEN 1 ELSE 0 END) AS UniqueWdlTrxNo
		,SUM(CASE WHEN(T4.HadBeneBiosVerified<>0) THEN 1 ELSE 0 END) AS HadBeneBiosVerified
		,SUM(CASE WHEN(T4.IsDormant<>0) THEN 1 ELSE 0 END) AS IsDormant
		,SUM(CASE WHEN(T4.IsDueForClawback<>0) THEN 1 ELSE 0 END) AS IsDueForClawback
		,SUM(CASE WHEN(T4.ClawbackAmount>0) THEN ClawbackAmount ELSE 0 END) AS ClawbackAmount
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
							   LEFT JOIN (
											SELECT T2.BeneAccountId,T2.HadUniqueWdl,T2.UniqueWdlTrxNo,T2.UniqueWdlDate,T2.HadBeneBiosVerified,T2.IsDormant,T2.DormancyDate,T2.IsDueForClawback,T2.ClawbackAmount
											FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
											WHERE T1.MonthId=@SystemCodeDetailId1 AND T1.[Year]=@Year
										) T4 ON T1.Id=T4.BeneAccountId
	WHERE T1.StatusId=@SystemCodeDetailId2
	GROUP BY T3.Code
END
GO



IF NOT OBJECT_ID('GetReconciliationOptions') IS NULL DROP PROC GetReconciliationOptions
GO
CREATE PROC GetReconciliationOptions
	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @Id=ISNULL(@Id,0)
	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PostPayrollApvOn>=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT T1.Id,T3.[Description] AS FromMonth,T4.[Description] AS ToMonth,T5.[Description] AS FinancialYear
	FROM PaymentCycle T1 INNER JOIN (
									  SELECT PaymentCycleId
									  FROM PaymentCycleDetail
									  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
									  GROUP BY PaymentCycleId
									) T2 ON T1.Id=T2.PaymentCycleId
						 INNER JOIN SystemCodeDetail T3 ON T1.FromMonthId=T3.Id
						 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
						 INNER JOIN SystemCodeDetail T5 ON T1.FinancialYearId=T5.Id
END
GO



IF NOT OBJECT_ID('GetReconciliationDetailOptions') IS NULL DROP PROC GetReconciliationDetailOptions
GO
CREATE PROC GetReconciliationDetailOptions
	@ReconciliationId int
   ,@PSPId int
   ,@UserId int
AS
BEGIN
	DECLARE @tbl_PaymentCycles TABLE(
		PaymentCycleId int
	);
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @CrFundsRequests money
	DECLARE @CrClawBacks money
	DECLARE @DrPayments money
	DECLARE @DrCommissions money
	DECLARE @Balance money
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0)

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT PaymentCycleId
								FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PostPayrollApvOn<=T2.EndDate
								GROUP BY PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	INSERT INTO @tbl_PaymentCycles(PaymentCycleId)
	SELECT T1.PaymentCycleId
	FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T1.FundsRequestOn>=T2.StartDate AND t1.PostPayrollApvOn<=T2.EndDate
	WHERE T2.Id=@ReconciliationId
	GROUP BY PaymentCycleId

	SELECT @CrFundsRequests=SUM(T2.FundsRequestAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T2.PaymentCycleId,SUM(T1.EntitlementAmount+T1.OtherAmount) AS FundsRequestAmount
											FROM FundsRequestDetail T1 INNER JOIN FundsRequest T2 ON T1.FundsRequestId=T2.Id
											WHERE T1.PSPId=@PSPId
											GROUP BY T2.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId 

	SELECT @CrClawBacks=SUM(T1.ClawbackAmount)
	FROM (
			SELECT DISTINCT T1.PaymentCycleId,T2.MonthId,T2.[Year],T3.ClawbackAmount
			FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
									   INNER JOIN (
													SELECT T1.Id,T1.MonthId,T1.[Year],SUM(T2.ClawbackAmount) AS ClawbackAmount
													FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
																					   INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
																					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
													WHERE T4.PSPId=@PSPId
													GROUP BY T1.Id,T1.MonthId,T1.[Year]
												 ) T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
		) T1

	SELECT @DrPayments=SUM(T2.TrxAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T1.PaymentCycleId,SUM(T1.TrxAmount) AS TrxAmount
											FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
															INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
															INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
											WHERE T4.PSPId=@PSPId
											GROUP BY T1.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId
	
	SELECT @DrCommissions=SUM(T1.PaymentZoneCommAmt)
	FROM  (
			SELECT T1.PaymentCycleId,T1.HHId,T2.PaymentZoneCommAmt AS PaymentZoneCommAmt
			FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
							INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
							INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
			WHERE T1.WasTrxSuccessful=1 AND T4.PSPId=@PSPId
		) T1 INNER JOIN (
							SELECT T1.PaymentCycleId,T6.HhId,MAX(CONVERT(int,T4.HadUniqueWdl)) AS HadUniqueWdl
							FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
													   INNER JOIN BeneAccountMonthlyActivity T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
													   INNER JOIN BeneAccountMonthlyActivityDetail T4 ON T3.Id=T4.BeneAccountMonthlyActivityId
													   INNER JOIN BeneficiaryAccount T5 ON T4.BeneAccountId=T5.Id
													   INNER JOIN HouseholdEnrolment T6 ON T5.HhEnrolmentId=T6.Id
													   INNER JOIN PSPBranch T7 ON T5.PSPBranchId=T7.Id
							WHERE T7.PSPId=@PSPId
							GROUP BY T1.PaymentCycleId,T6.HhId
							HAVING MAX(CONVERT(int,T4.HadUniqueWdl))<>0
						) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HhId=T2.HhId

	SELECT @CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrClawBacks=ISNULL(@CrClawBacks,0),@DrPayments=ISNULL(@DrPayments,0),@DrCommissions=ISNULL(@DrCommissions,0)

	SELECT @Balance=(@CrFundsRequests+@CrClawBacks)-(@DrPayments-@DrCommissions)			 

	SELECT @CrFundsRequests AS CrFundsRequests,@CrClawBacks AS CrClawBacks,@DrPayments AS DrPayments,@DrCommissions AS DrCommissions,@Balance AS Balance
END
GO