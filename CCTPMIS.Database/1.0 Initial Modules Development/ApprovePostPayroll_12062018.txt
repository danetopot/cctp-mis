
IF NOT OBJECT_ID('ApprovePostPayroll') IS NULL	DROP PROC ApprovePostPayroll
GO
CREATE PROC ApprovePostPayroll
	@PaymentCycleId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='POSTPAYROLLAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The PaymentCycleId appears not to be in the Post Payroll Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	SET @SysCode='Payment Stage'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO