/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB TABLES TO HOLD DATA IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 1ST MARCH, 2018
    UPDATED ON: 9TH MARCH, 2018
	
	NOTE: IT REQUIRES AN EXISTING DATABASE	
*/


IF NOT OBJECT_ID('GetBeneForEnrolment') IS NULL	DROP PROC GetBeneForEnrolment
GO
CREATE PROC GetBeneForEnrolment
	@ProgrammeId int
   ,@RegGroupId int
AS
BEGIN
	--TO DO LIST: 1. INCORPORATE WAITING LIST VALIDITY PERIOD!
	DECLARE @tbl_EnrolmentHhAnalysis TABLE(
		LocationId int NOT NULL
	   ,PovertyPerc float NOT NULL DEFAULT(0.00)
	   ,RegGroupHhs int NOT NULL DEFAULT(0)
	   ,BeneHhs int NOT NULL DEFAULT(0)
	   ,ExpPlanEqualShare int NOT NULL DEFAULT(0)
	   ,ExpPlanPovertyPrioritized int NOT NULL DEFAULT(0)
	)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode1 varchar(20)
	DECLARE @SysDetailCode2 varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE ProgrammeId=@ProgrammeId AND ApvBy IS NULL)
		SET @ErrorMsg='There is an existing enrolment batch for the programme which requires to be approved first'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='System Settings'
	SET @SysDetailCode1='CURFINYEAR'
	SET @SysDetailCode2='WAITLISTVALIDITYMONTHS'
	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1
	SELECT @SystemCodeDetailId2=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2

	SET @SysCode='HHStatus'
	SET @SysDetailCode1='VALPASS'
	SET @SysDetailCode2='PSPCARDED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2

	SELECT SUM(T1.RegGroupHhs) AS RegGroupHhs,SUM(T1.BeneHhs) AS BeneHhs,SUM(ScaleupEqualShare) AS ScaleupEqualShare,SUM(ScaleupPovertyPrioritized) AS ScaleupPovertyPrioritized
	FROM (
			SELECT T1.LocationId,T1.RegGroupHhs,ISNULL(T2.BeneHhs,0) AS BeneHhs,ISNULL(T3.PovertyHeadCountPerc,0.00) AS PovertyPerc,ISNULL(T3.ScaleupEqualShare,0) AS ScaleupEqualShare,ISNULL(T3.ScaleupPovertyPrioritized,0) AS ScaleupPovertyPrioritized
			FROM (
					SELECT T4.LocationId,COUNT(T1.Id) AS RegGroupHhs
					FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
									  INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
									  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
												 ) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId										 					  
					WHERE T1.ProgrammeId=@ProgrammeId AND T1.RegGroupId=@RegGroupId AND T1.StatusId=@SystemCodeDetailId3 --EVALUATE THE WAITING LIST VALIDITY PERIOD HERE!!! 
					GROUP BY T4.LocationId
				) T1 LEFT JOIN (
									SELECT T4.LocationId,COUNT(T1.Id) AS BeneHhs
									FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
														INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
														INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																						INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																						INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																						INNER JOIN District T5 ON T4.DistrictId=T5.Id
																						INNER JOIN County T6 ON T4.CountyId=T6.Id
																						INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																						INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																	) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId					  
									WHERE T1.StatusId=@SystemCodeDetailId4
									GROUP BY T4.LocationId
								) T2 ON T1.LocationId=T2.LocationId 
					 LEFT JOIN (
									SELECT T2.LocationId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
									FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
																INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
																INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																								INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																								INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																								INNER JOIN District T5 ON T4.DistrictId=T5.Id
																								INNER JOIN County T6 ON T4.CountyId=T6.Id
																								INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																								INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																			WHERE T8.IsDefault=1
																			) T4 ON T2.LocationId=T4.LocationId	
									WHERE T3.ProgrammeId=@ProgrammeId AND T1.FinancialYearId=@SystemCodeDetailId1
								) T3 ON T1.LocationId=T3.LocationId
			) T1
END
GO
--EXEC GetBeneForEnrolment @ProgrammeId=1,@RegGroupId=99



IF NOT OBJECT_ID('BeneEnrolmentStatus') IS NULL	DROP PROC BeneEnrolmentStatus
GO
CREATE PROC BeneEnrolmentStatus
	@NationalIdNo varchar(30)
   ,@UserId int
AS
BEGIN
	DECLARE @BeneAccountId int
	DECLARE @HhEnrolId int
	DECLARE @PSPCode varchar(30)
	DECLARE @PSPUserId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@NationalIdNo,'')=''
		SET @ErrorMsg='Please specify valid NationalIdNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T7.BeneAccountId,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','PSPCARDED','ONPAY'))
							   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1
							   INNER JOIN Person T4 ON T3.PersonId=T4.Id
							   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id AND T3.MemberRoleId=T5.PrimaryRecipientId
							   LEFT JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
							   LEFT JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
							   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
							   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	WHERE T4.NationalIdNo=@NationalIdNo 

	IF @BeneAccountId>0
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
	ELSE IF ISNULL(@PSPUserId,0)>0 AND @PSPUserId<>@UserId
		SET @ErrorMsg=@PSPCode
	ELSE IF ISNULL(@HhEnrolId,0)=0
		SET @ErrorMsg='The beneficiary appears not to be eligible for the programme'
	
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	ELSE
		SELECT '0' AS StatusId,'The beneficiary is available for enrolment' AS [Description]	
END
GO



IF NOT OBJECT_ID('GetUserProfile') IS NULL	DROP PROC GetUserProfile
GO
CREATE PROCEDURE [dbo].[GetUserProfile]
    @UserId  int
 AS
   BEGIN  

      select u.FirstName, 
      u.MiddleName, 
      u.Surname, 
      u.Avatar,
       u.Email,
        u.MobileNo,
        u.UserGroupId
       from    [User] u 
        where u.Id = @UserId

     END

GO



IF NOT OBJECT_ID('GetUserGroupRights') IS NULL	DROP PROC GetUserGroupRights
GO
CREATE PROCEDURE [dbo].[GetUserGroupRights]
    @UserId  int
 AS
   BEGIN  

   
     select UPPER( concat(m.Name,':', scd.Code) ) from Groupright r 
     inner join  ModuleRight mr on mr.Id = r.ModuleRightId
     inner join SystemCodeDetail scd on scd.Id = mr.RightId
     inner join UserGroup ug on ug.Id = r.UserGroupId
     inner join Module m on m.Id = mr.ModuleId
     inner join   dbo.[User]  u on u.UserGroupId = ug.Id
     and u.Id = @UserId

     END

GO



IF NOT OBJECT_ID('GetEnrolmentPspFiles') IS NULL	DROP PROC GetEnrolmentPspFiles
GO
CREATE PROCEDURE [dbo].[GetEnrolmentPspFiles]
     @UserId int
 AS
 BEGIN
     select 
     fc.Id, fc.[Name], fc.FilePath, fc.FileChecksum, fc.CreatedOn, fc.CreatedBy, fc.CreationTypeId, fc.TypeId,
     fd.Id as FileDownloadId, fd.FileCreationId, fd.FileChecksum as FileChecksum2 , fd.DownloadedBy, fd.DownloadedOn
     from FileCreation fc left outer  join 
     FileDownload fd on fc.Id = fd.FileCreationId
     and fd.DownloadedBy = @UserId
     order by fc.Id desc
     END

GO



