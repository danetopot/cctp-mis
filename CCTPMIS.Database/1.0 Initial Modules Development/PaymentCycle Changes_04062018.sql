
CREATE TABLE PaymentAccountActivityMonth(
	PaymentCycleId int NOT NULL
   ,MonthId int NOT NULL
   ,YearId int NOT NULL
   ,CONSTRAINT PK_PaymentAccountActivityMonth PRIMARY KEY (PaymentCycleId,MonthId,YearId)
   ,CONSTRAINT FK_PaymentAccountActivityMonth_PaymentCycle_PaymentCycleId FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAccountActivityMonth_SystemCodeDetail_YearId FOREIGN KEY (YearId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAccountActivityMonth_SystemCodeDetail_MonthId FOREIGN KEY (MonthId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
)
GO


IF NOT OBJECT_ID('AddEditPaymentCycle') IS NULL	DROP PROC AddEditPaymentCycle
GO
CREATE PROC [dbo].[AddEditPaymentCycle]
    @Id int=NULL,
    @Description varchar(128),
    @FinancialYearId int,
    @FromMonthId int,
    @ToMonthId int,
	@PaymentMonthlyActivityXML XML,
    @UserId int
AS
BEGIN
	DECLARE @tblPaymentMonthlyActivity TABLE(
		Id int
	   ,MonthId int
	   ,YearId int
	)
    DECLARE @ErrorMsg varchar(128)
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
	DECLARE @CalendarMonthsCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
    DECLARE @StatusId int

	SET @CalendarMonthsCode='Calendar Month'
	SET @FinancialYearCode='Financial Year'
    SET @SysCode='Payment Status'
    SET @SysDetailCode='PAYMENTOPEN'
    SELECT @StatusId  =T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO @tblPaymentMonthlyActivity(Id,MonthId,YearId)
	SELECT T1.Id,T1.MonthId,YearId
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(MonthId)[1]','int') AS MonthId
			  ,U.R.value('(YearId)[1]','money') AS YearId
		FROM @PaymentMonthlyActivityXML.nodes('PaymentMonthlyActivity/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.MonthId=T2.Id
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@CalendarMonthsCode
		 INNER JOIN SystemCodeDetail T4 ON T1.YearId=T4.Id
		 INNER JOIN SystemCode T5 ON T4.SystemCodeId=T5.Id AND T5.Code=@FinancialYearCode

	IF NOT EXISTS(SELECT 1 FROM @tblPaymentMonthlyActivity)
		SET @ErrorMsg='Please specify valid month to base successful beneficiary payment by PSP'
    ELSE IF EXISTS(SELECT 1
    FROM PaymentCycle
    WHERE StatusId=@StatusId and @Id  is NULL )
		SET @ErrorMsg='There is an existing payment cycle whose payment need to be completed'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END
	ELSE
    BEGIN
        BEGIN TRAN
        IF EXISTS(SELECT 1
        FROM PaymentCycle
        WHERE Id=@Id )
			UPDATE PaymentCycle SET [Description] = @Description,FinancialYearId =  @FinancialYearId ,FromMonthId =  @FromMonthId,ToMonthId = @ToMonthId , StatusId =  @StatusId,ModifiedBy = @UserId,ModifiedOn = GETDATE() WHERE Id =  @Id
		ELSE  
            INSERT INTO PaymentCycle
            ( [Description] ,FinancialYearId ,FromMonthId ,ToMonthId ,StatusId,CreatedBy,CreatedOn )
			VALUES
            ( @Description  , @FinancialYearId  , @FromMonthId  , @ToMonthId , @StatusId  , @UserId, GETDATE())

		SELECT @Id=Id FROM PaymentCycle WHERE FinancialYearId=@FinancialYearId AND FromMonthId=@FromMonthId AND ToMonthId=@ToMonthId 
		DELETE FROM PaymentAccountActivityMonth WHERE PaymentCycleId=@Id
		INSERT INTO PaymentAccountActivityMonth(PaymentCycleId,YearId,MonthId)
		SELECT @Id AS PaymentCycleId,T1.YearId,T1.MonthId
		FROM @tblPaymentMonthlyActivity T1

		IF @@ERROR>0
		BEGIN
				ROLLBACK TRAN
				SELECT 0 AS NoOfRows
			END
		ELSE
		BEGIN
            COMMIT TRAN
            SELECT @@ROWCOUNT AS NoOfRows
        END
    END
END
GO

