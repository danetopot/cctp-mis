IF NOT OBJECT_ID('GetPaymentCycleOptions') IS NULL	DROP PROC GetPaymentCycleOptions
GO
CREATE PROC [dbo].[GetPaymentCycleOptions]
AS
BEGIN
    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)

    DECLARE @FromMonthId int
    DECLARE @ToMonthId int
    DECLARE @CurFromMonthId int
    DECLARE @CurToMonthId int

    DECLARE @FinancialYearId int
    DECLARE @FinancialYear varchar(30)
    DECLARE @FromMonth  varchar(30)
    DECLARE @ToMonth  varchar(30)

    IF EXISTS(	SELECT 1
    FROM PaymentCycle
    where StatusId
				    IN(SELECT T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
    WHERE T2.Code='Payment Status' AND T1.Code='PAYMENTOPEN')
			)
	BEGIN
        RAISERROR('There seems  to be having a running payment cycle which needs to be concluded first',16,1)
        RETURN
    END
	ELSE
	BEGIN




        SELECT @FromMonthId=T2.Id, @CurFromMonthId = CAST( T2.Code AS INT)
			  , @ToMonthId=T3.Id, @CurToMonthId = CAST( T3.Code AS INT)
              , @FinancialYearId = T1.FinancialYearId



        FROM PaymentCycle T1
            INNER JOIN SystemCodeDetail T2 ON T1.FromMonthId=T2.Id
            INNER JOIN SystemCodeDetail T3 ON T1.ToMonthId=T3.Id
            INNER JOIN SystemCodeDetail T4 ON T1.StatusId=T4.Id
        WHERE T1.Id=(SELECT MAX(Id)
        FROM PaymentCycle)


        PRINT @CurFromMonthId

        PRINT @CurToMonthId

        IF(ISNULL(@FinancialYearId,'' )='')

        SELECT @FinancialYearId =  T1.Id
        FROM SystemCodeDetail T1
        WHERE T1.Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

		SELECT @FromMonthId=T3.Code+1
			  ,@ToMonthId=(T4.Code)+2 --BY DEFAULT THE PAYMENT CYCLES ARE BI-MONTHLY. ADDITIONAL LOGIC SHOULD TAKE CARE OF PROGRAMMES WITH VARYING FREQUENCIES
		FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T3 ON T1.FromMonthId=T3.Id
							 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
		WHERE T1.Id=(SELECT MAX(Id) FROM PaymentCycle)
		
		SELECT @FromMonthId=T1.Id,@FromMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CASE WHEN(@FromMonthId>12) THEN @FromMonthId-12 ELSE @FromMonthId END
		SELECT @ToMonthId=T1.Id, @ToMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CASE WHEN(@ToMonthId>12) THEN @ToMonthId-12 ELSE @ToMonthId END

        SELECT @FinancialYear = [Description]
        FROM SystemCodeDetail
        WHERE Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

        SELECT @FromMonthId AS 'FromMonthId', @ToMonthId as 'ToMonthId', @FromMonth as 'FromMonth' , @ToMonth as 'ToMonth', @FinancialYearId as 'FinancialYearId', @FinancialYear as 'FinancialYear'


    END


END
GO
