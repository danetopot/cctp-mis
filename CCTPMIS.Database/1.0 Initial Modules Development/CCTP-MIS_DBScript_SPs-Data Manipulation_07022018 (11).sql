

/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB SPs FOR DATA MANIPULATION IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH FEBRUARY, 2018
   MODIFIED ON: 
	
	NOTE: IT REQUIRES AN EXISTING DATABASE
*/


IF NOT OBJECT_ID('AddEditPaymentCycleDetail') IS NULL	DROP PROC AddEditPaymentCycleDetail
GO
CREATE PROC AddEditPaymentCycleDetail
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@EnrolmentGroupsXML XML
   ,@FilePath nvarchar(128)
   ,@HasSupportingDoc bit=0
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentEnrolmentGroup TABLE(
		Id int
	   ,EnrolmentGroupId int
	   ,PaymentAmount money
	)

	DECLARE @HHStatus_EnrolledProgCode varchar(20)
	DECLARE @HHStatus_EnrolledPSPCode varchar(20)
	DECLARE @HHStatus_PSPCardedCode varchar(20)
	DECLARE @HHStatus_OnPayrollCode varchar(20)
	DECLARE @HHStatus_OnSuspensionCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @CalendarMonthCode varchar(20)
	DECLARE @EnrolmentGroupCode varchar(20)
	DECLARE @PaymentCycleStageCode varchar(20)
	DECLARE @PaymentCycleStageCode_PAYMENTCYCLEAPV varchar(20)
	DECLARE @PaymentCycleStageId int
	DECLARE @PayableHHs int
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @EnrolmentGroups1 varchar(128), @EnrolmentGroups2 varchar(128)
	DECLARE @ErrorMsg varchar(128)



	SET @HHStatus_EnrolledProgCode = 'ENRL'
	SET @HHStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HHStatus_PSPCardedCode = 'PSPCARDED'
	SET @HHStatus_OnPayrollCode = 'ONPAY'
	SET @HHStatus_OnSuspensionCode = 'SUS'
	SET @EnrolmentGroupCode = 'Enrolment Group'
	SET @PaymentCycleStageCode = 'Payment Stage'
	SET @PaymentCycleStageCode_PAYMENTCYCLEAPV = 'PAYMENTCYCLEAPV'

	INSERT INTO @tblPaymentEnrolmentGroup(Id,EnrolmentGroupId,PaymentAmount)
	SELECT T1.Id,T1.EnrolmentGroupId,PaymentAmount
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(EnrolmentGroupId)[1]','int') AS EnrolmentGroupId
			  ,U.R.value('(PaymentAmount)[1]','money') AS PaymentAmount
		FROM @EnrolmentGroupsXML.nodes('EnrolmentGroups/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.EnrolmentGroupId=T2.Id
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@EnrolmentGroupCode

	SELECT @EnrolmentGroups1=COALESCE(@EnrolmentGroups1+','+CONVERT(varchar(20),T2.EnrolmentGroupId),@EnrolmentGroups1) FROM PaymentCycleDetail T1 INNER JOIN PaymentEnrolmentGroup T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId ORDER BY T2.EnrolmentGroupId ASC
	SELECT @EnrolmentGroups2=COALESCE(@EnrolmentGroups2+','+CONVERT(varchar(20),EnrolmentGroupId),@EnrolmentGroups1) FROM @tblPaymentEnrolmentGroup ORDER BY EnrolmentGroupId ASC
	SELECT @PaymentCycleStageId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@PaymentCycleStageCode AND T1.Code=@PaymentCycleStageCode_PAYMENTCYCLEAPV
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId
	SELECT @ProgrammeId=Id FROM Programme WHERE Id=@ProgrammeId

	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentEnrolmentGroup)
		SET @ErrorMsg='Please specify valid EnrolmentGroupsXML parameter'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF @EnrolmentGroups1=@EnrolmentGroups2
		SET @ErrorMsg='The Payment Cycle Detail already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @PayableHHs=COUNT(T1.Id)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
	WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HHStatus_EnrolledProgCode,@HHStatus_EnrolledPSPCode,@HHStatus_PSPCardedCode,@HHStatus_OnPayrollCode)

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.EnrolledHHs=@PayableHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCycleDetail(PaymentCycleId,ProgrammeId,EnrolledHHs,PaymentStageId,CreatedBy,CreatedOn)
		SELECT @PaymentCycleId,@ProgrammeId,@PayableHHs,@PaymentCycleStageId,@UserId,GETDATE()
	END
	
	DELETE T1
	FROM PaymentEnrolmentGroup T1 
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	INSERT INTO PaymentEnrolmentGroup(PaymentCycleId,ProgrammeId,EnrolmentGroupId,PaymentAmount)
	SELECT @PaymentCycleID,@ProgrammeId,EnrolmentGroupId,PaymentAmount
	FROM @tblPaymentEnrolmentGroup

	--RECORDING THE FILE
	IF @HasSupportingDoc=1 
	BEGIN
		SELECT @FromMonthId=T1.FromMonthId
			  ,@ToMonthId=T1.ToMonthId
			  ,@FinancialYearId=T1.FinancialYearId
		FROM PaymentCycle T1 INNER JOIN PaymentCycleDetail T2 ON T1.Id=T2.PaymentCycleId
		WHERE T2.ProgrammeId=@ProgrammeId

		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @FileName='SUPPORT'+'_'+'PAYMENTCYCLE'+'_'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@FromMonthId)+'-'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@ToMonthId)+'_'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.FileCreationId=@FileCreationId
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows,T1.PaymentCycleId,T1.ProgrammeId,T2.Id AS PaymentCycleStageId,T2.[Description] AS PaymentCycleStage,@FileName+@FileExtension AS SupportingDoc
		FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
END
GO



IF NOT OBJECT_ID('ApprovePaymentCycleDetail') IS NULL	DROP PROC ApprovePaymentCycleDetail
GO
CREATE PROC ApprovePaymentCycleDetail
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	
	SELECT @@ROWCOUNT AS NoOfRows
END
GO



IF NOT OBJECT_ID('UTILITY_SP_PWDGEN') IS NULL DROP PROC UTILITY_SP_PWDGEN
GO
CREATE PROCEDURE UTILITY_SP_PWDGEN
	@len int = 8,
	--@Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz.,#$%^&*-+_/=',
	@Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
	@output nvarchar(64) OUTPUT
AS
	SET NOCOUNT ON;
	SET @output = '';
	SELECT  @output += SUBSTRING(@Charset, FLOOR(ABS(CAST(CRYPT_GEN_RANDOM(8) AS BIGINT) / 9223372036854775808.5) * LEN(@Charset)) + 1, 1)
	FROM master.dbo.spt_values
	WHERE type = 'P' AND number < @len;
GO



IF NOT OBJECT_ID('ApproveEnrolmentPlan') IS NULL	DROP PROC ApproveEnrolmentPlan
GO
CREATE PROC ApproveEnrolmentPlan
	@Id int
   ,@UserId int
AS
BEGIN
	--TO DO LIST: 1. INCORPORATE WAITING LIST VALIDITY PERIOD
	DECLARE @tbl_EnrolmentHhAnalysis TABLE(
		Id int NOT NULL IDENTITY(1,1)
	   ,HhId int NOT NULL
	 --,PMTScore int NOT NULL
	   ,LocationId int NOT NULL
	)
	DECLARE @tbl_EnrolmentNumbers TABLE(
		Id int NOT NULL IDENTITY(1,1)
	   ,LocationId int NOT NULL
	   ,PovertyPerc float NOT NULL
	   ,RegGroupHhs int NOT NULL
	   ,BeneHhs int NOT NULL
	   ,ScaleupEqualShare int NOT NULL
	   ,ScaleupPovertyPrioritized int NOT NULL
	   ,StartsFromId int
	   ,EnrolmentNumbers int
	)
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @NumbersToEnroll int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='HHStatus'
	SET @SysDetailCode='VALPASS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='System Settings'
	SET @SysDetailCode='CURFINYEAR'
	SELECT @SystemCodeDetailId3=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='WAITLISTVALIDITYMONTHS'
	SELECT @SystemCodeDetailId4=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @NumbersToEnroll=EnrolmentNumbers FROM HouseholdEnrolmentPlan WHERE Id=@Id

	BEGIN TRAN

	INSERT INTO @tbl_EnrolmentHhAnalysis(HhId,LocationId)
	SELECT T1.Id,T5.LocationId
	FROM Household T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId AND T1.RegGroupId=T2.RegGroupId
					  INNER JOIN HouseholdSubLocation T3 ON T1.Id=T3.HhId
					  INNER JOIN GeoMaster T4 ON T3.GeoMasterId=T4.Id AND T4.IsDefault=1
					  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T3.SubLocationId=T5.SubLocationId AND T4.Id=T5.GeoMasterId		
					  LEFT JOIN (
									SELECT T3.ProgrammeId,T2.LocationId,T1.FinancialYearId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
									FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
																INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
																INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																								INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																								INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																								INNER JOIN District T5 ON T4.DistrictId=T5.Id
																								INNER JOIN County T6 ON T4.CountyId=T6.Id
																								INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																								INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																			WHERE T8.IsDefault=1
																			) T4 ON T2.LocationId=T4.LocationId	
								) T6 ON T1.ProgrammeId=T6.ProgrammeId AND T5.LocationId=T6.LocationId AND T6.FinancialYearId=@SystemCodeDetailId3				 					  
	WHERE T2.Id=@Id AND T1.StatusId=@SystemCodeDetailId1 --INCORPORATE WAITING LIST VALIDITY PERIOD HERE
	ORDER BY T6.PovertyHeadCountPerc,T5.LocationId--THIS IS WHERE YOU INCLUDE PMT ORDERING ALSO

	INSERT INTO @tbl_EnrolmentNumbers(LocationId,PovertyPerc,RegGroupHhs,BeneHhs,ScaleupEqualShare,ScaleupPovertyPrioritized,StartsFromId)
	SELECT T1.LocationId,ISNULL(T3.PovertyHeadCountPerc,0.00) AS PovertyPerc,T1.RegGroupHhs,ISNULL(T2.BeneHhs,0) AS BeneHhs,ISNULL(T3.ScaleupEqualShare,0) AS ScaleupEqualShare,ISNULL(T3.ScaleupPovertyPrioritized,0) AS ScaleupPovertyPrioritized,T4.StartsFromId
	FROM (
			SELECT T1.ProgrammeId,T5.LocationId,COUNT(T1.Id) AS RegGroupHhs
			FROM Household T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId AND T1.RegGroupId=T2.RegGroupId
							  INNER JOIN HouseholdSubLocation T3 ON T1.Id=T3.HhId
							  INNER JOIN GeoMaster T4 ON T3.GeoMasterId=T4.Id AND T4.IsDefault=1
							  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
										FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
															INNER JOIN Division T3 ON T2.DivisionId=T3.Id
															INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
															INNER JOIN District T5 ON T4.DistrictId=T5.Id
															INNER JOIN County T6 ON T4.CountyId=T6.Id
															INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										) T5 ON T3.SubLocationId=T5.SubLocationId AND T4.Id=T5.GeoMasterId
			WHERE T2.Id=@Id AND T1.StatusId=@SystemCodeDetailId1
			GROUP BY T1.ProgrammeId,T5.LocationId
		) T1 LEFT JOIN (
							SELECT T4.LocationId,COUNT(T1.Id) AS BeneHhs
							FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
												INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
												INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
															FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																				INNER JOIN District T5 ON T4.DistrictId=T5.Id
																				INNER JOIN County T6 ON T4.CountyId=T6.Id
																				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
															) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId	
												 INNER JOIN SystemCodeDetail T5 ON T1.StatusId=T5.Id
												 INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='HhStatus'			  
							WHERE T5.Code IN('ENRL','ENRLPSP','PSPCARDED','ONPAY')
							GROUP BY T4.LocationId
						) T2 ON T1.LocationId=T2.LocationId 
				LEFT JOIN (
							SELECT T3.ProgrammeId,T2.LocationId,T1.FinancialYearId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
							FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
														INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
														INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																						INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																						INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																						INNER JOIN District T5 ON T4.DistrictId=T5.Id
																						INNER JOIN County T6 ON T4.CountyId=T6.Id
																						INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																						INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																	WHERE T8.IsDefault=1
																	) T4 ON T2.LocationId=T4.LocationId	
						) T3 ON T1.ProgrammeId=T3.ProgrammeId AND T1.LocationId=T3.LocationId AND T3.FinancialYearId=@SystemCodeDetailId3	
				LEFT JOIN (
							SELECT LocationId,MIN(Id) AS StartsFromId
							FROM @tbl_EnrolmentHhAnalysis
							GROUP BY LocationId
						  ) T4 ON T1.LocationId=T4.LocationId
	ORDER BY PovertyPerc,LocationId

	--FIRST PRIORITIZE ENROLMENT OF EQUAL SHARE NUMBERS
	SET @SystemCodeDetailId1=1
	WHILE @NumbersToEnroll>0 AND EXISTS(SELECT 1 FROM @tbl_EnrolmentNumbers WHERE Id=@SystemCodeDetailId1)
	BEGIN
		SELECT @SystemCodeDetailId2=CASE WHEN(ScaleupEqualShare+ScaleupPovertyPrioritized<=0) THEN RegGroupHhs
										 WHEN(ScaleupEqualShare-BeneHhs>0) THEN CASE WHEN(ScaleupEqualShare-BeneHhs>RegGroupHhs) THEN RegGroupHhs ELSE ScaleupEqualShare-BeneHhs END
										 ELSE 0 
									END 
		FROM @tbl_EnrolmentNumbers 
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId2=CASE WHEN(@NumbersToEnroll>=@SystemCodeDetailId2) THEN @SystemCodeDetailId2 ELSE @NumbersToEnroll END
		SET @NumbersToEnroll=@NumbersToEnroll-@SystemCodeDetailId2
		
		UPDATE @tbl_EnrolmentNumbers
		SET EnrolmentNumbers=ISNULL(EnrolmentNumbers,0)+@SystemCodeDetailId2
		FROM @tbl_EnrolmentNumbers
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId1=@SystemCodeDetailId1+1
	END
	--THEN ENROLMENT OF POVERTY PRIORITIZED NUMBERS
	SET @SystemCodeDetailId1=1
	WHILE @NumbersToEnroll>0 AND EXISTS(SELECT 1 FROM @tbl_EnrolmentNumbers WHERE Id=@SystemCodeDetailId1)
	BEGIN
		SELECT @SystemCodeDetailId2=CASE WHEN(ScaleupEqualShare+ScaleupPovertyPrioritized<=0) THEN RegGroupHhs
										 WHEN((ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers)>0) THEN CASE WHEN((ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers)>(RegGroupHhs-EnrolmentNumbers)) THEN (RegGroupHhs-EnrolmentNumbers) ELSE (ScaleupEqualShare+ScaleupPovertyPrioritized)-(BeneHhs+EnrolmentNumbers) END
										 ELSE 0 
									END 
		FROM @tbl_EnrolmentNumbers 
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId2=CASE WHEN(@NumbersToEnroll>=@SystemCodeDetailId2) THEN @SystemCodeDetailId2 ELSE @NumbersToEnroll END
		SET @NumbersToEnroll=@NumbersToEnroll-@SystemCodeDetailId2
		
		UPDATE @tbl_EnrolmentNumbers
		SET EnrolmentNumbers=ISNULL(EnrolmentNumbers,0)+@SystemCodeDetailId2
		FROM @tbl_EnrolmentNumbers
		WHERE Id=@SystemCodeDetailId1

		SET @SystemCodeDetailId1=@SystemCodeDetailId1+1
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@SystemCodeDetailId1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.Id=@Id

	INSERT INTO HouseholdEnrolment(HhEnrolmentPlanId,HhId,BeneProgNoPrefix,ProgrammeNo)
	SELECT T1.HhEnrolmentPlanId,T1.HhId,T1.BeneProgNoPrefix,ISNULL(T2.MAXProgNo,0)+T1.RowId AS ProgrammeNo
	FROM (
			SELECT @Id AS HhEnrolmentPlanId,T1.HhId,T4.Id AS ProgrammeId,T4.BeneProgNoPrefix,ROW_NUMBER() OVER(PARTITION BY T4.Id ORDER BY T1.Id) AS RowId 
			FROM @tbl_EnrolmentHhAnalysis T1 INNER JOIN @tbl_EnrolmentNumbers T2 ON T1.LocationId=T2.LocationId 
											 INNER JOIN Household T3 ON T1.HhId=T3.Id
											 INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			WHERE T2.EnrolmentNumbers>0 AND T1.Id>=T2.StartsFromId AND T1.Id<(T2.StartsFromId+T2.EnrolmentNumbers) 
		) T1 LEFT JOIN (SELECT T3.Id AS ProgrammeId,MAX(ISNULL(T1.ProgrammeNo,0)) AS MAXProgNo
						FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id
												   INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id
						GROUP BY T3.Id
						) T2 ON T1.ProgrammeId=T2.ProgrammeId

	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRL'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T3
	SET T3.StatusId=@SystemCodeDetailId2
	FROM @tbl_EnrolmentHhAnalysis T1 INNER JOIN @tbl_EnrolmentNumbers T2 ON T1.LocationId=T2.LocationId 
									 INNER JOIN Household T3 ON T1.HhId=T3.Id
	WHERE T2.EnrolmentNumbers>0 AND T1.Id>=T2.StartsFromId AND T1.Id<(T2.StartsFromId+T2.EnrolmentNumbers) 

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('AddEditEnrolmentPlan') IS NULL	DROP PROC AddEditEnrolmentPlan
GO
CREATE PROC AddEditEnrolmentPlan
	@Id int=NULL
   ,@ProgrammeId int
   ,@RegGroupId int
   ,@RegGroupHhs int
   ,@BeneHhs int
   ,@ExpPlanEqualShare int
   ,@ExpPlanPovertyPrioritized int
   ,@EnrolmentNumbers int
   ,@EnrolmentGroupId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Id=@ProgrammeId)
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@RegGroupId AND T2.Code='Registration Group')
		SET @ErrorMsg='Please specify valid RegGroupId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@EnrolmentGroupId AND T2.Code='Enrolment Group')
		SET @ErrorMsg='Please specify valid EnrolmentGroupId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGENROLAPV'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.RegGroupId=@RegGroupId
		   ,T1.RegGroupHhs=@RegGroupHhs
		   ,T1.BeneHhs=@BeneHhs
		   ,T1.ExpPlanEqualShare=@BeneHhs
		   ,T1.ExpPlanPovertyPrioritized=@BeneHhs
		   ,T1.EnrolmentNumbers=@EnrolmentNumbers
		   ,T1.EnrolmentGroupId=@EnrolmentGroupId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM HouseholdEnrolmentPlan T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO HouseholdEnrolmentPlan(ProgrammeId,RegGroupId,RegGroupHhs,BeneHhs,ExpPlanEqualShare,ExpPlanPovertyPrioritized,EnrolmentNumbers,EnrolmentGroupId,StatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeId,@RegGroupId,@RegGroupHhs,@BeneHhs,@ExpPlanEqualShare,@ExpPlanPovertyPrioritized,@EnrolmentNumbers,@EnrolmentGroupId,@SystemCodeDetailId,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('HouseholdEnrolmentPlan')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO
--exec AddEditEnrolmentPlan @Id=0,@ProgrammeId=1,@RegGroupId=103,@RegGroupHhs=553449,@BeneHhs=0,@ExpPlanEqualShare=0,@ExpPlanPovertyPrioritized=0,@EnrolmentNumbers=100,@EnrolmentGroupId=104,@UserId=1
--exec AddEditEnrolmentPlan @Id=1,@ProgrammeId=1,@RegGroupId=103,@RegGroupHhs=553449,@BeneHhs=0,@ExpPlanEqualShare=0,@ExpPlanPovertyPrioritized=0,@EnrolmentNumbers=500,@EnrolmentGroupId=104,@UserId=1
--EXEC ApproveEnrolmentPlan @Id=2,@UserId=1
--select * from householdenrolmentplan
--select * from FileCreation
--select * from household where statusid=16
--exec DeleteEnrolmentPlan @Id=5
--select * from HouseholdEnrolment
--select * from systemcodedetail where id=31




IF NOT OBJECT_ID('DeleteEnrolmentPlan') IS NULL	DROP PROC DeleteEnrolmentPlan
GO
CREATE PROC DeleteEnrolmentPlan
	@Id int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE Id=@Id AND ApvBy IS NOT NULL)
		SET @ErrorMsg='The specified enrolment batch has been approved and cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
END
GO



IF NOT OBJECT_ID('GenerateEnrolmentFile') IS NULL	DROP PROC GenerateEnrolmentFile
GO
CREATE PROC GenerateEnrolmentFile
	@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	DROP TABLE #FileResults

	IF OBJECT_ID('temp_EnrolmentFile') IS NOT NULL	DROP TABLE temp_EnrolmentFile;
	CREATE TABLE temp_EnrolmentFile(
		EnrolmentNo int
	   ,ProgrammeNo varchar(50)
	   ,BeneFirstName varchar(50)
	   ,BeneMiddleName varchar(50)
	   ,BeneSurname varchar(50)
	   ,BeneIDNo varchar(30)
	   ,BeneSex varchar(20)
	   ,BeneDoB datetime
	   ,CGFirstName varchar(50)
	   ,CGMiddleName varchar(50)
	   ,CGSurname varchar(50)
	   ,CGIDNo varchar(30)
	   ,CGSex varchar(20)
	   ,CGDoB datetime
	   ,MobileNo1 varchar(20)
	   ,MobileNo2 varchar(20)
	   ,County varchar(30)
	   ,Constituency varchar(30)
	   ,District varchar(30)
	   ,Division varchar(30)
	   ,Location varchar(30)
	   ,SubLocation varchar(30)
	   );
	   
	INSERT INTO temp_EnrolmentFile(EnrolmentNo,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneIDNo,BeneSex,BeneDoB,CGFirstName,CGMiddleName,CGSurname,CGIDNo,CGSex,CGDoB,MobileNo1,MobileNo2,County,Constituency,District,Division,Location,SubLocation)
	SELECT T2.Id AS EnrolmentNo
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!
	WHERE T1.StatusId=@SystemCodeDetailId1

	IF NOT EXISTS(SELECT 1 FROM temp_EnrolmentFile)
		SET @ErrorMsg='There is no beneficiary selection approved for enrolment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='ENROLMENT_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_EnrolmentFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	--SELECT @SQLStmt
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	DROP TABLE temp_EnrolmentFile;
	
	--RECORDING THE FILE
	SET @SysCode='File Type'
	SET @SysDetailCode='ENROLMENT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SET @FileCreationId=IDENT_CURRENT('FileCreation')

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='PSPRECEIPT'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId2
	   ,T1.FileCreationId=@FileCreationId
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.StatusId=@SystemCodeDetailId1

	SELECT @FileCreationId AS FileCreationId
	SET NOCOUNT OFF
END
GO
--EXEC GenerateEnrolmentFile @FilePath='C:\MINE\',@DBServer='.\SQL2012',@DBName='CCTP-MIS',@DBUser='sa',@DBPassword='sa@pass',@UserId=1




IF NOT OBJECT_ID('BeneEnrolFileDownloaded') IS NULL	DROP PROC BeneEnrolFileDownloaded
GO
CREATE PROC BeneEnrolFileDownloaded
	@FileCreationId int
   ,@FileChecksum varchar(64)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @RowCount int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Id=@FileCreationId AND FileChecksum=@FileChecksum)
		SET @ErrorMsg='Please specify valid FileCreationId corresponding FileCheksum'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId AND T2.UserId=@UserId)
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM FileDownload WHERE FileCreationId=@FileCreationId AND DownloadedBy=@UserId)
			INSERT INTO FileDownload(FileCreationId,FileChecksum,DownloadedBy,DownloadedOn)
			SELECT @FileCreationId,@FileChecksum,@UserId,GETDATE() AS DownloadedOn

		SET @SysCode='Enrolment Status'
		SET @SysDetailCode='PSPENROL'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.StatusId=@SystemCodeDetailId1
		FROM HouseholdEnrolmentPlan T1
		WHERE T1.FileCreationId=@FileCreationId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FilePassword
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT FilePassword FROM FileCreation WHERE Id=@FileCreationId
	END
END
GO




IF NOT OBJECT_ID('PSPEnrolBeneficiary') IS NULL	DROP PROC PSPEnrolBeneficiary
GO
CREATE PROC PSPEnrolBeneficiary
	@EnrolmentNo int
   ,@BankCode varchar(20)
   ,@BranchCode varchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AccountOpenedOn datetime
   ,@PriReciFirstName varchar(50)
   ,@PriReciMiddleName varchar(50)=NULL
   ,@PriReciSurname varchar(50)
   ,@PriReciNationalIdNo varchar(30)
   ,@PriReciSex char(1)
   ,@PriReciDoB datetime=NULL
   ,@SecReciFirstName varchar(50)=NULL
   ,@SecReciMiddleName varchar(50)=NULL
   ,@SecReciSurname varchar(50)=NULL
   ,@SecReciNationalIdNo varchar(30)=NULL
   ,@SecReciSex char(1)=NULL
   ,@SecReciDoB datetime=NULL
   ,@PaymentCardNo varchar(50)=	NULL
   ,@MobileNo1 nvarchar(20)=NULL
   ,@MobileNo2 nvarchar(20)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @HhEnrolmentId int
	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @PriReciId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@PriReciSex
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PriReciId=T1.Id,@PriReciDoB=ISNULL(@PriReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.PrimaryRecipientId
	WHERE T1.FirstName=@PriReciFirstName AND T1.MiddleName=ISNULL(@PriReciMiddleName,'') AND T1.Surname=@PriReciSurname AND T1.NationalIdNo=@PriReciNationalIdNo AND T1.SexId=@SystemCodeDetailId3 --AND T1.DoB=@PriReciDoB 
		
	SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id,@SecReciDoB=ISNULL(@SecReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB 
						
	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
			 
				   
	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo AND T2.StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF @AccountOpenedOn IS NULL OR NOT (@AccountOpenedOn<=GETDATE())
		SET @ErrorMsg='Please specify valid AccountOpenedOn parameter'
	ELSE IF ISNULL(@PriReciId,0)=0
		SET @ErrorMsg='Please specify valid Primary Recipient details'
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)
		SET @ErrorMsg='The EnrolmentNo and caregiver details do not match'
	ELSE IF ISNULL(@MobileNo1,'')<>'' AND (CASE WHEN(@MobileNo1 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo1 parameter'
	ELSE IF ISNULL(@MobileNo2,'')<>'' AND (CASE WHEN(@MobileNo2 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo2 parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 
				   FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
											  INNER JOIN PSP T3 ON T2.PSPId=T3.Id
				   WHERE HhEnrolmentId=@EnrolmentNo AND ExpiryDate>GETDATE() 
						AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)
				   )
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
	ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@PriReciNationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	ELSE IF EXISTS(SELECT 1 FROM [70PlusRegistration] WHERE OP_CONFIRM_ID_NO=@PriReciNationalIdNo AND RefId>523449)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	--ELSE IF EXISTS(	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T7.BeneAccountId,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	--				FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','PSPCARDED','ONPAY'))
	--										   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1
	--										   INNER JOIN Person T4 ON T3.PersonId=T4.Id
	--										   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id AND T3.MemberRoleId=T5.PrimaryRecipientId
	--										   INNER JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
	--										   INNER JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
	--										   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
	--										   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	--				WHERE T4.NationalIdNo=@NationalIdNo 
	--				)

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SET @SysCode='System Settings'
	SET @SysDetailCode='BENEPSPRENEWALDATE'
	SELECT @AccountExpiryDate=CASE WHEN(ISNULL(T1.[Description],'')='' OR CONVERT(datetime,T1.[Description])<GETDATE()) THEN CONVERT(datetime,'30 June '+CONVERT(varchar(4),CASE WHEN(GETDATE()>CONVERT(datetime,'30 June '+CONVERT(varchar(4),YEAR(GETDATE())))) THEN YEAR(GETDATE())+1 ELSE YEAR(GETDATE()) END))
																														ELSE CONVERT(datetime,T1.[Description])
							  END
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS(SELECT 1 
			  FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
										 INNER JOIN PSP T3 ON T2.PSPId=T3.Id
			  WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo
			  )
	BEGIN
		UPDATE T1
		SET T1.PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) 
		   ,T1.AccountNo=@AccountNo
		   ,T1.AccountName=@AccountName
		   ,T1.OpenedOn=@AccountOpenedOn
		   ,T1.StatusId=@SystemCodeDetailId1
		   ,T1.ExpiryDate=@AccountExpiryDate
		FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
								   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
		WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo

		SELECT @AccountId=T1.Id
		FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
								   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
		WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo
	END
	ELSE
	BEGIN
		INSERT INTO BeneficiaryAccount(HhEnrolmentId,PSPBranchId,AccountNo,AccountName,OpenedOn,StatusId,ExpiryDate)
		SELECT @EnrolmentNo AS HhEnrolmentId
			,(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AS PSPBranchId
			,@AccountNo AS AccountNo
			,@AccountName AS AccountName
			,@AccountOpenedOn AS OpenedOn
			,@SystemCodeDetailId1 AS StatusId
			,@AccountExpiryDate AS ExpiryDate

		--SET @AccountId=IDENT_CURRENT('BeneficiaryAccount')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
		SELECT @AccountId=Id FROM BeneficiaryAccount WHERE PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AND AccountNo=@AccountNo
	END

	SET @SysCode='Card Status'
	SET @SysDetailCode= CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN '-1' ELSE '1' END
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId

	IF ISNULL(@PaymentCardId,0)>0
	BEGIN
		UPDATE T1
		SET T1.BeneAccountId=@AccountId
		   ,T1.PriReciId=@PriReciId
		   ,T1.PriReciFirstname=@PriReciFirstName
		   ,T1.PriReciMiddleName=@PriReciMiddleName
		   ,T1.PriReciSurname=@PriReciSurname
		   ,T1.PriReciNationalIdNo=@PriReciNationalIdNo
		   ,T1.PriReciSexId=@SystemCodeDetailId3
		   ,T1.PriReciDoB=@PriReciDoB
		   ,T1.SecReciId=@SecReciId
		   ,T1.SecReciFirstname=@SecReciFirstName
		   ,T1.SecReciMiddleName=@SecReciMiddleName
		   ,T1.SecReciSurname=@SecReciSurname
		   ,T1.SecReciNationalIdNo=@SecReciNationalIdNo
		   ,T1.SecReciSexId=@SystemCodeDetailId4
		   ,T1.SecReciDoB=@SecReciDoB
		   ,T1.PaymentCardNo=@PaymentCardNo
		   ,T1.MobileNo1=@MobileNo1
		   ,T1.MobileNo2=@MobileNo2
		   ,T1.StatusId=@SystemCodeDetailId2
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM BeneficiaryPaymentCard T1
		WHERE T1.Id=@PaymentCardId
	END
	ELSE
	BEGIN
		INSERT INTO BeneficiaryPaymentCard(BeneAccountId,PriReciId,PriReciFirstname,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSexId,PriReciDoB,SecReciId,SecReciFirstname,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSexId,SecReciDoB,PaymentCardNo,MobileNo1,MobileNo2,StatusId,CreatedBy,CreatedOn)
		SELECT @AccountId,@PriReciId,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@SystemCodeDetailId3,@PriReciDoB,@SecReciId,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SystemCodeDetailId4,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @PaymentCardId=Id FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId AND CreatedBy=@UserId AND StatusId=@SystemCodeDetailId2
		--SET @PaymentCardId=IDENT_CURRENT('BeneficiaryPaymentCard')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
	END


	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN 'ENRLPSP' ELSE 'PSPCARDED' END
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	   ,T1.ModifiedBy=@UserId
	   ,T1.ModifiedOn=GETDATE()
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo AND T1.StatusId=@SystemCodeDetailId1

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS BeneAccountId,NULL AS PaymentCardId,NULL AS PriReciId,NULL AS SecReciId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @AccountId AS BeneAccountId,@PaymentCardId AS PaymentCardId,@PriReciId AS PriReciId,@SecReciId AS SecReciId
	END
END
GO




IF NOT OBJECT_ID('PSPCardBeneficiary') IS NULL	DROP PROC PSPCardBeneficiary
GO
CREATE PROC PSPCardBeneficiary
	@EnrolmentNo int
   ,@AccountNo varchar(50)
   ,@PaymentCardNo varchar(50)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode			 
				   
	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRLPSP'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode	

	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo AND T2.StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id INNER JOIN Household T3 ON T2.HhId=T3.Id WHERE T1.AccountNo=@AccountNo AND T3.StatusId=@SystemCodeDetailId2)
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id 
													  INNER JOIN Household T3 ON T2.HhId=T3.Id
													  INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id AND T4.IsActive=1 
													  INNER JOIN PSP T5 ON T4.PSPId=T5.Id AND T5.IsActive=1 
				   WHERE T1.AccountNo=@AccountNo AND T2.Id=@EnrolmentNo AND T3.StatusId=@SystemCodeDetailId2 AND T5.UserId=@UserId)
		SET @ErrorMsg='Please specify valid EnrolmentNo, corresponding AccountNo and PSP UserId parameter'
	ELSE IF ISNULL(@PaymentCardNo,'')=''
		SET @ErrorMsg='Please specify valid PaymentCardNo parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SELECT @AccountId=Id FROM BeneficiaryAccount WHERE AccountNo=@AccountNo

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	UPDATE T1
	SET T1.PaymentCardNo=@PaymentCardNo
	   ,T1.StatusId=@SystemCodeDetailId2
	FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
								   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
	WHERE T3.Id=@EnrolmentNo AND T2.AccountNo=@AccountNo 

	SET @SysCode='HHStatus'
	SET @SysDetailCode='PSPCARDED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('PSPEnrolCargiver') IS NULL	DROP PROC PSPEnrolCargiver
GO
CREATE PROC PSPEnrolCargiver
	@EnrolmentNo int
   ,@BankCode varchar(20)
   ,@BranchCode varchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@SecReciFirstName varchar(50)
   ,@SecReciMiddleName varchar(50)=NULL
   ,@SecReciSurname varchar(50)
   ,@SecReciNationalIdNo varchar(30)=NULL
   ,@SecReciSex char(1)
   ,@SecReciDoB datetime=NULL

   ,@SecReciRT nvarchar(max)=NULL
   ,@SecReciRI nvarchar(max)=NULL
   ,@SecReciRMF nvarchar(max)=NULL
   ,@SecReciRRF nvarchar(max)=NULL
   ,@SecReciRP nvarchar(max)=NULL
   ,@SecReciLT nvarchar(max)=NULL
   ,@SecReciLI nvarchar(max)=NULL
   ,@SecReciLMF nvarchar(max)=NULL
   ,@SecReciLRF nvarchar(max)=NULL
   ,@SecReciLP nvarchar(max)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @HhEnrolmentId int
	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		
	SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id 
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB 
						
	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
			 
	SELECT @AccountId=T1.Id
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.ExpiryDate>GETDATE() AND T1.AccountNo=@AccountNo AND T1.AccountName=@AccountName AND T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId
				   
	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)
		SET @ErrorMsg='The EnrolmentNo and caregiver details do not match'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 
				   FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
											  INNER JOIN PSP T3 ON T2.PSPId=T3.Id
				   WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.ExpiryDate>GETDATE() 
						AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)
				   )
		SET @ErrorMsg='The household appears to have an existing account with a different PSP'
	ELSE IF(ISNULL(@AccountId,0)=0)
		SET @ErrorMsg='The specified account details do not return a match with an existing beneficiary account'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN
	
	SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId

	IF ISNULL(@PaymentCardId,0)>0
	BEGIN
		UPDATE T1
		SET T1.SecReciId=@SecReciId
		   ,T1.SecReciFirstname=@SecReciFirstName
		   ,T1.SecReciMiddleName=@SecReciMiddleName
		   ,T1.SecReciSurname=@SecReciSurname
		   ,T1.SecReciNationalIdNo=@SecReciNationalIdNo
		   ,T1.SecReciSexId=@SystemCodeDetailId4
		   ,T1.SecReciDoB=@SecReciDoB
		FROM BeneficiaryPaymentCard T1
		WHERE T1.Id=@PaymentCardId

		UPDATE T1
		SET T1.SecReciRT=CONVERT(varbinary(max),@SecReciRT)
		   ,T1.SecReciRI=CONVERT(varbinary(max),@SecReciRI)
		   ,T1.SecReciRMF=CONVERT(varbinary(max),@SecReciRMF)
		   ,T1.SecReciRRF=CONVERT(varbinary(max),@SecReciRRF)
		   ,T1.SecReciRP=CONVERT(varbinary(max),@SecReciRP)
		   ,T1.SecReciLT=CONVERT(varbinary(max),@SecReciLT)
		   ,T1.SecReciLI=CONVERT(varbinary(max),@SecReciLI)
		   ,T1.SecReciLMF=CONVERT(varbinary(max),@SecReciLMF)
		   ,T1.SecReciLRF=CONVERT(varbinary(max),@SecReciLRF)
		   ,T1.SecReciLP=CONVERT(varbinary(max),@SecReciLP)
		FROM PaymentCardBiometrics T1
		WHERE T1.BenePaymentCardId=@PaymentCardId
	END
	
	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('AddEditPaymentCardBiometrics') IS NULL	DROP PROC AddEditPaymentCardBiometrics
GO
CREATE PROC AddEditPaymentCardBiometrics
	@BenePaymentCardId int
   ,@PriReciRT varbinary(max)
   ,@PriReciRI varbinary(max)
   ,@PriReciRMF varbinary(max)
   ,@PriReciRRF varbinary(max)
   ,@PriReciRP varbinary(max)
   ,@PriReciLT varbinary(max)
   ,@PriReciLI varbinary(max)
   ,@PriReciLMF varbinary(max)
   ,@PriReciLRF varbinary(max)
   ,@PriReciLP varbinary(max)
   ,@SecReciRT nvarchar(max)=NULL
   ,@SecReciRI nvarchar(max)=NULL
   ,@SecReciRMF nvarchar(max)=NULL
   ,@SecReciRRF nvarchar(max)=NULL
   ,@SecReciRP nvarchar(max)=NULL
   ,@SecReciLT nvarchar(max)=NULL
   ,@SecReciLI nvarchar(max)=NULL
   ,@SecReciLMF nvarchar(max)=NULL
   ,@SecReciLRF nvarchar(max)=NULL
   ,@SecReciLP nvarchar(max)=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM BeneficiaryPaymentCard WHERE Id=@BenePaymentCardId)
		SET @ErrorMsg='Please specify valid PaymentCardId'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF NOT EXISTS(SELECT 1 FROM [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics WHERE BenePaymentCardId=@BenePaymentCardId)
	--BEGIN
	--	UPDATE T1
	--	SET T1.PriReciRT=@PriReciRT
	--	   ,T1.PriReciRI=@PriReciRI
	--	   ,T1.PriReciRMF=@PriReciRMF
	--	   ,T1.PriReciRRF=@PriReciRRF
	--	   ,T1.PriReciRP=@PriReciRP
	--	   ,T1.PriReciLT=@PriReciLT
	--	   ,T1.PriReciLI=@PriReciLI
	--	   ,T1.PriReciLMF=@PriReciLMF
	--	   ,T1.PriReciLRF=@PriReciLRF
	--	   ,T1.PriReciLP=@PriReciLP
	--	   ,T1.SecReciRT=CONVERT(varbinary(max),@SecReciRT)
	--	   ,T1.SecReciRI=CONVERT(varbinary(max),@SecReciRI)
	--	   ,T1.SecReciRMF=CONVERT(varbinary(max),@SecReciRMF)
	--	   ,T1.SecReciRRF=CONVERT(varbinary(max),@SecReciRRF)
	--	   ,T1.SecReciRP=CONVERT(varbinary(max),@SecReciRP)
	--	   ,T1.SecReciLT=CONVERT(varbinary(max),@SecReciLT)
	--	   ,T1.SecReciLI=CONVERT(varbinary(max),@SecReciLI)
	--	   ,T1.SecReciLMF=CONVERT(varbinary(max),@SecReciLMF)
	--	   ,T1.SecReciLRF=CONVERT(varbinary(max),@SecReciLRF)
	--	   ,T1.SecReciLP=CONVERT(varbinary(max),@SecReciLP)
	--	   ,T1.CreatedBy=@UserId
	--	   ,T1.CreatedOn=GETDATE()
	--	FROM [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics T1
	--	WHERE T1.BenePaymentCardId=@BenePaymentCardId
	--END
	--ELSE
	BEGIN
		INSERT INTO [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics(BenePaymentCardId,PriReciRT,PriReciRI,PriReciRMF,PriReciRRF,PriReciRP,PriReciLT,PriReciLI,PriReciLMF,PriReciLRF,PriReciLP,SecReciRT,SecReciRI,SecReciRMF,SecReciRRF,SecReciRP,SecReciLT,SecReciLI,SecReciLMF,SecReciLRF,SecReciLP,CreatedBy,CreatedOn)
		SELECT @BenePaymentCardId,@PriReciRT,@PriReciRI,@PriReciRMF,@PriReciRRF,@PriReciRP,@PriReciLT,@PriReciLI,@PriReciLMF,@PriReciLRF,@PriReciLP,CONVERT(varbinary(max),@SecReciRT),CONVERT(varbinary(max),@SecReciRI),CONVERT(varbinary(max),@SecReciRMF),CONVERT(varbinary(max),@SecReciRRF),CONVERT(varbinary(max),@SecReciRP),CONVERT(varbinary(max),@SecReciLT),CONVERT(varbinary(max),@SecReciLI),CONVERT(varbinary(max),@SecReciLMF),CONVERT(varbinary(max),@SecReciLRF),CONVERT(varbinary(max),@SecReciLP),@UserId,GETDATE()
	END

	--SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('ProcessPrepayrollAudit') IS NULL	DROP PROC ProcessPrepayrollAudit
GO
CREATE PROC ProcessPrepayrollAudit
	@PaymentCycleId int
   ,@ProgrammeId int
   --,@DBServer varchar(30)
   --,@DBName varchar(30)
   --,@DBUser varchar(30)
   --,@DBPassword nvarchar(30)
   --,@FilePath nvarchar(256)
   --,@DownloadPath nvarchar(256)
   ,@UserId int
AS
BEGIN
	DECLARE @ActiveHhs int
	DECLARE @TargetHhs int
	DECLARE @HhStatus_EnrolledProgCode varchar(20)
	DECLARE @HhStatus_EnrolledPSPCode varchar(20)
	DECLARE @HhStatus_PSPCardedCode varchar(20)
	DECLARE @HhStatus_OnPayrollCode varchar(20)
	DECLARE @HhStatus_OnSuspensionCode varchar(20)
	DECLARE @SysSetting_PSPACCDORMANCY int
	DECLARE @DormancyFromDate datetime
	DECLARE @DormancyToDate datetime
	DECLARE @BeneficiaryTypeId int
	DECLARE @SecondaryRecipientMandatory bit
	DECLARE @BeneficiaryType_INDIVIDUAL int
	DECLARE @ExceptionType_INVALIDBENEID int
	DECLARE @ExceptionType_INVALIDCGID int
	DECLARE @ExceptionType_DUPLICATEBENEIDIN int
	DECLARE @ExceptionType_DUPLICATEBENEIDACC int
	DECLARE @ExceptionType_DUPLICATECGIDIN int
	DECLARE @ExceptionType_DUPLICATECGIDACC int
	DECLARE @ExceptionType_SUSPICIOUSAMT int
	DECLARE @ExceptionType_SUSPICIOUSDORMANCY int
	DECLARE @ExceptionType_INELIGIBLEBENE int
	DECLARE @ExceptionType_INELIGIBLECG int
	DECLARE @ExceptionType_INELIGIBLESUS int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(128)

	DECLARE @FileName nvarchar(128)
	DECLARE @FileType nvarchar(10)
	DECLARE @NoOfRows int

	SET @HhStatus_EnrolledProgCode = 'ENRL'
	SET @HhStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HhStatus_PSPCardedCode = 'PSPCARDED'
	SET @HhStatus_OnPayrollCode = 'ONPAY'
	SET @HhStatus_OnSuspensionCode = 'SUS'

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLDRAFT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='PREPAYROLLFINAL'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ActiveHhs=COUNT(T1.Id),@TargetHhs=SUM(CASE WHEN(T6.EnrolmentGroupId>0) THEN 1 ELSE 0 END),@SystemCodeDetailId3=MAX(T3.PaymentStageId)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
					  INNER JOIN PaymentCycleDetail T3 ON T1.ProgrammeId=T3.ProgrammeId
					  INNER JOIN HouseholdEnrolment T4 ON T1.Id=T4.HhId
					  INNER JOIN HouseholdEnrolmentPlan T5 ON T4.HhEnrolmentPlanId=T5.Id
					  LEFT JOIN PaymentEnrolmentGroup T6 ON T3.PaymentCycleId=T6.PaymentCycleId AND T3.ProgrammeId=T6.ProgrammeId AND T5.EnrolmentGroupId=T6.EnrolmentGroupId								  
	WHERE T3.PaymentCycleId=@PaymentCycleId AND T3.ProgrammeId=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode,@HhStatus_OnSuspensionCode)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycle WHERE Id=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF	@SystemCodeDetailId3 NOT IN(@SystemCodeDetailId1,@SystemCodeDetailId2)
		SET @ErrorMsg='Once the Prepayroll has been APPROVED one CANNOT run the Prepayroll again'	
	ELSE IF ISNULL(@TargetHhs,0)=0
		SET @ErrorMsg='No target households'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='System Settings'
	SET @SysDetailCode='PSPACCDORMANCY'
	SELECT @SysSetting_PSPACCDORMANCY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='Beneficiary Type'
	SET @SysDetailCode='INDIVIDUAL'
	SELECT @BeneficiaryType_INDIVIDUAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Exception Type'
	SET @SysDetailCode='INVALIDBENEID'
	SELECT @ExceptionType_INVALIDBENEID=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INVALIDCGID'
	SELECT @ExceptionType_INVALIDCGID=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysDetailCode='DUPLICATEBENEIDIN'
	SELECT @ExceptionType_DUPLICATEBENEIDIN=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='DUPLICATEBENEIDACC'
	SELECT @ExceptionType_DUPLICATEBENEIDACC=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='DUPLICATECGIDIN'
	SELECT @ExceptionType_DUPLICATECGIDIN=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='DUPLICATECGIDACC'
	SELECT @ExceptionType_DUPLICATECGIDACC=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='SUSPICIOUSAMT'
	SELECT @ExceptionType_SUSPICIOUSAMT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='SUSPICIOUSDORMANCY'
	SELECT @ExceptionType_SUSPICIOUSDORMANCY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysDetailCode='INELIGIBLEBENE'
	SELECT @ExceptionType_INELIGIBLEBENE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INELIGIBLECG'
	SELECT @ExceptionType_INELIGIBLECG=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='INELIGIBLESUS'
	SELECT @ExceptionType_INELIGIBLESUS=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @BeneficiaryTypeId=T1.BeneficiaryTypeId
		  ,@SecondaryRecipientMandatory=T1.SecondaryRecipientMandatory
		  ,@DormancyToDate=CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code)+' '+CONVERT(varchar(4),CASE WHEN(T4.Code<7) THEN 1 ELSE 0 END+CONVERT(bigint,T5.Code)))
	FROM Programme T1 INNER JOIN PaymentCycleDetail T2 ON T1.Id=T2.ProgrammeId
					  INNER JOIN PaymentCycle T3 ON T2.PaymentCycleId=T3.Id
					  INNER JOIN SystemCodeDetail T4 ON T3.FromMonthId=T4.Id
					  INNER JOIN SystemCodeDetail T5 ON T3.FinancialYearId=T5.Id
	WHERE T2.PaymentCycleId=@PaymentCycleId AND T2.ProgrammeId=@ProgrammeId

	SET @DormancyFromDate=DATEADD(MM,-@SysSetting_PSPACCDORMANCY,@DormancyToDate)

	BEGIN TRAN

	DELETE T1 FROM Prepayroll T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollDuplicateID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidPaymentAccount T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollInvalidPaymentCard T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollIneligible T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
	DELETE T1 FROM PrepayrollSuspicious T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;

	INSERT INTO Prepayroll(PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSexId,BeneNationalIDNo,PriReciCanReceivePayment,CGPersonId,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSexId,CGNationalIDNo,TotalHhMembers,HhStatusId,BeneAccountId,BenePaymentCardId,SubLocationId,PaymentZoneId,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount)
	SELECT @PaymentCycleId,T3.Id AS ProgrammeId,T1.Id AS HhId,T4.PersonId AS BenePersonId,T4.FirstName AS BeneFirstName,T4.MiddleName AS BeneMiddleName,T4.Surname AS BeneSurname,T4.DoB AS BeneDoB,T4.SexId AS BeneSexId,T4.NationalIdNo AS BeneNationalIDNo,T3.PriReciCanReceivePayment,T5.PersonId AS CGPersonId,T5.FirstName AS CGFirstName,T5.MiddleName AS CGMiddleName,T5.Surname AS CGSurname,T5.DoB AS CGDoB,T5.SexId AS CGSexId,T5.NationalIdNo AS CGNationalIDNo,T6.TotalHhMembers,T1.StatusId AS HhStatusId,T14.Id AS BeneAccountId,T15.Id AS BenePaymentCardId,T7.SubLocationId,T8.Id AS PaymentZoneId,CASE WHEN(T8.IsPerc=1) THEN ((T3.EntitlementAmount+T10.AdjustmentAmount)*T8.Commission)/100 ELSE T8.Commission END AS PaymentZoneCommAmt,ISNULL(T9.BeneAccDormancy,0) AS ConseAccInactivity,T3.EntitlementAmount,T10.AdjustmentAmount
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
					  INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					  INNER JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T1.NationalIdNo
									FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
												   INNER JOIN Household T3 ON T2.HhId=T3.Id
												   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
									WHERE T2.StatusId=@SystemCodeDetailId1
								) T4 ON T1.Id=T4.HhId AND T4.RowId=1
					  LEFT JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T1.NationalIdNo
									FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
												   INNER JOIN Household T3 ON T2.HhId=T3.Id
												   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.SecondaryRecipientId
									WHERE T2.StatusId=@SystemCodeDetailId1
								) T5 ON T1.Id=T5.HhId AND T5.RowId=1
					  LEFT JOIN(
									SELECT HhId,COUNT(PersonId) AS TotalHhMembers
									FROM HouseholdMember
									WHERE StatusId=@SystemCodeDetailId1
									GROUP BY HhId
								) T6 ON T1.Id=T6.HhId
					  LEFT JOIN(
									SELECT T1.HhId,T1.SubLocationId,T2.LocationId,T4.PaymentZoneId
									FROM HouseholdSubLocation T1 INNER JOIN SubLocation T2 ON T1.SubLocationId=T2.Id
																 INNER JOIN GeoMaster T3 ON T1.GeoMasterId=T3.Id
																 INNER JOIN Location T4 ON T2.LocationId=T4.Id
									WHERE T3.IsDefault=1
								) T7 ON T1.Id=T7.HhId	
					  LEFT JOIN PaymentZone T8 ON T7.PaymentZoneId=T8.Id
					  LEFT JOIN (
									SELECT T3.HhId,SUM(CASE(T1.HasUniqueTrx) WHEN 1 THEN 1 ELSE 0 END) AS BeneAccDormancy
									FROM BeneficiaryAccountActivity T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
																	   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
																	   INNER JOIN SystemCodeDetail T4 ON T1.MonthId=T4.Id
									WHERE CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code)+' '+CONVERT(varchar(4),T1.[Year]))>@DormancyFromDate
										AND CONVERT(datetime,'1 '+dbo.fn_MonthName(T4.Code)+' '+CONVERT(varchar(4),T1.[Year]))<=@DormancyFromDate
									GROUP BY T3.HhId
								) T9 ON T1.Id=T9.HhId
					  LEFT JOIN (
									SELECT PaymentCycleId,HhId,SUM(AdjustmentAmount) AS AdjustmentAmount
									FROM PaymentAdjustment
									GROUP BY PaymentCycleId,HhId
								) T10 ON T10.PaymentCycleId=@PaymentCycleId AND T1.Id=T10.HHId
					  INNER JOIN HouseholdEnrolment T11 ON T1.Id=T11.HhId
					  INNER JOIN HouseholdEnrolmentPlan T12 ON T11.HhEnrolmentPlanId=T12.Id
					  INNER JOIN PaymentEnrolmentGroup T13 ON T13.PaymentCycleId=@PaymentCycleId AND T12.EnrolmentGroupId=T13.EnrolmentGroupId	
					  LEFT JOIN BeneficiaryAccount T14 ON T11.Id=T14.HhEnrolmentId AND T14.StatusId=@SystemCodeDetailId2 AND T14.ExpiryDate>GETDATE()
					  LEFT JOIN BeneficiaryPaymentCard T15 ON T14.Id=T15.BeneAccountId AND T15.StatusId=@SystemCodeDetailId3
	WHERE T3.Id=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)

	INSERT INTO PrepayrollInvalidID(PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,@ExceptionType_INVALIDBENEID
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)<>1
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,@ExceptionType_INVALIDCGID
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)<>1
	
	INSERT INTO PrepayrollDuplicateID(PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.BenePersonId,@ExceptionType_DUPLICATEBENEIDIN
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo,COUNT(T1.HhId) NoOfHhs
							FROM (
									SELECT T6.NationalIdNo,T1.Id AS HhId
									FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
														INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
														INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId 
														INNER JOIN HouseholdMember T5 ON T3.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
														INNER JOIN Person T6 ON T5.PersonId=T6.Id
									WHERE T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode) AND ISNUMERIC(T6.NationalIDNo)=1
								) T1
							GROUP BY CONVERT(bigint,T1.NationalIdNo)
							HAVING COUNT(T1.HhId)>1
						) T2 ON CONVERT(bigint,T1.BeneNationalIDNo)=T2.ResolvedIDNo
	UNION
	SELECT T1.PaymentCycleId,ProgrammeId,T1.HhId,T1.BenePersonId,@ExceptionType_DUPLICATEBENEIDACC
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T6.NationalIdNo
							FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
												INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
												INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
												INNER JOIN HouseholdMember T5 ON T3.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
												INNER JOIN Person T6 ON T5.PersonId=T6.Id
							WHERE NOT(T3.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL) AND ISNUMERIC(T6.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
							GROUP BY T6.NationalIdNo
						) T2 ON CONVERT(bigint,T1.BeneNationalIDNo)=CONVERT(bigint,T2.NationalIdNo)
	WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)
	UNION
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.CGPersonId,@ExceptionType_DUPLICATECGIDIN
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,CGNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo,COUNT(T1.HhId) NoOfHhs
							FROM (
									SELECT T6.NationalIdNo,T1.Id AS HhId
									FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
														INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
														INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId
														INNER JOIN HouseholdMember T5 ON T3.SecondaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
														INNER JOIN Person T6 ON T5.PersonId=T6.Id
									WHERE T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode) AND ISNUMERIC(T6.NationalIDNo)=1
								) T1
							GROUP BY CONVERT(bigint,T1.NationalIdNo)
							HAVING COUNT(T1.HhId)>1
						) T2 ON CONVERT(bigint,T1.CGNationalIDNo)=T2.ResolvedIDNo
	UNION
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.CGPersonId,@ExceptionType_DUPLICATECGIDACC
	FROM (
			SELECT PaymentCycleId,ProgrammeId,HhId,CGPersonId,CGNationalIDNo
			FROM Prepayroll 
			WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T6.NationalIdNo
							FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
												INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
												INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
												INNER JOIN HouseholdMember T5 ON T3.SecondaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId1
												INNER JOIN Person T6 ON T5.PersonId=T6.Id
							WHERE NOT(T3.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL) AND ISNUMERIC(T6.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
							GROUP BY T6.NationalIdNo
						) T2 ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T2.NationalIdNo)
	WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)

	INSERT INTO PrepayrollInvalidPaymentAccount(PaymentCycleId,ProgrammeId,HhId)
	SELECT PaymentCycleId,ProgrammeId,HhId
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BeneAccountId,0)<=0 

	INSERT INTO PrepayrollInvalidPaymentCard(PaymentCycleId,ProgrammeId,HhId)
	SELECT PaymentCycleId,ProgrammeId,HhId
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePaymentCardId,0)<=0

	INSERT INTO PrepayrollIneligible(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INELIGIBLEBENE
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePersonId,0)<=0
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_INELIGIBLECG
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND @SecondaryRecipientMandatory=1 AND ISNULL(CGPersonId,0)<=0
	UNION
	SELECT T1.PaymentCycleId,ProgrammeId,T1.HhId,@ExceptionType_INELIGIBLECG
	FROM Prepayroll T1 INNER JOIN SystemCodeDetail T2 ON T1.HhStatusId=T2.Id
	WHERE PaymentCycleId=@PaymentCycleId AND T2.Code=@HhStatus_OnSuspensionCode

	INSERT INTO PrepayrollSuspicious(PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_SUSPICIOUSDORMANCY
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND ConseAccInactivity>=@SysSetting_PSPACCDORMANCY
	UNION
	SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_SUSPICIOUSAMT
	FROM Prepayroll
	WHERE PaymentCycleId=@PaymentCycleId AND AdjustmentAmount<>0

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLFINAL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.EnrolledHhs=@ActiveHHs
	   ,T1.PrePayrollBy=@UserId
	   ,T1.PrePayrollOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		SELECT 1 AS NoRows
	END
	--EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
END
GO



IF NOT OBJECT_ID('GeneratePrepayrollExceptionsFile') IS NULL	DROP PROC GeneratePrepayrollExceptionsFile
GO
CREATE PROC GeneratePrepayrollExceptionsFile
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYMENTCYCLEAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='PREPAYROLLDRAFT'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId IN(@SystemCodeDetailId1,@SystemCodeDetailId2))
		SET @ErrorMsg='The prepayroll audit seems not to have been executed on the specified payment cycle'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'

	IF OBJECT_ID('temp_EXCEPTIONS') IS NOT NULL	DROP TABLE temp_EXCEPTIONS;
	CREATE TABLE temp_EXCEPTIONS(
		PaymentCycleId int NOT NULL
	   ,ProgrammeId tinyint NOT NULL
	   ,Programme nvarchar(20) NOT NULL
	   ,HhId int NOT NULL
	   ,BenePersonId int NOT NULL
	   ,BeneFirstName varchar(50) NOT NULL
	   ,BeneMiddleName varchar(50) NULL
	   ,BeneSurname varchar(50) NOT NULL
	   ,BeneDoB datetime NOT NULL
	   ,BeneSex varchar(20) NOT NULL
	   ,BeneNationalIDNo varchar(30) NULL
	   ,PriReciCanReceivePayment bit
	   ,IsInvalidBene bit NOT NULL
	   ,IsDuplicateBene bit NOT NULL
	   ,IsIneligibleBene bit NOT NULL
	   ,CGPersonId int NOT NULL
	   ,CGFirstName varchar(50) NOT NULL
	   ,CGMiddleName varchar(50) NULL
	   ,CGSurname varchar(50) NOT NULL
	   ,CGDoB datetime NOT NULL
	   ,CGSex varchar(20) NOT NULL
	   ,CGNationalIDNo varchar(30) NULL
	   ,IsInvalidCG bit NOT NULL
	   ,IsDuplicateCG bit NOT NULL
	   ,IsIneligibleCG bit NOT NULL
	   ,HhStatus varchar(20) NOT NULL
	   ,IsIneligibleHh bit NOT NULL
	   ,AccountNumber varchar(50) NOT NULL
	   ,IsInvalidAccount bit NOT NULL
	   ,IsDormantAccount bit NOT NULL
	   ,PaymentCardNumber varchar(50) NOT NULL
	   ,IsInvalidPaymentCard bit
	   ,PaymentZoneId smallint NOT NULL
	   ,PaymentZoneCommAmt money NOT NULL
	   ,ConseAccInactivity tinyint NOT NULL
	   ,EntitlementAmount money NOT NULL DEFAULT(0)
	   ,AdjustmentAmount money NOT NULL DEFAULT(0)
	   ,IsSuspiciousAmount bit NOT NULL
	   );
	   
	INSERT INTO temp_EXCEPTIONS(PaymentCycleId,ProgrammeId,Programme,HhId,BenePersonId,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBene,IsIneligibleBene,CGPersonId,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCG,IsIneligibleCG,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZoneId,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount)
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.BenePersonId,T1.BenePersonId,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneDoB,T2.Code AS BeneSex,T1.BeneNationalIDNo,T1.PriReciCanReceivePayment,CONVERT(bit,ISNULL(T5.PersonId,0)) AS IsInvalidBene,CONVERT(bit,ISNULL(T6.PersonId,0)) AS IsDuplicateBene,CONVERT(bit,ISNULL(T8.Id,0)) AS IsIneligibleBene,T1.CGPersonId,T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,T1.CGDoB,T3.Code AS CGSex,T1.CGNationalIDNo,CONVERT(bit,ISNULL(T9.PersonId,0)) AS IsInvalidCG,CONVERT(bit,ISNULL(T10.PersonId,0)) AS IsDuplicateCG,CONVERT(bit,ISNULL(T12.Code,0)) AS IsIneligibleCG,T4.Code AS HhStatus,0 AS IsIneligibleHh,T13.AccountNo AS AccountNumber,CONVERT(bit,ISNULL(T13.Id,0)) AS IsInvalidAccount,CONVERT(bit,ISNULL(T16.Id,0)) AS IsDormantAccount,T14.PaymentCardNo AS PaymentCardNumber,CONVERT(bit,ISNULL(T14.Id,0)) AS IsInvalidPaymentCard,T1.PaymentZoneId,T1.PaymentZoneCommAmt,T1.ConseAccInactivity,T1.EntitlementAmount,T1.AdjustmentAmount,0 AS IsSuspiciousAmount
	FROM Prepayroll T1 INNER JOIN SystemCodeDetail T2 ON T1.BeneSexId=T2.Id
					   INNER JOIN SystemCodeDetail T3 ON T1.CGSexId=T3.Id
					   INNER JOIN SystemCodeDetail T4 ON T1.HhStatusId=T4.Id
					   LEFT JOIN PrepayrollInvalidID T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.BenePersonId=T5.PersonId
					   LEFT JOIN PrepayrollDuplicateID T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.BenePersonId=T6.PersonId
					   LEFT JOIN PrepayrollIneligible T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId 
					   LEFT JOIN SystemCodeDetail T8 ON T7.ExceptionTypeId=T8.Id AND T8.Code=@Exception_INELIGIBLEBENE
					   LEFT JOIN PrepayrollInvalidID T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.BenePersonId=T9.PersonId
					   LEFT JOIN PrepayrollDuplicateID T10 ON T1.PaymentCycleId=T10.PaymentCycleId AND T1.ProgrammeId=T10.ProgrammeId AND T1.BenePersonId=T10.PersonId
					   LEFT JOIN PrepayrollIneligible T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.HhId=T11.HhId 
					   LEFT JOIN SystemCodeDetail T12 ON T7.ExceptionTypeId=T12.Id AND T12.Code=@Exception_INELIGIBLECG				   
					   LEFT JOIN BeneficiaryAccount T13 ON T1.BeneAccountId=T13.Id
					   LEFT JOIN BeneficiaryPaymentCard T14 ON T1.BenePaymentCardId=T14.Id
					   LEFT JOIN PrepayrollSuspicious T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
					   LEFT JOIN SystemCodeDetail T16 ON T15.ExceptionTypeId=T16.Id AND T16.Code=@Exception_SUSPICIOUSDORMANCY
	WHERE T1.PaymentCycleId=@PaymentCycleId

	UPDATE T1
	SET T1.IsIneligibleHh=1
	FROM temp_EXCEPTIONS T1 INNER JOIN PrepayrollIneligible T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId 
							INNER JOIN SystemCodeDetail T3 ON T2.ExceptionTypeId=T3.Id AND T3.Code=@Exception_INELIGIBLECG	

	UPDATE T1
	SET T1.IsSuspiciousAmount=1
	FROM temp_EXCEPTIONS T1 INNER JOIN PrepayrollSuspicious T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId 
							INNER JOIN SystemCodeDetail T3 ON T2.ExceptionTypeId=T3.Id AND T3.Code=@Exception_SUSPICIOUSAMT	

	IF EXISTS(SELECT 1 FROM temp_EXCEPTIONS)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SET @FileName='EXCEPTIONS_'

		SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
		SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
		SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
		SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName
		SET @FileExtension='.csv'
		SET @FileCompression='.rar'


		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_EXCEPTIONS" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

		DROP TABLE temp_EXCEPTIONS;
	
		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='EXCEPTION'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
		SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SET @FileCreationId=IDENT_CURRENT('FileCreation')

		UPDATE T1
		SET T1.ExceptionsFileId=@FileCreationId
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END

	SELECT ISNULL(@FileCreationId,0) AS FileCreationId
	SET NOCOUNT OFF
END
GO



IF NOT OBJECT_ID('ProcessPayrollExceptionsAction') IS NULL	DROP PROC ProcessPayrollExceptionsAction
GO
CREATE PROC ProcessPayrollExceptionsAction
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@ExceptionId int
   ,@Notes varchar(128)
   ,@EnrolmentGroupId int=NULL
   ,@PrepayrollExceptionsXML XML=NULL
   ,@ReplacePreviousActioning bit=0
   ,@FilePath nvarchar(128)
   ,@HasSupportingDoc bit=0
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @ExceptionCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @ErrorMsg varchar(128)

	IF NOT OBJECT_ID('temp_tblExceptionsAction') IS NULL	DROP TABLE temp_tblExceptionsAction;
	CREATE TABLE temp_tblExceptionsAction(
		PaymentCycleId int NOT NULL
	   ,HhId int NOT NULL
	   ,PersonId int NULL
	);

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
	SELECT @ExceptionCode=Code FROM SystemCodeDetail WHERE Id=@ExceptionId
	SELECT @FromMonthId=FromMonthId,@ToMonthId=ToMonthId,@FinancialYearId=FinancialYearId FROM PaymentCycle WHERE Id=@PaymentCycleId

	IF ISNULL(@EnrolmentGroupId,0)>0 
	BEGIN
		INSERT INTO temp_tblExceptionsAction(PaymentCycleId,HhId,PersonId)
		SELECT T1.PaymentCycleId,T1.HhId,T1.PersonId
		FROM PrepayrollInvalidID T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
									INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId AND T1.ExceptionTypeId=@ExceptionId
		UNION
		SELECT T1.PaymentCycleId,T1.HhId,T1.PersonId
		FROM PrepayrollDuplicateID T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
										INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId	AND T1.ExceptionTypeId=@ExceptionId
		UNION
		SELECT T1.PaymentCycleId,T1.HhId,NULL
		FROM PrepayrollInvalidPaymentAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
												INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId	AND T1.ExceptionTypeId=@ExceptionId
		UNION
		SELECT T1.PaymentCycleId,T1.HhId,NULL
		FROM PrepayrollInvalidPaymentCard T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
												INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId	AND T1.ExceptionTypeId=@ExceptionId
		UNION
		SELECT T1.PaymentCycleId,T1.HhId,NULL
		FROM PrepayrollSuspicious T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
										INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId	AND T1.ExceptionTypeId=@ExceptionId
		UNION
		SELECT T1.PaymentCycleId,T1.HhId,NULL
		FROM PrepayrollIneligible T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
										INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
		WHERE T3.EnrolmentGroupId=@EnrolmentGroupId	AND T1.ExceptionTypeId=@ExceptionId	
	END
	ELSE
		IF @PrepayrollExceptionsXML IS NOT NULL
		BEGIN
			IF EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID,@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC) AND T1.Id=@ExceptionId)
				INSERT INTO temp_tblExceptionsAction(PaymentCycleId,HhId,PersonId)
				SELECT T1.PaymentCycleId,T1.HhId,T1.PersonId
				FROM (
					SELECT U.R.value('(PaymentCycleId)[1]','int') AS PaymentCycleId
						  ,U.R.value('(HhId)[1]','int') AS HhId
						  ,U.R.value('(PersonId)[1]','int') AS PersonId
					FROM @PrepayrollExceptionsXML.nodes('PrepayrollExceptions/Record') AS U(R)
				) T1 
			ELSE
				INSERT INTO temp_tblExceptionsAction(PaymentCycleId,HhId)
				SELECT T1.PaymentCycleId,T1.HhId
				FROM (
					SELECT U.R.value('(PaymentCycleId)[1]','int') AS PaymentCycleId
						  ,U.R.value('(HhId)[1]','int') AS HhId
					FROM @PrepayrollExceptionsXML.nodes('PrepayrollExceptions/Record') AS U(R)
				) T1
		END

	IF NOT EXISTS(SELECT 1 FROM PaymentCycle WHERE Id=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id AND T2.Code='PREPAYROLLFINAL' WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The specified PaymentCycleId appears not to be in the Prepayroll Stage where Prepayroll Exceptions actioning can be done'
	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Id=@ExceptionId)
		SET @ErrorMsg='Please specify valid ExceptionType parameter'
	ELSE IF ISNULL(@Notes,'')=''
		SET @ErrorMsg='Please specify valid Notes parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM temp_tblExceptionsAction)
		SET @ErrorMsg='Please specify valid either EnrolementGroupId or PrepayrollExceptionsXML parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF @ReplacePreviousActioning=1
	BEGIN
		IF @ExceptionCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidID T1 WHERE T1.PaymentCycleId=@PaymentCycleId
		IF @ExceptionCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollDuplicateID T1 WHERE T1.PaymentCycleId=@PaymentCycleId
		IF @ExceptionCode=@Exception_INVALIDACC
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidPaymentAccount T1 WHERE T1.PaymentCycleId=@PaymentCycleId
		IF @ExceptionCode=@Exception_INVALIDCARD
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidPaymentCard T1 WHERE T1.PaymentCycleId=@PaymentCycleId
		IF @ExceptionCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollSuspicious T1 WHERE T1.PaymentCycleId=@PaymentCycleId
		IF @ExceptionCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollIneligible T1 WHERE T1.PaymentCycleId=@PaymentCycleId
	END

	IF @ExceptionCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID)
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidID T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId AND T1.PersonId=T2.PersonId
	IF @ExceptionCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC)
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollDuplicateID T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId AND T1.PersonId=T2.PersonId
	IF @ExceptionCode=@Exception_INVALIDACC
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidPaymentAccount T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId
	IF @ExceptionCode=@Exception_INVALIDCARD
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidPaymentCard T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId
	IF @ExceptionCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY)
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollSuspicious T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId
	IF @ExceptionCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS)
		UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollIneligible T1 INNER JOIN temp_tblExceptionsAction T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HHId=T2.HhId
	
	--RECORDING THE FILE
	IF @HasSupportingDoc=1 
	BEGIN
		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @FileName='SUPPORT'+'_'+'EXCEPTIONACTION'+'_'+@ExceptionCode+'_'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@FromMonthId)+'-'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@ToMonthId)+'_'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		IF @ExceptionCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID)
			UPDATE T1 SET T1.InvalidIDActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		IF @ExceptionCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC)
			UPDATE T1 SET T1.DuplicateIDActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		IF @ExceptionCode=@Exception_INVALIDACC
			UPDATE T1 SET T1.InvalidPaymentAccountActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		IF @ExceptionCode=@Exception_INVALIDCARD
			UPDATE T1 SET T1.InvalidPaymentCardActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		IF @ExceptionCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY)
			UPDATE T1 SET T1.IneligibleBeneficiaryActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		IF @ExceptionCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS)
			UPDATE T1 SET T1.SuspiciousPaymentActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @FileCreationId AS FileId
	END
	EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
	IF NOT OBJECT_ID('temp_tblExceptionsAction') IS NULL	DROP TABLE temp_tblExceptionsAction;
END
GO




IF NOT OBJECT_ID('FinalizePrepayroll') IS NULL	DROP PROC FinalizePrepayroll
GO
CREATE PROC FinalizePrepayroll
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id AND T2.Code='PREPAYROLLFINAL' WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The specified PaymentCycleId appears not to be in the Prepayroll Stage where Prepayroll Exceptions actioning can be done'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	
	SELECT @@ROWCOUNT AS NoOfRows
END
GO


IF NOT OBJECT_ID('GeneratePayrollFile') IS NULL	DROP PROC GeneratePayrollFile
GO
CREATE PROC GeneratePayrollFile
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not ready for generation'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Account Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	DECLARE @PSPs TABLE(
		RowId int NOT NULL IDENTITY(1,1)
	   ,PSPCode nvarchar(20) NOT NULL
	)

	IF OBJECT_ID('temp_PayrollFile') IS NOT NULL	DROP TABLE temp_PayrollFile;
	CREATE TABLE temp_PayrollFile(
		PaymentCycleId int NOT NULL
	   ,HhId int NOT NULL
	   ,ProgrammeNo varchar(50)
	   ,PSPCode nvarchar(20) NOT NULL
	   ,PSPBranchCode nvarchar(20) NOT NULL
	   ,AccountNo varchar(50) NOT NULL
	   ,AccountName varchar(100) NOT NULL
	   ,Amount money NOT NULL
	   );
	   
	INSERT INTO temp_PayrollFile(PaymentCycleId,HhId,ProgrammeNo,PSPCode,PSPBranchCode,AccountNo,AccountName,Amount)
	SELECT @PaymentCycleId AS PaymentCycleId,T1.HhId,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo,T5.Code AS PSPCode,T4.Code AS PSPBranchCode,T3.AccountNo,T3.AccountName,(T1.EntitlementAmount+T1.AdjustmentAmount) AS Amount
	FROM Prepayroll T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhId=T2.HhId
					   INNER JOIN BeneficiaryAccount T3 ON T2.Id=T3.HhEnrolmentId AND T3.StatusId=@SystemCodeDetailId1
					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id AND T4.IsActive=1
					   INNER JOIN PSP T5 ON T4.PSPId=T5.Id AND T5.IsActive=1
					   LEFT JOIN (
									SELECT PaymentCycleId,HhId FROM PrepayrollInvalidID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,HhId FROM PrepayrollDuplicateID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,HhId FROM PrepayrollIneligible WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,HhId FROM PrepayrollSuspicious WHERE ActionedApvBy IS NULL
								) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.HhId=T6.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.PaymentCycleId IS NULL

	IF NOT EXISTS(SELECT 1 FROM temp_PayrollFile)
		SET @ErrorMsg='There are no beneficiaries valid for payment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	INSERT INTO @PSPs(PSPCode)
	SELECT DISTINCT PSPCode FROM temp_PayrollFile

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='PAYROLL_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_PayrollFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	--SELECT @SQLStmt
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	DROP TABLE temp_PayrollFile;
	
	--RECORDING THE FILE
	SET @SysCode='File Type'
	SET @SysDetailCode='PAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SET @FileCreationId=IDENT_CURRENT('FileCreation')

	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYROLLEXCONF'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PaymentStageId=@SystemCodeDetailId2
	   ,T1.FileCreationId=@FileCreationId
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	SELECT @FileCreationId AS FileCreationId
	SET NOCOUNT OFF
END
GO



IF NOT OBJECT_ID('PayrollFileDownloaded') IS NULL	DROP PROC PayrollFileDownloaded
GO
CREATE PROC PayrollFileDownloaded
	@FileCreationId int
   ,@FileChecksum varchar(64)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @RowCount int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Id=@FileCreationId AND FileChecksum=@FileChecksum)
		SET @ErrorMsg='Please specify valid FileCreationId corresponding FileCheksum'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId AND T2.UserId=@UserId)
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM FileDownload WHERE FileCreationId=@FileCreationId AND DownloadedBy=@UserId)
			INSERT INTO FileDownload(FileCreationId,FileChecksum,DownloadedBy,DownloadedOn)
			SELECT @FileCreationId,@FileChecksum,@UserId,GETDATE() AS DownloadedOn

		SET @SysCode='Payroll Stage'
		SET @SysDetailCode='POSTPAYROLL'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId1
		FROM PaymentCycleDetail T1
		WHERE T1.FileCreationId=@FileCreationId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FilePassword
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT FilePassword FROM FileCreation WHERE Id=@FileCreationId
	END
END
GO




IF NOT OBJECT_ID('PSPPayrollTrx') IS NULL	DROP PROC PSPPayrollTrx
GO
CREATE PROC PSPPayrollTrx
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@HhId varchar(20)
   ,@PSPCode nvarchar(20)
   ,@PSPBranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AmountTransfered money
   ,@TrxNo nvarchar(50)
   ,@TrxDate datetime
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @AccountId int
	DECLARE @PaymentAmount money
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int


	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @AccountId=T3.AccountNo,@PaymentAmount=(T1.EntitlementAmount+T1.AdjustmentAmount)
	FROM Prepayroll T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhId=T2.HhId
					   INNER JOIN BeneficiaryAccount T3 ON T2.Id=T3.HhEnrolmentId AND T3.StatusId=@SystemCodeDetailId1
					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id AND T4.IsActive=1
					   INNER JOIN PSP T5 ON T4.PSPId=T5.Id AND T5.IsActive=1
					   LEFT JOIN (
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedApvBy IS NULL
								) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T5.Code=@PSPCode AND T4.Code=@PSPBranchCode AND T3.AccountNo=@AccountNo AND T3.AccountName=@AccountName AND T6.PaymentCycleId IS NULL 

				   
	IF ISNULL(@AccountId,0)=0
		SET @ErrorMsg='Please specify valid beneficiary account details'
	ELSE IF NOT (@PaymentAmount-@AmountTransfered=0)
		SET @ErrorMsg='Please specify valid transfer amount'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@PSPCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	IF EXISTS(SELECT 1 
			  FROM Payment T1
			  WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.HhId=@HhId
			  )
	BEGIN
		UPDATE T1
		SET T1.TrxAmount=@AmountTransfered
		   ,T1.TrxNo=@TrxNo
		   ,T1.TrxDate=@TrxDate
		FROM Payment T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.HhId=@HhId
	END
	ELSE
	BEGIN
		INSERT INTO Payment(PaymentCycleId,HhId,TrxAmount,TrxNo,TrxDate)
		SELECT @PaymentCycleId,@HhId,@AmountTransfered,@TrxNo,@TrxDate
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('PSPAccountActivity') IS NULL	DROP PROC PSPAccountActivity
GO
CREATE PROC PSPAccountActivity
	@PSPCode nvarchar(20)
   ,@PSPBranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@HasUniqueTrx bit
   ,@MonthNo tinyint
   ,@Year smallint
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @AccountId int
	DECLARE @MonthId int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @AccountId=T1.Id
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id AND T2.IsActive=1
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id AND T3.IsActive=1
	WHERE T3.Code=@PSPCode AND T2.Code=@PSPBranchCode AND T1.AccountNo=@AccountNo AND T1.AccountName=@AccountName
				   
	SELECT @MonthId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
	WHERE T2.Code='Calendar Months' AND T1.Code=@MonthNo


	IF ISNULL(@AccountId,0)=0
		SET @ErrorMsg='Please specify valid beneficiary account details'
	ELSE IF (ISNULL(@MonthId,0)<=0)
		SET @ErrorMsg='Please specify valid MonthNo parameter'	
	ELSE IF (ISNULL(@Year,0)<YEAR(GETDATE())-1 OR ISNULL(@Year,0)>YEAR(GETDATE()))
		SET @ErrorMsg='Please specify valid Year parameter'	
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@PSPCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	IF EXISTS(SELECT 1 
			  FROM BeneficiaryAccountActivity T1
			  WHERE T1.BeneAccountId=@AccountId AND T1.MonthId=@MonthId AND T1.[Year]=@Year
			  )
	BEGIN
		UPDATE T1
		SET T1.HasUniqueTrx=@HasUniqueTrx
		FROM BeneficiaryAccountActivity T1
		WHERE T1.BeneAccountId=@AccountId AND T1.MonthId=@MonthId AND T1.[Year]=@Year
	END
	ELSE
	BEGIN
		INSERT INTO BeneficiaryAccountActivity(BeneAccountId,MonthId,[Year],HasUniqueTrx)
		SELECT @AccountId,@MonthId,@Year,@HasUniqueTrx
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END
GO



