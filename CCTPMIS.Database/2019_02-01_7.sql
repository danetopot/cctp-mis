-- getusergrouprights 2

IF NOT OBJECT_ID('AddAuditTrail') IS NULL
DROP PROC AddAuditTrail
GO

CREATE PROC AddAuditTrail  
    @IPAddress varchar(15)  
   ,@MACAddress varchar(17)=NULL  
   ,@IMEI bigint=NULL  
   ,@ModuleRightCode varchar(64)  
   ,@Description varchar(64)   =''
   ,@TableName varchar(64)  
   ,@UserAgent varchar(64)  
   ,@Key1 int  
   ,@Key2 int=NULL  
   ,@Key3 int=NULL  
   ,@Key4 int=NULL  
   ,@Key5 int=NULL  
   ,@Record nvarchar(MAX)  
   ,@WasSuccessful bit  
   ,@UserId int = null  
AS  
BEGIN  
 DECLARE @ModuleRightId int  
 DECLARE @TableId int  
 DECLARE @SysCode varchar(20)  
 DECLARE @NoOfRows int  
 DECLARE @ErrorMsg varchar(128)  
  

 SELECT @ModuleRightId=T1.Id FROM ModuleRight T1 INNER JOIN Module T2 ON T1.ModuleId=T2.Id   
			 INNER JOIN SystemCodeDetail T3 ON T1.RightId=T3.Id  
			 INNER JOIN SystemCode T4 ON T3.SystemCodeId=T4.Id  
WHERE UPPER(T2.Name+':'+T1.Description)=UPPER(@ModuleRightCode)  
  
 BEGIN TRAN  

 SELECT @TableId =  Id FROM  [CCTP-MIS_AUDIT].dbo.LogTable where TableName = @TableName

 IF(ISNULL(@TableId,0)=0)
 insert into  [CCTP-MIS_AUDIT].dbo.LogTable(ModuleRightId,TableName)
 SELECT @ModuleRightId, @TableName
 
 DECLARE @FullName varchar(100)
 DECLARE @Email varchar(100)
 
 select @FullName= concat(Firstname,' ',MiddleName, '', Surname), @Email = Email from [User] where Id = @userId
 
 SELECT @TableId =  Id FROM  [CCTP-MIS_AUDIT].dbo. LogTable where TableName = @TableName
 
 INSERT INTO  [CCTP-MIS_AUDIT].dbo. AuditTrail(UserId,LogTime,IPAddress,MACAddress,IMEI,ModuleRightId,Key1,Key2,Key3,Key4,Key5,Record,TableId,WasSuccessful,FullName,Module,Description,UserName)  
 SELECT @UserId,GETDATE(),@IPAddress,@MACAddress,@IMEI,@ModuleRightId,@Key1,@Key2,@Key3,@Key4,@Key5,@Record  ,@TableId,@WasSuccessful,@FullName,@ModuleRightCode, @Description,@Email
 
 COMMIT TRAN     
 SELECT  0   as StatusId
END  
GO

 
