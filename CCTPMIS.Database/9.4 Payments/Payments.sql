IF OBJECT_ID('temp_Exceptions') IS NOT NULL	
DROP TABLE temp_Exceptions;
CREATE TABLE temp_Exceptions
(
	PaymentCycleId int NOT NULL,
	PaymentCycle varchar (128) NOT NULL,
	ProgrammeId int NOT NULL,
	Programme nvarchar (100) NOT NULL,
	EnrolmentNo int NOT NULL,
	ProgrammeNo varchar (50) NOT NULL,
	BeneFirstName varchar (50) NOT NULL,
	BeneMiddleName varchar (50) NULL,
	BeneSurname varchar (50) NOT NULL,
	BeneDoB datetime NOT NULL,
	BeneSex varchar (20) NOT NULL,
	BeneNationalIDNo varchar (30) NULL,
	PriReciCanReceivePayment bit NOT NULL,
	IsInvalidBene smallint NOT NULL,
	IsDuplicateBeneWithin smallint NOT NULL,
	IsDuplicateBeneAcross smallint NOT NULL,
	IsIneligibleBene smallint NOT NULL,
	CGFirstName varchar (50) NULL,
	CGMiddleName varchar (50) NULL,
	CGSurname varchar (50) NULL,
	CGDoB datetime NULL,
	CGSex varchar (20) NULL,
	CGNationalIDNo varchar (30) NULL,
	IsInvalidCG smallint NOT NULL,
	IsDuplicateCGWithin smallint NOT NULL,
	IsDuplicateCGAcross smallint NOT NULL,
	IsIneligibleCG smallint NOT NULL,
	HhStatus varchar (100) NOT NULL,
	IsIneligibleHh smallint NOT NULL,
	AccountNumber varchar (50) NULL,
	IsInvalidAccount smallint NOT NULL,
	IsDormantAccount smallint NOT NULL,
	PaymentCardNumber varchar (50) NULL,
	IsInvalidPaymentCard smallint NOT NULL,
	PaymentZone varchar (30) NOT NULL,
	PaymentZoneCommAmt money NOT NULL,
	ConseAccInactivity tinyint NOT NULL,
	EntitlementAmount money NOT NULL,
	AdjustmentAmount money NOT NULL,
	IsSuspiciousAmount smallint NOT NULL,
	SubLocation varchar (30) NULL,
	Location varchar (30) NULL,
	Division varchar (30) NULL,
	District varchar (30) NULL,
	County varchar (30) NULL,
	Constituency varchar (30) NULL,
	WasTrxSuccessful bit NULL,
	TrxNarration nvarchar (128) NULL
    ,
	CONSTRAINT PK_temp_Exceptions PRIMARY KEY (PaymentCycleId,EnrolmentNo)
)
GO



IF NOT OBJECT_ID('GeneratePrepayrollExceptionsFile') IS NULL 
	DROP PROC GeneratePrepayrollExceptionsFile
GO

CREATE PROC GeneratePrepayrollExceptionsFile
	@PaymentCycleId int
   ,
	@ProgrammeId tinyint
   ,
	@FilePath nvarchar(128)
   ,
	@DBServer varchar(30)
   ,
	@DBName varchar(30)
   ,
	@DBUser varchar(30)
   ,
	@DBPassword nvarchar(30)
   ,
	@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults
	(
		FileExists int
	   ,
		FileIsDirectory int
	   ,
		FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists, @FileIsDirectory=FileIsDirectory, @FileParentDirExists=FileParentDirExists
	FROM #FileResults

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYMENTCYCLEAPV'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF EXISTS(SELECT 1
		FROM PaymentCycleDetail
		WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId IN(@SystemCodeDetailId1))
		OR EXISTS(SELECT 1
		FROM PaymentCycleDetail
		WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND (PrePayrollBy IS NULL OR PrePayrollOn IS NULL))
		SET @ErrorMsg='The prepayroll seems not to have been executed on the specified payment cycle'
	ELSE IF EXISTS(SELECT 1
	FROM PaymentCycleDetail T1 INNER JOIN FileCreation T2 ON T1.ExceptionsFileId=T2.Id
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The prepayroll exceptions file seem to already have been generated'
	ELSE IF NOT EXISTS(SELECT 1
	FROM [User]
	WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'

	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	INSERT INTO temp_Exceptions
		(PaymentCycleId,PaymentCycle,ProgrammeId,Programme,EnrolmentNo,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBeneWithin,IsDuplicateBeneAcross,IsIneligibleBene,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCGWithin,IsDuplicateCGAcross,IsIneligibleCG,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZone,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount,SubLocation,Location,Division,District,County,Constituency)

	SELECT T1.PaymentCycleId, T2.[Description] AS PaymentCycle, T1.ProgrammeId, T3.Name AS Programme, TT4.Id AS EnrolmentNo, TT4.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),TT4.ProgrammeNo)))+CONVERT(varchar(6),TT4.ProgrammeNo) AS ProgrammeNo
		, T1.BeneFirstName, T1.BeneMiddleName, T1.BeneSurname, T1.BeneDoB, T4.Code AS BeneSex, T1.BeneNationalIDNo, T1.PriReciCanReceivePayment, CASE WHEN(ISNULL(T7.ActionedApvBy,0))>0 THEN -1 ELSE CONVERT(bit,ISNULL(T7.PersonId,0)) END AS IsInvalidBene
		, CASE WHEN(ISNULL(T8.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8.PersonId,0)) END AS IsDuplicateBeneWithin
		, CASE WHEN(ISNULL(T8A.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8A.PersonId,0)) END AS IsDuplicateBeneAcross		
		, CASE WHEN(ISNULL(T9.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T9.HhId,0)) END AS IsIneligibleBene
		, T1.CGFirstName, T1.CGMiddleName, T1.CGSurname, T1.CGDoB, T5.Code AS CGSex, T1.CGNationalIDNo, CASE WHEN(ISNULL(T11.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T11.PersonId,0)) END AS IsInvalidCG
		, CASE WHEN(ISNULL(T12.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12.PersonId,0)) END AS IsDuplicateCGWithin
		, CASE WHEN(ISNULL(T12A.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12A.PersonId,0)) END AS IsDuplicateCGAcross		
		, CASE WHEN(ISNULL(T13.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T13.HhId,0)) END AS IsIneligibleCG
		, T6.[Description] AS HhStatus, CASE WHEN(ISNULL(T19.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T19.HhId,0)) END AS IsIneligibleHh, TT15.AccountNo AS AccountNumber, CASE WHEN(ISNULL(T15.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T15.HhId,0)) END AS IsInvalidAccount, CASE WHEN(ISNULL(T17.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T17.HhId,0)) END AS IsDormantAccount, TT16.PaymentCardNo AS PaymentCardNumber, CASE WHEN(ISNULL(T16.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T16.HhId,0)) END AS IsInvalidPaymentCard, T21.Name AS PaymentZone, T1.PaymentZoneCommAmt, T1.ConseAccInactivity, T1.EntitlementAmount, T1.AdjustmentAmount, CASE WHEN(ISNULL(T18.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T18.HhId,0)) END AS IsSuspiciousAmount, T25.SubLocation, T25.Location, T25.Division, T25.District, T25.County, T25.Constituency
	FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
		INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
		INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
		INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
		LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
		INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
		LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy, T1.PersonId
		FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_DUPLICATEBENEIDIN
								  )  T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId

		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy, T1.PersonId
		FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_DUPLICATEBENEIDACC
									)  T8A ON T1.PaymentCycleId=T8A.PaymentCycleId AND T1.ProgrammeId=T8A.ProgrammeId AND T1.BenePersonId=T8A.PersonId

		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy
		FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
		LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId AND T1.HhId=T11.HhId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy, T1.PersonId
		FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_DUPLICATECGIDIN
								  )  T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId

		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy, T1.PersonId
		FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_DUPLICATECGIDACC
								  )  T12A ON T1.PaymentCycleId=T12A.PaymentCycleId AND T1.ProgrammeId=T12A.ProgrammeId AND T1.CGPersonId=T12A.PersonId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
		FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId
		LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
		LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
		LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.BenePaymentCardId=TT16.Id
		LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
		FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
		WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
		FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
		WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
		LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
		FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
		WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId
		LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
		INNER JOIN Household T22 ON T1.HhId=T22.Id
		INNER JOIN HouseholdSubLocation T23 ON T22.Id=T23.HhId
		INNER JOIN GeoMaster T24 ON T23.GeoMasterId=T24.Id AND T24.IsDefault=1
		INNER JOIN (
									SELECT T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T7.Name AS Division, T9.Name AS District, T10.Name AS County, T11.Name AS Constituency
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T7 ON T2.DivisionId=T7.Id
			INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
			INNER JOIN District T9 ON T8.DistrictId=T9.Id
			INNER JOIN County T10 ON T8.CountyId=T10.Id
			INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
								) T25 ON T23.SubLocationId=T25.SubLocationId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	DELETE 
	FROM temp_Exceptions 
	WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		AND NOT (IsInvalidBene=1 OR IsDuplicateBeneWithin=1 OR IsDuplicateBeneAcross=1 OR IsIneligibleBene=1 OR IsInvalidCG=1 OR IsDuplicateCGWithin=1 OR IsDuplicateCGAcross=1 OR IsIneligibleCG=1 OR IsIneligibleHh=1 OR IsInvalidAccount=1 OR IsDormantAccount=1 OR IsInvalidPaymentCard=1 OR IsSuspiciousAmount=1)

	IF EXISTS(SELECT 1
	FROM temp_Exceptions
	WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SELECT @FileName='EXCEPTIONS_PREPAYROLL_'+Code+'_'
		FROM Programme
		WHERE Id=@ProgrammeId

		SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
		SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
		SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
		SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName
		SET @FileExtension='.csv'
		SET @FileCompression='.rar'


		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM vw_temp_PrepayrollExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='EXCEPTION'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		IF NOT EXISTS(SELECT 1
		FROM FileCreation
		WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation
			(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
		SELECT @FileName+@FileCompression AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, @FilePassword AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id
		FROM FileCreation
		WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.ExceptionsFileId=@FileCreationId
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	SELECT ISNULL(@FileCreationId,0) AS FileCreationId
	SET NOCOUNT OFF
END

GO




CREATE VIEW [dbo].[vw_temp_PrepayrollExceptions]
AS
	SELECT PaymentCycle,Programme,EnrolmentNo,ProgrammeNo
		,BeneFirstName,BeneMiddleName,BeneSurname,CONVERT(varchar,BeneDoB,106) AS BeneDoB,BeneSex,BeneNationalIDNo AS BeneIDNo,CASE WHEN(IsInvalidBene<0) THEN 'Yes But Actioned' WHEN(IsInvalidBene=1) THEN 'Yes' ELSE '' END AS BeneIPRSMismatch
		,CASE WHEN(IsDuplicateBeneWithin<0) THEN 'Yes But Actioned' WHEN(IsDuplicateBeneWithin=1) THEN 'Yes' ELSE ' ' END AS BeneDuplicatedWithin
		,CASE WHEN(IsDuplicateBeneAcross<0) THEN 'Yes But Actioned' WHEN(IsDuplicateBeneAcross=1) THEN 'Yes' ELSE ' ' END AS BeneDuplicatedAcross
		,CASE WHEN(IsIneligibleBene<0) THEN 'Yes But Actioned' WHEN(IsIneligibleBene=1) THEN 'Yes' ELSE '' END AS BeneMissingOrIneligible
		,ISNULL(CGFirstName,'') AS CGFirstName,ISNULL(CGMiddleName,'') AS CGMiddleName,ISNULL(CGSurname,'') AS CGSurname,CONVERT(varchar,ISNULL(CGDoB,''),106) AS CGDoB,ISNULL(CGSex,'') AS CGSex,ISNULL(CGNationalIDNo,'') AS CGIDNo,CASE WHEN(IsInvalidCG<0) THEN 'Yes But Actioned' WHEN(IsInvalidCG=1) THEN 'Yes' ELSE '' END AS CGIPRSMismatch
		,CASE WHEN(IsDuplicateCGWithin<0) THEN 'Yes But Actioned' WHEN(IsDuplicateCGWithin=1) THEN 'Yes' ELSE ' ' END AS CGDuplicatedWithin
		,CASE WHEN(IsDuplicateCGAcross<0) THEN 'Yes But Actioned' WHEN(IsDuplicateCGAcross=1) THEN 'Yes' ELSE ' ' END AS CGDuplicatedAcross		
		,CASE WHEN(IsIneligibleCG<0) THEN 'Yes But Actioned' WHEN(IsIneligibleCG=1) THEN 'Yes' ELSE '' END AS CGMissing
		,replace(HhStatus,',',' ') HhStatus,CASE WHEN(IsIneligibleHh<0) THEN 'Yes But Actioned' WHEN(IsIneligibleHh=1) THEN 'Yes' ELSE ' ' END AS HhSuspended,ISNULL(AccountNumber,'') AS AccountNumber,CASE WHEN(IsInvalidAccount<0) THEN 'Yes But Actioned' WHEN(IsInvalidAccount=1) THEN 'Yes' ELSE '' END AS WithoutBankAccount,CASE WHEN(IsDormantAccount<0) THEN 'Yes But Actioned' WHEN(IsDormantAccount=1) THEN 'Yes' ELSE '' END AS DormantAccount,ISNULL(PaymentCardNumber,'') AS PaymentCardNumber,CASE WHEN(IsInvalidPaymentCard<0) THEN 'Yes But Actioned' WHEN(IsInvalidPaymentCard=1) THEN 'Yes' ELSE '' END AS WithoutPaymentCard,ConseAccInactivity AS AccountInactivityMonths,EntitlementAmount,AdjustmentAmount AS AdditionalAmount
		,CASE WHEN(IsSuspiciousAmount<0) THEN 'Yes But Actioned' WHEN(IsSuspiciousAmount=1) THEN 'Yes' ELSE ' ' END AS SuspiciousPayment
		,SubLocation,Location,Division,District,County,Constituency
	FROM temp_Exceptions
GO


