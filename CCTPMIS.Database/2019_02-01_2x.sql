

IF NOT OBJECT_ID('ListingException') IS NULL 
DROP TABLE ListingException 
GO

IF NOT OBJECT_ID('ListingPlanHH') IS NULL 
DROP TABLE ListingPlanHH 
GO

IF NOT OBJECT_ID('ListingAccept') IS NULL 
DROP TABLE ListingAccept 
GO

IF NOT OBJECT_ID('ListingPlanProgramme') IS NULL
DROP TABLE ListingPlanProgramme 
GO

IF NOT OBJECT_ID('TargetPlanProgramme') IS NULL	
DROP TABLE TargetPlanProgramme 
GO

IF NOT OBJECT_ID('IPRSCache') IS NULL	
DROP TABLE IPRSCache
GO

IF NOT OBJECT_ID('temp_IPRSCache') IS NULL	
DROP TABLE temp_IPRSCache
GO

IF NOT OBJECT_ID('temp_IPRSExport') IS NULL	
DROP TABLE temp_IPRSExport
GO

IF NOT OBJECT_ID('temp_IPRS') IS NULL	
DROP TABLE temp_IPRS
GO


IF NOT OBJECT_ID('ComValListingPlanHH') IS NULL	
DROP TABLE ComValListingPlanHH
GO

IF NOT OBJECT_ID('ComValListingException') IS NULL	
DROP TABLE ComValListingException
GO
IF NOT OBJECT_ID('ComValListingAccept') IS NULL	
DROP TABLE ComValListingAccept
GO




IF NOT OBJECT_ID('HouseholdRegProgramme') IS NULL	
DROP TABLE HouseholdRegProgramme
GO

IF NOT OBJECT_ID('HouseholdRegMemberDisability') IS NULL	
DROP TABLE HouseholdRegMemberDisability
GO

IF NOT OBJECT_ID('HouseholdRegMember') IS NULL	
DROP TABLE HouseholdRegMember
GO

IF NOT OBJECT_ID('HouseholdRegCharacteristic') IS NULL	
DROP TABLE HouseholdRegCharacteristic
GO

IF NOT OBJECT_ID('HouseholdReg') IS NULL	
DROP TABLE HouseholdReg
GO

IF NOT OBJECT_ID('HouseholdRegAccept') IS NULL	
DROP TABLE HouseholdRegAccept
GO

IF NOT OBJECT_ID('temp_RegExceptions') IS NULL	
DROP TABLE temp_RegExceptions
GO

IF NOT OBJECT_ID('TargetPlan') IS NULL	
DROP TABLE TargetPlan 
GO



CREATE TABLE IPRSCache(
   First_Name      VARCHAR(30) NOT NULL 
  ,Surname         VARCHAR(30)
  ,Middle_Name     VARCHAR(30)
  ,ID_Number       VARCHAR(20)
  ,Gender          VARCHAR(2)
  ,Date_of_Birth   VARCHAR(30)
  ,Date_of_Issue   VARCHAR(30)
  ,Place_of_Birth  VARCHAR(150)
  ,Serial_Number   BIGINT DEFAULT NULL
  ,[Address]        VARCHAR(300)
  ,[Status]         VARCHAR(30)
  ,DateCached     DATETIME NOT NULL DEFAULT GETDATE()
  ,CONSTRAINT IX_IPRSCache_ID_Number UNIQUE NONCLUSTERED (ID_Number)
  ,CONSTRAINT PK_IPRSCache PRIMARY KEY (ID_Number)
)
GO



CREATE TABLE temp_IPRSCache
(
	First_Name VARCHAR(30) NOT NULL,
	Surname VARCHAR(30) NULL,
	Middle_Name VARCHAR(30) NULL,
	ID_Number VARCHAR(20) NULL,
	Gender VARCHAR(2) NULL,
	Date_of_Birth VARCHAR(30) NULL,
	Date_of_Issue VARCHAR(30) NULL,
	Place_of_Birth VARCHAR(150) NULL,
	Serial_Number BIGINT   NULL,
	[Address] VARCHAR(300) NULL,
	[Status] VARCHAR(30) NULL,
	DateCached DATETIME NOT NULL
)    
GO

CREATE TABLE  temp_IPRSExport(
	FirstName varchar(20) NOT NULL,
	Middlename varchar(20) NULL,
	Surname varchar(20) NOT NULL,
	NationalIdNo varchar(20) NOT NULL,
	Sex varchar(2) NOT NULL,
	Date_Of_Birth datetime NOT NULL,
	TargetPlanId int NOT NULL
)  
GO

CREATE TABLE  temp_IPRS(
	IdNumber varchar(20)   NULL,
	Names varchar(70) NULL,
	ListingAcceptId int   NULL,
	ComValListingAcceptId int   NULL
)  
GO

CREATE TABLE  temp_RegExceptions(
	Id int NOT NULL,
	BeneName varchar(50)   NULL,
	BeneSex VARCHAR(5)   NULL,
	BeneDob date NULL,
	BeneNationalIdNo varchar(20) NULL,
	IPRS_IDNo bigint NULL,
	IPRS_Name varchar(50) NULL,
	IPRS_Sex VARCHAR(5)   NULL,
	IPRS_DoB date NULL,
	Bene_IDNoExists varchar(5) NOT NULL,
	Bene_FirstNameExists varchar(5) NOT NULL,
	Bene_MiddleNameExists varchar(5) NOT NULL,
	Bene_SurnameExists varchar(5) NOT NULL,
	Bene_DoBMatch varchar(5) NOT NULL,
	Bene_DoBYearMatch varchar(5) NOT NULL,
	Bene_SexMatch varchar(5) NOT NULL,
	CgName varchar(50) NULL,
	CgSex VARCHAR(5)   NULL,
	CgDob date NOT NULL,
	CgNationalIdNo varchar(20) NULL,
	IPRS_CG_IDNo varchar(20) NULL,
	IPRS_CG_Name varchar(50) NULL,
	IPRS_CG_Sex varchar(2) NULL,
	IPRS_CG_DoB date NULL,
	CG_IDNoExists varchar(5) NOT NULL,
	CG_FirstNameExists varchar(5) NOT NULL,
	CG_MiddleNameExists varchar(5) NOT NULL,
	CG_SurnameExists varchar(5) NOT NULL,
	CG_DoBMatch varchar(5) NOT NULL,
	CG_DoBYearMatch varchar(5) NOT NULL,
	CG_SexMatch varchar(5) NOT NULL,
	County varchar(30) NULL,
	Constituency varchar(30) NULL,
	District varchar(30) NULL,
	Division varchar(30) NULL,
	Location varchar(30) NULL,
	SubLocation varchar(30) NULL,
	TargetPlanId int NOT NULL,
 )
GO

CREATE TABLE TargetPlan(
	Id INT NOT NULL IDENTITY(1,1)
   ,[Name] varchar(30) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,[Start] datetime NOT NULL
   ,[End] datetime NOT NULL
   ,CategoryId int NOT NULL
   ,StatusId int NOT NULL
   ,CreatedBy INT NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy INT NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,ClosedBy int NULL
   ,ClosedOn datetime NULL
   ,FinalizeBy int NULL
   ,FinalizeOn datetime NULL
   ,FinalizeApvBy int NULL
   ,FinalizeApvOn datetime NULL  
   ,ExceptionBy int NULL
   ,ExceptionOn datetime NULL
   ,ValBy int NULL
   ,ValOn datetime NULL
   ,ValApvBy int NULL
   ,ValApvOn datetime NULL
   ,ExceptionsFileId int NULL
   ,ValidationFileId int NULL
   ,ComValBy int NULL
   ,ComValOn datetime NULL
   ,ComValApvBy int NULL
   ,ComValApvOn datetime NULL
   ,PostRegExceptionBy int NULL
   ,PostRegExceptionOn datetime NULL
   ,PostRegExceptionsFileId int NULL
   ,CONSTRAINT PK_TargetPlan PRIMARY KEY (Id)
   ,CONSTRAINT FK_TargetPlan_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_FinalizeBy FOREIGN KEY (FinalizeBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_FinalizeApvBy FOREIGN KEY (FinalizeApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ComValBy FOREIGN KEY (ComValBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ComValApvBy FOREIGN KEY (ComValApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ValBy FOREIGN KEY (ValBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_ExceptionBy FOREIGN KEY (ExceptionBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_User_PostRegExceptionBy FOREIGN KEY (PostRegExceptionBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION  
   ,CONSTRAINT FK_TargetPlan_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_FileCreation_PostRegExceptionsFileId FOREIGN KEY (PostRegExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_FileCreation_ExceptionsFileId FOREIGN KEY (ExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_TargetPlan_FileCreation_ValidationFileId FOREIGN KEY (ValidationFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE TargetPlanProgramme(
	 ListingPlanId		INT NOT NULL
	,ProgrammeId		TINYINT NOT NULL
	,TargetPlanId		INT NOT NULL
	,CONSTRAINT PK_TargetPlanProgramme PRIMARY KEY (ListingPlanId,ProgrammeId,TargetPlanId)
	,CONSTRAINT FK_TargetPlanProgramme_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION
	,CONSTRAINT FK_TargetPlanProgramme_TargetPlan_ListingPlanId FOREIGN KEY (ListingPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION
	,CONSTRAINT FK_TargetPlanProgramme_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION
 )
GO

CREATE TABLE ListingPlanProgramme(
	 ProgrammeId		TINYINT NOT NULL
	,TargetPlanId		INT NOT NULL
	,CONSTRAINT PK_ListingPlanProgramme PRIMARY KEY (ProgrammeId,TargetPlanId)
	,CONSTRAINT FK_ListingPlanProgramme_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION
	,CONSTRAINT FK_ListingPlanProgramme_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION
 )
GO

CREATE TABLE ListingAccept(
	Id INT NOT NULL IDENTITY(1,1)
   ,AcceptById int NOT NULL
   ,AcceptDate DATETIME NOT NULL DEFAULT GETDATE()
   ,AcceptApvById INT  NULL
   ,AcceptApvDate DATETIME  NULL
   ,ReceivedHHs int NOT NULL                                                   
   ,BatchName VARCHAR(50) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,TargetPlanId INT NOT NULL
   ,PendingValidation INT NOT NULL DEFAULT 0
   ,IsValidated BIT NOT NULL DEFAULT 0
   ,CONSTRAINT PK_ListingAccept PRIMARY KEY (Id)
   ,CONSTRAINT FK_ListingAccept_User_AcceptById FOREIGN KEY (AcceptById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ListingAccept_User_AcceptApvById FOREIGN KEY (AcceptApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ListingAccept_User_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ListingAccept_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO

CREATE TABLE ListingPlanHH (
	 Id					INT  NOT NULL IDENTITY(1,1)
	,UniqueId			VARCHAR(36)  NOT NULL
	,StartTime			DATETIME NULL
	,EndTime			DATETIME NULL
	,ProgrammeId		TINYINT  NOT NULL
	,RegistrationDate	DATETIME NOT NULL
	,RegDate	DATETIME   NULL
	,SubLocationId		INT  NOT NULL
	,LocationId			INT  NOT NULL
	,Years				INT  NOT NULL
	,Months				INT  NOT NULL
	,TargetPlanId		INT  NOT NULL
	,EnumeratorId		INT  NOT NULL
	,BeneFirstName		VARCHAR(25) NULL
	,BeneMiddleName		VARCHAR(25)		 NULL
	,BeneSurname		VARCHAR(25)		NULL
	,BeneNationalIdNo	VARCHAR(15)   NULL
	,BenePhoneNumber	VARCHAR(15)   NULL
	,BeneSexId			INT				NULL
	,BeneDoB				DATETIME  NULL
	,CgFirstName		VARCHAR(25) NOT NULL
	,CgMiddleName		VARCHAR(25)   NULL
	,CgSurname			VARCHAR(25) NOT NULL
	,CgNationalIdNo		VARCHAR(15)  NOT NULL
	,CgSexId			INT  NOT NULL
	,CgDoB				DATETIME NOT NULL
	,HouseholdMembers	INT  NOT NULL
	,StatusId			INT  NOT NULL
	,Village			VARCHAR(50) NOT NULL
	,PhysicalAddress	VARCHAR(50) NOT NULL
	,NearestReligiousBuilding VARCHAR(50) NOT NULL
	,NearestSchool		VARCHAR(50) NOT NULL
	,Longitude			FLOAT NOT NULL
	,Latitude			FLOAT NOT NULL
	,SyncEnumeratorId   INT  NOT NULL
	,SyncDate				DATETIME NOT NULL  DEFAULT GETDATE()
	,EnumeratorDeviceId INT  NOT NULL
	,ListingAcceptId	INT NULL
	,RejectReason varchar(128) NULL
	,RejectById INT NULL
	,RejectDate DATETIME NULL
	,CgPhoneNumber	VARCHAR(50) NOT NULL
	,AppVersion	VARCHAR(10) NOT NULL
	,AppBuild	VARCHAR(10) NOT NULL

	,CONSTRAINT PK_ListingPlanHH PRIMARY KEY (Id)
	,CONSTRAINT FK_ListingPlanHH_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_RegPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_SystemCodeDetail_BeneSexId FOREIGN KEY (BeneSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_SystemCodeDetail_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_ListingAccept_ListingAcceptId FOREIGN KEY (ListingAcceptId) REFERENCES ListingAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_EnumeratorDevice_EnumeratorDeviceId FOREIGN KEY (EnumeratorDeviceId) REFERENCES EnumeratorDevice(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ListingPlanHH_User_RejectedById FOREIGN KEY (RejectById) REFERENCES [USER](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO
CREATE TABLE ListingException(
   Id				INT  NOT NULL PRIMARY KEY 
  ,BeneName			VARCHAR(50)   NULL
  ,BeneSex			VARCHAR(2) NULL
  ,BeneDob			VARCHAR(50) NULL
  ,BeneNationalIdNo	VARCHAR(20) NULL
  ,IPRS_IDNo		BIGINT   NULL
  ,IPRS_Name		VARCHAR(50)  NULL
  ,IPRS_Sex			VARCHAR(2) NULL
  ,IPRS_DoB			DATE   NULL
  ,Bene_IDNoExists	BIT  NOT NULL
  ,Bene_FirstNameExists   BIT  NOT NULL
  ,Bene_MiddleNameExists  BIT  NOT NULL
  ,Bene_SurnameExists	 BIT  NOT NULL
  ,Bene_DoBMatch          BIT  NOT NULL
  ,Bene_DoBYearMatch      BIT  NOT NULL
  ,Bene_SexMatch          BIT  NOT NULL
  ,CgName                VARCHAR(50) NULL
  ,CgSex                 VARCHAR(2) NULL
  ,CgDob                 DATE NOT NULL
  ,CgNationalIdNo        VARCHAR(20) NULL
  ,IPRS_CG_IDNo          VARCHAR(20) NULL
  ,IPRS_CG_Name          VARCHAR(50) NULL
  ,IPRS_CG_Sex           VARCHAR(2) NULL
  ,IPRS_CG_DoB           DATE   NULL
  ,CG_IDNoExists         BIT  NOT NULL
  ,CG_FirstNameExists    BIT  NOT NULL
  ,CG_MiddleNameExists   BIT  NOT NULL
  ,CG_SurnameExists      BIT  NOT NULL
  ,CG_DoBMatch           BIT  NOT NULL
  ,CG_DoBYearMatch       BIT  NOT NULL
  ,CG_SexMatch           BIT  NOT NULL
  ,TargetPlanId		     INT  NOT NULL
  ,DateValidated         DATETIME NOT NULL
)
GO

CREATE TABLE ComValListingAccept(
	Id INT NOT NULL IDENTITY(1,1)
   ,AcceptById int NOT NULL
   ,AcceptDate DATETIME NOT NULL DEFAULT GETDATE()
   ,AcceptApvById INT  NULL
   ,AcceptApvDate DATETIME  NULL
   ,ReceivedHHs int NOT NULL                                                   
   ,BatchName VARCHAR(50) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,TargetPlanId INT NOT NULL
   ,PendingValidation INT NOT NULL DEFAULT 0
   ,IsValidated BIT NOT NULL DEFAULT 0
   ,CONSTRAINT PK_ComValListingAccept PRIMARY KEY (Id)
   ,CONSTRAINT FK_ComValListingAccept_User_AcceptById FOREIGN KEY (AcceptById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ComValListingAccept_User_AcceptApvById FOREIGN KEY (AcceptApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ComValListingAccept_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ComValListingAccept_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO

CREATE TABLE ComValListingPlanHH (
	 Id					INT  NOT NULL
	,UniqueId			VARCHAR(36) NOT NULL
	,StartTime			DATETIME NULL
	,EndTime			DATETIME NULL
	,ProgrammeId		TINYINT  NOT NULL
	,RegistrationDate	DATETIME NOT NULL
	,SubLocationId		INT  NOT NULL
	,LocationId			INT  NOT NULL
	,Years				INT  NOT NULL
	,Months				INT  NOT NULL
	,TargetPlanId		INT  NOT NULL
	,EnumeratorId		INT  NOT NULL
	,BeneFirstName		VARCHAR(25) NULL
	,BeneMiddleName		VARCHAR(25)		 NULL
	,BeneSurname		VARCHAR(25)		NULL
	,BeneNationalIdNo	VARCHAR(15)   NULL
	,BenePhoneNumber	VARCHAR(15)   NULL
	,BeneSexId			INT				NULL
	,BeneDoB				DATETIME  NULL
	,CgFirstName		VARCHAR(25) NOT NULL
	,CgMiddleName		VARCHAR(25)   NULL
	,CgSurname			VARCHAR(25) NOT NULL
	,CgNationalIdNo		VARCHAR(15)  NOT NULL
	,CgSexId			INT  NOT NULL
	,CgDoB				DATETIME NOT NULL
	,HouseholdMembers	INT  NOT NULL
	,StatusId			INT  NOT NULL
	,Village			VARCHAR(50) NOT NULL
	,PhysicalAddress	VARCHAR(50) NOT NULL
	,NearestReligiousBuilding VARCHAR(50) NOT NULL
	,NearestSchool		VARCHAR(50) NOT NULL
	,Longitude			FLOAT NOT NULL
	,Latitude			FLOAT NOT NULL
	,SyncEnumeratorId   INT  NOT NULL
	,SyncDate			 DATETIME NOT NULL  DEFAULT GETDATE()
	,EnumeratorDeviceId INT  NOT NULL
	,ComValListingAcceptId	INT NULL
	,ComValDate	DATETIME   NULL  DEFAULT GETDATE()
	,SyncUpDate				DATETIME NOT NULL
	,RegDate				DATETIME   NULL
	,UpdateDate				DATETIME   NULL
	,DownloadDate			DATETIME   NULL
	,AppVersion	VARCHAR(10) NOT NULL
	,AppBuild	VARCHAR(10) NOT NULL
	,CgPhoneNumber	VARCHAR(50)   NULL
	,CONSTRAINT PK_ComValListingPlanHH PRIMARY KEY (Id)
	,CONSTRAINT FK_ComValListingPlanHH_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_SystemCodeDetail_BeneSexId FOREIGN KEY (BeneSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_SystemCodeDetail_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_ComValListingAccept_ListingAcceptId FOREIGN KEY (ComValListingAcceptId) REFERENCES ComValListingAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComValListingPlanHH_EnumeratorDevice_EnumeratorDeviceId FOREIGN KEY (EnumeratorDeviceId) REFERENCES EnumeratorDevice(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
 )
GO

CREATE TABLE ComValListingException(
   Id				INT  NOT NULL PRIMARY KEY 
  ,BeneName			VARCHAR(50)   NULL
  ,BeneSex			VARCHAR(2) NULL
  ,BeneDob			VARCHAR(50) NULL
  ,BeneNationalIdNo	VARCHAR(20) NULL
  ,IPRS_IDNo		BIGINT   NULL
  ,IPRS_Name		VARCHAR(50)  NULL
  ,IPRS_Sex			VARCHAR(2) NULL
  ,IPRS_DoB			DATE   NULL
  ,Bene_IDNoExists	BIT  NOT NULL
  ,Bene_FirstNameExists   BIT  NOT NULL
  ,Bene_MiddleNameExists  BIT  NOT NULL
  ,Bene_SurnameExists	 BIT  NOT NULL
  ,Bene_DoBMatch          BIT  NOT NULL
  ,Bene_DoBYearMatch      BIT  NOT NULL
  ,Bene_SexMatch          BIT  NOT NULL
  ,CgName                VARCHAR(50) NULL
  ,CgSex                 VARCHAR(2) NULL
  ,CgDob                 DATE NOT NULL
  ,CgNationalIdNo        VARCHAR(20) NULL
  ,IPRS_CG_IDNo          VARCHAR(20) NULL
  ,IPRS_CG_Name          VARCHAR(50) NULL
  ,IPRS_CG_Sex           VARCHAR(2) NULL
  ,IPRS_CG_DoB           DATE   NULL
  ,CG_IDNoExists         BIT  NOT NULL
  ,CG_FirstNameExists    BIT  NOT NULL
  ,CG_MiddleNameExists   BIT  NOT NULL
  ,CG_SurnameExists      BIT  NOT NULL
  ,CG_DoBMatch           BIT  NOT NULL
  ,CG_DoBYearMatch       BIT  NOT NULL
  ,CG_SexMatch           BIT  NOT NULL
  ,TargetPlanId		     INT  NOT NULL
  ,DateValidated         DATETIME NOT NULL
)
GO


/********HOUSEHOLD REGISTRATION **********/

CREATE TABLE HouseholdRegAccept(
    Id INT NOT NULL IDENTITY(1,1)
   ,AcceptById int NOT NULL
   ,AcceptDate DATETIME NOT NULL DEFAULT GETDATE()
   ,AcceptApvById INT  NULL
   ,AcceptApvDate DATETIME  NULL
   ,ReceivedHHs int NOT NULL                                                   
   ,BatchName VARCHAR(50) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,TargetPlanId INT NOT NULL
   ,PendingValidation INT NOT NULL DEFAULT 0
   ,IsValidated BIT NOT NULL DEFAULT 0
   ,CONSTRAINT PK_HouseholdRegAccept PRIMARY KEY (Id)
   ,CONSTRAINT FK_HouseholdRegAccept_User_AcceptById FOREIGN KEY (AcceptById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegAccept_User_AcceptApvById FOREIGN KEY (AcceptApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegAccept_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegAccept_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO

CREATE TABLE HouseholdReg(
     Id INT NOT NULL 
    ,SubLocationId INT NOT NULL
    ,UniqueId   varchar(36) NULL
    ,LocationId INT NOT NULL
    ,ProgrammeId tinyint NOT NULL
    ,Village varchar(50) NULL
    ,StatusId INT NOT NULL
    ,Years int NOT NULL
    ,Months int NOT NULL
    ,NearestReligiousBuilding   varchar(50) NULL
    ,NearestSchool   varchar(50) NULL
    ,PhysicalAddress varchar(50) NULL
    ,StartTime  datetime NOT NULL
    ,EndTime datetime NOT NULL
    ,RegistrationDate datetime NOT NULL
    ,SyncUpDate DATETIME NOT NULL
    ,DownloadDate DATETIME NOT NULL
    ,RegDate1 DATETIME   NULL
    ,RegDate2 DATETIME   NULL
    ,RegDate3 DATETIME   NULL
    ,Longitude float NULL
    ,Latitude float NULL
    ,TargetPlanId INT NOT NULL
    ,EnumeratorId INT NOT NULL
    ,SyncEnumeratorId INT NOT NULL
    ,InterviewStatusId INT NOT NULL
    ,InterviewResultId INT NOT NULL
    ,HouseholdRegAcceptId INT NULL
    ,EnumeratorDeviceId INT  NOT NULL
    ,SyncDate DATETIME NOT NULL  DEFAULT GETDATE()
    ,AppVersion	VARCHAR(10) NOT NULL
    ,AppBuild	VARCHAR(10) NOT NULL
	,PMTScore DECIMAL NULL
    ,CONSTRAINT PK_HouseholdReg PRIMARY KEY (Id)
    ,CONSTRAINT FK_HouseholdReg_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdReg_TargetPlan_TargetPlanId FOREIGN KEY (TargetPlanId) REFERENCES TargetPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdReg_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdReg_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION	
)
GO

CREATE TABLE HouseholdRegCharacteristic(
         Id INT NOT NULL
        ,HouseholdMembers INT NOT NULL	
        ,HabitableRooms INT NOT NULL DEFAULT(1)
        ,IsOwnedId INT NOT NULL
        ,TenureStatusId INT not null
        ,RoofMaterialId INT NOT NULL
        ,WallMaterialId INT NOT NULL
        ,FloorMaterialId INT NOT NULL
        ,DwellingUnitRiskId INT NOT NULL
        ,WaterSourceId INT NOT NULL
        ,ToiletTypeId INT NOT NULL
        ,CookingFuelSourceId INT NOT NULL
        ,LightingSourceId INT NOT NULL
        ,IsTelevisionId INT NOT NULL		
        ,IsMotorcycleId INT NOT NULL
        ,IsTukTukId INT NOT NULL
        ,IsRefrigeratorId INT NOT NULL
        ,IsCarId INT NOT NULL
        ,IsMobilePhoneId INT NOT NULL
        ,IsBicycleId INT NOT NULL
        ,ExoticCattle INT NOT NULL
        ,IndigenousCattle INT NOT NULL
        ,Sheep INT NOT NULL
        ,Goats INT NOT NULL
        ,Camels INT NOT NULL
        ,Donkeys INT NOT NULL
        ,Pigs INT NOT NULL
        ,Chicken INT NOT NULL		
        ,Deaths INT NULL
        ,LiveBirths INT NULL	
        ,HouseholdConditionId INT not null
        ,IsSkippedMealId int not null
        ,NsnpProgrammesId int not null
        ,OtherProgrammesId int not null
        ,OtherProgrammeNames VARCHAR(100) NULL
        ,BenefitTypeId int   null
        ,LastReceiptAmount MONEY NULL
        ,InKindBenefit VARCHAR(100) NULL
        ,CONSTRAINT PK_HouseholdRegCharacteristic PRIMARY KEY (Id)
        ,CONSTRAINT FK_HouseholdRegCharacteristic_HouseholdReg_HouseholdRegId FOREIGN KEY (Id) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_TenureStatusId FOREIGN KEY (TenureStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_RoofMaterialId FOREIGN KEY (RoofMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_WallMaterialId FOREIGN KEY (WallMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_FloorMaterialId FOREIGN KEY (FloorMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_DwellingUnitRiskId FOREIGN KEY (DwellingUnitRiskId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_WaterSourceId FOREIGN KEY (WaterSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_ToiletTypeId FOREIGN KEY (ToiletTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_CookingFuelSourceId FOREIGN KEY (CookingFuelSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_LightingSourceId FOREIGN KEY (LightingSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_HouseholdConditionId FOREIGN KEY (HouseholdConditionId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_BenefitTypeId FOREIGN KEY (BenefitTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      
)
GO

CREATE TABLE HouseholdRegProgramme(
    HouseholdRegId INT NOT NULL
   ,ProgrammeId INT NOT NULL
   ,CONSTRAINT PK_HouseholdRegOtherProgramme PRIMARY KEY (HouseholdRegId,ProgrammeId)
   ,CONSTRAINT FK_HouseholdRegOtherProgramme_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegOtherProgramme_SystemCodeDetail_OtherProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE HouseholdRegMember(
     Id INT NOT NULL IDENTITY(1,1)
    ,HouseholdRegId INT NOT NULL
    ,MemberId varchar(36) NOT NULL
    ,CareGiverId varchar(36)  NULL
    ,SpouseId varchar(36)  NULL
    ,FirstName varchar(50) NOT NULL
    ,MiddleName varchar(50) NULL
    ,Surname varchar(50) NOT NULL
    ,IdentificationTypeId INT NOT NULL
    ,IdentificationNumber varchar(30) NULL
    ,PhoneNumber varchar(30) NULL
    ,RelationshipId INT NOT NULL 
    ,SpouseInHouseholdId INT   NULL 
    ,SexId INT NOT NULL
    ,DateOfBirth datetime NOT NULL
    ,MaritalStatusId INT NOT NULL
    ,ChronicIllnessStatusId INT NULL
    ,DisabilityCareStatusId INT NULL
    ,DisabilityTypeId INT NULL
    ,EducationLevelId INT NULL  
    ,FatherAliveStatusId INT NULL
    ,FormalJobNgoId INT NULL
    ,LearningStatusId INT NULL
    ,WorkTypeId INT NULL
    ,MotherAliveStatusId INT NULL 
    ,CONSTRAINT PK_HouseholdRegMember PRIMARY KEY (Id)
    ,CONSTRAINT FK_HouseholdRegMember_HouseholdReg_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION 
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_IdentificationTypeId FOREIGN KEY (IdentificationTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_RelationshipId FOREIGN KEY (RelationshipId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_SexId FOREIGN KEY (SexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_MaritalStatusId FOREIGN KEY (MaritalStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_FatherAliveId FOREIGN KEY (FatherAliveStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_MotherAliveId FOREIGN KEY (MotherAliveStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_ChronicIllnessStatusId FOREIGN KEY (ChronicIllnessStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_DisabilityTypeId FOREIGN KEY (DisabilityTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_EducationLevelId FOREIGN KEY (EducationLevelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE HouseholdRegMemberDisability(
      HouseholdRegMemberId VARCHAR(36) NOT NULL
     ,DisabilityId INT NOT NULL
     ,HouseholdRegId INT NOT NULL
     ,CONSTRAINT PK_HouseholdRegMemberDisability PRIMARY KEY (HouseholdRegMemberId,DisabilityId)
     ,CONSTRAINT FK_HouseholdRegMemberDisability_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
     ,CONSTRAINT FK_HouseholdRegMemberDisability_SystemCodeDetail_DisabilityId FOREIGN KEY (DisabilityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION

)
GO

