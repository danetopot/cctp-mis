﻿-- <Migration ID="dcb923a1-f257-4ecb-80bc-e6478ff15eac" />
GO

PRINT N'Dropping foreign keys from [dbo].[BeneAccountMonthlyActivityDetail]'
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] DROP CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneAccountMonthlyActivity_MonthlyActivityId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] DROP CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneficiaryAccount_BeneAccountId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] DROP CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] DROP CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[BeneAccountMonthlyActivity]'
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_SystemCodeDetail_MonthId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_User_CreatedBy]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_User_ModifiedBy]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_User_ApvBy]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [FK_BeneAccountMonthlyActivity_User_ClosedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[BeneficiaryPaymentCard]'
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_BeneficiaryAccount_BeneAccountId]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_Person_PriReciId]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_SystemCodeDetail_PriReciSexId]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_Person_SecReciId]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_SystemCodeDetail_SecReciSexId]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_User_CreatedBy]
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [FK_BeneficiaryPaymentCard_User_ApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Prepayroll]'
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_BeneficiaryAccount_BeneAccountId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_BeneficiaryPaymentCard_BenePaymentCardId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_Household_HHId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_PaymentCycleDetail_PaymentCycleId_ProgrammeId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_PaymentZone_PaymentZoneId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_Person_BenePersonId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_Person2]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_SystemCodeDetail_BeneSexId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_SystemCodeDetail_CGSexId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_SystemCodeDetail_HhStatusId]
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [FK_Prepayroll_SubLocation_SubLocationId]
GO
PRINT N'Dropping foreign keys from [dbo].[BeneficiaryAccount]'
GO
ALTER TABLE [dbo].[BeneficiaryAccount] DROP CONSTRAINT [FK_BeneficiaryAccount_HouseholdEnrolment_HhEnrolmentId]
GO
ALTER TABLE [dbo].[BeneficiaryAccount] DROP CONSTRAINT [FK_BeneficiaryAccount_PSPBranch_PSPBranchId]
GO
ALTER TABLE [dbo].[BeneficiaryAccount] DROP CONSTRAINT [FK_BeneficiaryAccount_SystemCodeDetail_StatusId]
GO
PRINT N'Dropping foreign keys from [dbo].[BulkTransferDetail]'
GO
ALTER TABLE [dbo].[BulkTransferDetail] DROP CONSTRAINT [FK_BulkTransferDetail_BulkTransfer_BulkTransferId]
GO
PRINT N'Dropping foreign keys from [dbo].[ChangeDocument]'
GO
ALTER TABLE [dbo].[ChangeDocument] DROP CONSTRAINT [FK_ChangeDocument_Change_ChangeId]
GO
ALTER TABLE [dbo].[ChangeDocument] DROP CONSTRAINT [FK_ChangeDocument_User_CreatedById]
GO
ALTER TABLE [dbo].[ChangeDocument] DROP CONSTRAINT [FK_ChangeDocument_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[ChangeNote]'
GO
ALTER TABLE [dbo].[ChangeNote] DROP CONSTRAINT [FK_ChangeNote_Change_ChangeId]
GO
ALTER TABLE [dbo].[ChangeNote] DROP CONSTRAINT [FK_ChangeNote_User_CreatedById]
GO
ALTER TABLE [dbo].[ChangeNote] DROP CONSTRAINT [FK_ChangeNote_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[Change]'
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [FK_Change_Programme_ProgrammeId]
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [FK_Change_HouseholdEnrolment_HhEnrolmentId]
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [FK_Change_BeneSex_BeneSexId]
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [FK_Change_CgSex_CgSexId]
GO
PRINT N'Dropping foreign keys from [dbo].[ComplaintDocument]'
GO
ALTER TABLE [dbo].[ComplaintDocument] DROP CONSTRAINT [FK_ComplaintDocument_SystemCodeDetail_ComplaintId]
GO
ALTER TABLE [dbo].[ComplaintDocument] DROP CONSTRAINT [FK_ComplaintDocument_User_CreatedById]
GO
ALTER TABLE [dbo].[ComplaintDocument] DROP CONSTRAINT [FKComplaintDocument_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[ComplaintNote]'
GO
ALTER TABLE [dbo].[ComplaintNote] DROP CONSTRAINT [FK_ComplaintNote_SystemCodeDetail_ComplaintId]
GO
ALTER TABLE [dbo].[ComplaintNote] DROP CONSTRAINT [FK_ComplaintNote_User_CreatedById]
GO
ALTER TABLE [dbo].[ComplaintNote] DROP CONSTRAINT [FK_ComplaintNote_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[Complaint]'
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_Programme_ProgrammeId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_HouseholdEnrolment_HhEnrolmentId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_SystemCodeDetail_ComplaintTypeId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_SystemCodeDetail_ReportedByTypeId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_User_CreatedBy]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_User_ApvBy]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_User_ResolvedBy]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_User_ClosedBy]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_SystemCodeDetail_ComplaintStatusId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_SystemCodeDetail_ComplaintLevelId]
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [FK_Complaint_SystemCodeDetail_SourceId]
GO
PRINT N'Dropping foreign keys from [dbo].[SubLocation]'
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [FK_SubLocation_Constituency_ConstituencyId]
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [FK_SubLocation_Location_LocationId]
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [FK_SubLocation_SystemCodeDetail_LocalityId]
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [FK_SubLocation_User_CreatedBy]
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [FK_SubLocation_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Ward]'
GO
ALTER TABLE [dbo].[Ward] DROP CONSTRAINT [FK_Ward_Constituency_ConstituencyId]
GO
ALTER TABLE [dbo].[Ward] DROP CONSTRAINT [FK_Ward_User_CreatedBy]
GO
ALTER TABLE [dbo].[Ward] DROP CONSTRAINT [FK_Ward_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Constituency]'
GO
ALTER TABLE [dbo].[Constituency] DROP CONSTRAINT [FK_Constituency_County_CountyId]
GO
ALTER TABLE [dbo].[Constituency] DROP CONSTRAINT [FK_Constituency_User_CreatedBy]
GO
ALTER TABLE [dbo].[Constituency] DROP CONSTRAINT [FK_Constituency_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Division]'
GO
ALTER TABLE [dbo].[Division] DROP CONSTRAINT [FK_Division_CountyDistrict_CountyDistrictId]
GO
ALTER TABLE [dbo].[Division] DROP CONSTRAINT [FK_Division_User_CreatedBy]
GO
ALTER TABLE [dbo].[Division] DROP CONSTRAINT [FK_Division_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[CountyDistrict]'
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [FK_CountyDistrict_County_CountyId]
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [FK_CountyDistrict_District_DistrictId]
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [FK_CountyDistrict_User_CreatedBy]
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [FK_CountyDistrict_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[County]'
GO
ALTER TABLE [dbo].[County] DROP CONSTRAINT [FK_County_GeoMaster_GeoMasterId]
GO
ALTER TABLE [dbo].[County] DROP CONSTRAINT [FK_County_User_CreatedBy]
GO
ALTER TABLE [dbo].[County] DROP CONSTRAINT [FK_County_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[DBBackup]'
GO
ALTER TABLE [dbo].[DBBackup] DROP CONSTRAINT [FK_DBBackup_User_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[District]'
GO
ALTER TABLE [dbo].[District] DROP CONSTRAINT [FK_District_GeoMaster_GeoMasterId]
GO
ALTER TABLE [dbo].[District] DROP CONSTRAINT [FK_District_User_CreatedBy]
GO
ALTER TABLE [dbo].[District] DROP CONSTRAINT [FK_District_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_Location_Division_DivisionId]
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_Location_Zone_PaymentZoneId]
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_Location_User_CreatedBy]
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_Location_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[EFCDocument]'
GO
ALTER TABLE [dbo].[EFCDocument] DROP CONSTRAINT [FK_EFCDocument_EFC_EFCId]
GO
ALTER TABLE [dbo].[EFCDocument] DROP CONSTRAINT [FK_EFCDocument_User_CreatedById]
GO
ALTER TABLE [dbo].[EFCDocument] DROP CONSTRAINT [FK_EFCDocument_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[EFCNote]'
GO
ALTER TABLE [dbo].[EFCNote] DROP CONSTRAINT [FK_EFCNote_EFC_EFCId]
GO
ALTER TABLE [dbo].[EFCNote] DROP CONSTRAINT [FK_EFCNote_User_CreatedById]
GO
ALTER TABLE [dbo].[EFCNote] DROP CONSTRAINT [FK_EFCNote_SystemCodeDetail_CategoryId]
GO
PRINT N'Dropping foreign keys from [dbo].[EFC]'
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_SystemCodeDetail_EFCTypeId]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_SystemCodeDetail_ReportedByTypeId]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_User_ClosedBy]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_SystemCodeDetail_SourceId]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_User_ApvBy]
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [FK_EFC_User_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [FK_ExpansionPlanDetail_ExpansionPlan_ExpansionPlanId]
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [FK_ExpansionPlanDetail_SystemCodeDetail_FinancialYearId]
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [FK_ExpansionPlanDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [FK_ExpansionPlanDetail_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[ExpansionPlan]'
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [FK_ExpansionPlan_ExpansionPlanMaster_ExpansionPlanMasterId]
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [FK_ExpansionPlan_Location_LocationId]
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [FK_ExpansionPlan_User_CreatedBy]
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [FK_ExpansionPlan_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[ExpansionPlanMaster]'
GO
ALTER TABLE [dbo].[ExpansionPlanMaster] DROP CONSTRAINT [FK_ExpansionPlanMaster_Programme_ProgrammeId]
GO
ALTER TABLE [dbo].[ExpansionPlanMaster] DROP CONSTRAINT [FK_ExpansionPlanMaste_User_CreatedBy]
GO
ALTER TABLE [dbo].[ExpansionPlanMaster] DROP CONSTRAINT [FK_ExpansionPlanMaster_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[FileDownload]'
GO
ALTER TABLE [dbo].[FileDownload] DROP CONSTRAINT [FK_FileDownload_FileCreation_FileCreationId]
GO
ALTER TABLE [dbo].[FileDownload] DROP CONSTRAINT [FK_FileDownload_User_DownloadedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_File_FileCreationId]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_Programme_ProgrammeId]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_SystemCodeDetail_RegGroupId]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_SystemCodeDetail_EnrolmentGroupId]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_User_CreatedBy]
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [FK_HouseholdEnrolmentPlan_User_ApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentCycle]'
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_FileCreation_ExceptionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_SystemCodeDetail_FinancialYearId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_SystemCodeDetail_FromMonthId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_SystemCodeDetail_ToMonthId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_Reconciliation_ReconciliationId]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_User_CreatedBy]
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [FK_PaymentCycle_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentCycleDetail]'
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_DuplicateIDActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_ExceptionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_FileCreationId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_IneligibleBeneficiaryActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_InvalidIDActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_InvalidPaymentAccountActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_InvalidPaymentCardActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_FileCreation_SuspiciousPaymentActionsFileId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_PaymentCycle_PaymentCycleId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_Programme]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_SystemCodeDetail_PaymentStageId]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PrePayrollBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PrepayrollApvBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PayrollBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PayrollVerBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PayrollApvBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PayrollExBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PayrollExConfBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PostPayrollBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_PostPayrollApvby]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_ReconciledBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_ReconciledApvBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [FK_PaymentCycleDetail_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[ReconciliationDetail]'
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [FK_ReconciliationDetail_FileCreation_BankStatementFileId]
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [FK_ReconciliationDetail_PSP_PSPId]
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [FK_ReconciliationDetail_Reconciliation_ReconciliationId]
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [FK_ReconciliationDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [FK_ReconciliationDetail_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[FileCreation]'
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [FK_FileCreation_SystemCodeDetail_TypeId]
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [FK_FileCreation_SystemCodeDetail_CreationTypeId]
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [FK_FileCreation_User1_TargetUserId]
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [FK_FileCreation_User2_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[FundsRequestDetail]'
GO
ALTER TABLE [dbo].[FundsRequestDetail] DROP CONSTRAINT [FK_FundsRequestDetail_FundsRequestId]
GO
ALTER TABLE [dbo].[FundsRequestDetail] DROP CONSTRAINT [FK_FundsRequestDetail_PSP]
GO
ALTER TABLE [dbo].[FundsRequestDetail] DROP CONSTRAINT [FK_FundsRequestDetail_ProgrammeId]
GO
PRINT N'Dropping foreign keys from [dbo].[FundsRequest]'
GO
ALTER TABLE [dbo].[FundsRequest] DROP CONSTRAINT [FK_FundsRequest_PaymentCycle_PaymentCycleId]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdSubLocation]'
GO
ALTER TABLE [dbo].[HouseholdSubLocation] DROP CONSTRAINT [FK_HouseholdSubLocation_GeoMaster_GeoMasterId]
GO
ALTER TABLE [dbo].[HouseholdSubLocation] DROP CONSTRAINT [FK_HouseholdSubLocation_Household_HhId]
GO
ALTER TABLE [dbo].[HouseholdSubLocation] DROP CONSTRAINT [FK_HouseholdSubLocation_SubLocation_SubLocationId]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdVillage]'
GO
ALTER TABLE [dbo].[HouseholdVillage] DROP CONSTRAINT [FK_HouseholdVillage_GeoMaster_GeoMasterId]
GO
ALTER TABLE [dbo].[HouseholdVillage] DROP CONSTRAINT [FK_HouseholdVillage_Household_HhId]
GO
ALTER TABLE [dbo].[HouseholdVillage] DROP CONSTRAINT [FK_HouseholdVillage_Village_VillageId]
GO
PRINT N'Dropping foreign keys from [dbo].[GeoMaster]'
GO
ALTER TABLE [dbo].[GeoMaster] DROP CONSTRAINT [FK_GeoMaster_User_CreatedBy]
GO
ALTER TABLE [dbo].[GeoMaster] DROP CONSTRAINT [FK_GeoMaster_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[GroupRight]'
GO
ALTER TABLE [dbo].[GroupRight] DROP CONSTRAINT [FK_GroupRight_UserGroup_UserGroupId]
GO
ALTER TABLE [dbo].[GroupRight] DROP CONSTRAINT [FK_GroupRight_ModuleRight_ModuleRightId]
GO
ALTER TABLE [dbo].[GroupRight] DROP CONSTRAINT [FK_GroupRight_User_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdEnrolment]'
GO
ALTER TABLE [dbo].[HouseholdEnrolment] DROP CONSTRAINT [FK_HouseholdEnrolment_HouseholdEnrolmentPlan_HhEnrolmentPlanId]
GO
ALTER TABLE [dbo].[HouseholdEnrolment] DROP CONSTRAINT [FK_HouseholdEnrolment_Household_HhId]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdMember]'
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_Household_HhId]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_Person_PersonId]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_SystemCodeDetail_RelationshipId]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_SystemCodeDetail_MemberRoleId]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_User_CreatedBy]
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [FK_HouseholdMember_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[HouseholdVillageElder]'
GO
ALTER TABLE [dbo].[HouseholdVillageElder] DROP CONSTRAINT [FK_HouseholdVillageElder_Household_HhId]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentAdjustment]'
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [FK_PaymentAdjustment_Household]
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [FK_PaymentAdjustment_PaymentCycle_PaymentCycleId]
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [FK_PaymentAdjustment_PaymentCycleDetail_PaymentCycleId_ProgrammeId]
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [FK_PaymentAdjustment_SystemCodeDetail_AdjustmentTypeId]
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [FK_PaymentAdjustment_User_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Household]'
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [FK_Household_Programme_ProgrammeId]
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [FK_Household_SystemCodeDetail_RegGroupId]
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [FK_Household_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [FK_Household_User_CreatedBy]
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [FK_Household_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[WardLocation]'
GO
ALTER TABLE [dbo].[WardLocation] DROP CONSTRAINT [FK_WardLocation_Location_LocationId]
GO
ALTER TABLE [dbo].[WardLocation] DROP CONSTRAINT [FK_WardLocation_User_CreatedBy]
GO
ALTER TABLE [dbo].[WardLocation] DROP CONSTRAINT [FK_WardLocation_User_ModifiedBy]
GO
ALTER TABLE [dbo].[WardLocation] DROP CONSTRAINT [FK_WardLocation_Ward_WardId]
GO
PRINT N'Dropping foreign keys from [dbo].[ModuleRight]'
GO
ALTER TABLE [dbo].[ModuleRight] DROP CONSTRAINT [FK_ModuleRight_Module]
GO
ALTER TABLE [dbo].[ModuleRight] DROP CONSTRAINT [FK_ModuleRight_SystemCodeDetail]
GO
PRINT N'Dropping foreign keys from [dbo].[PSPBranch]'
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [FK_PSPBranch_PSP]
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [FK_PSPBranch_SubLocation]
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [FK_PSPBranch_User1]
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [FK_PSPBranch_User2]
GO
PRINT N'Dropping foreign keys from [dbo].[PSP]'
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [FK_PSP_User_UserId]
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [FK_PSP_User_CreatedBy]
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [FK_PSP_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PasswordReset]'
GO
ALTER TABLE [dbo].[PasswordReset] DROP CONSTRAINT [FK_PasswordReset_User_UserId]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentAccountActivityMonth]'
GO
ALTER TABLE [dbo].[PaymentAccountActivityMonth] DROP CONSTRAINT [FK_PaymentAccountActivityMonth_PaymentCycle_PaymentCycleId]
GO
ALTER TABLE [dbo].[PaymentAccountActivityMonth] DROP CONSTRAINT [FK_PaymentAccountActivityMonth_SystemCodeDetail_MonthId]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentEnrolmentGroup]'
GO
ALTER TABLE [dbo].[PaymentEnrolmentGroup] DROP CONSTRAINT [FK_PaymentEnrolmentGroup_PaymentCycleDetail_PaymentCycleId_ProgrammeId]
GO
ALTER TABLE [dbo].[PaymentEnrolmentGroup] DROP CONSTRAINT [FK_PaymentEnrolemtnGroup_SystemCodeDetail_EnrolmentGroupId]
GO
PRINT N'Dropping foreign keys from [dbo].[PaymentZone]'
GO
ALTER TABLE [dbo].[PaymentZone] DROP CONSTRAINT [FK_PaymentZone_User_CreatedBy]
GO
ALTER TABLE [dbo].[PaymentZone] DROP CONSTRAINT [FK_PaymentZone_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Payment]'
GO
ALTER TABLE [dbo].[Payment] DROP CONSTRAINT [FK_Payment_Payroll_PaymentCycleId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[Payment] DROP CONSTRAINT [FK_Payment_User_CreatedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Payroll]'
GO
ALTER TABLE [dbo].[Payroll] DROP CONSTRAINT [FK_Payroll_Prepayroll_PaymentCycleId_ProgrammeId_HhId]
GO
PRINT N'Dropping foreign keys from [dbo].[PersonPension]'
GO
ALTER TABLE [dbo].[PersonPension] DROP CONSTRAINT [FK_PersonPension_Person_PersonId]
GO
PRINT N'Dropping foreign keys from [dbo].[PersonSocialAssistance]'
GO
ALTER TABLE [dbo].[PersonSocialAssistance] DROP CONSTRAINT [FK_PersonSocialAssistance_Person_PersonId]
GO
ALTER TABLE [dbo].[PersonSocialAssistance] DROP CONSTRAINT [FK_PersonSocialAssistance_SystemCodeDetailId_OtherSPProgrammeId]
GO
ALTER TABLE [dbo].[PersonSocialAssistance] DROP CONSTRAINT [FK_PersonSocialAssistance_SystemCodeDetailId_BenefitTypeId]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollDuplicateID]'
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [FK_PrepayrollDuplicateID_Person_PersonId]
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [FK_PrepayrollDuplicateID_Prepayroll_PaymentCycleId_ProgrammeId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [FK_PrepayrollDuplicateID_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [FK_PrepayrollDuplicateID_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [FK_PrepayrollDuplicateID_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollInvalidID]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [FK_PrepayrollInvalidID_Person_PersonId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [FK_PrepayrollInvalidID_Prepayroll_PaymentCycleId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [FK_PrepayrollInvalidID_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [FK_PrepayrollInvalidID_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [FK_PrepayrollInvalidID_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [FK_Person_User_CreatedBy]
GO
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [FK_Person_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollIneligible]'
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [FK_PrepayrollIneligible_Prepayroll_PaymentCycleId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [FK_PrepayrollIneligible_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [FK_PrepayrollIneligible_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [FK_PrepayrollIneligible_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollInvalidPaymentAccount]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [FK_PrepayrollNoPaymentAccount_Prepayroll_PaymentCycleId_ProgrameId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [FK_PrepayrollNoPaymentAccount_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [FK_PrepayrollNoPaymentAccount_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [FK_PrepayrollNoPaymentAccount_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollInvalidPaymentCard]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [FK_PrepayrollNoPaymentCard_Prepayroll_PaymentCycleId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [FK_PrepayrollNoPaymentCard_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [FK_PrepayrollNoPaymentCard_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [FK_PrepayrollNoPaymentCard_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[PrepayrollSuspicious]'
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [FK_PrepayrollSuspicious_Prepayroll_PaymentCycleId_ProgrammeId_HhId]
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [FK_PrepayrollSuspicious_SystemCodeDetail_ExceptionTypeId]
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [FK_PrepayrollSuspicious_User_ActionedBy]
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [FK_PrepayrollSuspicious_User_ActionedApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_SystemCodeDetail_BeneficiaryTypeId]
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_SystemCodeDetail_PrimaryRecipientId]
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_SystemCodeDetail_SecondaryRecipientId]
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_SystemCodeDetail_PaymentFrequencyId]
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_User_CreatedBy]
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [FK_Programme_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Reconciliation]'
GO
ALTER TABLE [dbo].[Reconciliation] DROP CONSTRAINT [FK_Reconciliation_SystemCodeDetail_StatusId]
GO
ALTER TABLE [dbo].[Reconciliation] DROP CONSTRAINT [FK_Reconciliation_User_CreatedBy]
GO
ALTER TABLE [dbo].[Reconciliation] DROP CONSTRAINT [FK_Reconciliation_User_ModifiedBy]
GO
ALTER TABLE [dbo].[Reconciliation] DROP CONSTRAINT [FK_Reconciliation_User_ApvBy]
GO
PRINT N'Dropping foreign keys from [dbo].[Village]'
GO
ALTER TABLE [dbo].[Village] DROP CONSTRAINT [FK_Village_SubLocation_SubLocationId]
GO
ALTER TABLE [dbo].[Village] DROP CONSTRAINT [FK_Village_User_CreatedBy]
GO
ALTER TABLE [dbo].[Village] DROP CONSTRAINT [FK_Village_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[SystemCodeDetail]'
GO
ALTER TABLE [dbo].[SystemCodeDetail] DROP CONSTRAINT [FK_SystemCodeDetail_SystemCode_SystemCodeId]
GO
ALTER TABLE [dbo].[SystemCodeDetail] DROP CONSTRAINT [FK_SystemCodeDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[SystemCodeDetail] DROP CONSTRAINT [FK_SystemCodeDetail_User_ModifiedBy]
GO
PRINT N'Dropping foreign keys from [dbo].[UserClaim]'
GO
ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_UserClaim_User_UserId]
GO
PRINT N'Dropping foreign keys from [dbo].[UserGroup]'
GO
ALTER TABLE [dbo].[UserGroup] DROP CONSTRAINT [FK_UserGroup_UserGroupProfile_UserGroupProfileId]
GO
PRINT N'Dropping foreign keys from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_UserGroup_UserGroupId]
GO
PRINT N'Dropping foreign keys from [dbo].[UserLogin]'
GO
ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UserLogin_User_UserId]
GO
PRINT N'Dropping foreign keys from [dbo].[UserRole]'
GO
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User_UserId]
GO
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role_RoleId]
GO
PRINT N'Dropping constraints from [dbo].[BeneAccountMonthlyActivityDetail]'
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] DROP CONSTRAINT [PK_BeneAccountMonthlyActivityDetail]
GO
PRINT N'Dropping constraints from [dbo].[BeneAccountMonthlyActivity]'
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivity] DROP CONSTRAINT [PK_BeneAccountMonthlyActivity]
GO
PRINT N'Dropping constraints from [dbo].[BeneficiaryAccount]'
GO
ALTER TABLE [dbo].[BeneficiaryAccount] DROP CONSTRAINT [PK_BeneficiaryAccount]
GO
PRINT N'Dropping constraints from [dbo].[BeneficiaryAccount]'
GO
ALTER TABLE [dbo].[BeneficiaryAccount] DROP CONSTRAINT [IX_BeneficiaryAccount_PSPBranchId_AccountNo]
GO
PRINT N'Dropping constraints from [dbo].[BeneficiaryPaymentCard]'
GO
ALTER TABLE [dbo].[BeneficiaryPaymentCard] DROP CONSTRAINT [PK_BeneficiaryPaymentCard]
GO
PRINT N'Dropping constraints from [dbo].[BulkTransferDetail]'
GO
ALTER TABLE [dbo].[BulkTransferDetail] DROP CONSTRAINT [PK_BulkTransferDetail]
GO
PRINT N'Dropping constraints from [dbo].[BulkTransferDetail]'
GO
ALTER TABLE [dbo].[BulkTransferDetail] DROP CONSTRAINT [DF__BulkTrans__Creat__7FA0E47B]
GO
PRINT N'Dropping constraints from [dbo].[BulkTransfer]'
GO
ALTER TABLE [dbo].[BulkTransfer] DROP CONSTRAINT [PK_BulkTransfer]
GO
PRINT N'Dropping constraints from [dbo].[BulkTransfer]'
GO
ALTER TABLE [dbo].[BulkTransfer] DROP CONSTRAINT [DF__BulkTrans__Creat__7CC477D0]
GO
PRINT N'Dropping constraints from [dbo].[ChangeDocument]'
GO
ALTER TABLE [dbo].[ChangeDocument] DROP CONSTRAINT [PK_ChangeDocument]
GO
PRINT N'Dropping constraints from [dbo].[ChangeDocument]'
GO
ALTER TABLE [dbo].[ChangeDocument] DROP CONSTRAINT [DF__ChangeDoc__Creat__64ECEE3F]
GO
PRINT N'Dropping constraints from [dbo].[ChangeNote]'
GO
ALTER TABLE [dbo].[ChangeNote] DROP CONSTRAINT [PK_ChangeNote]
GO
PRINT N'Dropping constraints from [dbo].[Change]'
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [PK_Change]
GO
PRINT N'Dropping constraints from [dbo].[Change]'
GO
ALTER TABLE [dbo].[Change] DROP CONSTRAINT [DF__Change__CreatedO__597B3B93]
GO
PRINT N'Dropping constraints from [dbo].[ComplaintDocument]'
GO
ALTER TABLE [dbo].[ComplaintDocument] DROP CONSTRAINT [PK_ComplaintDocument]
GO
PRINT N'Dropping constraints from [dbo].[ComplaintDocument]'
GO
ALTER TABLE [dbo].[ComplaintDocument] DROP CONSTRAINT [DF__Complaint__Creat__4E0988E7]
GO
PRINT N'Dropping constraints from [dbo].[ComplaintNote]'
GO
ALTER TABLE [dbo].[ComplaintNote] DROP CONSTRAINT [PK_ComplaintNote]
GO
PRINT N'Dropping constraints from [dbo].[ComplaintNote]'
GO
ALTER TABLE [dbo].[ComplaintNote] DROP CONSTRAINT [DF__Complaint__Creat__53C2623D]
GO
PRINT N'Dropping constraints from [dbo].[Complaint]'
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [PK_Complaint]
GO
PRINT N'Dropping constraints from [dbo].[Complaint]'
GO
ALTER TABLE [dbo].[Complaint] DROP CONSTRAINT [DF__Complaint__Creat__40AF8DC9]
GO
PRINT N'Dropping constraints from [dbo].[Constituency]'
GO
ALTER TABLE [dbo].[Constituency] DROP CONSTRAINT [PK_Constituency]
GO
PRINT N'Dropping constraints from [dbo].[Constituency]'
GO
ALTER TABLE [dbo].[Constituency] DROP CONSTRAINT [IX_Constituency_CountyId_Code]
GO
PRINT N'Dropping constraints from [dbo].[CountyDistrict]'
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [PK_CountyDistrict]
GO
PRINT N'Dropping constraints from [dbo].[CountyDistrict]'
GO
ALTER TABLE [dbo].[CountyDistrict] DROP CONSTRAINT [IX_CountyDistrict_CountyId_DistrictId]
GO
PRINT N'Dropping constraints from [dbo].[County]'
GO
ALTER TABLE [dbo].[County] DROP CONSTRAINT [PK_County]
GO
PRINT N'Dropping constraints from [dbo].[County]'
GO
ALTER TABLE [dbo].[County] DROP CONSTRAINT [IX_County_GeoMasterId_Code]
GO
PRINT N'Dropping constraints from [dbo].[District]'
GO
ALTER TABLE [dbo].[District] DROP CONSTRAINT [PK_District]
GO
PRINT N'Dropping constraints from [dbo].[District]'
GO
ALTER TABLE [dbo].[District] DROP CONSTRAINT [IX_District_GeoMasterId_Code]
GO
PRINT N'Dropping constraints from [dbo].[Division]'
GO
ALTER TABLE [dbo].[Division] DROP CONSTRAINT [PK_Division]
GO
PRINT N'Dropping constraints from [dbo].[Division]'
GO
ALTER TABLE [dbo].[Division] DROP CONSTRAINT [IX_Division_CountyDistrictId_Code]
GO
PRINT N'Dropping constraints from [dbo].[EFCDocument]'
GO
ALTER TABLE [dbo].[EFCDocument] DROP CONSTRAINT [PK_EFCDocument]
GO
PRINT N'Dropping constraints from [dbo].[EFCNote]'
GO
ALTER TABLE [dbo].[EFCNote] DROP CONSTRAINT [PK_EFCNote]
GO
PRINT N'Dropping constraints from [dbo].[EFC]'
GO
ALTER TABLE [dbo].[EFC] DROP CONSTRAINT [PK_EFC]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [PK_ExpansionPlanDetail]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [IX_ExpansionPlanDetail_ExpansionPlanId_FinancialYearId]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [DF__Expansion__Enrol__2665ABE1]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [DF__Expansion__Scale__2759D01A]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanDetail]'
GO
ALTER TABLE [dbo].[ExpansionPlanDetail] DROP CONSTRAINT [DF__Expansion__Scale__284DF453]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanMaster]'
GO
ALTER TABLE [dbo].[ExpansionPlanMaster] DROP CONSTRAINT [PK_ExpansionPlanMaster]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlanMaster]'
GO
ALTER TABLE [dbo].[ExpansionPlanMaster] DROP CONSTRAINT [IX_ExpansionPlanMaster_Code_ProgrammeId]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlan]'
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [PK_ExpansionPlan]
GO
PRINT N'Dropping constraints from [dbo].[ExpansionPlan]'
GO
ALTER TABLE [dbo].[ExpansionPlan] DROP CONSTRAINT [IX_ExpansionPlan_ExpansionPlanMasterId_LocationId]
GO
PRINT N'Dropping constraints from [dbo].[FileCreation]'
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [PK_FileCreation]
GO
PRINT N'Dropping constraints from [dbo].[FileCreation]'
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [IX_FileCreation_Name]
GO
PRINT N'Dropping constraints from [dbo].[FileCreation]'
GO
ALTER TABLE [dbo].[FileCreation] DROP CONSTRAINT [DF__FileCreat__IsSha__5F9E293D]
GO
PRINT N'Dropping constraints from [dbo].[FileDownload]'
GO
ALTER TABLE [dbo].[FileDownload] DROP CONSTRAINT [PK_FileDownload]
GO
PRINT N'Dropping constraints from [dbo].[FundsRequestDetail]'
GO
ALTER TABLE [dbo].[FundsRequestDetail] DROP CONSTRAINT [PK_FundsRequestDetail]
GO
PRINT N'Dropping constraints from [dbo].[FundsRequest]'
GO
ALTER TABLE [dbo].[FundsRequest] DROP CONSTRAINT [PK_FundsRequest]
GO
PRINT N'Dropping constraints from [dbo].[FundsRequest]'
GO
ALTER TABLE [dbo].[FundsRequest] DROP CONSTRAINT [IX_FundsRequest_PaymentCycleId_PSPId]
GO
PRINT N'Dropping constraints from [dbo].[GeoMaster]'
GO
ALTER TABLE [dbo].[GeoMaster] DROP CONSTRAINT [PK_GeoMaster]
GO
PRINT N'Dropping constraints from [dbo].[GeoMaster]'
GO
ALTER TABLE [dbo].[GeoMaster] DROP CONSTRAINT [IX_GeoMaster_Name]
GO
PRINT N'Dropping constraints from [dbo].[GeoMaster]'
GO
ALTER TABLE [dbo].[GeoMaster] DROP CONSTRAINT [DF__GeoMaster__IsDef__3C89F72A]
GO
PRINT N'Dropping constraints from [dbo].[GroupRight]'
GO
ALTER TABLE [dbo].[GroupRight] DROP CONSTRAINT [PK_GroupRight]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [PK_HouseholdEnrolmentPlan]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [DF__Household__RegGr__6A1BB7B0]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [DF__Household__BeneH__6B0FDBE9]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [DF__Household__ExpPl__6C040022]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolmentPlan]'
GO
ALTER TABLE [dbo].[HouseholdEnrolmentPlan] DROP CONSTRAINT [DF__Household__ExpPl__6CF8245B]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolment]'
GO
ALTER TABLE [dbo].[HouseholdEnrolment] DROP CONSTRAINT [PK_HouseholdEnrolment]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolment]'
GO
ALTER TABLE [dbo].[HouseholdEnrolment] DROP CONSTRAINT [IX_HouseholdEnrolment_HhId]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdEnrolment]'
GO
ALTER TABLE [dbo].[HouseholdEnrolment] DROP CONSTRAINT [IX_HouseholdEnrolment_BeneProgNoPrefix_ProgrammeNo]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdMember]'
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [PK_HouseholdMember]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdMember]'
GO
ALTER TABLE [dbo].[HouseholdMember] DROP CONSTRAINT [IX_HouseholdMember_HhId_PersonId]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdSubLocation]'
GO
ALTER TABLE [dbo].[HouseholdSubLocation] DROP CONSTRAINT [PK_HouseholdSubLocation]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdSubLocation]'
GO
ALTER TABLE [dbo].[HouseholdSubLocation] DROP CONSTRAINT [IX_HouseholdSubLocation_HhId_GeoMasterId_SubLocationId]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdVillage]'
GO
ALTER TABLE [dbo].[HouseholdVillage] DROP CONSTRAINT [PK_HouseholdVillage]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdVillage]'
GO
ALTER TABLE [dbo].[HouseholdVillage] DROP CONSTRAINT [IX_HouseholdVillage_GeoMasterId_VillageId]
GO
PRINT N'Dropping constraints from [dbo].[Household]'
GO
ALTER TABLE [dbo].[Household] DROP CONSTRAINT [PK_Household]
GO
PRINT N'Dropping constraints from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [PK_Location]
GO
PRINT N'Dropping constraints from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [IX_Location_DivisionId_Code]
GO
PRINT N'Dropping constraints from [dbo].[ModuleRight]'
GO
ALTER TABLE [dbo].[ModuleRight] DROP CONSTRAINT [PK_ModuleRight]
GO
PRINT N'Dropping constraints from [dbo].[ModuleRight]'
GO
ALTER TABLE [dbo].[ModuleRight] DROP CONSTRAINT [IX_ModuleRight_ModuleId_RightId]
GO
PRINT N'Dropping constraints from [dbo].[Module]'
GO
ALTER TABLE [dbo].[Module] DROP CONSTRAINT [PK_Module]
GO
PRINT N'Dropping constraints from [dbo].[PSPBranch]'
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [PK_PSPBranch]
GO
PRINT N'Dropping constraints from [dbo].[PSPBranch]'
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [IX_PSPBranch_Code]
GO
PRINT N'Dropping constraints from [dbo].[PSPBranch]'
GO
ALTER TABLE [dbo].[PSPBranch] DROP CONSTRAINT [DF__PSPBranch__IsAct__125EB334]
GO
PRINT N'Dropping constraints from [dbo].[PSP]'
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [PK_PSP]
GO
PRINT N'Dropping constraints from [dbo].[PSP]'
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [IX_PSP_Code]
GO
PRINT N'Dropping constraints from [dbo].[PSP]'
GO
ALTER TABLE [dbo].[PSP] DROP CONSTRAINT [DF__PSP__IsActive__0BB1B5A5]
GO
PRINT N'Dropping constraints from [dbo].[PasswordReset]'
GO
ALTER TABLE [dbo].[PasswordReset] DROP CONSTRAINT [PK_PasswordReset]
GO
PRINT N'Dropping constraints from [dbo].[PaymentAccountActivityMonth]'
GO
ALTER TABLE [dbo].[PaymentAccountActivityMonth] DROP CONSTRAINT [PK_PaymentAccountActivityMonth]
GO
PRINT N'Dropping constraints from [dbo].[PaymentAdjustment]'
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [PK_PaymentAdjustment]
GO
PRINT N'Dropping constraints from [dbo].[PaymentAdjustment]'
GO
ALTER TABLE [dbo].[PaymentAdjustment] DROP CONSTRAINT [DF__PaymentAd__Amoun__42CCE065]
GO
PRINT N'Dropping constraints from [dbo].[PaymentCardBiometrics]'
GO
ALTER TABLE [dbo].[PaymentCardBiometrics] DROP CONSTRAINT [IX_PaymentCardBiometrics_BenePaymentCardId]
GO
PRINT N'Dropping constraints from [dbo].[PaymentCycleDetail]'
GO
ALTER TABLE [dbo].[PaymentCycleDetail] DROP CONSTRAINT [PK_PaymentCycleDetail]
GO
PRINT N'Dropping constraints from [dbo].[PaymentCycle]'
GO
ALTER TABLE [dbo].[PaymentCycle] DROP CONSTRAINT [PK_PaymentCycle]
GO
PRINT N'Dropping constraints from [dbo].[PaymentEnrolmentGroup]'
GO
ALTER TABLE [dbo].[PaymentEnrolmentGroup] DROP CONSTRAINT [PK_PaymentEnrolmentGroup]
GO
PRINT N'Dropping constraints from [dbo].[PaymentZone]'
GO
ALTER TABLE [dbo].[PaymentZone] DROP CONSTRAINT [PK_PaymentZone]
GO
PRINT N'Dropping constraints from [dbo].[PaymentZone]'
GO
ALTER TABLE [dbo].[PaymentZone] DROP CONSTRAINT [DF__PaymentZo__IsPer__36D11DD4]
GO
PRINT N'Dropping constraints from [dbo].[Payment]'
GO
ALTER TABLE [dbo].[Payment] DROP CONSTRAINT [PK_Payment]
GO
PRINT N'Dropping constraints from [dbo].[Payroll]'
GO
ALTER TABLE [dbo].[Payroll] DROP CONSTRAINT [PK_Payroll]
GO
PRINT N'Dropping constraints from [dbo].[PersonSocialAssistance]'
GO
ALTER TABLE [dbo].[PersonSocialAssistance] DROP CONSTRAINT [IX_PersonSocialAssistance_PersonId_OtherSPProgrammeId]
GO
PRINT N'Dropping constraints from [dbo].[PersonSocialAssistance]'
GO
ALTER TABLE [dbo].[PersonSocialAssistance] DROP CONSTRAINT [DF__PersonSoc__Benef__4B973090]
GO
PRINT N'Dropping constraints from [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [PK_Person]
GO
PRINT N'Dropping constraints from [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [DF__Person__MobileNo__45DE573A]
GO
PRINT N'Dropping constraints from [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] DROP CONSTRAINT [DF__Person__MobileNo__46D27B73]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollDuplicateID]'
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [PK_PrepayrollDuplicateID]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollDuplicateID]'
GO
ALTER TABLE [dbo].[PrepayrollDuplicateID] DROP CONSTRAINT [DF__Prepayrol__Actio__68F2894D]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollIneligible]'
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [PK_PrepayrollIneligible]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollIneligible]'
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [IX_PrepayrollIneligible_PaymentCycleId_HhId_ExceptionTypeId]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollIneligible]'
GO
ALTER TABLE [dbo].[PrepayrollIneligible] DROP CONSTRAINT [DF__Prepayrol__Actio__00CA12DE]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidID]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [PK_PrepayrollInvalidID]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidID]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidID] DROP CONSTRAINT [DF__Prepayrol__Actio__61516785]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentAccount]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [PK_PrepayrollNoPaymentAccount]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentAccount]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [IX_PrepayrollNoPaymentAccount_PaymentCycleId_HhId]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentAccount]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentAccount] DROP CONSTRAINT [DF__Prepayrol__Actio__7187CF4E]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentCard]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [PK_PrepayrollNoPaymentCard]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentCard]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [IX_PrepayrollNoPaymentCard_PaymentCycleId_HhId]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollInvalidPaymentCard]'
GO
ALTER TABLE [dbo].[PrepayrollInvalidPaymentCard] DROP CONSTRAINT [DF__Prepayrol__Actio__7928F116]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollSuspicious]'
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [PK_PrepayrollSuspicious]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollSuspicious]'
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [IX_PrepayrollSuspicious_PaymentCycleId_HhId]
GO
PRINT N'Dropping constraints from [dbo].[PrepayrollSuspicious]'
GO
ALTER TABLE [dbo].[PrepayrollSuspicious] DROP CONSTRAINT [DF__Prepayrol__Actio__086B34A6]
GO
PRINT N'Dropping constraints from [dbo].[Prepayroll]'
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [PK_Prepayroll]
GO
PRINT N'Dropping constraints from [dbo].[Prepayroll]'
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [DF__Prepayrol__Entit__5303482E]
GO
PRINT N'Dropping constraints from [dbo].[Prepayroll]'
GO
ALTER TABLE [dbo].[Prepayroll] DROP CONSTRAINT [DF__Prepayrol__Adjus__53F76C67]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [PK_Programme]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [IX_Programme_Code]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [IX_Programme_BeneProgNoPrefix]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [DF__Programme__Entit__7F4BDEC0]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [DF__Programme__PriRe__004002F9]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [DF__Programme__Secon__01342732]
GO
PRINT N'Dropping constraints from [dbo].[Programme]'
GO
ALTER TABLE [dbo].[Programme] DROP CONSTRAINT [DF__Programme__IsAct__02284B6B]
GO
PRINT N'Dropping constraints from [dbo].[ReconciliationDetail]'
GO
ALTER TABLE [dbo].[ReconciliationDetail] DROP CONSTRAINT [PK_ReconciliationDetail]
GO
PRINT N'Dropping constraints from [dbo].[Reconciliation]'
GO
ALTER TABLE [dbo].[Reconciliation] DROP CONSTRAINT [PK_Reconciliation]
GO
PRINT N'Dropping constraints from [dbo].[Role]'
GO
ALTER TABLE [dbo].[Role] DROP CONSTRAINT [PK_Role]
GO
PRINT N'Dropping constraints from [dbo].[SubLocation]'
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [PK_SubLocation]
GO
PRINT N'Dropping constraints from [dbo].[SubLocation]'
GO
ALTER TABLE [dbo].[SubLocation] DROP CONSTRAINT [IX_SubLocation_LocationId_Code]
GO
PRINT N'Dropping constraints from [dbo].[SystemCodeDetail]'
GO
ALTER TABLE [dbo].[SystemCodeDetail] DROP CONSTRAINT [PK_SystemCodeDetail]
GO
PRINT N'Dropping constraints from [dbo].[SystemCodeDetail]'
GO
ALTER TABLE [dbo].[SystemCodeDetail] DROP CONSTRAINT [IX_SystemCodeDetail_SystemCodeId_Code]
GO
PRINT N'Dropping constraints from [dbo].[SystemCode]'
GO
ALTER TABLE [dbo].[SystemCode] DROP CONSTRAINT [PK_SystemCode]
GO
PRINT N'Dropping constraints from [dbo].[SystemCode]'
GO
ALTER TABLE [dbo].[SystemCode] DROP CONSTRAINT [IX_SystemCode_Code]
GO
PRINT N'Dropping constraints from [dbo].[SystemCode]'
GO
ALTER TABLE [dbo].[SystemCode] DROP CONSTRAINT [DF__SystemCod__IsUse__22CA2527]
GO
PRINT N'Dropping constraints from [dbo].[UserClaim]'
GO
ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [PK_UserClaim]
GO
PRINT N'Dropping constraints from [dbo].[UserGroupProfile]'
GO
ALTER TABLE [dbo].[UserGroupProfile] DROP CONSTRAINT [PK_UserGroupProfile]
GO
PRINT N'Dropping constraints from [dbo].[UserGroupProfile]'
GO
ALTER TABLE [dbo].[UserGroupProfile] DROP CONSTRAINT [IX_UserGroupProfile_Name]
GO
PRINT N'Dropping constraints from [dbo].[UserGroup]'
GO
ALTER TABLE [dbo].[UserGroup] DROP CONSTRAINT [PK_UserGroup]
GO
PRINT N'Dropping constraints from [dbo].[UserLogin]'
GO
ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [PK_UserLogin]
GO
PRINT N'Dropping constraints from [dbo].[UserRole]'
GO
ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [PK_UserRole]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [PK_User]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [IX_User_UserName]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [IX_User_Email]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__EmailConfi__0AF29B96]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__MobileNoCo__0BE6BFCF]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__IsActive__0CDAE408]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__AccessFail__0DCF0841]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__IsLocked__0EC32C7A]
GO
PRINT N'Dropping constraints from [dbo].[User]'
GO
ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF__User__LockoutEna__0FB750B3]
GO
PRINT N'Dropping constraints from [dbo].[Village]'
GO
ALTER TABLE [dbo].[Village] DROP CONSTRAINT [PK_Village]
GO
PRINT N'Dropping constraints from [dbo].[WardLocation]'
GO
ALTER TABLE [dbo].[WardLocation] DROP CONSTRAINT [PK_WardLocation]
GO
PRINT N'Dropping constraints from [dbo].[Ward]'
GO
ALTER TABLE [dbo].[Ward] DROP CONSTRAINT [PK_Ward]
GO
PRINT N'Dropping constraints from [dbo].[Ward]'
GO
ALTER TABLE [dbo].[Ward] DROP CONSTRAINT [IX_Ward_ConstituencyId_Code]
GO
PRINT N'Dropping constraints from [dbo].[__MigrationHistory]'
GO
ALTER TABLE [dbo].[__MigrationHistory] DROP CONSTRAINT [PK___MigrationHistory]
GO
PRINT N'Dropping constraints from [dbo].[temp_EnrolmentGroups]'
GO
ALTER TABLE [dbo].[temp_EnrolmentGroups] DROP CONSTRAINT [PK_temp_EnrolmentGroups]
GO
PRINT N'Dropping constraints from [dbo].[temp_ExceptionTypes]'
GO
ALTER TABLE [dbo].[temp_ExceptionTypes] DROP CONSTRAINT [PK_temp_ExceptionTypes]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [PK_temp_Exceptions]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsInv__231F2AE2]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsDup__24134F1B]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsIne__25077354]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsInv__25FB978D]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsDup__26EFBBC6]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsIne__27E3DFFF]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsIne__28D80438]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsInv__29CC2871]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsDor__2AC04CAA]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsInv__2BB470E3]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__Conse__2CA8951C]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__Entit__2D9CB955]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__Adjus__2E90DD8E]
GO
PRINT N'Dropping constraints from [dbo].[temp_Exceptions]'
GO
ALTER TABLE [dbo].[temp_Exceptions] DROP CONSTRAINT [DF__temp_Exce__IsSus__2F8501C7]
GO
PRINT N'Dropping constraints from [dbo].[CaseCategory]'
GO
ALTER TABLE [dbo].[CaseCategory] DROP CONSTRAINT [DF__CaseCateg__Creat__3DD3211E]
GO
PRINT N'Dropping constraints from [dbo].[HouseholdVillageElder]'
GO
ALTER TABLE [dbo].[HouseholdVillageElder] DROP CONSTRAINT [DF__Household__Mobil__420DC656]
GO
PRINT N'Dropping constraints from [dbo].[PersonPension]'
GO
ALTER TABLE [dbo].[PersonPension] DROP CONSTRAINT [DF__PersonPen__IsGov__505BE5AD]
GO
PRINT N'Dropping constraints from [dbo].[PersonPension]'
GO
ALTER TABLE [dbo].[PersonPension] DROP CONSTRAINT [DF__PersonPen__Pensi__515009E6]
GO
PRINT N'Dropping [dbo].[temp_Payroll]'
GO
DROP TABLE [dbo].[temp_Payroll]
GO
PRINT N'Dropping [dbo].[temp_PSPPayrollFile]'
GO
DROP TABLE [dbo].[temp_PSPPayrollFile]
GO
PRINT N'Dropping [dbo].[temp_ExceptionTypes]'
GO
DROP TABLE [dbo].[temp_ExceptionTypes]
GO
PRINT N'Dropping [dbo].[temp_ExceptionActions]'
GO
DROP TABLE [dbo].[temp_ExceptionActions]
GO
PRINT N'Dropping [dbo].[temp_EnrolmentGroups]'
GO
DROP TABLE [dbo].[temp_EnrolmentGroups]
GO
PRINT N'Dropping [dbo].[__MigrationHistory]'
GO
DROP TABLE [dbo].[__MigrationHistory]
GO
PRINT N'Dropping [dbo].[Role]'
GO
DROP TABLE [dbo].[Role]
GO
PRINT N'Dropping [dbo].[PaymentCardBiometrics]'
GO
DROP TABLE [dbo].[PaymentCardBiometrics]
GO
PRINT N'Dropping [dbo].[fn_MonthName]'
GO
DROP FUNCTION [dbo].[fn_MonthName]
GO
PRINT N'Dropping [dbo].[vw_temp_PostpayrollExceptions]'
GO
DROP VIEW [dbo].[vw_temp_PostpayrollExceptions]
GO
PRINT N'Dropping [dbo].[vw_temp_PayrollMobilization]'
GO
DROP VIEW [dbo].[vw_temp_PayrollMobilization]
GO
PRINT N'Dropping [dbo].[temp_PayrollFile]'
GO
DROP TABLE [dbo].[temp_PayrollFile]
GO
PRINT N'Dropping [dbo].[vw_temp_PrepayrollExceptions]'
GO
DROP VIEW [dbo].[vw_temp_PrepayrollExceptions]
GO
PRINT N'Dropping [dbo].[temp_Exceptions]'
GO
DROP TABLE [dbo].[temp_Exceptions]
GO
PRINT N'Dropping [dbo].[GenerateBulkTransferFile]'
GO
DROP PROCEDURE [dbo].[GenerateBulkTransferFile]
GO
PRINT N'Dropping [dbo].[GetHouseholdInfo]'
GO
DROP PROCEDURE [dbo].[GetHouseholdInfo]
GO
PRINT N'Dropping [dbo].[AddEditBulkTransferDetail]'
GO
DROP PROCEDURE [dbo].[AddEditBulkTransferDetail]
GO
PRINT N'Dropping [dbo].[GetPersonByEnrolmentNo]'
GO
DROP PROCEDURE [dbo].[GetPersonByEnrolmentNo]
GO
PRINT N'Dropping [dbo].[ApproveChangeRequest]'
GO
DROP PROCEDURE [dbo].[ApproveChangeRequest]
GO
PRINT N'Dropping [dbo].[WardLocation]'
GO
DROP TABLE [dbo].[WardLocation]
GO
PRINT N'Dropping [dbo].[Ward]'
GO
DROP TABLE [dbo].[Ward]
GO
PRINT N'Dropping [dbo].[UserRole]'
GO
DROP TABLE [dbo].[UserRole]
GO
PRINT N'Dropping [dbo].[UserLogin]'
GO
DROP TABLE [dbo].[UserLogin]
GO
PRINT N'Dropping [dbo].[UserGroupProfile]'
GO
DROP TABLE [dbo].[UserGroupProfile]
GO
PRINT N'Dropping [dbo].[UserClaim]'
GO
DROP TABLE [dbo].[UserClaim]
GO
PRINT N'Dropping [dbo].[SystemCode]'
GO
DROP TABLE [dbo].[SystemCode]
GO
PRINT N'Dropping [dbo].[ReconciliationDetail]'
GO
DROP TABLE [dbo].[ReconciliationDetail]
GO
PRINT N'Dropping [dbo].[PrepayrollSuspicious]'
GO
DROP TABLE [dbo].[PrepayrollSuspicious]
GO
PRINT N'Dropping [dbo].[PrepayrollInvalidPaymentCard]'
GO
DROP TABLE [dbo].[PrepayrollInvalidPaymentCard]
GO
PRINT N'Dropping [dbo].[PrepayrollInvalidPaymentAccount]'
GO
DROP TABLE [dbo].[PrepayrollInvalidPaymentAccount]
GO
PRINT N'Dropping [dbo].[PrepayrollInvalidID]'
GO
DROP TABLE [dbo].[PrepayrollInvalidID]
GO
PRINT N'Dropping [dbo].[PrepayrollIneligible]'
GO
DROP TABLE [dbo].[PrepayrollIneligible]
GO
PRINT N'Dropping [dbo].[PrepayrollDuplicateID]'
GO
DROP TABLE [dbo].[PrepayrollDuplicateID]
GO
PRINT N'Dropping [dbo].[PersonSocialAssistance]'
GO
DROP TABLE [dbo].[PersonSocialAssistance]
GO
PRINT N'Dropping [dbo].[PersonPension]'
GO
DROP TABLE [dbo].[PersonPension]
GO
PRINT N'Dropping [dbo].[Prepayroll]'
GO
DROP TABLE [dbo].[Prepayroll]
GO
PRINT N'Dropping [dbo].[PaymentEnrolmentGroup]'
GO
DROP TABLE [dbo].[PaymentEnrolmentGroup]
GO
PRINT N'Dropping [dbo].[Reconciliation]'
GO
DROP TABLE [dbo].[Reconciliation]
GO
PRINT N'Dropping [dbo].[PaymentCycleDetail]'
GO
DROP TABLE [dbo].[PaymentCycleDetail]
GO
PRINT N'Dropping [dbo].[PaymentAdjustment]'
GO
DROP TABLE [dbo].[PaymentAdjustment]
GO
PRINT N'Dropping [dbo].[PaymentAccountActivityMonth]'
GO
DROP TABLE [dbo].[PaymentAccountActivityMonth]
GO
PRINT N'Dropping [dbo].[Payment]'
GO
DROP TABLE [dbo].[Payment]
GO
PRINT N'Dropping [dbo].[Payroll]'
GO
DROP TABLE [dbo].[Payroll]
GO
PRINT N'Dropping [dbo].[PasswordReset]'
GO
DROP TABLE [dbo].[PasswordReset]
GO
PRINT N'Dropping [dbo].[Module]'
GO
DROP TABLE [dbo].[Module]
GO
PRINT N'Dropping [dbo].[PaymentZone]'
GO
DROP TABLE [dbo].[PaymentZone]
GO
PRINT N'Dropping [dbo].[HouseholdVillageElder]'
GO
DROP TABLE [dbo].[HouseholdVillageElder]
GO
PRINT N'Dropping [dbo].[Village]'
GO
DROP TABLE [dbo].[Village]
GO
PRINT N'Dropping [dbo].[HouseholdVillage]'
GO
DROP TABLE [dbo].[HouseholdVillage]
GO
PRINT N'Dropping [dbo].[SubLocation]'
GO
DROP TABLE [dbo].[SubLocation]
GO
PRINT N'Dropping [dbo].[HouseholdSubLocation]'
GO
DROP TABLE [dbo].[HouseholdSubLocation]
GO
PRINT N'Dropping [dbo].[HouseholdMember]'
GO
DROP TABLE [dbo].[HouseholdMember]
GO
PRINT N'Dropping [dbo].[HouseholdEnrolmentPlan]'
GO
DROP TABLE [dbo].[HouseholdEnrolmentPlan]
GO
PRINT N'Dropping [dbo].[Household]'
GO
DROP TABLE [dbo].[Household]
GO
PRINT N'Dropping [dbo].[UserGroup]'
GO
DROP TABLE [dbo].[UserGroup]
GO
PRINT N'Dropping [dbo].[GroupRight]'
GO
DROP TABLE [dbo].[GroupRight]
GO
PRINT N'Dropping [dbo].[ModuleRight]'
GO
DROP TABLE [dbo].[ModuleRight]
GO
PRINT N'Dropping [dbo].[PSP]'
GO
DROP TABLE [dbo].[PSP]
GO
PRINT N'Dropping [dbo].[FundsRequestDetail]'
GO
DROP TABLE [dbo].[FundsRequestDetail]
GO
PRINT N'Dropping [dbo].[FundsRequest]'
GO
DROP TABLE [dbo].[FundsRequest]
GO
PRINT N'Dropping [dbo].[PaymentCycle]'
GO
DROP TABLE [dbo].[PaymentCycle]
GO
PRINT N'Dropping [dbo].[FileDownload]'
GO
DROP TABLE [dbo].[FileDownload]
GO
PRINT N'Dropping [dbo].[FileCreation]'
GO
DROP TABLE [dbo].[FileCreation]
GO
PRINT N'Dropping [dbo].[ExpansionPlanDetail]'
GO
DROP TABLE [dbo].[ExpansionPlanDetail]
GO
PRINT N'Dropping [dbo].[Location]'
GO
DROP TABLE [dbo].[Location]
GO
PRINT N'Dropping [dbo].[ExpansionPlan]'
GO
DROP TABLE [dbo].[ExpansionPlan]
GO
PRINT N'Dropping [dbo].[ExpansionPlanMaster]'
GO
DROP TABLE [dbo].[ExpansionPlanMaster]
GO
PRINT N'Dropping [dbo].[EFCNote]'
GO
DROP TABLE [dbo].[EFCNote]
GO
PRINT N'Dropping [dbo].[EFCDocument]'
GO
DROP TABLE [dbo].[EFCDocument]
GO
PRINT N'Dropping [dbo].[EFC]'
GO
DROP TABLE [dbo].[EFC]
GO
PRINT N'Dropping [dbo].[Division]'
GO
DROP TABLE [dbo].[Division]
GO
PRINT N'Dropping [dbo].[DBBackup]'
GO
DROP TABLE [dbo].[DBBackup]
GO
PRINT N'Dropping [dbo].[District]'
GO
DROP TABLE [dbo].[District]
GO
PRINT N'Dropping [dbo].[CountyDistrict]'
GO
DROP TABLE [dbo].[CountyDistrict]
GO
PRINT N'Dropping [dbo].[GeoMaster]'
GO
DROP TABLE [dbo].[GeoMaster]
GO
PRINT N'Dropping [dbo].[Constituency]'
GO
DROP TABLE [dbo].[Constituency]
GO
PRINT N'Dropping [dbo].[County]'
GO
DROP TABLE [dbo].[County]
GO
PRINT N'Dropping [dbo].[ComplaintNote]'
GO
DROP TABLE [dbo].[ComplaintNote]
GO
PRINT N'Dropping [dbo].[ComplaintDocument]'
GO
DROP TABLE [dbo].[ComplaintDocument]
GO
PRINT N'Dropping [dbo].[Complaint]'
GO
DROP TABLE [dbo].[Complaint]
GO
PRINT N'Dropping [dbo].[ChangeNote]'
GO
DROP TABLE [dbo].[ChangeNote]
GO
PRINT N'Dropping [dbo].[ChangeDocument]'
GO
DROP TABLE [dbo].[ChangeDocument]
GO
PRINT N'Dropping [dbo].[Programme]'
GO
DROP TABLE [dbo].[Programme]
GO
PRINT N'Dropping [dbo].[Change]'
GO
DROP TABLE [dbo].[Change]
GO
PRINT N'Dropping [dbo].[BulkTransferDetail]'
GO
DROP TABLE [dbo].[BulkTransferDetail]
GO
PRINT N'Dropping [dbo].[BulkTransfer]'
GO
DROP TABLE [dbo].[BulkTransfer]
GO
PRINT N'Dropping [dbo].[Person]'
GO
DROP TABLE [dbo].[Person]
GO
PRINT N'Dropping [dbo].[BeneficiaryPaymentCard]'
GO
DROP TABLE [dbo].[BeneficiaryPaymentCard]
GO
PRINT N'Dropping [dbo].[PSPBranch]'
GO
DROP TABLE [dbo].[PSPBranch]
GO
PRINT N'Dropping [dbo].[HouseholdEnrolment]'
GO
DROP TABLE [dbo].[HouseholdEnrolment]
GO
PRINT N'Dropping [dbo].[BeneficiaryAccount]'
GO
DROP TABLE [dbo].[BeneficiaryAccount]
GO
PRINT N'Dropping [dbo].[BeneAccountMonthlyActivityDetail]'
GO
DROP TABLE [dbo].[BeneAccountMonthlyActivityDetail]
GO
PRINT N'Dropping [dbo].[User]'
GO
DROP TABLE [dbo].[User]
GO
PRINT N'Dropping [dbo].[BeneAccountMonthlyActivity]'
GO
DROP TABLE [dbo].[BeneAccountMonthlyActivity]
GO
PRINT N'Dropping [dbo].[SystemCodeDetail]'
GO
DROP TABLE [dbo].[SystemCodeDetail]
GO
PRINT N'Adding constraints to [dbo].[CaseCategory]'
GO
ALTER TABLE [dbo].[CaseCategory] ADD CONSTRAINT [DF__CaseCateg__Creat__5070F446] DEFAULT (getdate()) FOR [CreatedOn]
GO
