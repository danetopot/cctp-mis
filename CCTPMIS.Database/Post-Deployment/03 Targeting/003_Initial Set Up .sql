﻿
 DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int
---------------PAYMENT/PAYROLL STAGE-------------

SET @Code='Registration Status'
SET @Description='Registration status of Household'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Tenure Status'
SET @Description='TENURE status of the dwelling unit and/or surrounding terrain/land'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Wall Material'
SET @Description='Wall Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Floor Material'
SET @Description='Floor Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Roof Material'
SET @Description='Roof Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Toilet Type'
SET @Description='Toilet Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Water Source'
SET @Description='Water Source'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Lighting Source'
SET @Description='Lighting Source'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Cooking Fuel'
SET @Description='Cooking Fuel'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Dwelling Unit Risk'
SET @Description='Risks of the Dwelling unit'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

 
SET @Code='Boolean Options'
SET @Description='Boolean Options'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Conditions'
SET @Description='Household Conditions'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Option'
SET @Description='Household Options'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Other SP Programme'
SET @Description='Other Social Assistance Programme'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='SP Benefit Type'
SET @Description='Social Protection Benefit Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Interview Type'
SET @Description='Interview Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SET @Code='Interview Result'
SET @Description='Result of the interview'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Interview Status'
SET @Description=' Status of teh Interview'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

---------------------HTM MEMBERS OPTIONS---------------------------------------------

SET @Code='ID Document Type'
SET @Description='Various Identification Document Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Relationship'
SET @Description='How a member relates to Household Head'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Marital Status'
SET @Description='Marital statuses'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Disability'
SET @Description='Various kinds of ailements'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Education Attendance'
SET @Description='Highest levels of Education Attendance '
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Education Level'
SET @Description='Various highest levels of education'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Work Type'
SET @Description='A persons Work Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;
 

 SET @Code='REG_CATEGORIES'
SET @Description='Registration Exercise Categories'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;
 
SET @Code='REG_STATUS'
SET @Description='Registration Exercise Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


------------------------ TENURE STATUS ----------------------------------------------------------
SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code='Tenure Status'

IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='01'
	SET @Description='Purchased'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Constructed'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Inherited'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Government'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Local Authority'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Parastatal'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Private Company'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='08'
	SET @Description='Individual'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='09'
	SET @Description='Faith based organization/NGO'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other:'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END
SELECT @Id = NULL
------------------------ Wall Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code='Wall Material'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='01'
	SET @Description='Stone'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Brick/Block'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Mud/Wood'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Mud/Cement'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Wood only'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Corrugated iron sheets'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Grass/Reeds'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='08'
	SET @Description='Tin'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END

SELECT @Id = NULL
------------------------ Floor Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Floor Material'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='01'
	SET @Description='Cement'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Tiles'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Wood'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Earth'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL
------------------------ Roof Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Roof Material'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='01'
	SET @Description='Corrugated iron sheets'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Tiles'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Concrete'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Asbestos sheets'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Grass'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Makuti'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Tin'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='08'
	SET @Description='Mud/dung'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END


SELECT @Id = NULL
------------------------ Toilet Type ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Toilet Type'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Main sewer'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='Septic tank'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	SET @DetailCode='03'
	SET @Description='Cess pool'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='04'
	SET @Description='VIP pit latrine'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='Pit latrine covered'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='06'
	SET @Description='Pit latrine uncovered'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='07'
	SET @Description='Bucket Latrine'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='08'
	SET @Description='Bush'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END

SELECT @Id = NULL
------------------------Water Source ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code='Water Source'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='01'
	SET @Description='Pond'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Dam'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Lake'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Stream/River'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Protected spring water'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Unprotected spring water'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Protected well'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='08'
	SET @Description='Unprotected well'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='09'
	SET @Description='Borehole'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='10'
	SET @Description='Piped into dwelling'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='11'
	SET @Description='Piped'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='12'
	SET @Description='Jabia'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='13'
	SET @Description='Rain/Harvested'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='14'
	SET @Description='Water vendor'
	SET @OrderNo=14
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=15
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END


SELECT @Id = NULL
------------------------ Lighting Source ----------------------------------------------------------


SELECT @Id=Id FROM SystemCode WHERE Code ='Lighting Source'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Electricity'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Pressure lamp'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Lantern'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='04'
	SET @Description='Tin lamp'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='Gas lamp'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Fuel wood / Firewood'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Solar'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id = NULL
------------------------ Cooking Fuel ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Cooking Fuel'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Electricity'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Paraffin'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='LPG (Liquefied Petroleum Gas)'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Biogas'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Firewood'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Charcoal'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Solar'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id = NULL
------------------------ Dwelling Unit Risk ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Dwelling Unit Risk'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='None'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Landslide'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Flooding'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Fire'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
------------------------ Household Assets ----------------------------------------------------------
/*
SELECT @Id=Id FROM SystemCode WHERE Code ='Household Assets'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='Television'
	SET @Description='Television'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Motorcycle'
	SET @Description='Motorcycle'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Tuk-Tuk'
	SET @Description='Tuk-Tuk'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Refrigerator'
	SET @Description='Refrigerator'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Car'
	SET @Description='Car'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Mobile phone'
	SET @Description='Mobile phone'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Bicycle'
	SET @Description='Bicycle'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='Exotic cattle'
	SET @Description='Exotic cattle'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='Indigenous cattle'
	SET @Description='Indigenous cattle'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Sheep'
	SET @Description='Sheep'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='Goats'
	SET @Description='Goats'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='Camels'
	SET @Description='Camels'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Donkeys'
	SET @Description='Donkeys'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Pigs'
	SET @Description='Pigs'
	SET @OrderNo=14
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='Chicken'
	SET @Description='Chicken'
	SET @OrderNo=15
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


*/

----------------------------------------------


SELECT @Id = NULL
------------------------ Boolean Options ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Boolean Options'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Yes'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='00'
	SET @Description='No'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL
------------------------ Household Conditions ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Household Conditions'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Poor'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Fair'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Good'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Very Good'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
------------------------ Household Options ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Household Option'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Yes'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='No'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Don''t Know'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
------------------------ Id Document Type ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Id Document Type'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='National ID card'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Registration of birth'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Passport'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Other'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='None'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL
------------------------ Other SP Programme ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Other SP Programme'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='CT-OVC'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='HSNP'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='PWSD-CT'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='04'
	SET @Description='OPCT'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='All'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='None'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SELECT @Id = NULL
------------------------ SP Benefit Type ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code  ='SP Benefit Type'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Cash'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='In-kind'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Other'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL
------------------------ Relationship ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Relationship'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Head'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Spouse'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Son/Daughter'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Grandchild'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='Brother/Sister'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Father/mother'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='07'
	SET @Description='Nephew/Niece'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='08'
	SET @Description='In-Law'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='09'
	SET @Description='Grandparent'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='10'
	SET @Description='Other relative'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='11'
	SET @Description='Non-relative'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='12'
	SET @Description='Employee'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='98'
	SET @Description='DK'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id = NULL
------------------------ Marital Status ----------------------------------------------------------

SELECT @Id=Id FROM SystemCode WHERE Code ='Marital Status'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Never married'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='Married Monogamous'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Married Polygamous'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Widowed'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Divorced or separated'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='98'
	SET @Description='DK'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Disability'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='Visual'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='Hearing'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Speech'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='Physical'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='05'
	SET @Description='Mental'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='06'
	SET @Description='Self-care difficulties'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='97'
	SET @Description='Others'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='99'
	SET @Description='None'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SELECT @Id=Id
FROM SystemCode
WHERE Code ='Education Attendance'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='01'
	SET @Description='At school or learning institution'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='02'
	SET @Description='Left school or learning institution'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='03'
	SET @Description='Never went to school or learning institution'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='98'
	SET @Description='DK'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END





SELECT @Id=Id
FROM SystemCode
WHERE Code ='Education Level'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='96'
	SET @Description='Pre primary (ECD) or NONE'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='00'
	SET @Description='Standard 1 (incomplete)'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='01'
	SET @Description='Standard 1'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='Standard 2'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Standard 3'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='04'
	SET @Description='Standard 4'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	SET @DetailCode='05'
	SET @Description='Standard 5'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='06'
	SET @Description='Standard 6'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='07'
	SET @Description='Standard 7'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='08'
	SET @Description='Standard 8'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	SET @DetailCode='09'
	SET @Description='Form 1'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='10'
	SET @Description='Form 2'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='11'
	SET @Description='Form 3'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='12'
	SET @Description='Form 4'
	SET @OrderNo=14
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='13'
	SET @Description='Form 5'
	SET @OrderNo=15
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='14'
	SET @Description='Form 6'
	SET @OrderNo=16
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

 

	SET @DetailCode='15'
	SET @Description='Incomplete post-secondary'
	SET @OrderNo=17
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='16'
	SET @Description='Complete post-secondary'
	SET @OrderNo=18
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='17'
	SET @Description='Incomplete undergraduate or Polytechnic'
	SET @OrderNo=19
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='18'
	SET @Description='Complete undergraduate'
	SET @OrderNo=20
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='19'
	SET @Description='Incomplete master/PhD'
	SET @OrderNo=21
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='20'
	SET @Description='Complete master/PhD'
	SET @OrderNo=22
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=23
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




END

SELECT @Id=Id
FROM SystemCode
WHERE Code ='Work Type'
IF(ISNULL(@Id,0)>0)
BEGIN


	SET @DetailCode='01'
	SET @Description='Worked for pay'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='On leave'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Sick leave'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='04'
	SET @Description='Worked own or at family business or at family agriculture'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='Apprentice/Intern'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='06'
	SET @Description='Volunteer'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='07'
	SET @Description='Seeking work'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='08'
	SET @Description='No work available'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='09'
	SET @Description='Retired with pension'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='10'
	SET @Description='Homemaker'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='11'
	SET @Description='Full-time Student'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='12'
	SET @Description='Part-time student'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='13'
	SET @Description='Incapacitated'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='97'
	SET @Description='Other'
	SET @OrderNo=14
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END


SELECT @Id = NULL

-----------------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Interview Type'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='NEW HOUSEHOLD'
	SET @Description='NEW HOUSEHOLD'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='RECERTIFICATION'
	SET @Description='RECERTIFICATION'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL


-----------------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Interview Result'
IF(ISNULL(@Id,0)>0)
BEGIN

	 SET @DetailCode='00'
	SET @Description='Pending'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='01'
	SET @Description='Completed'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='02'
	SET @Description='Incompleted'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='03'
	SET @Description='Rejection'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='04'
	SET @Description='No one at home'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='05'
	SET @Description='Cannot find household'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END



SELECT @Id = NULL



-----------------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Interview Status'
IF(ISNULL(@Id,0)>0)
BEGIN

SET @DetailCode='00'
	SET @Description='Pending'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='01'
	SET @Description='Ongoing'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	 
	SET @DetailCode='02'
	SET @Description='Completed'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END



SELECT @Id = NULL
------------------------ 'Registration Status' ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Registration Status'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='REG'
	SET @Description='Registration '
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGSYNC'
	SET @Description='Registration Synced'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGAPV'
	SET @Description='Registration Approved'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGREJ'
	SET @Description='REGREJ'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='VALPASS'
	SET @Description='VALPASS'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='VALFAILED'
	SET @Description='VALFAILED'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 


END
  


SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code =  'REG_STATUS'
IF(ISNULL(@Id,0)>0)
BEGIN
   SET @DetailCode='CREATIONAPV'
	SET @Description='Creation Approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ACTIVE'
	SET @Description='Active'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUBMISSIONAPV'
	SET @Description='Submitted for Approval'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	
	SET @DetailCode='IPRSVAL'
	SET @Description='SR/IPRS VALIDATION'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='COMMVAL'
	SET @Description='Community Validation'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='COMMVALAPV'
	SET @Description='Community Validation Approval'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='CLOSED'
	SET @Description='Closed'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
END
 
SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code = 'REG_CATEGORIES'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='LISTING'
	SET @Description='Household Listing'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGISTRATION'
	SET @Description='Registration'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECERTIFICATION'
	SET @Description='Re-Certification'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;	 
END
 


SELECT @Id=Id FROM SystemCode WHERE Code='File Type'
IF(ISNULL(@Id,0)>0)
BEGIN

	SET @DetailCode='REG_VALIDATION'
	SET @Description='IPRS Export File'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REG_EXCEPTIONS'
	SET @Description='Targrting Exception File'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='IPRS_EXPORT'
	SET @Description='IPRS Export File'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

