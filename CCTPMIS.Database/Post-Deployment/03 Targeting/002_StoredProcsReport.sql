﻿IF NOT OBJECT_ID('GetProgOfficerSummary') IS NULL	
DROP PROC GetProgOfficerSummary
GO
CREATE PROC GetProgOfficerSummary
	@UserId int
AS
BEGIN
	DECLARE @Exists INT =0
	SELECT @Exists = COUNT(Id)
	FROM ProgrammeOfficer
	WHERE UserId = @UserId
	IF(@Exists>0)
   SELECT TOP 1
		CountyId, ConstituencyId, UserId
	FROM ProgrammeOfficer
	WHERE UserId = @UserId
	ORDER BY 1 DESC
   ELSE 
   SELECT NULL AS CountyId, NULL AS ConstituencyId, NULL AS UserId
END
 GO
 


IF NOT OBJECT_ID('GetListingSummary') IS NULL	
 DROP PROC GetListingSummary
GO
CREATE PROC GetListingSummary
	@TargetPlanId int,
	@CountyId int = null,
	@ConstituencyId int = null
AS
BEGIN

 SELECT
		MAX(T1.ExceptionsFileId) AS ExceptionsFileId
, MAX(T1.ValidationFileId) AS ValidationFileId 
, SUM(CASE WHEN(T2.Id>0) THEN 1 ELSE 0 END) AS RegisteredHouseholds
 , MAX(T5.AcceptedBatches) AS AcceptedBatches
 , MAX(T6.PendingApproval) AS PendingApproval
, MAX(T7.ApprovedBatches) AS ApprovedBatches
, SUM(CASE WHEN(T2.ListingAcceptId IS NOT NULL) THEN 1 ELSE 0 END) AS AcceptedHouseholds
, SUM(CASE WHEN(T2.ListingAcceptId IS NULL AND T2.RejectById IS NULL) THEN 1 ELSE 0 END) AS PendingHouseholds
, SUM(CASE WHEN(T2.RejectById IS NOT NULL ) THEN 1 ELSE 0 END) AS RejectedHouseholds
, SUM(CASE WHEN(T2.BeneNationalIdNo IS NOT NULL AND T2.ListingAcceptId IS NOT NULL AND T2.RejectById IS NULL ) THEN 1 ELSE 0 END) AS TotalBeneficiaries
, SUM(CASE WHEN(T2.CgNationalIdNo IS NOT NULL AND T2.ListingAcceptId IS NOT NULL AND T2.RejectById IS NULL ) THEN 1 ELSE 0 END) AS TotalCaregivers
, SUM(CASE WHEN(T4.CG_IDNoExists>0) THEN 1 ELSE 0 END) AS CG_IDNoExists
, SUM(CASE WHEN(T4.CG_SexMatch>0) THEN 1 ELSE 0 END) AS CGSexMatch
, SUM(CASE WHEN(T4.CG_DoBYearMatch>0) THEN 1 ELSE 0 END) AS CG_DoBYearMatch
, SUM(CASE WHEN(T4.CG_DoBMatch>0) THEN 1 ELSE 0 END) AS CG_DoBMatch
, SUM(CASE WHEN(T4.Bene_IDNoExists>0) THEN 1 ELSE 0 END) AS BeneIDNoExists
, SUM(CASE WHEN(T4.Bene_SexMatch>0) THEN 1 ELSE 0 END) AS BeneSexMatch
, SUM(CASE WHEN(T4.Bene_DoBYearMatch>0) THEN 1 ELSE 0 END) AS BeneDoBYearMatch
, SUM(CASE WHEN(T4.Bene_DoBMatch>0) THEN 1 ELSE 0 END) AS BeneDoBMatch
	FROM TargetPlan  T1
		LEFT JOIN ListingPlanHH T2 ON T1.Id =T2.TargetPlanId
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T2.SubLocationId
		LEFT JOIN ( SELECT DISTINCT Id, TargetPlanId , ReceivedHHs, ConstituencyId, AcceptApvById,AcceptApvDate
		FROM ListingAccept)  T3 ON T1.Id =  T3.TargetPlanId AND T2.ListingAcceptId = T3.Id
		LEFT JOIN ( SELECT DISTINCT Id, CG_IDNoExists, CG_DoBYearMatch, CG_SexMatch, Bene_DoBMatch, Bene_DoBYearMatch, Bene_SexMatch, Bene_IDNoExists, CG_DoBMatch, TargetPlanId
		FROM ListingException)   T4 ON T1.Id =  T4.TargetPlanId AND T2.Id = T4.Id
		LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS AcceptedBatches  FROM ListingAccept GROUP BY TargetPlanId ) T5 ON T3.Id=T5.TargetPlanId
		LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS PendingApproval  FROM ListingAccept  WHERE AcceptApvById IS NULL GROUP BY TargetPlanId ) T6 ON T3.Id=T6.TargetPlanId
	    LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS ApprovedBatches  FROM ListingAccept  WHERE AcceptApvById IS not NULL GROUP BY TargetPlanId ) T7 ON T3.Id=T7.TargetPlanId
	
	WHERE  T1.Id =@TargetPlanId
		AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
		AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END

END
GO


IF NOT OBJECT_ID('GetPendingListingPlanHH') IS NULL	
 DROP PROC GetPendingListingPlanHH
 GO
CREATE PROC GetPendingListingPlanHH
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN
	SELECT
		COUNT(Id) AS PendingHHs,
		TG.ConstituencyId,
		@TargetPlanId AS TargetPlanId,
		TG.Constituency,
		TG.County,
		TG.LocationId,
		TG.Location
	FROM ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.ListingAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.RejectReason IS NULL
	GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location
END
GO

IF NOT OBJECT_ID('GetPendingListingPlanHHCount') IS NULL	
 DROP PROC GetPendingListingPlanHHCount
 GO
CREATE PROC GetPendingListingPlanHHCount
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

 
	SELECT
	  COUNT(Id) as PendingHHs 
	FROM ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.ListingAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId AND RejectById IS NULL
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.RejectReason IS NULL
	--GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location

 
END
GO


 

IF NOT OBJECT_ID('GetProgOfficerLocations') IS NULL	
DROP PROC GetProgOfficerLocations
GO
CREATE PROC GetProgOfficerLocations
	@CountyId int = NULL,
	@ConstituencyId int = NULL
AS
BEGIN

	IF(@CountyId IS NOT NULL AND @ConstituencyId IS NOT NULL)
 BEGIN
		SELECT DISTINCT T2.*
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		WHERE T7.ID = @ConstituencyId
	END

	IF(@CountyId IS NOT NULL AND @ConstituencyId IS   NULL)
 BEGIN
		SELECT DISTINCT T2.*
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		WHERE T6.Id = @CountyId
	END

 ELSE 
 BEGIN
		SELECT TOP 0
			*
		FROM [Location]
	END

END

GO


IF NOT OBJECT_ID('GetListingBatches') IS NULL	
 DROP PROC GetListingBatches
 GO
CREATE PROC GetListingBatches
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	SELECT DISTINCT T1.*, TG.County, TG.Constituency, TG.ConstituencyId, T2.FirstName AS AcceptBy,
		T3.Id AS AcceptApvById, 
		T3.FirstName AS AcceptApvBy, T4.Name AS TargetPlan
	FROM ListingAccept T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.ConstituencyId  = T1.ConstituencyId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.TargetPlanId = @TargetPlanId
		LEFT JOIN [USER] T2 ON T1.AcceptById = T2.Id
		LEFT JOIN [USER] T3 ON T1.AcceptApvById = T3.Id
		LEFT JOIN TargetPlan T4 ON T1.TargetPlanId = T4.Id

END
GO



IF NOT OBJECT_ID('GetBatchHHListingPendingIPRS') IS NULL	
DROP PROC GetBatchHHListingPendingIPRS
GO
CREATE PROC GetBatchHHListingPendingIPRS
	@ListingAcceptId INT
AS
BEGIN
	DECLARE @PendingValidation int


	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(select 1
	from ListingAccept WHERE Id = @ListingAcceptId AND AcceptApvDate IS NULL)
SET @ErrorMsg ='This Batch has not been Approved. This Cannot be Run against SR/IPRS.'

	IF EXISTS(select 1 from ListingAccept where Id = @ListingAcceptId AND IsValidated=1 )
SET @ErrorMsg ='This Batch already been Run against SR/IPRS.'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END


;
	WITH
		X
		AS
		
		(
							SELECT T1.BeneNationalIdNo as IDNumber, CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') 
THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS Names
				FROM ListingPlanHH T1
					LEFT OUTER JOIN (SELECT ID_Number AS IDNo
					FROM IPRSCache) AS T2 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)
				WHERE T2.IDNo is null AND T1.ListingAcceptId = @ListingAcceptId
					AND T1.BeneNationalIdNo IS NOT NULL
			UNION
				SELECT T1.CgNationalIdNo, CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS BeneName
				FROM
					ListingPlanHH T1
					LEFT OUTER JOIN ( SELECT ID_Number AS IDNo
					FROM IPRSCache) AS T3 ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)
				WHERE T3.IDNo is null AND T1.ListingAcceptId = @ListingAcceptId
		)
	  
	select @PendingValidation = count(*) 	from X
	UPDATE  ListingAccept SET PendingValidation = @PendingValidation, IsValidated = CASE WHEN @PendingValidation =0 THEN 1 ELSE 0 END  
WHERE Id =@ListingAcceptId
END
GO




 
IF NOT OBJECT_ID('GetComValListingSummary') IS NULL	
 DROP PROC GetComValListingSummary
 GO
CREATE PROC GetComValListingSummary
	@TargetPlanId int
	,
	@CountyId int = null
	,
	@ConstituencyId int = null
AS
BEGIN

	SELECT
		MAX(T1.ExceptionsFileId) AS ExceptionsFileId
, MAX(T1.ValidationFileId) AS ValidationFileId 
, SUM(CASE WHEN(T2.Id>0) THEN 1 ELSE 0 END) AS RegisteredHouseholds
, MAX(T5.AcceptedBatches) AS AcceptedBatches
, SUM(CASE WHEN(T2.ComValListingAcceptId>0) THEN 1 ELSE 0 END) AS AcceptedHouseholds
, SUM(CASE WHEN(T2.ComValListingAcceptId IS NULL AND T2.Id>0) THEN 1 ELSE 0 END) AS PendingHouseholds
, SUM(CASE WHEN(T4.CG_IDNoExists>0) THEN 1 ELSE 0 END) AS CG_IDNoExists
, SUM(CASE WHEN(T4.CG_SexMatch>0) THEN 1 ELSE 0 END) AS CGSexMatch
, SUM(CASE WHEN(T4.CG_DoBYearMatch>0) THEN 1 ELSE 0 END) AS CG_DoBYearMatch
, SUM(CASE WHEN(T4.CG_DoBMatch>0) THEN 1 ELSE 0 END) AS CG_DoBMatch
, SUM(CASE WHEN(T4.Bene_IDNoExists>0) THEN 1 ELSE 0 END) AS BeneIDNoExists
, SUM(CASE WHEN(T4.Bene_SexMatch>0) THEN 1 ELSE 0 END) AS BeneSexMatch
, SUM(CASE WHEN(T4.Bene_DoBYearMatch>0) THEN 1 ELSE 0 END) AS BeneDoBYearMatch
, SUM(CASE WHEN(T4.Bene_DoBMatch>0) THEN 1 ELSE 0 END) AS BeneDoBMatch
, MAX(T6.TargetPlanId) AS   TargetPlanId
, MAX(T6.RegHH) AS  RegHH 
, MAX(T6.ExitedHH ) AS   ExitedHH
, MAX(T6.ConfirmedHH) AS   ConfirmedHH
, MAX(T6.AmendedHH) AS   AmendedHH
, MAX(T6.CValHH) AS  CValHH 
, MAX(T6.PendingAcceptanceHH) AS   PendingAcceptanceHH

	FROM TargetPlan  T1
		LEFT JOIN ComValListingPlanHH T2 ON T1.Id =T2.TargetPlanId
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id)  AS TG ON TG.SubLocationId  = T2.SubLocationId
		LEFT JOIN ( SELECT DISTINCT Id, TargetPlanId , ReceivedHHs, ConstituencyId FROM ComValListingAccept)  T3 ON T1.Id =  T3.TargetPlanId AND T2.ComValListingAcceptId = T3.Id
		LEFT JOIN ( SELECT DISTINCT Id, CG_IDNoExists, CG_DoBYearMatch, CG_SexMatch, Bene_DoBMatch, Bene_DoBYearMatch, Bene_SexMatch, Bene_IDNoExists, CG_DoBMatch, TargetPlanId FROM ListingException)   T4 ON T1.Id =  T4.TargetPlanId AND T2.Id = T4.Id
		LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS AcceptedBatches FROM ComValListingAccept GROUP BY TargetPlanId ) T5 ON T3.Id=T5.TargetPlanId

		LEFT JOIN ( 

		SELECT
		MAX(T2.TargetPlanId) TargetPlanId,
		 COUNT(T1.Id) AS RegHH,
		SUM(CASE  WHEN (T3.Code ='REGEXIT' ) THEN 1 ELSE 0 END)  AS ExitedHH,
		SUM(CASE  WHEN (T3.Code ='REGCONFIRM'  ) THEN 1 ELSE 0 END)  AS ConfirmedHH,
		SUM(CASE  WHEN (T3.Code ='REGCORRECT'  ) THEN 1 ELSE 0 END)  AS AmendedHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)>0    ) then 1 else 0 end)  AS CValHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)=0    ) then 1 else 0 end)  AS PendingAcceptanceHH FROM   ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 	
  AS TG ON TG.SubLocationId  = T1.SubLocationId AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
		AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
		LEFT OUTER JOIN ComValListingPlanHH T2 ON T1.Id =T2.Id left outer JOIN SystemCodeDetail T3 ON T2.StatusId = T3.Id
	WHERE T2.TargetPlanId = @TargetPlanId ) T6 ON T2.TargetPlanId = T6.TargetPlanId
		
	WHERE  T1.Id =@TargetPlanId
		AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
		AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END

END
GO


IF NOT OBJECT_ID('GetPendingComValListingPlanHH') IS NULL	
 DROP PROC GetPendingComValListingPlanHH
 GO
CREATE PROC GetPendingComValListingPlanHH
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN
	SELECT
		COUNT(Id) AS PendingHHs,
		TG.ConstituencyId,
		@TargetPlanId AS TargetPlanId,
		TG.Constituency,
		TG.County,
		TG.LocationId,
		TG.Location
	FROM ComValListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.ComValListingAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
	--AND T1.RejectReason IS NULL
	GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location
END
GO

IF NOT OBJECT_ID('GetPendingComValListingPlanHHCount') IS NULL	
 DROP PROC GetPendingComValListingPlanHHCount
 GO
CREATE PROC GetPendingComValListingPlanHHCount
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	DECLARE @PendingHHs INT = 0
	SELECT
		  COUNT(Id) AS PendingHHs
	FROM ComValListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.ComValListingAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId --AND RejectById IS NULL
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END


 
END
GO


 



IF NOT OBJECT_ID('GetComValListingBatches') IS NULL	
 DROP PROC GetComValListingBatches
 GO
CREATE PROC GetComValListingBatches
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	SELECT DISTINCT T1.*, TG.County, TG.Constituency, TG.ConstituencyId, T2.FirstName AS AcceptBy,
		T3.FirstName AS AcceptApvBy,T3.Id AS AcceptApvById, T4.Name AS TargetPlan
	FROM ComValListingAccept T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.ConstituencyId  = T1.ConstituencyId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.TargetPlanId = @TargetPlanId
		LEFT JOIN [USER] T2 ON T1.AcceptById = T2.Id
		LEFT JOIN [USER] T3 ON T1.AcceptApvById = T3.Id
		LEFT JOIN TargetPlan T4 ON T1.TargetPlanId = T4.Id

END
GO


IF NOT OBJECT_ID('GetHouseholdsReadyForDownload') IS NULL	
DROP PROC GetHouseholdsReadyForDownload
GO

CREATE PROC GetHouseholdsReadyForDownload
	@EnumeratorId int
AS
BEGIN

	SELECT T1.*,

		CASE WHEN T2.CG_IDNoExists = 1 AND T2.CG_SexMatch = 1 AND T2.CG_DoBYearMatch = 1 AND( T2.CG_FirstNameExists =1 OR T2. CG_MiddleNameExists=1 OR T2.CG_SurnameExists =1) THEN 1 ELSE 0 END AS CgMatches,
		CASE WHEN T2.Bene_IDNoExists = 1 AND T2.Bene_SexMatch = 1 AND T2.Bene_DoBYearMatch = 1 AND ( T2.Bene_FirstNameExists =1 OR T2.Bene_MiddleNameExists=1 OR T2.Bene_SurnameExists =1)   THEN 1 ELSE 0 END AS BeneMatches,

		CASE WHEN  T1.BeneNationalIdNo IS NULL THEN 0 ELSE 1 END AS NoBene

	FROM ListingPlanHH T1
		INNER JOIN ListingException T2 ON T1.Id = T2.Id
		INNER JOIN SubLocation T3 ON T1.SubLocationId = T3.Id
		INNER JOIN Location T4 ON T1.LocationId = T4.Id AND T3.LocationId = T4.Id
		INNER JOIN EnumeratorLocation T5 ON T4.Id = T5.LocationId AND T5.IsActive = 1 AND T1.RejectById IS NULL AND T5.EnumeratorId = @EnumeratorId
		LEFT JOIN ComValListingPlanHH T6 ON T1.Id = T6.Id
	WHERE T6.UniqueId IS NULL
END
 go

 

IF NOT OBJECT_ID('GetBatchComValHHListingPendingIPRS') IS NULL	
DROP PROC GetBatchComValHHListingPendingIPRS
GO
CREATE PROC GetBatchComValHHListingPendingIPRS
	@ComValListingAcceptId INT
AS
BEGIN
	DECLARE @PendingValidation int


	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(select 1
	from ComValListingAccept WHERE Id = @ComValListingAcceptId AND AcceptApvDate IS NULL)
SET @ErrorMsg ='This Batch has not been Approved. This Cannot be Run against SR/IPRS.'

	IF EXISTS(select 1 from ComValListingAccept where Id = @ComValListingAcceptId AND IsValidated=1 )
SET @ErrorMsg ='This Batch already been Run against SR/IPRS.'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END


;
	WITH
		X
		AS
		
		(
							SELECT T1.BeneNationalIdNo as IDNumber, CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') 
THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS Names
				FROM  ComValListingPlanHH T1
					LEFT OUTER JOIN (SELECT ID_Number AS IDNo
					FROM IPRSCache) AS T2 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)
				WHERE T2.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId
					AND T1.BeneNationalIdNo IS NOT NULL
			UNION
				SELECT T1.CgNationalIdNo, CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS BeneName
				FROM
					ComValListingPlanHH T1
					LEFT OUTER JOIN ( SELECT ID_Number AS IDNo
					FROM IPRSCache) AS T3 ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)
				WHERE T3.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId
		)
	  
	select @PendingValidation = count(*) 	from X
	UPDATE  ComValListingAccept SET PendingValidation = @PendingValidation, IsValidated = CASE WHEN @PendingValidation =0 THEN 1 ELSE 0 END  
WHERE Id =@ComValListingAcceptId
END
GO
 IF NOT OBJECT_ID('GetPendingBatchIPRS') IS NULL	
DROP PROC GetPendingBatchIPRS
GO
CREATE PROC GetPendingBatchIPRS(
	@ListingAcceptId INT)
AS
BEGIN 
	SELECT T1.BeneNationalIdNo as IDNumber, CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'')  THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS Names FROM ListingPlanHH T1
	LEFT OUTER JOIN (SELECT ID_Number AS IDNo FROM IPRSCache) AS T2 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo) WHERE T2.IDNo is null AND T1.ListingAcceptId = @ListingAcceptId AND T1.BeneNationalIdNo IS NOT NULL
	UNION
	SELECT T1.CgNationalIdNo, CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS BeneName FROM ListingPlanHH T1 
	LEFT OUTER JOIN ( SELECT ID_Number AS IDNo FROM IPRSCache) AS T3 ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo) WHERE T3.IDNo is null AND T1.ListingAcceptId = @ListingAcceptId AND T1.CgNationalIdNo IS NOT NULL	 
END
GO




IF NOT OBJECT_ID('GetHouseholdsReadyForComValDownload') IS NULL	
DROP PROC GetHouseholdsReadyForComValDownload
GO


CREATE PROC GetHouseholdsReadyForComValDownload
	@EnumeratorId int
AS
BEGIN


DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @StatusId INT = NULL

SET @SysCode ='REG_STATUS'
SET @SysDetailCode ='COMMVAL'

SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SELECT T1.*,

		CASE WHEN T2.CG_IDNoExists = 1 AND T2.CG_SexMatch = 1 AND T2.CG_DoBYearMatch = 1 AND( T2.CG_FirstNameExists =1 OR T2. CG_MiddleNameExists=1 OR T2.CG_SurnameExists =1) THEN 1 ELSE 0 END AS CgMatches,
		CASE WHEN T2.Bene_IDNoExists = 1 AND T2.Bene_SexMatch = 1 AND T2.Bene_DoBYearMatch = 1 AND ( T2.Bene_FirstNameExists =1 OR T2.Bene_MiddleNameExists=1 OR T2.Bene_SurnameExists =1)   THEN 1 ELSE 0 END AS BeneMatches,

		CASE WHEN  T1.BeneNationalIdNo IS NULL THEN 0 ELSE 1 END AS HasBene

	FROM ListingPlanHH T1
		INNER JOIN ListingException T2 ON T1.Id = T2.Id
		INNER JOIN SubLocation T3 ON T1.SubLocationId = T3.Id
		
		INNER JOIN Location T4 ON T1.LocationId = T4.Id AND T3.LocationId = T4.Id
		INNER JOIN EnumeratorLocation T5 ON T4.Id = T5.LocationId AND T5.IsActive = 1  AND T1.RejectById IS NULL AND T5.EnumeratorId = @EnumeratorId
		LEFT JOIN ComValListingPlanHH T6 ON T1.Id = T6.Id
		LEFT JOIN TargetPlan T7 ON  T6.TargetPlanId = T7.Id   AND T7.StatusId = @StatusId
		
	WHERE T6.UniqueId IS NULL
END

GO 
   
IF NOT OBJECT_ID('GetComValBatchPendingIPRS') IS NULL	
DROP PROC GetComValBatchPendingIPRS
GO


CREATE PROC GetComValBatchPendingIPRS(
	@ComValListingAcceptId INT)
AS
BEGIN




	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)

	DECLARE @ConfirmedId INT = NULL
	DECLARE @CorrectedId INT = NULL
	DECLARE @PendingPersons INT = NULL
	
	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGCORRECT'

	SELECT @CorrectedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='REGCONFIRM'
 SELECT @ConfirmedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 
 delete from temp_IPRS where ComValListingAcceptId = @ComValListingAcceptId
 ;with X as (
			SELECT T1.BeneNationalIdNo as IDNumber, CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') 
THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS Names
		FROM ComValListingPlanHH T1
			LEFT OUTER JOIN (SELECT ID_Number AS IDNo
			FROM IPRSCache) AS T2 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)
		WHERE T2.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId AND T1.StatusId IN (@CorrectedId,@ConfirmedId)
		AND T1.BeneNationalIdNo IS NOT NULL
	UNION
		SELECT T1.CgNationalIdNo, CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS BeneName
		FROM
			ComValListingPlanHH T1 LEFT OUTER JOIN ( SELECT ID_Number AS IDNo
			FROM IPRSCache) AS T3 ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)
		WHERE T3.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId AND T1.StatusId IN (@CorrectedId,@ConfirmedId) 
		AND T1.CgNationalIdNo IS NOT NULL
		)
		insert into temp_IPRS(IdNumber,Names,ComValListingAcceptId)
		SELECT IdNumber,Names,@ComValListingAcceptId as ComValListingAcceptId  FROM X


	select @PendingPersons = count(*) from temp_IPRS where ComValListingAcceptId = @ComValListingAcceptId
	UPDATE  ComValListingAccept SET PendingValidation = @PendingPersons, IsValidated = CASE WHEN @PendingPersons =0 THEN 1 ELSE 0 END  
WHERE Id =@ComValListingAcceptId


SELECT IdNumber,Names FROM temp_IPRS where ComValListingAcceptId = ComValListingAcceptId 

delete FROM temp_IPRS --where ComValListingAcceptId = ComValListingAcceptId  
END

GO



 IF NOT OBJECT_ID('GetComValSummary') IS NULL	
DROP PROC GetComValSummary
GO
 
CREATE PROC GetComValSummary
	@Id int
AS
BEGIN
	SELECT
		COUNT(T1.Id) AS RegHH,
		SUM(CASE  WHEN (T3.Code ='REGEXIT' ) THEN 1 ELSE 0 END)  AS ExitedHH,
		SUM(CASE  WHEN (T3.Code ='REGCONFIRM'  ) THEN 1 ELSE 0 END)  AS ConfirmedHH,
		SUM(CASE  WHEN (T3.Code ='REGCORRECT'  ) THEN 1 ELSE 0 END)  AS AmendedHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)>0    ) then 1 else 0 end)  AS CValHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)=0    ) then 1 else 0 end)  AS PendingAcceptanceHH
	FROM   ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 	
  AS TG ON TG.SubLocationId  = T1.SubLocationId AND T1.RejectById IS NULL
		LEFT OUTER JOIN ComValListingPlanHH T2 ON T1.Id =T2.Id left outer JOIN SystemCodeDetail T3 ON T2.StatusId = T3.Id
	WHERE T1.TargetPlanId = @Id 

END
GO



IF NOT OBJECT_ID('GetHHListingClosureSummary') IS NULL	
DROP PROC GetHHListingClosureSummary
GO
CREATE PROC GetHHListingClosureSummary
	@TargetPlanId int
    AS
    BEGIN
SELECT T4.Id, T4.Name 'ProgrammeName', COUNT(T1.Id) 'BeneficiaryHousehold'   FROM ComValListingPlanHH   T1
INNER JOIN TargetPlan T2 ON T2.Id = T1.TargetPlanId
INNER JOIN  ListingPlanProgramme T3 ON T1.ProgrammeId = T3.ProgrammeId and T2.Id = T3.TargetPlanId
INNER JOIN Programme T4 ON T4.Id = T1.ProgrammeId
 INNER JOIN  SystemCodeDetail T7 ON T1.StatusId = T7.Id  AND T7.Code IN ('REGCONFIRM','REGCORRECT')
 INNER JOIN dbo.ComValListingException T8  ON T8.Id = T1.Id  
WHERE 
	(T8.BeneNationalIdNo IS NOT NULL AND (T8.Bene_SexMatch = 1 AND T8.Bene_DoBYearMatch = 1 AND ( T8.Bene_FirstNameExists =1 OR T8.Bene_MiddleNameExists=1 OR T8.Bene_SurnameExists =1)) OR  T8.BeneNationalIdNo IS NULL)
	AND (T8.CG_SexMatch = 1 AND T8.CG_DoBYearMatch = 1 AND( T8.CG_FirstNameExists =1 OR T8.CG_MiddleNameExists=1 OR  T8.CG_SurnameExists =1)) AND T8.TargetPlanId=@TargetPlanId
  
  GROUP BY T4.Id,T4.Name

  END

GO

/**** HOUSEHOLD  REGISTRATION*/

IF NOT OBJECT_ID('GetTargetPlanProgrammeOptions') IS NULL	
   DROP PROC GetTargetPlanProgrammeOptions
GO
CREATE PROC GetTargetPlanProgrammeOptions
AS
BEGIN
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @CategoryId INT
DECLARE @StatusId INT

DECLARE @PrimaryRecipientId INT


SET @SysCode='REG_STATUS'
SET @SysDetailCode='CLOSED'

SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'

SELECT @PrimaryRecipientId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



SELECT  T1.Id ListingPlanId, T2.Id ProgrammeId,  CONCAT(T1.[Name],' - ', T2.[Name]) AS  TarName FROM  TargetPlan T1
LEFT JOIN Programme T2 ON 1=1
LEFT JOIN TargetPlanProgramme T3 ON T2.Id = T3.ProgrammeId  AND T1.Id = T3.TargetPlanId
WHERE T1.StatusId = @StatusId  AND T3.TargetPlanId IS NULL AND T2.IsActive =1
AND T2.PrimaryRecipientId = @PrimaryRecipientId

END

GO





  


IF NOT OBJECT_ID('GetHouseholdsReadyForRegistration') IS NULL	
 DROP PROC GetHouseholdsReadyForRegistration
GO
CREATE PROC GetHouseholdsReadyForRegistration
	@EnumeratorId INT
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @CategoryId INT
	DECLARE @StatusId INT
	DECLARE @ValidHHStatusId INT

	SET @SysCode='REG_CATEGORIES'
	SET @SysDetailCode='REGISTRATION'
	SELECT @CategoryId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='ACTIVE'
	SELECT @StatusId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 

 	DECLARE @ConfirmedId INT
   DECLARE @AmmendedId INT

	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGCONFIRM'
	SELECT @ConfirmedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		 
SET @SysDetailCode='REGCORRECT'
	SELECT @AmmendedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
  	 

  
  
	SELECT DISTINCT T3.Id, T3.UniqueId, T3.StartTime, T3.EndTime, T3.ProgrammeId, T3.RegDate, T3.SubLocationId, T3.LocationId, T3.Years, T3.Months, T1.Id as TargetPlanId, T3.EnumeratorId, T3.BeneFirstName, T3.BeneMiddleName, T3.BeneSurname, T3.BeneNationalIdNo, T3.BenePhoneNumber, T3.BeneSexId, T3.BeneDoB, T3.CgFirstName, T3.CgMiddleName, T3.CgSurname, T3.CgNationalIdNo, T3.CgSexId, T3.CgDoB, T3.HouseholdMembers, T3.StatusId, T3.Village, T3.PhysicalAddress, T3.NearestReligiousBuilding, T3.NearestSchool, T3.Longitude, T3.Latitude, T3.SyncEnumeratorId, T3.SyncDate, T3.EnumeratorDeviceId, T3.ComValListingAcceptId, T3.AppVersion, T3.AppBuild, T3.CgPhoneNumber, T3.SyncUpDate, T3.DownloadDate
	FROM TargetPlan T1
	INNER JOIN TargetPlanProgramme T2 ON T1.Id = T2.TargetPlanId
	INNER JOIN ComValListingPlanHH T3 ON  T2.ListingPlanId = T3.TargetPlanId AND T2.TargetPlanId = T1.Id  
	AND T1.StatusId = @StatusId AND T1.CategoryId = @CategoryId 
	AND T3.StatusId IN( @AmmendedId,@ConfirmedId)
	INNER JOIN EnumeratorLocation T4 ON T3.LocationId = T4.LocationId AND T4.IsActive = 1 AND T4.EnumeratorId= @EnumeratorId
	 
	 INNER JOIN Programme T6 ON T6.Id = T3.ProgrammeId
 	
      INNER JOIN SystemCodeDetail T7 ON T6.PrimaryRecipientId =T7.Id  AND T7.Code='Caregiver'
	    INNER JOIN dbo.ComValListingException T8  ON T8.Id = T3.Id  
WHERE 
	(T8.BeneNationalIdNo IS NOT NULL AND (T8.Bene_SexMatch = 1 AND T8.Bene_DoBYearMatch = 1 AND ( T8.Bene_FirstNameExists =1 OR T8.Bene_MiddleNameExists=1 OR T8.Bene_SurnameExists =1)) OR  T8.BeneNationalIdNo IS NULL)
	AND (T8.CG_SexMatch = 1 AND T8.CG_DoBYearMatch = 1 AND( T8.CG_FirstNameExists =1 OR T8.CG_MiddleNameExists=1 OR  T8.CG_SurnameExists =1))  
  
END
GO


IF NOT OBJECT_ID('GetRegistrationSummary') IS NULL	
 DROP PROC GetRegistrationSummary
GO
CREATE PROC GetRegistrationSummary
	@TargetPlanId int,
	@CountyId int = null,
	@ConstituencyId int = null
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @ConfirmedId INT
	DECLARE @AmmendedId INT
 

	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGCONFIRM'
	SELECT @ConfirmedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		 
SET @SysDetailCode='REGCORRECT'
	SELECT @AmmendedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
		ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
  	 

	SELECT  
	      MAX(T1.ExceptionsFileId) AS ExceptionsFileId
		, MAX(T1.ValidationFileId) AS ValidationFileId 
		, MAX(T7.HouseholdsReady)  AS ListedHouseholds 
		, SUM(CASE WHEN(T2.Id>0) THEN 1 ELSE 0 END) AS RegisteredHouseholds
		, SUM(CASE WHEN(T4.Code='01') THEN 1 ELSE 0 END) AS CompletedHouseholds
		, SUM(CASE WHEN(T4.Code='04') THEN 1 ELSE 0 END) AS NoOneAtHomeHouseholds
		, SUM(CASE WHEN(T4.Code='05') THEN 1 ELSE 0 END) AS CannotFindHouseholds
		, MAX(T5.AcceptedBatches) AS AcceptedBatches
		, SUM(CASE WHEN(T2.HouseholdRegAcceptId>0) THEN 1 ELSE 0 END) AS AcceptedHouseholds
		, SUM(CASE WHEN(T2.HouseholdRegAcceptId IS NULL AND T2.Id>0) THEN 1 ELSE 0 END) AS PendingHouseholds
 
	FROM TargetPlan  T1
		
			    INNER JOIN ( SELECT  T2.TargetPlanId, COUNT(T1.Id) AS HouseholdsReady FROM TargetPlan T1
					INNER JOIN TargetPlanProgramme T2 ON T1.Id = T2.TargetPlanId AND T1.Id =@TargetPlanId
					INNER JOIN ComValListingPlanHH T3 ON  T2.ListingPlanId = T3.TargetPlanId AND T2.TargetPlanId = T1.Id  AND T3.StatusId IN( @AmmendedId,@ConfirmedId) 
					INNER JOIN Programme T6 ON T6.Id = T3.ProgrammeId  AND T2.ProgrammeId = T6.Id
					INNER JOIN SystemCodeDetail T7 ON T6.PrimaryRecipientId =T7.Id  AND T7.Code='Caregiver'
					INNER JOIN dbo.ComValListingException T8  ON T8.Id = T3.Id  
					AND 
					(T8.BeneNationalIdNo IS NOT NULL AND (T8.Bene_SexMatch = 1 AND T8.Bene_DoBYearMatch = 1 AND ( T8.Bene_FirstNameExists =1 OR T8.Bene_MiddleNameExists=1 OR T8.Bene_SurnameExists =1)) OR  T8.BeneNationalIdNo IS NULL)
					AND (T8.CG_SexMatch = 1 AND T8.CG_DoBYearMatch = 1 AND( T8.CG_FirstNameExists =1 OR T8.CG_MiddleNameExists=1 OR  T8.CG_SurnameExists =1))  	 
					GROUP BY T2.TargetPlanId
				) T7 ON T7.TargetPlanId = T1.Id 
			LEFT JOIN HouseholdReg T2 ON T1.Id =T2.TargetPlanId
			LEFT JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T2.SubLocationId 
	AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
	 AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END

 LEFT JOIN SystemCodeDetail T4 ON T2.InterviewResultId = T4.Id
		 LEFT JOIN ( SELECT DISTINCT Id, TargetPlanId , ReceivedHHs, ConstituencyId FROM HouseholdRegAccept)  T3 ON T1.Id =  T3.TargetPlanId AND T2.HouseholdRegAcceptId = T3.Id      
		 LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS AcceptedBatches	FROM HouseholdRegAccept GROUP BY TargetPlanId ) T5 ON T3.Id=T5.TargetPlanId
	    LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS AcceptedBatches	FROM HouseholdRegAccept GROUP BY TargetPlanId ) T6 ON T3.Id=T6.TargetPlanId

	
	  WHERE  T1.Id =@TargetPlanId
	  
	   
END
GO




IF NOT OBJECT_ID('GetPendingRegistrationPlanHH') IS NULL	
 DROP PROC GetPendingRegistrationPlanHH
 GO
CREATE PROC GetPendingRegistrationPlanHH
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN
	SELECT
		COUNT(Id) AS PendingHHs,
		TG.ConstituencyId,
		@TargetPlanId AS TargetPlanId,
		TG.Constituency,
		TG.County,
		TG.LocationId,
		TG.Location
	FROM HouseholdReg T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.HouseholdRegAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
		  
	GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location
END
GO
















IF NOT OBJECT_ID('GetPendingRegistrationPlanHHCount') IS NULL	
 DROP PROC GetPendingRegistrationPlanHHCount
 GO
CREATE PROC GetPendingRegistrationPlanHHCount
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	DECLARE @PendingHHs INT = 0
	SELECT
		@PendingHHs = COUNT(Id)
	FROM HouseholdReg T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.HouseholdRegAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId  
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
 
	GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location

	SELECT @PendingHHs AS PendingHHs
END
GO





IF NOT OBJECT_ID('GetRegistrationBatches') IS NULL	
 DROP PROC GetRegistrationBatches
 GO
CREATE PROC GetRegistrationBatches
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	SELECT DISTINCT T1.*, TG.County, TG.Constituency, TG.ConstituencyId, T2.FirstName AS AcceptBy,
		T3.FirstName AS AcceptApvBy, T4.Name AS TargetPlan
	FROM  HouseholdRegAccept T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.ConstituencyId  = T1.ConstituencyId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.TargetPlanId = @TargetPlanId
		LEFT JOIN [USER] T2 ON T1.AcceptById = T2.Id
		LEFT JOIN [USER] T3 ON T1.AcceptApvById = T3.Id
		LEFT JOIN TargetPlan T4 ON T1.TargetPlanId = T4.Id

END
GO



IF NOT OBJECT_ID('ProcessHouseholdReg') IS NULL	
DROP PROC ProcessHouseholdReg
GO
CREATE PROC ProcessHouseholdReg
	@TargetPlanId int
   ,@UserId int

AS

BEGIN
 
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @HHStatusId INT
DECLARE @MemberStatusId INT

DECLARE @CgRoleId INT
DECLARE @MemberRoleId INT

SET @SysCode='HhStatus'
SET @SysDetailCode='REG'
SELECT @HHStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @MemberStatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SET @SysCode='Member Role'
SET @SysDetailCode='CAREGIVER'
SELECT @CgRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Member Role'
SET @SysDetailCode='BENEFICIARY'
SELECT @MemberRoleId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON   T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



INSERT INTO Household (ProgrammeId,RegGroupId,RefId,Village,StatusId,CreatedBy,CreatedOn) 
SELECT T1.ProgrammeId,  T1.TargetPlanId,T1.Id, T1.Village,@HHStatusId,@UserId,GETDATE() FROM  HouseholdReg   T1
INNER JOIN TargetPlan T2 ON T2.Id = T1.TargetPlanId
AND  T1.TargetPlanId =@TargetPlanId 
 
INSERT INTO Person(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,CreatedBy,CreatedOn)
SELECT 'RegRefId: '+CONVERT(varchar,T1.Id),T1.FirstName,T1.MiddleName,T1.Surname,T1.SexId,T1.DateOfBirth,T1.IdentificationNumber,T1.IdentificationNumber,T1.PhoneNumber,0,'',0,@UserId,GETDATE()
FROM dbo.HouseholdRegMember T1 
INNER JOIN dbo.HouseholdReg T2 ON T1.HouseholdRegId = T2.Id
INNER JOIN dbo.TargetPlan T3 ON T2.Id = T3.Id AND T3.Id = @TargetPlanId
LEFT JOIN  dbo.Person T4 ON 'RegRefId: '+CONVERT(varchar,T1.Id) = T4.RefId WHERE  T4.RefId IS  NULL
	
INSERT INTO HouseholdMember(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
SELECT T8.Id,T2.Id,T1.RelationshipId, 

CASE WHEN (T9.CountCareGiverId>0) THEN @CgRoleId ELSE @MemberRoleId END
,@MemberStatusId,@UserId,GETDATE()
FROM dbo.HouseholdRegMember T1 
INNER JOIN dbo.HouseholdReg T2 ON T1.HouseholdRegId = T2.Id
INNER JOIN dbo.TargetPlan T3 ON T2.Id = T3.Id AND T3.Id = @TargetPlanId
INNER JOIN Person T7 ON 'RegRefId: '+CONVERT(varchar,T1.Id) = T7.RefId
INNER JOIN Household T8 on T2.TargetPlanId =T8.RegGroupId and T2.TargetPlanId=@TargetPlanId
LEFT OUTER JOIN ( SELECT CareGiverId, COUNT(CareGiverId) 'CountCareGiverId'    FROM  HouseholdRegMember GROUP BY CareGiverId ) T9 ON T1.MemberId = T9.CareGiverId 
			 
END
GO

