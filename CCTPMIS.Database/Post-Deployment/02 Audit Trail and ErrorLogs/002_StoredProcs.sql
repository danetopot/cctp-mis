﻿IF NOT OBJECT_ID('AddAuditTrail') IS NULL
DROP PROC AddAuditTrail
GO

CREATE PROC AddAuditTrail  
 @IPAddress varchar(15)  
   ,@MACAddress varchar(17)=NULL  
   ,@IMEI bigint=NULL  
   ,@ModuleRightCode varchar(64)  
   ,@Description varchar(64)   =''
   ,@TableName varchar(64)  
   ,@UserAgent varchar(64)  
   ,@Key1 int  
   ,@Key2 int=NULL  
   ,@Key3 int=NULL  
   ,@Key4 int=NULL  
   ,@Key5 int=NULL  
   ,@Record nvarchar(MAX)  
   ,@WasSuccessful bit  
   ,@UserId int = null  
AS  
BEGIN  
 DECLARE @ModuleRightId int  
 DECLARE @TableId int  
 DECLARE @SysCode varchar(20)  
 DECLARE @NoOfRows int  
 DECLARE @ErrorMsg varchar(128)  
  
 SET @SysCode='System Right'  
 SELECT @ModuleRightId=T1.Id FROM ModuleRight T1 INNER JOIN Module T2 ON T1.ModuleId=T2.Id   
			 INNER JOIN SystemCodeDetail T3 ON T1.RightId=T3.Id  
			 INNER JOIN SystemCode T4 ON T3.SystemCodeId=T4.Id AND T4.Code=@SysCode  
WHERE UPPER(T2.Name+':'+T1.Description)=UPPER(@ModuleRightCode)  
  
 BEGIN TRAN  

 SELECT @TableId =  Id FROM  UAT_CCTPMIS_AUDIT.dbo. LogTable where TableName = @TableName

 IF(ISNULL(@TableId,0)=0)
 insert into  UAT_CCTPMIS_AUDIT.dbo.LogTable(ModuleRightId,TableName)
 SELECT @ModuleRightId, @TableName
 
 DECLARE @FullName varchar(100)
 DECLARE @Email varchar(100)
 
 select @FullName= concat(Firstname,' ',MiddleName, '', Surname), @Email = Email from [User] where Id = @userId
 
 SELECT @TableId =  Id FROM  UAT_CCTPMIS_AUDIT.dbo. LogTable where TableName = @TableName
 
 INSERT INTO  UAT_CCTPMIS_AUDIT.dbo. AuditTrail(UserId,LogTime,IPAddress,MACAddress,IMEI,ModuleRightId,Key1,Key2,Key3,Key4,Key5,Record,TableId,WasSuccessful,FullName,Module,Description,UserName)  
 SELECT @UserId,GETDATE(),@IPAddress,@MACAddress,@IMEI,@ModuleRightId,@Key1,@Key2,@Key3,@Key4,@Key5,@Record  ,@TableId,@WasSuccessful,@FullName,@ModuleRightCode, @Description,@Email
 
 COMMIT TRAN     
 SELECT  0   
END  
GO



IF NOT OBJECT_ID('GetAuditTrail') IS NULL	DROP PROC GetAuditTrail
GO
CREATE PROC GetAuditTrail
	@Id int=NULL
   ,@UserId int=NULL
   ,@StartDate datetime=NULL
   ,@EndDate datetime=NULL
   ,@IPAddress varchar(15)=NULL
   ,@MACAddress varchar(15)=NULL
   ,@IMEI bigint=NULL
   ,@ModuleId int=NULL
   ,@ModuleRightId int=NULL
   ,@Key1 int=null
   ,@Key2 int=null
   ,@Key3 int=null
   ,@Key4 int=null
   ,@Key5 int=null
AS
BEGIN
	SELECT T1.Id,T1.UserId,T5.UserName,T1.LogTime,T1.IPAddress,T1.MACAddress,T1.IMEI,T3.Name AS Module,T1.ModuleRightId,T2.[Description] AS ModuleRightDescription,T1.Record,T1.WasSuccessful
	FROM UAT_CCTPMIS_AUDIT.dbo.AuditTrail T1 INNER JOIN ModuleRight T2 ON T1.ModuleRightId=T2.Id
					   INNER JOIN Module T3 ON T2. ModuleId=T3.Id
					   INNER JOIN SystemCodeDetail T4 ON T2.RightId=T4.Id
					   INNER JOIN [User] T5 ON T1.UserId=T5.Id
	WHERE T1.Id=ISNULL(@Id,T1.Id)
		AND T1.UserId=ISNULL(@UserId,T1.UserId)
		AND T1.LogTime>=ISNULL(@StartDate,T1.LogTime) AND T1.LogTime<=ISNULL(@EndDate,T1.LogTime)
		AND T1.IPAddress=ISNULL(@IPAddress,T1.IPAddress)
		AND T1.MACAddress=ISNULL(@MACAddress,T1.MACAddress)
		AND T1.IMEI=ISNULL(@IMEI,T1.IMEI)
		AND T3.Id=ISNULL(@ModuleId,T3.Id)
		AND T1.ModuleRightId=ISNULL(@ModuleRightId,T1.ModuleRightId)
		AND T1.Key1=ISNULL(@key1,T1.Key1)
		AND T1.Key2=ISNULL(@key2,T1.Key2)
		AND T1.Key3=ISNULL(@key3,T1.Key3)
		AND T1.Key4=ISNULL(@key4,T1.Key4)
		AND T1.Key5=ISNULL(@key5,T1.Key5)
END
GO
