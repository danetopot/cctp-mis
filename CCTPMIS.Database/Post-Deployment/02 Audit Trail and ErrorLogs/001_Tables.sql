﻿IF NOT OBJECT_ID('AuditTrailDetail') IS NULL	DROP TABLE AuditTrailDetail
GO

IF NOT OBJECT_ID('AuditTrail') IS NULL	
DROP TABLE AuditTrail
GO
CREATE TABLE AuditTrail(
	Id int NOT NULL IDENTITY(1,1)
   ,UserId int   NULL
   ,UserName VARCHAR(100)
   ,FullName VARCHAR(100)
   ,Module VARCHAR(100)
   ,Description VARCHAR(100)
   ,LogTime	datetime NOT NULL
   ,IPAddress varchar(15) NOT NULL
   ,MACAddress varchar(17) NULL
   ,IMEI varchar(36) NULL
   ,ModuleRightId  int  NOT NULL
   ,TableId  int NOT NULL
   ,Key1 int NOT NULL
   ,Key2 int NULL
   ,Key3 int NULL
   ,Key4 int NULL
   ,Key5 int NULL
   ,Record nvarchar(MAX) NULL
   ,WasSuccessful bit 
   ,CONSTRAINT PK_AuditTrail PRIMARY KEY (Id)
 )
GO



IF NOT OBJECT_ID('LogTable') IS NULL
	DROP TABLE LogTable
GO

CREATE TABLE LogTable  (
	Id int NOT NULL IDENTITY(1,1)
	,ModuleRightId  int   NULL
	,TableName varchar(100)
 )
 GO
