﻿
IF NOT OBJECT_ID('AddEditCaseCategory') IS NULL	
DROP PROC AddEditCaseCategory
GO

CREATE PROC AddEditCaseCategory
	@Id int=NULL
   ,
	@CaseTypeId int
   ,
	@Name varchar(20)
   ,
	@Code varchar(20)
   ,
	@Description varchar(128)
   ,
	@DocumentsToAttach varchar(128)
   ,
	@ReportingProcess varchar(128)
   ,
	@Timeline int
   ,
	@TypeOfDayId int
   ,
	@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id
	FROM [User]
	WHERE Id=@UserId



	IF ISNULL(@CaseTypeId,0)=0
		SET @ErrorMsg='Please specify valid Case Type'	
	ELSE IF ISNULL(@Code,'')=''
		SET @ErrorMsg='Please specify valid Code parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'

	IF @DocumentsToAttach IS NULL
		SET @ErrorMsg='Please specify valid DocumentsToAttach parameter'

	IF @ReportingProcess IS NULL
		SET @ErrorMsg='Please specify valid ReportingProcess parameter'

	ELSE IF EXISTS(SELECT 1
	FROM CaseCategory
	WHERE CaseTypeId=@CaseTypeId AND Code=@Code AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the  Code ('+@Code+') under the same Case Type '
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.CaseTypeId=@CaseTypeId
		   ,T1.Name=@Name
		   ,T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.DocumentsToAttach=@DocumentsToAttach
		   ,T1.ReportingProcess=@ReportingProcess
		   ,T1.Timeline=@Timeline
		   ,T1.TypeofDayId=@TypeofDayId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM CaseCategory T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO CaseCategory
			(CaseTypeId,[Name],Code,[Description],DocumentsToAttach,ReportingProcess,Timeline, TypeofDayId, CreatedBy,CreatedOn)
		SELECT @CaseTypeId, @Name, @Code, @Description, @DocumentsToAttach, @ReportingProcess, @Timeline, @TypeofDayId, @UserId, GETDATE()

		SET @Id=IDENT_CURRENT('CaseCategory')
	END

	SET @NoOfRows=@@ROWCOUNT

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

	END
END

GO



Declare @Id int
   ,@CaseTypeId int
   ,@Name varchar(20)
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@DocumentsToAttach varchar(128)
   ,@ReportingProcess varchar(128)
   ,@Timeline int
   ,@TypeOfDayId int
   ,@UserId int
   ,@SystemCode varchar(20)
   ,@SystemDescription varchar(128)
 
   ,@SysCode varchar(20)
   ,@SysDetailCode varchar(128)


SET @SysCode='SLA Day Types'
SET @SysDetailCode='Working'
SELECT @TypeOfDayId=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SET @UserId =1
SET @SysCode='Case Type'
SET @SysDetailCode='EFC'
SELECT @CaseTypeId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF(@CaseTypeId IS NOT NULL)
BEGIN

	SELECT @Name='Quality of service', 
			@Code='Quality_of_service', 
			@Description='Use the form  to  record  the client''s experience  about our programmes and how we can improve our offer',
			@DocumentsToAttach='Quality Service Evaluation Form',
			@ReportingProcess='SAU Officer to fill in the clients comments into the System', 
			@Timeline=10, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
			@CaseTypeId=@CaseTypeId,
			@Name=@Name,
			@Code=@Code,
			@Description=@Description ,
			@DocumentsToAttach=@DocumentsToAttach,
			@ReportingProcess=@ReportingProcess ,
			@Timeline=@Timeline ,
			@TypeOfDayId=@TypeOfDayId ,
			@UserId =@UserId

	SELECT @Name='Corruption / Fraud', 
			@Code='Corruption_Fraud', 
			@Description='Report suspected misconduct or mal administration to maintain our Integrity. All information is protected and will only be visible to SAU''s Head',
			@DocumentsToAttach='Any document that may support the claim',
			@ReportingProcess='Fill in the suspected misconduct or Maladministration', 
			@Timeline=10, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
			@CaseTypeId=@CaseTypeId,
			@Name=@Name,
			@Code=@Code,
			@Description=@Description ,
			@DocumentsToAttach=@DocumentsToAttach,
			@ReportingProcess=@ReportingProcess ,
			@Timeline=@Timeline ,
			@TypeOfDayId=@TypeOfDayId ,
			@UserId =@UserId

	SELECT	@Name='Error', 
			@Code='Error', 
			@Description='Error',
			@DocumentsToAttach='Any document that may support the claim',
			@ReportingProcess='Fill the System Error Form and Submit', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
			@CaseTypeId=@CaseTypeId,
			@Name=@Name,
			@Code=@Code,
			@Description=@Description ,
			@DocumentsToAttach=@DocumentsToAttach,
			@ReportingProcess=@ReportingProcess ,
			@Timeline=@Timeline ,
			@TypeOfDayId=@TypeOfDayId ,
			@UserId =@UserId
 
END




SET @CaseTypeId = NULL

SET @SysCode='Case Type'
SET @SysDetailCode='UPDATE'
SELECT @CaseTypeId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode 
T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF(@CaseTypeId IS NOT NULL)
BEGIN

	SELECT	@Name='HH Member Details Correction', 
			@Code='HHM_CORRECT', 
			@Description='Correction of HH member or Beneficiary or caregiver details',
			@DocumentsToAttach='Copy of National ID or Birth Certificate Copy of letter from the chief or Token from SAU and copy of Paymemnt Card ',
			@ReportingProcess='Sub-County officer fill in the update form for the household and submit the form to SAU for correction', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT	@Name='Household Relocation', 
			@Code='RELOCATION', 
			@Description='Household Relocation  from 1 Sublocation to another',
			@DocumentsToAttach='Sub-County officer''s letter, Scanned copy of National ID and copy of Payment Card',
			@ReportingProcess='Beneficiary to go to the Subcount Officer where they have relocated to.', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT  @Name='Change of PSP Account', 
			@Code='BANK_ACC_CHANGE', 
			@Description='Change of PSP Account',
			@DocumentsToAttach='Copy of ID, Copy of payment card',
			@ReportingProcess='Sub-County officer to write a letter to SAU requesting for change of PSP Account and issuance of a new card', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT  @Name='Change of PSP', 
			@Code='PSP_CHANGE', 
			@Description='Change of PSP',
			@DocumentsToAttach='A letter, Copy of ID, Copy of payment card',
			@ReportingProcess='letter to SAU requesting for change of PSP and issuance of a new card', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT  @Name='Correction of HH details', 
			@Code='HH_CORRECTION', 
			@Description='Correction of Household Info',
			@DocumentsToAttach='A letter from the officer, Copy of ID, Copy of the beneficiaries'' payment card, Copy of birth certificate',
			@ReportingProcess='Sub-County officer fill in the update form for the household', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT @Name='Exit of a Beneficiary Household', 
			@Code='HH_EXIT', 
			@Description='Exit of Household',
			@DocumentsToAttach='Death certificate or Burial permit, A letter from the officer for exiting undeserving case/not traced/ relocated HHs',
			@ReportingProcess='Sub-County officer to fill the form requesting Exit of the Household', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT @Name='Exit of Household Member', 
			@Code='HH_MEMBER_EXIT', 
			@Description='Exit of Household Member',
			@DocumentsToAttach='A letter from the officer detailing the reason for Exit of the Household member',
			@ReportingProcess='Sub-County officer to fill the form requesting Exit of the Household Member', 
			@Timeline=4, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT @Name='Adding of a new Household Member', 
			@Code='NEW_HH_MEMBER', 
			@Description='Adding of a new Household Member',
			@DocumentsToAttach='A letter from the officer, Copy of ID, Copy of the bank card, Birth certificate/notification',
			@ReportingProcess='Beneficiary to produce a copy of a birth certificate or ID Sub-County officer to fill in the update form and forward to SAU', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	SELECT @Name='Death of Beneficiary', 
			@Code='BENEFICIARY_DEATH', 
			@Description='Death of Beneficiary',
			@DocumentsToAttach='Death certificate or Burial permit',
			@ReportingProcess='Sub-County officer to fill the Form and submit to SAU requesting for exit', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId

	 SELECT @Name='Change of caregiver', 
			@Code='CAREGIVER_CHANGE', 
			@Description='Change of Caregiver',
			@DocumentsToAttach='Sub-County officer to write an introductory letter requesting for change to SAU, Copy of caregiver and beneficiary ID, Copy of payment card, Burial permit, Update form',
			@ReportingProcess='Officer to do a request of change of caregiver to SAU, Updates are done and sent to PSPs and programme officer, Officers mobilize the beneficiaries to go for BIOS, PSPs issue new card or reset the BIOS', 
			@Timeline=20, 
			@TypeOfDayId=@TypeOfDayId

	EXEC   dbo.AddEditCaseCategory @Id=null,
		   @CaseTypeId=@CaseTypeId,
		   @Name=@Name,
		   @Code=@Code,
		   @Description=@Description ,
		   @DocumentsToAttach=@DocumentsToAttach,
		   @ReportingProcess=@ReportingProcess ,
		   @Timeline=@Timeline ,
		   @TypeOfDayId=@TypeOfDayId ,
		   @UserId =@UserId
	 
END

SET @CaseTypeId = NULL

SET @SysCode='Case Type'
SET @SysDetailCode='GRIEVANCE'
SELECT @CaseTypeId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode 
T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF(@CaseTypeId IS NOT NULL)
BEGIN
SELECT	@Name='Inclusion/Exclusion', 
		@Code='INCLUSION_EXCLUSION', 
		@Description='Inclusion/Exclusion',
		@DocumentsToAttach='Letter from the officer, Evidence(BWC Minutes), Recommendations ,A list of the potential beneficiaries',
		@ReportingProcess='Officer to carryout investigations, provide evidence and recommendations on inclusion/exclusion cases and submit to SAU and considering the minimum requirements per program. Excluded beneficiaries can be considered when there are replacements to be done in a particular location where beneficiary comes from or when scale-up is done', 
		@Timeline=20, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

SELECT	@Name='Double registration', 
		@Code='DOUBLE_REGISTRATION', 
		@Description='Double registration',
		@DocumentsToAttach='Letter from the officer, Evidence, Recommendations ,Copy of both inua jamii cards and officer to indicate the preferred program to retain in the MIS',
		@ReportingProcess='Officer to carryout investigations, provide evidence and recommendations to SAU ', 
		@Timeline=20, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

 SELECT @Name='Card not received', 
		@Code='CARD_NOT_RECEIVED', 
		@Description='Card not received',
		@DocumentsToAttach='Attach Required documents',
		@ReportingProcess='  ', 
		@Timeline=4, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

SELECT	@Name='Failed Bios', 
		@Code='FAILED_BIOS', 
		@Description='Failed Bios',
		@DocumentsToAttach='Attach Required documents',
		@ReportingProcess='  ', 
		@Timeline=4, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

 SELECT @Name='Non-collection', 
		@Code='NON_COLLECTION', 
		@Description='Non-collection',
		@DocumentsToAttach='Attach Required documents',
		@ReportingProcess=' ', 
		@Timeline=4, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

 SELECT @Name='Less payment', 
		@Code='LESS_PAYMENT', 
		@Description='Less payment',
		@DocumentsToAttach='Attach Required documents',
		@ReportingProcess=' ', 
		@Timeline=10, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

SELECT	@Name='Missed payment', 
		@Code='MISSED_PAYMENT', 
		@Description='Missed payment',
		@DocumentsToAttach='Attach Required documents',
		@ReportingProcess=' ', 
		@Timeline=4, 
		@TypeOfDayId=@TypeOfDayId

EXEC   dbo.AddEditCaseCategory @Id=null,
	   @CaseTypeId=@CaseTypeId,
	   @Name=@Name,
	   @Code=@Code,
	   @Description=@Description ,
	   @DocumentsToAttach=@DocumentsToAttach,
	   @ReportingProcess=@ReportingProcess ,
	   @Timeline=@Timeline ,
	   @TypeOfDayId=@TypeOfDayId ,
	   @UserId =@UserId

END

go