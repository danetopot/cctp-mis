DECLARE @StartDate datetime
DECLARE @EndDate datetime
DECLARE @IndicatorId int
DECLARE @MonitoringPeriodId int

DECLARE @FromMonthId int
DECLARE @ToMonthId int
DECLARE @FromMonthPrevCycleId int
DECLARE @ToMonthPrevCycleId int

SET @MonitoringPeriodId =0

SELECT @StartDate = StartDate, @EndDate = EndDate
FROM MonitoringPeriod
WHERE id = @MonitoringPeriodId
SELECT @FromMonthPrevCycleId=StartMonthId, @ToMonthPrevCycleId = EndMonthId
FROM MonitoringPeriod
WHERE EndDate = DATEADD(DAY, -1, @StartDate)

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_EVER_PAID'

-- Start the Beneficiaries Ever on Payroll

if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN (select DISTINCT HhId, T1.ProgrammeId
                FROM Payroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId = T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId AND T2.PayrollApvOn <= @EndDate ) T4 ON T4.HhId = T3.Id AND T4.ProgrammeId = T3.ProgrammeId
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END
SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_ON_PAYROLL'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN (select DISTINCT HhId, T1.ProgrammeId
                FROM Payroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId = T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId AND T2.PayrollApvOn between @StartDate and @EndDate ) T4 ON T4.HhId = T3.Id AND T4.ProgrammeId = T3.ProgrammeId
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END
SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_EXITED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdExit  T4 ON T3.Id = T4.HhId AND T4.ExitedOn  between @StartDate and @EndDate
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_EVER_EXITED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdExit  T4 ON T3.Id = T4.HhId AND T4.ExitedOn < @EndDate
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_ON_PAYROLL_FIRST'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Z
        AS
        (
            SELECT ROW_NUMBER() OVER(PARTITION BY T1.HHID ORDER BY T2.PaymentCycleId ASC) AS RowId, HhId, T1.ProgrammeId, T2.PayrollApvOn
            FROM Payroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId = T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId
                INNER JOIN Programme T3 ON T3.Id = T2.ProgrammeId AND  T2.PayrollApvOn < @EndDate 
        
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male  , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale , Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN
                (select DISTINCT HhId, T1.ProgrammeId, T1.PaymentCycleId
                FROM Payroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId = T2.PaymentCycleId 
                        AND T1.ProgrammeId = T2.ProgrammeId AND T2.PayrollApvOn between @StartDate and @EndDate 
                )  T4 ON T4.HhId = T3.Id AND T4.ProgrammeId = T3.ProgrammeId
				INNER JOIN Z ON T4.ProgrammeId = Z.ProgrammeId AND T4.HhId = Z.HhId AND Z.RowId =1 and z.PayrollApvOn between @StartDate and @EndDate 
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END
SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_INELIGIBLE'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    DECLARE @Exception_INVALIDBENEID varchar(20)
    DECLARE @Exception_INVALIDCGID varchar(20)
    DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
    DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
    DECLARE @Exception_DUPLICATECGIDIN varchar(20)
    DECLARE @Exception_DUPLICATECGIDACC varchar(20)
    DECLARE @Exception_INVALIDACC varchar(20)
    DECLARE @Exception_INVALIDCARD varchar(20)
    DECLARE @Exception_SUSPICIOUSAMT varchar(20)
    DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
    DECLARE @Exception_INELIGIBLEBENE varchar(20)
    DECLARE @Exception_INELIGIBLECG varchar(20)
    DECLARE @Exception_INELIGIBLESUS varchar(20)
    DECLARE @ErrorMsg varchar(128)
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    SET @SysCode='Exception Type'
    SET @Exception_INVALIDBENEID='INVALIDBENEID'
    SET @Exception_INVALIDCGID='INVALIDCGID'
    SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
    SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
    SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
    SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
    SET @Exception_INVALIDACC='INVALIDACC'
    SET @Exception_INVALIDCARD='INVALIDCARD'
    SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
    SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
    SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
    SET @Exception_INELIGIBLECG='INELIGIBLECG'
    SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T0.ConstituencyId, T1.ProgrammeId, T1.PaymentCycleId,
                SUM(CASE WHEN(T2.Code='M') THEN 1 ELSE 0 END) AS Male 
            , SUM(CASE WHEN(T2.Code='F') THEN 1 ELSE 0 END) AS FeMale
            , Count(T1.HhId) as Total
            FROM Prepayroll T1 INNER JOIN SystemCodeDetail T2 ON T1.BeneSexId=T2.Id
                INNER JOIN SystemCodeDetail T3 ON T1.CGSexId=T3.Id
                INNER JOIN SystemCodeDetail T4 ON T1.HhStatusId=T4.Id
                INNER JOIN PaymentCycleDetail T18 ON T18.PaymentCycleId = T1.PaymentCycleId AND T1.ProgrammeId = T18.ProgrammeId
                INNER JOIN SubLocation T0 ON T1.SubLocationId = T0.Id
                INNER JOIN Constituency T01 ON T01.Id = T0.ConstituencyId
                LEFT JOIN PrepayrollInvalidID T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.BenePersonId=T5.PersonId
                LEFT JOIN PrepayrollDuplicateID T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.BenePersonId=T6.PersonId
                LEFT JOIN PrepayrollIneligible T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
                LEFT JOIN SystemCodeDetail T8 ON T7.ExceptionTypeId=T8.Id AND T8.Code=@Exception_INELIGIBLEBENE
                LEFT JOIN PrepayrollInvalidID T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.CGPersonId=T9.PersonId
                LEFT JOIN PrepayrollDuplicateID T10 ON T1.PaymentCycleId=T10.PaymentCycleId AND T1.ProgrammeId=T10.ProgrammeId AND T1.CGPersonId=T10.PersonId
                LEFT JOIN PrepayrollIneligible T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.HhId=T11.HhId
                LEFT JOIN SystemCodeDetail T12 ON T7.ExceptionTypeId=T12.Id AND T12.Code=@Exception_INELIGIBLECG
                LEFT JOIN BeneficiaryAccount T13 ON T1.BeneAccountId=T13.Id
                LEFT JOIN BeneficiaryPaymentCard T14 ON T1.BenePaymentCardId=T14.Id
                LEFT JOIN PrepayrollSuspicious T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
                LEFT JOIN SystemCodeDetail T16 ON T15.ExceptionTypeId=T16.Id AND T16.Code=@Exception_SUSPICIOUSDORMANCY
                left join Payroll T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
            WHERE T17.HhId IS NULL AND T18.PayrollApvOn BETWEEN @StartDate AND @EndDate
            GROUP BY T0.ConstituencyId, T1.ProgrammeId, T1.PaymentCycleId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END



SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_REGISTERED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id AND T3.CreatedOn  between @StartDate and @EndDate
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'COMPLAINTS_CLOSED_OUTSIDE'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Complaint T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ResolvedOn IS NOT NULL AND T4.ResolvedOn BETWEEN @StartDate AND @EndDate
                INNER JOIN SystemCodeDetail T401 ON T4.ComplaintStatusId = T401.Id ----AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ComplaintTypeId  = T402.Id AND DATEDIFF(day, T4.ResolvedOn,T4.ReceivedOn) >T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'COMPLAINTS_CLOSED_WITHIN'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Complaint T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ResolvedOn IS NOT NULL AND T4.ResolvedOn BETWEEN @StartDate AND @EndDate
                INNER JOIN SystemCodeDetail T401 ON T4.ComplaintStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ComplaintTypeId  = T402.Id AND DATEDIFF(day, T4.ResolvedOn,T4.ReceivedOn) <=T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END


SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'COMPLAINTS_OPEN_OUTSIDE'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Complaint T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ResolvedOn IS   NULL
                INNER JOIN SystemCodeDetail T401 ON T4.ComplaintStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ComplaintTypeId  = T402.Id AND DATEDIFF(day, @EndDate,T4.ReceivedOn) >T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'COMPLAINTS_OPEN_WITHIN'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Complaint T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ResolvedOn IS NULL
                INNER JOIN SystemCodeDetail T401 ON T4.ComplaintStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ComplaintTypeId  = T402.Id AND DATEDIFF(day, @EndDate,T4.ReceivedOn) <=T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'UPDATES_CLOSED_OUTSIDE'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Change T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ClosedOn IS NOT NULL AND T4.ClosedOn BETWEEN @StartDate AND @EndDate
                INNER JOIN SystemCodeDetail T401 ON T4.ChangeStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ChangeTypeId  = T402.Id AND DATEDIFF(day, T4.ClosedOn,T4.ReceivedOn) >T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'UPDATES_CLOSED_WITHIN'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Change T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ClosedOn IS NOT NULL AND T4.ClosedOn BETWEEN @StartDate AND @EndDate
                INNER JOIN SystemCodeDetail T401 ON T4.ChangeStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ChangeTypeId  = T402.Id AND DATEDIFF(day, T4.ClosedOn,T4.ReceivedOn) <=T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END


SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'UPDATES_OPEN_OUTSIDE'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Change T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ClosedOn IS   NULL
                INNER JOIN SystemCodeDetail T401 ON T4.ChangeStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ChangeTypeId  = T402.Id AND DATEDIFF(day, @EndDate,T4.ReceivedOn) >T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'UPDATES_OPEN_WITHIN'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId, SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment T01 ON T3.Id =T01.HhId
                INNER JOIN Change T4 ON T4.HhEnrolmentId = T01.Id AND T4.ProgrammeId = T3.ProgrammeId AND T4.ClosedOn IS NULL
                INNER JOIN SystemCodeDetail T401 ON T4.ChangeStatusId = T401.Id --AND T401.Code=''
                INNER JOIN CaseCategory T402 ON T4.ChangeTypeId  = T402.Id AND DATEDIFF(day, @EndDate,T4.ReceivedOn) <=T402.Timeline
                INNER JOIN Programme T5A ON T5A.Id = T4.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END


SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_ENROLLED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment  T4 ON T3.Id = T4.HhId
                inner join HouseholdEnrolmentPlan T401 ON T4.HhEnrolmentPlanId = T401.Id AND T401.ApvOn BETWEEN @StartDate AND @EndDate
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_ACCOUNTOPENED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment  T4 ON T3.Id = T4.HhId
                inner join BeneficiaryAccount T401 ON T4.Id = T401.HhEnrolmentId AND ISNULL(T401.DateAdded,T401.OpenedOn)  BETWEEN @StartDate AND @EndDate
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'BENE_CARDED'
-- Start the Beneficiaries On Payroll within the Reporting Period
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id ConstituencyId, T2.Id as ProgrammeId
            from Constituency T1  CROSS JOIN Programme T2
        ),
        Y
        AS
        (
            SELECT T3.ProgrammeId, T1.ConstituencyId,
                SUM(CASE WHEN(T7.Code='M') THEN 1 ELSE 0 END) AS Male , SUM(CASE WHEN(T7.Code='F') THEN 1 ELSE 0 END) AS FeMale, Count(T5.Id) as Total
            from
                (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T1.ConstituencyId, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
                FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                    INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                    INNER JOIN District T5 ON T4.DistrictId=T5.Id
                    INNER JOIN County T6 ON T4.CountyId=T6.Id
                    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
                WHERE T8.IsDefault=1 ) T1
                INNER JOIN HouseholdSubLocation T2 ON T2.SubLocationId = T1.SubLocationId
                INNER JOIN Household T3 ON T2.HhId = T3.Id
                INNER JOIN HouseholdEnrolment  T4 ON T3.Id = T4.HhId
                inner join BeneficiaryAccount T401 ON T4.Id = T401.HhEnrolmentId
                INNER JOIN BeneficiaryPaymentCard T402 ON T401.Id = T402.BeneAccountId AND ISNULL(T402.CreatedOn,T402.ModifiedOn) BETWEEN @StartDate AND @EndDate
                    AND T402.PaymentCardNo IS NOT NULL
                INNER JOIN Programme T5A ON T5A.Id = T3.ProgrammeId
                INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T5A.PrimaryRecipientId=T5.MemberRoleId
                INNER JOIN Person T6 ON T5.PersonId=T6.Id
                INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
                LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T5A.SecondaryRecipientId=T8.MemberRoleId
                LEFT JOIN Person T9 ON T8.PersonId=T9.Id
                LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            GROUP BY   T3.ProgrammeId,T1.ConstituencyId
        )
    INSERT INTO PeriodicConstituencyBeneIndicator
        (MonitoringPeriodId,ConstituencyId,ProgrammeId,IndicatorId,Male,Female,Total)
    SELECT @MonitoringPeriodId, x.ConstituencyId, x.ProgrammeId, @IndicatorId, ISNULL(MALE,0), ISNULL(Female,0), ISNULL(Total,0)
    FROM X LEFT JOIN Y ON X.ConstituencyId = Y.ConstituencyId and x.ProgrammeId = y.ProgrammeId
END



SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'FUNDS_REQUESTED'
if(@IndicatorId >0)
BEGIN
    ;WITH
        X
        AS
        (
            SELECT T1.Id as ProgrammeId, T2.ID AS PspId
            --, T3.Id FundsRequestId
            FROM Programme T1 CROSS JOIN PSP T2
            -- CROSS JOIN FundsRequest T3
        ) ,
        Y
        AS
        (
            SELECT T1.ProgrammeId, T1.PSPId, T3.FromMonthId,T3.ToMonthId,
            SUM((ISNULL(T1.CommissionAmount,0) + ISNULL(T1.EntitlementAmount,0) +ISNULL(T1.OtherAmount,0) )) Amount
            
            FROM FundsRequestDetail T1
                INNER JOIN FundsRequest T2 ON T1.FundsRequestId = T2.Id
                INNER JOIN PaymentCycle T3 ON T2.PaymentCycleId = T3.Id
                INNER JOIN PaymentCycleDetail T4 ON T3.Id = T4.PaymentCycleId and T4.ProgrammeId = T1.ProgrammeId 
                AND T4.FundsRequestApvOn BETWEEN @StartDate AND @EndDate
            GROUP BY T1.ProgrammeId, T1.PSPId,T3.FromMonthId,T3.ToMonthId
        )

    INSERT INTO PeriodicPaymentIndicator(MonitoringPeriodId,ProgrammeId,IndicatorId,PspId,AmountWithin,AmountWithout)
    SELECT
        @MonitoringPeriodId, x.ProgrammeId, @IndicatorId, x.PspId, CASE WHEN  y.Amount IS NULL THEN 0 ELSE  y.Amount  END , 0
    FROM X FULL OUTER JOIN Y ON X.ProgrammeId = Y.ProgrammeId AND X.PSPId = Y.PSPId
    ORDER BY  X.ProgrammeId

END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'PAYMENT_DISBURSEMENT'
if(@IndicatorId >0)
BEGIN

    ;WITH
        X
        AS
        (
            SELECT T1.Id as ProgrammeId, T2.ID AS PspId
            FROM Programme T1 CROSS JOIN PSP T2
        )
,
        Y
        AS
        (
            SELECT T1.ProgrammeId, T6.PSPId, SUM(T1.PaymentAmount) PaymentAmount
            FROM Payroll T1
                INNER JOIN PaymentCycle T2 ON T2.Id = T1.PaymentCycleId
                INNER JOIN PaymentCycleDetail T3 ON T2.Id = T3.PaymentCycleId and T3.ProgrammeId = T1.ProgrammeId
                INNER JOIN HouseholdEnrolment T4 ON T1.HhId = T4.HhId
                INNER JOIN BeneficiaryAccount T5 ON T4.Id = T5.HhEnrolmentId
                INNER JOIN PSPBranch T6 ON T5.PSPBranchId = T6.Id
                    AND T3.PayrollApvOn BETWEEN @StartDate AND @EndDate
            GROUP BY T1.ProgrammeId, T6.PSPId
        )

    INSERT INTO PeriodicPaymentIndicator
        (MonitoringPeriodId,ProgrammeId,IndicatorId,PSPId,AmountWithin,AmountWithout,PercentWithin,PercentWithout)
    SELECT
        @MonitoringPeriodId, x.ProgrammeId, @IndicatorId, X.PSPId, ISNULL(y.PaymentAmount,0),0,0,0
    FROM X FULL OUTER JOIN Y ON X.ProgrammeId = Y.ProgrammeId AND X.PSPId = Y.PSPId
    ORDER BY  X.ProgrammeId
END
SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'TOTAL_PAYMENT_DISBURSEMENT'
if(@IndicatorId >0)
BEGIN

    ;WITH
        X
        AS
        (
            SELECT T1.Id as ProgrammeId, T2.ID AS PspId
            FROM Programme T1 CROSS JOIN PSP T2
        )
,
        Y
        AS
        (
            SELECT T1.ProgrammeId, T6.PSPId, SUM(T1.PaymentAmount) PaymentAmount
            FROM Payroll T1
                INNER JOIN PaymentCycle T2 ON T2.Id = T1.PaymentCycleId
                INNER JOIN PaymentCycleDetail T3 ON T2.Id = T3.PaymentCycleId and T3.ProgrammeId = T1.ProgrammeId
                INNER JOIN HouseholdEnrolment T4 ON T1.HhId = T4.HhId
                INNER JOIN BeneficiaryAccount T5 ON T4.Id = T5.HhEnrolmentId
                INNER JOIN PSPBranch T6 ON T5.PSPBranchId = T6.Id
                    AND T3.PayrollApvOn < @EndDate
            GROUP BY T1.ProgrammeId, T6.PSPId
        )

    INSERT INTO PeriodicPaymentIndicator
        (MonitoringPeriodId,ProgrammeId,IndicatorId,PSPId,AmountWithin,amountwithout,PercentWithin,PercentWithout)
    SELECT
        @MonitoringPeriodId, x.ProgrammeId, @IndicatorId, X.PSPId, ISNULL(y.PaymentAmount,0), 0,0,0
    FROM X FULL OUTER JOIN Y ON X.ProgrammeId = Y.ProgrammeId AND X.PSPId = Y.PSPId
    ORDER BY  X.ProgrammeId
END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'PAYMENT_TIMELY'
if(@IndicatorId >0)
BEGIN

    ;WITH
        X
        AS
        (
            SELECT T1.Id as ProgrammeId, T2.ID AS PspId
            FROM Programme T1 CROSS JOIN PSP T2
        )
,
        Y
        AS
        (
            SELECT T1.ProgrammeId, T6.PSPId, SUM(T1.PaymentAmount) PaymentAmount, Count(T1.HhId) Households, T0.EntitlementAmount
            FROM Payroll T1
                INNER JOIN Programme T0 ON T1.ProgrammeId = T0.Id
                INNER JOIN PaymentCycle T2 ON T2.Id = T1.PaymentCycleId
                INNER JOIN PaymentCycleDetail T3 ON T2.Id = T3.PaymentCycleId and T3.ProgrammeId = T1.ProgrammeId
                INNER JOIN HouseholdEnrolment T4 ON T1.HhId = T4.HhId
                INNER JOIN BeneficiaryAccount T5 ON T4.Id = T5.HhEnrolmentId
                INNER JOIN PSPBranch T6 ON T5.PSPBranchId = T6.Id
                    AND T3.PayrollApvOn BETWEEN @StartDate AND @EndDate and T2.ToMonthId=@ToMonthPrevCycleId and T2.FromMonthId = @ToMonthId
            GROUP BY T1.ProgrammeId, T6.PSPId, T0.EntitlementAmount
        )

    INSERT INTO PeriodicPaymentIndicator
        (MonitoringPeriodId,ProgrammeId,IndicatorId,PSPId,AmountWithin,amountwithout,PercentWithin,PercentWithout)
    SELECT
        @MonitoringPeriodId, x.ProgrammeId, @IndicatorId, X.PSPId, ISNULL(y.PaymentAmount,0),0,0,

        ISNULL(CASE WHEN Households = 0 THEN 0 ELSE ((ISNULL(y.PaymentAmount,0))*100 /(Households*EntitlementAmount)) END, 0)
    FROM X FULL OUTER JOIN Y ON X.ProgrammeId = Y.ProgrammeId AND X.PSPId = Y.PSPId
    ORDER BY  X.ProgrammeId


END

SELECT @IndicatorId = NULL
SELECT @IndicatorId = Id
FROM Indicator
WHERE  Code  = 'PAYMENT_OUTSIDE'
if(@IndicatorId >0)
BEGIN

    ;WITH
        X
        AS
        (
            SELECT T1.Id as ProgrammeId, T2.ID AS PspId
            FROM Programme T1 CROSS JOIN PSP T2
        )
,
        Y
        AS
        (
            SELECT T1.ProgrammeId, T6.PSPId, SUM(T1.PaymentAmount) PaymentAmount, Count(T1.HhId) Households, T0.EntitlementAmount
            FROM Payroll T1
                INNER JOIN Programme T0 ON T1.ProgrammeId = T0.Id
                INNER JOIN PaymentCycle T2 ON T2.Id = T1.PaymentCycleId
                INNER JOIN PaymentCycleDetail T3 ON T2.Id = T3.PaymentCycleId and T3.ProgrammeId = T1.ProgrammeId
                INNER JOIN HouseholdEnrolment T4 ON T1.HhId = T4.HhId
                INNER JOIN BeneficiaryAccount T5 ON T4.Id = T5.HhEnrolmentId
                INNER JOIN PSPBranch T6 ON T5.PSPBranchId = T6.Id
                    AND T3.PayrollApvOn BETWEEN @StartDate AND @EndDate and T2.ToMonthId=@ToMonthPrevCycleId and T2.FromMonthId = @ToMonthId
            GROUP BY T1.ProgrammeId, T6.PSPId,T0.EntitlementAmount
        )

    INSERT INTO PeriodicPaymentIndicator
        (MonitoringPeriodId,ProgrammeId,IndicatorId,PSPId,AmountWithin,amountwithout,PercentWithin,PercentWithout)
    SELECT @MonitoringPeriodId, x.ProgrammeId, @IndicatorId, X.PSPId,

        CASE WHEN Households = 0 THEN 0 ELSE ISNULL(y.PaymentAmount,0) - ISNULL(Households*EntitlementAmount,0)  END,
        CASE WHEN ISNULL(y.PaymentAmount,0)=0 OR ISNULL(Households*EntitlementAmount,0)=0   THEN 0 ELSE (100-ISNULL(y.PaymentAmount,0)*100/ISNULL(Households*EntitlementAmount,0)) END
		,0,0
    FROM X FULL OUTER JOIN Y ON X.ProgrammeId = Y.ProgrammeId AND X.PSPId = Y.PSPId
    ORDER BY  X.ProgrammeId

END
SELECT @IndicatorId = NULL
UPDATE MonitoringPeriod SET  IsActive = 0 WHERE Id <> @MonitoringPeriodId AND IsActive = 1
UPDATE MonitoringPeriod SET  IsActive = 1 WHERE Id = @MonitoringPeriodId
