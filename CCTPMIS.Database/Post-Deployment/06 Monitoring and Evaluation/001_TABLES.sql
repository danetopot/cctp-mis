﻿IF NOT OBJECT_ID('PeriodicConstituencyPaymentIndicators') IS NULL	
DROP TABLE PeriodicConstituencyPaymentIndicators
GO

IF NOT OBJECT_ID('PeriodicConstituencyBeneIndicators') IS NULL	
DROP TABLE PeriodicConstituencyBeneIndicators
GO


IF NOT OBJECT_ID('MonitoringReportCategory') IS NULL	
DROP TABLE MonitoringReportCategory
GO

IF NOT OBJECT_ID('MonitoringReport') IS NULL	
DROP TABLE MonitoringReport
GO

IF NOT OBJECT_ID('Indicator') IS NULL	
DROP TABLE Indicator
GO

IF NOT OBJECT_ID('MonitoringPeriod') IS NULL	
DROP TABLE MonitoringPeriod
GO

IF NOT OBJECT_ID('MonitoringIndicator') IS NULL	
DROP TABLE MonitoringIndicator
GO

 
CREATE TABLE MonitoringReportCategory
(
   Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(50) NOT NULL,
    Code varchar(20) NOT NULL,
    [Description] varchar(500) NOT NULL,
    CreatedOn DATETIME  DEFAULT GETDATE(),
    CreatedBy INT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    ApvBy INT NULL,
    ApvOn datetime NULL,
    CONSTRAINT PK_MonitoringReportCategory PRIMARY KEY (Id),
    CONSTRAINT FK_MonitoringReportCategory_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReportCategory_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReportCategory_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT IX_MonitoringReportCategory_Name UNIQUE NONCLUSTERED ([Name])
)
GO

CREATE TABLE MonitoringReport
(
     Id int NOT NULL IDENTITY(1,1),
    MonitoringReportCategoryId INT NULL,
    [FileName] VARCHAR(300) NOT NULL,
    FilePath VARCHAR(300) NOT NULL,
    [Description] VARCHAR(300) NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    Icon VARCHAR(300) NOT NULL default 'pdf.png',
    CreatedOn DATETIME NOT NULL  DEFAULT GETDATE(),
    CreatedBy INT NULL,
    StatusId INT NOT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    VerifiedOn datetime NULL,
    VerifiedBy INT NULL,
    ApvBy INT NULL,
    ApvOn datetime NULL,
    CONSTRAINT PK_MonitoringReport PRIMARY KEY (Id),
    CONSTRAINT FK_MonitoringReport_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReport_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReport_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
)
GO

CREATE TABLE Indicator
(
       Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(50) NOT NULL,
    Code varchar(20) NOT NULL,
    [Description] varchar(500) NOT NULL,
    CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CreatedBy INT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    CONSTRAINT PK_Indicator PRIMARY KEY (Id),
    CONSTRAINT FK_Indicator_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_Indicator_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
)
GO

CREATE TABLE MonitoringPeriod
(
       Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(50) NOT NULL,
    StartDate DATETIME NOT NULL DEFAULT GETDATE(),
    EndDate DATETIME NOT NULL DEFAULT GETDATE(),
    StartMonthId INT NOT NULL,
    EndMonthId INT NOT NULL,
    [Year] INT NOT NULL,
    IsActive BIT NOT NULL DEFAULT 0,
	CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CONSTRAINT PK_MonitoringPeriod PRIMARY KEY (Id),
 )


CREATE TABLE PeriodicConstituencyBeneIndicators
(
    MonitoringPeriodId INT NOT NULL,
    ConstituencyId INT NOT NULL,
    ProgrammeId INT NOT NULL,
    BeneOnPayroll INT NOT NULL,
    BeneExited INT NOT NULL,
    BeneFirstTimeOnPayroll INT NOT NULL,
    BeneEverPaid INT NOT NULL,
    BeneNOTOnPayroll INT NOT NULL,
    ComplaintOpenWithinSLA INT NOT NULL,
    ComplaintOpenOutsideSLA INT NOT NULL,
    ComplaintResolvedWithinSLA INT NOT NULL,
    ComplaintResolvedOutsideSLA INT NOT NULL,
    ComplaintRejected INT NOT NULL,
    UpdateOpenWithinSLA INT NOT NULL,
    UpdateOpenOutsideSLA INT NOT NULL,
    UpdateClosedWithinSLA INT NOT NULL,
    UpdateClosedOutsideSLA INT NOT NULL,
    UpdateRejectedWithinSLA INT NOT NULL,
    UpdateRejectedOutsideSLA INT NOT NULL,
)


CREATE TABLE PeriodicConstituencyPaymentIndicators
(
    MonitoringPeriodId INT NOT NULL,
    ConstituencyId INT NOT NULL,
    ProgrammeId INT NOT NULL,
    AmountRequested DECIMAL NOT NULL,
    AmountPaidInOutsideTime DECIMAL NOT NULL,
    AmountPaidInTime DECIMAL NOT NULL,
    TotalAmountEverRequested DECIMAL NOT NULL
)
GO
