﻿-- look into the   AddEditSystemCode and AddEditSystemCodeDetail varchar parameters


ALTER PROC [dbo].[AddEditSystemCodeDetail]
	@Id int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(30)=NULL
   ,@DetailCode varchar(30)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=Id
		FROM SystemCode
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SystemCodeId AND Code=@DetailCode AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.Code=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetail T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('SystemCodeDetail')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		--EXEC GetSystemCodeDetails	
	END
END
GO
ALTER PROC [dbo].[AddEditSystemCode]
	@Id int=NULL
   ,@Code varchar(30)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCode WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCode T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCode(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END
GO


 
DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(30)
DECLARE @DetailCode varchar(30)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT
DECLARE @Getter INT

 
SET @Code='Monitoring Report Status'
SET @Description='Monitoring Report Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Monitoring Report Status'
IF(ISNULL(@Id,0)>0)
	BEGIN

	SET @DetailCode='Creation Approval'
	SET @Description='Creation Approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Verified'
	SET @Description='Verified'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Approved'
	SET @Description='Approved'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Rejected'
	SET @Description='Rejected'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	END

	GO
  
 INSERT INTO MonitoringReportCategory ([Name],CODE,[Description],CreatedOn,CreatedBy)
SELECT T1.[Name],T1.CODE,T1.[Description],T1.CreatedOn,T1.CreatedBy
FROM(
SELECT  'Internal Monitoring Report' AS [Name] , 'IMR' Code , 'Internal Monitoring Report' Description ,GETDATE() CreatedOn ,1 AS CreatedBy --FROM  MonitoringReportCategory
UNION
SELECT 'External Monitoring Report' , 'EMR', 'External Monitoring Report', GETDATE(), 1
UNION
SELECT 'Spot-check Report' , 'SCR', 'Spot-check Report', GETDATE(), 1
UNION
SELECT 'Impact Evaluation Report' , 'IER', 'Impact Evaluation Report', GETDATE(), 1
UNION
SELECT 'Beneficiary Satisfaction Survey Report' , 'PIBBSSR', 'Programme Implementation Beneficiary Satisfaction Survey Report', GETDATE(), 1
) T1 LEFT JOIN MonitoringReportCategory T2 ON T1.Code = T2.Code where T2.ID IS NULL