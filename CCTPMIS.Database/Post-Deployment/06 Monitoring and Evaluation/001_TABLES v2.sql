
IF NOT OBJECT_ID('PeriodicConstituencyPaymentIndicators') IS NULL	
DROP TABLE PeriodicConstituencyPaymentIndicators
GO

IF NOT OBJECT_ID('PeriodicConstituencyBeneIndicators') IS NULL	
DROP TABLE PeriodicConstituencyBeneIndicators
GO

IF NOT OBJECT_ID('PeriodicPaymentIndicator') IS NULL	
DROP TABLE PeriodicPaymentIndicator
GO

IF NOT OBJECT_ID('PeriodicConstituencyBeneIndicator') IS NULL	
DROP TABLE PeriodicConstituencyBeneIndicator
GO

IF NOT OBJECT_ID('MonitoringReportCategory') IS NULL	
DROP TABLE MonitoringReportCategory
GO


IF NOT OBJECT_ID('Indicator') IS NULL	
DROP TABLE Indicator
GO

IF NOT OBJECT_ID('MonitoringPeriod') IS NULL	
DROP TABLE MonitoringPeriod
GO

IF NOT OBJECT_ID('MonitoringIndicator') IS NULL	
DROP TABLE MonitoringIndicator
GO

IF NOT OBJECT_ID('HouseholdExit') IS NULL	
DROP TABLE HouseholdExit
GO

IF NOT OBJECT_ID('MonitoringReport') IS NULL	
DROP TABLE MonitoringReport
GO

CREATE TABLE MonitoringReportCategory
(
    Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(50) NOT NULL,
    Code varchar(20) NOT NULL,
    [Description] varchar(500) NOT NULL,
    CreatedOn DATETIME DEFAULT GETDATE(),
    CreatedBy INT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    ApvBy INT NULL,
    ApvOn datetime NULL,
    CONSTRAINT PK_MonitoringReportCategory PRIMARY KEY (Id),
    CONSTRAINT FK_MonitoringReportCategory_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReportCategory_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReportCategory_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT IX_MonitoringReportCategory_Name UNIQUE NONCLUSTERED ([Name])
)
GO

CREATE TABLE MonitoringReport
(
    Id int NOT NULL IDENTITY(1,1),
    MonitoringReportCategoryId INT NULL,
    [FileName] VARCHAR(300) NOT NULL,
    FilePath VARCHAR(300) NOT NULL,
    [Description] VARCHAR(300) NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    Icon VARCHAR(300) NOT NULL default 'pdf.png',
    CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CreatedBy INT NULL,
    StatusId INT NOT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    VerifiedOn datetime NULL,
    VerifiedBy INT NULL,
    ApvBy INT NULL,
    ApvOn datetime NULL,
    CONSTRAINT PK_MonitoringReport PRIMARY KEY (Id),
    CONSTRAINT FK_MonitoringReport_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReport_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_MonitoringReport_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
)
GO

CREATE TABLE Indicator
(
    Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(150) NOT NULL,
    Code varchar(30) NOT NULL,
    Process varchar(20) NOT NULL,
    [Description] varchar(500) NOT NULL,
    CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CreatedBy INT NULL,
    ModifiedOn datetime NULL,
    ModifiedBy INT NULL,
    CONSTRAINT PK_Indicator PRIMARY KEY (Id),
    CONSTRAINT FK_Indicator_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT FK_Indicator_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION,
)
GO

CREATE TABLE MonitoringPeriod
(
    Id int NOT NULL IDENTITY(1,1),
    [Name] varchar(50) NOT NULL,
    StartDate DATETIME NOT NULL DEFAULT GETDATE(),
    EndDate DATETIME NOT NULL DEFAULT GETDATE(),
    StartMonthId INT NOT NULL,
    EndMonthId INT NOT NULL,
    [Year] INT NOT NULL,
    IsActive BIT NOT NULL DEFAULT 0,
    CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CONSTRAINT PK_MonitoringPeriod PRIMARY KEY (Id),
)


CREATE TABLE PeriodicConstituencyBeneIndicator
(
    MonitoringPeriodId INT NOT NULL,
    ConstituencyId INT NOT NULL,
    ProgrammeId TINYINT NOT NULL,
    IndicatorId int not NULL,
    MALE INT NOT NULL,
    Female int not NULL,
    Total int not null
)


CREATE TABLE PeriodicPaymentIndicator
(
    MonitoringPeriodId INT NOT NULL,
    PspId TINYINT NOT NULL,
    ProgrammeId TINYINT NOT NULL,
    IndicatorId int not NULL,
    AmountWithin decimal(18,2) NOT NULL DEFAULT 0,
    AmountWithout decimal(18,2) NOT NULL DEFAULT 0,
    PercentWithin float not NULL DEFAULT 0,
    PercentWithout float not NULL DEFAULT 0,
)
GO


CREATE TABLE HouseholdExit
(
    HHId INT NOT NULL,
    ReasonId INT NOT NULL,
    TypeId INT NOT NULL,
    ExitedOn DATETIME NOT NULL DEFAULT GETDATE(),
    CreatedBy INT NULL,
)
GO
