
declare @startdate date = '2018-03-01'
declare @enddate   date = GETDATE()


DECLARE @UserId INT = 1, @CreatedOn DATETIME = getdate()

INSERT INTO Indicator
  (Code, [Name], Process, [Description],CreatedBy, CreatedOn)

SELECT T1.Code, T1.[Name], T1.Process, T1.[Description], T1.UserId, T1.CreatedOn
FROM (
   SELECT 'BENE_ON_PAYROLL' Code , 'Number of Beneficiaries on payroll' AS [Name], 'Beneficiaries' AS Process, 'Total Number of Beneficiaries who are on payroll during the current reporting period.' As [Description], @UserId as UserId, @CreatedOn as CreatedOn
  UNION
    SELECT 'BENE_EXITED', 'Number of Beneficiaries exited', 'Beneficiaries', 'Total Number of Beneficiaries flagged for exit using case management module/ otherwise whose date of exit is between start and end date of the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'BENE_ON_PAYROLL_FIRST', 'Number of Beneficiaries on payroll for the first time', 'Beneficiaries', 'Total Number of Beneficiaries who are on payroll for first time during the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'BENE_EVER_PAID', 'Number of Beneficiaries ever paid', 'Beneficiaries', 'Total number of Beneficiaries who have ever received a payment. ', @UserId, @CreatedOn
  UNION
    SELECT 'BENE_INELIGIBLE', 'Number of eligible Beneficiaries not on payroll', 'Beneficiaries', 'Total Number of Beneficiaries on pre-payroll not actioned during the reporting period', @UserId, @CreatedOn
   UNION
    SELECT 'BENE_ENROLLED', 'Number of Beneficiaries Enrolled by the Programme', 'Beneficiaries', 'Total Number of Beneficiaries Enrolled by the Programme', @UserId, @CreatedOn
 UNION
    SELECT 'BENE_ACCOUNTOPENED', 'Number of Beneficiaries whose accounts were opened ', 'Beneficiaries', 'Total Number of Beneficiaries whose accounte were opened ', @UserId, @CreatedOn
 UNION
    SELECT 'BENE_CARDED', 'Number of Beneficiaries who were Carded ', 'Beneficiaries', 'Total Number of Beneficiaries who were Carded', @UserId, @CreatedOn
 UNION
    SELECT 'BENE_REGISTERED', 'Number of Beneficiaries Registered', 'Beneficiaries', 'Total Number of Beneficiaries Registered', @UserId, @CreatedOn
 
  UNION
    SELECT 'FUNDS_REQUESTED', 'Amount requested in KES', 'Payments', 'Amounts requested within the Reporting Period', @UserId, @CreatedOn
  UNION
    SELECT 'TOTAL_FUNDS_REQUESTED', 'Total Amount requested in KES', 'Payments', 'Total amount Requested', @UserId, @CreatedOn
  UNION
    SELECT 'PAYMENT_TIMELY', '% of payments disbursed to payment service provider on time', 'Payments', 'Amount of Kenya Shillings requested for PSP within the Reporting Period', @UserId, @CreatedOn
  UNION
    SELECT 'PAYMENT_OUTSIDE', '% of payments disbursed to payment service provider NOT on time', 'Payments', 'Amount of Kenya Shillings requested for PSP within the Reporting Period not following OM Cycle Periods', @UserId, @CreatedOn
  UNION
    SELECT 'PAYMENT_DISBURSEMENT', 'Disbursed amounts within the reporting period', 'Payments', 'Amount of Kenya Shillings disbursed to Beneficiaries during the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'TOTAL_PAYMENT_DISBURSEMENT', 'Total Disbursed amounts', 'Payments', 'Amount of Kenya Shillings disbursed to Beneficiaries up to the end of the reporting period', @UserId, @CreatedOn
 
  UNION
    SELECT 'COMPLAINTS_OPEN_WITHIN', 'Number of open complaints within SLA timeframe during the reporting period', 'Complaints', 'Number of complaints that have not been resolved and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'COMPLAINTS_OPEN_OUTSIDE', 'Number of open complaints outside the SLA timeframe during the reporting period', 'Complaints', 'Number of complaints that have been resolved and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'COMPLAINTS_CLOSED_WITHIN', 'Number of RESOLVED complaints within SLA timeframe during the reporting period', 'Complaints', 'Number of complaints that have been resolved and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'COMPLAINTS_CLOSED_OUTSIDE', 'Number of RESOLVED complaints outside SLA timeframe during the reporting period', 'Complaints', 'Number of complaints that have been resolved and have exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'UPDATES_OPEN_WITHIN', 'Number of open updates within SLA timeframe during the reporting period', 'Updates', 'Number of updates that have not been CLOSED and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'UPDATES_OPEN_OUTSIDE', 'Number of open updates outside the SLA timeframe during the reporting period', 'Updates', 'Number of updates that have been CLOSED and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'UPDATES_CLOSED_WITHIN', 'Number of CLOSED updates within SLA timeframe during the reporting period', 'Updates', 'Number of updates that have been CLOSED and have not exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
  UNION
    SELECT 'UPDATES_CLOSED_OUTSIDE', 'Number of CLOSED updates outside SLA timeframe during the reporting period', 'Updates', 'Number of updates that have been CLOSED and have exceeded SLA timeframes within the reporting period', @UserId, @CreatedOn
            ) T1
  LEFT JOIN Indicator T2 ON T1.Code = T2.Code
WHERE T2.Code IS NULL
ORDER BY Code ASC





--- POPULATE THE PREVIOUS MONITORING PERIODS SINCE '2018-03-01'
;with
  cte
  as
  (
    select top (datediff(month,@startdate,@enddate)+0)
      Month = convert(date,dateadd(month,row_number() over (order by (select 1))-1,@startdate))
    from master..spt_values
    order by month
  )
INSERT INTO MonitoringPeriod
  ([Name],StartDate,EndDate,StartMonthId, EndMonthId,[Year],IsActive,CreatedOn)

select
  [Name] =  CONCAT( DATENAME(mm, CTE.month),'-', DATENAME(mm, dateadd(MONTH,1,CTE.month)), ' ',DATENAME(YYYY, dateadd(MONTH,1,CTE.month)))
  , PeriodStart = dateadd(MONTH,0,CTE.month)
  , PeriodEnd = eomonth(dateadd(MONTH,1,CTE.month))
  , StartMonthId = T2.Id
  , EndMonthId =  T4.Id
  , YEAR = DATENAME(YYYY, dateadd(MONTH,1,CTE.month))
  , IsActive =0
  , CreatedOn = GETDATE()
from cte
  INNER JOIN SystemCodeDetail T2 ON  DATENAME(mm, cte.month) = T2.[Description]
  INNER JOIN SystemCode T3 ON T2.SystemCodeId = T3.Id AND T3.Code='Calendar Months'
  INNER JOIN SystemCodeDetail T4 ON  DATENAME(mm, dateadd(MONTH,1,cte.month) )= T4.[Description]
    AND DATEPART(m,month)%2=1
  LEFT JOIN MonitoringPeriod T5 ON T5.StartMonthId = T2.Id AND T5.EndMonthId = T4.Id AND T5.Year =  DATENAME(YYYY, dateadd(MONTH,1,CTE.month))
WHERE T5.Id IS NULL
ORDER BY PeriodStart ASC

 -----------

 



