﻿

SELECT @Id = NULL
------------------------ 'Registration Status' ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Registration Status'
IF(ISNULL(@Id,0)>0)
BEGIN
	SET @DetailCode='REG'
	SET @Description='Registration '
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGSYNC'
	SET @Description='Household Synced'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='REGREJ'
	SET @Description='Household Rejected'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	 SET @DetailCode='REGACC'
	SET @Description='Household Accepted'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	
	SET @DetailCode='REGAPV'
	SET @Description='Household Approved'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

   
	SET @DetailCode='REGVALPASS'
	SET @Description='Household Passed Validation'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	SET @DetailCode='REGVALFAILED'
	SET @Description='Household Failed Validation'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='REGCONFIRM'
	SET @Description='Household Confirmed'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='REGEXIT'
	SET @Description='Household Exited'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='REGCORRECT'
	SET @Description='Household Corrected'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
 

 
SET @Code='REG_CATEGORIES'
SET @Description='Registration Exercise Categories'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SET @Code='REG_STATUS'
SET @Description='Registration Exercise Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code =  'REG_STATUS'
IF(ISNULL(@Id,0)>0)
BEGIN
   SET @DetailCode='CREATIONAPV'
	SET @Description='Creation Approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ACTIVE'
	SET @Description='Active'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUBMISSIONAPV'
	SET @Description='Submitted Report Approval'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	
	SET @DetailCode='IPRSVAL'
	SET @Description='IPRS VALIDATION'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	 SET @DetailCode='PMTASS'
	SET @Description='PMT and Vulnerability Assessment'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 

	SET @DetailCode='COMMVAL'
	SET @Description='Community Validation'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='COMMVALAPV'
	SET @Description='Community Validation Approval'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	 SET @DetailCode='POSTREG'
	SET @Description='Post Targeting Process'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
	SET @DetailCode='CLOSED'
	SET @Description='Household Listing Closed'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
END
 
SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code = 'REG_CATEGORIES'
IF(ISNULL(@Id,0)>0)
BEGIN
   SET @DetailCode='LISTING'
	SET @Description='House Hold Listing'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGISTRATION'
	SET @Description='Household Registration'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECERTIFICATION'
	SET @Description='Household Re-certification'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;	 
END



SELECT @Id=Id FROM SystemCode WHERE Code='File Type'
SET @DetailCode='REG_VALIDATION'
SET @Description='IPRS Export File'
SET @OrderNo=1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

SELECT @Id=Id FROM SystemCode WHERE Code='File Type'
SET @DetailCode='REG_EXCEPTIONS'
SET @Description='IPRS Export File'
SET @OrderNo=1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

SET @DetailCode='IPRS_EXPORT'
SET @Description='IPRS Export File'
SET @OrderNo=1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



SET @Code='COM_VAL_STATUS'
SET @Description='Registration Household Community Validation'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code = 'COM_VAL_STATUS'
IF(ISNULL(@Id,0)>0)
BEGIN
   SET @DetailCode='EXITED'
	SET @Description='Exited'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	
	SET @DetailCode='EDITING'
	SET @Description='Editing'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='AMENDED'
	SET @Description='Amended'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CONFIRMED'
	SET @Description='Confirmed'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;	 
END




SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code = 'COM_VAL_STATUS'
IF(ISNULL(@Id,0)>0)
BEGIN
   SET @DetailCode='Pending'
	SET @Description='Pending'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	 
	SET @DetailCode='Ongoing'
	SET @Description='Ongoing'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;	 
  
  SET @DetailCode='Complete'
	SET @Description='Complete'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;	 
END


