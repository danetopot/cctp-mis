﻿
DECLARE @Id INT 
DECLARE @ModuleName VARCHAR(20)
DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW VARCHAR(20)
DECLARE @SysRightCode_ENTRY VARCHAR(20)
DECLARE @SysRightCode_MODIFIER VARCHAR(20)
DECLARE @SysRightCode_DELETION VARCHAR(20)
DECLARE @SysRightCode_APPROVAL VARCHAR(20)
DECLARE @SysRightCode_FINALIZE VARCHAR(20)
DECLARE @SysRightCode_VERIFY VARCHAR(20)
DECLARE @SysRightCode_EXPORT VARCHAR(20)
DECLARE @SysRightCode_DOWNLOAD VARCHAR(20)
DECLARE @SysRightCode_UPLOAD VARCHAR(20)

DECLARE @SysRightId_VIEW INT
DECLARE @SysRightId_ENTRY INT
DECLARE @SysRightId_MODIFIER INT
DECLARE @SysRightId_DELETION INT
DECLARE @SysRightId_APPROVAL INT
DECLARE @SysRightId_FINALIZE INT
DECLARE @SysRightId_VERIFY INT
DECLARE @SysRightId_EXPORT INT
DECLARE @SysRightId_DOWNLOAD INT
DECLARE @SysRightId_UPLOAD INT

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD

SET @ModuleName = 'Administration'
SELECT @Id = Id FROM Module WHERE [Name] = @ModuleName
IF(ISNULL(@Id,0)>0)
BEGIN
 INSERT INTO Module(Name, [Description],ParentModuleId)
	SELECT T1.Name,T1.[Description],T1.ParentModuleId
	FROM (SELECT 2 AS Id,  'Enumerators' AS Name,'Registration, Targeting and Retargeting Enumerators' AS [Description], @Id AS ParentModuleId
		  UNION
		  SELECT 1 AS Id,  'Programme-Officers','Programme Officers - Users Assigned County or SubCounties',  @Id AS ParentModuleId
	)T1		LEFT JOIN Module T2 ON T1.Name = T2.Name WHERE  T2.Name IS  NULL
 END
 SELECT @Id =NULL
 SELECT @ModuleName  =NULL 
  


SET @ModuleCode='Enumerators'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,'DEACTIVATION'

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL



  
SET @ModuleCode='Programme-Officers'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION, 'DEACTIVATION'
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL






SET @ModuleCode='Programmes'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,'DEACTIVATION'

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL









SET @ModuleCode='System'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,'CHANGE PASSWORD' AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'CONFIRM EMAIL'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'RESET PASSWORD'
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,'LOGIN'
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,'LOG OFF'
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD, 'CHANGE AVATAR'
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL, 'EDIT PROFILE'

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL
