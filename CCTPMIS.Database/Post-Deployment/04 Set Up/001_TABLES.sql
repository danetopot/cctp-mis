﻿
IF NOT OBJECT_ID('EnumeratorDevice') IS NULL 
DROP TABLE EnumeratorDevice 
GO

IF NOT OBJECT_ID('EnumeratorLocation') IS NULL	
DROP TABLE EnumeratorLocation 
GO

IF NOT OBJECT_ID('Enumerator') IS NULL	
DROP TABLE Enumerator
GO

IF NOT OBJECT_ID('ProgrammeOfficer') IS NULL	
DROP TABLE ProgrammeOfficer
GO

IF NOT OBJECT_ID('UserSSO') IS NULL	
DROP TABLE UserSSO
GO


CREATE TABLE UserSSO(
	Id INT NOT NULL IDENTITY(1,1)
   ,SessionId varchar(30) NOT NULL
   ,UserId int NOT NULL
   ,[DATE] DATE NOT NULL DEFAULT GETDATE()
   ,CONSTRAINT PK_Login PRIMARY KEY (Id)
)
GO

CREATE TABLE ProgrammeOfficer(
	Id int NOT NULL IDENTITY(1,1)
   ,UserId int NOT NULL
   ,CountyId int NULL
   ,StatusId int NULL
   ,ConstituencyId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,DeactivatedBy int NULL
   ,DeactivatedOn datetime NULL
   ,DeactivatedApvBy int NULL
   ,DeactivatedApvOn datetime NULL
   ,CONSTRAINT PK_ProgrammeOfficer PRIMARY KEY (Id)
   ,CONSTRAINT FK_ProgrammeOfficer_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_County_CountyId FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_DeactivatedBy FOREIGN KEY (DeactivatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ProgrammeOfficer_User_DeactivatedApvBy FOREIGN KEY (DeactivatedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE Enumerator (
	Id int NOT NULL IDENTITY(1,1)
   ,PasswordHash nvarchar(256) NULL
   ,Email nvarchar(100) NOT NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL  
   ,NationalIdNo varchar(50) NOT NULL  
   ,MobileNo nvarchar(20) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,DeactivateDate datetime NULL
   ,LoginDate datetime NULL
   ,ActivityDate datetime NULL
   ,AccessFailedCount int NOT NULL DEFAULT(0)
   ,LockoutEnabled BIT NOT NULL
   ,LockoutEndDateUtc  DATETIME NULL
   ,PasswordChangeDate DATETIME NULL
   ,SecurityStamp VARCHAR(36)   NULL
   ,IsLocked bit NOT NULL DEFAULT(0)   
   ,CreatedBy int NOT NULL   
   ,ConstituencyId int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeactivatedBy int NULL
   ,DeactivatedOn datetime NULL
   ,CONSTRAINT PK_Enumerator PRIMARY KEY (Id)
   ,CONSTRAINT FK_Enumerator_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Enumerator_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Enumerator_User_DeactivatedBy FOREIGN KEY (DeactivatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Enumerator_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Enumerator_Email UNIQUE NONCLUSTERED (Email)
   ,CONSTRAINT IX_Enumerator_NationalIdNo UNIQUE NONCLUSTERED (NationalIdNo)
)
GO

CREATE TABLE EnumeratorLocation (
	  Id INT NOT NULL IDENTITY(1,1) 
	 ,EnumeratorId int NOT NULL
	 ,LocationId INT NOT NULL
	 ,IsActive BIT NOT NULL DEFAULT 0
	 ,CONSTRAINT PK_EnumeratorLocation PRIMARY KEY (Id)
	 ,CONSTRAINT FK_EnumeratorLocation_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE CASCADE ON DELETE NO ACTION
	 ,CONSTRAINT FK_EnumeratorLocation_Location_LocationId FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	 ,CONSTRAINT IX_EnumeratorLocation_CountyId_DistrictId UNIQUE NONCLUSTERED(LocationId,EnumeratorId)
)
GO

CREATE TABLE EnumeratorDevice(
	 Id					INT  NOT NULL IDENTITY(1,1) 
	,DeviceId		    VARCHAR(36) NOT NULL
	,DeviceModel		VARCHAR(15) NOT NULL
	,DeviceManufacturer VARCHAR(15) NOT NULL
	,DeviceName			VARCHAR(15) NOT NULL
	,[Version]			VARCHAR(15)  NOT NULL
	,VersionNumber		VARCHAR(15)  NOT NULL
	,[Platform]			VARCHAR(15) NOT NULL
	,Idiom				VARCHAR(10) NOT NULL
	,IsDevice			BIT NOT NULL
	,EnumeratorId		INT  NOT NULL
	,CONSTRAINT PK_EnumeratorDevice PRIMARY KEY (Id)
	,CONSTRAINT FK_EnumeratorDevice_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	)
GO






ALTER TABLE UserGroup ADD UserGroupTypeId  INT NOT NULL DEFAULT 0
	GO
	DECLARE @Id int
	SELECT @Id = Id from SystemCodeDetail where Code='Internal'
UPDATE  UserGroup set  UserGroupTypeId = @id where [Name] not like '%psp%' and [Name] not like '%Auditor%'

 SELECT @Id = Id from SystemCodeDetail where Code='External'
 UPDATE  UserGroup set  UserGroupTypeId = @id where [Name] like   '%psp%' or [Name]  like    '%Auditor%'

 ALTER TABLE UserGroup ALTER COLUMN UserGroupTypeId  INT NOT NULL --DEFAULT 0
 GO
 ALTER TABLE UserGroup ADD CONSTRAINT FK_UserGroup_SystemCodeDetail_UserGroupTypeId FOREIGN KEY (UserGroupTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
 GO	