﻿ALTER TABLE BeneAccountMonthlyActivity Add StartDate datetime default GETDATE()
ALTER TABLE BeneAccountMonthlyActivity Add EndDate datetime default GETDATE()

ALTER TABLE BeneAccountMonthlyActivity ADD ApvBy INT NULL
ALTER TABLE BeneAccountMonthlyActivity ADD ApvOn datetime NULL
-- ammend the Stored peoc that saves the account activity to have the startdate and Enddate without sending them as parameters

Update BeneAccountMonthlyActivity set StartDate = 
  DATEADD(MONTH, DATEDIFF(MONTH, 0,CONVERT (DATETIME ,(CONCAT( T1.yEAR,'-' ,T2.Code,'-01'))) ) , 0)  ,
  EndDate =
 DATEADD(SECOND, -1, DATEADD(MONTH, 1,  DATEADD(MONTH, DATEDIFF(MONTH, 0, CONVERT (DATETIME ,(CONCAT( T1.yEAR,'-' ,T2.Code,'-01')))) , 0) ) )
   from  BeneAccountMonthlyActivity T1 
INNER JOIN SystemCodeDetail T2 ON T1.MonthId = T2.Id

IF NOT OBJECT_ID('temp_ReportID') IS NULL	
DROP TABLE temp_ReportID
GO

   CREATE TABLE  temp_ReportID (
	 ReportId INT NOT NULL
	 ,ReportType VARCHAR(36) NOT NULL
	 ,ReportGuId VARCHAR(36)
	 ,CONSTRAINT PK_temp_ReportID PRIMARY KEY (ReportId,ReportType,ReportGuId)
	)
	GO







 IF NOT OBJECT_ID('GetDateRange') IS NULL	
DROP PROC GetDateRange
GO
CREATE PROC GetDateRange 
@DateRange VARCHAR(20)
AS
BEGIN
DECLARE @StartDate datetime,
	@EndDate datetime

	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')

	SELECT CONVERT(VARCHAR,@StartDate,23) StartDate, CONVERT(VARCHAR,@EndDate,23)  EndDate

	END

	GO

IF NOT OBJECT_ID('GetGeoLocations') IS NULL	
DROP PROC GetGeoLocations
GO
CREATE PROC GetGeoLocations (@type varchar(10) )
 
AS
BEGIN
IF(@type = 'SUBLOCATION')
BEGIN
SELECT T8.Id AS GeoMasterId,T1.ID, T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id


END

IF(@type = 'LOCATION')
BEGIN
SELECT DISTINCT T8.Id AS GeoMasterId, t2.Id,  T2.Id AS LocationId, T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County 
											  FROM  Location T2  
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id

END
 
 IF(@type = 'SUBCOUNTY')
BEGIN
SELECT DISTINCT T7.ID, T8.Id AS GeoMasterId,  T6.Name AS County, T7.Name AS Constituency, T7.Id ConstituencyId
											FROM  Constituency T7  
																   INNER JOIN County T6 ON T7.CountyId=T6.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
END


 IF(@type = 'COUNTY')
BEGIN
SELECT  DISTINCT T6.* FROM County   T6
 INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
END

 IF(@type = 'PSP')
BEGIN
SELECT  DISTINCT CONVERT(int, T1.Id) Id, T1.Code, t1.Name FROM PSP t1
END

END



	/********************************************************/

/****************CASE MANAGEMENT     ********************/
/********************************************************/
IF NOT OBJECT_ID('RptGetEFC') IS NULL	
DROP PROC RptGetEFC
GO
CREATE PROC RptGetEFC
	@DateRange VARCHAR(20),
	@StartDate datetime,
	@EndDate datetime,
	@SexXml xml,
	@Typexml xml,
	@Statusxml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
	@Page int =1,
	@PageSize int=10,
	@Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime	 

AS
BEGIN
	IF(@Page=0) set @Page =1
	DECLARE  @offset int = (@Page-1)*@PageSize
	DECLARE  @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')

	IF(@Type='SUMMARY')
	BEGIN
		SELECT @NoOfRows = count(DISTINCT CONCAT(T1.ConstituencyId ,T2.Code ) )
		FROM EFC T1
			INNER JOIN CaseCategory T2 ON T1.EFCTypeId = T2.Id
			INNER JOIN SystemCodeDetail T3 ON T1.ReportedByTypeId = T3.Id
			INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id
			INNER JOIN SystemCodeDetail T5 ON T1.SourceId = T5.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN County T8 ON T7.CountyId=T8.Id
			LEFT JOIN SystemCodeDetail T6 ON T1.ReportedBySexId = T6.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @TypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T1.EFCTypeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @StatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.StatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T7.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T1.ConstituencyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T1.ReportedBySexId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT COUNT(T1.Id) Total , COUNT(CASE WHEN T4.Code = 'Open' THEN 1 END ) AS [Open], COUNT(CASE WHEN T4.Code = 'closed' THEN 1 END ) AS Closed, COUNT(T1.Id) AS Total, T8.Name County , T7.Name Constituency, T2.Description EFCType, T4.Description EFCStatus, @NoOfRows NoOfRows 
		, COUNT(CASE WHEN  (DATEADD(DAY,T2.Timeline, T1.ReportedDate)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'MeetSLA' 
		, COUNT(CASE WHEN  (DATEADD(DAY,T2.Timeline, T1.ReportedDate)) < (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'FailSLA'
		FROM EFC T1
			INNER JOIN CaseCategory T2 ON T1.EFCTypeId = T2.Id
			INNER JOIN SystemCodeDetail T3 ON T1.ReportedByTypeId = T3.Id
			INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id
			INNER JOIN SystemCodeDetail T5 ON T1.SourceId = T5.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN County T8 ON T7.CountyId=T8.Id
			LEFT JOIN SystemCodeDetail T6 ON T1.ReportedBySexId = T6.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @TypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T1.EFCTypeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @StatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.StatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T7.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T1.ConstituencyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T1.ReportedBySexId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		GROUP BY  T7.Name, T8.Name, T2.Description , T4.Description
		ORDER by   T8.Name, T7.Name,T2.Description , T4.Description
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END
	ELSE  IF(@Type='DETAILS')
	BEGIN
		SELECT @NoOfRows = COUNT(T1.Id)
		FROM EFC T1
			INNER JOIN CaseCategory T2 ON T1.EFCTypeId = T2.Id
			INNER JOIN SystemCodeDetail T3 ON T1.ReportedByTypeId = T3.Id
			INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id
			INNER JOIN SystemCodeDetail T5 ON T1.SourceId = T5.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN County T8 ON T7.CountyId=T8.Id
			LEFT JOIN SystemCodeDetail T6 ON T1.ReportedBySexId = T6.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @TypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T1.EFCTypeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @StatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.StatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T7.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T1.ConstituencyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T1.ReportedBySexId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T1.*, t1.CreatedOn ReceivedOn, CASE WHEN  (DATEADD(DAY,T2.Timeline, T1.ReportedDate)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END) THEN 'YES' ELSE 'NO' END  AS SLA, T2.Name EFCType, T3.Description ReportedByType, T4.Description Status, T5.Description Source, T7.Name Constituency, T8.Name County, T6.Description ReportedBySex, @NoOfRows NoOfRows
		FROM EFC T1
			INNER JOIN CaseCategory T2 ON T1.EFCTypeId = T2.Id
			INNER JOIN SystemCodeDetail T3 ON T1.ReportedByTypeId = T3.Id
			INNER JOIN SystemCodeDetail T4 ON T1.StatusId = T4.Id
			INNER JOIN SystemCodeDetail T5 ON T1.SourceId = T5.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN County T8 ON T7.CountyId=T8.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @TypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T1.EFCTypeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @StatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.StatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T7.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T1.ConstituencyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T1.ReportedBySexId
			LEFT JOIN SystemCodeDetail T6 ON T1.ReportedBySexId = T6.Id
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		ORDER BY T1.CreatedOn
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

	END
END
GO

IF NOT OBJECT_ID('RptGetChange') IS NULL	
DROP PROC RptGetChange
GO
CREATE PROC RptGetChange
	@DateRange VARCHAR(20),
	@StartDate datetime,
	@EndDate datetime,
	@ProgrammeXml xml,
	@SexXml xml,
	@ChangeTypexml xml,
	@ChangeStatusxml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
	@Page int =1,
	@PageSize int=10,
	@Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime
AS
BEGIN
	IF(@Page=0) set @Page =1
	DECLARE  @offset int = (@Page-1)*@PageSize
	DECLARE  @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')

	IF(@Type='SUMMARY')
	 BEGIN
		SELECT @NoOfRows =   count(DISTINCT CONCAT(T13.Location,T4.Code ) )
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
					) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T13.Constituency, T13.County, T13.Location, T4.Code Programme, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS Male, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS Female, COUNT(T1.Id) AS Total, @NoOfRows NoOfRows , COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'MeetSLA' , COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) < (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'FailSLA'
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id INNER JOIN Division T3 ON T2.DivisionId=T3.Id INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id INNER JOIN District T5 ON T4.DistrictId=T5.Id INNER JOIN County T6 ON T4.CountyId=T6.Id INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN SystemCodeDetail  T14 ON T1.ChangeLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ChangeStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		GROUP BY  T13.Constituency, T13.County, T13.Location,  T4.Code
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END

	ELSE  IF(@Type='DETAILS')
	 BEGIN

		SELECT @NoOfRows = COUNT(1)
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T1.*, T4.Code AS ProgrammeCode, T4.Name AS ProgrammeName, T2.Id  as EnrolmentNo, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo,
			T6.Id AS PriReciPersonId, T6.FirstName AS PriReciFirstName, T6.MiddleName AS PriReciMiddleName, T6.Surname AS PriReciSurname, T6.SexId AS PriReciSexId, T7.Code AS PriReciSex, T6.NationalIdNo AS PriReciNationalIdNo, T6.BirthCertNo AS PriReciBirthCertNo, T6.MobileNo1 AS PriReciMobileNo1, T6.MobileNo2 AS PriReciMobileNo2    
		 , T9.Id AS SecReciPersonId, T9.FirstName AS SecReciFirstName, T9.MiddleName AS SecReciMiddleName, T9.Surname AS SecReciSurname, T9.SexId AS SecReciSexId, T10.Code AS SecReciSex, T9.NationalIdNo AS SecReciNationalIdNo, T9.BirthCertNo AS SecReciBirthCertNo, T9.MobileNo1 AS SecReciMobileNo1, T9.MobileNo2 AS SecReciMobileNo2    
		 , T13.County, t13.Constituency, T13.District, T13.Division, T13.Location, T13.SubLocation, T13.GeoMasterId, T13.CountyId
		 , T141.Name CaseCategory
		 , T14.Description ChangeLevel
		 , T15.Description ChangeStatus
		 , T16.Description ReportedBySex
		 , T17.Description ReportedByType
		 , CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END) THEN 'YES' ELSE 'NO' END  AS SLA
		 , @NoOfRows NoOfRows
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			inner JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			inner JOIN SystemCodeDetail  T14 ON T1.ChangeLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ChangeStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T13.ConstituencyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END
END
GO

IF NOT OBJECT_ID('RptGetComplaints') IS NULL	
DROP PROC RptGetComplaints
GO
CREATE PROC RptGetComplaints
	@DateRange VARCHAR(20),
	@StartDate datetime,
	@EndDate datetime,
	@ProgrammeXml xml,
	@SexXml xml,
	@ComplaintTypexml xml,
	@ComplaintStatusxml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
	@Page int =1,
	@PageSize int=10,
	@Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime

AS
BEGIN
	IF(@Page=0) set @Page =1
	DECLARE  @offset int = (@Page-1)*@PageSize
	DECLARE  @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')
	IF(@Type='SUMMARY')
	BEGIN
		SELECT @NoOfRows = COUNT(1)
		FROM Complaint T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			inner JOIN CaseCategory  T141 ON T1.ComplaintTypeId = T141.Id
			INNER JOIN ( SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ComplaintStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		GROUP BY  T13.Constituency, T13.County, T13.Location,  T4.Code

		SELECT T13.Constituency, T13.County, T13.Location, T4.Code Programme, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS Male, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS Female, COUNT(T1.Id) AS Total
		, @NoOfRows NoOfRows 
		, COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'MeetSLA' , COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) < (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'FailSLA'

		FROM Complaint T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ComplaintTypeId = T141.Id
			INNER JOIN SystemCodeDetail  T14 ON T1.ComplaintLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ComplaintStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ComplaintStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T13.ConstituencyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		GROUP BY  T13.Constituency, T13.County, T13.Location,  T4.Code
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END
 
	ELSE  IF(@Type='DETAILS')
		BEGIN
		SELECT @NoOfRows = COUNT(1)
		FROM Complaint T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ComplaintTypeId = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ComplaintStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T1.*, T4.Code AS ProgrammeCode, T4.Name AS ProgrammeName, T2.Id  as EnrolmentNo, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo,
			T6.Id AS PriReciPersonId, T6.FirstName AS PriReciFirstName, T6.MiddleName AS PriReciMiddleName, T6.Surname AS PriReciSurname, T6.SexId AS PriReciSexId, T7.Code AS PriReciSex, T6.NationalIdNo AS PriReciNationalIdNo, T6.BirthCertNo AS PriReciBirthCertNo, T6.MobileNo1 AS PriReciMobileNo1, T6.MobileNo2 AS PriReciMobileNo2    
		, T9.Id AS SecReciPersonId, T9.FirstName AS SecReciFirstName, T9.MiddleName AS SecReciMiddleName, T9.Surname AS SecReciSurname, T9.SexId AS SecReciSexId, T10.Code AS SecReciSex, T9.NationalIdNo AS SecReciNationalIdNo, T9.BirthCertNo AS SecReciBirthCertNo, T9.MobileNo1 AS SecReciMobileNo1, T9.MobileNo2 AS SecReciMobileNo2    
		, T13.County, t13.Constituency, T13.District, T13.Division, T13.Location, T13.SubLocation, T13.GeoMasterId, T13.CountyId
		, T141.Name CaseCategory
		, T14.Description ComplaintLevel
		, T15.Description ComplaintStatus
		, T16.Description ReportedBySex
		, T17.Description ReportedByType
		, CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END) THEN 'YES' ELSE 'NO' END  AS SLA
		, @NoOfRows NoOfRows
		FROM Complaint T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ComplaintTypeId = T141.Id
			INNER JOIN SystemCodeDetail  T14 ON T1.ComplaintLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ComplaintStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ComplaintStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ComplaintStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T13.ConstituencyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END
END
GO


IF NOT OBJECT_ID('RptGetChangeMultiple') IS NULL	
DROP PROC RptGetChangeMultiple
GO
CREATE PROC RptGetChangeMultiple
	@DateRange VARCHAR(20),
	@StartDate datetime,
	@EndDate datetime,
	@ProgrammeXml xml,
	@SexXml xml,
	@ChangeTypexml xml,
	@ChangeStatusxml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
	@Page int =1,
	@PageSize int=10,
	@Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime
AS
BEGIN
	IF(@Page=0) set @Page =1
	DECLARE  @offset int = (@Page-1)*@PageSize
	DECLARE  @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')

	IF(@Type='SUMMARY')
	 BEGIN
		SELECT @NoOfRows =   count(DISTINCT CONCAT(T13.Location,T4.Code ) )
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
					) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T13.Constituency, T13.County, T13.Location, T4.Code Programme, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS Male, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS Female, COUNT(T1.Id) AS Total, @NoOfRows NoOfRows , COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'MeetSLA' , COUNT(CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) < (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END)  THEN 1 END) AS 'FailSLA'
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id INNER JOIN Division T3 ON T2.DivisionId=T3.Id INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id INNER JOIN District T5 ON T4.DistrictId=T5.Id INNER JOIN County T6 ON T4.CountyId=T6.Id INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN SystemCodeDetail  T14 ON T1.ChangeLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ChangeStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		GROUP BY  T13.Constituency, T13.County, T13.Location,  T4.Code
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END

	ELSE  IF(@Type='DETAILS')
	 BEGIN

		SELECT @NoOfRows = COUNT(1)
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			INNER JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate

		SELECT T1.*, T4.Code AS ProgrammeCode, T4.Name AS ProgrammeName, T2.Id  as EnrolmentNo, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo,
			T6.Id AS PriReciPersonId, T6.FirstName AS PriReciFirstName, T6.MiddleName AS PriReciMiddleName, T6.Surname AS PriReciSurname, T6.SexId AS PriReciSexId, T7.Code AS PriReciSex, T6.NationalIdNo AS PriReciNationalIdNo, T6.BirthCertNo AS PriReciBirthCertNo, T6.MobileNo1 AS PriReciMobileNo1, T6.MobileNo2 AS PriReciMobileNo2    
		 , T9.Id AS SecReciPersonId, T9.FirstName AS SecReciFirstName, T9.MiddleName AS SecReciMiddleName, T9.Surname AS SecReciSurname, T9.SexId AS SecReciSexId, T10.Code AS SecReciSex, T9.NationalIdNo AS SecReciNationalIdNo, T9.BirthCertNo AS SecReciBirthCertNo, T9.MobileNo1 AS SecReciMobileNo1, T9.MobileNo2 AS SecReciMobileNo2    
		 , T13.County, t13.Constituency, T13.District, T13.Division, T13.Location, T13.SubLocation, T13.GeoMasterId, T13.CountyId
		 , T141.Name CaseCategory
		 , T14.Description ChangeLevel
		 , T15.Description ChangeStatus
		 , T16.Description ReportedBySex
		 , T17.Description ReportedByType
		 , CASE WHEN  (DATEADD(DAY,T141.Timeline, T1.ReceivedOn)) > (CASE WHEN T1.ClosedOn IS NULL THEN GETDATE() ELSE T1.ClosedOn END) THEN 'YES' ELSE 'NO' END  AS SLA
		 , @NoOfRows NoOfRows
		FROM Change T1
			INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
			INNER JOIN Household T3 ON T2.HhId=T3.Id
			INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
			INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			LEFT JOIN Person T9 ON T8.PersonId=T9.Id
			LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T6.Id CountyId, T7.Id ConstituencyId, T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
			inner JOIN CaseCategory  T141 ON T1.ChangeTypeId = T141.Id
			inner JOIN SystemCodeDetail  T14 ON T1.ChangeLevelId = T14.Id
			INNER JOIN SystemCodeDetail  T15 ON T1.ChangeStatusId = T15.Id
			INNER JOIN SystemCodeDetail  T16 ON T1.ReportedBySexId = T16.Id
			INNER JOIN SystemCodeDetail  T17 ON T1.ReportedByTypeId = T17.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1) T18 ON T18.ID  = T1.ProgrammeId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @SexXml.nodes('Report/Record') AS U(R) ) T1) T20 ON T20.Id  = T6.SexId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeTypeXml.nodes('Report/Record') AS U(R) ) T1) T19 ON T19.ID  = T141.Id
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ChangeStatusXml.nodes('Report/Record') AS U(R) ) T1) T19b ON T19b.ID  = T1.ChangeStatusId
			INNER JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @CountyXml.nodes('Report/Record') AS U(R) ) T1) T21 ON T21.ID  = T13.CountyId
			LEFT JOIN (SELECT T1.Id
			FROM ( SELECT U.R.value('(Id)[1]','int') AS Id
				FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1) T22 ON T22.ID  = T13.ConstituencyId
		WHERE T1.CreatedOn BETWEEN @StartDate AND @EndDate
		ORDER by T13.Constituency, T13.County, T13.Location, T4.Code 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END
END
GO

/********************************************************/
/**************** ENROLMENT    **************************/
/********************************************************/
IF NOT OBJECT_ID('RptGetPendingEnrolment') IS NULL	
DROP PROC RptGetPendingEnrolment
GO
CREATE PROC RptGetPendingEnrolment

	@DateRange VARCHAR(20),
	@StartDate datetime,
	@EndDate datetime,
	@ProgrammeXml xml,
	@PrimarySexXml xml,
	@SecondarySexXml xml,
	@StatusXml xml,
	@RegGroupXml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
	@Page int =1,
	@PageSize int=10,
	@Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime
AS
BEGIN
	IF(@Page=0) set @Page =1
	DECLARE  @offset int = (@Page-1)*@PageSize
	DECLARE  @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')



	DECLARE  @tblTableId TABLE(
		Id int,
		IDType VARCHAR(20)
	)

	INSERT INTO @tblTableId
		(Id,IDType)
								SELECT T1.Id, T1.IDType
		FROM ( SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS IDType
			FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS IDType
			FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'STATUS' AS IDType
			FROM @StatusXml.nodes('Report/Record') AS U(R)) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'REGGROUP' AS IDType
			FROM @RegGroupXml.nodes('Report/Record') AS U(R) ) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS IDType
			FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS IDType
			FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
	UNION
		SELECT T1.Id, T1.IDType
		FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS IDType
			FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1

	IF(@Type='SUMMARY')
	BEGIN

		select @NoOfRows = count(T2.Id)
		FROM Household T2
			INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
			INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			INNER JOIN Person T9 ON T8.PersonId=T9.Id
			INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
				T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId


			--INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
			--INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
			--INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
			--INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
			--INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
			--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
			-- LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
			LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE T14.HhId IS NULL


		SELECT
			T13.Constituency
		 , T13.County
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		, T4.Name Programme
		, @NoOfRows NoOfRows 
		, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(T2.Id) AS TotalSecReci

		FROM Household T2
			INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
			INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			INNER JOIN Person T9 ON T8.PersonId=T9.Id
			INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
				T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

			--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
			--	INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
			----	INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
			--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
			INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'
			--	LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
			--	 LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
			LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE  T14.HhId IS NULL
		GROUP BY  T13.Constituency
		 , T13.County
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T4.Name
		ORDER by  T13.Constituency
		 , T13.County
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T4.Name 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

	END

 ELSE  IF(@Type='DETAILS')
	BEGIN

		select @NoOfRows = count(T2.Id)
		FROM Household T2
			INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id --AND T2.CreatedOn BETWEEN @StartDate AND @EndDate 
			INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			INNER JOIN Person T9 ON T8.PersonId=T9.Id
			INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
				T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

			--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
			--			INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
			--		INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
			--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
			INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'
			--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
			--LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
			LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE  T14.HhId IS NULL


		SELECT t2.Id, T6.FirstName AS BeneFirstName, t2.CreatedOn, T4.Name Programme
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		, @NoOfRows NoOfRows
		FROM Household T2
			INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id --AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
			INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
			INNER JOIN Person T6 ON T5.PersonId=T6.Id
			INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
			LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
			INNER JOIN Person T9 ON T8.PersonId=T9.Id
			INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
			INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
			INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
			INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
				T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

			--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
			--	INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
			--		INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
			--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
			--  INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
			--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
			--LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
			LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE    T14.Id IS NULL

		ORDER by  T13.CountyId, T13.ConstituencyId,T13.SubLocationId
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;
	END

END
GO


IF NOT OBJECT_ID('RptGetBeneficiariesWithNoAccounts') IS NULL	
DROP PROC RptGetBeneficiariesWithNoAccounts
GO

CREATE PROC RptGetBeneficiariesWithNoAccounts
    @DateRange VARCHAR(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @StatusXml xml,
    @RegGroupXml xml,
    @EnrolmentGroupXml xml,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE  @offset int = (@Page-1)*@PageSize
    DECLARE  @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    DECLARE  @Guid VARCHAR(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			                                                             SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'STATUS' AS ReportType
                FROM @StatusXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'REGGROUP' AS ReportType
                FROM @RegGroupXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'ENROLMENT_GRP' AS ReportType
                FROM @EnrolmentGroupXml.nodes('Report/Record') AS U(R) ) T1
			) T0


    
    
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T10.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T10.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T10.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
            INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
            INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId AND T2A.CreatedOn BETWEEN @StartDate AND @EndDate
            INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id
            INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id

            INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
            INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
            INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
            INNER JOIN temp_ReportID T18 ON T18.ReportId  = T1.ProgrammeId AND T18.ReportType = 'PROGRAMME' and T18.ReportGuId = @Guid
            INNER JOIN temp_ReportID T20 ON T20.ReportId  = T6.SexId AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.StatusId AND T19.ReportType = 'STATUS' and T19.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19b ON T19b.ReportId  = T1.RegGroupId AND T19b.ReportType = 'REGGROUP' and T19B.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19c ON T19c.ReportId  = T2A.EnrolmentGroupId AND T19C.ReportType = 'ENROLMENT_GRP' and T19C.ReportGuId = @Guid
            INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid

            LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid

            LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
            INNER JOIN Person T9 ON T8.PersonId=T9.Id
            INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            LEFT JOIN temp_ReportID T23 ON T23.ReportId  = T9.SexId AND T23.ReportType = 'SEC_SEX' and T23.ReportGuId = @Guid
            LEFT JOIN BeneficiaryAccount T3 ON T3.HhEnrolmentId= T2.Id
        WHERE T3.Id IS NULL

        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
	select X.*, T13.*, T4.Name, T4.code from x 
	inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	ELSE  IF(@Type='DETAILS')
	BEGIN
        SELECT t2.Id, T6.FirstName AS BeneFirstName, T1.CreatedOn, T4.Name Programme
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		, COUNT(T1.Id) OVER () as  NoOfRows
        FROM Household T1
            INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
            INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId
            INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id
            INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id

            INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
            INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
            INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

            INNER JOIN temp_ReportID T18 ON T18.ReportId  = T1.ProgrammeId AND T18.ReportType = 'PROGRAMME' and T18.ReportGuId = @Guid
            INNER JOIN temp_ReportID T20 ON T20.ReportId  = T6.SexId AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.StatusId AND T19.ReportType = 'STATUS' and T19.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19b ON T19b.ReportId  = T1.RegGroupId AND T19b.ReportType = 'REGGROUP' and T19B.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19c ON T19c.ReportId  = T2A.EnrolmentGroupId AND T19C.ReportType = 'ENROLMENT_GRP' and T19C.ReportGuId = @Guid
            INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid

            LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid

            LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
            INNER JOIN Person T9 ON T8.PersonId=T9.Id
            INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            LEFT JOIN temp_ReportID T23 ON T23.ReportId  = T9.SexId AND T23.ReportType = 'SEC_SEX' and T23.ReportGuId = @Guid
            LEFT JOIN BeneficiaryAccount T3 ON T3.HhEnrolmentId= T2.Id
        WHERE T3.Id IS NULL

        ORDER BY T1.CreatedOn
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END
GO



IF NOT OBJECT_ID('RptGetBeneficiariesPaymentCards') IS NULL	
DROP PROC RptGetBeneficiariesPaymentCards
GO

CREATE PROC RptGetBeneficiariesPaymentCards
    @DateRange VARCHAR(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @PspXml xml,
    @CardStatusXml xml,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE  @offset int = (@Page-1)*@PageSize
    DECLARE  @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    DECLARE  @Guid VARCHAR(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			 SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
               
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'PSP' AS ReportType
                FROM @PspXml.nodes('Report/Record') AS U(R) ) T1 
				UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CARD_STATUS' AS ReportType
                FROM @CardStatusXml.nodes('Report/Record') AS U(R) ) T1
			) T0


    
    
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T10.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T10.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T10.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
            INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
            INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId   AND T2A.CreatedOn BETWEEN @StartDate AND @EndDate
            INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id
            INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
            INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
            INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
            INNER JOIN (
                        SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                        T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                        INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                        INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                        INNER JOIN District T5 ON T4.DistrictId=T5.Id
                        INNER JOIN County T6 ON T4.CountyId=T6.Id
                        INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                        INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
			INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId 
            INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
            INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
			LEFT JOIN ( 
						SELECT DISTINCT T1.BeneAccountId, T1.PaymentCardNo, T1.StatusId   FROM BeneficiaryPaymentCard T1 
					) T17 ON T14.Id=T17.BeneAccountId INNER JOIN SystemCodeDetail T10 ON T17.StatusId=T10.Id                   		 
            INNER JOIN temp_ReportID T18 ON T18.ReportId  = T1.ProgrammeId AND T18.ReportType = 'PROGRAMME' and T18.ReportGuId = @Guid
            INNER JOIN temp_ReportID T20 ON T20.ReportId  = T6.SexId AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19C ON T19c.ReportId  = T16.ID AND T19C.ReportType = 'PSP' and T19C.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19D ON T19D.ReportId  = T17.StatusId AND T19D.ReportType = 'CARD_STATUS' and T19D.ReportGuId = @Guid
            INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
            LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid			 
            LEFT JOIN HouseholdMember T80 ON T2.Id=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
            INNER JOIN Person T90 ON T80.PersonId=T90.Id
            INNER JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
            LEFT JOIN temp_ReportID T23 ON T23.ReportId  = T90.SexId AND T23.ReportType = 'SEC_SEX' and T23.ReportGuId = @Guid          
        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
	select X.*, T13.*, T4.Name, T4.code from x 
	inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	
	
	ELSE  IF(@Type='DETAILS')
	BEGIN
      SELECT   
           @NoOfRows  =  Count_BIG(T1.Id) 
           
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN BeneficiaryPaymentCard T17 ON T14.Id = T17.BeneAccountId
    INNER JOIN SystemCodeDetail T18 ON T17.StatusId=T18.Id                 		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    INNER JOIN temp_ReportID T24 ON T24.ReportId  = T17.StatusId AND T24.ReportType = 'CARD_STATUS' and T24.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
 
    
    SELECT  T2.Id
    ,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo
		
    , T6.FirstName AS BeneFirstName 
    ,T1.CreatedOn
    , T4.Code Programme
          , T6.MiddleName AS BeneMiddleName
          , T6.Surname AS BeneSurname
          , T6.NationalIdNo AS BeneIDNo
          , T7.Code AS BeneSex
          , T6.DoB AS BeneDoB
          , ISNULL(T90.FirstName,'') AS CGFirstName
          , ISNULL(T90.MiddleName,'') AS CGMiddleName
          , ISNULL(T90.Surname,'') AS CGSurname
          , ISNULL(T90.NationalIdNo,'') AS CGIDNo
          , ISNULL(T100.Code,'') AS CGSex
          , ISNULL(T90.DoB,'') AS CGDoB
          , ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
          , ISNULL(T90.MobileNo1,T90.MobileNo2) AS MobileNo2
          , T13.County
          , T13.Constituency
          , T13.District
          , T13.Division
          , T13.Location
          , T13.SubLocation
          ,T15.[Name] AS Psp
          ,T16.[Name] AS PspBranch
          ,T14.OpenedOn AccountOpenedOn
         ,T14.DateAdded AccountAddedOn
          ,T14.AccountNo
          ,T14.AccountName
          ,T17.PaymentCardNo
          ,T18.[Description] CardingStatus
          ,T14B.[Description] AccountStatus
          ,T1B.[Description] HouseholdStatus
          ,@NoOfRows NoOfRows            
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN BeneficiaryPaymentCard T17 ON T14.Id = T17.BeneAccountId
    INNER JOIN SystemCodeDetail T18 ON T17.StatusId=T18.Id                 		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    INNER JOIN temp_ReportID T24 ON T24.ReportId  = T17.StatusId AND T24.ReportType = 'CARD_STATUS' and T24.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
    ORDER BY T1.Id
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END
GO

IF NOT OBJECT_ID('RptGetBeneficiariesAccounts') IS NULL	
DROP PROC RptGetBeneficiariesAccounts
GO
CREATE PROC  RptGetBeneficiariesAccounts
 
	  @DateRange VARCHAR(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @PspXml xml,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE  @offset int = (@Page-1)*@PageSize
    DECLARE  @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    DECLARE  @Guid VARCHAR(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			 SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
               
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'PSP' AS ReportType
                FROM @PspXml.nodes('Report/Record') AS U(R) ) T1 
			 
			) T0
      
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T100.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T100.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T100.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
         		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
     LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
      
        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
	select X.*, T13.*, T4.Name, T4.code from x 
	inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	
	
	ELSE  IF(@Type='DETAILS')
	BEGIN
      SELECT   
           @NoOfRows  =  Count_BIG(T1.Id) 
           
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
 
    
    SELECT  T2.Id
    ,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo
		
    , T6.FirstName AS BeneFirstName 
    ,T1.CreatedOn
    , T4.Code Programme
          , T6.MiddleName AS BeneMiddleName
          , T6.Surname AS BeneSurname
          , T6.NationalIdNo AS BeneIDNo
          , T7.Code AS BeneSex
          , T6.DoB AS BeneDoB
          , ISNULL(T90.FirstName,'') AS CGFirstName
          , ISNULL(T90.MiddleName,'') AS CGMiddleName
          , ISNULL(T90.Surname,'') AS CGSurname
          , ISNULL(T90.NationalIdNo,'') AS CGIDNo
          , ISNULL(T100.Code,'') AS CGSex
          , ISNULL(T90.DoB,'') AS CGDoB
          , ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
          , ISNULL(T90.MobileNo1,T90.MobileNo2) AS MobileNo2
          , T13.County
          , T13.Constituency
          , T13.District
          , T13.Division
          , T13.Location
          , T13.SubLocation
          ,T15.[Name] AS Psp
          ,T16.[Name] AS PspBranch
          ,T14.OpenedOn AccountOpenedOn
         ,T14.DateAdded AccountAddedOn
          ,T14.AccountNo
          ,T14.AccountName
           
          ,T14B.[Description] AccountStatus
          ,T1B.[Description] HouseholdStatus
          ,@NoOfRows NoOfRows            
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
               		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
    ORDER BY T1.Id
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END
GO

 




IF NOT OBJECT_ID('RptGetEnrolment') IS NULL	
DROP PROC RptGetEnrolment
GO
CREATE PROC RptGetEnrolment
	@HhEnrolmentPlanId   int,
	@ProgrammeId int,
	@AccountOpenedId int,
	@CardIssuedId int,
	@WithCaregiverId int,
	@ReportTypeId int
AS
BEGIN

	DECLARE  @SysCode VARCHAR(30)
	DECLARE  @SysDetailCode VARCHAR(30)
	DECLARE  @SystemCodeDetailId1 int
	DECLARE  @SystemCodeDetailId2 int
	DECLARE  @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SET @SysCode='Card Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	;with
		x
		as
		(
			SELECT T2.Id AS EnrolmentNo, pr.Code as Programme
		, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T2.ProgrammeNo)))+CONVERT(VARCHAR(6),T2.ProgrammeNo) AS ProgrammeNo
		, ISNULL(BPC.PriReciFirstName+' ','')+ISNULL(BPC.PriReciMiddleName+' ','')+ISNULL(BPC.PriReciSurname+' ','') AS BeneficiaryName
		, BPC.PriReciNationalIdNo AS BeneficiaryIdNo
		, T7.Code AS BeneficiarySex
		, BPC.PriReciDoB AS BeneficiaryDOB	
		, ISNULL(BPC.SecReciFirstName+' ','')+ISNULL(BPC.SecReciMiddleName+' ','')+ISNULL(BPC.SecReciSurname+' ','') AS CaregiverName
		, ISNULL(BPC.SecReciNationalIdNo,'') AS CaregiverIdNo
		, ISNULL(T10.Code,'') AS CaregiverSex
		, ISNULL(BPC.SecReciDoB,'') AS CaregiverDOB
		, ISNULL(BPC.MobileNo1,'') AS MobileNo
		, ISNULL(BPC.MobileNo2,'') AS MobileNo2
		, T13.County
		, T13.Constituency as SubCounty
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		, P.[Name] AS 'BANK'
		, PB.[NAME] AS 'BankBranch' 
		, BPC.PaymentCardNo      
		, CASE WHEN BPC.PaymentCardNo IS NULL THEN 'Account Opened' ELSE 'Account Opened,Payment Card Issued-' + cast(ISNULL(@ProgrammeId,0) as [VARCHAR]) END  AS 'Status'

			FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
				INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
				INNER JOIN Household h on h.Id=T2.HhId
				INNER JOIN Programme pr on pr.Id=h.ProgrammeId
				INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
				INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
					INNER JOIN Division T3 ON T2.DivisionId=T3.Id
					INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
					INNER JOIN District T5 ON T4.DistrictId=T5.Id
					INNER JOIN County T6 ON T4.CountyId=T6.Id
					INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
					INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1
		) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
				INNER JOIN (SELECT MIN(Id) AS Id, HhEnrolmentId, PSPBranchId
				FROM BeneficiaryAccount
				WHERE StatusId=@SystemCodeDetailId1
				GROUP BY HhEnrolmentId,PSPBranchId) BA ON BA.HhEnrolmentId = T2.Id
				INNER JOIN PSPBranch PB ON BA.PSPBranchId = PB.Id
				INNER JOIN PSP P ON PB.PSPId = P.ID
				LEFT JOIN (SELECT T1.BeneAccountId, T1.PaymentCardNo, T1.PriReciId, T1.PriReciFirstName, T1.PriReciMiddleName, T1.PriReciSurname, T1.PriReciSexId, T1.PriReciDoB, T1.PriReciNationalIdNo, T1.SecReciId, T1.SecReciFirstName, T1.SecReciMiddleName, T1.SecReciSurname, T1.SecReciSexId, T1.SecReciDoB, T1.SecReciNationalIdNo, T1.MobileNo1, T1.MobileNo2
				FROM BeneficiaryPaymentCard T1 INNER JOIN (
														SELECT BeneAccountId, MIN(Id) AS Id
					FROM BeneficiaryPaymentCard
					WHERE StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
					GROUP BY BeneAccountId
													) T2 ON T1.Id=T2.Id
		) BPC ON BPC.BeneAccountId = BA.Id
				LEFT JOIN SystemCodeDetail T7 ON BPC.PriReciSexId=T7.Id
				LEFT JOIN SystemCodeDetail T10 ON BPC.SecReciSexId=T10.Id
			WHERE T1.Id=ISNULL(@HHenrolmentPlanId,T1.Id)

				--AND h.ProgrammeId=ISNULL(@ProgrammeId,h.ProgrammeId)
				AND h.ProgrammeId = CASE WHEN @ProgrammeId IS NOT NULL THEN @ProgrammeId ELSE h.ProgrammeId END

				AND ISNULL(BPC.PaymentCardNo,'') = 
  CASE 
  WHEN @CardIssuedId = 1 THEN ISNULL(BPC.PaymentCardNo,0)
  WHEN @CardIssuedId = 2 THEN ''
  ELSE ISNULL(BPC.PaymentCardNo,'')
  END


				AND ISNULL(BPC.SecReciFirstName,'') = 
  CASE 
  WHEN @WithCaregiverId = 1 THEN ISNULL(BPC.SecReciFirstName,0)
  WHEN @WithCaregiverId = 2 THEN ''
  ELSE ISNULL(BPC.SecReciFirstName,'')
  END

				AND ISNULL(BPC.BeneAccountId,'') = 
  CASE 
  WHEN @AccountOpenedId = 1 THEN ISNULL(BPC.BeneAccountId,0)
  WHEN @AccountOpenedId = 2 THEN ''
  ELSE ISNULL(BPC.BeneAccountId,'')
  END
		)

	SELECT *
	INTO #Bens
	FROM x

	IF(@ReportTypeId=1)
BEGIN
		select *
		from #Bens
	END
ELSE
BEGIN
		select x.County, x.SubCounty, x.[Location], x.SubLocation,
			COUNT(CASE WHEN X.BeneficiarySex = 'M' THEN 1 END ) AS Male,
			COUNT(CASE WHEN X.BeneficiarySex = 'F' THEN 1 END ) AS Female,
			COUNT(x.BeneficiaryIdNo) AS Total
		FROM #Bens x
		GROUP BY  x.County,x.SubCounty,x.[Location],x.SubLocation
		ORDER by x.County,x.SubCounty,x.[Location],x.SubLocation
	END







END

GO




















































IF NOT OBJECT_ID('RptGetMonthlyActivity') IS NULL	
DROP PROC RptGetMonthlyActivity
GO
CREATE PROC  RptGetMonthlyActivity
	@ProgrammeId int
   ,
	@ActivityMonthId int
	,
	@UniqueWithdrawalsId int
	,
	@ProofOfLifeId int
   ,
	@DormancyId int
	,
	@ReportTypeId int
AS
BEGIN

	;with
		x
		as
		(
			select
				dbo.fn_MonthName(m.Code,1) as ActivityMonth,
				T4.Programme, T7.Id AS EnrolmentNo, T3.AccountNo, T4.BeneficiaryIdNo, T4.BeneficiaryName, T4.BeneficiaryDOB, T4.BeneficiarySex,
				T5.CaregiverIdNo, T5.CaregiverName, T5.CaregiverDOB, T5.CaregiverSex,

				T14.SubLocation,
				T14.Location,
				T14.Division,
				T14.District,
				T14.County,
				T14.Constituency as SubCounty

, CASE t2.HadUniqueWdl When 1 THEN 'Yes' ELSE 'No' END  as HadUniqueWithdrawal
, t2.UniqueWdlDate as DateOfWithdrawal
, CASE t2.IsDormant When 1 THEN 'Yes' ELSE 'No' END  as AccountDormant
, t2.DormancyDate
, CASE t2.HadBeneBiosVerified When 1 THEN 'Yes' ELSE 'No' END  as ProofOfLife 
, CASE t2.IsDueForClawback When 1 THEN 'Yes' ELSE 'No' END  as IsDueForClawback,
				t2.ClawbackAmount
			from BeneAccountMonthlyActivity t1
				INNER JOIN BeneAccountMonthlyActivityDetail t2 on t1.Id=t2.BeneAccountMonthlyActivityId
				INNER JOIN SystemCodeDetail m on m.Id=t1.MonthId
				INNER JOIN BeneficiaryAccount T3 ON T3.Id=t2.BeneAccountId
				INNER JOIN HouseholdEnrolment T7 ON T3.HhEnrolmentId=T7.Id
				INNER JOIN Household T11 ON t7.HhId=T11.Id
				INNER JOIN HouseholdSubLocation T12 ON T11.Id=T12.HhId
				INNER JOIN GeoMaster T13 ON T12.GeoMasterId=T13.Id AND T13.IsDefault=1
				INNER JOIN (
	SELECT T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T7.Name AS Division, T9.Name AS District, T10.Name AS County, T11.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
					INNER JOIN Division T7 ON T2.DivisionId=T7.Id
					INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
					INNER JOIN District T9 ON T8.DistrictId=T9.Id
					INNER JOIN County T10 ON T8.CountyId=T10.Id
					INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
					INNER JOIN GeoMaster T12 ON T10.GeoMasterId=T10.GeoMasterId AND T12.IsDefault=1
) T14 ON T12.SubLocationId=T14.SubLocationId

 INNER JOIN (
SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId, T2.HhId, T1.Id AS PersonId,
					ISNULL(T1.FirstName+' ','')+ISNULL(T1.MiddleName+' ','')+ISNULL(T1.Surname+' ','') AS BeneficiaryName
, T1.DoB as BeneficiaryDOB, T1.NationalIdNo as BeneficiaryIdNo, T4.Code 'Programme', T5.[Code] BeneficiarySex
				FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
					INNER JOIN Household T3 ON T2.HhId=T3.Id
					INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
					INNER JOIN SystemCodeDetail T5 ON T1.SexId = T5.Id
) T4 ON T1.Id=T4.HhId AND T4.RowId=1
				LEFT JOIN (
SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId, T2.HhId, T1.Id AS PersonId,
					ISNULL(T1.FirstName+' ','')+ISNULL(T1.MiddleName+' ','')+ISNULL(T1.Surname+' ','') AS CaregiverName,
					T1.DoB as CaregiverDOB, T1.NationalIdNo as CaregiverIdNo, T5.Code AS CaregiverSex
				FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
					INNER JOIN Household T3 ON T2.HhId=T3.Id
					INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.SecondaryRecipientId
					INNER JOIN SystemCodeDetail T5 ON T1.SexId = T5.Id
) T5 ON T1.Id=T5.HhId AND T5.RowId=1
			WHERE t1.MonthId= CASE WHEN @ActivityMonthId IS NOT NULL THEN @ActivityMonthId ELSE t1.MonthId END
				and T11.ProgrammeId=CASE WHEN @ProgrammeId IS NOT NULL THEN @ProgrammeId ELSE T11.ProgrammeId END
				and t2.HadUniqueWdl=CASE WHEN @UniqueWithdrawalsId IS NOT NULL THEN @UniqueWithdrawalsId ELSE t2.HadUniqueWdl END
				and t2.HadBeneBiosVerified=CASE WHEN @ProofOfLifeId IS NOT NULL THEN @ProofOfLifeId ELSE t2.HadBeneBiosVerified END
				and t2.IsDormant=CASE WHEN @DormancyId IS NOT NULL THEN @DormancyId ELSE t2.IsDormant END
		)
	SELECT *
	INTO #Bens
	FROM x

	IF(@ReportTypeId=1)
BEGIN
		select *
		from #Bens
	END
ELSE
BEGIN
		select x.County, x.SubCounty, x.[Location], x.SubLocation,
			COUNT(CASE WHEN X.BeneficiarySex = 'M' THEN 1 END ) AS Male,
			COUNT(CASE WHEN X.BeneficiarySex = 'F' THEN 1 END ) AS Female,
			COUNT(x.BeneficiaryIdNo) AS Total
		FROM #Bens x
		GROUP BY  x.County,x.SubCounty,x.[Location],x.SubLocation
		ORDER by x.County,x.SubCounty,x.[Location],x.SubLocation
	END

End
GO


IF NOT OBJECT_ID('RptGetPayrollStatement') IS NULL	
DROP PROC RptGetPayrollStatement
GO
CREATE PROC  RptGetPayrollStatement

	@ProgrammeId int
   ,
	@PaymentCycleId int
   ,
	@CreditStatusId int
   ,
	@PaymentTypeId int
	,
	@ReportTypeId int
AS
BEGIN
	;with
		x
		as
		(
			SELECT T7.Id AS EnrolmentNo, pr.Code as Programme, T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(VARCHAR(6),T7.ProgrammeNo)))+CONVERT(VARCHAR(6),T7.ProgrammeNo) AS ProgrammeNo,
				T10.Name AS PSP, T10.Code AS PSPCode, T9.Code AS PSPBranchCode,
				T8.AccountNo, T8.AccountName, ISNULL(t4.FirstName+' ','')+ISNULL(t4.MiddleName+' ','')+ISNULL(t4.Surname+' ','') AS BeneficiaryName,
				g.Code as BeneficiarySex, t4.DoB as BeneficiaryDOB,
				T1.PaymentAmount AS Amount, T4.NationalIDNo AS BeneficiaryIDNo,
				T4.MobileNo1 AS BeneficiaryContact,
				T5.NationalIDNo AS CaregiverIDNo,
				ISNULL(t5.FirstName+' ','')+ISNULL(t5.MiddleName+' ','')+ISNULL(t5.Surname+' ','') AS CaregiverName, cx.Code as CaregiverSex,
				T5.MobileNo1 AS CaregiverContact, t5.DoB as CaregiverDOB,
				T14.SubLocation,
				T14.Location,
				T14.Division,
				T14.District,
				T14.County,
				T14.Constituency as SubCounty,
				T2.[Description] AS PaymentCycle,
				CASE WHEN p.TrxAmount > 40000 THEN  40000  ELSE p.TrxAmount END as EntitlementAmount,
				CASE WHEN p.TrxAmount > 40000 THEN  p.TrxAmount-40000  ELSE 0 END as ArrearsAmount,
				p.TrxNo AS TransactionNo,
				CASE p.WasTrxSuccessful When 1 THEN 'Successful' ELSE 'Failed' END as CreditStatus
			FROM Payroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
				INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
				INNER JOIN Person T4 ON T3.BenePersonId=T4.Id
				LEFT JOIN Person T5 ON T3.CGPersonId=T5.Id
				INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
				INNER JOIN Programme pr on pr.Id=T1.ProgrammeId
				INNER JOIN Payment p ON T1.PaymentCycleId=p.PaymentCycleId AND T1.ProgrammeId=p.ProgrammeId AND T1.HhId=p.HhId
				INNER JOIN BeneficiaryAccount T8 ON T3.BeneAccountId=T8.Id
				INNER JOIN PSPBranch T9 ON T8.PSPBranchId=T9.Id
				INNER JOIN PSP T10 ON T9.PSPId=T10.Id
				INNER JOIN Household T11 ON T1.HhId=T11.Id
				INNER JOIN SystemCodeDetail g on g.Id =T4.SexId
				INNER JOIN SystemCodeDetail cx on cx.Id =T5.SexId
				INNER JOIN HouseholdSubLocation T12 ON T11.Id=T12.HhId
				INNER JOIN GeoMaster T13 ON T12.GeoMasterId=T13.Id AND T13.IsDefault=1
				INNER JOIN (
									SELECT T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T7.Name AS Division, T9.Name AS District, T10.Name AS County, T11.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
					INNER JOIN Division T7 ON T2.DivisionId=T7.Id
					INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
					INNER JOIN District T9 ON T8.DistrictId=T9.Id
					INNER JOIN County T10 ON T8.CountyId=T10.Id
					INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
					INNER JOIN GeoMaster T12 ON T10.GeoMasterId=T10.GeoMasterId AND T12.IsDefault=1
								) T14 ON T12.SubLocationId=T14.SubLocationId
			WHERE T1.PaymentCycleId=CASE WHEN @PaymentCycleId IS NOT NULL THEN @PaymentCycleId ELSE T1.PaymentCycleId END
				AND T1.ProgrammeId=CASE WHEN @ProgrammeId IS NOT NULL THEN @ProgrammeId ELSE T1.ProgrammeId END
				AND p.WasTrxSuccessful = CASE WHEN @CreditStatusId IS NOT NULL THEN @CreditStatusId ELSE p.WasTrxSuccessful END
		)
	SELECT *
	INTO #Bens
	FROM x

	IF(@ReportTypeId=1)
BEGIN
		select *
		from #Bens
	END
ELSE
BEGIN
		select x.County, x.SubCounty, x.[Location], x.SubLocation,
			COUNT(CASE WHEN X.BeneficiarySex = 'M' THEN 1 END ) AS Male,
			COUNT(CASE WHEN X.BeneficiarySex = 'F' THEN 1 END ) AS Female,
			COUNT(x.BeneficiaryIdNo) AS Total
		FROM #Bens x
		GROUP BY  x.County,x.SubCounty,x.[Location],x.SubLocation
		ORDER by x.County,x.SubCounty,x.[Location],x.SubLocation
	END

End
GO

IF NOT OBJECT_ID('RptGetPrePayrollException') IS NULL	
DROP PROC RptGetPrePayrollException
GO
CREATE PROC RptGetPrePayrollException
	@ProgrammeId int
   ,
	@PaymentCycleId int
   ,
	@CreditStatusId int
   ,
	@PaymentTypeId int
   ,
	@StatusId INT
   ,
	@ExceptionCategoryId INT
   ,
	@ReportTypeId int
AS
BEGIN

	DECLARE  @FileName VARCHAR(128)
	DECLARE  @FileExtension VARCHAR(5)
	DECLARE  @FileCompression VARCHAR(5)
	DECLARE  @FilePathName VARCHAR(128)
	DECLARE  @SQLStmt VARCHAR(8000)
	DECLARE  @FileExists bit
	DECLARE  @FileIsDirectory bit
	DECLARE  @FileParentDirExists bit
	DECLARE  @DatePart_Day char(2)
	DECLARE  @DatePart_Month char(2)
	DECLARE  @DatePart_Year char(4)
	DECLARE  @DatePart_Time char(4)
	DECLARE  @SysCode VARCHAR(20)
	DECLARE  @SysDetailCode VARCHAR(20)
	DECLARE  @SystemCodeDetailId1 int
	DECLARE  @SystemCodeDetailId2 int
	DECLARE  @FileCreationId int
	DECLARE  @FilePassword nVARCHAR(64)
	DECLARE  @Exception_INVALIDBENEID VARCHAR(20)
	DECLARE  @Exception_INVALIDCGID VARCHAR(20)
	DECLARE  @Exception_DUPLICATEBENEIDIN VARCHAR(20)
	DECLARE  @Exception_DUPLICATEBENEIDACC VARCHAR(20)
	DECLARE  @Exception_DUPLICATECGIDIN VARCHAR(20)
	DECLARE  @Exception_DUPLICATECGIDACC VARCHAR(20)
	DECLARE  @Exception_INVALIDACC VARCHAR(20)
	DECLARE  @Exception_INVALIDCARD VARCHAR(20)
	DECLARE  @Exception_SUSPICIOUSAMT VARCHAR(20)
	DECLARE  @Exception_SUSPICIOUSDORMANCY VARCHAR(20)
	DECLARE  @Exception_INELIGIBLEBENE VARCHAR(20)
	DECLARE  @Exception_INELIGIBLECG VARCHAR(20)
	DECLARE  @Exception_INELIGIBLESUS VARCHAR(20)
	DECLARE  @ErrorMsg VARCHAR(128)
	DECLARE  @NoOfRows int

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'

	;with
		x
		as
		(
			SELECT T1.PaymentCycleId, T2.[Description] AS PaymentCycle, T1.ProgrammeId, T3.Code AS Programme, TT4.Id AS EnrolmentNo
  
		, ISNULL(T1.BeneFirstName+' ','')+ISNULL(T1.BeneMiddleName+' ','')+ISNULL(T1.BeneSurname+' ','') AS BeneficiaryName,
				T1.BeneDoB as BeneficiaryDoB, T4.Code AS BeneficiarySex, T1.BeneNationalIDNo as BeneficiaryIdNo,

				T1.PriReciCanReceivePayment,
				CASE WHEN(ISNULL(T7.ActionedApvBy,0))>0 THEN -1 ELSE CONVERT(bit,ISNULL(T7.PersonId,0)) END AS IsInvalidBene,
				CASE WHEN(ISNULL(T8.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8.PersonId,0)) END AS IsDuplicateBene,
				CASE WHEN(ISNULL(T9.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T9.HhId,0)) END AS IsIneligibleBene,
				ISNULL(T1.CGFirstName+' ','')+ISNULL(T1.CGMiddleName+' ','')+ISNULL(T1.CGSurname+' ','') AS CaregiverName,
				T1.CGDoB as CareGiverDOB, T5.Code AS CaregiverSex, T1.CGNationalIDNo as CaregiverIdNo,
				CASE WHEN(ISNULL(T11.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T11.PersonId,0)) END AS IsInvalidCG,
				CASE WHEN(ISNULL(T12.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12.PersonId,0)) END AS IsDuplicateCG, CASE WHEN(ISNULL(T13.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T13.HhId,0)) END AS IsIneligibleCG
		, T6.[Description] AS HhStatus, CASE WHEN(ISNULL(T19.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T19.HhId,0)) END AS IsIneligibleHh, TT15.AccountNo AS AccountNumber, CASE WHEN(ISNULL(T15.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T15.HhId,0)) END AS IsInvalidAccount, CASE WHEN(ISNULL(T17.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T17.HhId,0)) END AS IsDormantAccount, TT16.PaymentCardNo AS PaymentCardNumber, CASE WHEN(ISNULL(T16.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T16.HhId,0)) END AS IsInvalidPaymentCard, T21.Name AS PaymentZone, T1.PaymentZoneCommAmt,
				T1.ConseAccInactivity, T1.EntitlementAmount, T1.AdjustmentAmount, CASE WHEN(ISNULL(T18.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T18.HhId,0)) END AS IsSuspiciousAmount, T25.SubLocation, T25.Location, T25.Division, T25.District, T25.County,
				T25.Constituency as SubCounty
		, CASE T11.Actioned When 1 THEN 'Yes' ELSE 'No' END as Actioned,
				T11.Notes as Reasons

			FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
				INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
				INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
				INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
				LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
				INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
				LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
				LEFT JOIN PrepayrollDuplicateID T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId
				LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.ActionedApvBy
				FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
				LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId
				LEFT JOIN PrepayrollDuplicateID T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId
				LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
				FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId
				LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
				LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
				LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.BenePaymentCardId=TT16.Id
				LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
				LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
				FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
				WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
				LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
				FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
				WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
				LEFT JOIN (SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, ActionedApvBy
				FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
				WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId
				LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
				INNER JOIN Household T22 ON T1.HhId=T22.Id
				INNER JOIN HouseholdSubLocation T23 ON T22.Id=T23.HhId
				INNER JOIN GeoMaster T24 ON T23.GeoMasterId=T24.Id AND T24.IsDefault=1
				INNER JOIN (
									SELECT T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T7.Name AS Division, T9.Name AS District, T10.Name AS County, T11.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
					INNER JOIN Division T7 ON T2.DivisionId=T7.Id
					INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
					INNER JOIN District T9 ON T8.DistrictId=T9.Id
					INNER JOIN County T10 ON T8.CountyId=T10.Id
					INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
								) T25 ON T23.SubLocationId=T25.SubLocationId
			WHERE T1.PaymentCycleId=CASE WHEN @PaymentCycleId IS NOT NULL THEN @PaymentCycleId ELSE T1.PaymentCycleId END
				AND T1.ProgrammeId=CASE WHEN @ProgrammeId IS NOT NULL THEN @ProgrammeId ELSE T1.ProgrammeId END
		)
	SELECT *
	INTO #Bens
	FROM x

	IF(@ReportTypeId=1)
BEGIN
		select *
		from #Bens
	END
ELSE
BEGIN
		select x.County, x.SubCounty, x.[Location], x.SubLocation,
			COUNT(CASE WHEN X.BeneficiarySex = 'M' THEN 1 END ) AS Male,
			COUNT(CASE WHEN X.BeneficiarySex = 'F' THEN 1 END ) AS Female,
			COUNT(x.BeneficiaryIdNo) AS Total
		FROM #Bens x
		GROUP BY  x.County,x.SubCounty,x.[Location],x.SubLocation
		ORDER by x.County,x.SubCounty,x.[Location],x.SubLocation
	END

End
GO

IF NOT OBJECT_ID('RptListing') IS NULL	
DROP PROC RptListing
GO
CREATE PROCEDURE  RptListing
	@CountyId   int,
	@SubCountyId int,
	@LocationId int,
	@SubLocationId int

AS
BEGIN

	SELECT

		*,
		TG.Constituency,
		TG.County,
		TG.LocationId,
		TG.Location
	FROM ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId

	WHERE TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
		AND TG.ConstituencyId = CASE WHEN @SubCountyId IS NOT NULL THEN @SubCountyId ELSE TG.ConstituencyId END
		AND TG.LocationId = CASE WHEN @LocationId IS NOT NULL THEN @LocationId ELSE TG.LocationId END
		AND TG.SubLocationId = CASE WHEN @SubLocationId IS NOT NULL THEN @SubLocationId ELSE TG.SubLocationId END
END

GO
