

---- STEP 001---

SELECT T2.* into Payment_Duplicates FROM Payment  T2 INNER JOIN (SELECT  T1.HhId FROM Payment T1 where T1.PaymentCycleId=4 group by T1.HhId having COUNT(T1.HhId)>1) T1 ON T1.HhId = T2.HhId 
WHERE T2.PaymentCycleId=4
ORDER BY T2.HhId ASC
GO
---- STEP 002---
DELETE T1 FROM PAYMENT T1 
INNER JOIN Payment_Duplicates T2 ON T1.HhId = T2.HhId AND T1.PaymentCycleId =T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId
WHERE T1.PaymentCycleId=4
GO
-- STEP 003
GO

 ;with y as ( SELECT  PaymentCycleId,ProgrammeId,HhId,TrxAmount,TrxDate,TrxNarration,TrxNo,WasTrxSuccessful,CreatedBy,CreatedOn FROM Payment_Duplicates ),
Z as ( SELECT ROW_NUMBER() OVER( partition by PaymentCycleId, ProgrammeId,HhId,TrxAmount, TrxNo  order by HhId desc) as RN,* FROM y )

INSERT INTO Payment  (PaymentCycleId,ProgrammeId,HhId,TrxAmount,TrxDate,TrxNarration,TrxNo,WasTrxSuccessful,CreatedBy,CreatedOn)  
SELECT PaymentCycleId,ProgrammeId,HhId,TrxAmount,TrxDate,TrxNarration,TrxNo,WasTrxSuccessful,CreatedBy,CreatedOn FROM Z WHERE RN=1
GO


-- STEP 003
ALTER TABLE Payment ADD CONSTRAINT PK_Payment PRIMARY KEY (PaymentCycleId,HhId)
ALTER TABLE Payment ADD CONSTRAINT FK_Payment_Payroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Payroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
ALTER TABLE Payment ADD CONSTRAINT FK_Payment_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
GO

IF NOT OBJECT_ID('Payment_Duplicates') IS NULL
	DROP TABLE Payment_Duplicates
GO
 

 
