 IF NOT OBJECT_ID('PSPEnrolBeneficiary') IS NULL	
   DROP PROC PSPEnrolBeneficiary
GO
 
CREATE PROC PSPEnrolBeneficiary 
	@EnrolmentNo int
   ,@TokenId int
   ,@BankCode nvarchar(20)
   ,@BranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AccountOpenedOn datetime
   ,@PriReciFirstName varchar(50)
   ,@PriReciMiddleName varchar(50)=NULL
   ,@PriReciSurname varchar(50)
   ,@PriReciNationalIdNo varchar(30)
   ,@PriReciSex char(1)
   ,@PriReciDoB datetime=NULL
   ,@SecReciFirstName varchar(50)=NULL
   ,@SecReciMiddleName varchar(50)=NULL
   ,@SecReciSurname varchar(50)=NULL
   ,@SecReciNationalIdNo varchar(30)=NULL
   ,@SecReciSex char(1)=NULL
   ,@SecReciDoB datetime=NULL
   ,@PaymentCardNo varchar(50)=	NULL
   ,@MobileNo1 nvarchar(20)=NULL
   ,@MobileNo2 nvarchar(20)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @HhEnrolmentId int
	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @PriReciId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @SystemCodeDetailId5 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @EnrolmentLimit int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int
    
	DECLARE @SecondaryRecMandatoryIfSupplied bit = 0


	SET @EnrolmentLimit=523129
	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId5=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@PriReciSex
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PriReciId=T1.Id,@PriReciDoB=ISNULL(@PriReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.PrimaryRecipientId
	WHERE T1.FirstName=@PriReciFirstName AND T1.MiddleName=ISNULL(@PriReciMiddleName,'') AND T1.Surname=@PriReciSurname AND T1.NationalIdNo=@PriReciNationalIdNo AND T1.SexId=@SystemCodeDetailId3 --AND T1.DoB=@PriReciDoB 
		
	SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id,@SecReciDoB=ISNULL(@SecReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB 
						
	SELECT @SecondaryRecMandatoryIfSupplied =1
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T3.Id = @EnrolmentNo and T6.Code='OPCT' 
		

	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
	 
     IF(ISNULL(@TokenId,0)=0)
     SET @ErrorMsg='Please specify valid Token ID parameter'
    ELSE IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id INNER JOIN Household T3 ON T1.HhId=T3.Id WHERE T1.Id=@EnrolmentNo AND T3.TokenId = @TokenId AND T2.StatusId=@SystemCodeDetailId1)  
		SET @ErrorMsg='Please specify valid Corresponding EnrolmentNo and TokenID parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF @AccountOpenedOn IS NULL OR NOT (@AccountOpenedOn<=GETDATE())
		SET @ErrorMsg='Please specify valid AccountOpenedOn parameter'
	ELSE IF ISNULL(@PriReciId,0)=0
		SET @ErrorMsg='Please specify valid Primary Recipient details'
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	
	ELSE IF((@SecondaryRecMandatoryIfSupplied=1) AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'


	ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)
		SET @ErrorMsg='The EnrolmentNo and Secondary Recipient details do not match'
	ELSE IF ISNULL(@MobileNo1,'')<>'' AND (CASE WHEN(@MobileNo1 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo1 parameter'
	ELSE IF ISNULL(@MobileNo2,'')<>'' AND (CASE WHEN(@MobileNo2 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo2 parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 
				   FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
											  INNER JOIN PSP T3 ON T2.PSPId=T3.Id
				   WHERE HhEnrolmentId=@EnrolmentNo AND ExpiryDate>GETDATE() 
						AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)
				   )
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
    

	ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@PriReciNationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	--ELSE IF EXISTS(SELECT 1 FROM [70PlusRegistration] WHERE OP_CONFIRM_ID_NO=@PriReciNationalIdNo AND RefId>523449)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
	--	SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	--ELSE IF (SELECT COUNT(DISTINCT T1.HhEnrolmentId) FROM BeneficiaryAccount T1 WHERE T1.StatusId=@SystemCodeDetailId5)>=@EnrolmentLimit	
	--	SET @ErrorMsg='The beneficiary enrolment numbers limit has exceeded. Currently, no more beneficiary enrolment will be allowed'
	ELSE IF EXISTS(	  SELECT 1 
					  FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
												 INNER JOIN PSP T3 ON T2.PSPId=T3.Id
					  WHERE T1.HhEnrolmentId=@EnrolmentNo
				)
		SET @ErrorMsg='Once a beneficiary has been enrolled with a valid account number it cannot be changed'			  
	--ELSE IF EXISTS(	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T7.BeneAccountId,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	--				FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','PSPCARDED','ONPAY'))
	--										   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1
	--										   INNER JOIN Person T4 ON T3.PersonId=T4.Id
	--										   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id AND T3.MemberRoleId=T5.PrimaryRecipientId
	--										   INNER JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
	--										   INNER JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
	--										   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
	--										   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	--				WHERE T4.NationalIdNo=@NationalIdNo 
	--				)

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SET @SysCode='System Settings'
	SET @SysDetailCode='BENEPSPRENEWALDATE'
	SELECT @AccountExpiryDate=CASE WHEN(ISNULL(T1.[Description],'')='' OR CONVERT(datetime,T1.[Description])<GETDATE()) THEN CONVERT(datetime,'30 June '+CONVERT(varchar(4),CASE WHEN(GETDATE()>CONVERT(datetime,'30 June '+CONVERT(varchar(4),YEAR(GETDATE())))) THEN YEAR(GETDATE())+1 ELSE YEAR(GETDATE()) END))
																													    ELSE CONVERT(datetime,T1.[Description])
							  END
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	--IF EXISTS(SELECT 1 
	--		  FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--									 INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--		  WHERE T1.HhEnrolmentId=@EnrolmentNo --AND T1.AccountNo=@AccountNo
	--		  )
	--BEGIN
	--	UPDATE T1
	--	SET T1.PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) 
	--	   ,T1.AccountNo=@AccountNo
	--	   ,T1.AccountName=@AccountName
	--	   ,T1.OpenedOn=@AccountOpenedOn
	--	   ,T1.StatusId=@SystemCodeDetailId1
	--	   ,T1.ExpiryDate=@AccountExpiryDate
	--	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo

	--	SELECT @AccountId=T1.Id
	--	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo
	--END
	--ELSE
	--BEGIN
		INSERT INTO BeneficiaryAccount(HhEnrolmentId,PSPBranchId,AccountNo,AccountName,OpenedOn,StatusId,ExpiryDate)
		SELECT @EnrolmentNo AS HhEnrolmentId
			,(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AS PSPBranchId
			,@AccountNo AS AccountNo
			,@AccountName AS AccountName
			,@AccountOpenedOn AS OpenedOn
			,@SystemCodeDetailId1 AS StatusId
			,@AccountExpiryDate AS ExpiryDate

		--SET @AccountId=IDENT_CURRENT('BeneficiaryAccount')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
		SELECT @AccountId=Id FROM BeneficiaryAccount WHERE PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AND AccountNo=@AccountNo
	--END

	SET @SysCode='Card Status'
	SET @SysDetailCode= CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN '-1' ELSE '1' END
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId

	--IF ISNULL(@PaymentCardId,0)>0
	--BEGIN
	--	UPDATE T1
	--	SET T1.BeneAccountId=@AccountId
	--	   ,T1.PriReciId=@PriReciId
	--	   ,T1.PriReciFirstname=@PriReciFirstName
	--	   ,T1.PriReciMiddleName=@PriReciMiddleName
	--	   ,T1.PriReciSurname=@PriReciSurname
	--	   ,T1.PriReciNationalIdNo=@PriReciNationalIdNo
	--	   ,T1.PriReciSexId=@SystemCodeDetailId3
	--	   ,T1.PriReciDoB=@PriReciDoB
	--	   ,T1.SecReciId=@SecReciId
	--	   ,T1.SecReciFirstname=@SecReciFirstName
	--	   ,T1.SecReciMiddleName=@SecReciMiddleName
	--	   ,T1.SecReciSurname=@SecReciSurname
	--	   ,T1.SecReciNationalIdNo=@SecReciNationalIdNo
	--	   ,T1.SecReciSexId=@SystemCodeDetailId4
	--	   ,T1.SecReciDoB=@SecReciDoB
	--	   ,T1.PaymentCardNo=@PaymentCardNo
	--	   ,T1.MobileNo1=@MobileNo1
	--	   ,T1.MobileNo2=@MobileNo2
	--	   ,T1.StatusId=@SystemCodeDetailId2
	--	   ,T1.ModifiedBy=@UserId
	--	   ,T1.ModifiedOn=GETDATE()
	--	FROM BeneficiaryPaymentCard T1
	--	WHERE T1.Id=@PaymentCardId
	--END
	--ELSE
	--BEGIN
		INSERT INTO BeneficiaryPaymentCard(BeneAccountId,PriReciId,PriReciFirstname,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSexId,PriReciDoB,SecReciId,SecReciFirstname,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSexId,SecReciDoB,PaymentCardNo,MobileNo1,MobileNo2,StatusId,CreatedBy,CreatedOn)
		SELECT @AccountId,@PriReciId,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@SystemCodeDetailId3,@PriReciDoB,@SecReciId,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SystemCodeDetailId4,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @PaymentCardId=Id FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId AND CreatedBy=@UserId AND StatusId=@SystemCodeDetailId2
		--SET @PaymentCardId=IDENT_CURRENT('BeneficiaryPaymentCard')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
	--END


	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN 'ENRLPSP' ELSE 'PSPCARDED' END
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	   ,T1.ModifiedBy=@UserId
	   ,T1.ModifiedOn=GETDATE()
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo AND T1.StatusId=@SystemCodeDetailId1

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS BeneAccountId,NULL AS PaymentCardId,NULL AS PriReciId,NULL AS SecReciId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @AccountId AS BeneAccountId,@PaymentCardId AS PaymentCardId,@PriReciId AS PriReciId,@SecReciId AS SecReciId
	END
END

GO
