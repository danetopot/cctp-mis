



select reggroupid,createdon,count(id) from household group by reggroupid,createdon


SELECT COUNT(ProgrammeNumber)
	,SUM(CASE WHEN(IPRS_IDNoExists=1) THEN 1 ELSE 0 END) AS VALPASS 
	,SUM(CASE WHEN(IPRS_IDNoExists=0) THEN 1 ELSE 0 END) AS VALPASS 
FROM [LegacyDB].dbo.ActiveCTOVCBeneficiaries 
WHERE ISNULL(Exited,0)=0


SELECT COUNT(ProgrammeNumber)
	,SUM(CASE WHEN(IPRS_CGIDNoExists=1 AND (IPRS_IDNoExists=1 OR ISNULL(IDNumber,'')='')) THEN 1 ELSE 0 END) AS VALPASS 
	,SUM(CASE WHEN(IPRS_CGIDNoExists=0 OR (IPRS_IDNoExists=0 AND ISNULL(IDNumber,'')<>'')) THEN 1 ELSE 0 END) AS VALPASS 
FROM LegacyDB.dbo.ActivePwSDBeneficiariesUPDATED 
WHERE ISNULL(Exited,0)=0


SELECT COUNT(ProgrammeNumber)
	,SUM(CASE WHEN(IPRS_IDNoExists=1 AND (IPRS_CGIDNoExists=1 OR ISNULL(CGIDNumber,'')='')) THEN 1 ELSE 0 END) AS VALPASS 
	,SUM(CASE WHEN(IPRS_IDNoExists=0 OR (IPRS_CGIDNoExists=0 AND ISNULL(CGIDNumber,'')<>'')) THEN 1 ELSE 0 END) AS VALPASS 
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED 
WHERE ISNULL(Exited,0)=0



select count(distinct COUNTID) from GeoLocations.dbo.GeoLocationsCensus2009
select count(distinct CONSTID) from GeoLocations.dbo.GeoLocationsCensus2009
select count(distinct DISTID) from GeoLocations.dbo.GeoLocationsCensus2009
select count(distinct DIVID) from GeoLocations.dbo.GeoLocationsCensus2009
select count(distinct LOCID) from GeoLocations.dbo.GeoLocationsCensus2009
select count(distinct SUBLID) from GeoLocations.dbo.GeoLocationsCensus2009

select count(distinct code) from county
select count(distinct code) from Constituency
select count(distinct code) from District
select count(distinct code) from Division
select count(distinct code) from Location
select count(distinct code) from SubLocation

