








DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT
DECLARE @Getter INT






SET @Code='User Group Type'
SET @Description='User Group Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='User Group Type'
IF(ISNULL(@Id,0)>0)
	BEGIN

	SET @DetailCode='Internal'
	SET @Description='Internal'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='External'
	SET @Description='External'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	END

	GO

	

	ALTER TABLE UserGroup ADD UserGroupTypeId  INT NOT NULL DEFAULT 0
	go
	DECLARE @Id int
	SELECT @Id = Id from SystemCodeDetail where Code='Internal'
UPDATE  UserGroup set  UserGroupTypeId = @id where [Name] not like '%psp%' and [Name] not like '%Auditor%'

 SELECT @Id = Id from SystemCodeDetail where Code='External'
 UPDATE  UserGroup set  UserGroupTypeId = @id where [Name] like   '%psp%' or [Name]  like    '%Auditor%'