getenrolmentprogress

SELECT T1.Code AS SubLocationCode,T1.Name AS SubLocation,T2.Code AS LocationCode,T2.Name AS Location,T3.Code AS DivisionCode,T3.Name AS Division,T5.Code AS DistrictCode,T5.Name AS District,T6.Code AS CountyCode,T6.Name AS County,T7.Code AS ConstituencyCode,T7.Name AS Constituency,T8.Code AS LocalityCode,T8.[Description] AS Locality
FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
					INNER JOIN Division T3 ON T2.DivisionId=T3.Id
					INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
					INNER JOIN District T5 ON T4.DistrictId=T5.Id
					INNER JOIN County T6 ON T4.CountyId=T6.Id
					INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
					INNER JOIN SystemCodeDetail T8 ON T1.LocalityId=T8.Id

SELECT RegGroupId,CreatedOn,COUNT(Id) FROM Household GROUP BY RegGroupId,CreatedOn

INSERT INTO Ward(Code,Name,ConstituencyId,CreatedBy,CreatedOn)

SELECT CONVERT(varchar(20),T1.CAWCode),T1.CAW,T3.Id AS ConstituencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
FROM GeoLocations.dbo.CAW T1 LEFT JOIN County T2 ON T1.CountyCode=T2.Code
							 LEFT JOIN Constituency T3 ON T1.ConstituencyCode=T3.Code
							 LEFT JOIN County T4 ON T3.CountyId=T4.Id


SELECT T1.Code AS SubLocationCode,T1.Name AS SubLocation,T5.Code AS LocalityCode,T5.[Description] AS Locality,T4.Code AS LocationCode,T4.Name AS Location,T3.Code AS ConstituencyCode,T3.Name AS Constituency,COUNT(T6.HhId) AS NoOfHhs
FROM SubLocation T1 LEFT JOIN GeoLocations.DBO.GeoLocationsCensus2009 T2 ON T1.Code=CONVERT(varchar(20),CONVERT(bigint,T2.SUBLID))
					LEFT JOIN Constituency T3 ON T1.ConstituencyId=T3.Id
					LEFT JOIN Location T4 ON T1.LocationId=T4.Id
					LEFT JOIN SystemCodeDetail T5 ON T1.LocalityId=T5.Id
					LEFT JOIN HouseholdSubLocation T6 ON T1.Id=T6.SubLocationId
WHERE T2.SUBLID IS NULL 
GROUP BY T1.Code,T1.Name,T5.Code,T5.[Description],T4.Code,T4.Name,T3.Code,T3.Name




SELECT T1.Code AS SubLocationCode,T1.Name AS SubLocation,T5.Code AS LocalityCode,T5.[Description] AS Locality,T4.Code AS LocationCode,T4.Name AS Location,T3.Code AS ConstituencyCode,T3.Name AS Constituency,COUNT(T6.HhId) AS NoOfHhs
FROM SubLocation T1 LEFT JOIN Constituency T3 ON T1.ConstituencyId=T3.Id
					LEFT JOIN Location T4 ON T1.LocationId=T4.Id
					LEFT JOIN GeoLocations.DBO.GeoLocationsCensus2009 T2 ON T4.Code=CONVERT(varchar(20),CONVERT(bigint,T2.LOCID))
					LEFT JOIN SystemCodeDetail T5 ON T1.LocalityId=T5.Id
					LEFT JOIN HouseholdSubLocation T6 ON T1.Id=T6.SubLocationId
WHERE T2.LOCID IS NULL and t1.id not in(
SELECT T1.id--Code AS SubLocationCode,T1.Name AS SubLocation,T5.Code AS LocalityCode,T5.[Description] AS Locality,T4.Code AS LocationCode,T4.Name AS Location,T3.Code AS ConstituencyCode,T3.Name AS Constituency,COUNT(T6.HhId) AS NoOfHhs
FROM SubLocation T1 LEFT JOIN GeoLocations.DBO.GeoLocationsCensus2009 T2 ON T1.Code=CONVERT(varchar(20),CONVERT(bigint,T2.SUBLID))
					LEFT JOIN Constituency T3 ON T1.ConstituencyId=T3.Id
					LEFT JOIN Location T4 ON T1.LocationId=T4.Id
					LEFT JOIN SystemCodeDetail T5 ON T1.LocalityId=T5.Id
					LEFT JOIN HouseholdSubLocation T6 ON T1.Id=T6.SubLocationId
WHERE T2.SUBLID IS NULL 
GROUP BY T1.id)
GROUP BY T1.Code,T1.Name,T5.Code,T5.[Description],T4.Code,T4.Name,T3.Code,T3.Name











