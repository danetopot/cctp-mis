
ALTER PROC GetEnrolmentProgressReportByCountyAndProgramme
@Programme VARCHAR(10)
AS
BEGIN
select  County, [1] AS CoopBank, [2] AS PostBank, [3] AS EBL, [4] AS KCB

FROM 

(
SELECT T1.Name 'County', T1.Id, T2.* FROM County  T1 LEFT JOIN ( 
SELECT T1.Id 'BeneAcc',T13.County CountyName, T2.PSPId, T4.Name 'Programme'
from   BeneficiaryAccount T1
  inner join  PSPBranch T2 ON T1.PSPBranchId = T2.Id
  INNER JOIN PSP T3 ON T3.Id = T2.PSPId
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id
  INNER JOIN Household T6 ON T5.HhId=T6.Id
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE T1.DateAdded IS not NULL  AND T1.StatusId=23 AND T4.Code=@Programme
  ) T2 ON T1.Name = T2.CountyName
  ) P
  PIVOT (COUNT (BeneAcc) FOR PSPId IN ([1],[2],[3],[4]))AS PVT


   ORDER BY PVT.County
   END
   


   GO

   GetEnrolmentProgressReportByCountyAndProgramme 'OPCT'
   go
 GetEnrolmentProgressReportByCountyAndProgramme 'CT-OVC'
 go
  GetEnrolmentProgressReportByCountyAndProgramme 'PwSD-CT'
  go

