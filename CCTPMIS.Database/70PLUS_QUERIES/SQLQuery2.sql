

DECLARE @CreatedinNovember datetime = '2018-11-01'

SELECT COUNT(T1.PersonId), FirstName, MiddleName, Surname, DoB, T1.HhId FROM HouseholdMember T1 
INNER JOIN  Household T2 ON T1.HhId = T2.ID
INNER JOIN  HouseholdSubLocation T3 ON T2.ID= T3.HhId  
INNER JOIN Person T4 ON T4.Id = T1.PersonId AND T2.CreatedOn>@CreatedinNovember
  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency

								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id

													INNER JOIN Division T3 ON T2.DivisionId=T3.Id

													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id

													INNER JOIN District T5 ON T4.DistrictId=T5.Id

													INNER JOIN County T6 ON T4.CountyId=T6.Id AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo')

													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id

													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id

								) T5 ON T3.SubLocationId=T5.SubLocationId AND T3.GeoMasterId=T5.GeoMasterId		
 GROUP BY FirstName, MiddleName, Surname, DoB, T1.HhId
HAVING COUNT(T4.ID)>1






select top 100 * from BeneficiaryAccount order by id desc