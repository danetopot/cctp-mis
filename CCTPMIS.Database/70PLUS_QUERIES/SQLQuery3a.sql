


DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW varchar(20)
DECLARE @SysRightCode_ENTRY varchar(20)
DECLARE @SysRightCode_MODIFIER varchar(20)
DECLARE @SysRightCode_DELETION varchar(20)
DECLARE @SysRightCode_APPROVAL varchar(20)
DECLARE @SysRightCode_FINALIZE varchar(20)
DECLARE @SysRightCode_VERIFY varchar(20)
DECLARE @SysRightCode_EXPORT varchar(20)
DECLARE @SysRightCode_DOWNLOAD varchar(20)
DECLARE @SysRightCode_UPLOAD varchar(20)

DECLARE @SysRightId_VIEW int
DECLARE @SysRightId_ENTRY int
DECLARE @SysRightId_MODIFIER int
DECLARE @SysRightId_DELETION int
DECLARE @SysRightId_APPROVAL int
DECLARE @SysRightId_FINALIZE int
DECLARE @SysRightId_VERIFY int
DECLARE @SysRightId_EXPORT int
DECLARE @SysRightId_DOWNLOAD int
DECLARE @SysRightId_UPLOAD int

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD


SET @ModuleCode='PSP-Profile'


SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL