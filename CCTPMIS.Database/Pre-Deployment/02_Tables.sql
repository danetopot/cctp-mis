/*
	   PURPOSE: THIS DB SCRIPT ATTEMPTS TO CREATE ALL THE DB TABLES TO HOLD DATA IN THE CCTP MIS
	CREATED BY: JOAB SELELYA (MIS SPECIALIST - DEVELOPMENT PATHWAYS LTD)
	CREATED ON: 7TH FEBRUARY, 2018
	UPDATED ON: 6TH MARCH, 2018
	
	NOTE: IT REQUIRES AN EXISTING DATABASE	
*/

IF NOT OBJECT_ID('BeneAccountMonthlyActivityDetail') IS NULL	DROP TABLE BeneAccountMonthlyActivityDetail
GO

IF NOT OBJECT_ID('BeneAccountMonthlyActivity') IS NULL	DROP TABLE BeneAccountMonthlyActivity
GO

IF NOT OBJECT_ID('Payment') IS NULL	DROP TABLE Payment
GO

IF NOT OBJECT_ID('Payroll') IS NULL	DROP TABLE Payroll
GO

IF NOT OBJECT_ID('PrepayrollSuspicious') IS NULL	DROP TABLE PrepayrollSuspicious
GO

IF NOT OBJECT_ID('PrepayrollIneligible') IS NULL	DROP TABLE PrepayrollIneligible
GO

IF NOT OBJECT_ID('PrepayrollInvalidPaymentCard') IS NULL	DROP TABLE PrepayrollInvalidPaymentCard
GO

IF NOT OBJECT_ID('PrepayrollInvalidPaymentAccount') IS NULL	DROP TABLE PrepayrollInvalidPaymentAccount
GO

IF NOT OBJECT_ID('PrepayrollDuplicateID') IS NULL	DROP TABLE PrepayrollDuplicateID
GO

IF NOT OBJECT_ID('PrepayrollInvalidID') IS NULL	DROP TABLE PrepayrollInvalidID
GO

IF NOT OBJECT_ID('Prepayroll') IS NULL	DROP TABLE Prepayroll
GO

IF NOT OBJECT_ID('FundsRequestDetail') IS NULL	DROP TABLE FundsRequestDetail
GO

IF NOT OBJECT_ID('FundsRequest') IS NULL	DROP TABLE FundsRequest
GO

IF NOT OBJECT_ID('PaymentAdjustment') IS NULL	DROP TABLE PaymentAdjustment
GO

IF NOT OBJECT_ID('PaymentEnrolmentGroup') IS NULL	DROP TABLE PaymentEnrolmentGroup
GO

IF NOT OBJECT_ID('PaymentTargetGroup') IS NULL	DROP TABLE PaymentTargetGroup
GO

IF NOT OBJECT_ID('PaymentCycleDetail') IS NULL	DROP TABLE PaymentCycleDetail
GO

IF NOT OBJECT_ID('PaymentAccountActivityMonth') IS NULL	DROP TABLE PaymentAccountActivityMonth
GO

IF NOT OBJECT_ID('PaymentCycle') IS NULL	DROP TABLE PaymentCycle
GO

IF NOT OBJECT_ID('ReconciliationDetail') IS NULL	DROP TABLE ReconciliationDetail
GO

IF NOT OBJECT_ID('Reconciliation') IS NULL	DROP TABLE Reconciliation
GO

IF NOT OBJECT_ID('PaymentCardBiometrics') IS NULL	DROP TABLE PaymentCardBiometrics
GO

IF NOT OBJECT_ID('BeneficiaryPaymentCard') IS NULL	DROP TABLE BeneficiaryPaymentCard
GO

IF NOT OBJECT_ID('BeneficiaryAccount') IS NULL	DROP TABLE BeneficiaryAccount
GO

IF NOT OBJECT_ID('HouseholdEnrolment') IS NULL	DROP TABLE HouseholdEnrolment
GO

IF NOT OBJECT_ID('HouseholdEnrolmentPlan') IS NULL	DROP TABLE HouseholdEnrolmentPlan
GO

IF NOT OBJECT_ID('HouseholdMember') IS NULL	DROP TABLE HouseholdMember
GO

IF NOT OBJECT_ID('PersonPension') IS NULL	DROP TABLE PersonPension
GO

IF NOT OBJECT_ID('PersonSocialAssistance') IS NULL	DROP TABLE PersonSocialAssistance
GO

IF NOT OBJECT_ID('Person') IS NULL	DROP TABLE Person
GO

IF NOT OBJECT_ID('HouseholdVillageElder') IS NULL	DROP TABLE HouseholdVillageElder
GO

IF NOT OBJECT_ID('HouseholdVillage') IS NULL	DROP TABLE HouseholdVillage
GO

IF NOT OBJECT_ID('HouseholdSubLocation') IS NULL	DROP TABLE HouseholdSubLocation
GO

IF NOT OBJECT_ID('Household') IS NULL	DROP TABLE Household
GO

IF NOT OBJECT_ID('DBBackup') IS NULL	DROP TABLE DBBackup
GO

IF NOT OBJECT_ID('ExpansionPlanDetail') IS NULL	DROP TABLE ExpansionPlanDetail
GO

IF NOT OBJECT_ID('ExpansionPlan') IS NULL	DROP TABLE ExpansionPlan
GO

IF NOT OBJECT_ID('FileDownload') IS NULL	DROP TABLE FileDownload
GO

IF NOT OBJECT_ID('FileCreation') IS NULL	DROP TABLE FileCreation
GO

IF NOT OBJECT_ID('ExpansionPlanMaster') IS NULL	DROP TABLE ExpansionPlanMaster
GO

IF NOT OBJECT_ID('PSPBranch') IS NULL	DROP TABLE PSPBranch
GO

IF NOT OBJECT_ID('PSP') IS NULL	DROP TABLE PSP
GO

IF NOT OBJECT_ID('Programme') IS NULL	DROP TABLE Programme
GO

IF NOT OBJECT_ID('WardLocation') IS NULL	DROP TABLE WardLocation
GO

IF NOT OBJECT_ID('Village') IS NULL	DROP TABLE Village
GO

IF NOT OBJECT_ID('SubLocation') IS NULL	DROP TABLE SubLocation
GO

IF NOT OBJECT_ID('Location') IS NULL	DROP TABLE Location
GO

IF NOT OBJECT_ID('Division') IS NULL	DROP TABLE Division
GO

IF NOT OBJECT_ID('CountyDistrict') IS NULL	DROP TABLE CountyDistrict
GO

IF NOT OBJECT_ID('District') IS NULL	DROP TABLE District
GO

IF NOT OBJECT_ID('Ward') IS NULL	DROP TABLE Ward
GO

IF NOT OBJECT_ID('Constituency') IS NULL	DROP TABLE Constituency
GO

IF NOT OBJECT_ID('County') IS NULL	DROP TABLE County
GO

IF NOT OBJECT_ID('PaymentZone') IS NULL	DROP TABLE PaymentZone
GO

IF NOT OBJECT_ID('GeoMaster') IS NULL	DROP TABLE GeoMaster
GO

IF NOT OBJECT_ID('GroupRight') IS NULL	DROP TABLE GroupRight
GO

IF NOT OBJECT_ID('ModuleRight') IS NULL	DROP TABLE ModuleRight
GO

IF NOT OBJECT_ID('Module') IS NULL	DROP TABLE Module
GO

IF NOT OBJECT_ID('SystemCodeDetail') IS NULL	DROP TABLE SystemCodeDetail
GO

IF NOT OBJECT_ID('SystemCode') IS NULL	DROP TABLE SystemCode
GO

IF NOT OBJECT_ID('PasswordReset') IS NULL	DROP TABLE PasswordReset
GO

IF NOT OBJECT_ID('[UserRole]') IS NULL	DROP TABLE [UserRole]
GO

IF NOT OBJECT_ID('[Role]') IS NULL	DROP TABLE [Role]
GO

IF NOT OBJECT_ID('[UserLogin]') IS NULL	DROP TABLE [UserLogin]
GO

IF NOT OBJECT_ID('[UserClaim]') IS NULL	DROP TABLE [UserClaim]
GO

IF NOT OBJECT_ID('User') IS NULL	DROP TABLE [User]
GO

IF NOT OBJECT_ID('UserGroup') IS NULL	DROP TABLE UserGroup
GO

IF NOT OBJECT_ID('UserGroupProfile') IS NULL	DROP TABLE UserGroupProfile
GO

IF NOT OBJECT_ID('[__MigrationHistory]') IS NULL	DROP TABLE [__MigrationHistory]
GO
/* REQUIRED

-- To allow advanced options to be changed.  
EXEC sp_configure 'show advanced options', 1;  
GO  
-- To update the currently configured value for advanced options.  
RECONFIGURE;  
GO  
-- To enable the feature.  
EXEC sp_configure 'xp_cmdshell', 1;  
GO  
-- To update the currently configured value for this feature.  
RECONFIGURE;  
GO 

IF NOT OBJECT_ID('fn_SystemCodeExist') IS NULL	DROP FUNCTION fn_SystemCodeExist
GO

CREATE FUNCTION fn_SystemCodeExist(@SystemCode tinyint,@Value int)
RETURNS bit
AS
BEGIN
	DECLARE @RetVal bit
	IF EXISTS(SELECT 1 
			  FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.SystemCodeId
			  WHERE UPPER(T2.Code)=(CASE(@SystemCode) WHEN 1 THEN 'Target Group'
													  WHEN 2 THEN 'Other Programme'
													  WHEN 3 THEN 'Locality'
													  ELSE '0'
									END
									) AND T1.SystemCodeDetailId=@Value
			  )
		SET @RetVal=1
	ELSE
		SET @RetVal=0
	RETURN @RetVal
END
GO


IF NOT OBJECT_ID('fn_ProgrammeNumber') IS NULL	DROP FUNCTION fn_ProgrammeNumber
GO
CREATE FUNCTION fn_ProgrammeNumber(@HhEnrolmentPlanId int)
RETURNS int
AS
BEGIN
	RETURN (
			SELECT MAX(T1.ProgrammeNo)+1
			FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id
									   INNER JOIN HouseholdEnrolmentPlan T3 ON T3.Id=@HhEnrolmentPlanId AND T2.ProgrammeId=T3.ProgrammeId
			)
END
GO


*/


IF NOT OBJECT_ID('fn_MonthName') IS NULL	DROP FUNCTION fn_MonthName
GO
CREATE FUNCTION fn_MonthName(@MonthId tinyint,@IsShortName bit=1)
RETURNS varchar(30)
AS
BEGIN
	RETURN CASE(@IsShortName) WHEN 1 THEN CASE(@MonthId)
										WHEN 1 THEN 'Jan'
										WHEN 2 THEN 'Feb'
										WHEN 3 THEN 'Mar'
										WHEN 4 THEN 'Apr'
										WHEN 5 THEN 'May'
										WHEN 6 THEN 'Jun'
										WHEN 7 THEN 'Jul'
										WHEN 8 THEN 'Aug'
										WHEN 9 THEN 'Sep'
										WHEN 10 THEN 'Oct'
										WHEN 11 THEN 'Nov'
										WHEN 12 THEN 'Dec'
										ELSE ''
									END
							  ELSE CASE(@MonthId)
										WHEN 1 THEN 'January'
										WHEN 2 THEN 'February'
										WHEN 3 THEN 'March'
										WHEN 4 THEN 'April'
										WHEN 5 THEN 'May'
										WHEN 6 THEN 'June'
										WHEN 7 THEN 'July'
										WHEN 8 THEN 'August'
										WHEN 9 THEN 'September'
										WHEN 10 THEN 'October'
										WHEN 11 THEN 'November'
										WHEN 12 THEN 'December'
										ELSE ''
									END
			END
END
GO



CREATE TABLE [dbo].[__MigrationHistory] (
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
	CONSTRAINT [PK___MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
)
GO



CREATE TABLE UserGroupProfile(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL     
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_UserGroupProfile PRIMARY KEY (Id)
	,CONSTRAINT IX_UserGroupProfile_Name UNIQUE NONCLUSTERED (Name)
)
GO



CREATE TABLE UserGroup(
	Id int NOT NULL IDENTITY(1,1)
   ,UserGroupProfileId int
   ,Name varchar(20) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_UserGroup PRIMARY KEY (Id)
   ,CONSTRAINT FK_UserGroup_UserGroupProfile_UserGroupProfileId FOREIGN KEY (UserGroupProfileId) REFERENCES UserGroupProfile(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE [User](
	Id int NOT NULL IDENTITY(1,1)
   ,UserName nvarchar(256) NOT NULL
   ,PasswordHash nvarchar(256) NULL
   ,SecurityStamp nvarchar(256) NULL
   ,UserGroupId int NOT NULL
   ,Email nvarchar(100) NOT NULL
   ,EmailConfirmed bit NOT NULL DEFAULT(0)
   ,PasswordChangeDate datetime NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL
   ,Avatar nvarchar(70) NULL
   ,Organization varchar(100) NOT NULL
   ,Department varchar(100) NOT NULL
   ,Position varchar(100) NOT NULL
   ,MobileNo nvarchar(20) NOT NULL
   ,MobileNoConfirmed bit NOT NULL DEFAULT(0)
   ,IsActive bit NOT NULL DEFAULT(0)
   ,DeactivateDate datetime NULL
   ,LoginDate datetime NULL
   ,ActivityDate datetime NULL
   ,AccessFailedCount int NOT NULL DEFAULT(0)
   ,IsLocked bit NOT NULL DEFAULT(0)
   ,LockoutEnabled bit NOT NULL DEFAULT(0)
   ,LockoutEndDateUTC datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_User PRIMARY KEY (Id)
   ,CONSTRAINT FK_User_UserGroup_UserGroupId FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT IX_User_UserName UNIQUE NONCLUSTERED (UserName)
   ,CONSTRAINT IX_User_Email UNIQUE NONCLUSTERED (Email)
)
GO



CREATE TABLE [dbo].[UserClaim] (
	[Id] [int] NOT NULL IDENTITY,
	[ClaimType] [nvarchar](256),
	[ClaimValue] [nvarchar](256),
	[UserId] [int] NOT NULL,
	CONSTRAINT [PK_UserClaim] PRIMARY KEY ([Id])
   ,CONSTRAINT FK_UserClaim_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE [dbo].[UserLogin] (
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [int] NOT NULL,
	CONSTRAINT [PK_UserLogin] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
   ,CONSTRAINT FK_UserLogin_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION

)
GO



CREATE TABLE [dbo].[Role] (
	[Id] [int] NOT NULL IDENTITY,
	[Description] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	CONSTRAINT [PK_Role] PRIMARY KEY ([Id])
)
GO



CREATE TABLE [dbo].[UserRole] (
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	CONSTRAINT [PK_UserRole] PRIMARY KEY ([UserId], [RoleId])
   ,CONSTRAINT FK_UserRole_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_UserRole_Role_RoleId FOREIGN KEY (RoleId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE [dbo].[PasswordReset] (
	[Id] [bigint] NOT NULL IDENTITY,
	[UserId] [int] NOT NULL,
	[RequestTime] [datetime] NOT NULL,
	[ResetCode] [nvarchar](128),
	[ResetTime] [datetime],
	[IsReset] [bit] NOT NULL,
	[ValidityPeriod] [int] NOT NULL,
	CONSTRAINT [PK_PasswordReset] PRIMARY KEY ([Id])
   ,CONSTRAINT FK_PasswordReset_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE SystemCode(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,IsUserMaintained bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_SystemCode PRIMARY KEY (Id)
   ,CONSTRAINT IX_SystemCode_Code UNIQUE NONCLUSTERED (Code)
)
GO



CREATE TABLE SystemCodeDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,SystemCodeId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,OrderNo tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SystemCodeDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_SystemCodeDetail_SystemCode_SystemCodeId FOREIGN KEY (SystemCodeId) REFERENCES SystemCode(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_SystemCodeDetail_SystemCodeId_Code UNIQUE NONCLUSTERED (SystemCodeId,Code)
)
GO



CREATE TABLE Module(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,ParentModuleId tinyint NULL
   ,CONSTRAINT PK_Module PRIMARY KEY (Id)
)
GO



CREATE TABLE ModuleRight(
	Id INT NOT NULL IDENTITY(1,1)
   ,ModuleId tinyint NOT NULL
   ,RightId int NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_ModuleRight PRIMARY KEY (Id)
   ,CONSTRAINT FK_ModuleRight_Module FOREIGN KEY (ModuleId) REFERENCES Module(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_ModuleRight_SystemCodeDetail FOREIGN KEY (RightId) REFERENCES SystemCodeDetail(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT IX_ModuleRight_ModuleId_RightId UNIQUE NONCLUSTERED (ModuleId,RightId)
)
GO



CREATE TABLE GroupRight(
	UserGroupId int NOT NULL
   ,ModuleRightId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_GroupRight PRIMARY KEY (UserGroupId,ModuleRightId)
   ,CONSTRAINT FK_GroupRight_UserGroup_UserGroupId FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_ModuleRight_ModuleRightId FOREIGN KEY (ModuleRightId) REFERENCES GHTght(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_GroupRight_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PaymentZone(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,Commission money NOT NULL
   ,IsPerc bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PaymentZone PRIMARY KEY (Id)
   ,CONSTRAINT FK_PaymentZone_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentZone_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE GeoMaster(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,IsDefault bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_GeoMaster PRIMARY KEY (Id)
   ,CONSTRAINT FK_GeoMaster_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_GeoMaster_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_GeoMaster_Name UNIQUE NONCLUSTERED(Name)
)
GO



CREATE TABLE County(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_County PRIMARY KEY (Id)
   ,CONSTRAINT FK_County_GeoMaster_GeoMasterId FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_County_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_County_GeoMasterId_Code UNIQUE NONCLUSTERED(GeoMasterId,Code)
)
GO



CREATE TABLE Constituency(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CountyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Constituency PRIMARY KEY (Id)
   ,CONSTRAINT FK_Constituency_County_CountyId FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Constituency_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Constituency_CountyId_Code UNIQUE NONCLUSTERED(CountyId,Code)
)
GO



CREATE TABLE Ward(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Ward PRIMARY KEY (Id)
   ,CONSTRAINT FK_Ward_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Ward_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Ward_ConstituencyId_Code UNIQUE NONCLUSTERED(ConstituencyId,Code)
)
GO



CREATE TABLE District(
	Id int NOT NULL IDENTITY(1,1)
   ,GeoMasterId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_District PRIMARY KEY (Id)
   ,CONSTRAINT FK_District_GeoMaster_GeoMasterId FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_District_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_District_GeoMasterId_Code UNIQUE NONCLUSTERED(GeoMasterId,Code)
)
GO



CREATE TABLE CountyDistrict(
	Id int NOT NULL IDENTITY(1,1)
   ,CountyId int NOT NULL
   ,DistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CountyDistrict PRIMARY KEY (Id)
   ,CONSTRAINT FK_CountyDistrict_County_CountyId FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_District_DistrictId FOREIGN KEY (DistrictId) REFERENCES District(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CountyDistrict_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_CountyDistrict_CountyId_DistrictId UNIQUE NONCLUSTERED(CountyId,DistrictId)
)
GO



CREATE TABLE Division(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,CountyDistrictId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Division PRIMARY KEY (Id)
   ,CONSTRAINT FK_Division_CountyDistrict_CountyDistrictId FOREIGN KEY (CountyDistrictId) REFERENCES CountyDistrict(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Division_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Division_CountyDistrictId_Code UNIQUE NONCLUSTERED(CountyDistrictId,Code)
)
GO



CREATE TABLE Location(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,DivisionId int NOT NULL
   ,PaymentZoneId smallint NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Location PRIMARY KEY (Id)
   ,CONSTRAINT FK_Location_Division_DivisionId FOREIGN KEY (DivisionId) REFERENCES Division(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Location_Zone_PaymentZoneId FOREIGN KEY (PaymentZoneId) REFERENCES PaymentZone(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Location_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Location_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Location_DivisionId_Code UNIQUE NONCLUSTERED(DivisionId,Code)
)
GO



CREATE TABLE SubLocation(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,LocalityId int NOT NULL
   ,LocationId int NOT NULL
   ,ConstituencyId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SubLocation PRIMARY KEY (Id)
   ,CONSTRAINT FK_SubLocation_Location_LocationId FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_SystemCodeDetail_LocalityId FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SubLocation_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_SubLocation_LocationId_Code UNIQUE NONCLUSTERED(LocationId,Code)
)
GO

CREATE TABLE WardLocation(
	WardId int NOT NULL
   ,LocationId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_WardLocation PRIMARY KEY (WardId,LocationId)
   ,CONSTRAINT FK_WardLocation_Ward_WardId FOREIGN KEY (WardId) REFERENCES Ward(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_Location_LocationId FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_WardLocation_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE Village(
	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,Name varchar(30) NOT NULL
   ,SubLocationId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Village PRIMARY KEY (Id)
   ,CONSTRAINT FK_Village_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_Village_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Village_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE [Programme](
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,BeneficiaryTypeId int NOT NULL	--INDIVIDUAL OR HOUSEHOLD ENTITLEMENT
   ,EntitlementAmount money NOT NULL DEFAULT(0)
   ,PrimaryRecipientId int NOT NULL
   ,PriReciCanReceivePayment bit NOT NULL DEFAULT(1)
   ,SecondaryRecipientId int NULL
   ,SecondaryRecipientMandatory bit NOT NULL DEFAULT(0)
   ,BeneProgNoPrefix varchar(10) NOT NULL
   ,PaymentFrequencyId int NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Programme PRIMARY KEY (Id) 
   ,CONSTRAINT FK_Programme_SystemCodeDetail_BeneficiaryTypeId FOREIGN KEY (BeneficiaryTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail_PrimaryRecipientId FOREIGN KEY (PrimaryRecipientId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail_SecondaryRecipientId FOREIGN KEY (SecondaryRecipientId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_SystemCodeDetail_PaymentFrequencyId FOREIGN KEY (PaymentFrequencyId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Programme_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_Programme_Code UNIQUE NONCLUSTERED(Code)
   ,CONSTRAINT IX_Programme_BeneProgNoPrefix UNIQUE NONCLUSTERED(BeneProgNoPrefix)

)
GO



CREATE TABLE PSP(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,PrimaryContact varchar(100) NOT NULL
   ,PrimaryTel1 nvarchar(20) NOT NULL
   ,PrimaryTel2 nvarchar(20) NULL
   ,PrimaryEmail varchar(100) NOT NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,UserId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSP PRIMARY KEY (Id)
   ,CONSTRAINT FK_PSP_User_UserId FOREIGN KEY (UserId) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSP_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PSP_Code UNIQUE NONCLUSTERED (Code)
   ,
)
GO



CREATE TABLE PSPBranch(
	Id smallint NOT NULL IDENTITY(1,1)
   ,PSPId tinyint NOT NULL
   ,Code nvarchar(20) NOT NULL
   ,Name nvarchar(100) NOT NULL
   ,SubLocationId int NULL
   ,Latitude float NULL
   ,Longitude float NULL
   ,IsActive bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PSPBranch PRIMARY KEY (Id)
   ,CONSTRAINT FK_PSPBranch_PSP FOREIGN KEY (PSPId) REFERENCES PSP(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_SubLocation FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_User1 FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PSPBranch_User2 FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PSPBranch_Code UNIQUE NONCLUSTERED (PSPId,Code)
)
GO



CREATE TABLE ExpansionPlanMaster(
	Id smallint NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,EffectiveFromDate datetime NOT NULL
   ,EffectiveToDate datetime NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlanMaster PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlanMaster_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanMaste_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanMaster_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_ExpansionPlanMaster_Code_ProgrammeId UNIQUE NONCLUSTERED(Code,ProgrammeId)
)
GO



CREATE TABLE ExpansionPlan(
	Id int NOT NULL IDENTITY(1,1)
   ,ExpansionPlanMasterId smallint NOT NULL
   ,LocationId int NOT NULL
   ,PovertyHeadCountPerc float --SMALL AREA ESTIMATE
   ,CategoricalHHs int
   ,ScaleupEqualShare int
   ,ScaleupPovertyPrioritized int
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlan PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlan_ExpansionPlanMaster_ExpansionPlanMasterId FOREIGN KEY (ExpansionPlanMasterId) REFERENCES ExpansionPlanMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_Location_LocationId FOREIGN KEY (LocationId) REFERENCES Location(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlan_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_ExpansionPlan_ExpansionPlanMasterId_LocationId UNIQUE NONCLUSTERED(ExpansionPlanMasterId,LocationId)
)
GO



CREATE TABLE ExpansionPlanDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,ExpansionPlanId int NOT NULL
   ,FinancialYearId int NOT NULL
   ,EnrolledHHs int NOT NULL DEFAULT(0)
   ,ScaleupEqualShare int NOT NULL DEFAULT(0)
   ,ScaleupPovertyPrioritized int NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ExpansionPlanDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_ExpansionPlanDetail_ExpansionPlan_ExpansionPlanId FOREIGN KEY (ExpansionPlanId) REFERENCES ExpansionPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_SystemCodeDetail_FinancialYearId FOREIGN KEY (FinancialYearId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ExpansionPlanDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_ExpansionPlanDetail_ExpansionPlanId_FinancialYearId UNIQUE NONCLUSTERED(ExpansionPlanId,FinancialYearId)
)
GO



CREATE TABLE DBBackup(
	DBBackupId int NOT NULL IDENTITY(1,1)
   ,FilePath nvarchar(128) NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT FK_DBBackup_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE Household(
	Id int NOT NULL IDENTITY(1,1)
   ,ProgrammeId tinyint NOT NULL
   ,RegGroupId int NOT NULL
   ,RefId varchar(50) NULL
   ,Village varchar(50) NULL
   ,StatusId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Household PRIMARY KEY (Id)
   ,CONSTRAINT FK_Household_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Household_SystemCodeDetail_RegGroupId FOREIGN KEY (RegGroupId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Household_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Household_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Household_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE HouseholdSubLocation(
	HhId int NOT NULL
   ,GeoMasterId int NOT NULL
   ,SubLocationId int NOT NULL
   ,CONSTRAINT PK_HouseholdSubLocation PRIMARY KEY (HhId,GeoMasterId,SubLocationId)
   ,CONSTRAINT FK_HouseholdSubLocation_Household_HhId FOREIGN KEY (HhId) REFERENCES Household(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdSubLocation_GeoMaster_GeoMasterId FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdSubLocation_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_HouseholdSubLocation_HhId_GeoMasterId_SubLocationId UNIQUE NONCLUSTERED(HhId,GeoMasterId,SubLocationId)
)
GO



CREATE TABLE HouseholdVillage(
	HhId int NOT NULL
   ,GeoMasterId int NOT NULL
   ,VillageId int NOT NULL
   ,CONSTRAINT PK_HouseholdVillage PRIMARY KEY (HhId,GeoMasterId,VillageId)
   ,CONSTRAINT FK_HouseholdVillage_GeoMaster_GeoMasterId FOREIGN KEY (GeoMasterId) REFERENCES GeoMaster(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdVillage_Household_HhId FOREIGN KEY (HhId) REFERENCES Household(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdVillage_Village_VillageId FOREIGN KEY (VillageId) REFERENCES Village(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_HouseholdVillage_GeoMasterId_VillageId UNIQUE NONCLUSTERED(GeoMasterId,VillageId)
)
GO



CREATE TABLE HouseholdVillageElder(
	HhId int NOT NULL
   ,Name varchar(100) NOT NULL
   ,MobileNo nvarchar(20) NOT NULL
   ,MobileNoConfirmed bit NOT NULL DEFAULT(0)
   ,CONSTRAINT FK_HouseholdVillageElder_Household_HhId FOREIGN KEY (HhId) REFERENCES Household(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE Person(
	Id int NOT NULL IDENTITY(1,1)
   ,RefId varchar(50) NULL
   ,FirstName varchar(50) NOT NULL
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50) NOT NULL
   ,SexId int NOT NULL
   ,DoB datetime NOT NULL
   ,BirthCertNo varchar(50) NULL
   ,NationalIdNo varchar(30) NULL
   ,MobileNo1 nvarchar(20) NULL
   ,MobileNo1Confirmed bit NOT NULL DEFAULT(0)
   ,MobileNo2 nvarchar(20) NULL
   ,MobileNo2Confirmed bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_Person PRIMARY KEY (Id)
   ,CONSTRAINT FK_Person_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_Person_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   --,CONSTRAINT IX_Person_BirthCertNo UNIQUE NONCLUSTERED(BirthCertNo)
   --,CONSTRAINT IX_Person_NationalIdNo UNIQUE NONCLUSTERED(NationalIdNo)
)
GO



CREATE TABLE PersonSocialAssistance(
	PersonId int NULL
   ,OtherSPProgrammeId int NOT NULL
   ,[Description] varchar(128) NULL
   ,BenefitTypeId int NOT NULL
   ,BenefitAmount money NOT NULL DEFAULT(0)
   ,CONSTRAINT FK_PersonSocialAssistance_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_PersonSocialAssistance_SystemCodeDetailId_OtherSPProgrammeId FOREIGN KEY (OtherSPProgrammeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PersonSocialAssistance_SystemCodeDetailId_BenefitTypeId FOREIGN KEY (BenefitTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PersonSocialAssistance_PersonId_OtherSPProgrammeId UNIQUE NONCLUSTERED(PersonId,OtherSPProgrammeId)
)
GO



CREATE TABLE PersonPension(
	PersonId int NULL
   ,IsGovPension bit NOT NULL DEFAULT(0)
   ,[Description] varchar(128) NULL
   ,PensionAmount money NOT NULL DEFAULT(0)
   ,CONSTRAINT FK_PersonPension_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE HouseholdMember(
	Id int NOT NULL IDENTITY(1,1)
   ,HhId int NOT NULL
   ,PersonId int NOT NULL
   ,RelationshipId int NOT NULL
   ,MemberRoleId int NOT NULL
   ,StatusId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_HouseholdMember PRIMARY KEY (Id)
   ,CONSTRAINT FK_HouseholdMember_Household_HhId FOREIGN KEY (HhId) REFERENCES Household(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_SystemCodeDetail_RelationshipId FOREIGN KEY (RelationshipId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_SystemCodeDetail_MemberRoleId FOREIGN KEY (MemberRoleId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdMember_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_HouseholdMember_HhId_PersonId UNIQUE NONCLUSTERED(HhId,PersonId)
)
GO



CREATE TABLE FileCreation(
	Id int NOT NULL IDENTITY(1,1)
   ,Name varchar(128) NOT NULL
   ,TypeId int NOT NULL	--WHETHER ENROLMENT, PAYMENT etc
   ,CreationTypeId int NOT NULL --WHETHER AN UPLOAD, SYSTEM GENERATION
   ,FilePath varchar(128) NOT NULL
   ,FileChecksum varchar(64) NULL
   ,FilePassword nvarchar(64) NULL
   ,IsShared bit NOT NULL DEFAULT(0)
   ,TargetUserId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_FileCreation PRIMARY KEY (Id)
   ,CONSTRAINT FK_FileCreation_SystemCodeDetail_TypeId FOREIGN KEY (TypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_FileCreation_SystemCodeDetail_CreationTypeId FOREIGN KEY (CreationTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_FileCreation_User1_TargetUserId FOREIGN KEY (TargetUserId) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_FileCreation_User2_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_FileCreation_Name UNIQUE NONCLUSTERED(Name)
)
GO



CREATE TABLE FileDownload(
	Id int NOT NULL IDENTITY(1,1)
   ,FileCreationId int NOT NULL
   ,FileChecksum varchar(64) NULL
   ,DownloadedBy int NOT NULL
   ,DownloadedOn datetime NOT NULL
   ,CONSTRAINT PK_FileDownload PRIMARY KEY (Id)
   ,CONSTRAINT FK_FileDownload_FileCreation_FileCreationId FOREIGN KEY (FileCreationId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_FileDownload_User_DownloadedBy FOREIGN KEY (DownloadedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE HouseholdEnrolmentPlan(
	Id int NOT NULL IDENTITY(1,1)
   ,ProgrammeId tinyint NOT NULL
   ,RegGroupId int NOT NULL
   ,RegGroupHhs int NOT NULL DEFAULT(0)
   ,BeneHhs int NOT NULL DEFAULT(0)
   ,ExpPlanEqualShare int NOT NULL DEFAULT(0)
   ,ExpPlanPovertyPrioritized int NOT NULL DEFAULT(0)
   ,EnrolmentNumbers int NOT NULL
   ,EnrolmentGroupId int NOT NULL
   ,StatusId int NOT NULL
   ,FileCreationId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,CONSTRAINT PK_HouseholdEnrolmentPlan PRIMARY KEY (Id)		
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_File_FileCreationId FOREIGN KEY (FileCreationId) REFERENCES FileCreation(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_SystemCodeDetail_RegGroupId FOREIGN KEY (RegGroupId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_SystemCodeDetail_EnrolmentGroupId FOREIGN KEY (EnrolmentGroupId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolmentPlan_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE HouseholdEnrolment(
	Id int NOT NULL IDENTITY(1,1)
   ,HhEnrolmentPlanId int NOT NULL
   ,HhId int NOT NULL
   ,BeneProgNoPrefix varchar(10) NOT NULL
   ,ProgrammeNo int NOT NULL
   ,CONSTRAINT PK_HouseholdEnrolment PRIMARY KEY (Id)
   ,CONSTRAINT FK_HouseholdEnrolment_HouseholdEnrolmentPlan_HhEnrolmentPlanId FOREIGN KEY (HhEnrolmentPlanId) REFERENCES HouseholdEnrolmentPlan(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdEnrolment_Household_HhId FOREIGN KEY (HhId) REFERENCES Household(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_HouseholdEnrolment_HhId UNIQUE NONCLUSTERED(HhId)
   ,CONSTRAINT IX_HouseholdEnrolment_BeneProgNoPrefix_ProgrammeNo UNIQUE NONCLUSTERED(BeneProgNoPrefix,ProgrammeNo)
)
GO	



CREATE TABLE BeneficiaryAccount(
	Id int NOT NULL IDENTITY(1,1)
   ,HhEnrolmentId int NOT NULL
   ,PSPBranchId smallint NOT NULL
   ,AccountNo varchar(50) NOT NULL
   ,AccountName varchar(100) NOT NULL
   ,OpenedOn datetime NOT NULL
   ,StatusId int NOT NULL
   ,ExpiryDate datetime NOT NULL
   ,CONSTRAINT PK_BeneficiaryAccount PRIMARY KEY (Id)
   ,CONSTRAINT FK_BeneficiaryAccount_HouseholdEnrolment_HhEnrolmentId FOREIGN KEY (HhEnrolmentId) REFERENCES HouseholdEnrolment(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryAccount_PSPBranch_PSPBranchId FOREIGN KEY (PSPBranchId) REFERENCES PSPBranch(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryAccount_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_BeneficiaryAccount_PSPBranchId_AccountNo UNIQUE NONCLUSTERED(PSPBranchId,AccountNo)
)
GO



CREATE TABLE BeneficiaryPaymentCard(
	Id int NOT NULL IDENTITY(1,1)
   ,BeneAccountId int NOT NULL
   ,PriReciId int NOT NULL
   ,PriReciFirstName varchar(50) NOT NULL
   ,PriReciMiddleName varchar(50) NULL
   ,PriReciSurname varchar(50) NOT NULL
   ,PriReciNationalIdNo varchar(30) NOT NULL
   ,PriReciSexId int NOT NULL
   ,PriReciDoB datetime NOT NULL
   ,SecReciId int NULL
   ,SecReciFirstName varchar(50) NULL
   ,SecReciMiddleName varchar(50) NULL
   ,SecReciSurname varchar(50) NULL
   ,SecReciNationalIdNo varchar(30) NULL
   ,SecReciSexId int NULL
   ,SecReciDoB datetime NULL   
   ,MobileNo1 nvarchar(20) NULL
   ,MobileNo2 nvarchar(20) NULL
   ,PaymentCardNo varchar(50) NULL
   ,StatusId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,CONSTRAINT PK_BeneficiaryPaymentCard PRIMARY KEY (Id)
   ,CONSTRAINT FK_BeneficiaryPaymentCard_BeneficiaryAccount_BeneAccountId FOREIGN KEY (BeneAccountId) REFERENCES BeneficiaryAccount(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_Person_PriReciId FOREIGN KEY (PriReciId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_SystemCodeDetail_PriReciSexId FOREIGN KEY (PriReciSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_Person_SecReciId FOREIGN KEY (SecReciId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_SystemCodeDetail_SecReciSexId FOREIGN KEY (SecReciSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneficiaryPaymentCard_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   --,CONSTRAINT IX_BeneficiaryPaymentCard_BeneAccountId_PaymentCardNo UNIQUE NONCLUSTERED(BeneAccountId,PaymentCardNo)
)
GO



CREATE TABLE PaymentCardBiometrics(
	BenePaymentCardId int NOT NULL
   ,PriReciRT varbinary(max) NULL
   ,PriReciRI varbinary(max) NULL
   ,PriReciRMF varbinary(max) NULL
   ,PriReciRRF varbinary(max) NULL
   ,PriReciRP varbinary(max) NULL
   ,PriReciLT varbinary(max) NULL
   ,PriReciLI varbinary(max) NULL
   ,PriReciLMF varbinary(max) NULL
   ,PriReciLRF varbinary(max) NULL
   ,PriReciLP varbinary(max) NULL
   ,SecReciRT varbinary(max) NULL
   ,SecReciRI varbinary(max) NULL
   ,SecReciRMF varbinary(max) NULL                                               
   ,SecReciRRF varbinary(max) NULL
   ,SecReciRP varbinary(max) NULL
   ,SecReciLT varbinary(max) NULL
   ,SecReciLI varbinary(max) NULL
   ,SecReciLMF varbinary(max) NULL
   ,SecReciLRF varbinary(max) NULL
   ,SecReciLP varbinary(max) NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   --,CONSTRAINT FK_PaymentCardBiometrics_BeneficiaryPaymentCard_BenePaymentCardId FOREIGN KEY (BenePaymentCardId) REFERENCES BeneficiaryPaymentCard(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   --,CONSTRAINT FK_PaymentCardBiometrics_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PaymentCardBiometrics_BenePaymentCardId UNIQUE NONCLUSTERED(BenePaymentCardId)
)
GO




CREATE TABLE Reconciliation(
	Id int NOT NULL IDENTITY(1,1)
   ,StartDate datetime NOT NULL
   ,EndDate datetime NOT NULL
   ,StatusId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,CONSTRAINT PK_Reconciliation PRIMARY KEY (Id)
   ,CONSTRAINT FK_Reconciliation_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_Reconciliation_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Reconciliation_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Reconciliation_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE ReconciliationDetail(
	ReconciliationId int NOT NULL
   ,PSPId tinyint NOT NULL
   ,OpeningBalance money NOT NULL
   ,CrFundsRequests money NOT NULL
   ,CrFundsRequestsStatement money NOT NULL
   ,CrFundsRequestsDiffNarration varchar(128) NULL
   ,CrClawBacks money NOT NULL
   ,CrClawBacksStatement money NOT NULL
   ,CrClawBacksDiffNarration varchar(128) NULL
   ,DrPayments money NOT NULL
   ,DrPaymentsStatement money NOT NULL
   ,DrPaymentsDiffNarration varchar(128) NULL
   ,DrCommissions money NOT NULL
   ,DrCommissionsStatement money NOT NULL
   ,DrCommissionsDiffNarration varchar(128) NULL
   ,Balance money NOT NULL
   ,BalanceStatement money NOT NULL
   ,BalanceDiffNarration varchar(128) NULL
   ,BankStatementFileId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_ReconciliationDetail PRIMARY KEY (ReconciliationId,PSPId)
   ,CONSTRAINT FK_ReconciliationDetail_Reconciliation_ReconciliationId FOREIGN KEY (ReconciliationId) REFERENCES Reconciliation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_ReconciliationDetail_PSP_PSPId FOREIGN KEY (PSPId) REFERENCES PSP(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_ReconciliationDetail_FileCreation_BankStatementFileId FOREIGN KEY (BankStatementFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_ReconciliationDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_ReconciliationDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO




CREATE TABLE PaymentCycle(
	Id int NOT NULL IDENTITY(1,1)
   ,[Description] varchar(128) NOT NULL
   ,FinancialYearId int NOT NULL
   ,FromMonthId int NOT NULL
   ,ToMonthId int NOT NULL
   ,StatusId int NOT NULL
   ,ExceptionsFileId int NULL
   ,ReconciliationId int NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PaymentCycle PRIMARY KEY (Id)
   ,CONSTRAINT FK_PaymentCycle_SystemCodeDetail_FinancialYearId FOREIGN KEY (FinancialYearId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_SystemCodeDetail_FromMonthId FOREIGN KEY (FromMonthId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_SystemCodeDetail_ToMonthId FOREIGN KEY (ToMonthId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_FileCreation_ExceptionsFileId FOREIGN KEY (ExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_Reconciliation_ReconciliationId FOREIGN KEY (ReconciliationId) REFERENCES Reconciliation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycle_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PaymentAccountActivityMonth(
	PaymentCycleId int NOT NULL
   ,MonthId int NOT NULL
   ,[Year] smallint NOT NULL
   ,CONSTRAINT PK_PaymentAccountActivityMonth PRIMARY KEY (PaymentCycleId,MonthId,[Year])
   ,CONSTRAINT FK_PaymentAccountActivityMonth_PaymentCycle_PaymentCycleId FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAccountActivityMonth_SystemCodeDetail_MonthId FOREIGN KEY (MonthId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
)
GO



CREATE TABLE PaymentCycleDetail(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,FileCreationId int NULL
   ,ExceptionsFileId int NULL
   ,InvalidIDActionsFileId int NULL
   ,DuplicateIDActionsFileId int NULL
   ,InvalidPaymentAccountActionsFileId int NULL
   ,InvalidPaymentCardActionsFileId int NULL
   ,IneligibleBeneficiaryActionsFileId int NULL
   ,SuspiciousPaymentActionsFileId int NULL
   ,PaymentStageId int NOT NULL
   ,EnrolledHhs int NULL
   ,PrePayrollBy int NULL
   ,PrePayrollOn datetime NULL
   ,PrepayrollApvBy int NULL
   ,PrepayrollApvOn datetime NULL -- SHOULD NOT MOVE PAST THIS STAGE IF THE REQUEST FOR FUNDS FOR ALL PROGRAMMES HAS NOT BEEN APPROVED!
   ,FundsRequestBy int NULL
   ,FundsRequestOn datetime NULL
   ,FundsRequestApvBy int NULL
   ,FundsRequestApvOn datetIme NULL
   ,PayrollBy int NULL
   ,PayrollOn datetime NULL
   ,PayrollVerBy int NULL
   ,PayrollVerOn datetime NULL
   ,PayrollApvBy int NULL
   ,PayrollApvOn datetime NULL
   ,PayrollExBy int NULL
   ,PayrollExOn datetime NULL
   ,PayrollExConfBy int NULL
   ,PayrollExConfOn datetime NULL
   ,PostPayrollBy int NULL
   ,PostPayrollOn datetime NULL
   ,PostPayrollApvby int NULL
   ,PostPayrollApvOn datetime NULL
   ,ReconciledBy int NULL
   ,ReconciledOn datetime NULL
   ,ReconciledApvBy int NULL
   ,ReconciledApvOn datetime NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_PaymentCycleDetail PRIMARY KEY (PaymentCycleId,ProgrammeId)
   ,CONSTRAINT FK_PaymentCycleDetail_PaymentCycle_PaymentCycleId FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_Programme FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_FileCreationId FOREIGN KEY (FileCreationId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_ExceptionsFileId FOREIGN KEY (ExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_InvalidIDActionsFileId FOREIGN KEY (InvalidIDActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_DuplicateIDActionsFileId FOREIGN KEY (DuplicateIDActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_InvalidPaymentAccountActionsFileId FOREIGN KEY (InvalidPaymentAccountActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_InvalidPaymentCardActionsFileId FOREIGN KEY (InvalidPaymentCardActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_IneligibleBeneficiaryActionsFileId FOREIGN KEY (IneligibleBeneficiaryActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_FileCreation_SuspiciousPaymentActionsFileId FOREIGN KEY (SuspiciousPaymentActionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_SystemCodeDetail_PaymentStageId FOREIGN KEY (PaymentStageId) REFERENCES SystemCodeDetail(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymentCycleDetail_User_PrePayrollBy FOREIGN KEY (PrePayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PrepayrollApvBy FOREIGN KEY (PrepayrollApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PayrollBy FOREIGN KEY (PayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PayrollVerBy FOREIGN KEY (PayrollVerBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PayrollApvBy FOREIGN KEY (PayrollApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PayrollExBy FOREIGN KEY (PayrollExBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PayrollExConfBy FOREIGN KEY (PayrollExConfBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PostPayrollBy FOREIGN KEY (PostPayrollBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_PostPayrollApvby FOREIGN KEY (PostPayrollApvby) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_ReconciledBy FOREIGN KEY (ReconciledBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_ReconciledApvBy FOREIGN KEY (ReconciledApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PaymentCycleDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PaymentEnrolmentGroup(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,EnrolmentGroupId int NOT NULL
   ,PaymentAmount money NOT NULL
   ,CONSTRAINT PK_PaymentEnrolmentGroup PRIMARY KEY (PaymentCycleId,ProgrammeId,EnrolmentGroupId)
   ,CONSTRAINT FK_PaymentEnrolmentGroup_PaymentCycleDetail_PaymentCycleId_ProgrammeId FOREIGN KEY (PaymentCycleId,ProgrammeId) REFERENCES PaymentCycleDetail(PaymentCycleId,ProgrammeId) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentEnrolemtnGroup_SystemCodeDetail_EnrolmentGroupId FOREIGN KEY (EnrolmentGroupId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
)
GO



CREATE TABLE PaymentAdjustment(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HHId int NOT NULL
   ,AdjustmentTypeId int NOT NULL
   ,AdjustmentAmount money NOT NULL
   ,AmountAdjusted money NOT NULL DEFAULT(0.00)
   ,RefNo nvarchar(50) NULL
   ,Notes nvarchar(128) NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_PaymentAdjustment PRIMARY KEY (PaymentCycleId,ProgrammeId,HHId,AdjustmentTypeId)
   ,CONSTRAINT FK_PaymentAdjustment_PaymentCycleDetail_PaymentCycleId_ProgrammeId FOREIGN KEY (PaymentCycleId,ProgrammeId) REFERENCES PaymentCycleDetail(PaymentCycleId,ProgrammeId) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAdjustment_Household FOREIGN KEY (HHId) REFERENCES Household(Id) ON UPDATE CASCADE
   ,CONSTRAINT FK_PaymentAdjustment_PaymentCycle_PaymentCycleId FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAdjustment_SystemCodeDetail_AdjustmentTypeId FOREIGN KEY (AdjustmentTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_PaymentAdjustment_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE FundsRequest(
	Id int NOT NULL IDENTITY(1,1)
   ,PaymentCycleId int NOT NULL
   ,FileCreationId int NULL
   ,CONSTRAINT PK_FundsRequest PRIMARY KEY (Id)
   ,CONSTRAINT FK_FundsRequest_PaymentCycle_PaymentCycleId FOREIGN KEY (PaymentCycleId) REFERENCES PaymentCycle(Id) ON UPDATE NO ACTION
   ,CONSTRAINT IX_FundsRequest_PaymentCycleId_PSPId UNIQUE NONCLUSTERED(Id,PaymentCycleId)
)
GO



CREATE TABLE FundsRequestDetail(
	FundsRequestId int NOT NULL
   ,PSPId tinyint NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,PayrollHhs int NOT NULL
   ,EntitlementAmount money NOT NULL
   ,OtherAmount money NOT NULL
   ,CommissionAmount money NOT NULL
   ,CONSTRAINT PK_FundsRequestDetail PRIMARY KEY (FundsRequestId,ProgrammeId,PSPId)
   ,CONSTRAINT FK_FundsRequestDetail_FundsRequestId FOREIGN KEY (FundsRequestId) REFERENCES FundsRequest(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_FundsRequestDetail_PSP FOREIGN KEY (PSPId) REFERENCES PSP(Id) ON UPDATE CASCADE ON DELETE NO ACTION
   ,CONSTRAINT FK_FundsRequestDetail_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE Prepayroll(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,BenePersonId int NOT NULL
   ,BeneFirstName varchar(50) NOT NULL
   ,BeneMiddleName varchar(50) NULL
   ,BeneSurname varchar(50) NOT NULL
   ,BeneDoB datetime NOT NULL
   ,BeneSexId int NOT NULL
   ,BeneNationalIDNo varchar(30) NULL
   ,PriReciCanReceivePayment bit
   ,CGPersonId int NULL
   ,CGFirstName varchar(50) NULL
   ,CGMiddleName varchar(50) NULL
   ,CGSurname varchar(50) NULL
   ,CGDoB datetime NULL
   ,CGSexId int NULL
   ,CGNationalIDNo varchar(30) NULL
   ,TotalHhMembers int NOT NULL
   ,HhStatusId int NOT NULL
   ,BeneAccountId int NULL
   ,BenePaymentCardId int NULL
   ,SubLocationId int NOT NULL
   ,PaymentZoneId smallint NOT NULL
   ,PaymentZoneCommAmt money NOT NULL
   ,ConseAccInactivity tinyint NOT NULL
   ,EntitlementAmount money NOT NULL DEFAULT(0)
   ,AdjustmentAmount money NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_Prepayroll PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_Prepayroll_PaymentCycleDetail_PaymentCycleId_ProgrammeId FOREIGN KEY (PaymentCycleId,ProgrammeId) REFERENCES PaymentCycleDetail(PaymentCycleId,ProgrammeId) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_Household_HHId FOREIGN KEY (HHId) REFERENCES Household(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_Person_BenePersonId FOREIGN KEY (BenePersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_SystemCodeDetail_BeneSexId FOREIGN KEY (BeneSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_Person2 FOREIGN KEY (CGPersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_SystemCodeDetail_CGSexId FOREIGN KEY (CGSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_SystemCodeDetail_HhStatusId FOREIGN KEY (HhStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_BeneficiaryAccount_BeneAccountId FOREIGN KEY (BeneAccountId) REFERENCES BeneficiaryAccount(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_BeneficiaryPaymentCard_BenePaymentCardId FOREIGN KEY (BenePaymentCardId) REFERENCES BeneficiaryPaymentCard(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_Prepayroll_PaymentZone_PaymentZoneId FOREIGN KEY (PaymentZoneId) REFERENCES PaymentZone(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollInvalidID(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,PersonId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollInvalidID PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
   ,CONSTRAINT FK_PrepayrollInvalidID_Prepayroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollInvalidID_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollInvalidID_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollInvalidID_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollInvalidID_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollDuplicateID(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,PersonId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollDuplicateID PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)
   ,CONSTRAINT FK_PrepayrollDuplicateID_Prepayroll_PaymentCycleId_ProgrammeId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollDuplicateID_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollDuplicateID_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollDuplicateID_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollDuplicateID_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE PrepayrollInvalidPaymentAccount(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollNoPaymentAccount PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_PrepayrollNoPaymentAccount_Prepayroll_PaymentCycleId_ProgrameId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollNoPaymentAccount_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollNoPaymentAccount_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollNoPaymentAccount_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PrepayrollNoPaymentAccount_PaymentCycleId_HhId UNIQUE NONCLUSTERED(PaymentCycleId,HhId)
)
GO



CREATE TABLE PrepayrollInvalidPaymentCard(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollNoPaymentCard PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_PrepayrollNoPaymentCard_Prepayroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollNoPaymentCard_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollNoPaymentCard_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollNoPaymentCard_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PrepayrollNoPaymentCard_PaymentCycleId_HhId UNIQUE NONCLUSTERED(PaymentCycleId,HhId)
)
GO


CREATE TABLE PrepayrollIneligible(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollIneligible PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_PrepayrollIneligible_Prepayroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollIneligible_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollIneligible_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollIneligible_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PrepayrollIneligible_PaymentCycleId_HhId_ExceptionTypeId UNIQUE NONCLUSTERED(PaymentCycleId,HhId,ExceptionTypeId)
)
GO



CREATE TABLE PrepayrollSuspicious(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,ExceptionTypeId int NOT NULL
   ,Actioned bit NOT NULL DEFAULT(0)
   ,Notes varchar(128) NULL
   ,ActionedBy int NULL
   ,ActionedOn datetime NULL
   ,ActionedApvBy int NULL
   ,ActionedApvOn datetime NULL
   ,CONSTRAINT PK_PrepayrollSuspicious PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_PrepayrollSuspicious_Prepayroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollSuspicious_SystemCodeDetail_ExceptionTypeId FOREIGN KEY (ExceptionTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_PrepayrollSuspicious_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_PrepayrollSuspicious_User_ActionedApvBy FOREIGN KEY (ActionedApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_PrepayrollSuspicious_PaymentCycleId_HhId UNIQUE NONCLUSTERED(PaymentCycleId,HhId)
)
GO


CREATE TABLE Payroll(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,PaymentAmount money NOT NULL
   ,CONSTRAINT PK_Payroll PRIMARY KEY (PaymentCycleId,ProgrammeId,HhId)
   ,CONSTRAINT FK_Payroll_Prepayroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Prepayroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
)
GO



CREATE TABLE Payment(
	PaymentCycleId int NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,HhId int NOT NULL
   ,WasTrxSuccessful bit NOT NULL
   ,TrxAmount money NULL
   ,TrxNo nvarchar(50) NULL
   ,TrxDate datetime NULL
   ,TrxNarration nvarchar(128) NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,CONSTRAINT PK_Payment PRIMARY KEY (PaymentCycleId,HhId)
   ,CONSTRAINT FK_Payment_Payroll_PaymentCycleId_ProgrammeId_HhId FOREIGN KEY (PaymentCycleId,ProgrammeId,HhId) REFERENCES Payroll(PaymentCycleId,ProgrammeId,HhId) ON UPDATE CASCADE ON DELETE CASCADE
   ,CONSTRAINT FK_Payment_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE CASCADE ON DELETE NO ACTION
)
GO



CREATE TABLE BeneAccountMonthlyActivity(
	Id int NOT NULL IDENTITY(1,1)
   ,MonthId int NOT NULL
   ,[Year] smallint NOT NULL
   ,StatusId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int
   ,ModifiedOn datetime
   ,ApvBy int
   ,ApvOn datetime
   ,ClosedBy int
   ,ClosedOn datetime
   ,CONSTRAINT PK_BeneAccountMonthlyActivity PRIMARY KEY (Id)
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_SystemCodeDetail_MonthId FOREIGN KEY (MonthId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivity_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE BeneAccountMonthlyActivityDetail(
	BeneAccountMonthlyActivityId int NOT NULL
   ,BeneAccountId int NOT NULL
   ,HadUniqueWdl bit NOT NULL
   ,UniqueWdlTrxNo nvarchar(50)
   ,UniqueWdlDate datetime
   ,HadBeneBiosVerified bit NOT NULL
   ,IsDormant bit NOT NULL
   ,DormancyDate datetime NULL
   ,IsDueForClawback bit NOT NULL
   ,ClawbackAmount money NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int
   ,ModifiedOn datetime
   ,CONSTRAINT PK_BeneAccountMonthlyActivityDetail PRIMARY KEY (BeneAccountMonthlyActivityId,BeneAccountId)
   ,CONSTRAINT FK_BeneAccountMonthlyActivityDetail_BeneAccountMonthlyActivity_MonthlyActivityId FOREIGN KEY (BeneAccountMonthlyActivityId) REFERENCES BeneAccountMonthlyActivity(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivityDetail_BeneficiaryAccount_BeneAccountId FOREIGN KEY (BeneAccountId) REFERENCES BeneficiaryAccount(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivityDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_BeneAccountMonthlyActivityDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO








-------------TEMP FOR PROCESSING
IF OBJECT_ID('temp_Exceptions') IS NOT NULL	DROP TABLE temp_Exceptions;
GO
CREATE TABLE temp_Exceptions(
	PaymentCycleId int
	,PaymentCycle varchar(128) NOT NULL
	,ProgrammeId int NOT NULL
	,Programme nvarchar(20) NOT NULL
	,EnrolmentNo int NOT NULL
	,ProgrammeNo varchar(50) NOT NULL
	,BeneFirstName varchar(50) NOT NULL
	,BeneMiddleName varchar(50) NULL
	,BeneSurname varchar(50) NOT NULL
	,BeneDoB datetime NOT NULL
	,BeneSex varchar(20) NOT NULL
	,BeneNationalIDNo varchar(30) NULL
	,PriReciCanReceivePayment bit NOT NULL
	,IsInvalidBene bit NOT NULL DEFAULT(0)
	,IsDuplicateBene bit NOT NULL DEFAULT(0)
	,IsIneligibleBene bit NOT NULL DEFAULT(0)
	,CGFirstName varchar(50) NULL
	,CGMiddleName varchar(50) NULL
	,CGSurname varchar(50) NULL
	,CGDoB datetime NULL
	,CGSex varchar(20) NULL
	,CGNationalIDNo varchar(30) NULL
	,IsInvalidCG bit NOT NULL DEFAULT(0)
	,IsDuplicateCG bit NOT NULL DEFAULT(0)
	,IsIneligibleCG bit NOT NULL DEFAULT(0)
	,HhStatus varchar(20) NOT NULL
	,IsIneligibleHh bit NOT NULL DEFAULT(0)
	,AccountNumber varchar(50) NULL
	,IsInvalidAccount bit NOT NULL DEFAULT(0)
	,IsDormantAccount bit NOT NULL DEFAULT(0)
	,PaymentCardNumber varchar(50) NULL
	,IsInvalidPaymentCard bit NOT NULL DEFAULT(0)
	,PaymentZone varchar(30) NOT NULL
	,PaymentZoneCommAmt money NOT NULL
	,ConseAccInactivity tinyint NOT NULL DEFAULT(0)
	,EntitlementAmount money NOT NULL DEFAULT(0)
	,AdjustmentAmount money NOT NULL DEFAULT(0)
	,IsSuspiciousAmount bit NOT NULL DEFAULT(0)
	,SubLocation varchar(30)
	,Location varchar(30)
	,Division varchar(30)
	,District varchar(30)
	,County varchar(30)
	,Constituency varchar(30)
	,WasTrxSuccessful bit
	,TrxNarration nvarchar(128)
	,CONSTRAINT PK_temp_Exceptions PRIMARY KEY (PaymentCycleId,EnrolmentNo)
)
GO



IF NOT OBJECT_ID('vw_temp_Exceptions') IS NULL	DROP VIEW vw_temp_Exceptions;
GO
IF NOT OBJECT_ID('vw_temp_PrepayrollExceptions') IS NULL	DROP VIEW vw_temp_PrepayrollExceptions;
GO
CREATE VIEW vw_temp_PrepayrollExceptions
AS
	SELECT PaymentCycle,Programme,EnrolmentNo,ProgrammeNo
		,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBene,IsIneligibleBene
		,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCG,IsIneligibleCG
		,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZone,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount
		,SubLocation,Location,Division,District,County,Constituency
	FROM temp_Exceptions
GO



IF NOT OBJECT_ID('temp_ExceptionTypes') IS NULL DROP TABLE temp_ExceptionTypes
GO
CREATE TABLE temp_ExceptionTypes(
	PaymentCycleId int NOT NULL
	,ProgrammeId int NOT NULL
	,ExceptionTypeId int NOT NULL
	,ExceptionTypeCode varchar(20) NOT NULL
	,CONSTRAINT PK_temp_ExceptionTypes PRIMARY KEY (PaymentCycleId,ProgrammeId,ExceptionTypeId)
)
GO



IF NOT OBJECT_ID('temp_EnrolmentGroups') IS NULL DROP TABLE temp_EnrolmentGroups
GO
CREATE TABLE temp_EnrolmentGroups(
	PaymentCycleId int NOT NULL
	,ProgrammeId int NOT NULL
	,EnrolmentGroupId int NOT NULL
	,CONSTRAINT PK_temp_EnrolmentGroups PRIMARY KEY (PaymentCycleId,ProgrammeId,EnrolmentGroupId)
)
GO



IF NOT OBJECT_ID('temp_ExceptionActions') IS NULL DROP TABLE temp_ExceptionActions
GO
CREATE TABLE temp_ExceptionActions(
	PaymentCycleId int NOT NULL
	,ProgrammeId int NOT NULL
	,HhId int NOT NULL
	,PersonId int NULL
)
GO



IF OBJECT_ID('temp_Payroll') IS NOT NULL	DROP TABLE temp_Payroll;
CREATE TABLE temp_Payroll(
	PaymentCycleId int NOT NULL
	,ProgrammeId int NOT NULL
	,HhId int NOT NULL
	,PaymentAmount money NOT NULL
)
GO


IF OBJECT_ID('temp_PayrollFile') IS NOT NULL	DROP TABLE temp_PayrollFile
GO
CREATE TABLE temp_PayrollFile(
	PaymentCycleId int NOT NULL
	,PaymentCycle varchar(128) NOT NULL
	,EnrolmentNo int NOT NULL
	,ProgrammeNo varchar(50) NOT NULL
	,BankId int NOT NULL
	,BankCode nvarchar(20) NOT NULL
	,BranchCode nvarchar(20) NOT NULL
	,AccountNo varchar(50) NOT NULL
	,AccountName varchar(100) NOT NULL
	,Amount money NOT NULL
	,BeneficiaryIDNo varchar(30)
	,BeneficiaryContact nvarchar(20)
	,CaregiverIDNo varchar(30)
	,CaregiverContact nvarchar(20)
	,SubLocation varchar(30)
	,Location varchar(30)
	,Division varchar(30)
	,District varchar(30)
	,County varchar(30)
	,Constituency varchar(30)
	)
GO



IF NOT OBJECT_ID('vw_temp_PayrollMobilization') IS NULL	DROP VIEW vw_temp_PayrollMobilization;
GO
CREATE VIEW vw_temp_PayrollMobilization
AS
	SELECT PaymentCycle,EnrolmentNo,ProgrammeNo,BankCode AS BankName,BranchCode AS BranchName,AccountNo,AccountName,Amount,BeneficiaryIDNo,CaregiverIDNo,BeneficiaryContact AS PrimaryContact,CaregiverContact AS AlternativeContact,SubLocation,Location,Division,District,County,Constituency
	FROM temp_PayrollFile
GO



IF OBJECT_ID('temp_PSPPayrollFile') IS NOT NULL	DROP TABLE temp_PSPPayrollFile
GO
CREATE TABLE temp_PSPPayrollFile(
	PaymentCycleId int NOT NULL
	,EnrolmentNo int NOT NULL
	,BankCode nvarchar(20) NOT NULL
	,BranchCode nvarchar(20) NOT NULL
	,AccountNo varchar(50) NOT NULL
	,AccountName varchar(100) NOT NULL
	,Amount money NOT NULL
	)
GO



IF NOT OBJECT_ID('vw_temp_PostpayrollExceptions') IS NULL	DROP VIEW vw_temp_PostpayrollExceptions;
GO
CREATE VIEW vw_temp_PostpayrollExceptions
AS
	SELECT PaymentCycle,Programme,EnrolmentNo,ProgrammeNo
		,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBene,IsIneligibleBene
		,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCG,IsIneligibleCG
		,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZone,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount
		,WasTrxSuccessful,TrxNarration
		,SubLocation,Location,Division,DistricFt,County,Constituency
	FROM temp_Exceptions
GO

