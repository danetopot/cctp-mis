﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF NOT OBJECT_ID('AddEditSystemCode') IS NULL	
DROP PROC AddEditSystemCode
GO
CREATE PROC AddEditSystemCode
	@Id int=NULL
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCode WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCode T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCode(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END

GO



IF NOT OBJECT_ID('AddEditSystemCodeDetail') IS NULL	
DROP PROC AddEditSystemCodeDetail
GO


CREATE PROC AddEditSystemCodeDetail

	@Id int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(20)=NULL
   ,@DetailCode varchar(20)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=Id
		FROM SystemCode
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SystemCodeId AND Code=@DetailCode AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.Code=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetail T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('SystemCodeDetail')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		--EXEC GetSystemCodeDetails	
	END
END

GO



