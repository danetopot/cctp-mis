

IF NOT OBJECT_ID('SystemCodeDetail') IS NULL	DROP TABLE SystemCodeDetail
GO

IF NOT OBJECT_ID('SystemCode') IS NULL	DROP TABLE SystemCode
GO

CREATE TABLE SystemCode(
  	Id int NOT NULL IDENTITY(1,1)
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,IsUserMaintained bit NOT NULL DEFAULT(0)
   ,CONSTRAINT PK_SystemCode PRIMARY KEY (Id)
   ,CONSTRAINT IX_SystemCode_Code UNIQUE NONCLUSTERED (Code)
)
GO


 

CREATE TABLE SystemCodeDetail(
	Id int NOT NULL IDENTITY(1,1)
   ,SystemCodeId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,OrderNo tinyint NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_SystemCodeDetail PRIMARY KEY (Id)
   ,CONSTRAINT FK_SystemCodeDetail_SystemCode_SystemCodeId FOREIGN KEY (SystemCodeId) REFERENCES SystemCode(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_SystemCodeDetail_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_SystemCodeDetail_SystemCodeId_Code UNIQUE NONCLUSTERED (SystemCodeId,Code)
)
GO


/*
Registration and Targeting Tables
*/
IF NOT OBJECT_ID('HouseholdReg') IS NULL	
DROP TABLE HouseholdReg
GO
CREATE TABLE HouseholdReg(
	 Id INT NOT NULL IDENTITY(1,1)
   ,SubLocationId INT NOT NULL
   ,ProgrammeId tinyint NOT NULL
   ,Village varchar(50) NULL
   ,StatusId INT NOT NULL
   ,DwellingStartDate DATETIME NOT NULL
   ,NearestReligiousBuilding   varchar(50) NULL
   ,NearestSchool   varchar(50) NULL
   ,PhysicalAddress varchar(50) NULL
   ,CaptureStartDate datetime NOT NULL
   ,CaptureEndDate datetime NOT NULL
   ,Longitude float NULL
   ,Latitude float NULL
   ,Elevation float NULL
   ,TargetPlanId INT NOT NULL
   ,HouseholdRegAcceptId INT NULL
   ,HabitableRooms INT NOT NULL DEFAULT(1)
   ,SerialNo varchar(36) not null
   ,DeviceId varchar(36) NOT NULL
   ,VersionId varchar(10) not null
   ,SyncDate DATETIME NULL
   ,CreatedBy INT NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy INT NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_HouseholdReg PRIMARY KEY (Id)
   ,CONSTRAINT FK_HouseholdReg_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdReg_SystemCodeDetail_RegGroupId FOREIGN KEY (RegGroupId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdReg_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdReg_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    
)
GO

IF NOT OBJECT_ID('HouseholdRegCharacteristic') IS NULL	
DROP TABLE HouseholdRegCharacteristic
GO
CREATE TABLE HouseholdRegCharacteristic(
  	HouseholdRegId INT NOT NULL
   ,IsOwnHouse bit NOT NULL
   ,TenureStatusId INT not null
   ,RoofMaterialId INT NOT NULL
   ,WallMaterialId INT NOT NULL
   ,FloorMaterialId INT NOT NULL
   ,UnitRiskId INT NOT NULL
   ,WaterSourceId INT NOT NULL
   ,ToiletTypeId INT NOT NULL
   ,CookingFuelSourceId INT NOT NULL
   ,LightingSourceId INT NOT NULL
   ,HouseholdConditionId INT not null
   ,LiveBirths INT not null
   ,Deaths INT not null
   ,HasSkippedMeal  BIT NOT NULL
   ,IsRecievingNSNPBenefit BIT NOT NULL
   ,IsReceivingOtherBenefit BIT NOT NULL
   ,OtherProgrammes VARCHAR(100) NULL
   ,OtherProgrammesBenefitTypeId INT NULL
   ,OtherProgrammesBenefitAmount MONEY NULL
   ,OtherProgrammesInKindBenefit VARCHAR(100) NULL
   ,CONSTRAINT PK_HouseholdRegCharacteristic PRIMARY KEY (HouseholdRegId)
   ,CONSTRAINT FK_HouseholdRegCharacteristic_HouseholdReg_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_TenureStatusId FOREIGN KEY (TenureStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_RoofMaterialId FOREIGN KEY (RoofMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_WallMaterialId FOREIGN KEY (WallMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_FloorMaterialId FOREIGN KEY (FloorMaterialId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_UnitRiskId FOREIGN KEY (UnitRiskId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_WaterSourceId FOREIGN KEY (WaterSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_ToiletTypeId FOREIGN KEY (ToiletTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_CookingFuelSourceId FOREIGN KEY (CookingFuelSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_LightingSourceId FOREIGN KEY (LightingSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_HouseholdConditionId FOREIGN KEY (HouseholdConditionId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegCharacteristic_SystemCodeDetail_OtherProgrammesBenefitTypeId FOREIGN KEY (OtherProgrammesBenefitTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      
)
GO
-- To store both Items and Livestocks.(AssetTypeId) For Items we store HasItem, for Livestock we store the nummber. 
IF NOT OBJECT_ID('HouseholdRegAsset')	IS NULL	
DROP TABLE HouseholdRegAsset
GO
CREATE TABLE HouseholdRegAsset(
      HouseholdRegId INT NOT NULL
      ,AssetTypeId INT NOT NULL
      ,AssetId INT NOT NULL
      ,HasItem bit  NULL DEFAULT (0)
      ,ItemCount TINYINT NOT NULL DEFAULT (0)
      ,CONSTRAINT PK_HouseholdRegAsset PRIMARY KEY (HouseholdRegId,AssetTypeId,AssetId)
      ,CONSTRAINT FK_HouseholdRegAsset_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      ,CONSTRAINT FK_HouseholdRegAsset_SystemCodeDetail_AssetId FOREIGN KEY (AssetId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      ,CONSTRAINT FK_HouseholdRegAsset_SystemCodeDetail_AssetTypeId FOREIGN KEY (AssetTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO


IF NOT OBJECT_ID('HouseholdRegOtherProgramme') IS NULL
	DROP TABLE HouseholdRegOtherProgramme
GO
CREATE TABLE HouseholdRegOtherProgramme(
	  HouseholdRegId INT NOT NULL
   ,OtherProgrammeId INT NOT NULL
   ,CONSTRAINT PK_HouseholdRegOtherProgramme PRIMARY KEY (HouseholdRegId,OtherProgrammeId)
   ,CONSTRAINT FK_HouseholdRegOtherProgramme_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_HouseholdRegOtherProgramme_SystemCodeDetail_OtherProgrammeId FOREIGN KEY (OtherProgrammeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

IF NOT OBJECT_ID('HouseholdRegMember') IS NULL	
DROP TABLE HouseholdRegMember
GO
CREATE TABLE HouseholdRegMember(
     Id INT NOT NULL IDENTITY(1,1)
    ,HouseholdRegId INT NOT NULL
    ,FirstName varchar(50) NOT NULL
    ,MiddleName varchar(50) NULL
    ,Surname varchar(50) NOT NULL
    ,IdentificationTypeId INT NOT NULL
    ,BirthCertNo varchar(30)
    ,NationalIDNo varchar(30) NULL
    ,RelationshipId INT NOT NULL 
    ,SexId INT NOT NULL
    ,DoB datetime NOT NULL
    ,MaritalStatusId INT NOT NULL
    ,IsSpouseInHousehold bit not NULL
    ,SpouseId INT NULL
    ,OrphanhoodTypeId INT NOT NULL
    ,FatherAliveId INT not NULL
    ,MotherAliveId INT not NULL
    ,IllnessTypeId INT NOT NULL
    ,DisabilityTypeId INT NOT NULL
    ,DisabilityCaregiverId INT not NULL
    ,MobileNumber1 nvarchar(20) 
    ,MobileNumber2 nvarchar(20)
    ,ExternalMember bit NOT NULL DEFAULT(0)
    ,MainCaregiverId INT NULL
    ,EducationAttendanceId INT not  NULL
    ,EducationLevelId INT not NULL
    ,OccupationTypeId INT NOT NULL
    ,FormalJobTypeId INT NOT NULL
     ,CONSTRAINT PK_HouseholdRegMember PRIMARY KEY (Id)
    ,CONSTRAINT FK_HouseholdRegMember_HouseholdReg_HouseholdRegId FOREIGN KEY (HouseholdRegId) REFERENCES HouseholdReg(Id) ON UPDATE NO ACTION ON DELETE NO ACTION 
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_IdentificationTypeId FOREIGN KEY (IdentificationTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_RelationshipId FOREIGN KEY (RelationshipId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_SexId FOREIGN KEY (SexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_MaritalStatusId FOREIGN KEY (MaritalStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_OrphanhoodTypeId FOREIGN KEY (OrphanhoodTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_FatherAliveId FOREIGN KEY (FatherAliveId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_MotherAliveId FOREIGN KEY (MotherAliveId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_IllnessTypeId FOREIGN KEY (IllnessTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_DisabilityTypeId FOREIGN KEY (DisabilityTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_DisabilityCaregiverId FOREIGN KEY (DisabilityCaregiverId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_EducationAttendanceId FOREIGN KEY (EducationAttendanceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_EducationLevelId FOREIGN KEY (EducationLevelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_OccupationTypeId FOREIGN KEY (OccupationTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_HouseholdRegMember_SystemCodeDetail_FormalJobTypeId FOREIGN KEY (FormalJobTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

IF NOT OBJECT_ID('HouseholdRegMemberDisability') IS NULL	
DROP TABLE HouseholdRegMemberDisability
GO
CREATE TABLE HouseholdRegMemberDisability(
     HouseholdRegMemberId INT NOT NULL
    ,DisabilityId INT NOT NULL
     ,CONSTRAINT PK_HouseholdRegMemberDisability PRIMARY KEY (HouseholdRegMemberId,DisabilityId)
    ,CONSTRAINT FK_HouseholdRegMemberDisability_HouseholdRegMember_HouseholdRegMemberId FOREIGN KEY (HouseholdRegMemberId) REFERENCES HouseholdRegMember(Id) ON UPDATE NO ACTION ON DELETE NO ACTION 
    ,CONSTRAINT FK_HouseholdRegMemberDisability_SystemCodeDetail_DisabilityId FOREIGN KEY (DisabilityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION

)

/*
PMT  Tables
*/
/*
IF NOT OBJECT_ID('PMTModel') IS NULL	
DROP TABLE PMTModel
GO
CREATE TABLE PMTModel(
	Id INT NOT NULL IDENTITY(1,1)
  ,[Name] varchar
)
GO
*/
/* 
The PMT Constant can ride on SystemCodeDetail Table
IF NOT OBJECT_ID('PMTConstant') IS NULL	DROP TABLE PMTConstant
GO
CREATE TABLE PMTConstant(
  IN 
)
GO
*/


IF NOT OBJECT_ID('PMTCutOff') IS NULL	
DROP TABLE PMTCutOff
GO
CREATE TABLE PMTCutOff(
       PMTModelId INT NOT NULL
      ,LocalityId INT NOT NULL
      ,PovertyLevelId INT NOT NULL
      ,USD FLOAT NOT NULL
      ,KES FLOAT NOT NULL
      ,LogScale FLOAT NOT NULL
      ,CONSTRAINT FK_PMTCutOff_SystemCodeDetail_PmtModelId FOREIGN KEY (PmtModelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      ,CONSTRAINT FK_PMTCutOff_SystemCodeDetail_LocalityId FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
      ,CONSTRAINT FK_PMTCutOff_SystemCodeDetail_PovertyLevelId FOREIGN KEY (PovertyLevelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO


IF NOT OBJECT_ID('PMTWeight') IS NULL	DROP TABLE PMTWeight
GO
CREATE TABLE PMTWeight(
     ConstantId INT NOT NULL
    ,LocalityId INT NOT NULL
    ,[Weight] FLOAT NOT NULL
    ,CONSTRAINT FK_PMTWeight_SystemCodeDetail_ConstantId FOREIGN KEY (ConstantId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_PMTWeight_SystemCodeDetail_LocalityId FOREIGN KEY (LocalityId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO


IF NOT OBJECT_ID('PMTScore') IS NULL	DROP TABLE PMTScore
GO
CREATE TABLE PMTScore(
 HouseholdRegId INT NOT NULL
,ConstantId INT NOT NULL
,Score FLOAT NOT NULL
,CreatedBy INT NOT NULL
,CreatedOn DATETIME NOT NULL
,CONSTRAINT FK_PMTScore_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    
)
GO
