
IF NOT OBJECT_ID('ComplaintNote') IS NULL	
DROP TABLE ComplaintNote
GO
IF NOT OBJECT_ID('ComplaintDocument') IS NULL	
DROP TABLE ComplaintDocument
GO
IF NOT OBJECT_ID('Complaint') IS NULL	
DROP TABLE Complaint
GO


CREATE TABLE  Complaint(
	Id int IDENTITY(1,1) NOT NULL,
	ProgrammeId tinyint NOT NULL,
	HhEnrolmentId int NOT NULL,
	ComplaintTypeId int NULL,
	SerialNo int NULL,
	ReportedByName varchar(50) NULL,
	ReportedByIdNo varchar(50) NULL,
	ReportedByPhone varchar(50) NULL,
	ReportedBySexId int NULL,
	ReportedByTypeId int NOT NULL, 
	ComplaintDate datetime NULL,
	CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
	CreatedBy int NULL,
	ModifiedOn datetime NULL,
	ModifiedBy int NULL,
	ApvBy int NULL,
	ApvOn datetime NULL,
	ResolvedBy int NULL,
	ResolveOn datetime NULL,
	ClosedBy int NULL,
	ClosedOn datetime NULL,
	ComplaintStatusId int NULL,
	ComplaintLevelId int NULL,   
	ReceivedBy nvarchar(50) NULL,
	Designation nvarchar(50) NULL,
	ChangeSourceId  int NULL
	,CONSTRAINT PK_Complaint PRIMARY KEY (Id)
	,CONSTRAINT FK_Complaint_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ResolvedBy FOREIGN KEY (ResolvedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_HouseholdEnrolment_HhEnrolmentId FOREIGN KEY (HhEnrolmentId) REFERENCES HouseholdEnrolment(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintTypeId FOREIGN KEY (ComplaintTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintStatusId FOREIGN KEY (ComplaintStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintLevelId FOREIGN KEY (ComplaintLevelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ReportedByTypeId FOREIGN KEY (ReportedByTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ChangeSourceId FOREIGN KEY (ChangeSourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	 )
GO

CREATE TABLE  ComplaintDocument(
	Id int IDENTITY(1,1) NOT NULL
	,ComplaintId int NOT NULL
	,CreatedById int NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,FileName VARCHAR(300) NOT NULL
	,Note VARCHAR(300) NOT NULL
	,CategoryId int NOT NULL
	,CONSTRAINT PK_ComplaintDocument PRIMARY KEY (Id)
	,CONSTRAINT FK_ComplaintDocument_SystemCodeDetail_ComplaintId FOREIGN KEY (ComplaintId) REFERENCES Complaint(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintDocument_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FKComplaintDocument_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE  ComplaintNote(
	Id int IDENTITY(1,1) NOT NULL
	,ComplaintId int NOT NULL
	,CreatedById int NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,Note VARCHAR(300) NOT NULL
	,EntryDate datetime NULL
	,CategoryId int NOT NULL
	,CONSTRAINT PK_ComplaintNote PRIMARY KEY (Id)
	,CONSTRAINT FK_ComplaintNote_SystemCodeDetail_ComplaintId FOREIGN KEY (ComplaintId) REFERENCES Complaint(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintNote_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintNote_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO