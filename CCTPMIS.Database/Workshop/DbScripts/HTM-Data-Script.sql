---------------PAYMENT/PAYROLL STAGE-------------
SET @Code='Tenure Status'
SET @Description='TENURE status of the dwelling unit and/or surrounding terrain/land'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Wall Material'
SET @Description='Wall Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Floor Material'
SET @Description='Floor Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Roof Material'
SET @Description='Roof Material'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Toilet Type'
SET @Description='Toilet Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Water Source'
SET @Description='Water Source'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Lighting Source'
SET @Description='Lighting Source'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Cooking Fuel'
SET @Description='Cooking Fuel'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Dwelling Unit Risk'
SET @Description='Risks of the Dwelling unit'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Assets'
SET @Description='Household Assets'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Asset Types'
SET @Description='Household Asset Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Boolean Options'
SET @Description='Boolean Options'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Conditions'
SET @Description='Household Conditions'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Household Option'
SET @Description='Household Conditions'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Other SP Programme'
SET @Description='Other Social Assistance Programme'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='SP Benefit Type'
SET @Description='Social Protection Benefit Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

---------------------HTM MEMBERS OPTIONS---------------------------------------------

SET @Code='Identification Document Type'
SET @Description='Various Identification Document Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Relationship'
SET @Description='How a member relates to Household Head'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Marital Status'
SET @Description='Marital statuses'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Disability'
SET @Description='Various kinds of ailements'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Education Attendance'
SET @Description='Highest levels of Education Attendance '
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SET @Code='Education Level'
SET @Description='Various highest levels of education'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SET @Code='Work Type'
SET @Description='A persons Work Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SET @Code='Interview Type'
SET @Description='Interview Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SET @Code='Interview Result'
SET @Description='Result of the interview'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;



SELECT @Id = NULL
------------------------ TENURE STATUS ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code='Tenure Status'

IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Purchased'
    SET @Description='Purchased'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Constructed'
    SET @Description='Constructed'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Inherited'
    SET @Description='Inherited'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Government'
    SET @Description='Government'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Local Authority'
    SET @Description='Local Authority'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Parastatal'
    SET @Description='Parastatal'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Private Company'
    SET @Description='Private Company'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Individual'
    SET @Description='Individual'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Faith based organization/NGO'
    SET @Description='Faith based organization/NGO'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other:'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END
SELECT @Id = NULL
------------------------ Wall Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code='Wall Material'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Stone'
    SET @Description='Stone'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Brick/Block'
    SET @Description='Brick/Block'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Mud/Wood'
    SET @Description='Mud/Wood'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Mud/Cement'
    SET @Description='Mud/Cement'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Wood only'
    SET @Description='Wood only'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Corrugated iron sheets'
    SET @Description='Corrugated iron sheets'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Grass/Reeds'
    SET @Description='Grass/Reeds'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Tin'
    SET @Description='Tin'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END

SELECT @Id = NULL
------------------------ Floor Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Floor Material'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Cement'
    SET @Description='Cement'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Tiles'
    SET @Description='Tiles'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Wood'
    SET @Description='Wood'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Earth'
    SET @Description='Earth'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL
------------------------ Roof Material ----------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code = 'Roof Material'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Corrugated iron sheets'
    SET @Description='Corrugated iron sheets'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Tiles'
    SET @Description='Tiles'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Concrete'
    SET @Description='Concrete'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Asbestos sheets'
    SET @Description='Asbestos sheets'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Grass'
    SET @Description='Grass'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Makuti'
    SET @Description='Makuti'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Tin'
    SET @Description='Tin'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Mud/dung'
    SET @Description='Mud/dung'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END


SELECT @Id = NULL
------------------------ Toilet Type ----------------------------------------------------------


SELECT @Id=Id
FROM SystemCode
WHERE Code ='Toilet Type'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Main sewer'
    SET @Description='Main sewer'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Septic tank'
    SET @Description='Septic tank'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



    SET @DetailCode='Cess pool'
    SET @Description='Cess pool'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='VIP pit latrine'
    SET @Description='VIP pit latrine'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Pit latrine covered'
    SET @Description='Pit latrine covered'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Pit latrine uncovered'
    SET @Description='Pit latrine uncovered'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Bucket Latrine'
    SET @Description='Bucket Latrine'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Bush'
    SET @Description='Bush'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Water Source'
IF(ISNULL(@Id,0)>0)
BEGIN


    SET @DetailCode='Pond'
    SET @Description='Pond'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Dam'
    SET @Description='Dam'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Lake'
    SET @Description='Lake'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Stream/River'
    SET @Description='Stream/River'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Protected spring water'
    SET @Description='Protected spring water'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Unprotected spring water'
    SET @Description='Unprotected spring water'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Protected well'
    SET @Description='Protected well'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Unprotected well'
    SET @Description='Unprotected well'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Borehole'
    SET @Description='Borehole'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Piped into dwelling'
    SET @Description='Piped into dwelling'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Piped'
    SET @Description='Piped'
    SET @OrderNo=11
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Jabia'
    SET @Description='Jabia'
    SET @OrderNo=12
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Rain/Harvested'
    SET @Description='Rain/Harvested'
    SET @OrderNo=13
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Water vendor'
    SET @Description='Water vendor'
    SET @OrderNo=14
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=15
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




END

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Lighting Source'
IF(ISNULL(@Id,0)>0)
BEGIN


    SET @DetailCode='Electricity'
    SET @Description='Electricity'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Pressure lamp'
    SET @Description='Pressure lamp'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Lantern'
    SET @Description='Lantern'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Tin lamp'
    SET @Description='Tin lamp'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Gas lamp'
    SET @Description='Gas lamp'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Fuel wood / Firewood'
    SET @Description='Fuel wood / Firewood'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Solar'
    SET @Description='Solar'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Cooking Fuel'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Electricity'
    SET @Description='Electricity'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Paraffin'
    SET @Description='Paraffin'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='LPG (Liquefied Petroleum Gas)'
    SET @Description='LPG (Liquefied Petroleum Gas)'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Biogas'
    SET @Description='Biogas'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Firewood'
    SET @Description='Firewood'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Charcoal'
    SET @Description='Charcoal'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Solar'
    SET @Description='Solar'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Dwelling Unit Risk'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='None'
    SET @Description='None'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Landslide'
    SET @Description='Landslide'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Flooding'
    SET @Description='Flooding'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Fire'
    SET @Description='Fire'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Household Assets'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Television'
    SET @Description='Television'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Motorcycle'
    SET @Description='Motorcycle'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Tuk-Tuk'
    SET @Description='Tuk-Tuk'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Refrigerator'
    SET @Description='Refrigerator'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Car'
    SET @Description='Car'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Mobile phone'
    SET @Description='Mobile phone'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Bicycle'
    SET @Description='Bicycle'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Exotic cattle'
    SET @Description='Exotic cattle'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Indigenous cattle'
    SET @Description='Indigenous cattle'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Sheep'
    SET @Description='Sheep'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Goats'
    SET @Description='Goats'
    SET @OrderNo=11
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Camels'
    SET @Description='Camels'
    SET @OrderNo=12
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Donkeys'
    SET @Description='Donkeys'
    SET @OrderNo=13
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Pigs'
    SET @Description='Pigs'
    SET @OrderNo=14
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Chicken'
    SET @Description='Chicken'
    SET @OrderNo=15
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Boolean Options'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Yes'
    SET @Description='Yes'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='No'
    SET @Description='No'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




END

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Household Conditions'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Poor'
    SET @Description='Poor'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Fair'
    SET @Description='Fair'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Good'
    SET @Description='Good'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Very Good'
    SET @Description='Very Good'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Household Option'
IF(ISNULL(@Id,0)>0)
BEGIN

    SELECT @Id = NULL
    SET @DetailCode='Yes'
    SET @Description='Yes'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='No'
    SET @Description='No'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Don''t Know'
    SET @Description='Don''t Know'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Identification Document Type'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='National ID card'
    SET @Description='National ID card'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Registration of birth'
    SET @Description='Registration of birth'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Passport'
    SET @Description='Passport'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='None'
    SET @Description='None'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



END


SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code ='Other SP Programme'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='CT-OVC'
    SET @Description='CT-OVC'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='HSNP'
    SET @Description='HSNP'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='PWSD-CT'
    SET @Description='PWSD-CT'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='OPCT'
    SET @Description='OPCT'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='All'
    SET @Description='All'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='None'
    SET @Description='None'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code ='SP Benefit Type'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Cash'
    SET @Description='Cash'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='In-kind'
    SET @Description='In-kind'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




END

SELECT @Id = NULL

SELECT @Id=Id
FROM SystemCode
WHERE Code ='Relationship'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Head'
    SET @Description='Head'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Spouse'
    SET @Description='Spouse'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Son/Daughter'
    SET @Description='Son/Daughter'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Grandchild'
    SET @Description='Grandchild'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Brother/Sister'
    SET @Description='Brother/Sister'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Father/mother'
    SET @Description='Father/mother'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Nephew/Niece'
    SET @Description='Nephew/Niece'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='In-Law'
    SET @Description='In-Law'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Grandparent'
    SET @Description='Grandparent'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other relative'
    SET @Description='Other relative'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Non-relative'
    SET @Description='Non-relative'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Employee'
    SET @Description='Employee'
    SET @OrderNo=11
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='DK'
    SET @Description='DK'
    SET @OrderNo=12
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Marital Status'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Never married'
    SET @Description='Never married'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Married Monogamous'
    SET @Description='Married Monogamous'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Married Polygamous'
    SET @Description='Married Polygamous'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Widowed'
    SET @Description='Widowed'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Divorced or separated'
    SET @Description='Divorced or separated'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='DK'
    SET @Description='DK'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Disability'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Visual'
    SET @Description='Visual'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Hearing'
    SET @Description='Hearing'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Speech'
    SET @Description='Speech'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Physical'
    SET @Description='Physical'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Mental'
    SET @Description='Mental'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Self-care difficulties'
    SET @Description='Self-care difficulties'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Others'
    SET @Description='Others'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='None'
    SET @Description='None'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SELECT @Id=Id
FROM SystemCode
WHERE Code ='Education Attendance'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='At School'
    SET @Description='At school or learning institution'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Left School'
    SET @Description='Left school or learning institution'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Never Attended'
    SET @Description='Never went to school or learning institution'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END





SELECT @Id=Id
FROM SystemCode
WHERE Code ='Education Level'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Pre primary (ECD) or NONE'
    SET @Description='Pre primary (ECD) or NONE'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 1 (incomplete)'
    SET @Description='Standard 1 (incomplete)'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 1'
    SET @Description='Standard 1'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 2'
    SET @Description='Standard 2'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 3'
    SET @Description='Standard 3'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 4'
    SET @Description='Standard 4'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



    SET @DetailCode='Standard 5'
    SET @Description='Standard 5'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 6'
    SET @Description='Standard 6'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 7'
    SET @Description='Standard 7'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Standard 8'
    SET @Description='Standard 8'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



    SET @DetailCode='Form 1'
    SET @Description='Form 1'
    SET @OrderNo=11
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Form 2'
    SET @Description='Form 2'
    SET @OrderNo=12
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Form 3'
    SET @Description='Form 3'
    SET @OrderNo=13
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Form 4'
    SET @Description='Form 4'
    SET @OrderNo=14
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Form 5'
    SET @Description='Form 5'
    SET @OrderNo=15
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Form 6'
    SET @Description='Form 6'
    SET @OrderNo=16
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;





    SET @DetailCode='Incomplete post-secondary'
    SET @Description='Incomplete post-secondary'
    SET @OrderNo=17
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Complete post-secondary'
    SET @Description='Complete post-secondary'
    SET @OrderNo=18
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Incomplete undergraduate or Polytechnic'
    SET @Description='Incomplete undergraduate or Polytechnic'
    SET @OrderNo=19
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Complete undergraduate'
    SET @Description='Complete undergraduate'
    SET @OrderNo=20
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Incomplete master/PhD'
    SET @Description='Incomplete master/PhD'
    SET @OrderNo=21
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Complete master/PhD'
    SET @Description='Complete master/PhD'
    SET @OrderNo=22
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=23
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




END

SELECT @Id=Id
FROM SystemCode
WHERE Code ='Work Type'
IF(ISNULL(@Id,0)>0)
BEGIN


    SET @DetailCode='Worked for pay'
    SET @Description='Worked for pay'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='On leave'
    SET @Description='On leave'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Sick leave'
    SET @Description='Sick leave'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Worked own or at family business or	at family agriculture'
    SET @Description='Worked own or at family business or	at family agriculture'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Apprentice/Intern'
    SET @Description='Apprentice/Intern'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Volunteer'
    SET @Description='Volunteer'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Seeking work'
    SET @Description='Seeking work'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='No work available'
    SET @Description='No work available'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Retired with pension'
    SET @Description='Retired with pension'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Homemaker'
    SET @Description='Homemaker'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Full-time Student'
    SET @Description='Full-time Student'
    SET @OrderNo=11
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Part-time student'
    SET @Description='Part-time student'
    SET @OrderNo=12
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Incapacitated'
    SET @Description='Incapacitated'
    SET @OrderNo=13
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=14
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END


SELECT @Id = NULL

-----------------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Interview Type'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='NEW HOUSEHOLD'
    SET @Description='NEW HOUSEHOLD'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='RECERTIFICATION'
    SET @Description='RECERTIFICATION'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SELECT @Id = NULL


-----------------------------------------------------------------
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Interview Result'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Completed'
    SET @Description='Completed'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Incompleted'
    SET @Description='Incompleted'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Rejection'
    SET @Description='Rejection'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='No one at home'
    SET @Description='No one at home'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Cannot find household'
    SET @Description='Cannot find household'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END