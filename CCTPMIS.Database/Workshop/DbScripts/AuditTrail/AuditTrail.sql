﻿IF NOT OBJECT_ID('AuditTrailDetail') IS NULL	DROP TABLE AuditTrailDetail
GO

IF NOT OBJECT_ID('AuditTrail') IS NULL	DROP TABLE AuditTrail
GO
CREATE TABLE AuditTrail(
	Id int NOT NULL IDENTITY(1,1)
   ,UserId int   NULL
   ,UserName VARCHAR(100)
   ,FullName VARCHAR(100)
   ,Module VARCHAR(100)
   ,Description VARCHAR(100)
   ,LogTime	datetime NOT NULL
   ,IPAddress varchar(15) NOT NULL
   ,MACAddress varchar(17) NULL
   ,IMEI varchar(36) NULL
   ,ModuleRightId  int  NOT NULL
   ,TableId  int NOT NULL
   ,Key1 int NOT NULL
   ,Key2 int NULL
   ,Key3 int NULL
   ,Key4 int NULL
   ,Key5 int NULL
   ,Record nvarchar(MAX) NULL
   ,WasSuccessful bit 
   ,CONSTRAINT PK_AuditTrail PRIMARY KEY (Id)
 )
GO



IF NOT OBJECT_ID('LogTable') IS NULL
	DROP TABLE LogTable
GO
CREATE TABLE LogTable  (
	Id int NOT NULL IDENTITY(1,1)
	,ModuleRightId  int   NULL
	,TableName varchar(100)
 )
 GO


 
IF NOT OBJECT_ID('AddAudtTrail') IS NULL	DROP PROC AddAudtTrail
GO

CREATE PROC AddAudtTrail  
 @IPAddress varchar(15)  
   ,@MACAddress varchar(17)=NULL  
   ,@IMEI bigint=NULL  
   ,@ModuleRightCode varchar(64)  
   ,@Description varchar(64)   =''
   ,@TableName varchar(64)  
   ,@UserAgent varchar(64)  
   ,@Key1 int  
   ,@Key2 int=NULL  
   ,@Key3 int=NULL  
   ,@Key4 int=NULL  
   ,@Key5 int=NULL  
   ,@Record nvarchar(MAX)  
   ,@WasSuccessful bit  
   ,@UserId int = null  
AS  
BEGIN  
 DECLARE @ModuleRightId int  
 DECLARE @TableId int  
 DECLARE @SysCode varchar(20)  
 DECLARE @NoOfRows int  
 DECLARE @ErrorMsg varchar(128)  
  
 SET @SysCode='System Right'  
 SELECT @ModuleRightId=T1.Id FROM ModuleRight T1 INNER JOIN Module T2 ON T1.ModuleId=T2.Id   
			 INNER JOIN SystemCodeDetail T3 ON T1.RightId=T3.Id  
			 INNER JOIN SystemCode T4 ON T3.SystemCodeId=T4.Id AND T4.Code=@SysCode  
WHERE UPPER(T2.Name+':'+T1.Description)=UPPER(@ModuleRightCode)  
  
 BEGIN TRAN  

 SELECT @TableId =  Id FROM  UAT_CCTPMIS_AUDIT.dbo. LogTable where TableName = @TableName

 IF(ISNULL(@TableId,0)=0)
 insert into  UAT_CCTPMIS_AUDIT.dbo.LogTable(ModuleRightId,TableName)
 SELECT @ModuleRightId, @TableName
 
 DECLARE @FullName varchar(100)
 DECLARE @Email varchar(100)
 
 select @FullName= concat(Firstname,' ',MiddleName, '', Surname), @Email = Email from [User] where Id = @userId
 
 SELECT @TableId =  Id FROM  UAT_CCTPMIS_AUDIT.dbo. LogTable where TableName = @TableName
 
 INSERT INTO  UAT_CCTPMIS_AUDIT.dbo. AuditTrail(UserId,LogTime,IPAddress,MACAddress,IMEI,ModuleRightId,Key1,Key2,Key3,Key4,Key5,Record,TableId,WasSuccessful,FullName,Module,Description,UserName)  
 SELECT @UserId,GETDATE(),@IPAddress,@MACAddress,@IMEI,@ModuleRightId,@Key1,@Key2,@Key3,@Key4,@Key5,@Record  ,@TableId,@WasSuccessful,@FullName,@ModuleRightCode, @Description,@Email
 
 COMMIT TRAN     
 SELECT  0   
END  
GO






IF NOT OBJECT_ID('GetAuditTrail') IS NULL	DROP PROC GetAuditTrail
GO
CREATE PROC GetAuditTrail
	@Id int=NULL
   ,@UserId int=NULL
   ,@StartDate datetime=NULL
   ,@EndDate datetime=NULL
   ,@IPAddress varchar(15)=NULL
   ,@MACAddress varchar(15)=NULL
   ,@IMEI bigint=NULL
   ,@ModuleId int=NULL
   ,@ModuleRightId int=NULL
   ,@Key1 int=null
   ,@Key2 int=null
   ,@Key3 int=null
   ,@Key4 int=null
   ,@Key5 int=null
AS
BEGIN
	SELECT T1.Id,T1.UserId,T5.UserName,T1.LogTime,T1.IPAddress,T1.MACAddress,T1.IMEI,T3.Name AS Module,T1.ModuleRightId,T2.[Description] AS ModuleRightDescription,T1.Record,T1.WasSuccessful
	FROM AuditTrail T1 INNER JOIN ModuleRight T2 ON T1.ModuleRightId=T2.Id
					   INNER JOIN Module T3 ON T2. ModuleId=T3.Id
					   INNER JOIN SystemCodeDetail T4 ON T2.RightId=T4.Id
					   INNER JOIN [User] T5 ON T1.UserId=T5.Id
	WHERE T1.Id=ISNULL(@Id,T1.Id)
		AND T1.UserId=ISNULL(@UserId,T1.UserId)
		AND T1.LogTime>=ISNULL(@StartDate,T1.LogTime) AND T1.LogTime<=ISNULL(@EndDate,T1.LogTime)
		AND T1.IPAddress=ISNULL(@IPAddress,T1.IPAddress)
		AND T1.MACAddress=ISNULL(@MACAddress,T1.MACAddress)
		AND T1.IMEI=ISNULL(@IMEI,T1.IMEI)
		AND T3.Id=ISNULL(@ModuleId,T3.Id)
		AND T1.ModuleRightId=ISNULL(@ModuleRightId,T1.ModuleRightId)
		AND T1.Key1=ISNULL(@key1,T1.Key1)
		AND T1.Key2=ISNULL(@key2,T1.Key2)
		AND T1.Key3=ISNULL(@key3,T1.Key3)
		AND T1.Key4=ISNULL(@key4,T1.Key4)
		AND T1.Key5=ISNULL(@key5,T1.Key5)
END
GO


--INSERTING THE MODULE
INSERT INTO Module(Name, [Description],ParentModuleId)
SELECT T1.Name,T1.[Description],T1.ParentModuleId
FROM (SELECT 1 AS ModuleNo,'System' AS Name,'System' AS [Description],NULL AS ParentModuleId
	)T1 LEFT JOIN Module T2 ON T1.Name=T2.Name
WHERE T2.Id IS NULL



--CREATING A SYSTEM RIGHT FOR ACCESSING THE SYSTEM

--CREATING A SYSTEM RIGHT FOR ACCESSING THE SYSTEM
DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int
DECLARE @SysRightId_VIEW int
DECLARE @SysRightCode_VIEW varchar(20)


 
DECLARE @SysRightCode_ENTRY VARCHAR(20)
DECLARE @SysRightCode_MODIFIER VARCHAR(20)
DECLARE @SysRightCode_DELETION VARCHAR(20)
DECLARE @SysRightCode_APPROVAL VARCHAR(20)
DECLARE @SysRightCode_FINALIZE VARCHAR(20)
DECLARE @SysRightCode_VERIFY VARCHAR(20)
DECLARE @SysRightCode_EXPORT VARCHAR(20)
DECLARE @SysRightCode_DOWNLOAD VARCHAR(20)
DECLARE @SysRightCode_UPLOAD VARCHAR(20)

 
DECLARE @SysRightId_ENTRY INT
DECLARE @SysRightId_MODIFIER INT
DECLARE @SysRightId_DELETION INT
DECLARE @SysRightId_APPROVAL INT
DECLARE @SysRightId_FINALIZE INT
DECLARE @SysRightId_VERIFY INT
DECLARE @SysRightId_EXPORT INT
DECLARE @SysRightId_DOWNLOAD INT
DECLARE @SysRightId_UPLOAD INT

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD

 
SET @ModuleCode='System'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode


INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId AS ModuleId,@SysRightId_EXPORT AS RightId,'LOGIN' AS [Description]
		UNION
		SELECT @ModuleId ,@SysRightId_ENTRY ,'CHANGE PASSWORD'  
		UNION
		SELECT @ModuleId ,@SysRightId_MODIFIER ,'RESET PASSWORD'  
		UNION
		SELECT @ModuleId ,@SysRightId_DELETION ,'LOG OFF'  
		UNION
		SELECT @ModuleId ,@SysRightId_VERIFY ,'EDIT PROFILE'  

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL



INSERT INTO Module(Name, [Description],ParentModuleId)
SELECT T1.Name,T1.[Description],T1.ParentModuleId
FROM (SELECT 1 AS ModuleNo,'Audit Trail' AS Name,'Audit Trail' AS [Description],NULL AS ParentModuleId
	)T1 LEFT JOIN Module T2 ON T1.Name=T2.Name
WHERE T2.Id IS NULL


SET @ModuleCode='Audit Trail'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
 
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId AS ModuleId,@SysRightId_EXPORT AS RightId, @SysRightCode_EXPORT AS [Description]
			
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL