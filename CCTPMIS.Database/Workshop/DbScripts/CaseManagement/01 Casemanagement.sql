﻿
IF NOT OBJECT_ID('ComplaintNote') IS NULL	
DROP TABLE ComplaintNote
GO
IF NOT OBJECT_ID('ComplaintDocument') IS NULL	
DROP TABLE ComplaintDocument
GO
IF NOT OBJECT_ID('Complaint') IS NULL	
DROP TABLE Complaint
GO
 IF NOT OBJECT_ID('ChangeNote') IS NULL	
DROP TABLE ChangeNote
GO
IF NOT OBJECT_ID('ChangeDocument') IS NULL	
DROP TABLE ChangeDocument
GO
IF NOT OBJECT_ID('Change') IS NULL	
DROP TABLE Change
GO
IF NOT OBJECT_ID('CaseCategory') IS NULL	
DROP TABLE CaseCategory
GO

IF NOT OBJECT_ID('EFCNote') IS NULL	
DROP TABLE EFCNote
GO
IF NOT OBJECT_ID('EFCDocument') IS NULL	
DROP TABLE EFCDocument
GO
IF NOT OBJECT_ID('EFC') IS NULL	
DROP TABLE EFC
GO



IF NOT OBJECT_ID('BulkTransferDetail') IS NULL	
DROP TABLE BulkTransferDetail
GO
IF NOT OBJECT_ID('BulkTransfer') IS NULL	
DROP TABLE BulkTransfer
GO


IF NOT OBJECT_ID('temp_BulkTransferFile') IS NULL	
DROP TABLE temp_BulkTransferFile
GO




CREATE TABLE  temp_BulkTransferFile(
	BulkTransferId INT NOT NULL,
	ChangeType varchar(100) NOT NULL,
	ProgrammeId tinyint NOT NULL,
	ProgrammeCode nvarchar(20) NOT NULL,
	ProgrammeName nvarchar(100) NOT NULL,
	PriReciPersonId int NOT NULL,
	PriReciFirstName varchar(50) NOT NULL,
	PriReciMiddleName varchar(50) NULL,
	PriReciSurname varchar(50) NOT NULL,
	PriReciSex varchar(20) NOT NULL,
	PriReciNationalIdNo varchar(30) NULL,
	PriReciMobileNo1 nvarchar(20) NULL,
	PriReciMobileNo2 nvarchar(20) NULL,
	SecReciFirstName varchar(50) NULL,
	SecReciMiddleName varchar(50) NULL,
	SecReciSurname varchar(50) NULL,
	SecReciSexId int NULL,
	SecReciSex varchar(20) NULL,
	SecReciNationalIdNo varchar(30) NULL,
	SecReciMobileNo1 nvarchar(20) NULL,
	SecReciMobileNo2 nvarchar(20) NULL,
	AccountNo varchar(50) NULL,
	AccountName varchar(100) NULL,
	PSPBranch nvarchar(100) NULL,
	PSP nvarchar(100) NULL,
	AccountOpenedOn datetime NULL,
	SubLocationName varchar(30) NOT NULL,
	LocationName varchar(30) NOT NULL,
	DivisionName varchar(30) NOT NULL,
	DistrictName varchar(30) NOT NULL,
	CountyName varchar(30) NOT NULL,
	ConstituencyName varchar(30) NOT NULL
)  
GO

CREATE TABLE  CaseCategory(
	Id int IDENTITY(1,1) NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,[Name] varchar(30)
	,Description varchar(200)
	,CreatedBy int NULL
	,ModifiedOn datetime NULL
	,ModifiedBy int NULL
	,ApvBy int NULL
	,ApvOn datetime NULL
	,CONSTRAINT PK_CaseCategory PRIMARY KEY (Id)
)
GO

CREATE TABLE  Complaint(
	Id int IDENTITY(1,1) NOT NULL,
	ProgrammeId tinyint NOT NULL,
	HhEnrolmentId int NOT NULL,
	ComplaintTypeId int NULL,
	SerialNo int NULL,
	ReportedByName varchar(50) NULL,
	ReportedByIdNo varchar(50) NULL,
	ReportedByPhone varchar(50) NULL,
	ReportedBySexId int NULL,
	ReportedByTypeId int NOT NULL, 
	ComplaintDate datetime NULL,
	CreatedOn DATETIME NOT NULL DEFAULT GETDATE(),
	CreatedBy int NULL,
	ModifiedOn datetime NULL,
	ModifiedBy int NULL,
	ApvBy int NULL,
	ApvOn datetime NULL,
	ResolvedBy int NULL,
	ResolvedOn datetime NULL,
	ClosedBy int NULL,
	ClosedOn datetime NULL,
	VerifiedBy int NULL,
	VerifiedOn datetime NULL,
	EscalatedBy int NULL,
	EscalatedOn datetime NULL,
	Escalated2By int NULL,
	Escalated2On datetime NULL,
	ComplaintStatusId int NULL,
	ComplaintLevelId int NULL,   
	ReceivedBy nvarchar(50) NULL,
	Designation nvarchar(50) NULL,
	SourceId  int NULL,
	ConstituencyId  int NULL,
	ReceivedOn datetime NULL
	,CONSTRAINT PK_Complaint PRIMARY KEY (Id)
	,CONSTRAINT FK_Complaint_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_User_ResolvedBy FOREIGN KEY (ResolvedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_HouseholdEnrolment_HhEnrolmentId FOREIGN KEY (HhEnrolmentId) REFERENCES HouseholdEnrolment(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintTypeId FOREIGN KEY (ComplaintTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintStatusId FOREIGN KEY (ComplaintStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ComplaintLevelId FOREIGN KEY (ComplaintLevelId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_ReportedByTypeId FOREIGN KEY (ReportedByTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Complaint_SystemCodeDetail_SourceId FOREIGN KEY (SourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	 )
GO

CREATE TABLE  ComplaintDocument(
	Id int IDENTITY(1,1) NOT NULL
	,ComplaintId int NOT NULL
	,CreatedById int NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,FileName VARCHAR(300) NOT NULL
	,FilePath VARCHAR(300) NOT NULL
	,CategoryId int NOT NULL
	,CONSTRAINT PK_ComplaintDocument PRIMARY KEY (Id)
	,CONSTRAINT FK_ComplaintDocument_SystemCodeDetail_ComplaintId FOREIGN KEY (ComplaintId) REFERENCES Complaint(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintDocument_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FKComplaintDocument_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE  ComplaintNote(
	Id int IDENTITY(1,1) NOT NULL
	,ComplaintId int NOT NULL
	,CreatedById int NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,Note VARCHAR(300) NOT NULL
	,CategoryId int NOT NULL
	,CONSTRAINT PK_ComplaintNote PRIMARY KEY (Id)
	,CONSTRAINT FK_ComplaintNote_SystemCodeDetail_ComplaintId FOREIGN KEY (ComplaintId) REFERENCES Complaint(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintNote_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ComplaintNote_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE Change(
	 Id INT NOT NULL IDENTITY(1,1)
	,ProgrammeId TINYINT NOT NULL
	,HhEnrolmentId INT NOT NULL
	,ChangeTypeId INT NOT NULL
	,ChangeLevelId INT NOT NULL
	,SerialNo INT NULL
	,ReportedByTypeId INT NOT NULL
	,ReportedByName VARCHAR(50) NULL
	,ReportedByIdNo VARCHAR(50) NULL
	,ReportedByPhone VARCHAR(50) NULL
	,ReportedBySexId INT NULL
	,ChangeDate DATETIME NULL	
	,ReceivedBy VARCHAR  (50) NULL	
	,ReceivedOn DATETIME NOT NULL
	,Designation VARCHAR  (50) NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,CreatedBy INT NULL	
	,ApvBy int NULL
	,ApvOn datetime NULL
	,ModifiedOn DATETIME NULL
	,ModifiedBy INT NULL  
	,ConfirmedOn DATETIME NULL
	,ConfirmedBy INT NULL  
	,VerifiedBy INT NULL
	,VerifiedOn DATETIME NULL
	,RejectedBy INT NULL
	,RejectedOn DATETIME NULL
 
 
	 ,EscalatedBy INT NULL
	,EscalatedOn DATETIME NULL
 
	 ,BulkTransferBy INT NULL
	,BulkTransferOn DATETIME NULL

	,ClosedBy INT NULL
	,ClosedOn DATETIME NULL
	,ApprovedBy INT NULL
	,ApprovedOn DATETIME NULL
	,BeneFullName VARCHAR  (50) NULL
	,BeneNationalIdNo VARCHAR  (50) NULL
	,BenePhoneNumber VARCHAR  (50) NULL
	,BeneSexId INT NULL
	,BeneDoB DATETIME NULL
	,CgFullName VARCHAR  (50) NULL
	,CgNationalIdNo VARCHAR  (50) NULL
	,CgPhoneNumber VARCHAR  (50) NULL
	,CgSexId INT NULL
	,CgDoB DATETIME NULL
	,DeathDate DATETIME NULL
	,DeathNotificationDate DATETIME NULL
	,ChangeStatusId INT NOT NULL
	,ChangeSourceId INT NULL  
	,RelationshipId INT NULL
	,SubLocationId INT NULL
	,PersonId INT NULL
	,FirstName VARCHAR(50)
	,MiddleName VARCHAR(50) NULL
	,Surname VARCHAR(50)
	,PhoneNumber VARCHAR(50)
	,SexId INT NULL
	,DoB DATETIME NULL    
	,BirthCertNo VARCHAR(50) NULL
	,NationalIdNo VARCHAR(30) NULL
	,UpdatedRoleId INT NULL
	,UpdatedRelationshipId INT NULL
	,UpdatedPersonId INT NULL
	,UpdatedFirstName VARCHAR(50) NULL
	,UpdatedMiddleName VARCHAR(50) NULL
	,UpdatedSurname VARCHAR(50) NULL
	,UpdatedSexId INT NULL
	,UpdatedDoB DATETIME NULL
	,UpdatedBirthCertNo VARCHAR(50) NULL
	,UpdatedNationalIdNo VARCHAR(30) NULL
	,UpdatedMobileNo1 VARCHAR(20) NULL
	,UpdatedMobileNo2 VARCHAR(20) NULL
	,UpdatedPhoneNumber VARCHAR(20) NULL
	,UpdatedSubLocationId INT NULL
	,ConstituencyId int not null
	,CONSTRAINT PK_Change PRIMARY KEY (Id)
	,CONSTRAINT FK_Change_BeneSex_BeneSexId FOREIGN KEY (BeneSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Change_CgSex_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Change_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_Change_HouseholdEnrolment_HhEnrolmentId FOREIGN KEY (HhEnrolmentId) REFERENCES HouseholdEnrolment(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	)
GO

CREATE TABLE  ChangeNote
(
	Id INT IDENTITY(1,1) NOT NULL
	,ChangeId INT NOT NULL
	,CreatedById INT NOT NULL
	,CreatedOn DATETIME  NULL
	,Note VARCHAR(max) NOT NULL
	,CategoryId INT NOT NULL
	,CONSTRAINT PK_ChangeNote PRIMARY KEY (Id)
	,CONSTRAINT FK_ChangeNote_Change_ChangeId FOREIGN KEY (ChangeId) REFERENCES Change(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ChangeNote_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ChangeNote_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE  ChangeDocument (
	Id INT IDENTITY(1,1) NOT NULL
	,ChangeId INT NOT NULL
	,CreatedById INT NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()
	,[FileName] VARCHAR(300) NOT NULL
	,FilePath VARCHAR(300) NOT NULL
	,CategoryId INT NOT NULL
	,CONSTRAINT PK_ChangeDocument PRIMARY KEY (Id)
	,CONSTRAINT FK_ChangeDocument_Change_ChangeId FOREIGN KEY (ChangeId) REFERENCES Change(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ChangeDocument_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_ChangeDocument_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO


CREATE TABLE EFC (
	 Id int NOT NULL IDENTITY
	,EFCTypeId int NOT NULL
	,SerialNo INT NULL
	,ReportedByName nvarchar (50)
	,ReportedByIdNo nvarchar (20)
	,ReportedByPhone nvarchar (20)
	,ReportedBySexId int NULL
	,ReportedByTypeId INT NOT NULL 
	,ReportedDate datetime 
	,ClosedOn datetime 
	,ClosedBy INT NULL
	,ReceivedBy nvarchar (50)
	,Regarding nvarchar (200)
	,Designation nvarchar (50)
	,SourceId INT NOT NULL 
	,StatusId INT NOT NULL 
	,ConstituencyId int NOT NULL
	,ApvBy INT NULL
	,ApvOn datetime
	,CreatedBy  INT NOT NULL
	,CreatedOn datetime NOT NULL
	,ModifiedBy INT NULL
	,ModifiedOn datetime 
	,CONSTRAINT PK_EFC PRIMARY KEY (Id)
	,CONSTRAINT FK_EFC_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_SystemCodeDetail_EFCTypeId FOREIGN KEY (EFCTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_SystemCodeDetail_ReportedByTypeId FOREIGN KEY (ReportedByTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_SystemCodeDetail_SourceId FOREIGN KEY (SourceId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFC_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)

CREATE TABLE EFCNote (
	Id int NOT NULL IDENTITY(1,1)
	,EFCId int NOT NULL
	,Note nvarchar (256) NOT NULL
	,CreatedById INT NOT NULL
	,CategoryId int NOT NULL
	,CreatedOn datetime
	,CONSTRAINT PK_EFCNote PRIMARY KEY (Id)
	,CONSTRAINT FK_EFCNote_EFC_EFCId FOREIGN KEY (EFCId) REFERENCES EFC(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFCNote_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFCNote_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)


CREATE TABLE EFCDocument (
	 Id int NOT NULL IDENTITY(1,1)
	,EFCId int NOT NULL
	,CreatedById INT NOT NULL
	,CategoryId int NOT NULL
	,CreatedOn DATETIME NOT NULL
	,FilePath nvarchar (128)
	,FileName nvarchar (256)
	,CONSTRAINT PK_EFCDocument PRIMARY KEY (Id)
	,CONSTRAINT FK_EFCDocument_EFC_EFCId FOREIGN KEY (EFCId) REFERENCES EFC(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFCDocument_User_CreatedById FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT FK_EFCDocument_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)



CREATE TABLE  BulkTransfer(
	Id int IDENTITY(1,1) NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()	
	,CreatedBy int NULL
	,ModifiedOn datetime NULL
	,ModifiedBy int NULL
	,ApvBy int NULL
	,ApvOn datetime NULL
	,[Name] varchar(30)
	,StatusId int not NULL
	,FileId int   NULL
	,Description varchar(200)
	,CONSTRAINT PK_BulkTransfer PRIMARY KEY (Id)
)
GO


CREATE TABLE  BulkTransferDetail(
	Id int IDENTITY(1,1) NOT NULL
	,ChangeId int NOT NULL
	,BulkTransferId int NOT NULL
	,CreatedOn DATETIME NOT NULL DEFAULT GETDATE()	
	,CreatedBy int NULL
	,CONSTRAINT PK_BulkTransferDetail PRIMARY KEY (Id)
	,CONSTRAINT FK_BulkTransferDetail_BulkTransfer_BulkTransferId FOREIGN KEY (BulkTransferId) REFERENCES BulkTransfer(Id) ON UPDATE NO ACTION ON DELETE NO ACTION	
)
GO
