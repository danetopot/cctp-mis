﻿
DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT
DECLARE @Getter INT






SET @Code='Change Source'
SET @Description='CNG  Change Source'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Change Source'
IF(ISNULL(@Id,0)>0)
    BEGIN

    SET @DetailCode='CHURCH_MOSQUE'
    SET @Description='Church / Mosque'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='CHIEF_ASS_CHIEF'
    SET @Description='Chief/ Assistant Chief'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='MIN_OFFICERS'
    SET @Description='Ministry Officers'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='PSP'
    SET @Description='PSP'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='BWC'
    SET @Description='BWC'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='BENEFICIARY'
    SET @Description='Beneficiary'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='CAREGIVER'
    SET @Description='Caregiver'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL





SET @Code='Case Report By'
SET @Description='Case Report By User Type'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Case Report By'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='BENEFICIARY'
    SET @Description='BENEFICIARY'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='CAREGIVER'
    SET @Description='CAREGIVER'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='OTHER'
    SET @Description='OTHER'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='ANONYMOUS'
    SET @Description='ANONYMOUS'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


END


SET @Code='Escalation Levels'
SET @Description='Case Management Escalation Levels'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Escalation Levels'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Sub County'
    SET @Description='Sub County'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='County'
    SET @Description='County'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='PSP'
    SET @Description='PSP'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='National'
    SET @Description='National'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END

SET @Code='Case Status'
SET @Description='Case Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Case Status'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Open'
    SET @Description='Open'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Verified'
    SET @Description='Verified'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Escalated'
    SET @Description='Escalated'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Resolved'
    SET @Description='Resolved'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Closed'
    SET @Description='Closed'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Follow Up'
    SET @Description='Follow Up'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL

SET @Code='Change Status'
SET @Description='Change Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Change Status'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Open'
    SET @Description='Open'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Accepted'
    SET @Description='Accepted'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


    SET @DetailCode='Rejected'
    SET @Description='Rejected'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Escalated'
    SET @Description='Escalated'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Bulk Transfer'
    SET @Description='Bulk Transfer'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Approved'
    SET @Description='Approved'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Closed'
    SET @Description='Closed'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END
SELECT @Id = NULL

SET @Code='Complaint Types'
SET @Description='Complaint Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Complaint Types'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='MISSED_PAYMENT'
    SET @Description='Missed payment'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='LESS_PAYMENT'
    SET @Description='Less payment'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='NON_COLLECTION'
    SET @Description='Non-collection'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='FAILED_BIOS'
    SET @Description='Failed Bios'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='CARD_NOT_RECEIVED'
    SET @Description='Card not received'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='CARD_REPLACEMENT'
    SET @Description='Card Replacement'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='INCLUSION_EXCLUSION'
    SET @Description='Inclusion/Exclusion'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END

SET @Code='EFC'
SET @Description='Error, Fraud and Corruption'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='EFC'
IF(ISNULL(@Id,0)>0)
BEGIN
    SET @DetailCode='Corruption_Fraud'
    SET @Description='Corruption/ Fraud'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Quality_of_service'
    SET @Description='Quality of service'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
END
SELECT @Id = NULL
SET @Code='Change Types'
SET @Description='Change Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Change Types'
IF(ISNULL(@Id,0)>0)
    BEGIN
    SET @DetailCode='CAREGIVER_CHANGE'
    SET @Description='Change of Caregiver'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='BENEFICIARY_DEATH'
    SET @Description='Death of Beneficiary'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='NEW_HH_MEMBER'
    SET @Description='New Household Member'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='HH_MEMBER_EXIT'
    SET @Description='Exit of Household Member'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='HH_EXIT'
    SET @Description='Exit of Household'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='HH_CORRECTION'
    SET @Description='Correction of Household Info'
    SET @OrderNo=6
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='PSP_CHANGE'
    SET @Description='Change of PSP'
    SET @OrderNo=7
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='CARD_REPLACEMENT'
    SET @Description='Card Replacement'
    SET @OrderNo=8
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='BANK_ACC_CHANGE'
    SET @Description='Change of Bank Account'
    SET @OrderNo=9
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='RELOCATION'
    SET @Description='Relocation'
    SET @OrderNo=10
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
SELECT @Id = NULL
    




	
SET @Code='Bulk Transfer Status'
SET @Description='Bulk Transfer Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Bulk Transfer Status'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='Open'
    SET @Description='Open'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Generated'
    SET @Description='Generated'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Shared'
    SET @Description='Shared'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Approved'
    SET @Description='Approved'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
END
