﻿

IF NOT OBJECT_ID('ApproveChangeRequest') IS NULL	
DROP PROC ApproveChangeRequest
GO

CREATE PROC ApproveChangeRequest
	@Id int,
	@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @HhId int
	DECLARE @PersonId int
	DECLARE @UpdatedPersonId int
	DECLARE @RelationshipIdCode varchar(20)
	DECLARE @RelationshipCode varchar(20)
	DECLARE @Relationship_BeneficiaryCode varchar(20)
	DECLARE @Relationship_CaregiverCode varchar(20)
	DECLARE @MemberRoleCode varchar(20)
	DECLARE @MemberRole_BeneficiaryCode varchar(20)
	DECLARE @MemberRole_CaregiverCode varchar(20)
	DECLARE @MemberRole_MemberCode varchar(20)
	DECLARE @UpdatedSubLocationId int
	DECLARE @GeoMasterId int
	DECLARE @ConstituencyId int
	DECLARE @ChangeTypeId int


	DECLARE @RelationshipId int
	DECLARE @StatusCode varchar(20)

	DECLARE @StatusId int

	DECLARE @MemberStatus int

	IF NOT EXISTS(SELECT 1
	FROM [User]
	WHERE Id=@UserId)
	SET @ErrorMsg='Please specify valid User'

	SET @SysCode='Change Status'
	SET @SysDetailCode='Accepted'
	SELECT @StatusId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS (SELECT 1
	FROM CHANGE
	where   Id = @Id AND ChangeStatusId = @StatusId )
	SET @ErrorMsg='This Update is not ready for Approval.'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END


	SELECT @StatusId = NULL


	SET @MemberRoleCode='Member Role'
	SET @MemberRole_BeneficiaryCode='BENEFICIARY'

	SET @MemberRole_CaregiverCode='CAREGIVER'
	SET @MemberRole_MemberCode='MEMBER'

	SET @RelationshipCode='Relationship'
	SET @Relationship_BeneficiaryCode='BENEFICIARY'
	SET @Relationship_CaregiverCode='CAREGIVER'


	SELECT
		@HhId=T2.HhId,
		@UpdatedSubLocationId= T1.UpdatedSubLocationId,
		@UpdatedPersonId=T1.UpdatedPersonId
	FROM Change T1
		INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
	WHERE T1.Id=@Id


	SET @SysCode='Change Types'
	SET @SysDetailCode='CAREGIVER_CHANGE'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@MemberRoleCode AND T1.Code=@MemberRole_CaregiverCode

		SELECT @RelationshipIdCode=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@RelationshipCode AND T1.Code=@Relationship_CaregiverCode

		-- DEACTIVATE PREVIOUS CG
		UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId1 	FROM HouseholdMember T1 
				 WHERE T1.HhId=@HhId AND T1.RelationshipId=@RelationshipId

		IF(ISNULL(@UpdatedPersonId,0)>0)
			BEGIN
			SET @SysCode='Member Status'
			SET @SysDetailCode='1'

			SELECT @SystemCodeDetailId3=T1.Id
			FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

			UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId3, T1.RelationshipId =@SystemCodeDetailId2 	
					FROM HouseholdMember T1 WHERE T1.HhId=@HhId AND T1.PersonId = @UpdatedPersonId

		END
			ELSE
			BEGIN
			INSERT INTO Person
				(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,CreatedBy,CreatedOn)
			SELECT 'ChangeId: '+CONVERT(varchar,T1.Id), T1.UpdatedFirstName, T1.UpdatedMiddleName, T1.UpdatedSurname, T1.UpdatedSexId, T1.UpdatedDoB, T1.UpdatedBirthCertNo, T1.UpdatedNationalIdNo, T1.UpdatedMobileNo1, 0, T1.UpdatedMobileNo2, 0, @UserId, GETDATE()
			FROM Change T1
			WHERE T1.Id=@Id

			SELECT @PersonId=Id
			FROM Person
			WHERE RefId='ChangeId: '+CONVERT(varchar,@Id)

			SET @SysCode='Member Status'
			SET @SysDetailCode='1'
			SELECT @SystemCodeDetailId1=T1.Id
			FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

			INSERT INTO HouseholdMember
				(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
			SELECT @HhId, @PersonId, UpdatedRelationshipId, @SystemCodeDetailId2, @SystemCodeDetailId1, @UserId, GETDATE()
			from Change
			where Id = @Id


		END



		SET @StatusCode='Escalated'

	END






	SET @SysCode='Change Types'
	SET @SysDetailCode='BENEFICIARY_DEATH'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SELECT @HhId=T2.HhId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SET @SysCode='HhStatus'
		SET @SysDetailCode='EX'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1 SET T1.StatusId=@SystemCodeDetailId1 FROM Household T1 WHERE T1.Id=@HhId

		SET @StatusCode='Escalated'
	END


	SET @SysCode='Change Types'
	SET @SysDetailCode='HH_EXIT'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SELECT @HhId=T2.HhId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SET @SysCode='HhStatus'
		SET @SysDetailCode='EX'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1 SET T1.StatusId=@SystemCodeDetailId1 FROM Household T1 WHERE T1.Id=@HhId

		SET @StatusCode='Escalated'
	END

	SET @SysCode='Change Types'
	SET @SysDetailCode='HH_MEMBER_EXIT'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
			
			BEGIN
		SELECT @HhId=T3.HhId   , @UpdatedPersonId=T1.UpdatedPersonId
		FROM Change T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId INNER JOIN HouseholdEnrolment T3 ON T2.Id=T3.HhEnrolmentPlanId AND T1.HhEnrolmentId=T3.Id
		WHERE T1.Id=@Id

		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
				SET T1.StatusId=@SystemCodeDetailId1 FROM HouseholdMember T1 WHERE T1.HhId=@HhId AND T1.PersonId=@UpdatedPersonId


		SET @StatusCode='Approved'
	END



	SET @SysCode='Change Types'
	SET @SysDetailCode='Relocation'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SELECT @GeoMasterId=T5.GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN County T5 ON T4.CountyId=T5.Id
		WHERE T1.Id=@UpdatedSubLocationId

		IF EXISTS(SELECT 1
		FROM HouseholdSubLocation
		WHERE Hhid=@HhId AND GeoMasterId=@GeoMasterId)
						UPDATE T1
						SET T1.SubLocationId=@UpdatedSubLocationId
						FROM HouseholdSubLocation T1
						WHERE T1.HhId=@HhId AND T1.GeoMasterId=@GeoMasterId
					ELSE
						INSERT INTO HouseholdSubLocation
			(HhId,GeoMasterId,SubLocationId)
		SELECT @HhId, @GeoMasterId, @UpdatedSubLocationId

		SET @StatusCode='Approved'
	END

	SET @SysCode='Change Types'
	SET @SysDetailCode='NEW_HH_MEMBER'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		INSERT INTO Person
			(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,CreatedBy,CreatedOn)
		SELECT 'ChangeId: '+CONVERT(varchar,T1.Id), T1.UpdatedFirstName, T1.UpdatedMiddleName, T1.UpdatedSurname, T1.UpdatedSexId, T1.UpdatedDoB, T1.UpdatedBirthCertNo, T1.UpdatedNationalIdNo, T1.UpdatedMobileNo1, 0, T1.UpdatedMobileNo2, 0, @UserId, GETDATE()
		FROM Change T1
		WHERE T1.Id=@Id

		SELECT @PersonId=Id
		FROM Person
		WHERE RefId='ChangeId: '+CONVERT(varchar,@Id)

		SET @SysCode='Member Status'
		SET @SysDetailCode='1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO HouseholdMember
			(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
		SELECT @HhId, PersonId, RelationshipId, UpdatedRelationshipId, @SystemCodeDetailId1, @UserId, GETDATE()
		from Change
		where Id = @Id

		SET @StatusCode='Approved'

	END



	SET @SysCode='Change Types'
	SET @SysDetailCode='HH_CORRECTION'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SELECT @GeoMasterId=T5.GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN County T5 ON T4.CountyId=T5.Id
		WHERE T1.Id=@UpdatedSubLocationId

		IF EXISTS(SELECT 1
		FROM HouseholdSubLocation
		WHERE Hhid=@HhId AND GeoMasterId=@GeoMasterId)
						UPDATE T1
						SET T1.SubLocationId=@UpdatedSubLocationId
						FROM HouseholdSubLocation T1
						WHERE T1.HhId=@HhId AND T1.GeoMasterId=@GeoMasterId
					ELSE
						INSERT INTO HouseholdSubLocation
			(HhId,GeoMasterId,SubLocationId)
		SELECT @HhId, @GeoMasterId, @UpdatedSubLocationId

		SET @StatusCode='Approved'
	END

	SET @SysCode='Change Types'
	SET @SysDetailCode='PSP_CHANGE'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Account Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		UPDATE T2
			SET T2.StatusId=@SystemCodeDetailId1
			FROM BeneficiaryAccount T2 INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
			WHERE T3.HhId=@HhId AND T2.StatusId<>@SystemCodeDetailId1

		SET @StatusCode='Escalated'
	END

	SET @SysCode='Change Types'
	SET @SysDetailCode='CARD_REPLACEMENT'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Card Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId1 FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id WHERE T3.HhId=@HhId AND T1.StatusId<>@SystemCodeDetailId1
		SET @StatusCode='Escalated'
	END


	SET @SysCode='Change Types'
	SET @SysDetailCode='BANK_ACC_CHANGE'

	SELECT @ChangeTypeId=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SET @SysCode='Account Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id
		UPDATE T2
			SET T2.StatusId=@SystemCodeDetailId1
			FROM BeneficiaryAccount T2 INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
			WHERE T3.HhId=@HhId AND T2.StatusId<>@SystemCodeDetailId1

		SET @StatusCode='Escalated'
	END




	SET @SysCode='Account Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	IF(ISNULL(@StatusCode,'')<>'')
				BEGIN
		SET @SysCode='Change Status'
		SELECT @StatusId  =T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@StatusCode
		UPDATE  Change SET 
					 EscalatedBy = CASE WHEN @StatusCode ='Escalated' THEN  @UserId ELSE NULL END,
					 EscalatedOn = CASE WHEN @StatusCode ='Escalated' THEN  GETDATE()ELSE NULL END,
					 ApprovedBy = CASE WHEN @StatusCode ='Approved' THEN  @UserId ELSE NULL END,
					 ApprovedOn = CASE WHEN @StatusCode ='Approved' THEN  GETDATE()ELSE NULL END, 
					 ChangeStatusId = @StatusId WHERE Id = @Id

		SELECT *
		From Change
		where Id = @Id
	END
				ELSE
				BEGIN
		SET @ErrorMsg = 'The Update was not effected. '
		IF ISNULL(@ErrorMsg,'')<>''
					BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
	END
END
GO


IF NOT OBJECT_ID('GetPersonByEnrolmentNo') IS NULL	DROP PROC GetPersonByEnrolmentNo
GO
CREATE PROC GetPersonByEnrolmentNo
	@Id int
AS
BEGIN
	SELECT  
		T4.*
	from HouseholdEnrolment T1
		inner JOIN Household T2 ON T1.HhId = T2.ID
		INNER JOIN HouseholdMember T3 ON T2.Id = T3.HhId
		INNER JOIN Person T4 ON T3.PersonId = T4.ID
		INNER JOIN SystemCodeDetail T5 ON T3.RelationshipId = T5.Id
		INNER JOIN SystemCodeDetail T6 ON T3.MemberRoleId = T6.Id
			AND T5.Code!='BENEFICIARY'
			AND T5.Code!='CAREGIVER'
			AND T1.Id = @Id

END

GO



IF NOT OBJECT_ID('AddEditBulkTransferDetail') IS NULL	
DROP PROC AddEditBulkTransferDetail
GO

CREATE PROC AddEditBulkTransferDetail
    @Id INT
    ,
    @UserId INT
AS

BEGIN

    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @SystemCodeDetailId3 int
    DECLARE @TransferStatusId int
    DECLARE @StatusCode varchar(20)
    DECLARE @StatusId int
    DECLARE @ErrorMsg varchar(200)
    IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
	SET @ErrorMsg='Please specify valid User'

    SET @SysCode='Change Status'
    SET @SysDetailCode='Escalated'
    SELECT @StatusId = T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    IF NOT EXISTS (SELECT 1
    FROM CHANGE
    where   ChangeStatusId = @StatusId )
	SET @ErrorMsg='There Exists no  Update is not ready for Bulk Transfer.'
    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END
    ELSE
    BEGIN
        INSERT INTO  BulkTransferDetail
            ( ChangeId ,BulkTransferId,CreatedBy,CreatedOn )
        SELECT Id, @Id, @UserId, GETDATE()
        FROM CHANGE
        where   ChangeStatusId = @StatusId
        SET @SysCode='Change Status'
        SET @SysDetailCode='Bulk Transfer'
        SELECT @TransferStatusId = T1.Id
        FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
        UPDATE CHANGE  set BulkTransferBy = @UserId, BulkTransferOn= GETDATE(),  ChangeStatusId= @TransferStatusId  WHERE   ChangeStatusId = @StatusId
        SELECT @@ROWCOUNT AS NoOfRows
    END
END


GO
IF NOT OBJECT_ID('GetHouseholdInfo') IS NULL	
DROP PROC GetHouseholdInfo
GO

CREATE PROC GetHouseholdInfo  
 @Id int  
AS  
BEGIN  
 DECLARE @SysCode varchar(20)  
 DECLARE @SysDetailCode varchar(20)  
 DECLARE @SystemCodeDetailId1 int  
 DECLARE @SystemCodeDetailId2 int  
 DECLARE @SystemCodeDetailId3 int  
 DECLARE @Len tinyint  
 DECLARE @Prefix varchar(10)  
 DECLARE @ProgNo int  
 DECLARE @HhId int  
 DECLARE @LoopVar int  
    
  
 SET @SysCode='Member Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Account Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Card Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
   
  SELECT @HhId=HhId FROM HouseholdEnrolment WHERE  id =@Id    
  
  IF @HhId>0  
  BEGIN  
   SELECT T2.Id AS ProgrammeId,T2.Code AS ProgrammeCode,T2.Name AS ProgrammeName  
    ,T5.Id AS PriReciPersonId,T5.FirstName AS PriReciFirstName,T5.MiddleName AS PriReciMiddleName,T5.Surname AS PriReciSurname,T5.SexId AS PriReciSexId,T10.Code AS PriReciSex,T5.NationalIdNo AS PriReciNationalIdNo,T5.BirthCertNo AS PriReciBirthCertNo,T5.MobileNo1 AS PriReciMobileNo1,T5.MobileNo2 AS PriReciMobileNo2  
    ,T6.Id AS SecReciPersonId,T6.FirstName AS SecReciFirstName,T6.MiddleName AS SecReciMiddleName,T6.Surname AS SecReciSurname,T6.SexId AS SecReciSexId,T11.Code AS SecReciSex,T6.NationalIdNo AS SecReciNationalIdNo,T6.BirthCertNo AS SecReciBirthCertNo,T6.MobileNo1 AS SecReciMobileNo1,T6.MobileNo2 AS SecReciMobileNo2  
    ,T7.AccountNo,T7.AccountName,T7.PSPBranch,T7.PSP,T7.OpenedOn AS AccountOpenedOn  
    ,T12.PaymentCardNo AS PaymentCardNo  
    ,T9.SubLocationId,T9.SubLocationName,T9.LocationId,T9.LocationName,T9.DivisionId,T9.DivisionName,T9.DistrictId,T9.DistrictName,T9.CountyId,T9.CountyName,T9.ConstituencyId,T9.ConstituencyName  
   FROM Household T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id  
         INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId  
         INNER JOIN HouseholdMember T4  ON T1.Id=T4.HhId  
         INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1  
         LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1  
         LEFT JOIN (  
           SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn  
           FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id  
                    INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id  
                    INNER JOIN PSP T4 ON T3.PSPId=T4.Id  
           WHERE T2.HhId=@HhId AND T1.StatusId=@SystemCodeDetailId2  
          ) T7 ON T3.Id=T7.HhEnrolmentId  
         INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId  
         INNER JOIN (  
           SELECT T1.Id AS SubLocationId,T1.Name AS SubLocationName,T2.Id AS LocationId,T2.Name AS LocationName,T3.Id AS DivisionId,T3.Name AS DivisionName,T5.Id AS DistrictId,T5.Name AS DistrictName,T6.Id AS CountyId,T6.Name AS CountyName,T7.Id AS ConstituencyId,T7.Name AS ConstituencyName,T8.Id AS GeoMasterId  
           FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id  
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id  
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id  
                INNER JOIN District T5 ON T4.DistrictId=T5.Id  
                INNER JOIN County T6 ON T4.CountyId=T6.Id  
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id  
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1  
          ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId  
         INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id  
         LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id  
         LEFT JOIN (  
           SELECT DISTINCT T1.BeneAccountId,T1.PaymentCardNo  
           FROM BeneficiaryPaymentCard T1   
           WHERE T1.StatusId=@SystemCodeDetailId3  
          ) T12 ON T7.BeneAccountId=T12.BeneAccountId  
   WHERE T3.Id = @Id  
  
   RETURN  
  END  
 
 END  
   
 
  

IF NOT OBJECT_ID('GenerateBulkTransferFile') IS NULL	
DROP PROC GenerateBulkTransferFile
GO

IF NOT OBJECT_ID('GenerateBulkTransferFile') IS NULL	
DROP PROC GenerateBulkTransferFile
GO
CREATE PROC GenerateBulkTransferFile
    @FilePath nvarchar(128),
    @DBServer varchar(30),
    @DBName varchar(30),
    @DBUser varchar(30),
    @DBPassword nvarchar(30),
    @Id int,
    @UserId int
AS

BEGIN
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @SystemCodeDetailId3 int
    DECLARE @Len tinyint
    DECLARE @Prefix varchar(10)
    DECLARE @ProgNo int
    DECLARE @HhId int
    DECLARE @LoopVar int


    DECLARE @FileName varchar(128)
    DECLARE @FileExtension varchar(5)
    DECLARE @FileCompression varchar(5)
    DECLARE @FilePathName varchar(128)
    DECLARE @SQLStmt varchar(8000)
    DECLARE @FileExists bit
    DECLARE @FileIsDirectory bit
    DECLARE @FileParentDirExists bit
    DECLARE @DatePart_Day char(2)
    DECLARE @DatePart_Month char(2)
    DECLARE @DatePart_Year char(4)
    DECLARE @DatePart_Time char(4)

    DECLARE @FileId int
    DECLARE @FilePassword nvarchar(64)
    DECLARE @ErrorMsg varchar(128)
    DECLARE @NoOfRows int

    IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
    CREATE TABLE #FileResults
    (
        FileExists int
	   ,
        FileIsDirectory int
	   ,
        FileParentDirExists int
    );

    INSERT INTO #FileResults
    EXEC Master.dbo.xp_fileexist @FilePath

    SELECT @FileExists=FileExists, @FileIsDirectory=FileIsDirectory, @FileParentDirExists=FileParentDirExists
    FROM #FileResults

    IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1
    FROM [User]
    WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END



    SET @SysCode='Member Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Account Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId2=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Card Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId3=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    DELETE FROM  temp_BulkTransferFile

    INSERT INTO temp_BulkTransferFile
        (BulkTransferId,ChangeType,ProgrammeId,ProgrammeCode,ProgrammeName,PriReciFirstName,PriReciMiddleName,PriReciSurname,PriReciSex,PriReciNationalIdNo,PriReciMobileNo1,PriReciMobileNo2,SecReciFirstName,SecReciMiddleName,SecReciSurname,SecReciSexId,SecReciSex,SecReciNationalIdNo,SecReciMobileNo1,SecReciMobileNo2,AccountNo,AccountName,PSPBranch,PSP,AccountOpenedOn,SubLocationName,LocationName,DivisionName,DistrictName,CountyName,ConstituencyName)
    SELECT BTD.BulkTransferId, CT.[Description] AS ChangeType, T2.Id AS ProgrammeId, T2.Code AS ProgrammeCode, T2.Name AS ProgrammeName  , T5.FirstName AS PriReciFirstName, T5.MiddleName AS PriReciMiddleName, T5.Surname AS PriReciSurname,
        T10.Code AS PriReciSex, T5.NationalIdNo AS PriReciNationalIdNo,
        T5.MobileNo1 AS PriReciMobileNo1, T5.MobileNo2 AS PriReciMobileNo2  
  , T6.FirstName AS SecReciFirstName, T6.MiddleName AS SecReciMiddleName, T6.Surname AS SecReciSurname,
        T6.SexId AS SecReciSexId, T11.Code AS SecReciSex, T6.NationalIdNo AS SecReciNationalIdNo,
        T6.MobileNo1 AS SecReciMobileNo1, T6.MobileNo2 AS SecReciMobileNo2  
	, T7.AccountNo, T7.AccountName, T7.PSPBranch, T7.PSP, T7.OpenedOn AS AccountOpenedOn  
, T9.SubLocationName, T9.LocationName, T9.DivisionName, T9.DistrictName, T9.CountyName, T9.ConstituencyName
    FROM Household T1

        INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
        INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId
        INNER JOIN Change C ON T3.Id = C.HhEnrolmentId
        INNER JOIN BulkTransferDetail BTD ON C.Id = BTD.ChangeId
        INNER JOIN SystemCodeDetail CT ON C.ChangeTypeId = CT.Id
        INNER JOIN HouseholdMember T4 ON T1.Id=T4.HhId
        INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1
        LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1

        INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId
        INNER JOIN (  
           SELECT T1.Id AS SubLocationId, T1.Name AS SubLocationName, T2.Id AS LocationId, T2.Name AS LocationName, T3.Id AS DivisionId, T3.Name AS DivisionName, T5.Id AS DistrictId, T5.Name AS DistrictName, T6.Id AS CountyId, T6.Name AS CountyName, T7.Id AS ConstituencyId, T7.Name AS ConstituencyName, T8.Id AS GeoMasterId
        FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
            INNER JOIN Division T3 ON T2.DivisionId=T3.Id
            INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
            INNER JOIN District T5 ON T4.DistrictId=T5.Id
            INNER JOIN County T6 ON T4.CountyId=T6.Id
            INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
            INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1  
          ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId
        INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id
        LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id
        LEFT JOIN (  
           SELECT DISTINCT T1.HhEnrolmentId, T1.Id AS BeneAccountId, T1.AccountNo, T1.AccountName, T3.Name AS PSPBranch, T4.Name AS PSP, T1.OpenedOn
        FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
            INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id
            INNER JOIN PSP T4 ON T3.PSPId=T4.Id
        WHERE  T1.StatusId=@SystemCodeDetailId2  
          ) T7 ON T3.Id=T7.HhEnrolmentId
    where BTD.BulkTransferId = @Id

    EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

    SELECT @FileName='BULK_TRANSFER_'

    SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
    SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
    SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
    SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
    SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
    SET @FilePathName=@FilePath+@FileName
    SET @FileExtension='.csv'
    SET @FileCompression='.rar'

    SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_BulkTransferFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
    EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
    SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
    EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

    SET @SysCode='File Type'
    SET @SysDetailCode='ENROLMENT'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='File Creation Type'
    SET @SysDetailCode='SYSGEN'
    SELECT @SystemCodeDetailId2=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    INSERT INTO FileCreation
        (Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
    SELECT @FileName+@FileCompression AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, @FilePassword AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

    SET @FileId=IDENT_CURRENT('FileCreation')

    SET @SysCode='Change Status'
    SET @SysDetailCode='Bulk Transfer'
    SELECT @SystemCodeDetailId2=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    UPDATE T1 SET T1.ChangeStatusId = @SystemCodeDetailId2  FROM Change T1
        INNER JOIN BulkTransferDetail T2 ON T1.Id = T2.BulkTransferId AND T2.BulkTransferId = @Id
    UPDATE T1 SET T1.FileId = @FileId  FROM BulkTransfer T1 WHERE T1.Id=@Id
    SELECT @FileId AS FileId
END
GO

