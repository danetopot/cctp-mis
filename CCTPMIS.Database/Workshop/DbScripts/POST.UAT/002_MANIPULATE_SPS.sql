﻿

IF NOT OBJECT_ID('AddEditHhListingPlan') IS NULL	
   DROP PROC AddEditHhListingPlan
GO
 CREATE PROC AddEditHhListingPlan
	@Id INT=NULL
   ,@Name VARCHAR(30)
   ,@Description VARCHAR(128)
   ,@FromDate DATETIME
   ,@ToDate DATETIME
   ,@Programmes XML
   ,@Category VARCHAR(20)
   ,@UserId int
AS
BEGIN
  DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
			SET @ErrorMsg='Please specify valid UserId parameter'	 
	ELSE IF EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='Once the Exercise has been approved it cannot be modified.'

SET @SysCode='REG_CATEGORIES'
SELECT @CategoryId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@Category

SET @SysCode='REG_STATUS'
SET @SysDetailCode='CREATIONAPV'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF ISNULL(@CategoryId,'')=''
SET @ErrorMsg='Exercise Category must be specified'

IF ISNULL(@StatusId,'')=''
SET @ErrorMsg='Exercise Status must be specified'

IF ISNULL(@ErrorMsg,'')<>''
BEGIN
	RAISERROR(@ErrorMsg,16,1)
	RETURN
END
BEGIN TRAN
	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1 SET T1.[Name] =  @Name,T1.[Description] = @Description,T1.[Start] = @FromDate,T1.[End] = @ToDate,T1.ModifiedBy=@UserId,T1.ModifiedOn=GETDATE() FROM  TargetPlan T1 WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
	INSERT INTO TargetPlan ([Name],[Description],[Start],[End],CategoryId,StatusId,CreatedBy,CreatedOn) 
	SELECT @Name,@Description,@FromDate,@ToDate,@CategoryId,@StatusId,@UserId,GETDATE()
	SELECT @Id=IDENT_CURRENT('TargetPlan')
	END
	DELETE FROM ListingPlanProgramme WHERE TargetPlanId = @Id
	INSERT INTO ListingPlanProgramme(TargetPlanId,ProgrammeId) 
	SELECT T1.TargetPlanId,T1.ProgrammeId FROM (
			SELECT  
			 @Id AS TargetPlanId
			,U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId
		FROM @Programmes.nodes('ListingPlanProgrammeOptions/Record') AS U(R)
	) T1

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('ApproveTargetPlan') IS NULL	
DROP PROC ApproveTargetPlan
GO
CREATE PROC ApproveTargetPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @CreationStatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CategoryId INT


SELECT @CategoryId = T1.CategoryId, @StatusId = T1.StatusId FROM TargetPlan T1  where Id = @Id
	IF (ISNULL(@StatusId,'')='' OR ISNULL(@CategoryId,'')='')
   SET @ErrorMsg='Exercise was not found'
 
SET @SysCode='REG_STATUS'
SET @SysDetailCode='CREATIONAPV'
SELECT @CreationStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

if(@CreationStatusId<>@StatusId)
 SET @ErrorMsg='The specified Registration Exercise is not in the approval stage'
 
SET @SysDetailCode='ACTIVE'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF   EXISTS(SELECT 1 FROM TargetPlan WHERE Id<>@Id AND StatusId=@StatusId  AND CategoryId = @CategoryId)
		SET @ErrorMsg='There exists another Exercise that is currently active'
 
 ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN	 
	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO


IF NOT OBJECT_ID('AddEditHHListing') IS NULL	
DROP PROC AddEditHHListing
GO
CREATE PROC AddEditHHListing 
	   @ProgrammeId TINYINT
	   ,@HouseHoldXml XML
	AS
BEGIN


	DECLARE @ErrorMsg varchar(256)
	DECLARE @SysCode varchar(20)
	DECLARE @EditingCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @EditingId int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @TargetPlanId int
	DECLARE @StatusId int
	DECLARE @CategoryId int
	DECLARE @Id int

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='ACTIVE' 

	SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	

	SET @SysCode='REG_CATEGORIES'
		SET @SysDetailCode='LISTING' 
	SELECT @CategoryId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	
	
	

	SELECT @Id = Id FROM  TargetPlan WHERE  StatusId = @StatusId  AND  CategoryId = @CategoryId  
	IF (ISNULL(@Id,0)=0)
	 SET @ErrorMsg='No Active Targeting Plan currently Accepting Data' 
	 DECLARE @ProgName varchar(30)
	 SELECT @ProgName = [Name] FROM Programme WHERE Id = @Id

	IF NOT  EXISTS(SELECT 1 FROM  ListingPlanProgramme WHERE  ProgrammeId = @ProgrammeId AND TargetPlanId = @Id ) 
	SET @ErrorMsg='No Active Trageting Plan Accepting Data for PROGRAMME ' --+ @ProgName
	 
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	 
	 DECLARE @EnumeratorDeviceId  int = NULL

	 SELECT @EnumeratorDeviceId = Id  FROM   EnumeratorDevice WHERE DeviceId IN ( SELECT U.R.value('(DeviceId)[1]','VARCHAR(36)') AS DeviceId   FROM @HouseHoldXml.nodes('HHListings/Record') AS U(R))
	
	 IF ISNULL(@EnumeratorDeviceId,0)=0
 BEGIN
	 INSERT INTO EnumeratorDevice ( DeviceId, DeviceModel, DeviceManufacturer, DeviceName,[Version],VersionNumber, [Platform],Idiom,IsDevice, EnumeratorId)
	 SELECT T1.DeviceId, DeviceModel, DeviceManufacturer, DeviceName,[Version],VersionNumber, [Platform],Idiom,IsDevice,EnumeratorId
	 FROM
	 (
	 SELECT 
	 U.R.value('(DeviceId)[1]','VARCHAR(36)') AS DeviceId
	,U.R.value('(DeviceModel)[1]','VARCHAR(15)') AS DeviceModel
	,U.R.value('(DeviceManufacturer)[1]','VARCHAR(15)') AS DeviceManufacturer
	,U.R.value('(DeviceName)[1]','VARCHAR(15)') AS DeviceName
	,U.R.value('(Version)[1]','VARCHAR(15)')  AS [Version]
	,U.R.value('(VersionNumber)[1]','VARCHAR(15) ') AS VersionNumber
	,U.R.value('(Platform)[1]','VARCHAR(15)') AS [Platform]
	,U.R.value('(Idiom)[1]','VARCHAR(10)') AS Idiom
	,U.R.value('(IsDevice)[1]','BIT') AS IsDevice
	,U.R.value('(SyncEnumeratorId)[1]','INT ') AS EnumeratorId
	FROM @HouseHoldXml.nodes('HHListings/Record') AS U(R)
) T1

	 SELECT @EnumeratorDeviceId=IDENT_CURRENT('EnumeratorDevice')
 END
	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGSYNC' 
	SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode	
	INSERT INTO ListingPlanHH(UniqueId ,StartTime ,EndTime ,ProgrammeId ,RegDate ,SubLocationId,LocationId,Years,Months,TargetPlanId ,EnumeratorId  ,BeneFirstName,BeneMiddleName ,BeneSurname,BeneNationalIdNo,BeneSexId,BeneDoB,BenePhoneNumber,CgFirstName,CgMiddleName ,CgSurname  ,CgNationalIdNo,CgSexId,CgDoB,CgPhoneNumber,HouseholdMembers  ,Village  ,PhysicalAddress  ,NearestReligiousBuilding,NearestSchool,Longitude,Latitude,SyncEnumeratorId,StatusId,EnumeratorDeviceId,AppVersion,AppBuild)
	SELECT    UniqueId ,StartTime ,EndTime ,ProgrammeId ,RegDate ,SubLocationId,LocationId,Years,Months, @Id	 ,EnumeratorId  ,BeneFirstName,BeneMiddleName ,BeneSurname,BeneNationalIdNo,BeneSexId,BeneDoB,BenePhoneNumber,CgFirstName,CgMiddleName ,CgSurname  ,CgNationalIdNo,CgSexId,CgDoB,CgPhoneNumber,HouseholdMembers    ,Village  ,PhysicalAddress  ,NearestReligiousBuilding,NearestSchool,Longitude,Latitude,SyncEnumeratorId,@StatusId ,@EnumeratorDeviceId,AppVersion,AppBuild
	FROM (
		SELECT  	 
	 U.R.value('(Id)[1]','INT') AS Id
	,U.R.value('(UniqueId)[1]','VARCHAR(36)') AS UniqueId
	,U.R.value('(StartTime)[1]','DATETIME ') AS StartTime
	,U.R.value('(EndTime)[1]','DATETIME') AS EndTime
	,U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId
	,U.R.value('(RegDate)[1]','DATETIME') AS RegDate
	,U.R.value('(SubLocationId)[1]','INT') AS SubLocationId
	,U.R.value('(LocationId)[1]','INT') AS LocationId
	,U.R.value('(Years)[1]','INT') AS Years
	,U.R.value('(Months)[1]','INT') AS Months
	,U.R.value('(EnumeratorId)[1]','INT') AS EnumeratorId
	,U.R.value('(BeneFirstName)[1]','VARCHAR(25)') AS BeneFirstName
	,U.R.value('(BeneMiddleName)[1]','VARCHAR(25)') AS BeneMiddleName
	,U.R.value('(BeneSurname)[1]','VARCHAR(25)') AS BeneSurname 
	,U.R.value('(BeneNationalIdNo)[1]','VARCHAR(15)') AS BeneNationalIdNo
	,CASE WHEN U.R.value('(BeneSexId)[1]','INT') =0 THEN NULL ELSE U.R.value('(BeneSexId)[1]','INT') END AS BeneSexId
	,U.R.value('(BeneDoB)[1]','DATETIME') AS BeneDoB
	,U.R.value('(BenePhoneNumber)[1]','VARCHAR(15)') AS BenePhoneNumber
	,U.R.value('(CgFirstName)[1]','VARCHAR(25)') AS CgFirstName
	,U.R.value('(CgMiddleName)[1]','VARCHAR(25)') AS CgMiddleName
	,U.R.value('(CgSurname)[1]','VARCHAR(25)') AS CgSurname
	,U.R.value('(CgNationalIdNo)[1]','VARCHAR(15)') AS CgNationalIdNo
	,U.R.value('(CgPhoneNumber)[1]','VARCHAR(15)') AS CgPhoneNumber
	,U.R.value('(CgSexId)[1]','INT') AS CgSexId
	,U.R.value('(CgDoB)[1]','DATETIME ') AS CgDoB
	,U.R.value('(HouseholdMembers)[1]','INT') AS HouseholdMembers
	,U.R.value('(Village)[1]','VARCHAR(30)') AS Village
	,U.R.value('(PhysicalAddress)[1]','VARCHAR(50)') AS PhysicalAddress
	,U.R.value('(NearestReligiousBuilding)[1]','VARCHAR(50)') AS NearestReligiousBuilding
	,U.R.value('(NearestSchool)[1]','VARCHAR(50)') AS NearestSchool
	,U.R.value('(Longitude)[1]','FLOAT') AS Longitude
	,U.R.value('(Latitude)[1]','FLOAT') AS Latitude
	,U.R.value('(SyncEnumeratorId)[1]','INT ') AS SyncEnumeratorId
	,U.R.value('(AppVersion)[1]','VARCHAR(15)') AS AppVersion
	,U.R.value('(AppBuild)[1]','VARCHAR(15) ') AS AppBuild	
FROM @HouseHoldXml.nodes('HHListings/Record') AS U(R)
) T1
END

SELECT 0 AS StatusId, 'Success' AS [Description]
GO


 IF NOT OBJECT_ID('AcceptListingHH') IS NULL	
DROP PROC AcceptListingHH
GO

 CREATE PROC AcceptListingHH
 @TargetPlanId INT,
 @ConstituencyId INT ,
 @LocationId INT  = null,
 @BatchName VARCHAR(30),
 @UserId INT
 as
 BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @ListingAcceptId INT
  DECLARE @InsertDate DATE = GETDATE()
   DECLARE @SyncId INT
   DECLARE @AcceptId INT

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'	 
	 IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@TargetPlanId AND ApvBy>0)
	SET @ErrorMsg='The Registration Plan is not Approved. You cannot Accept Data into this Batch'

SET @SysCode='Registration Status'
SET @SysDetailCode='REGACC'
SELECT @AcceptId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysDetailCode='REGSYNC'
SELECT @SyncId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SELECT @ReceivedHHs = COUNT(Id) FROM ListingPlanHH T1
	INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = T1.SubLocationId  
																	  WHERE TG.ConstituencyId =@ConstituencyId 
																	  AND TG.LocationId = CASE WHEN @LocationId IS NOT NULL THEN @LocationId ELSE TG.LocationId END 
																	  AND T1.ListingAcceptId  IS NULL AND T1.StatusId = @SyncId
 
 IF(@ReceivedHHs=0)
 SET @ErrorMsg='The sub county has no Pending Data'

		IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	
BEGIN TRAN



INSERT INTO ListingAccept (AcceptById,AcceptDate,ReceivedHHs,BatchName,ConstituencyId,TargetPlanId) 
 SELECT @UserId, @InsertDate,@ReceivedHHs,@BatchName,@ConstituencyId,@TargetPlanId 
select @ListingAcceptId = Id from ListingAccept  where BatchName = @BatchName  AND TargetPlanId=@TargetPlanId
AND @ReceivedHHs = @ReceivedHHs AND ConstituencyId = @ConstituencyId 	AND AcceptDate = @InsertDate


BEGIN
UPDATE T1
SET T1.ListingAcceptId = @ListingAcceptId, T1.StatusId = @AcceptId
FROM ListingPlanHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId AND T1.RejectReason IS NULL
				AND TG.LocationId = CASE WHEN @LocationId IS NOT NULL THEN @LocationId ELSE TG.LocationId END 
				AND  TG.ConstituencyId =@ConstituencyId AND T1.ListingAcceptId  IS NULL  AND T1.StatusId = @SyncId 
END
						
					   
IF @@ERROR>0
BEGIN
	ROLLBACK TRAN
	SELECT 0 AS NoOfRows
END
ELSE
BEGIN
	COMMIT TRAN
	SELECT 1 AS NoOfRows
END
END
 GO



 IF NOT OBJECT_ID('RejectListingHH') IS NULL	
 DROP PROC RejectListingHH
 GO
CREATE PROC RejectListingHH
 @Id INT
,@Reason varchar(128) 
,@UserId int = null
AS 
BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @ListingAcceptId INT
  DECLARE @InsertDate DATE = GETDATE()


	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGSYNC' 
	SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode	
	
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'	 
	 IF NOT EXISTS(SELECT 1 FROM ListingPlanHH WHERE Id=@Id AND ListingAcceptId is not null and StatusId = @StatusId)
	SET @ErrorMsg='The Household is already  Accepted. You cannot Reject Accepted Households'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	  DECLARE @RejectedStatusId INT
	SET @SysDetailCode='REGREJ' 
	SELECT @RejectedStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode	

	UPDATE ListingPlanHH SET StatusId =@RejectedStatusId, RejectReason = @Reason, RejectById = @UserId, RejectDate = getdate()
	WHERE  Id = @Id and StatusId = @StatusId

	SELECT 0 as NoOfRows
END
GO
 
 
IF NOT OBJECT_ID('ApproveHHListingPlanBatch') IS NULL	
DROP PROC ApproveHHListingPlanBatch
GO
CREATE PROC ApproveHHListingPlanBatch
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @AcceptedId INT = NULL 
	DECLARE @ApprovedId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
 

	 IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'


IF NOT  EXISTS(SELECT  1 FROM ListingAccept WHERE Id = @Id AND AcceptApvById IS NULL  )
 SET @ErrorMsg='Household Listing Batch was not found or is Already Approved'
 
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

 SET @SysCode='Registration Status'
SET @SysDetailCode='REGACC'
SELECT @AcceptedId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysDetailCode='REGAPV'
SELECT @ApprovedId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	BEGIN TRAN	 
	UPDATE T1
	SET T1.AcceptApvById=@UserId ,T1.AcceptApvDate=GETDATE() FROM  ListingAccept T1 WHERE T1.Id=@Id AND T1.AcceptApvById IS NULL

	UPDATE T2 SET T2.StatusId = @ApprovedId from ListingPlanHH T2 WHERE T2.ListingAcceptId = @Id AND T2.StatusId = @AcceptedId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('FinalizeTargetPlan') IS NULL	
DROP PROC FinalizeTargetPlan
GO
CREATE PROC FinalizeTargetPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='ACTIVE'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM  TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Listing Exercise is not Active, It cannot be Finalized for Validation'
	
	IF  EXISTS(SELECT 1 FROM   ListingPlanHH WHERE  TargetPlanId=@Id  and ListingAcceptId is null and RejectReason = null )
		SET @ErrorMsg='The specified Listing Exercise  has data pending acceptance'
	

	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FinalizeBy=@UserId
	   ,T1.FinalizeOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO


IF NOT OBJECT_ID('FinalizeApvTargetPlan') IS NULL	
DROP PROC FinalizeApvTargetPlan
GO
CREATE PROC FinalizeApvTargetPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM  TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not Finalized, It cannot be Approved for  Validation'
	
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='IPRSVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FinalizeApvBy=@UserId
	   ,T1.FinalizeApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM  TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



  IF NOT OBJECT_ID('GenerateIPRSExportFile') IS NULL	
DROP PROC GenerateIPRSExportFile
GO
CREATE PROC GenerateIPRSExportFile
	 @TargetPlanId int
	,@FilePath varchar(128)
	,@DBServer varchar(30)
	,@DBName varchar(30)
	,@DBUser varchar(30)
	,@DBPassword varchar(30)
	,@UserId int 

AS
  
  BEGIN
  DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
  DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  DECLARE @ErrorMsg varchar(128)
  DECLARE @SystemCodeDetailId1 int
  DECLARE @SystemCodeDetailId2 int
  
  
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	
	  IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	
 IF EXISTS (SELECT 1 FROM   ListingPlanHH WHERE  TargetPlanId=@TargetPlanId and ListingAcceptId IS NULL AND RejectReason IS NULL )
	 SET @ErrorMsg='You cannot Generate Exceptions. There exists Households that have not been Accepted'

	   IF ISNULL(@ErrorMsg,'')<>''
		BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
   
   DELETE FROM temp_IprsExport WHERE TargetPlanId = @TargetPlanId
   
   
; WITH T1 AS 
(
SELECT    T1.BeneFirstName AS FirstName, T1.BeneMiddleName as Middlename, T1.BeneSurname as Surname, T1.BeneNationalIdNo as NationalIdNo, T2.Code as Sex, T1.BeneDoB as Date_Of_Birth, T1.TargetPlanId  FROM  ListingPlanHH T1
INNER JOIN SystemCodeDetail T2 ON T1.BeneSexId = T2.Id
LEFT OUTER JOIN (SELECT ID_Number AS IDNo FROM  IPRSCache) AS T3   
ON CONVERT(bigint,T1.BeneNationalIdNo)=CONVERT(bigint,T3.IDNo) 
WHERE T3.IDNo IS NULL    AND T1.BeneNationalIdNo IS NOT NULL
AND T1.TargetPlanId =@TargetPlanId
UNION
SELECT  T1.CgFirstName, T1.CgMiddleName, T1.CgSurname, T1.CgNationalIdNo, T2.Code, T1.CgDoB, T1.TargetPlanId   FROM ListingPlanHH T1
INNER JOIN SystemCodeDetail T2 ON T1.CgSexId = T2.Id
LEFT OUTER JOIN (SELECT ID_Number AS IDNo FROM  IPRSCache) AS T3   
ON CONVERT(bigint,T1.CgNationalIdNo)=CONVERT(bigint,T3.IDNo) 
WHERE T3.IDNo IS NULL 
AND T1.TargetPlanId =@TargetPlanId
)
INSERT INTO temp_IprsExport (
   FirstName
  ,Middlename
  ,Surname
  ,NationalIdNo
  ,Sex
  ,Date_Of_Birth
  ,TargetPlanId
)  
SELECT FirstName,Middlename,Surname,NationalIdNo,Sex,Date_Of_Birth,TargetPlanId  FROM T1
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='REG_IPRS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_IprsExport" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='IPRS_EXPORT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
	SELECT @FileCreationId AS FileCreationId
END
GO

 IF NOT OBJECT_ID('ImportBulkIPRS') IS NULL	
 DROP PROC ImportBulkIPRS
GO

CREATE PROC ImportBulkIPRS
AS
BEGIN
DECLARE @ErrorMsg varchar(128)
IF NOT EXISTS(SELECT 1 FROM temp_IPRSCache T1 LEFT JOIN IPRSCache T2  ON CONVERT(bigint,T1.ID_Number)=CONVERT(bigint,T2.ID_Number) WHERE T2.ID_Number IS NULL)
SET @ErrorMsg='All Imported Records are already in the Database!. Try another Batch'

 IF ISNULL(@ErrorMsg,'')<>''
		BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
INSERT INTO IPRSCache (First_Name,Surname,Middle_Name,ID_Number,Gender,Date_of_Birth,Date_of_Issue,Place_of_Birth,Serial_Number,Address,Status,DateCached)  
SELECT  T1.First_Name,T1.Surname,T1.Middle_Name,T1.ID_Number,T1.Gender,T1.Date_of_Birth,T1.Date_of_Issue,T1.Place_of_Birth,T1.Serial_Number,T1.[Address],T1.[Status],T1.DateCached
FROM temp_IPRSCache T1 LEFT JOIN IPRSCache T2  ON CONVERT(bigint,T1.ID_Number)=CONVERT(bigint,T2.ID_Number) WHERE T2.ID_Number IS NULL
DELETE FROM temp_IPRSCache
SELECT @@ROWCOUNT AS NoOfRows
END
GO


IF NOT OBJECT_ID('GenerateRegistrationExceptions') IS NULL	
DROP PROC GenerateRegistrationExceptions
GO
CREATE PROC GenerateRegistrationExceptions
	@TargetPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
 
 
 BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  ListingPlanHH WHERE TargetPlanId=@TargetPlanId and ListingAcceptId IS NULL  AND RejectReason IS   NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions --WHERE TargetPlanId = @TargetPlanId
	
 
	
; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T4.Code as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T6.Code as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 ListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId  AND T1.TargetPlanId =  @TargetPlanId

  LEFT OUTER JOIN SystemCodeDetail T4 ON T1.BeneSexId = T4.Id
	LEFT OUTER JOIN SystemCodeDetail T6 ON T1.CgSexId = T4.Id

	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,  Gender AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 AND  T1.TargetPlanId = @TargetPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,   Gender AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
		   
	   INSERT INTO temp_RegExceptions (Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,County,Constituency,District,Division,Location,SubLocation) 

	SELECT Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (Bene_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS Bene_IDNoExists,
	CASE WHEN (Bene_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_FirstNameExists,
	CASE WHEN (Bene_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_MiddleNameExists,
	CASE WHEN (Bene_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_SurnameExists,
	CASE WHEN (Bene_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBMatch,
	CASE WHEN (Bene_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBYearMatch,
	CASE WHEN (Bene_SexMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
	CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	TargetPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	WHERE 
	Bene_IDNoExists = 0 OR Bene_SexMatch = 0 OR Bene_DoBYearMatch = 0 OR ( Bene_FirstNameExists =0 AND Bene_MiddleNameExists=0 AND Bene_SurnameExists =0)
	OR 
	CG_IDNoExists = 0 OR CG_SexMatch = 0 OR CG_DoBYearMatch = 0 OR ( CG_FirstNameExists =0 AND CG_MiddleNameExists=0 AND  CG_SurnameExists =0)
	AND TargetPlanId=@TargetPlanId
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='HHLCOMVAL_EXS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_EXCEPTIONS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
  
  UPDATE T1
	SET T1.ExceptionsFileId=@FileCreationId
	FROM  TargetPlan T1
	WHERE T1.Id=@TargetPlanId
	SELECT @FileCreationId AS FileCreationId
  
 END

GO

IF NOT OBJECT_ID('GenerateCommunityValList') IS NULL	
DROP PROC GenerateCommunityValList
GO

CREATE PROC GenerateCommunityValList
	@TargetPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS

BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  ListingPlanHH WHERE TargetPlanId=@TargetPlanId and ListingAcceptId IS NULL AND RejectReason IS   NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions -- WHERE TargetPlanId = @TargetPlanId
	
; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T4.Code as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T6.Code as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 ListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId    AND T1.TargetPlanId =  @TargetPlanId


	LEFT OUTER JOIN SystemCodeDetail T4 ON T1.BeneSexId = T4.Id
	LEFT OUTER JOIN SystemCodeDetail T6 ON T1.CgSexId = T4.Id
	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,  Gender  AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 AND  T1.TargetPlanId = @TargetPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,  Gender  AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	   INSERT INTO temp_RegExceptions (Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,County,Constituency,District,Division,Location,SubLocation) 

	SELECT Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (Bene_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS Bene_IDNoExists,
	CASE WHEN (Bene_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_FirstNameExists,
	CASE WHEN (Bene_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_MiddleNameExists,
	CASE WHEN (Bene_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_SurnameExists,
	CASE WHEN (Bene_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBMatch,
	CASE WHEN (Bene_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBYearMatch,
	CASE WHEN (Bene_SexMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
	CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	TargetPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	WHERE 
	Bene_IDNoExists = 1 AND Bene_SexMatch = 1 AND Bene_DoBYearMatch = 1 AND ( Bene_FirstNameExists =1 OR Bene_MiddleNameExists=1 OR Bene_SurnameExists =1)
	OR 
	CG_IDNoExists = 1 AND CG_SexMatch = 1 AND CG_DoBYearMatch = 1 AND( CG_FirstNameExists =1 OR CG_MiddleNameExists=1 OR  CG_SurnameExists =1)
	AND TargetPlanId=@TargetPlanId
  
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='HHL_COMVALIST_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_VALIDATION'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
 

 ; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T4.Code as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T6.Code as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 ListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId    AND T1.TargetPlanId =  @TargetPlanId


	LEFT OUTER JOIN SystemCodeDetail T4 ON T1.BeneSexId = T4.Id
	LEFT OUTER JOIN SystemCodeDetail T6 ON T1.CgSexId = T4.Id
	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, Gender AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 AND  T1.TargetPlanId = @TargetPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, Gender AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	

  INSERT INTO   ListingException(Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,DateValidated) 
  
  
SELECT  DISTINCT X.Id,X.BeneName,X.BeneSex,X.BeneDob,REPLACE(X.BeneNationalIdNo,'.',''),X.IPRS_IDNo,X.IPRS_Name,X.IPRS_Sex,X.IPRS_DoB,
X.Bene_IDNoExists,X.Bene_FirstNameExists,X.Bene_MiddleNameExists,X.Bene_SurnameExists,X.Bene_DoBMatch,X.Bene_DoBYearMatch,
X.Bene_SexMatch,X.CgName,X.CgSex,X.CgDob,REPLACE(X.CGNationalIDNo,'.',''),X.IPRS_CG_IDNo,X.IPRS_CG_Name,X.IPRS_CG_Sex,X.IPRS_CG_DoB,
X.CG_IDNoExists,X.CG_FirstNameExists,X.CG_MiddleNameExists,X.CG_SurnameExists,X.CG_DoBMatch,X.CG_DoBYearMatch,
X.CG_SexMatch,X.TargetPlanId,GETDATE() AS DateValidated   FROM  X
 LEFT JOIN ListingException T2 ON X.Id = T2.Id 

  WHERE X.TargetPlanId=  @TargetPlanId and T2.Id is null
  

  UPDATE T1
	SET T1.ValidationFileId=@FileCreationId
  ,T1.ValBy = @UserId
  ,T1.ValOn = GETDATE()
	FROM  TargetPlan T1
	WHERE T1.Id=@TargetPlanId

	SELECT @FileCreationId AS FileCreationId
  
 END
  
GO


IF NOT OBJECT_ID('ApproveTargetPlanValidation') IS NULL	
DROP PROC ApproveTargetPlanValidation
GO
CREATE PROC ApproveTargetPlanValidation
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='IPRSVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in Validation stage'
	
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ValBy=@UserId
	   ,T1.ValOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO




IF NOT OBJECT_ID('AddEditComValHHListing') IS NULL	
DROP PROC AddEditComValHHListing
GO
CREATE PROC AddEditComValHHListing
	@ProgrammeId TINYINT
	   ,
	@HouseHoldXml XML
AS
BEGIN
	DECLARE @ErrorMsg varchar(256)
	DECLARE @SysCode varchar(20)
	DECLARE @EditingCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @EditingId int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @TargetPlanId int
	DECLARE @StatusId int
	DECLARE @CategoryId int
	DECLARE @Id int
	DECLARE @EnumeratorDeviceId  int = NULL
	DECLARE @DeviceId  varchar(36)


	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVAL'

	SELECT @StatusId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SET @SysCode='REG_CATEGORIES'
	SET @SysDetailCode='REGISTRATION'
	SELECT @CategoryId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @DeviceId = U.R.value('(DeviceId)[1]','VARCHAR(36)') , @Id = U.R.value('(Id)[1]','INT') FROM @HouseHoldXml.nodes('ComHHListings/ComHHListingVm') AS U(R)

	IF  EXISTS(SELECT 1
	FROM ComValListingPlanHH
	WHERE Id = @Id AND ComValListingAcceptId IS  NOT NULL ) 
	SET @ErrorMsg ='This Record has been submitted and Accepted'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @EnumeratorDeviceId =  Id
	from EnumeratorDevice
	where DeviceId = @DeviceId


	BEGIN


		IF ISNULL(@EnumeratorDeviceId,0)=0
	  INSERT INTO EnumeratorDevice
			( DeviceId, DeviceModel, DeviceManufacturer, DeviceName,[Version],VersionNumber, [Platform],Idiom,IsDevice, EnumeratorId)
		SELECT T1.DeviceId, DeviceModel, DeviceManufacturer, DeviceName, [Version], VersionNumber, [Platform], Idiom, IsDevice, EnumeratorId
		FROM
			(
	 SELECT
				U.R.value('(DeviceId)[1]','VARCHAR(36)') AS DeviceId
	, U.R.value('(DeviceModel)[1]','VARCHAR(15)') AS DeviceModel
	, U.R.value('(DeviceManufacturer)[1]','VARCHAR(15)') AS DeviceManufacturer
	, U.R.value('(DeviceName)[1]','VARCHAR(15)') AS DeviceName
	, U.R.value('(Version)[1]','VARCHAR(15)')  AS [Version]
	, U.R.value('(VersionNumber)[1]','VARCHAR(15) ') AS VersionNumber
	, U.R.value('(Platform)[1]','VARCHAR(15)') AS [Platform]
	, U.R.value('(Idiom)[1]','VARCHAR(10)') AS Idiom
	, U.R.value('(IsDevice)[1]','BIT') AS IsDevice
	, U.R.value('(SyncEnumeratorId)[1]','INT ') AS EnumeratorId
			FROM @HouseHoldXml.nodes('ComHHListings/ComHHListingVm') AS U(R)
) T1
		SELECT @EnumeratorDeviceId=IDENT_CURRENT('EnumeratorDevice')

		BEGIN TRAN

		IF  EXISTS(SELECT 1
		FROM ComValListingPlanHH
		WHERE Id = @Id AND ComValListingAcceptId IS  NULL ) 
	DELETE FROM ComValListingPlanHH WHERE Id = @Id

		INSERT INTO ComValListingPlanHH
			(Id,UniqueId ,StartTime ,EndTime ,ProgrammeId ,RegDate ,SubLocationId,LocationId,Years,Months,TargetPlanId ,EnumeratorId ,BeneFirstName,BeneMiddleName ,BeneSurname,BeneNationalIdNo,BeneSexId,BeneDoB,CgFirstName,CgMiddleName ,CgSurname ,CgNationalIdNo,CgSexId,CgDoB,HouseholdMembers ,Village ,PhysicalAddress ,NearestReligiousBuilding,NearestSchool,Longitude,Latitude,SyncEnumeratorId,StatusId,EnumeratorDeviceId,AppVersion,AppBuild,ComValDate,DownloadDate,SyncUpDate)
		SELECT Id, UniqueId , StartTime , EndTime , ProgrammeId , RegDate , SubLocationId, LocationId, Years, Months, TargetPlanId	 , EnumeratorId  , BeneFirstName, BeneMiddleName , BeneSurname, BeneNationalIdNo, BeneSexId, BeneDoB, CgFirstName, CgMiddleName , CgSurname  , CgNationalIdNo, CgSexId, CgDoB, HouseholdMembers    , Village  , PhysicalAddress  , NearestReligiousBuilding, NearestSchool, Longitude, Latitude, SyncEnumeratorId, StatusId , @EnumeratorDeviceId, AppVersion, AppBuild, ComValDate, DownloadDate, SyncUpDate
		FROM (
		SELECT
				U.R.value('(Id)[1]','INT') AS Id
	, U.R.value('(UniqueId)[1]','VARCHAR(36)') AS UniqueId
	, U.R.value('(StartTime)[1]','DATETIME ') AS StartTime
	, U.R.value('(EndTime)[1]','DATETIME') AS EndTime
	, U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId
	, U.R.value('(RegDate)[1]','DATETIME') AS RegDate
	, U.R.value('(SubLocationId)[1]','INT') AS SubLocationId
	, U.R.value('(LocationId)[1]','INT') AS LocationId
	, U.R.value('(Years)[1]','INT') AS Years
	, U.R.value('(Months)[1]','INT') AS Months
	, U.R.value('(TargetPlanId)[1]','INT') AS TargetPlanId
	, U.R.value('(EnumeratorId)[1]','INT') AS EnumeratorId
	, U.R.value('(BeneFirstName)[1]','VARCHAR(25)') AS BeneFirstName
	, U.R.value('(BeneMiddleName)[1]','VARCHAR(25)') AS BeneMiddleName
	, U.R.value('(BeneSurname)[1]','VARCHAR(25)') AS BeneSurname 
	, U.R.value('(BeneNationalIdNo)[1]','VARCHAR(15)') AS BeneNationalIdNo
	, CASE WHEN U.R.value('(BeneSexId)[1]','INT') =0 THEN NULL ELSE U.R.value('(BeneSexId)[1]','INT') END AS BeneSexId
	, U.R.value('(BeneDoB)[1]','DATETIME') AS BeneDoB
	, U.R.value('(CgFirstName)[1]','VARCHAR(25)') AS CgFirstName
	, U.R.value('(CgMiddleName)[1]','VARCHAR(25)') AS CgMiddleName
	, U.R.value('(CgSurname)[1]','VARCHAR(25)') AS CgSurname
	, U.R.value('(CgNationalIdNo)[1]','VARCHAR(15)') AS CgNationalIdNo
	, U.R.value('(CgSexId)[1]','INT') AS CgSexId
	, U.R.value('(CgDoB)[1]','DATETIME ') AS CgDoB
	, U.R.value('(HouseholdMembers)[1]','INT') AS HouseholdMembers
	, U.R.value('(Village)[1]','VARCHAR(30)') AS Village
	, U.R.value('(PhysicalAddress)[1]','VARCHAR(50)') AS PhysicalAddress
	, U.R.value('(NearestReligiousBuilding)[1]','VARCHAR(50)') AS NearestReligiousBuilding
	, U.R.value('(NearestSchool)[1]','VARCHAR(50)') AS NearestSchool
	, U.R.value('(Longitude)[1]','FLOAT') AS Longitude
	, U.R.value('(Latitude)[1]','FLOAT') AS Latitude
	, U.R.value('(SyncEnumeratorId)[1]','INT ') AS SyncEnumeratorId
	, U.R.value('(ComValDate)[1]','DATE ') AS ComValDate
	, U.R.value('(SyncUpDate)[1]','DATE ') AS SyncUpDate
	, U.R.value('(DownloadDate)[1]','DATE ') AS DownloadDate
	, U.R.value('(ComValListingAcceptId)[1]','INT ') AS ComValListingAcceptId
	, U.R.value('(StatusId)[1]','INT ') AS StatusId
	, U.R.value('(AppVersion)[1]','VARCHAR(15)') AS AppVersion
	, U.R.value('(AppBuild)[1]','VARCHAR(15) ') AS AppBuild
			--	,U.R.value('(ComValDate)[1]','DATE') AS ComValDate	
			FROM @HouseHoldXml.nodes('ComHHListings/ComHHListingVm') AS U(R)
) T1


		IF @@ERROR>0
	BEGIN
			ROLLBACK TRAN
			SELECT -1 AS StatusId, 'Error - ' + cast(@@ERROR as varchar(10)) AS [Description]
		END
	ELSE
	BEGIN
			COMMIT TRAN
			SELECT 0 AS StatusId, 'Success' AS [Description]
		END

	END
END
GO


 IF NOT OBJECT_ID('AcceptComValListingHH') IS NULL	
DROP PROC AcceptComValListingHH
GO
 CREATE PROC AcceptComValListingHH
 @TargetPlanId INT,
 @ConstituencyId INT ,
 @LocationId INT  = null,
 @BatchName VARCHAR(30),
 @UserId INT
 as
 BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @ComValListingAcceptId INT
  DECLARE @InsertDate DATE = GETDATE()
   DECLARE @SyncId INT
   DECLARE @AcceptId INT

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'	 
	 IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@TargetPlanId AND ApvBy<>0)
	SET @ErrorMsg='The Registration Plan is not Approved. You cannot Accept Data into this Batch'

SET @SysCode='Registration Status'
SET @SysDetailCode='REGACC'
SELECT @AcceptId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysDetailCode='REGSYNC'
SELECT @SyncId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 
 SELECT @ReceivedHHs = COUNT(Id) FROM  ComValListingPlanHH   T1
	INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = T1.SubLocationId  
																	  WHERE TG.ConstituencyId =@ConstituencyId 
																	  AND TG.LocationId = CASE WHEN @LocationId IS NOT NULL THEN @LocationId ELSE TG.LocationId END 
																	  AND T1.ComValListingAcceptId  IS NULL
 
 IF(@ReceivedHHs=0)
 SET @ErrorMsg='The sub county has no Pending Data'

		IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	
BEGIN TRAN


INSERT INTO ComValListingAccept (AcceptById,AcceptDate,ReceivedHHs,BatchName,ConstituencyId,TargetPlanId) 
 SELECT @UserId, @InsertDate,@ReceivedHHs,@BatchName,@ConstituencyId,@TargetPlanId 
select @ComValListingAcceptId = Id from ComValListingAccept  where BatchName = @BatchName  AND TargetPlanId=@TargetPlanId
AND @ReceivedHHs = @ReceivedHHs AND ConstituencyId = @ConstituencyId 	AND AcceptDate = @InsertDate


BEGIN
UPDATE T1
SET T1.ComValListingAcceptId = @ComValListingAcceptId  --, T1.StatusId = @AcceptId
FROM ComValListingPlanHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId --AND T1.RejectReason IS NULL
				AND TG.LocationId = CASE WHEN @LocationId IS NOT NULL THEN @LocationId ELSE TG.LocationId END 
				AND  TG.ConstituencyId =@ConstituencyId AND T1.ComValListingAcceptId   IS NULL   
END
						
					   
IF @@ERROR>0
BEGIN
	ROLLBACK TRAN
	SELECT 0 AS NoOfRows
END
ELSE
BEGIN
	COMMIT TRAN
	SELECT 1 AS NoOfRows
END
END
GO


IF NOT OBJECT_ID('ApproveComValHHListingPlanBatch') IS NULL	
DROP PROC ApproveComValHHListingPlanBatch
GO
CREATE PROC ApproveComValHHListingPlanBatch
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @AcceptedId INT = NULL 
	DECLARE @ApprovedId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
 

	 IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'


IF NOT  EXISTS(SELECT  1 FROM ComValListingAccept WHERE Id = @Id AND  AcceptApvById IS NULL  )
 SET @ErrorMsg='Household Listing Community Validation Batch was not found or is Already Approved'
 
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

 SET @SysCode='Registration Status'
SET @SysDetailCode='REGACC'
SELECT @AcceptedId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

--SET @SysDetailCode='REGAPV' 
--SELECT @ApprovedId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	BEGIN TRAN	 
	UPDATE T1
	SET T1.AcceptApvById=@UserId ,T1.AcceptApvDate=GETDATE() FROM  ComValListingAccept T1 WHERE T1.Id=@Id AND T1.AcceptApvById IS NULL
	--UPDATE T2 SET T2.StatusId = @ApprovedId from ComValListingPlanHH T2 WHERE T2.ComValListingAcceptId = @Id AND T2.StatusId = @AcceptedId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO




IF NOT OBJECT_ID('ProcessComVal') IS NULL	
DROP PROC ProcessComVal
GO
CREATE PROC ProcessComVal
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the Community Validation stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVALAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ComValBy=@UserId
	   ,T1.ComValOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('ApproveComVal') IS NULL	
DROP PROC ApproveComVal
GO
CREATE PROC ApproveComVal
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVALAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the Community Validation stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='POSTREG'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ComValApvBy=@UserId
	   ,T1.ComValApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('GeneratePostHHListingExceptions') IS NULL	
DROP PROC GeneratePostHHListingExceptions
GO

CREATE PROC GeneratePostHHListingExceptions
	@TargetPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'

	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 
DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions WHERE TargetPlanId = @TargetPlanId
	
	
; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T4.Code as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T6.Code as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	  ComValListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId   AND  T1.TargetPlanId = @TargetPlanId


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,  Gender  AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	LEFT OUTER JOIN SystemCodeDetail T4 ON T1.BeneSexId = T4.Id
	LEFT OUTER JOIN SystemCodeDetail T6 ON T1.CgSexId = T4.Id
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo,  Gender  AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	 


	   INSERT INTO temp_RegExceptions (Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,County,Constituency,District,Division,Location,SubLocation) 
	
	SELECT Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (Bene_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS Bene_IDNoExists,
	CASE WHEN (Bene_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_FirstNameExists,
	CASE WHEN (Bene_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_MiddleNameExists,
	CASE WHEN (Bene_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_SurnameExists,
	CASE WHEN (Bene_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBMatch,
	CASE WHEN (Bene_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBYearMatch,
	CASE WHEN (Bene_SexMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
	CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	TargetPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	 
	 
	WHERE 
	Bene_IDNoExists = 0 OR Bene_SexMatch = 0 OR Bene_DoBYearMatch = 0 OR ( Bene_FirstNameExists =0 OR Bene_MiddleNameExists=0 OR Bene_SurnameExists =0)
	OR 
	CG_IDNoExists = 0 OR CG_SexMatch = 0 OR CG_DoBYearMatch = 0 OR ( CG_FirstNameExists =0 OR CG_MiddleNameExists=0 OR  CG_SurnameExists =0)
		AND TargetPlanId=@TargetPlanId
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='POSTHHL_EXCEPTIONS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_EXCEPTIONS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
  
  UPDATE T1
	SET T1.PostRegExceptionsFileId=@FileCreationId, 
	T1.PostRegExceptionBy = @UserId, 
	T1.PostRegExceptionOn =	GETDATE()
	FROM  TargetPlan T1
	WHERE T1.Id=@TargetPlanId
	SELECT @FileCreationId AS FileCreationId
  
 END
GO





IF NOT OBJECT_ID('CloseTargetPlan') IS NULL	
DROP PROC CloseTargetPlan
GO
CREATE PROC CloseTargetPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='POSTREG'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the closure stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='CLOSED'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ClosedBy=@UserId
	   ,T1.ClosedOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO