

DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW varchar(20)
DECLARE @SysRightCode_ENTRY varchar(20)
DECLARE @SysRightCode_MODIFIER varchar(20)
DECLARE @SysRightCode_DELETION varchar(20)
DECLARE @SysRightCode_APPROVAL varchar(20)
DECLARE @SysRightCode_FINALIZE varchar(20)
DECLARE @SysRightCode_VERIFY varchar(20)
DECLARE @SysRightCode_EXPORT varchar(20)
DECLARE @SysRightCode_DOWNLOAD varchar(20)
DECLARE @SysRightCode_UPLOAD varchar(20)

DECLARE @SysRightId_VIEW int
DECLARE @SysRightId_ENTRY int
DECLARE @SysRightId_MODIFIER int
DECLARE @SysRightId_DELETION int
DECLARE @SysRightId_APPROVAL int
DECLARE @SysRightId_FINALIZE int
DECLARE @SysRightId_VERIFY int
DECLARE @SysRightId_EXPORT int
DECLARE @SysRightId_DOWNLOAD int
DECLARE @SysRightId_UPLOAD int

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD


SET @ModuleCode='Enrolment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='PSP-Profile'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='PSP-Enrolment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Programmes'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Donors'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='PSPs'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='PSPBranches'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='GeoUnits'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='General Parameters'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Expansion Plans'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='User Groups'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Users'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Database Backups'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL


SET @ModuleCode='Payment Cycle'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,@SysRightCode_DELETION
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Payment Detail'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Pre-Payroll'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'RUN & GENERATE EXCEPTIONS'
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,'RE-RUN'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACTION'	--FOR ACTIONING
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Request For Funds'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Payroll'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'PROCESS'
		UNION
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
		UNION
		SELECT @ModuleId,@SysRightId_EXPORT,'SHARE'
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='PSP-Payment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_DOWNLOAD,@SysRightCode_DOWNLOAD
		UNION
		SELECT @ModuleId,@SysRightId_UPLOAD,@SysRightCode_UPLOAD
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

INSERT INTO GroupRight(UserGroupId,ModuleRightId,CreatedBy,CreatedOn)
SELECT T1.Id AS UserGroupId,T2.Id AS ModuleRightId,1 AS CreatedBy,GETDATE() AS CreatedOn
FROM UserGroup T1 CROSS JOIN ModuleRight T2
				  LEFT JOIN GroupRight T3 ON T1.Id=T3.UserGroupId AND T2.Id=T3.ModuleRightId
WHERE T1.Name='System' AND T3.UserGroupId IS NULL
