﻿IF NOT OBJECT_ID('GetProgOfficerSummary') IS NULL	
DROP PROC GetProgOfficerSummary
GO
 CREATE PROC GetProgOfficerSummary @UserId int
 AS
 BEGIN
 DECLARE @Exists INT =0
  SELECT @Exists = COUNT(Id)   FROM   ProgrammeOfficer WHERE UserId = @UserId  
   IF(@Exists>0)
   SELECT TOP 1 CountyId, ConstituencyId, UserId FROM ProgrammeOfficer WHERE UserId = @UserId  ORDER BY 1 DESC
   ELSE 
   SELECT NULL AS CountyId,NULL AS ConstituencyId, @UserId AS UserId FROM ProgrammeOfficer 
 END
 GO

IF NOT OBJECT_ID('GetPendingRegistrationHH') IS NULL	
DROP PROC GetPendingRegistrationHH
GO
CREATE PROC GetPendingRegistrationHH
@RegPlanId INT=NULL
AS 
BEGIN
SELECT  
COUNT(Id) AS PendingHHs, 
TG.ConstituencyId, 
@RegPlanId AS RegPlanId,
TG.Constituency, TG.County	
FROM RegistrationHH T1
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
INNER JOIN Division T3 ON T2.DivisionId=T3.Id
INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
INNER JOIN District T5 ON T4.DistrictId=T5.Id
INNER JOIN County T6 ON T4.CountyId=T6.Id
INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId   
AND T1.RegAcceptId  IS NULL and T1.RegPlanId = @RegPlanId
GROUP BY TG.ConstituencyId, TG.Constituency,
TG.County
END
GO


IF NOT OBJECT_ID('GetPendingRegistrationHHCv') IS NULL	
DROP PROC GetPendingRegistrationHHCv
GO
CREATE PROC GetPendingRegistrationHHCv
@RegPlanId INT=NULL
AS 
BEGIN
SELECT  
COUNT(Id) AS PendingHHs, 
TG.ConstituencyId, 
@RegPlanId AS RegPlanId,
TG.Constituency, TG.County	
FROM RegistrationHHCv T1
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
INNER JOIN Division T3 ON T2.DivisionId=T3.Id
INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
INNER JOIN District T5 ON T4.DistrictId=T5.Id
INNER JOIN County T6 ON T4.CountyId=T6.Id
INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.SubLocationId  = T1.SubLocationId   
AND T1.RegAcceptCvId  IS NULL and T1.RegPlanId = @RegPlanId
GROUP BY TG.ConstituencyId, TG.Constituency,
TG.County
END
GO


IF NOT OBJECT_ID('GetRegistrationSummary') IS NULL	
 DROP PROC GetRegistrationSummary
GO
CREATE PROC GetRegistrationSummary
@RegPlanId int
AS
BEGIN

 
SELECT
 MAX(T1.ExceptionsFileId) AS ExceptionsFileId
,MAX(T1.ValidationFileId) AS ValidationFileId 
,SUM(CASE WHEN(T2.Id>0) THEN 1 ELSE 0 END) AS RegisteredHouseholds
,MAX(T5.AcceptedBatches) AS AcceptedBatches
,SUM(CASE WHEN(T2.RegAcceptId>0) THEN 1 ELSE 0 END) AS AcceptedHouseholds
,SUM(CASE WHEN(T2.RegAcceptId IS NULL AND T2.Id>0) THEN 1 ELSE 0 END) AS PendingHouseholds
,SUM(CASE WHEN(T4.CG_IDNoExists>0) THEN 1 ELSE 0 END) AS CG_IDNoExists
,SUM(CASE WHEN(T4.CG_SexMatch>0) THEN 1 ELSE 0 END) AS CGSexMatch
,SUM(CASE WHEN(T4.CG_DoBYearMatch>0) THEN 1 ELSE 0 END) AS CG_DoBYearMatch
,SUM(CASE WHEN(T4.CG_DoBMatch>0) THEN 1 ELSE 0 END) AS CG_DoBMatch
,SUM(CASE WHEN(T4.HHD_IDNoExists>0) THEN 1 ELSE 0 END) AS HHDIDNoExists
,SUM(CASE WHEN(T4.HHD_SexMatch>0) THEN 1 ELSE 0 END) AS HHDSexMatch
,SUM(CASE WHEN(T4.HHD_DoBYearMatch>0) THEN 1 ELSE 0 END) AS HHDDoBYearMatch
,SUM(CASE WHEN(T4.HHD_DoBMatch>0) THEN 1 ELSE 0 END) AS HHDDoBMatch
FROM   RegPlan  T1 
LEFT JOIN RegistrationHH T2 ON T1.Id =T2.RegPlanId
LEFT JOIN ( SELECT DISTINCT Id, RegPlanId , ReceivedHHs, ConstituencyId  FROM RegAccept)  T3 ON T1.Id =  T3.RegPlanId AND T2.RegAcceptId = T3.Id
LEFT JOIN ( SELECT DISTINCT Id, CG_IDNoExists,CG_DoBYearMatch,CG_SexMatch, HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,HHD_IDNoExists,CG_DoBMatch,RegPlanId FROM RegException)   T4 ON T1.Id =  T4.RegPlanId AND T2.Id = T4.Id
LEFT JOIN ( SELECT RegPlanId,COUNT(Id) AS AcceptedBatches FROM RegAccept GROUP BY RegPlanId ) T5 ON T3.Id=T5.RegPlanId

WHERE  T1.Id =@RegPlanId
END
GO





IF NOT OBJECT_ID('GetRegCvSummary') IS NULL	
DROP PROC GetRegCvSummary
GO
CREATE PROC GetRegCvSummary 
@RegPlanId int 

AS
BEGIN
 
 SELECT 
COUNT(T1.Id) AS RegHH,
SUM(CASE  WHEN (T3.Code ='EXITED' AND  T2.RegAcceptCvId IS NOT  NULL) THEN 1 ELSE 0 END)  AS ExitedHH,
SUM(CASE  WHEN (T3.Code ='AMENDED'  AND  T2.RegAcceptCvId IS NOT  NULL) THEN 1 ELSE 0 END)  AS AmendedHH,
SUM(CASE  WHEN (T3.Code ='CONFIRMED'  AND  T2.RegAcceptCvId IS NOT  NULL) THEN 1 ELSE 0 END)  AS ConfirmedHH,
SUM( CASE WHEN(ISNULL(T2.Id,0)>0  AND  T2.RegAcceptCvId IS NOT  NULL) then 1 else 0 end)  AS CValHH,
SUM( CASE WHEN(ISNULL(T2.Id,0)>0  AND  T2.RegAcceptCvId IS   NULL) then 1 else 0 end)  AS PendingAcceptance
FROM RegistrationHH T1
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
INNER JOIN Division T3 ON T2.DivisionId=T3.Id
INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
INNER JOIN District T5 ON T4.DistrictId=T5.Id
INNER JOIN County T6 ON T4.CountyId=T6.Id
INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 	  AS TG ON TG.SubLocationId  = T1.SubLocationId  
LEFT OUTER  JOIN RegistrationHHCv T2 ON T1.Id =T2.RegistrationHHId   
INNER JOIN SystemCodeDetail T3 ON T2.CvStatusId = T3.Id
AND T1.RegPlanId = @RegPlanId
END

GO



IF NOT OBJECT_ID('GetTarPlanProgrammeOptions') IS NULL	
DROP PROC GetTarPlanProgrammeOptions
GO

IF NOT OBJECT_ID('GetTarPlanProgrammeOptions') IS NULL	
DROP PROC GetTarPlanProgrammeOptions
GO
CREATE PROC GetTarPlanProgrammeOptions
AS
BEGIN
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @CategoryId INT
DECLARE @StatusId INT

SET @SysCode='REG_STATUS'
SET @SysDetailCode='CLOSED'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
SELECT  T1.Id RegPlanId, T2.Id ProgrammeId,  CONCAT(T1.[Name],' - ', T2.[Name]) AS  TarName FROM  RegPlan T1
LEFT JOIN Programme T2 ON 1=1
LEFT JOIN TarPlanProgramme T3 ON T2.Id = T3.ProgrammeId  AND T1.Id = T3.RegPlanId
AND T2.Id <>1
WHERE T1.StatusId = @StatusId  AND T3.TarPlanId IS NULL
END
GO
