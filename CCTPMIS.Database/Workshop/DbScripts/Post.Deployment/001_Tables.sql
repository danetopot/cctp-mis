﻿
IF NOT OBJECT_ID('temp_IprsExport') IS NULL	
DROP TABLE temp_IprsExport
GO
IF NOT OBJECT_ID('temp_RegExceptions') IS NULL	
DROP TABLE temp_RegExceptions
GO
IF NOT OBJECT_ID('RegistrationHHCv') IS NULL	
DROP TABLE RegistrationHHCv
GO
 IF NOT OBJECT_ID('RegAcceptCv') IS NULL	
DROP TABLE RegAcceptCv
GO

IF NOT OBJECT_ID('RegistrationHH') IS NULL	
DROP TABLE RegistrationHH
GO

IF NOT OBJECT_ID('RegException') IS NULL	
DROP TABLE RegException
GO
IF NOT OBJECT_ID('RegAccept') IS NULL	
DROP TABLE RegAccept
GO

IF NOT OBJECT_ID('TargetingHH') IS NULL	
DROP TABLE TargetingHH
GO
IF NOT OBJECT_ID('TarPlanProgramme') IS NULL	
DROP TABLE TarPlanProgramme
GO

IF NOT OBJECT_ID('RegPlan') IS NULL	
DROP TABLE RegPlan
GO






CREATE TABLE RegPlan(
	Id INT NOT NULL IDENTITY(1,1)
   ,[Name] varchar(30) NOT NULL
   ,[Description] varchar(128) NOT NULL
   ,[Start] datetime NOT NULL
   ,[End] datetime NOT NULL
   ,TargetHHs int
   ,CategoryId int NOT NULL
   ,StatusId int NOT NULL
   ,CreatedBy INT NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy INT NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,ClosedBy int NULL
   ,ClosedOn datetime NULL
   ,FinalizeBy int NULL
   ,FinalizeOn datetime NULL
   ,FinalizeApvBy int NULL
   ,FinalizeApvOn datetime NULL  
   ,ExceptionBy int NULL
   ,ExceptionOn datetime NULL
   ,ValBy int NULL
   ,ValOn datetime NULL
   ,ExceptionsFileId int NULL
   ,ValidationFileId int NULL
   ,ComValBy int NULL
   ,ComValOn datetime NULL
   ,ComValApvBy int NULL
   ,ComValApvOn datetime NULL
   ,PostRegExceptionBy int NULL
   ,PostRegExceptionOn datetime NULL
   ,PostRegExceptionsFileId int NULL
   ,CONSTRAINT PK_RegPlan PRIMARY KEY (Id)
   ,CONSTRAINT FK_RegPlan_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ApvBy FOREIGN KEY (ApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ClosedBy FOREIGN KEY (ClosedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_FinalizeBy FOREIGN KEY (FinalizeBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_FinalizeApvBy FOREIGN KEY (FinalizeApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ComValBy FOREIGN KEY (ComValBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ComValApvBy FOREIGN KEY (ComValApvBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ValBy FOREIGN KEY (ValBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_ExceptionBy FOREIGN KEY (ExceptionBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_User_PostRegExceptionBy FOREIGN KEY (PostRegExceptionBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION  
   ,CONSTRAINT FK_RegPlan_SystemCodeDetail_CategoryId FOREIGN KEY (CategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_FileCreation_PostRegExceptionsFileId FOREIGN KEY (PostRegExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_FileCreation_ExceptionsFileId FOREIGN KEY (ExceptionsFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegPlan_FileCreation_ValidationFileId FOREIGN KEY (ValidationFileId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO
te
CREATE TABLE RegAccept(
	Id INT NOT NULL IDENTITY(1,1)
   ,AccById int NOT NULL
   ,AccDate DATETIME NOT NULL DEFAULT GETDATE()
   ,AccApvById INT  NULL
   ,AccApvDate DATETIME  NULL
   ,ReceivedHHs int NOT NULL                                                   
   ,BatchName VARCHAR(20) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,RegPlanId INT NOT NULL
   ,CONSTRAINT PK_RegAccept PRIMARY KEY (Id)
   ,CONSTRAINT FK_RegAccept_User_AccById FOREIGN KEY (AccById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_User_AccApvById FOREIGN KEY (AccApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_User_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO


CREATE TABLE RegException(
   Id                    INTEGER  NOT NULL PRIMARY KEY 
  ,HHdName               VARCHAR(50) NOT NULL
  ,HHdSex                INTEGER  NOT NULL
  ,HHdDob                VARCHAR(50) NOT NULL
  ,HHdNationalIdNo       VARCHAR(20)  NOT NULL
  ,IPRS_IDNo             BIGINT   NULL
  ,IPRS_Name             VARCHAR(50)  NULL
  ,IPRS_Sex              INTEGER   NULL
  ,IPRS_DoB              DATE   NULL
  ,HHD_IDNoExists        BIT  NOT NULL
  ,HHD_FirstNameExists   BIT  NOT NULL
  ,HHD_MiddleNameExists  BIT  NOT NULL
  ,HHD_SurnameExists BIT  NOT NULL
  ,HHD_DoBMatch          BIT  NOT NULL
  ,HHD_DoBYearMatch      BIT  NOT NULL
  ,HHD_SexMatch          BIT  NOT NULL
  ,CgName                VARCHAR(50)
  ,CgSex                 INTEGER  NOT NULL
  ,CgDob                 DATE NOT NULL
  ,CgNationalIdNo        VARCHAR(20) NULL
  ,IPRS_CG_IDNo          VARCHAR(20) NULL
  ,IPRS_CG_Name          VARCHAR(50) NULL
  ,IPRS_CG_Sex           VARCHAR(2) NULL
  ,IPRS_CG_DoB           DATE   NULL
  ,CG_IDNoExists         BIT  NOT NULL
  ,CG_FirstNameExists    BIT  NOT NULL
  ,CG_MiddleNameExists   BIT  NOT NULL
  ,CG_SurnameExists      BIT  NOT NULL
  ,CG_DoBMatch           BIT  NOT NULL
  ,CG_DoBYearMatch       BIT  NOT NULL
  ,CG_SexMatch           BIT  NOT NULL
  ,RegPlanId		     INT NOT NULL
  ,DateValidated         DATETIME NOT NULL
)
GO



CREATE TABLE RegistrationHH(
		 Id INT  NOT NULL IDENTITY(1,1)
		,UniqueId       VARCHAR(36)
		,StartTime      DATETIME NULL
		,EndTime        DATETIME NULL
		,ProgrammeId    TINYINT  NOT NULL
		,RegistrationDate DATETIME NOT NULL
		,SubLocationId  INT  NOT NULL
		,LocationId     INT  NOT NULL
		,Years		  INT  NOT NULL
		,Months		  INT  NOT NULL
		,RegPlanId	   INT  NOT NULL
		,EnumeratorId    INT  NOT NULL
		,HHdFirstName    VARCHAR(25) NOT NULL
		,HHdMiddleName   VARCHAR(25)   NULL
		,HHdSurname      VARCHAR(25) NOT NULL
		,HHdNationalIdNo VARCHAR(15)  NOT NULL
		,HHdSexId       INT  NOT NULL
		,HHdDoB         DATETIME NOT NULL
		,CgFirstName    VARCHAR(25) NOT NULL
		,CgMiddleName   VARCHAR(25)   NULL
		,CgSurname      VARCHAR(25) NOT NULL
		,CgNationalIdNo VARCHAR(15)  NOT NULL
		,CgSexId        INT  NOT NULL
		,CgDoB          DATETIME NOT NULL
		,HouseholdMembers INT  NOT NULL
		,StatusId       INT  NOT NULL
		,Village        VARCHAR(50) NOT NULL
		,PhysicalAddress VARCHAR(50) NOT NULL
		,NearestReligiousBuilding VARCHAR(50) NOT NULL
		,NearestSchool  VARCHAR(50) NOT NULL
		,Longitude     FLOAT NOT NULL
		,Latitude       FLOAT NOT NULL
		,SyncEnumeratorId   INT  NOT NULL
		,DeviceId       VARCHAR(36) NOT NULL
		,DeviceModel    VARCHAR(15) NOT NULL
		,DeviceManufacturer  VARCHAR(15) NOT NULL
		,DeviceName       VARCHAR(15) NOT NULL
		,[Version]        VARCHAR(15)  NOT NULL
		,VersionNumber  VARCHAR(15)  NOT NULL
		,AppVersion     VARCHAR(15) NOT NULL
		,AppBuild       VARCHAR(15)  NOT NULL
		,[Platform]  VARCHAR(15) NOT NULL
		,Idiom VARCHAR(10) NOT NULL
		,IsDevice       BIT NOT NULL
		,RegAcceptId INT NULL
		,CONSTRAINT PK_RegistrationHH PRIMARY KEY (Id)
		,CONSTRAINT FK_RegistrationHH_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	    ,CONSTRAINT FK_RegistrationHH_RegPlan_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_HHdSexId FOREIGN KEY (HHdSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_RegAccept_RegAcceptId FOREIGN KEY (RegAcceptId) REFERENCES RegAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE RegAcceptCv(
	Id INT NOT NULL IDENTITY(1,1)
   ,AccById int NOT NULL
   ,AccDate DATETIME NOT NULL DEFAULT GETDATE()
   ,AccApvById INT  NULL
   ,AccApvDate DATETIME  NULL
   ,ReceivedHHs int NOT NULL                                                   
   ,BatchName VARCHAR(20) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,RegPlanId INT NOT NULL
   ,CONSTRAINT PK_RegAcceptCv PRIMARY KEY (Id)
   ,CONSTRAINT FK_RegAcceptCv_User_AccById FOREIGN KEY (AccById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAcceptCv_User_AccApvById FOREIGN KEY (AccApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAcceptCv_User_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAcceptCv_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO

CREATE TABLE RegistrationHHCv(
		 RegistrationHHId INT  NOT NULL
        ,Id INT NOT NULL       
		,UniqueId       VARCHAR(36)
		,StartTime      DATETIME NULL
		,EndTime        DATETIME NULL
		,ProgrammeId    TINYINT  NOT NULL
		,RegistrationDate DATETIME NOT NULL
		,SubLocationId  INT  NOT NULL
		,LocationId     INT  NOT NULL
		,Years		  INT  NOT NULL
		,Months		  INT  NOT NULL
		,RegPlanId	   INT  NOT NULL
		,EnumeratorId    INT  NOT NULL
		,HHdFirstName    VARCHAR(25) NOT NULL
		,HHdMiddleName   VARCHAR(25)   NULL
		,HHdSurname      VARCHAR(25) NOT NULL
		,HHdNationalIdNo VARCHAR(15)  NOT NULL
		,HHdSexId       INT  NOT NULL
		,HHdDoB         DATETIME NOT NULL
		,CgFirstName    VARCHAR(25) NOT NULL
		,CgMiddleName   VARCHAR(25)   NULL
		,CgSurname      VARCHAR(25) NOT NULL
		,CgNationalIdNo VARCHAR(15)  NOT NULL
		,CgSexId        INT  NOT NULL
		,CgDoB          DATETIME NULL
		,HouseholdMembers INT  NOT NULL
		,StatusId       INT  NOT NULL
		,Village        VARCHAR(30) NOT NULL
		,PhysicalAddress VARCHAR(50) NOT NULL
		,NearestReligiousBuilding VARCHAR(50) NOT NULL
		,NearestSchool  VARCHAR(50) NOT NULL
		,Longitude     FLOAT NOT NULL
		,Latitude       FLOAT NOT NULL
		,SyncEnumeratorId   INT  NOT NULL
		,DeviceId       VARCHAR(36) NOT NULL
		,DeviceModel    VARCHAR(15) NOT NULL
		,DeviceManufacturer  VARCHAR(15) NOT NULL
		,DeviceName       VARCHAR(15) NOT NULL
		,[Version]        VARCHAR(15)  NOT NULL
		,VersionNumber  VARCHAR(15)  NOT NULL
		,AppVersion     VARCHAR(15) NOT NULL
		,AppBuild       VARCHAR(15)  NOT NULL
		,[Platform]  VARCHAR(15) NOT NULL
		,Idiom VARCHAR(10) NOT NULL
		,IsDevice       BIT NOT NULL
		,RegAcceptId INT NOT NULL
        ,RegAcceptCvId INT NULL
		,CvStatusId INT NOT NULL
        ,CvEnumeratorId INT NOT NULL
        ,CvSyncDownDate DATETIME NOT NULL
		,CvUpdateDate DATETIME NOT NULL
		,CvSyncUpDate DATETIME NOT NULL
        ,CvSyncEnumeratorId INT NOT NULL
		,CONSTRAINT PK_RegistrationHHCv PRIMARY KEY (Id)
		,CONSTRAINT FK_RegistrationHHCv_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	    ,CONSTRAINT FK_RegistrationHHCv_RegPlan_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_SystemCodeDetail_HHdSexId FOREIGN KEY (HHdSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHHCv_SystemCodeDetail_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHHCv_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHHCv_RegAccept_RegAcceptId FOREIGN KEY (RegAcceptId) REFERENCES RegAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHHCv_RegAcceptCv_RegAcceptCvId FOREIGN KEY (RegAcceptCvId) REFERENCES RegAcceptCv(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_SystemCodeDetail_CvStatusId FOREIGN KEY (CvStatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_Enumerator_CvSyncEnumeratorId FOREIGN KEY (CvEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHHCv_Enumerator_CvEnumeratorId FOREIGN KEY (CvEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
 )
GO



CREATE TABLE temp_RegExceptions(
	Id                    INTEGER  NOT NULL PRIMARY KEY 
	,HHdName               VARCHAR(50) NOT NULL
	,HHdSex                INTEGER  NOT NULL
	,HHdDob                VARCHAR(50) NOT NULL
	,HHdNationalIdNo       INTEGER  NOT NULL
	,IPRS_IDNo             INTEGER    NULL
	,IPRS_Name             VARCHAR(50)   NULL
	,IPRS_Sex              INTEGER    NULL
	,IPRS_DoB              DATE    NULL
	,HHD_IDNoExists        BIT  NOT NULL
	,HHD_FirstNameExists   BIT  NOT NULL
	,HHD_MiddleNameExists  BIT  NOT NULL
	,HHD_SurnameExists BIT  NOT NULL
	,HHD_DoBMatch          BIT  NOT NULL
	,HHD_DoBYearMatch      BIT  NOT NULL
	,HHD_SexMatch          BIT  NOT NULL
	,CgName                VARCHAR(50)
	,CgSex                 INTEGER  NOT NULL
	,CgDob                 DATE NOT NULL
	,CgNationalIdNo        VARCHAR(20) NULL
	,IPRS_CG_IDNo          VARCHAR(20) NULL
	,IPRS_CG_Name          VARCHAR(50) NULL
	,IPRS_CG_Sex           VARCHAR(2) NULL
	,IPRS_CG_DoB           DATE   NULL
	,CG_IDNoExists         BIT  NOT NULL
	,CG_FirstNameExists    BIT  NOT NULL
	,CG_MiddleNameExists   BIT  NOT NULL
	,CG_SurnameExists      BIT  NOT NULL
	,CG_DoBMatch           BIT  NOT NULL
	,CG_DoBYearMatch       BIT  NOT NULL
	,CG_SexMatch           BIT  NOT NULL
	,County varchar(30)
	,Constituency varchar(30)
	,District varchar(30)
	,Division varchar(30)
	,Location varchar(30)
	,SubLocation varchar(30)
	,RegPlanId		     INT NOT NULL
)
GO









-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


CREATE TABLE TargetingHH(
 RegistrationHHId INT  NOT NULL
,Id INT NOT NULL       
,UniqueId       VARCHAR(36)
,StartTime      DATETIME NULL
,EndTime        DATETIME NULL
,ProgrammeId    TINYINT  NOT NULL
,RegistrationDate DATETIME NOT NULL
,SubLocationId  INT  NOT NULL
,LocationId     INT  NOT NULL
,Years		  INT  NOT NULL
,Months		  INT  NOT NULL
,RegPlanId	   INT  NOT NULL
,TarPlanId	   INT  NOT NULL
,EnumeratorId    INT  NOT NULL 
,SyncEnumeratorId    INT  NOT NULL
,HouseholdMembers INT  NOT NULL
,StatusId       INT  NOT NULL
,Village        VARCHAR(30) NOT NULL
,PhysicalAddress VARCHAR(50) NOT NULL
,NearestReligiousBuilding VARCHAR(50) NOT NULL
,NearestSchool  VARCHAR(50) NOT NULL
,Longitude     FLOAT NOT NULL
,Latitude       FLOAT NOT NULL
,DeviceId       VARCHAR(36) NOT NULL
,DeviceModel    VARCHAR(15) NOT NULL
,DeviceManufacturer  VARCHAR(15) NOT NULL
,DeviceName       VARCHAR(15) NOT NULL
,[Version]        VARCHAR(15)  NOT NULL
,VersionNumber  VARCHAR(15)  NOT NULL
,AppVersion     VARCHAR(15) NOT NULL
,AppBuild       VARCHAR(15)  NOT NULL
,[Platform]  VARCHAR(15) NOT NULL
,Idiom VARCHAR(10) NOT NULL
,IsDevice       BIT NOT NULL
,TarAcceptId INT  NULL
,SyncDownDate DATETIME NOT NULL
,UpdateDate DATETIME NOT NULL
,SyncUpDate DATETIME NOT NULL
,CONSTRAINT PK_TargetingHH PRIMARY KEY (Id)
,CONSTRAINT FK_TargetingHH_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_RegPlan_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_RegPlan_TarPlanId FOREIGN KEY (TarPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
,CONSTRAINT FK_TargetingHH_RegAccept_TarAcceptId FOREIGN KEY (TarAcceptId) REFERENCES RegAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO

CREATE TABLE TarPlanProgramme(
	 RegPlanId int NOT NULL
	,ProgrammeId tinyint NOT NULL
	,TarPlanId int NOT NULL
	,CONSTRAINT PK_TargetingPlanProgramme PRIMARY KEY (RegPlanId,ProgrammeId,TarPlanId)
	,CONSTRAINT FK_TargetingPlanProgramme_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION
	,CONSTRAINT FK_TargetingPlanProgramme_RegPlan_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION
	,CONSTRAINT FK_TargetingPlanProgramme_RegPlan_TarPlanId FOREIGN KEY (TarPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION
 )
