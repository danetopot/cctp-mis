﻿
DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT

DECLARE @Getter INT



SET @Code='Iden Doc Type'
SET @Description='Various Identification Document Types'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id=Id
FROM SystemCode
WHERE Code ='Iden Doc Type'
IF(ISNULL(@Id,0)>0)
BEGIN

    SET @DetailCode='National ID'
    SET @Description='National ID card'
    SET @OrderNo=1
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Registration Birth'
    SET @Description='Registration of birth'
    SET @OrderNo=2
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Passport'
    SET @Description='Passport'
    SET @OrderNo=3
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='Other'
    SET @Description='Other'
    SET @OrderNo=4
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

    SET @DetailCode='None'
    SET @Description='None'
    SET @OrderNo=5
    EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



END
