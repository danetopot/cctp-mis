﻿
DECLARE @Id INT 
DECLARE @ModuleName VARCHAR(20)
DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW VARCHAR(20)
DECLARE @SysRightCode_ENTRY VARCHAR(20)
DECLARE @SysRightCode_MODIFIER VARCHAR(20)
DECLARE @SysRightCode_DELETION VARCHAR(20)
DECLARE @SysRightCode_APPROVAL VARCHAR(20)
DECLARE @SysRightCode_FINALIZE VARCHAR(20)
DECLARE @SysRightCode_VERIFY VARCHAR(20)
DECLARE @SysRightCode_EXPORT VARCHAR(20)
DECLARE @SysRightCode_DOWNLOAD VARCHAR(20)
DECLARE @SysRightCode_UPLOAD VARCHAR(20)

DECLARE @SysRightId_VIEW INT
DECLARE @SysRightId_ENTRY INT
DECLARE @SysRightId_MODIFIER INT
DECLARE @SysRightId_DELETION INT
DECLARE @SysRightId_APPROVAL INT
DECLARE @SysRightId_FINALIZE INT
DECLARE @SysRightId_VERIFY INT
DECLARE @SysRightId_EXPORT INT
DECLARE @SysRightId_DOWNLOAD INT
DECLARE @SysRightId_UPLOAD INT

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD



SET @ModuleName = 'Registration'
SELECT @Id = Id FROM Module WHERE [Name] = @ModuleName
IF(ISNULL(@Id,0)>0)
BEGIN
 INSERT INTO Module(Name, [Description],ParentModuleId)
	SELECT T1.Name,T1.[Description],T1.ParentModuleId
	FROM (SELECT  1 AS Id,  'Registration Plans' AS Name,'Registration Groups Management' AS [Description], @Id AS ParentModuleId
		  UNION
		  SELECT 2 AS Id,  'Registration Acceptance','Registration Groups Batches Acceptance and Approval',  @Id AS ParentModuleId
		 UNION
		  SELECT 3 AS Id,  'Registration Validation','Single Registry, IPRS  Validation. Community Validation and Exceptions',  @Id AS ParentModuleId
		 UNION
		  SELECT 4 AS Id,  'Community Validation','Communty Validation of all registrerd data.',  @Id AS ParentModuleId
		 UNION
		  SELECT 5 AS Id,  'Closure Of Registration','Closure Of Registration Process',  @Id AS ParentModuleId
	)T1		LEFT JOIN Module T2 ON T1.Name = T2.Name WHERE  T2.Name IS  NULL
 END
 SELECT @Id =NULL
 SELECT @ModuleName  =NULL 
  

SET @ModuleCode='Registration Plans'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION		
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Registration Acceptance'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACCEPT'
		UNION		
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL


SET @ModuleCode='Registration Validation'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'EXCEPTIONS'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'VALIDATION'
		UNION		
		SELECT @ModuleId,@SysRightId_DOWNLOAD,'EXPORTIPRS'
		UNION		
		SELECT @ModuleId,@SysRightId_UPLOAD,'IMPORTIPRS'
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Community Validation'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACCEPT'
		UNION		
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Closure Of Registration'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'EXCEPTIONS'
		UNION	
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL




SET @ModuleName = 'Targeting'
SELECT @Id = Id FROM Module WHERE [Name] = @ModuleName
IF(ISNULL(@Id,0)>0)
BEGIN
 INSERT INTO Module(Name, [Description],ParentModuleId)
	SELECT T1.Name,T1.[Description],T1.ParentModuleId
	FROM (SELECT  1 AS Id,  'Targeting Plans' AS Name,'Targeting Groups Management' AS [Description], @Id AS ParentModuleId
		  UNION
		  SELECT 2 AS Id,  'Targeting Acceptance','Targeting Groups Batches Acceptance and Approval',  @Id AS ParentModuleId
		 UNION
		  SELECT 3 AS Id,  'Targeting Validation','Single Registry, IPRS  Validation. Community Validation and Exceptions',  @Id AS ParentModuleId
		 UNION
		  SELECT 4 AS Id,  'PMT Assesment','PMT & Vulnerability assessment of Beneficiary households data.',  @Id AS ParentModuleId
	   UNION
		  SELECT 5 AS Id,  'Community Validation','Communty Validation of all registered data.',  @Id AS ParentModuleId
		 UNION
		  SELECT 6 AS Id,  'Closure Of Targeting','Closure Of Targeting Process',  @Id AS ParentModuleId
	)T1		LEFT JOIN Module T2 ON T1.Name = T2.Name WHERE  T2.Name IS  NULL
 END
 SELECT @Id =NULL
 SELECT @ModuleName  =NULL 
   

SET @ModuleCode='Targeting Plans'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION		
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Targeting Acceptance'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACCEPT'
		UNION		
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL


SET @ModuleCode='Targeting Validation'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'EXCEPTIONS'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'VALIDATION'
		UNION		
		SELECT @ModuleId,@SysRightId_DOWNLOAD,'EXPORTIPRS'
		UNION		
		SELECT @ModuleId,@SysRightId_UPLOAD,'IMPORTIPRS'
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL


SET @ModuleCode='Community Validation'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACCEPT'
		UNION		
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL


SET @ModuleCode='PMT Assesment'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'VALIDATE'
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,'ACCEPT'
		UNION		
		SELECT @ModuleId,@SysRightId_VERIFY,@SysRightCode_VERIFY
		UNION
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL

SET @ModuleCode='Closure Of Targeting'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,'EXCEPTIONS'
		UNION	
		SELECT @ModuleId,@SysRightId_FINALIZE,@SysRightCode_FINALIZE
		UNION
		SELECT @ModuleId,@SysRightId_APPROVAL,@SysRightCode_APPROVAL
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL






SET @ModuleName = 'Administration'
SELECT @Id = Id FROM Module WHERE [Name] = @ModuleName
IF(ISNULL(@Id,0)>0)
BEGIN
 INSERT INTO Module(Name, [Description],ParentModuleId)
	SELECT T1.Name,T1.[Description],T1.ParentModuleId
	FROM (SELECT 2 AS Id,  'Enumerators' AS Name,'Registration, Targeting and Retargeting Enumerators' AS [Description], @Id AS ParentModuleId
		  UNION
		  SELECT 1 AS Id,  'Programme-Officers','Programme Officers - Users Assigned County or SubCounties',  @Id AS ParentModuleId
	)T1		LEFT JOIN Module T2 ON T1.Name = T2.Name WHERE  T2.Name IS  NULL
 END
 SELECT @Id =NULL
 SELECT @ModuleName  =NULL 
  


SET @ModuleCode='Enumerators'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION,'DEACTIVATION'

	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL



  
SET @ModuleCode='Programme-Officers'
SELECT @ModuleId=Id FROM Module WHERE Name=@ModuleCode
INSERT INTO ModuleRight(ModuleId,RightId,[Description])
SELECT T1.ModuleId,T1.RightId,T1.[Description]
FROM (
		SELECT @ModuleId AS ModuleId,@SysRightId_VIEW AS RightId,@SysRightCode_VIEW AS [Description]
		UNION
		SELECT @ModuleId,@SysRightId_ENTRY,@SysRightCode_ENTRY
		UNION
		SELECT @ModuleId,@SysRightId_MODIFIER,@SysRightCode_MODIFIER
		UNION
		SELECT @ModuleId,@SysRightId_DELETION, 'DEACTIVATION'
	)T1 LEFT JOIN ModuleRight T2 ON T1.ModuleId=T2.ModuleId AND T1.RightId=T2.RightId
WHERE T2.Id IS NULL
