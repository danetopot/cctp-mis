﻿
IF NOT OBJECT_ID('AddEditRegPlan') IS NULL	
DROP PROC AddEditRegPlan
GO
 CREATE PROC AddEditRegPlan
	@Id INT=NULL
   ,@Name VARCHAR(30)
   ,@Description VARCHAR(128)
   ,@FromDate DATETIME
   ,@ToDate DATETIME
   ,@TargetHHs INT
   ,@Category VARCHAR(20)
   ,@UserId int
AS
BEGIN
  DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
			SET @ErrorMsg='Please specify valid UserId parameter'
	 
	ELSE IF EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='Once the Exercise has been approved it cannot be modified.'

SET @SysCode='REG_CATEGORIES'
SELECT @CategoryId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@Category

SET @SysCode='REG_STATUS'
SET @SysDetailCode='CREATIONAPV'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF ISNULL(@CategoryId,'')=''
SET @ErrorMsg='Exercise Category must be specified'
IF ISNULL(@StatusId,'')=''
SET @ErrorMsg='Exercise Status must be specified'

IF ISNULL(@ErrorMsg,'')<>''
BEGIN
	RAISERROR(@ErrorMsg,16,1)
	RETURN
END
BEGIN TRAN
	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.[Name] =  @Name
       ,T1.[Description] = @Description
       ,T1.[Start] = @FromDate
       ,T1.[End] = @ToDate
       ,T1.TargetHHs = @TargetHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM  RegPlan T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
    INSERT INTO RegPlan ([Name],[Description],[Start],[End],TargetHHs,CategoryId,StatusId,CreatedBy,CreatedOn) 
    SELECT @Name,@Description,@FromDate,@ToDate,@TargetHHs,@CategoryId,@StatusId,@UserId,GETDATE()
	END
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('ApproveRegPlan') IS NULL	
DROP PROC ApproveRegPlan
GO
CREATE PROC ApproveRegPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @CreationStatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CategoryId INT


SELECT @CategoryId = T1.CategoryId, @StatusId = T1.StatusId FROM RegPlan T1  where Id = @Id
	IF (ISNULL(@StatusId,'')='' OR ISNULL(@CategoryId,'')='')
   SET @ErrorMsg='Exercise was not found'
 
SET @SysCode='REG_STATUS'
SET @SysDetailCode='CREATIONAPV'
SELECT @CreationStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 
if(@CreationStatusId<>@StatusId)
 SET @ErrorMsg='The specified Registration Exercise is not in the approval stage'
 
SET @SysDetailCode='ACTIVE'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF   EXISTS(SELECT 1 FROM RegPlan WHERE Id<>@Id AND StatusId=@StatusId  AND CategoryId = @CategoryId)
		SET @ErrorMsg='There exists another Exercise that is currently active'
 
 ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN	 
	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO
 
 
IF NOT OBJECT_ID('ApproveRegPlanBatches') IS NULL	
DROP PROC ApproveRegPlanBatches
GO
CREATE PROC ApproveRegPlanBatches
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @CreationStatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CategoryId INT


SELECT @CategoryId = T1.CategoryId, @StatusId = T1.StatusId FROM RegPlan T1  where Id = @Id
	IF (ISNULL(@StatusId,'')='' OR ISNULL(@CategoryId,'')='')
   SET @ErrorMsg='Exercise was not found'
 
SET @SysCode='REG_STATUS'
SET @SysDetailCode='ACTIVE'
SELECT @CreationStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF(@CreationStatusId<>@StatusId)
 SET @ErrorMsg='The specified Registration Exercise is not in the stage accepting Data from Enumerators'
  
 ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN	 
	UPDATE T1
	SET T1.AccApvById=@UserId
	   ,T1.AccApvDate=GETDATE()
	FROM  RegAccept T1
	WHERE T1.RegPlanId=@Id AND 	T1.AccApvById IS NULL
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO


IF NOT OBJECT_ID('FinalizeRegPlan') IS NULL	
DROP PROC FinalizeRegPlan
GO
CREATE PROC FinalizeRegPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='ACTIVE'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not Active, It cannot be Finalized for Validation'
    
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FinalizeBy=@UserId
	   ,T1.FinalizeOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('FinalizeApvRegPlan') IS NULL	
DROP PROC FinalizeApvRegPlan
GO
CREATE PROC FinalizeApvRegPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not Finalized, It cannot be Approved for  Validation'
    
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='IPRSVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FinalizeApvBy=@UserId
	   ,T1.FinalizeApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO

IF NOT OBJECT_ID('ApproveRegPlanValidation') IS NULL	
DROP PROC ApproveRegPlanValidation
GO
CREATE PROC ApproveRegPlanValidation
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='IPRSVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in Validation stage'
    
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ValBy=@UserId
	   ,T1.ValOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO





IF NOT OBJECT_ID('AcceptRegistrationHouseholds') IS NULL	
DROP PROC AcceptRegistrationHouseholds
GO
 IF NOT OBJECT_ID('AcceptRegistrationHousehold') IS NULL	
DROP PROC AcceptRegistrationHousehold
GO

 CREATE PROC AcceptRegistrationHousehold

 @RegPlanId INT,
 @ConstituencyId INT ,
 @BatchName VARCHAR(30),
 @UserId INT
 as
 BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @RegAcceptId INT
  DECLARE @InsertDate DATE = GETDATE()

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'	 
	 IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@RegPlanId AND ApvBy>0)
	SET @ErrorMsg='The Registration Plan is not Approved. You cannot Accept Data into this Batch'

	SELECT @ReceivedHHs = COUNT(Id) FROM RegistrationHH T1
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = T1.SubLocationId  
																	  WHERE TG.ConstituencyId =@ConstituencyId 
																	  AND T1.RegAcceptId  IS NULL
 
 IF(@ReceivedHHs=0)
 SET @ErrorMsg='The sub county has no Pending Data'

		IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	
BEGIN TRAN



INSERT INTO RegAccept (AccById,AccDate,ReceivedHHs,BatchName,ConstituencyId,RegPlanId) 
 SELECT @UserId, @InsertDate,@ReceivedHHs,@BatchName,@ConstituencyId,@RegPlanId  
select @RegAcceptId = Id from RegAccept  where BatchName = @BatchName  AND @RegPlanId=@RegPlanId  
AND @ReceivedHHs = @ReceivedHHs AND ConstituencyId = @ConstituencyId 	AND AccDate = @InsertDate


BEGIN
UPDATE T1
SET T1.RegAcceptId = @RegAcceptId
FROM RegistrationHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId
				AND  TG.ConstituencyId =@ConstituencyId AND T1.RegAcceptId  IS NULL
END
						
					   
IF @@ERROR>0
BEGIN
	ROLLBACK TRAN
	SELECT 0 AS NoOfRows
END
ELSE
BEGIN
	COMMIT TRAN
	SELECT 1 AS NoOfRows
END
END
 GO


IF NOT OBJECT_ID('IPRSValidate') IS NULL	
DROP PROC IPRSValidate
GO
CREATE PROC IPRSValidate(@RegPlanId int)
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)

	 IF EXISTS (SELECT 1 FROM  RegistrationHH WHERE RegPlanId=@RegPlanId and RegAcceptId IS NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

	   IF ISNULL(@ErrorMsg,'')<>''
		BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 
SELECT  
     T1.Id
    ,T1.HHdFirstName+CASE WHEN(T1.HHdFirstName<>'') THEN ' ' ELSE '' END+T1.HHdMiddleName+CASE WHEN(T1.HHdSurname<>'') THEN ' ' ELSE '' END+T1.HHdSurname AS HHdName 
    ,T1.HHdSexId as HHdSex
    ,T1.HHdDoB as HHdDob
    ,T1.HHdNationalIdNo 
    ,T2.IDNo AS IPRS_IDNo
    ,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
    ,T2.Sex AS IPRS_Sex
    ,T2.DoB AS IPRS_DoB		
    ,CONVERT(bit,ISNULL(T2.IDNo,0)) AS HHD_IDNoExists
    ,CASE WHEN(T1.HHdFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_FirstNameExists
    ,CASE WHEN(T1.HHdMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_MiddleNameExists
    ,CASE WHEN(T1.HHdSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_BeneSurnameExists
    ,CASE WHEN(T1.HHdDoB=T2.DoB) THEN 1 ELSE 0 END AS HHD_DoBMatch
    ,CASE WHEN(YEAR(T1.HHdDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS HHD_DoBYearMatch
    ,CASE WHEN(T1.HHdSexId=T2.Sex) THEN 1 ELSE 0 END AS HHD_SexMatch 
    ,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
    ,T1.CgSexId as CgSex
    ,T1.CgDoB as CgDob
    ,T1.CgNationalIdNo 
    ,T3.IDNo AS IPRS_CG_IDNo
    ,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
    ,T3.Sex AS IPRS_CG__Sex
    ,T3.DoB AS IPRS_CG_DoB		
    ,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
    ,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
    ,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
    ,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
    ,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
    ,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
    ,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch   
 FROM
 RegistrationHH T1 
 INNER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
 ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T2.IDNo) 
 AND  T1.RegPlanId = @RegPlanId
 LEFT JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
   ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)   
 END
GO


IF NOT OBJECT_ID('GenerateRegistrationExceptions') IS NULL	
DROP PROC GenerateRegistrationExceptions
GO
CREATE PROC GenerateRegistrationExceptions
    @RegPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
    DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  RegistrationHH WHERE RegPlanId=@RegPlanId and RegAcceptId IS NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions WHERE RegPlanId = @RegPlanId
	
 
	; WITH X AS (
	SELECT  
		 T1.Id
		,T1.HHdFirstName+CASE WHEN(T1.HHdFirstName<>'') THEN ' ' ELSE '' END+T1.HHdMiddleName+CASE WHEN(T1.HHdSurname<>'') THEN ' ' ELSE '' END+T1.HHdSurname AS HHdName 
		,T1.HHdSexId as HHdSex
		,T1.HHdDoB as HHdDob
		,T1.HHdNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS HHD_IDNoExists
		,CASE WHEN(T1.HHdFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_FirstNameExists
		,CASE WHEN(T1.HHdMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_MiddleNameExists
		,CASE WHEN(T1.HHdSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_SurnameExists
		,CASE WHEN(T1.HHdDoB=T2.DoB) THEN 1 ELSE 0 END AS HHD_DoBMatch
		,CASE WHEN(YEAR(T1.HHdDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS HHD_DoBYearMatch
		,CASE WHEN(T1.HHdSexId=T2.Sex) THEN 1 ELSE 0 END AS HHD_SexMatch 
		,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@RegPlanId RegPlanId
	 FROM
	 RegistrationHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId 


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T2.IDNo) 
	 AND  T1.RegPlanId = @RegPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)  )
	    
	   INSERT INTO temp_RegExceptions (Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,HHD_IDNoExists,HHD_FirstNameExists,HHD_MiddleNameExists,HHD_SurnameExists,HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,RegPlanId,County,Constituency,District,Division,Location,SubLocation) 

	SELECT Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (HHD_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS HHD_IDNoExists,
	CASE WHEN (HHD_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_FirstNameExists,
	CASE WHEN (HHD_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_MiddleNameExists,
	CASE WHEN (HHD_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_SurnameExists,
	CASE WHEN (HHD_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBMatch,
	CASE WHEN (HHD_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBYearMatch,
	CASE WHEN (HHD_SexMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
    CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	RegPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	WHERE 
	HHD_IDNoExists = 0 OR HHD_SexMatch = 0 OR HHD_DoBYearMatch = 0 OR ( HHD_FirstNameExists =0 AND HHD_MiddleNameExists=0 AND HHD_SurnameExists =0)
	OR 
    CG_IDNoExists = 0 OR CG_SexMatch = 0 OR CG_DoBYearMatch = 0 OR ( CG_FirstNameExists =0 AND CG_MiddleNameExists=0 AND  CG_SurnameExists =0)
	AND RegPlanId=@RegPlanId
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='REG_EXCEPTIONS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_EXCEPTIONS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
  
  UPDATE T1
	SET T1.ExceptionsFileId=@FileCreationId
	FROM  RegPlan T1
	WHERE T1.Id=@RegPlanId
	SELECT @FileCreationId AS FileCreationId
  
 END
GO

IF NOT OBJECT_ID('GenerateCommunityValList') IS NULL	
DROP PROC GenerateCommunityValList
GO
CREATE PROC GenerateCommunityValList
    @RegPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
    DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  RegistrationHH WHERE RegPlanId=@RegPlanId and RegAcceptId IS NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions WHERE RegPlanId = @RegPlanId
	
; WITH X AS (
	SELECT  
		 T1.Id
		,T1.HHdFirstName+CASE WHEN(T1.HHdFirstName<>'') THEN ' ' ELSE '' END+T1.HHdMiddleName+CASE WHEN(T1.HHdSurname<>'') THEN ' ' ELSE '' END+T1.HHdSurname AS HHdName 
		,T1.HHdSexId as HHdSex
		,T1.HHdDoB as HHdDob
		,T1.HHdNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS HHD_IDNoExists
		,CASE WHEN(T1.HHdFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_FirstNameExists
		,CASE WHEN(T1.HHdMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_MiddleNameExists
		,CASE WHEN(T1.HHdSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_SurnameExists
		,CASE WHEN(T1.HHdDoB=T2.DoB) THEN 1 ELSE 0 END AS HHD_DoBMatch
		,CASE WHEN(YEAR(T1.HHdDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS HHD_DoBYearMatch
		,CASE WHEN(T1.HHdSexId=T2.Sex) THEN 1 ELSE 0 END AS HHD_SexMatch 
		,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@RegPlanId RegPlanId
	 FROM
	 RegistrationHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId 


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T2.IDNo) 
	 AND  T1.RegPlanId = @RegPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)  )
	    
	   INSERT INTO temp_RegExceptions (Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,HHD_IDNoExists,HHD_FirstNameExists,HHD_MiddleNameExists,HHD_SurnameExists,HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,RegPlanId,County,Constituency,District,Division,Location,SubLocation) 

	SELECT Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (HHD_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS HHD_IDNoExists,
	CASE WHEN (HHD_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_FirstNameExists,
	CASE WHEN (HHD_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_MiddleNameExists,
	CASE WHEN (HHD_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_SurnameExists,
	CASE WHEN (HHD_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBMatch,
	CASE WHEN (HHD_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBYearMatch,
	CASE WHEN (HHD_SexMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
    CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	RegPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	WHERE 
	HHD_IDNoExists = 1 AND HHD_SexMatch = 1 AND HHD_DoBYearMatch = 1 AND ( HHD_FirstNameExists =1 OR HHD_MiddleNameExists=1 OR HHD_SurnameExists =1)
	OR 
    CG_IDNoExists = 1 AND CG_SexMatch = 1 AND CG_DoBYearMatch = 1 AND( CG_FirstNameExists =1 OR CG_MiddleNameExists=1 OR  CG_SurnameExists =1)
	AND RegPlanId=@RegPlanId
  
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='REG_VALIDATION_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_VALIDATION'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
 

 ; WITH Y AS (
	SELECT  
		 T1.Id
		,T1.HHdFirstName+CASE WHEN(T1.HHdFirstName<>'') THEN ' ' ELSE '' END+T1.HHdMiddleName+CASE WHEN(T1.HHdSurname<>'') THEN ' ' ELSE '' END+T1.HHdSurname AS HHdName 
		,T1.HHdSexId as HHdSex
		,T1.HHdDoB as HHdDob
		,T1.HHdNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS HHD_IDNoExists
		,CASE WHEN(T1.HHdFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_FirstNameExists
		,CASE WHEN(T1.HHdMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_MiddleNameExists
		,CASE WHEN(T1.HHdSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_SurnameExists
		,CASE WHEN(T1.HHdDoB=T2.DoB) THEN 1 ELSE 0 END AS HHD_DoBMatch
		,CASE WHEN(YEAR(T1.HHdDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS HHD_DoBYearMatch
		,CASE WHEN(T1.HHdSexId=T2.Sex) THEN 1 ELSE 0 END AS HHD_SexMatch 
		,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@RegPlanId RegPlanId
	 FROM
	 RegistrationHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId 


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T2.IDNo) 
	 AND  T1.RegPlanId = @RegPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)  )

  INSERT INTO RegException (Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,HHD_IDNoExists,HHD_FirstNameExists,HHD_MiddleNameExists,HHD_SurnameExists,HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,RegPlanId,DateValidated) 
  
  SELECT Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,HHD_IDNoExists,HHD_FirstNameExists,HHD_MiddleNameExists,HHD_SurnameExists,HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,RegPlanId,GETDATE() AS DateValidated   FROM  Y 
  WHERE RegPlanId=  @RegPlanId
  
  UPDATE T1
	SET T1.ValidationFileId=@FileCreationId
  ,T1.ValBy = @UserId
  ,T1.ValOn = GETDATE()
	FROM  RegPlan T1
	WHERE T1.Id=@RegPlanId

	SELECT @FileCreationId AS FileCreationId
  
 END
GO

  IF NOT OBJECT_ID('GenerateIPRSExportFile') IS NULL	
DROP PROC GenerateIPRSExportFile
GO
CREATE PROC GenerateIPRSExportFile
     @RegPlanId int
    ,@FilePath varchar(128)
    ,@DBServer varchar(30)
    ,@DBName varchar(30)
    ,@DBUser varchar(30)
    ,@DBPassword varchar(30)
    ,@UserId int 

AS
  
  BEGIN
  DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
  DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  DECLARE @ErrorMsg varchar(128)
  DECLARE @SystemCodeDetailId1 int
  DECLARE @SystemCodeDetailId2 int
  
  
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
    
	  IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
    
 IF EXISTS (SELECT 1 FROM  RegistrationHH WHERE RegPlanId=@RegPlanId and RegAcceptId IS NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There exists Households that have not been Accepted'

	   IF ISNULL(@ErrorMsg,'')<>''
		BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
   
   DELETE FROM temp_IprsExport WHERE RegPlanId = @RegPlanId
   
   
; WITH T1 AS 
(
SELECT    T1.HHdFirstName AS FirstName, T1.HHdMiddleName as Middlename, T1.HHdSurname as Surname, T1.HHdNationalIdNo as NationalIdNo, T2.Code as Sex, T1.HHdDoB as Date_Of_Birth, T1.RegPlanId  FROM RegistrationHH T1
INNER JOIN SystemCodeDetail T2 ON T1.HHdSexId = T2.Id
LEFT OUTER JOIN (SELECT ID_Number AS IDNo FROM  IPRSCache) AS T3   
ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T3.IDNo) 
WHERE T3.IDNo IS NULL 
AND T1.RegPlanId =@RegPlanId
UNION
SELECT  T1.CgFirstName, T1.CgMiddleName, T1.CgSurname, T1.CgNationalIdNo, T2.Code, T1.CgDoB, T1.RegPlanId   FROM RegistrationHH T1
INNER JOIN SystemCodeDetail T2 ON T1.CgSexId = T2.Id
LEFT OUTER JOIN (SELECT ID_Number AS IDNo FROM  IPRSCache) AS T3   
ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T3.IDNo) 
WHERE T3.IDNo IS NULL 
AND T1.RegPlanId =@RegPlanId
)
INSERT INTO temp_IprsExport (
   FirstName
  ,Middlename
  ,Surname
  ,NationalIdNo
  ,Sex
  ,Date_Of_Birth
  ,RegPlanId
)  
SELECT FirstName,Middlename,Surname,NationalIdNo,Sex,Date_Of_Birth,RegPlanId  FROM T1
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='REG_IPRS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_IprsExport" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='IPRS_EXPORT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
 	SELECT @FileCreationId AS FileCreationId
END
GO

 IF NOT OBJECT_ID('ImportBulkIPRS') IS NULL	
 DROP PROC ImportBulkIPRS
GO

CREATE PROC ImportBulkIPRS
AS
BEGIN
DECLARE @ErrorMsg varchar(128)
IF NOT EXISTS(SELECT 1 FROM temp_IPRSCache T1 LEFT JOIN IPRSCache T2  ON CONVERT(bigint,T1.ID_Number)=CONVERT(bigint,T2.ID_Number) WHERE T2.ID_Number IS NULL)
SET @ErrorMsg='All Imported Records are already in the Database!. Try another Batch'

 IF ISNULL(@ErrorMsg,'')<>''
		BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
INSERT INTO IPRSCache (First_Name,Surname,Middle_Name,ID_Number,Gender,Date_of_Birth,Date_of_Issue,Place_of_Birth,Serial_Number,Address,Status,DateCached)  
SELECT  T1.First_Name,T1.Surname,T1.Middle_Name,T1.ID_Number,T1.Gender,T1.Date_of_Birth,T1.Date_of_Issue,T1.Place_of_Birth,T1.Serial_Number,T1.[Address],T1.[Status],T1.DateCached
FROM temp_IPRSCache T1 LEFT JOIN IPRSCache T2  ON CONVERT(bigint,T1.ID_Number)=CONVERT(bigint,T2.ID_Number) WHERE T2.ID_Number IS NULL
DELETE FROM temp_IPRSCache
SELECT @@ROWCOUNT AS NoOfRows
END
GO



 IF NOT OBJECT_ID('AcceptRegistrationHouseholdCv') IS NULL	
DROP PROC AcceptRegistrationHouseholdCv
GO

 CREATE PROC AcceptRegistrationHouseholdCv

 @RegPlanId INT,
 @ConstituencyId INT ,
 @BatchName VARCHAR(30),
 @UserId INT
 as
 BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @RegAcceptCvId INT
  DECLARE @InsertDate DATE = GETDATE()

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'	 
	 IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@RegPlanId AND ApvBy>0)
	SET @ErrorMsg='The Registration Plan is not Approved. You cannot Accept Community Validation Data into this Batch'

	SELECT @ReceivedHHs = COUNT(Id) FROM RegistrationHHCv T1
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = T1.SubLocationId  
																	  WHERE TG.ConstituencyId =@ConstituencyId 
																	  AND T1.RegAcceptCvId  IS NULL
 
 IF(@ReceivedHHs=0)
 SET @ErrorMsg='The sub county has no Pending Data'

		IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	
BEGIN TRAN



INSERT INTO RegAcceptCv (AccById,AccDate,ReceivedHHs,BatchName,ConstituencyId,RegPlanId) 
 SELECT @UserId, @InsertDate,@ReceivedHHs,@BatchName,@ConstituencyId,@RegPlanId  
select @RegAcceptCvId = Id from RegAcceptCv  where BatchName = @BatchName  AND @RegPlanId=@RegPlanId  
AND @ReceivedHHs = @ReceivedHHs AND ConstituencyId = @ConstituencyId 	AND AccDate = @InsertDate


BEGIN
UPDATE T1
SET T1.RegAcceptCvId = @RegAcceptCvId
FROM RegistrationHHCv T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId
				AND  TG.ConstituencyId =@ConstituencyId AND T1.RegAcceptCvId  IS NULL
END
						
					   
IF @@ERROR>0
BEGIN
	ROLLBACK TRAN
	SELECT 0 AS NoOfRows
END
ELSE
BEGIN
	COMMIT TRAN
	SELECT 1 AS NoOfRows
END
END
 GO

 
IF NOT OBJECT_ID('ApproveCvRegPlanBatches') IS NULL	
DROP PROC ApproveCvRegPlanBatches
GO
CREATE PROC ApproveCvRegPlanBatches
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @CreationStatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)
	DECLARE @CategoryId INT


SELECT @CategoryId = T1.CategoryId, @StatusId = T1.StatusId FROM RegPlan T1  where Id = @Id
	IF (ISNULL(@StatusId,'')='' OR ISNULL(@CategoryId,'')='')
   SET @ErrorMsg='Exercise was not found'
 
SET @SysCode='REG_STATUS'
SET @SysDetailCode='COMMVAL'
SELECT @CreationStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF(@CreationStatusId<>@StatusId)
 SET @ErrorMsg='The specified Registration Exercise is not in the stage accepting Data from Enumerators'
  
 ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN	 
	UPDATE T1
	SET T1.AccApvById=@UserId
	   ,T1.AccApvDate=GETDATE()
	FROM  RegAcceptCv T1
	WHERE T1.RegPlanId=@Id AND 	T1.AccApvById IS NULL
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('AddEditRegistrationHHCv') IS NULL	
DROP PROC AddEditRegistrationHHCv
GO


CREATE PROC AddEditRegistrationHHCv
	 @Id INT
	,@RegistrationHHCvXml XML
	AS
BEGIN


	DECLARE @ErrorMsg varchar(128)
	DECLARE @SysCode varchar(20)
	DECLARE @EditingCode varchar(20)
	DECLARE @EditingId int
	DECLARE @SystemCodeDetailId2 int
	
	SET @SysCode = 'COM_VAL_STATUS'
	SET @EditingCode = 'EDITING'
	SELECT @EditingId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 
	ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@EditingCode


	IF  EXISTS(SELECT 1 FROM RegistrationHHCv WHERE Id = @Id AND RegAcceptCvId IS NOT NULL  ) 
	SET @ErrorMsg='This Household has already been submitted and Accepted'

	IF  EXISTS(SELECT 1 FROM RegistrationHHCv WHERE Id = @Id AND  CvStatusId =@EditingId  ) 
	SET @ErrorMsg='This Household Cannot be Submitted until Editing is Complete.'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END


	IF  EXISTS(SELECT 1 FROM RegistrationHHCv WHERE Id = @Id AND RegAcceptCvId IS  NULL ) 
	DELETE FROM RegistrationHHCv WHERE Id = @Id			
	INSERT INTO RegistrationHHCv(RegistrationHHId,Id,UniqueId ,StartTime ,EndTime ,ProgrammeId ,RegistrationDate ,SubLocationId,LocationId,Years,Months,RegPlanId	 ,EnumeratorId  ,HHdFirstName,HHdMiddleName ,HHdSurname,HHdNationalIdNo,HHdSexId,HHdDoB,CgFirstName,CgMiddleName ,CgSurname  ,CgNationalIdNo,CgSexId,CgDoB,HouseholdMembers  ,StatusId  ,Village  ,PhysicalAddress  ,NearestReligiousBuilding,NearestSchool,Longitude,Latitude,SyncEnumeratorId ,DeviceId ,DeviceModel ,DeviceManufacturer  ,DeviceName,[Version] ,VersionNumber ,AppVersion,AppBuild,[Platform]  ,Idiom ,IsDevice ,RegAcceptId ,RegAcceptCvId ,CvStatusId,CvEnumeratorId,CvSyncDownDate ,CvUpdateDate ,CvSyncUpDate ,CvSyncEnumeratorId )
	SELECT  RegistrationHHId,Id,UniqueId ,StartTime ,EndTime ,ProgrammeId ,RegistrationDate ,SubLocationId,LocationId,Years,Months,RegPlanId	 ,EnumeratorId  ,HHdFirstName,HHdMiddleName ,HHdSurname,HHdNationalIdNo,HHdSexId,HHdDoB,CgFirstName,CgMiddleName ,CgSurname  ,CgNationalIdNo,CgSexId,CgDoB,HouseholdMembers  ,StatusId  ,Village  ,PhysicalAddress  ,NearestReligiousBuilding,NearestSchool,Longitude,Latitude,SyncEnumeratorId ,DeviceId ,DeviceModel ,DeviceManufacturer  ,DeviceName,[Version] ,VersionNumber ,AppVersion,AppBuild,[Platform]  ,Idiom ,IsDevice ,RegAcceptId ,RegAcceptCvId ,CvStatusId,CvEnumeratorId,CvSyncDownDate ,CvUpdateDate ,CvSyncUpDate ,CvSyncEnumeratorId 
	FROM (
		SELECT  
		U.R.value('(Id)[1]','INT') AS RegistrationHHId
	,U.R.value('(Id)[1]','INT') AS Id
	,U.R.value('(UniqueId)[1]','VARCHAR(36)') AS UniqueId
	,U.R.value('(StartTime)[1]','DATETIME ') AS StartTime
	,U.R.value('(EndTime)[1]','DATETIME') AS EndTime
	,U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId
	,U.R.value('(RegistrationDate)[1]','DATETIME') AS RegistrationDate
	,U.R.value('(SubLocationId)[1]','INT') AS SubLocationId
	,U.R.value('(LocationId)[1]','INT') AS LocationId
	,U.R.value('(Years)[1]','INT') AS Years
	,U.R.value('(Months)[1]','INT') AS Months
	,U.R.value('(RegPlanId)[1]','INT') AS RegPlanId
	,U.R.value('(EnumeratorId)[1]','INT') AS EnumeratorId
	,U.R.value('(HHdFirstName)[1]','VARCHAR(25)') AS HHdFirstName
	,U.R.value('(HHdMiddleName)[1]','VARCHAR(25)') AS HHdMiddleName
	,U.R.value('(HHdSurname)[1]','VARCHAR(25)') AS HHdSurname 
	,U.R.value('(HHdNationalIdNo)[1]','VARCHAR(15)') AS HHdNationalIdNo
	,U.R.value('(HHdSexId)[1]','INT') AS HHdSexId
	,U.R.value('(HHdDoB)[1]','DATETIME') AS HHdDoB
	,U.R.value('(CgFirstName)[1]','VARCHAR(25)') AS CgFirstName
	,U.R.value('(CgMiddleName)[1]','VARCHAR(25)') AS CgMiddleName
	,U.R.value('(CgSurname)[1]','VARCHAR(25)') AS CgSurname
	,U.R.value('(CgNationalIdNo)[1]','VARCHAR(15)') AS CgNationalIdNo
	,U.R.value('(CgSexId)[1]','INT') AS CgSexId
	,U.R.value('(CgDoB)[1]','DATETIME ') AS CgDoB
	,U.R.value('(HouseholdMembers)[1]','INT') AS HouseholdMembers
	,U.R.value('(StatusId)[1]','INT') AS StatusId
	,U.R.value('(Village)[1]','VARCHAR(30)') AS Village
	,U.R.value('(PhysicalAddress)[1]','VARCHAR(50)') AS PhysicalAddress
	,U.R.value('(NearestReligiousBuilding)[1]','VARCHAR(50)') AS NearestReligiousBuilding
	,U.R.value('(NearestSchool)[1]','VARCHAR(50)') AS NearestSchool
	,U.R.value('(Longitude)[1]','FLOAT') AS Longitude
	,U.R.value('(Latitude)[1]','FLOAT') AS Latitude
	,U.R.value('(SyncEnumeratorId)[1]','INT ') AS SyncEnumeratorId
	,U.R.value('(DeviceId)[1]','VARCHAR(36)') AS DeviceId
	,U.R.value('(DeviceModel)[1]','VARCHAR(15)') AS DeviceModel
	,U.R.value('(DeviceManufacturer)[1]','VARCHAR(15)') AS DeviceManufacturer
	,U.R.value('(DeviceName)[1]','VARCHAR(15)') AS DeviceName
	,U.R.value('(Version)[1]','VARCHAR(15)')  AS [Version]
	,U.R.value('(VersionNumber)[1]','VARCHAR(15) ') AS VersionNumber
	,U.R.value('(AppVersion)[1]','VARCHAR(15)') AS AppVersion
	,U.R.value('(AppBuild)[1]','VARCHAR(15) ') AS AppBuild
	,U.R.value('(Platform)[1]','VARCHAR(15)') AS [Platform]
	,U.R.value('(Idiom)[1]','VARCHAR(10)') AS Idiom
	,U.R.value('(IsDevice)[1]','BIT') AS IsDevice
	,U.R.value('(RegAcceptId)[1]','INT') AS RegAcceptId
	,NULL AS RegAcceptCvId
	,U.R.value('(CvStatusId)[1]','INT') AS CvStatusId
	,U.R.value('(CvEnumeratorId)[1]','INT') AS CvEnumeratorId
	,U.R.value('(CvSyncDownDate)[1]','DATETIME') AS CvSyncDownDate
	,U.R.value('(CvUpdateDate)[1]','DATETIME') AS CvUpdateDate
	,U.R.value('(CvSyncUpDate)[1]','DATETIME') AS CvSyncUpDate
	,U.R.value('(CvSyncEnumeratorId)[1]','INT')  AS CvSyncEnumeratorId
FROM @RegistrationHHCvXml.nodes('RegistrationHHCvs/Record') AS U(R)
) T1
END
GO


IF NOT OBJECT_ID('ProcessComVal') IS NULL	
DROP PROC ProcessComVal
GO
CREATE PROC ProcessComVal
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVAL'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the Community Validation stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVALAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ComValBy=@UserId
	   ,T1.ComValOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('ApproveComVal') IS NULL	
DROP PROC ApproveComVal
GO
CREATE PROC ApproveComVal
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='COMMVALAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the Community Validation stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='POSTREG'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ComValApvBy=@UserId
	   ,T1.ComValApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO




/*
CREATE PROC [dbo].[AddEditReconciliationDetail]
	@ReconciliationId int=NULL
   ,@PSPId int
   ,@OpeningBalance money
   ,@CrFundsRequests money
   ,@CrFundsRequestsStatement money
   ,@CrFundsRequestsDiffNarration varchar(128)
   ,@CrClawBacks money
   ,@CrClawBacksStatement money
   ,@CrClawBacksDiffNarration varchar(128)
   ,@DrPayments money
   ,@DrPaymentsStatement money
   ,@DrPaymentsDiffNarration varchar(128)
   ,@DrCommissions money
   ,@DrCommissionsStatement money
   ,@DrCommissionsDiffNarration varchar(128)
   ,@Balance money
   ,@BalanceStatement money
   ,@BalanceDiffNarration varchar(128)
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)	
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Recon Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0),@OpeningBalance=ISNULL(@OpeningBalance,0)
		,@CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrFundsRequestsStatement=ISNULL(@CrFundsRequestsStatement,0),@CrFundsRequestsDiffNarration=ISNULL(@CrFundsRequestsDiffNarration,'')
		,@CrClawBacks=ISNULL(@CrClawBacks,0),@CrClawBacksStatement=ISNULL(@CrClawBacksStatement,0),@CrClawBacksDiffNarration=ISNULL(@CrClawBacksDiffNarration,'')
		,@DrPayments=ISNULL(@DrPayments,0),@DrPaymentsStatement=ISNULL(@DrPaymentsStatement,0)	,@DrPaymentsDiffNarration=ISNULL(@DrPaymentsDiffNarration,'')
		,@DrCommissions=ISNULL(@DrCommissions,0),@DrCommissionsStatement=ISNULL(@DrCommissionsStatement,0),@DrCommissionsDiffNarration=ISNULL(@DrCommissionsDiffNarration,'')
		,@Balance=ISNULL(@Balance,0),@BalanceStatement=ISNULL(@BalanceStatement,0),@BalanceDiffNarration=ISNULL(@BalanceDiffNarration,'')

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT T1.PaymentCycleId
								FROM PaymentCycleDetail T1 LEFT JOIN (SELECT PaymentCycleId,MAX(TrxDate) AS TrxDate FROM Payment GROUP BY PaymentCycleId) T2 ON T1.PaymentCycleId=T2.PaymentCycleId 
														   INNER JOIN Reconciliation T3 ON T3.Id=@ReconciliationId AND (T1.FundsRequestOn BETWEEN T3.StartDate AND T3.EndDate) AND (ISNULL(T2.TrxDate,T1.PostPayrollApvOn)<=T3.EndDate)
								GROUP BY T1.PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF (@CrFundsRequests<>@CrFundsRequestsStatement AND @CrFundsRequestsDiffNarration='')
		SET @ErrorMsg='Please specify valid CrFundsRequestsDiffNarration parameter'
	ELSE IF (@CrClawBacks<>@CrClawBacksStatement AND @CrClawBacksDiffNarration='')
		SET @ErrorMsg='Please specify valid CrClawBacksDiffNarration parameter'
	ELSE IF (@DrPayments<>@DrPaymentsStatement AND @DrPaymentsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrPaymentsDiffNarration parameter'
	ELSE IF (@DrCommissions<>@DrCommissionsStatement AND @DrCommissionsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrCommissionsDiffNarration parameter'
	ELSE IF (@Balance<>@BalanceStatement AND @BalanceDiffNarration='')
		SET @ErrorMsg='Please specify valid BalanceDiffNarration parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId)
	BEGIN
		UPDATE T1
		SET T1.OpeningBalance=@OpeningBalance
		   ,T1.CrFundsRequests=@CrFundsRequests
		   ,T1.CrFundsRequestsStatement=@CrFundsRequestsStatement
		   ,T1.CrFundsRequestsDiffNarration=@CrFundsRequestsDiffNarration
		   ,T1.CrClawBacks=@CrClawBacks
		   ,T1.CrClawBacksStatement=@CrClawBacksStatement
		   ,T1.CrClawBacksDiffNarration=@CrClawBacksDiffNarration
		   ,T1.DrPayments=@DrPayments
		   ,T1.DrPaymentsStatement=@DrPaymentsStatement
		   ,T1.DrPaymentsDiffNarration=@DrPaymentsDiffNarration
		   ,T1.DrCommissions=@DrCommissions
		   ,T1.DrCommissionsStatement=@DrCommissionsStatement
		   ,T1.DrCommissionsDiffNarration=@DrCommissionsDiffNarration
		   ,T1.Balance=@Balance
		   ,T1.BalanceStatement=@BalanceStatement
		   ,T1.BalanceDiffNarration=@BalanceDiffNarration
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ReconciliationDetail T1
		WHERE T1.ReconciliationId=@ReconciliationId AND T1.PSPId=@PSPId
			
		SELECT @FileCreationId=BankStatementFileId FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId

		SELECT @FileName= [Name]  from  FileCreation where id = @FileCreationId
	END
	ELSE
	BEGIN
		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @FileName='SUPPORT'+'_'+'RECONCILIATION'+'_'+CONVERT(varchar(11),T1.StartDate,106)+'-'+CONVERT(varchar(11),T1.EndDate,106) FROM Reconciliation T1 WHERE Id=@ReconciliationId
		SELECT @FileName=@FileName+'_'+T1.Code FROM PSP T1 WHERE Id=@PSPId
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		INSERT INTO ReconciliationDetail(ReconciliationId,PSPId,OpeningBalance,CrFundsRequests,CrFundsRequestsStatement,CrFundsRequestsDiffNarration,CrClawBacks,CrClawBacksStatement,CrClawBacksDiffNarration,DrPayments,DrPaymentsStatement,DrPaymentsDiffNarration,DrCommissions,DrCommissionsStatement,DrCommissionsDiffNarration,Balance,BalanceStatement,BalanceDiffNarration,BankStatementFileId,CreatedBy,CreatedOn)
		SELECT @ReconciliationId,@PSPId,@OpeningBalance,@CrFundsRequests,@CrFundsRequestsStatement,@CrFundsRequestsDiffNarration,@CrClawBacks,@CrClawBacksStatement,@CrClawBacksDiffNarration,@DrPayments,@DrPaymentsStatement,@DrPaymentsDiffNarration,@DrCommissions,@DrCommissionsStatement,@DrCommissionsDiffNarration,@Balance,@BalanceStatement,@BalanceDiffNarration,@FileCreationId,@UserId,GETDATE()

		SELECT @FileName = @FileName+@FileExtension
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId,@FileCreationId AS FileId,@FileName AS SupportingDoc
	END
END

GO
*/

IF NOT OBJECT_ID('CloseRegPlan') IS NULL	
DROP PROC CloseRegPlan
GO
CREATE PROC CloseRegPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='POSTREG'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not in the closure stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='CLOSED'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ClosedBy=@UserId
	   ,T1.ClosedOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM RegPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('GeneratePostRegistrationExceptions') IS NULL	
DROP PROC GeneratePostRegistrationExceptions
GO
CREATE PROC GeneratePostRegistrationExceptions
    @RegPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
    DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  RegistrationHH WHERE RegPlanId=@RegPlanId and RegAcceptId IS NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions WHERE RegPlanId = @RegPlanId
	
 
	; WITH X AS (
	SELECT  
		 T1.Id
		,T1.HHdFirstName+CASE WHEN(T1.HHdFirstName<>'') THEN ' ' ELSE '' END+T1.HHdMiddleName+CASE WHEN(T1.HHdSurname<>'') THEN ' ' ELSE '' END+T1.HHdSurname AS HHdName 
		,T1.HHdSexId as HHdSex
		,T1.HHdDoB as HHdDob
		,T1.HHdNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS HHD_IDNoExists
		,CASE WHEN(T1.HHdFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_FirstNameExists
		,CASE WHEN(T1.HHdMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_MiddleNameExists
		,CASE WHEN(T1.HHdSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS HHD_SurnameExists
		,CASE WHEN(T1.HHdDoB=T2.DoB) THEN 1 ELSE 0 END AS HHD_DoBMatch
		,CASE WHEN(YEAR(T1.HHdDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS HHD_DoBYearMatch
		,CASE WHEN(T1.HHdSexId=T2.Sex) THEN 1 ELSE 0 END AS HHD_SexMatch 
		,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@RegPlanId RegPlanId
	 FROM
	 RegistrationHHCv T1 

	  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
							INNER JOIN Division T3 ON T2.DivisionId=T3.Id
							INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
							INNER JOIN District T5 ON T4.DistrictId=T5.Id
							INNER JOIN County T6 ON T4.CountyId=T6.Id
							INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
							INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		) T5 ON T1.SubLocationId=T5.SubLocationId 

	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,T1.HHdNationalIdNo)=CONVERT(bigint,T2.IDNo) 
	 AND  T1.RegPlanId = @RegPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,T1.CGNationalIDNo)=CONVERT(bigint,T3.IDNo)  )

	   INSERT INTO temp_RegExceptions (Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,HHD_IDNoExists,HHD_FirstNameExists,HHD_MiddleNameExists,HHD_SurnameExists,HHD_DoBMatch,HHD_DoBYearMatch,HHD_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,RegPlanId,County,Constituency,District,Division,Location,SubLocation) 
	
	SELECT Id,HHdName,HHdSex,HHdDob,HHdNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (HHD_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS HHD_IDNoExists,
	CASE WHEN (HHD_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_FirstNameExists,
	CASE WHEN (HHD_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_MiddleNameExists,
	CASE WHEN (HHD_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS HHD_SurnameExists,
	CASE WHEN (HHD_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBMatch,
	CASE WHEN (HHD_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_DoBYearMatch,
	CASE WHEN (HHD_SexMatch=1) THEN 'YES' ELSE 'NO' END AS HHD_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
    CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	RegPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	 
	 
	WHERE 
	HHD_IDNoExists = 0 OR HHD_SexMatch = 0 OR HHD_DoBYearMatch = 0 OR ( HHD_FirstNameExists =0 AND HHD_MiddleNameExists=0 AND HHD_SurnameExists =0)
	OR 
    CG_IDNoExists = 0 OR CG_SexMatch = 0 OR CG_DoBYearMatch = 0 OR ( CG_FirstNameExists =0 AND CG_MiddleNameExists=0 AND  CG_SurnameExists =0)
		AND RegPlanId=@RegPlanId
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='POSTREG_EXCEPTIONS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_EXCEPTIONS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
  
  UPDATE T1
	SET T1.PostRegExceptionsFileId=@FileCreationId, 
	T1.PostRegExceptionBy = @UserId, 
	T1.PostRegExceptionOn =	GETDATE()
	FROM  RegPlan T1
	WHERE T1.Id=@RegPlanId
	SELECT @FileCreationId AS FileCreationId
  
 END
GO



IF NOT OBJECT_ID('AddEditTarPlan') IS NULL	
DROP PROC AddEditTarPlan
GO
 CREATE PROC AddEditTarPlan
	@Id INT=NULL
   ,@Name VARCHAR(30)
   ,@Description VARCHAR(128)
   ,@FromDate DATETIME
   ,@ToDate DATETIME
   ,@TargetHHs INT = 0
   ,@Category VARCHAR(20)
   ,@UserId INT
   ,@TarPlanProgrammeXml xml
AS
BEGIN
  DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
			SET @ErrorMsg='Please specify valid UserId parameter'
	 
	ELSE IF EXISTS(SELECT 1 FROM RegPlan WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='Once the Exercise has been approved it cannot be modified.'
SET @SysCode='REG_CATEGORIES'
SELECT @CategoryId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@Category

SET @SysCode='REG_STATUS'
SET @SysDetailCode='CREATIONAPV'
SELECT @StatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON 
T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

IF ISNULL(@CategoryId,'')=''
SET @ErrorMsg='Exercise Category must be specified'
IF ISNULL(@StatusId,'')=''
SET @ErrorMsg='Exercise Status must be specified'

IF ISNULL(@ErrorMsg,'')<>''
BEGIN
	RAISERROR(@ErrorMsg,16,1)
	RETURN
END
BEGIN TRAN
	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.[Name] =  @Name
       ,T1.[Description] = @Description
       ,T1.[Start] = @FromDate
       ,T1.[End] = @ToDate
       ,T1.TargetHHs = @TargetHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM  RegPlan T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
    INSERT INTO RegPlan ([Name],[Description],[Start],[End],TargetHHs,CategoryId,StatusId,CreatedBy,CreatedOn) 
    SELECT @Name,@Description,@FromDate,@ToDate,@TargetHHs,@CategoryId,@StatusId,@UserId,GETDATE()	
	SELECT @Id = IDENT_CURRENT('RegPlan')
	END

	DELETE FROM TarPlanProgramme WHERE TarPlanId = @Id

	 INSERT INTO TarPlanProgramme(RegPlanId,ProgrammeId,TarPlanId)
	  
SELECT T1.RegPlanId,T1.ProgrammeId, @Id as Id  FROM (
		SELECT  
		U.R.value('(RegPlanId)[1]','INT') AS RegPlanId
	   ,U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId
	FROM @TarPlanProgrammeXml.nodes('TarPlanProgrammeOptions/Record') AS U(R)
) T1



 
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO



IF NOT OBJECT_ID('GeneratePostHHListingExceptions') IS NULL	
DROP PROC GeneratePostHHListingExceptions
GO

CREATE PROC GeneratePostHHListingExceptions
    @TargetPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS
BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
    DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
  	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'

	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 
DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions WHERE TargetPlanId = @TargetPlanId
	
	
; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T1.BeneSexId as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 RegistrationHHCv T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId 


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 AND  T1.TargetPlanId = @TargetPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	 


	   INSERT INTO temp_RegExceptions (Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,County,Constituency,District,Division,Location,SubLocation) 
	
	SELECT Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (Bene_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS Bene_IDNoExists,
	CASE WHEN (Bene_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_FirstNameExists,
	CASE WHEN (Bene_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_MiddleNameExists,
	CASE WHEN (Bene_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_SurnameExists,
	CASE WHEN (Bene_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBMatch,
	CASE WHEN (Bene_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBYearMatch,
	CASE WHEN (Bene_SexMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
    CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	TargetPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	 
	 
	WHERE 
	Bene_IDNoExists = 0 OR Bene_SexMatch = 0 OR Bene_DoBYearMatch = 0 OR ( Bene_FirstNameExists =0 OR Bene_MiddleNameExists=0 OR Bene_SurnameExists =0)
	OR 
    CG_IDNoExists = 0 OR CG_SexMatch = 0 OR CG_DoBYearMatch = 0 OR ( CG_FirstNameExists =0 OR CG_MiddleNameExists=0 OR  CG_SurnameExists =0)
		AND TargetPlanId=@TargetPlanId
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='POSTREG_EXCEPTIONS_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_EXCEPTIONS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
  
  
  UPDATE T1
	SET T1.PostRegExceptionsFileId=@FileCreationId, 
	T1.PostRegExceptionBy = @UserId, 
	T1.PostRegExceptionOn =	GETDATE()
	FROM  TargetPlan T1
	WHERE T1.Id=@TargetPlanId
	SELECT @FileCreationId AS FileCreationId
  
 END
GO
