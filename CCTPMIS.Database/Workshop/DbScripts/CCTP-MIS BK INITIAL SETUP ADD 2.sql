

DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT

DECLARE @Getter INT






SELECT @Id=Id FROM SystemCode  WHERE Code='System Settings'
IF(ISNULL(@Id,0)>0)
BEGIN
SELECT @Description = NULL
SELECT @Description=Id FROM SystemCodeDetail WHERE Code='2017'
SET @DetailCode='CURFINYEAR'
SET @Description=@Description
SET @OrderNo=1
SET @Id = 1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

SELECT @Description = NULL
SELECT @Description=Id FROM SystemCodeDetail WHERE Code='REG PILOT 2018'
SET @DetailCode='CURREGGROUP'
SET @Description=@Description
SET @OrderNo=1
SET @Id = 1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
 
SET @DetailCode='DELETEONSYNC'
SET @Description='1'
SET @OrderNo=1
SET @Id = 1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

END
