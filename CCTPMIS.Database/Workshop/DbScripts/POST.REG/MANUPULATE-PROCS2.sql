﻿
IF NOT OBJECT_ID('GenerateCommunityValList') IS NULL	
DROP PROC GenerateCommunityValList
GO

CREATE PROC GenerateCommunityValList
	@TargetPlanId int
   ,@FilePath varchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword varchar(30)
   ,@UserId int
AS

BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @Male int
	DECLARE @Female int  
	DECLARE @ErrorMsg varchar(128)
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
  DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
  DECLARE @NoOfRows int
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
  IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	
  DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		 FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);
  
  INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
 IF EXISTS (SELECT 1 FROM  ListingPlanHH WHERE TargetPlanId=@TargetPlanId and ListingAcceptId IS NULL AND RejectReason IS   NULL)
	 SET @ErrorMsg='You cannot Generate Exceptions. There Exists Households that have not been Accepted'

DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
   
  SET @SysCode='Sex'
	SET @SysDetailCode='M'
	SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='F'
	SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode 

	DELETE FROM temp_RegExceptions -- WHERE TargetPlanId = @TargetPlanId
	
; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T1.BeneSexId as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 ListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId AND  T1.TargetPlanId = @TargetPlanId


	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	   INSERT INTO temp_RegExceptions (Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,County,Constituency,District,Division,Location,SubLocation) 

	SELECT Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,
	CASE WHEN (Bene_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS Bene_IDNoExists,
	CASE WHEN (Bene_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_FirstNameExists,
	CASE WHEN (Bene_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_MiddleNameExists,
	CASE WHEN (Bene_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS Bene_SurnameExists,
	CASE WHEN (Bene_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBMatch,
	CASE WHEN (Bene_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_DoBYearMatch,
	CASE WHEN (Bene_SexMatch=1) THEN 'YES' ELSE 'NO' END AS Bene_SexMatch,
	CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,
	CASE WHEN (CG_IDNoExists =1) THEN 'YES' ELSE 'NO' END AS CG_IDNoExists,
	CASE WHEN (CG_FirstNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_FirstNameExists,
	CASE WHEN (CG_MiddleNameExists=1) THEN 'YES' ELSE 'NO' END AS CG_MiddleNameExists,
	CASE WHEN (CG_SurnameExists=1) THEN 'YES' ELSE 'NO' END AS CG_SurnameExists,
	CASE WHEN (CG_DoBMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBMatch,
	CASE WHEN (CG_DoBYearMatch=1) THEN 'YES' ELSE 'NO' END AS CG_DoBYearMatch,
	CASE WHEN (CG_SexMatch=1) THEN 'YES' ELSE 'NO' END AS CG_SexMatch,
	
	TargetPlanId,County,Constituency,District,Division,Location,SubLocation FROM X
	WHERE 
	Bene_IDNoExists = 1 AND Bene_SexMatch = 1 AND Bene_DoBYearMatch = 1 AND ( Bene_FirstNameExists =1 OR Bene_MiddleNameExists=1 OR Bene_SurnameExists =1)
	OR 
	CG_IDNoExists = 1 AND CG_SexMatch = 1 AND CG_DoBYearMatch = 1 AND( CG_FirstNameExists =1 OR CG_MiddleNameExists=1 OR  CG_SurnameExists =1)
	AND TargetPlanId=@TargetPlanId
  
  
  EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='REG_VALIDATION_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'
  
	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_RegExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='REG_VALIDATION'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn
	SET @FileCreationId=IDENT_CURRENT('FileCreation')
 

 ; WITH X AS (
	SELECT  
		 T1.Id
		 ,CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS BeneName 
		,T1.BeneSexId as BeneSex
		,T1.BeneDoB as BeneDob
		,T1.BeneNationalIdNo 
		,T2.IDNo AS IPRS_IDNo
		,T2.FirstName+CASE WHEN(T2.MiddleName<>'') THEN ' ' ELSE '' END+T2.MiddleName+CASE WHEN(T2.Surname<>'') THEN ' ' ELSE '' END+T2.Surname AS IPRS_Name
		,T2.Sex AS IPRS_Sex
		,T2.DoB AS IPRS_DoB		
		,CONVERT(bit,ISNULL(T2.IDNo,0)) AS Bene_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS Bene_SurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS Bene_DoBMatch
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS Bene_DoBYearMatch
		,CASE WHEN(T1.BeneSexId=T2.Sex) THEN 1 ELSE 0 END AS Bene_SexMatch 
		--,T1.CgFirstName+CASE WHEN(T1.CgFirstName<>'') THEN ' ' ELSE '' END+T1.CgMiddleName+CASE WHEN(T1.CgSurname<>'') THEN ' ' ELSE '' END+T1.CgSurname AS CgName 
		,CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS CgName 
		,T1.CgSexId as CgSex
		,T1.CgDoB as CgDob
		,T1.CgNationalIdNo 
		,T3.IDNo AS IPRS_CG_IDNo
		,T3.FirstName+CASE WHEN(T3.MiddleName<>'') THEN ' ' ELSE '' END+T3.MiddleName+CASE WHEN(T3.Surname<>'') THEN ' ' ELSE '' END+T3.Surname AS IPRS_CG_Name
		,T3.Sex AS IPRS_CG_Sex
		,T3.DoB AS IPRS_CG_DoB		
		,CONVERT(bit,ISNULL(T3.IDNo,0)) AS CG_IDNoExists
		,CASE WHEN(T1.CgFirstName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_FirstNameExists
		,CASE WHEN(T1.CgMiddleName IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_MiddleNameExists
		,CASE WHEN(T1.CgSurname IN(T3.FirstName,T3.MiddleName,T3.Surname)) THEN 1 ELSE 0 END AS CG_SurnameExists
		,CASE WHEN(T1.CgDoB=T3.DoB) THEN 1 ELSE 0 END AS CG_DoBMatch
		,CASE WHEN(YEAR(T1.CgDoB)=YEAR(T3.DoB)) THEN 1 ELSE 0 END AS CG_DoBYearMatch
		,CASE WHEN(T1.CgSexId=T3.Sex) THEN 1 ELSE 0 END AS CG_SexMatch
		,T5.County,T5.Constituency,T5.District,T5.Division,T5.Location,T5.SubLocation
		,@TargetPlanId TargetPlanId
	 FROM
	 ListingPlanHH T1 
	 INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
													INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
								) T5 ON T1.SubLocationId=T5.SubLocationId  AND  TargetPlanId=@TargetPlanId

	LEFT OUTER JOIN (SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex , Date_of_Birth AS DoB,Serial_Number FROM IPRSCache) AS T2
	 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)  
	 AND  T1.TargetPlanId = @TargetPlanId
	 LEFT OUTER JOIN ( SELECT First_Name AS FirstName, Surname, Middle_Name AS MiddleName, ID_Number AS IDNo, CASE WHEN Gender = 'M' THEN @Male ELSE @Female  END AS Sex, Date_of_Birth AS DoB, Serial_Number FROM IPRSCache) AS T3
	   ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)  ) 
	   
	

  INSERT INTO   ListingException(Id,BeneName,BeneSex,BeneDob,BeneNationalIdNo,IPRS_IDNo,IPRS_Name,IPRS_Sex,IPRS_DoB,Bene_IDNoExists,Bene_FirstNameExists,Bene_MiddleNameExists,Bene_SurnameExists,Bene_DoBMatch,Bene_DoBYearMatch,Bene_SexMatch,CgName,CgSex,CgDob,CgNationalIdNo,IPRS_CG_IDNo,IPRS_CG_Name,IPRS_CG_Sex,IPRS_CG_DoB,CG_IDNoExists,CG_FirstNameExists,CG_MiddleNameExists,CG_SurnameExists,CG_DoBMatch,CG_DoBYearMatch,CG_SexMatch,TargetPlanId,DateValidated) 
  
  
SELECT  DISTINCT X.Id,X.BeneName,X.BeneSex,X.BeneDob,REPLACE(X.BeneNationalIdNo,'.',''),X.IPRS_IDNo,X.IPRS_Name,X.IPRS_Sex,X.IPRS_DoB,
X.Bene_IDNoExists,X.Bene_FirstNameExists,X.Bene_MiddleNameExists,X.Bene_SurnameExists,X.Bene_DoBMatch,X.Bene_DoBYearMatch,
X.Bene_SexMatch,X.CgName,X.CgSex,X.CgDob,REPLACE(X.CGNationalIDNo,'.',''),X.IPRS_CG_IDNo,X.IPRS_CG_Name,X.IPRS_CG_Sex,X.IPRS_CG_DoB,
X.CG_IDNoExists,X.CG_FirstNameExists,X.CG_MiddleNameExists,X.CG_SurnameExists,X.CG_DoBMatch,X.CG_DoBYearMatch,
X.CG_SexMatch,X.TargetPlanId,GETDATE() AS DateValidated   FROM  X
 LEFT JOIN ListingException T2 ON X.Id = T2.Id 

  WHERE X.TargetPlanId=  @TargetPlanId and T2.Id is null
  

  UPDATE T1
	SET T1.ValidationFileId=@FileCreationId
  ,T1.ValBy = @UserId
  ,T1.ValOn = GETDATE()
	FROM  TargetPlan T1
	WHERE T1.Id=@TargetPlanId

	SELECT @FileCreationId AS FileCreationId
  
 END
  
GO