﻿-- DROP TABLES
 IF NOT OBJECT_ID('RegistrationHH') IS NULL	
  DROP TABLE RegistrationHH
GO

IF NOT OBJECT_ID('RegAccept') IS NULL	
  DROP TABLE RegAccept
GO

-- CREATE TABLES

CREATE TABLE RegAccept(
	Id INT NOT NULL IDENTITY(1,1)
   ,AccById int NOT NULL
   ,AccDate DATE NOT NULL DEFAULT GETDATE()
   ,AccApvById int  NULL
   ,AccApvDate DATE  NULL
   ,ReceivedHHs int NOT NULL
   ,BatchName VARCHAR(20) NOT NULL
   ,ConstituencyId INT NOT NULL
   ,RegPlanId INT NOT NULL
   ,CONSTRAINT PK_RegAccept PRIMARY KEY (Id)
   ,CONSTRAINT FK_RegAccept_User_AccById FOREIGN KEY (AccById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_User_AccApvById FOREIGN KEY (AccApvById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_User_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_RegAccept_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   )
GO



CREATE TABLE RegistrationHH(
		 Id INT  NOT NULL IDENTITY(1,1)
		,UniqueId       VARCHAR(36)
		,StartTime      DATETIME NULL
		,EndTime        DATETIME NULL
		,ProgrammeId    TINYINT  NOT NULL
		,RegistrationDate DATETIME NOT NULL
		,SubLocationId  INT  NOT NULL
		,LocationId     INT  NOT NULL
		,Years		  INT  NOT NULL
		,Months		  INT  NOT NULL
		,RegPlanId	   INT  NOT NULL
		,EnumeratorId    INT  NOT NULL
		,HHdFirstName    VARCHAR(20) NOT NULL
		,HHdMiddleName   VARCHAR(20)   NULL
		,HHdSurname      VARCHAR(20) NOT NULL
		,HHdNationalIdNo VARCHAR(10)  NOT NULL
		,HHdSexId       INT  NOT NULL
		,HHdDoB         DATETIME NOT NULL
		,CgFirstName    VARCHAR(25) NOT NULL
		,CgMiddleName   VARCHAR(25)   NULL
		,CgSurname      VARCHAR(25) NOT NULL
		,CgNationalIdNo VARCHAR(10)  NOT NULL
		,CgSexId        INT  NOT NULL
		,CgDoB          DATETIME NULL
		,HouseholdMembers INT  NOT NULL
		,StatusId       INT  NOT NULL
		,Village        VARCHAR(30) NOT NULL
		,PhysicalAddress VARCHAR(50) NOT NULL
		,NearestReligiousBuilding VARCHAR(50) NOT NULL
		,NearestSchool  VARCHAR(50) NOT NULL
		,Longitude     FLOAT NOT NULL
		,Latitude       FLOAT NOT NULL
		,SyncEnumeratorId   INT  NOT NULL
		,DeviceId       VARCHAR(36) NOT NULL
		,DeviceModel    VARCHAR(15) NOT NULL
		,DeviceManufacturer  VARCHAR(15) NOT NULL
		,DeviceName       VARCHAR(15) NOT NULL
		,[Version]        VARCHAR(15)  NOT NULL
		,VersionNumber  VARCHAR(15)  NOT NULL
		,AppVersion     VARCHAR(15) NOT NULL
		,AppBuild       VARCHAR(15)  NOT NULL
		,[Platform]  VARCHAR(15) NOT NULL
		,Idiom VARCHAR(10) NOT NULL
		,IsDevice       BIT NOT NULL
		,RegAcceptId INT NULL
		,CONSTRAINT PK_RegistrationHH PRIMARY KEY (Id)
		,CONSTRAINT FK_RegistrationHH_Enumerator_EnumeratorId FOREIGN KEY (EnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Enumerator_SyncEnumeratorId FOREIGN KEY (SyncEnumeratorId) REFERENCES Enumerator(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_Location_LocationId FOREIGN KEY (LocationId) REFERENCES [Location](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	    ,CONSTRAINT FK_RegistrationHH_RegPlan_RegPlanId FOREIGN KEY (RegPlanId) REFERENCES RegPlan(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
		,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_HHdSexId FOREIGN KEY (HHdSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
        ,CONSTRAINT FK_RegistrationHH_RegAccept_RegAcceptId FOREIGN KEY (RegAcceptId) REFERENCES RegAccept(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO