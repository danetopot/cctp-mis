USE [DCS]
GO
/****** Object:  StoredProcedure [dbo].[GetPMTScore]    Script Date: 21/06/2018 12:00:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetPMTScore]  
   @pTargetingId int  
  ,@pMessage  dbo.description  
  ,@pStatus  int  
AS  
 BEGIN  
  declare @Age        int  
    , @CaregiverAgeScore    float  
    , @CaregiverAgeSquareScore   float  
    , @CaregiverEducationLevel   int  
    , @CaregiverEducationScore   float  
    , @Over18WorkingScore    float  
    , @BoysWorkingScore     float  
    , @GirlsWorkingScore    float  
    , @AdultEquivalence     float  
    , @AdultEquivalenceScore   float  
    , @AdultEquivalenceSquareScore  float  
    , @HouseholdMembers     int  
    , @HouseholdSizeScore    float  
    , @HouseholdSizeSquareScore   float  
    , @Under5Count      int  
    , @Under5Score      float  
    , @Under5SquareScore    float  
    , @OVCUnder17Count     int  
    , @OVCUnder17Score     float  
    , @OVCUnder17SquareScore   float  
    , @WaterSourceScore     float  
    , @LightingSourceScore    float  
    , @FuelSourceScore     float  
    , @ToiletScore      float  
    , @WallScore      float  
    , @RoofScore      float  
    , @FloorScore      float  
    , @ZebuCattleScore     float  
    , @HybridCattleScore    float  
    , @SheepScore      float  
    , @CamelsScore      float  
    , @GoatsScore      float  
    , @ProvincialScore     float  
    , @NoCamelNoZebuScore    float  
    , @NorthEasternNoCamelScore   float  
    , @PMTScore       float  
    , @ConstantValue     float  
    , @Category       int  
    , @TargetingDate     DateTime  
    , @ConstantId      int  
      
      
  select @AdultEquivalence = 0  
  select @PMTScore = 0  
    
  if not exists (select * from TargetingHeader where ( TargetingId = @pTargetingId ) and ( isnull(Result, -1) = 0 ))  
   begin      
    return  
   end  
    
     -- Section 1: Caregiver details  
     --picking age  
   select @Category = Category, @TargetingDate = TargetingDate from SubLocation   
    inner join TargetingHeader on SubLocation.SubLocationId = TargetingHeader.SubLocationId  
   where TargetingId = @pTargetingId  
  
   select @Age = (select dbo.GetAge(DateOfBirth, getdate())), @CaregiverEducationLevel = isnull(EducationId, 0)    
   from TargetingDetail where RelationshipId = ( select top 1 RelationshipId from Relationship where Category = 2 ) and TargetingId = @pTargetingId  
  
   -- caregiver age  
   select @CaregiverAgeScore = @Age * (select Weight from PMTWeight where ConstantId = 1 and Category = @Category)   
   
   select @PMTScore = @PMTScore + isnull(@CaregiverAgeScore, 0)  
     
   delete PMTScore where ( TargetingId = @pTargetingId )  
   insert into PMTScore values (@pTargetingId, 1, isnull(@CaregiverAgeScore, 0))  
     
   --school grade  
   select @CaregiverEducationScore = Weight from PMTWeight  
   inner join Education on ( PMTWeight.ConstantId = Education.PMTConstant )  
   where ( EducationId = isnull(@CaregiverEducationLevel, 0) ) and ( Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@CaregiverEducationScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 2, isnull(@CaregiverEducationScore, 0))  
     
   --adults over 18 and working  
   select @Over18WorkingScore = ( Count(TargetingDetailId) * (select Weight from PMTWeight where ConstantId = 7 and Category = @Category) )  
   from TargetingDetail where TargetingId = @pTargetingId and ( (select dbo.GetAge(DateOfBirth, getdate())) >= 18 ) and ( Working = 1 )  
  
   select @PMTScore = @PMTScore + isnull(@Over18WorkingScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 7, isnull(@Over18WorkingScore, 0))  
     
   --boys working  
   select @BoysWorkingScore = ( Count(TargetingDetailId) * (select Weight from PMTWeight where ConstantId = 5 and Category = @Category) )  
   from TargetingDetail where TargetingId = @pTargetingId and ( (select dbo.GetAge(DateOfBirth, getdate())) between 6 and 17 ) and ( Sex = 'M' )  
                                                          and ( Working = 1 )  
  
   select @PMTScore = @PMTScore + isnull(@BoysWorkingScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 5, isnull(@BoysWorkingScore, 0))  
     
   --girls working  
   select @GirlsWorkingScore = ( Count(TargetingDetailId) * (select Weight from PMTWeight where ConstantId = 6 and Category = @Category) )  
   from TargetingDetail where TargetingId = @pTargetingId and ( (select dbo.GetAge(DateOfBirth, getdate())) between 6 and 17 ) and ( Sex = 'F' )  
                                                          and ( Working = 1 )  
   
   select @PMTScore = @PMTScore + isnull(@GirlsWorkingScore, 0)  
    
   insert into PMTScore values (@pTargetingId, 6, isnull(@GirlsWorkingScore, 0))  
     
   --household size  
   select @HouseholdMembers = COUNT(TargetingDetailId) from TargetingDetail where TargetingId = @pTargetingId  
     
   select @HouseholdSizeScore = @HouseholdMembers * (select Weight from PMTWeight where ConstantId = 8 and Category = @Category)  
     
   select @PMTScore = @PMTScore + isnull(@HouseholdSizeScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 8, isnull(@HouseholdSizeScore, 0))  
     
   --household size square     
   select @HouseholdSizeSquareScore = (@HouseholdMembers * @HouseholdMembers) * (select Weight from PMTWeight where ConstantId = 9 and Category = @Category)  
     
   select @PMTScore = @PMTScore + isnull(@HouseholdSizeSquareScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 9, isnull(@HouseholdSizeSquareScore, 0))  
           
   --@Under5Score  
   select @Under5Count = Count(TargetingDetailId)   
   from TargetingDetail where TargetingId = @pTargetingId and ( (select dbo.GetAge(DateOfBirth, getdate())) <= 5 )  
   
   select @Under5Score = @Under5Count * (select Weight from PMTWeight where ConstantId = 10 and Category = @Category)  
   
   select @PMTScore = @PMTScore + isnull(@Under5Score, 0)  
  
   insert into PMTScore values (@pTargetingId, 10, isnull(@Under5Score, 0))  
     
   --@Under5SquareScore  
   select @Under5SquareScore = (@Under5Count * @Under5Count) * (select Weight from PMTWeight where ConstantId = 11 and Category = @Category)  
  
   select @PMTScore = @PMTScore + isnull(@Under5SquareScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 11, isnull(@Under5SquareScore, 0))  
     
   --@OVCUnder17Score   --altered 29/03/2014 by Richard Chirchir to include Vulnerable Children
   --illness=1 or disabled=1
   select @OVCUnder17Count = Count(TargetingDetailId)   
   from TargetingDetail where TargetingId = @pTargetingId and ( (select dbo.GetAge(DateOfBirth, getdate())) <= 17 ) 
   and ((( BiologicalFatherAlive = 2 ) or ( BiologicalMotherAlive = 2 ))  
   or((Illness=1) or (Disabled=1)))
  
   select @OVCUnder17Score = @OVCUnder17Count * (select Weight from PMTWeight where ConstantId = 12 and Category = @Category)  
  
   select @PMTScore = @PMTScore + isnull(@OVCUnder17Score, 0)  
  
   insert into PMTScore values (@pTargetingId, 12, isnull(@OVCUnder17Score, 0))  
     
   --@OVCUnder17SquareScore  
   select @OVCUnder17SquareScore = (@OVCUnder17Score * @OVCUnder17Score) * (select Weight from PMTWeight where ConstantId = 13 and Category = @Category)  
   
   select @PMTScore = @PMTScore + isnull(@OVCUnder17SquareScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 13, isnull(@OVCUnder17SquareScore, 0))  
     
   --@WaterSourceScore  
   select @WaterSourceScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( WaterSource = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@WaterSourceScore, 0)  
  
   if (isnull(@WaterSourceScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@WaterSourceScore, 0))  
    end  
      
   --@LightingSourceScore  
   select @LightingSourceScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( LightingSource = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@LightingSourceScore, 0)  
  
   if (isnull(@LightingSourceScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@LightingSourceScore, 0))  
    end  
      
   --@FuelSourceScore  
   select @FuelSourceScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( FuelSource = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@FuelSourceScore, 0)  
  
   if (isnull(@FuelSourceScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@FuelSourceScore, 0))  
    end  
      
   --@ToiletScore  
   select @ToiletScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( Toilet = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@ToiletScore, 0)  
  
   if (isnull(@ToiletScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@ToiletScore, 0))  
    end  
      
   --@WallScore  
   select @WallScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( Wall = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@WallScore, 0)  
     
   if (isnull(@WallScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@WallScore, 0))  
    end  
      
   --@RoofScore  
   select @RoofScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( Roof = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@RoofScore, 0)  
  
   if (isnull(@RoofScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@RoofScore, 0))  
    end  
      
   --@FloorScore  
   select @FloorScore = Weight, @ConstantId = Parameter.PMTConstant from TargetingHeader   
    inner join Parameter on ( Floor = ParameterId)  
    inner join PMTConstant on ( Parameter.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTConstant.ConstantId = PMTWeight.ConstantId )  
   where TargetingId = @pTargetingId and ( PMTWeight.Category = @Category )  
  
   select @PMTScore = @PMTScore + isnull(@FloorScore, 0)  
     
   if (isnull(@FloorScore, 0) <> 0)  
    begin  
     insert into PMTScore values (@pTargetingId, ISNULL(@ConstantId, 0), isnull(@FloorScore, 0))  
    end  
      
   --@ZebuCattleScore  
   select @ZebuCattleScore = (select Weight from PMTWeight where ConstantId = 28 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and ZebuCattle = 0  
  
   select @PMTScore = @PMTScore + isnull(@ZebuCattleScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 28, isnull(@ZebuCattleScore, 0))  
     
   --@HybridCattleScore  
   select @HybridCattleScore = (select Weight from PMTWeight where ConstantId = 29 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and HybridCattle between 1 and 2  
     
   select @PMTScore = @PMTScore + isnull(@HybridCattleScore, 0)  
        
   insert into PMTScore values (@pTargetingId, 29, isnull(@HybridCattleScore, 0))  
   select @HybridCattleScore = 0  
     
   select @HybridCattleScore = (select Weight from PMTWeight where ConstantId = 30 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and HybridCattle > 2   
  
   select @PMTScore = @PMTScore + isnull(@HybridCattleScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 30, isnull(@HybridCattleScore, 0))  
     
   --@SheepScore  
   select @SheepScore = (select Weight from PMTWeight where ConstantId = 31 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and Sheep between 1 and 5  
  
   select @PMTScore = @PMTScore + isnull(@SheepScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 31, isnull(@SheepScore, 0))  
   select @SheepScore = 0  
     
   select @SheepScore = (select Weight from PMTWeight where ConstantId = 32 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and Sheep > 5  
  
   select @PMTScore = @PMTScore + isnull(@SheepScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 32, isnull(@SheepScore, 0))  
     
   --@CamelsScore  
   select @CamelsScore = (select Weight from PMTWeight where ConstantId = 33 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and Camels = 0  
  
   select @PMTScore = @PMTScore + isnull(@CamelsScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 33, isnull(@CamelsScore, 0))  
     
   --@GoatsScore  
   select @GoatsScore = (select Weight from PMTWeight where ConstantId = 34 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and Goats = 0  
  
   select @PMTScore = @PMTScore + isnull(@GoatsScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 34, isnull(@GoatsScore, 0))  
     
   --@ProvincialScore  
   select @ProvincialScore = Weight  
   from TargetingHeader   
    inner join SubLocation on TargetingHeader.SubLocationId = SubLocation.SubLocationId   
    inner join Location on SubLocation.LocationId = Location.LocationId   
    inner join Division on Location.DivisionId = Division.DivisionId   
    inner join District on Division.DistrictId = District.DistrictId   
    inner join County on District.CountyId = County.CountyId   
    inner join Province on County.ProvinceId = Province.ProvinceId  
    inner join PMTConstant on ( Province.PMTConstant = PMTConstant.ConstantId )  
    inner join PMTWeight on ( PMTWeight.ConstantId = PMTConstant.ConstantId )  
   where TargetingId = @pTargetingId and PMTWeight.Category = @Category  
     
   select @PMTScore = @PMTScore + isnull(@ProvincialScore, 0)  
  
   insert into PMTScore values (@pTargetingId, 35, isnull(@ProvincialScore, 0))  
     
   --NoCamelNoZebuScore  
   select @NoCamelNoZebuScore = (select Weight from PMTWeight where ConstantId = 41 and Category = @Category)   
   from TargetingHeader where TargetingId = @pTargetingId and Camels = 0 and ZebuCattle = 0  
  
   select @PMTScore = @PMTScore + isnull(@NoCamelNoZebuScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 41, isnull(@NoCamelNoZebuScore, 0))  
  
   --NorthEasternNoCamelScore  
   select @NorthEasternNoCamelScore = (select Weight from PMTWeight where ConstantId = 42 and Category = @Category)   
   from TargetingHeader   
    inner join SubLocation on TargetingHeader.SubLocationId = SubLocation.SubLocationId   
    inner join Location on SubLocation.LocationId = Location.LocationId   
    inner join Division on Location.DivisionId = Division.DivisionId   
    inner join District on Division.DistrictId = District.DistrictId   
    inner join County on District.CountyId = County.CountyId   
    inner join Province on County.ProvinceId = Province.ProvinceId  
   where Province.ProvinceCode = 5 and Camels = 0  
  
   select @PMTScore = @PMTScore + isnull(@NorthEasternNoCamelScore, 0)  
     
   insert into PMTScore values (@pTargetingId, 42, isnull(@NorthEasternNoCamelScore, 0))  
     
   --constant  
   select @ConstantValue = Weight from PMTWeight where ConstantId = 43 and Category = @Category  
     
   select @PMTScore = @PMTScore + isnull(@ConstantValue, 0)  
     
   insert into PMTScore values (@pTargetingId, 43, isnull(@ConstantValue, 0))  
     
   update TargetingHeader set PMTScore = @PMTScore where TargetingId = @pTargetingId  
 END
