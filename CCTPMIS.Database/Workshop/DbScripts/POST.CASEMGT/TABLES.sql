﻿-----------------DB TABLES---------------------

IF NOT OBJECT_ID('CaseAction') IS NULL	
DROP Table CaseAction
GO

CREATE TABLE CaseIssue(
	Id int NOT NULL IDENTITY(1,1)
   ,CaseTypeId int NOT NULL
   ,CaseCategoryId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,UpdateEffectId int NULL
   ,MaxLimit tinyint NOT NULL DEFAULT(0)
   ,SLADays tinyint NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CaseIssue PRIMARY KEY (Id)
   ,CONSTRAINT FK_CaseIssue_SystemCodeDetail_CaseTypeId FOREIGN KEY (CaseTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_SystemCodeDetail_CaseCategoryId FOREIGN KEY (CaseCategoryId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_SystemCodeDetail_UpdateEffectId FOREIGN KEY (UpdateEffectId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_CaseIssue_Code UNIQUE NONCLUSTERED(Code)
)
GO



CREATE TABLE CaseFlow(
	Id int NOT NULL IDENTITY(1,1)
   ,CaseIssueId int NOT NULL
   ,StepNo tinyint NOT NULL
   ,StepName varchar(30) NOT NULL
   ,[Description] varchar(128) NULL
   ,UserGroupId int NOT NULL
   ,StatusId int NOT NULL
   ,CanResolve bit NOT NULL DEFAULT(0)
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeletedBy int NULL
   ,DeletedOn datetime NULL
   ,CONSTRAINT PK_CaseFlow PRIMARY KEY (Id)
   ,CONSTRAINT FK_CaseFlow_CaseIssue_CaseIssueId FOREIGN KEY (CaseIssueId) REFERENCES CaseIssue(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseFlow_UserGroup_UserGroupId FOREIGN KEY (UserGroupId) REFERENCES UserGroup(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseFlow_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseFlow_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION 
   ,CONSTRAINT FK_CaseFlow_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseFlow_User_DeletedBy FOREIGN KEY (DeletedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_CaseFlow_CaseIssueId_StepNo UNIQUE NONCLUSTERED(CaseIssueId,StepNo)
)
GO



CREATE TABLE CaseFlowDoc(
	Id int NOT NULL IDENTITY(1,1)
   ,CaseFlowId int NOT NULL
   ,SuppDocId int NOT NULL
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,DeletedBy int NULL
   ,DeletedOn datetime NULL
   ,CONSTRAINT PK_CaseFlowDocument PRIMARY KEY (Id)
   ,CONSTRAINT FK_CaseFlowDocument_CaseFlow_CaseFlowId FOREIGN KEY (CaseFlowId) REFERENCES CaseFlow(Id) ON UPDATE NO ACTION ON DELETE CASCADE
   ,CONSTRAINT FK_CaseFlowDocument__SystemCodeDetail_SuppDocId FOREIGN KEY (SuppDocId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_CaseFlowDocument_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION 
   ,CONSTRAINT FK_CaseFlowDocument_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseFlowDocument_User_DeletedBy FOREIGN KEY (DeletedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO



CREATE TABLE CaseReport(
	Id int NOT NULL IDENTITY(1,1)
   ,SerialNo varchar(20) NULL
   ,ProgrammeId tinyint NOT NULL
   ,BeneProgNoPrefix varchar(10) NULL
   ,ProgrammeNo int NULL
   ,MemberRoleId int NULL
   ,PersonId int NULL
   ,Names varchar(100) NULL
   ,SexId int NULL
   ,IdNo varchar(30) NULL
   ,MobileNo nvarchar(20) NULL
   ,CaseIssueId int NOT NULL
   ,Notes varchar(4000) NULL
   ,CountyId int NULL
   ,ConstituencyId int NULL
   ,SubLocationId int NULL
   ,ReceivedBy varchar(100) NOT NULL
   ,ReceivedByDesig varchar(50) NULL
   ,ReceivedOn datetime NOT NULL
   ,FileCreationId int NULL
   ,CaseActionId int NOT NULL -- THE LATEST ACTION ON THE CASE
   ,CaseFlowId int NOT NULL -- THE NEXT STEP IN WORKFLOW FOR PROCESSING
   ,StatusId int NOT NULL	-- THE (PREVIOUS) WORKFLOW ASSOCIATED STATUS
   ,CreatedBy int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CaseReport PRIMARY KEY (Id)
   ,CONSTRAINT FK_CaseReport_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_SystemCodeDetail_MemberRoleId FOREIGN KEY (MemberRoleId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_SystemCodeDetail_SexId FOREIGN KEY (SexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_CaseIssue_CaseIssueId FOREIGN KEY (CaseIssueId) REFERENCES CaseIssue(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_County_CountyId FOREIGN KEY (CountyId) REFERENCES County(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_Constituency_ConstituencyId FOREIGN KEY (ConstituencyId) REFERENCES Constituency(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_SubLocation_SubLocationId FOREIGN KEY (SubLocationId) REFERENCES SubLocation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_FileCreation FOREIGN KEY (FileCreationId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_SystemCodeDetail_CaseActionId FOREIGN KEY (CaseActionId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_CaseFlow_CaseFlowId FOREIGN KEY (CaseFlowId) REFERENCES CaseFlow(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_SystemCodeDetail_StatusId FOREIGN KEY (StatusId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_User_CreatedBy FOREIGN KEY (CreatedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReport_User_ModifiedBy FOREIGN KEY (ModifiedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_CaseReport_SerialNo UNIQUE NONCLUSTERED(SerialNo)
)
GO


CREATE TABLE CaseReportHistory(
	CaseReportId int NOT NULL
   ,RelationshipId int NULL
   ,PersonId int NULL
   ,FirstName varchar(50)
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50)
   ,SexId int NULL
   ,DoB datetime NULL
   ,BirthCertNo varchar(50) NULL
   ,NationalIdNo varchar(30) NULL
   ,MobileNo1 nvarchar(20) NULL
   ,MobileNo2 nvarchar(20) NULL
   ,SubLocationId int NULL
   ,UpdatedRelationshipId int NULL
   ,UpdatedPersonId int NULL
   ,UpdatedFirstName varchar(50) NULL
   ,UpdatedMiddleName varchar(50) NULL
   ,UpdatedSurname varchar(50) NULL
   ,UpdatedSexId int NULL
   ,UpdatedDoB datetime NULL
   ,UpdatedBirthCertNo varchar(50) NULL
   ,UpdatedNationalIdNo varchar(30) NULL
   ,UpdatedMobileNo1 nvarchar(20) NULL
   ,UpdatedMobileNo2 nvarchar(20) NULL
   ,UpdatedSubLocationId int NULL
   ,CONSTRAINT FK_CaseReportHistory_CaseReport_CaseReportId FOREIGN KEY (CaseReportId) REFERENCES CaseReport(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReportHistory_SystemCodeDetail_RelationshipId FOREIGN KEY (RelationshipId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReportHistory_Person_PersonId FOREIGN KEY (PersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReportHistory_Person_UpdatedPersonId FOREIGN KEY (UpdatedPersonId) REFERENCES Person(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseReportHistory_SystemCodeDetail_UpdatedRelationshipId FOREIGN KEY (UpdatedRelationshipId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
)
GO




CREATE TABLE CaseAction(
	Id int NOT NULL IDENTITY(1,1)
   ,CaseReportId int NOT NULL
   ,CaseFlowId int NOT NULL
   ,CaseActionId int NOT NULL
   ,Notes nvarchar(128) NOT NULL
   ,FileCreationId int NULL
   ,ActionedBy int NOT NULL
   ,ActionedOn datetime NOT NULL
   ,CONSTRAINT PK_CaseAction PRIMARY KEY(Id)
   ,CONSTRAINT FK_CaseAction_CaseReport_CaseReportId FOREIGN KEY (CaseReportId) REFERENCES CaseReport(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseAction_CaseFlow_CaseFlowId FOREIGN KEY (CaseFlowId) REFERENCES CaseFlow(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseAction_SystemCodeDetail_CaseActionId FOREIGN KEY (CaseActionId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseAction_FileCreation_FileCreationId FOREIGN KEY (FileCreationId) REFERENCES FileCreation(Id) ON UPDATE NO ACTION
   ,CONSTRAINT FK_CaseAction_User_ActionedBy FOREIGN KEY (ActionedBy) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION

)
GO


---------------DATA MANIPULATION SPS-------------------


