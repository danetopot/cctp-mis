﻿IF NOT OBJECT_ID('ChangeCategory') IS NULL	
DROP TABLE ChangeCategory
GO

IF NOT OBJECT_ID('Change') IS NULL	
DROP TABLE Change
GO

CREATE TABLE ChangeCategory(
	Id int NOT NULL IDENTITY(1,1)
   ,CaseTypeId int NOT NULL
   ,Code varchar(20) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,MaxLimit tinyint NOT NULL DEFAULT(0)
   ,SLADays tinyint NOT NULL DEFAULT(0)
   ,CreatedById int NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedById int NULL
   ,ModifiedOn datetime NULL
   ,CONSTRAINT PK_CaseIssue PRIMARY KEY (Id)
   ,CONSTRAINT FK_CaseIssue_SystemCodeDetail_CaseTypeId FOREIGN KEY (CaseTypeId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_User_CreatedBy FOREIGN KEY (CreatedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT FK_CaseIssue_User_ModifiedBy FOREIGN KEY (ModifiedById) REFERENCES [User](Id) ON UPDATE NO ACTION ON DELETE NO ACTION
   ,CONSTRAINT IX_CaseIssue_Code UNIQUE NONCLUSTERED(Code)
)
GO


IF NOT OBJECT_ID('Change') IS NULL	
DROP TABLE Change
GO
CREATE TABLE Change(
	Id INT NOT NULL IDENTITY(1,1)
	,ProgrammeId TINYINT NOT NULL
	,HhEnrolmentId INT NOT NULL
	,Document VARCHAR(MAX) NULL
	,ChangeDate datetime NULL
	,ChangeTypeId INT NOT NULL
	,SRNationalId VARCHAR(MAX) NULL
	,Notes VARCHAR(MAX) NULL
	,VillageId INT NULL
	,NotificationDate datetime NULL
	,DeathDate datetime NULL
	,DateCreated datetime NULL
	,DateActioned datetime NULL
	,ActionedById VARCHAR(128) NULL
	,CreatedById VARCHAR(128) NULL
	,SRGenderId INT NULL
	,BeneficiaryName VARCHAR(MAX) NULL
	,SRName VARCHAR(MAX) NULL
	,StatusId INT NOT NULL
	,FamilyResolutionDocument VARCHAR(MAX) NULL
	,KinName VARCHAR(MAX) NULL
	,KinNationalId VARCHAR(MAX) NULL
	,KinGenderId INT NULL
	,ChangeSourceId INT NULL
   ,RelationshipId int NULL
   ,PersonId int NULL
   ,FirstName varchar(50)
   ,MiddleName varchar(50) NULL
   ,Surname varchar(50)
   ,SexId int NULL
   ,DoB datetime NULL
   ,BirthCertNo varchar(50) NULL
   ,NationalIdNo varchar(30) NULL
   ,MobileNo1 nvarchar(20) NULL
   ,MobileNo2 nvarchar(20) NULL
   ,SubLocationId int NULL
   ,UpdatedRelationshipId int NULL
   ,UpdatedPersonId int NULL
   ,UpdatedFirstName varchar(50) NULL
   ,UpdatedMiddleName varchar(50) NULL
   ,UpdatedSurname varchar(50) NULL
   ,UpdatedSexId int NULL
   ,UpdatedDoB datetime NULL
   ,UpdatedBirthCertNo varchar(50) NULL
   ,UpdatedNationalIdNo varchar(30) NULL
   ,UpdatedMobileNo1 nvarchar(20) NULL
   ,UpdatedMobileNo2 nvarchar(20) NULL
   ,UpdatedSubLocationId int NULL
   ,BenName varchar  (50) NULL,
	BenIdNo varchar  (50) NULL,
	BenPhone varchar  (50) NULL,
	BenSexId int NULL,
    CgName varchar  (50) NULL,
	CgIdNo varchar  (50) NULL,
	CgPhone varchar  (50) NULL,
	CgSexId int NULL,
	ClosedBy INT NULL,
	ClosedOn datetime null
	,CreatedBy INT NOT NULL
   ,CreatedOn datetime NOT NULL
   ,ModifiedBy INT NULL
   ,ModifiedOn datetime NULL
   ,ApvBy int NULL
   ,ApvOn datetime NULL
   ,ActionedDate datetime NULL,
 	ReceivedBy varchar  (50) NULL,	
	Designation varchar  (50) NULL,	
	Names varchar  (50) NULL,	
	IdNo varchar  (50) NULL,
     MobileNo varchar  (50) NULL

	 ,CONSTRAINT FK_Change_BenSex_BenSexId FOREIGN KEY (BenSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
	    ,CONSTRAINT FK_Change_CgSex_CgSexId FOREIGN KEY (CgSexId) REFERENCES SystemCodeDetail(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
       
    ,CONSTRAINT PK_Change PRIMARY KEY (Id)
    ,CONSTRAINT FK_Change_Programme_ProgrammeId FOREIGN KEY (ProgrammeId) REFERENCES Programme(Id) ON UPDATE NO ACTION ON DELETE NO ACTION
    ,CONSTRAINT FK_Change_HouseholdEnrolment_HhEnrolmentId FOREIGN KEY (HhEnrolmentId) REFERENCES HouseholdEnrolment(Id) ON UPDATE NO ACTION ON DELETE NO ACTION

)
GO


 IF NOT OBJECT_ID('ChangeNote') IS NULL	
DROP TABLE ChangeNote
GO
CREATE TABLE  ChangeNote(
	Id int IDENTITY(1,1) NOT NULL,
	ChangeId int NOT NULL,
	CreatedById int NOT NULL,
	DateCreated datetime  NULL,
	Note nvarchar(max) NOT NULL,
	EntryDate datetime  NULL,
	CategoryId int NOT NULL
    ,CONSTRAINT PK_ChangeNote PRIMARY KEY (Id)
	)
GO



