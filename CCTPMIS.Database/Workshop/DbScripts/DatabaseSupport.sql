﻿------------------------------------------------------
IF NOT OBJECT_ID('GetClassFromTable') IS NULL	
DROP PROCEDURE GetClassFromTable
GO
CREATE PROCEDURE  GetClassFromTable(@TableName sysname )
AS
BEGIN
DECLARE @Result VARCHAR(max) = 'public class ' + @TableName + '{'
SELECT @Result = @Result + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }'
FROM
( SELECT  REPLACE(col.name, ' ', '_') ColumnName, column_id ColumnId,
		CASE typ.name 
			WHEN 'bigint' THEN 'lONg'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'mONey' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmONey' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			else 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'mONey', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmONey', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			else '' 
		END NullableSign
	FROM sys.columns col
		join sys.types typ ON
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	where object_id = object_id(@TableName)
) t
ORDER BY ColumnId
SET @Result = @Result  + '
}'
print @Result
END
GO
------------------------------------------------------

IF NOT OBJECT_ID('WriteToFile') IS NULL	
DROP PROCEDURE WriteToFile
GO
CREATE PROCEDURE WriteToFile
@File        VARCHAR(2000),
@Text        VARCHAR(MAX)
AS 
BEGIN 
DECLARE @OLE            INT 
DECLARE @FileID         INT 
EXECUTE sp_OACreate 'Scripting.FileSystemObject', @OLE OUT 
EXECUTE sp_OAMethod @OLE, 'OpenTextFile', @FileID OUT, @File, 8, 1 
EXECUTE sp_OAMethod @FileID, 'WriteLine', Null, @Text
EXECUTE sp_OADestroy @FileID 
EXECUTE sp_OADestroy @OLE 
END  
GO
------------------------------------------------------
IF NOT OBJECT_ID('GetTableClassesFromDatabase') IS NULL	
DROP PROCEDURE GetTableClassesFromDatabase
GO
CREATE  PROC GetTableClassesFromDatabase
@File VARCHAR(2000),
@NameSpace VARCHAR(2000)
AS

BEGIN
DECLARE @LoopId INT = 1
DECLARE @TableName VARCHAR(100) =''
DECLARE @Tab CHAR = CHAR(9)
DECLARE @NewLine CHAR = CHAR(10)
DECLARE @CarriageReturn CHAR = CHAR(13)
DECLARE @OLE INT 
DECLARE @FileID INT  
DECLARE @Code VARCHAR(MAX) = ''
DECLARE @tblLooper TABLE( Id INT NOT NULL IDENTITY(1,1),TableName VARCHAR(100))


DECLARE @DateStamp VARCHAR(2000)

SELECT  @DateStamp = CONVERT(VARCHAR(10), GETDATE(), 102)

INSERT INTO @tblLooper(TableName)
SELECT [NAME] FROM dbo.SysObjects  WHERE TYPE = 'u'  ORDER BY  [NAME] ASC 
WHILE EXISTS(SELECT 1 FROM @tblLooper WHERE Id=@LoopId)
BEGIN
SELECT @TableName = TableName FROM @tblLooper WHERE Id=@LoopId  
DECLARE @Result VARCHAR(MAX) = @NewLine  + @NewLine + 'public class ' + @TableName +'
{'
	
SELECT @Result = @Result + @NewLine  + @Tab  + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }
' FROM
( SELECT 
		replace(col.name, ' ', '_') ColumnName,
		column_id ColumnId,
		case typ.name 
			WHEN 'bigint' THEN 'long'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'money' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmoney' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			ELSE 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'money', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmoney', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			ELSE '' 
		END NullableSign
	FROM sys.columns col
		JOIN sys.types typ on
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	WHERE object_id = object_id(@TableName)
) t
ORDER BY ColumnId

SET @Result = @Result  +'}'
SET @Code = @Code + @Result
SET @LoopId = @LoopId+1 
END
SET @Code = @Code + @NewLine
SELECT @File = CONCAT( @File, DB_NAME(), '_',  @DateStamp,'.cs')
 EXEC dbo.WriteToFile @File,   @Code
END
GO
------------------------------------------------------





------------------------------------------------------
IF NOT OBJECT_ID('GetClassFromTable') IS NULL	
DROP PROCEDURE GetClassFromTable
GO
CREATE PROCEDURE  GetClassFromTable(@TableName sysname )
AS
BEGIN
DECLARE @Result VARCHAR(max) = 'public class ' + @TableName + '{'
SELECT @Result = @Result + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }'
FROM
( SELECT  REPLACE(col.name, ' ', '_') ColumnName, column_id ColumnId,
		CASE typ.name 
			WHEN 'bigint' THEN 'lONg'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'mONey' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmONey' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			else 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'mONey', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmONey', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			else '' 
		END NullableSign
	FROM sys.columns col
		join sys.types typ ON
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	where object_id = object_id(@TableName)
) t
ORDER BY ColumnId
SET @Result = @Result  + '
}'
print @Result
END
GO
------------------------------------------------------

IF NOT OBJECT_ID('WriteToFile') IS NULL	
DROP PROCEDURE WriteToFile
GO
CREATE PROCEDURE WriteToFile
@File        VARCHAR(2000),
@Text        VARCHAR(MAX)
AS 
BEGIN 
DECLARE @OLE            INT 
DECLARE @FileID         INT 
EXECUTE sp_OACreate 'Scripting.FileSystemObject', @OLE OUT 
EXECUTE sp_OAMethod @OLE, 'OpenTextFile', @FileID OUT, @File, 8, 1 
EXECUTE sp_OAMethod @FileID, 'WriteLine', Null, @Text
EXECUTE sp_OADestroy @FileID 
EXECUTE sp_OADestroy @OLE 
END  
GO
------------------------------------------------------
IF NOT OBJECT_ID('GetTableClassesFromDatabase') IS NULL	
DROP PROCEDURE GetTableClassesFromDatabase
GO
CREATE  PROC GetTableClassesFromDatabase
@File VARCHAR(2000),
@NameSpace VARCHAR(2000)
AS

BEGIN
DECLARE @LoopId INT = 1
DECLARE @TableName VARCHAR(100) =''
DECLARE @Tab CHAR = CHAR(9)
DECLARE @NewLine CHAR = CHAR(10)
DECLARE @CarriageReturn CHAR = CHAR(13)
DECLARE @OLE INT 
DECLARE @FileID INT  
DECLARE @Code VARCHAR(MAX) = ''
DECLARE @tblLooper TABLE( Id INT NOT NULL IDENTITY(1,1),TableName VARCHAR(100))


DECLARE @DateStamp VARCHAR(2000)

SELECT  @DateStamp = CONVERT(VARCHAR(10), GETDATE(), 102)

INSERT INTO @tblLooper(TableName)
SELECT [NAME] FROM dbo.SysObjects  WHERE TYPE = 'u'  ORDER BY  [NAME] ASC 
WHILE EXISTS(SELECT 1 FROM @tblLooper WHERE Id=@LoopId)
BEGIN
SELECT @TableName = TableName FROM @tblLooper WHERE Id=@LoopId  
DECLARE @Result VARCHAR(MAX) = @NewLine  + @NewLine + 'public class ' + @TableName +'
{'
	
SELECT @Result = @Result + @NewLine  + @Tab  + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }
' FROM
( SELECT 
		replace(col.name, ' ', '_') ColumnName,
		column_id ColumnId,
		case typ.name 
			WHEN 'bigint' THEN 'long'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'money' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmoney' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			ELSE 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'money', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmoney', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			ELSE '' 
		END NullableSign
	FROM sys.columns col
		JOIN sys.types typ on
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	WHERE object_id = object_id(@TableName)
) t
ORDER BY ColumnId

SET @Result = @Result  +'}'
SET @Code = @Code + @Result
SET @LoopId = @LoopId+1 
END
SET @Code = @Code + @NewLine
SELECT @File = CONCAT( @File, DB_NAME(), '_',  @DateStamp,'.cs')
 EXEC dbo.WriteToFile @File,   @Code
END
GO
------------------------------------------------------


IF NOT OBJECT_ID('AddEditSystemCode') IS NULL	
DROP PROC AddEditSystemCode
GO
CREATE PROC AddEditSystemCode
	@Id int=NULL
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCode WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCode T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCode(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END

GO



IF NOT OBJECT_ID('AddEditSystemCodeDetail') IS NULL	
DROP PROC AddEditSystemCodeDetail
GO


CREATE PROC AddEditSystemCodeDetail

	@Id int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(20)=NULL
   ,@DetailCode varchar(20)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=Id
		FROM SystemCode
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SystemCodeId AND Code=@DetailCode AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.Code=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetail T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('SystemCodeDetail')
	END

	SET @NoOfRows=@@ROWCOUNTt
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		--EXEC GetSystemCodeDetails	
	END
END

GO












EXEC GetTableClassesFromDatabase 
@File ='c:\\WWWROOT\REG.CS',
@NameSpace ='cctp'




