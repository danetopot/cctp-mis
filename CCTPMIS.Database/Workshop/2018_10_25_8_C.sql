

IF NOT OBJECT_ID('IPRSCheckmember') IS NULL	
DROP PROC IPRSCheckmember
GO
CREATE PROC IPRSCheckmember
	@IdNumber VARCHAR(10)
AS
BEGIN

 DECLARE @SysCode varchar(20)  
 DECLARE @SysDetailCode varchar(20) 
 DECLARE @Male int  
 DECLARE @Female INT
 
 SET @SysCode='Sex'  
 SET @SysDetailCode='M'  
 SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
 SET @SysDetailCode='F'  
 SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode   
  


 SELECT  First_Name, Surname, Middle_Name, ID_Number, CASE WHEN Gender ='M' THEN CONVERT(VARCHAR,@Male) ELSE CONVERT(VARCHAR,@FeMale) END AS Gender, Date_of_Birth, Date_of_Issue, Place_of_Birth, Serial_Number, Address, Status, DateCached, Gender AS Sex FROM  IPRSCache

 WHERE ID_Number =@IdNumber
END
GO


 EXEC dbo.IPRSCheckmember @IdNumber = '25025823'  
 