

IF NOT OBJECT_ID('FinalizeApvRegistrationPlan') IS NULL	
DROP PROC FinalizeApvRegistrationPlan
GO
CREATE PROC FinalizeApvRegistrationPlan
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @StatusId INT = NULL 
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@StatusId,'')=''
   SET @ErrorMsg='Exercise Status must be specified'

	IF NOT EXISTS(SELECT 1 FROM  TargetPlan WHERE Id=@Id AND StatusId=@StatusId)
		SET @ErrorMsg='The specified Registration Exercise is not Finalized, It cannot be Approved for  Validation'
	
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='PMTASS'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 
	INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode 
	AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FinalizeApvBy=@UserId
	   ,T1.FinalizeApvOn=GETDATE()
	   ,T1.StatusId=@StatusId
	FROM  TargetPlan T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END
GO
