DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int

SET @MIG_STAGE = 1




IF @MIG_STAGE = 1	--1. GENERAL SETUP
BEGIN	
	SET @Code='System Settings'
	SET @Description='The System Settings'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Beneficiary Type'
	SET @Description='Whether household or individual beneficiary'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='HHStatus'
	SET @Description='The household status in the programme'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Account Status'
	SET @Description='The beneficiary bank account status'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Card Status'
	SET @Description='The beneficiary payment card status'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Member Status'
	SET @Description='The beneficiary bank account status'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Enrolment Status'
	SET @Description='The enrolment stages'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Status'
	SET @Description='The payment/payroll status'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Stage'
	SET @Description='The payment/payroll stages'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Frequency'
	SET @Description='The frequency of programme beneficiary payment'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Payment Adjustment'
	SET @Description='The payment/payroll adjustments'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='GeoUnit'
	SET @Description='The geographical units available in the system'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Locality'
	SET @Description='Whether a location is considered to be rural or urban'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Sex'
	SET @Description='The sex of a person'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;
	
	SET @Code='Registration Group'
	SET @Description='Associates a potential beneficiary with a particular registration effort'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Enrolment Group'
	SET @Description='Associates a beneficiary with a particular enrolment effort'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Member Role'
	SET @Description='The household member role'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Relationship'
	SET @Description='How a member relates to the beneficiary'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='System Right'
	SET @Description='MIS system right'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Financial Year'
	SET @Description='Programme Financial Year'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Livelihood'
	--SET @Description='Various kinds of livelihoods'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Ailment'
	--SET @Description='Various kinds of ailements'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Disability'
	--SET @Description='Various kinds of ailements'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Education Level'
	--SET @Description='Various highest levels of education'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Level'
	--SET @Description='Various levels from which a grievance is reported'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Type'
	--SET @Description='Various types of grievances'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Reporting Channel'
	--SET @Description='The medium used to relay information'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Complainant Type'
	--SET @Description='Various types of grievance complainants'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Grievance Resolution'
	--SET @Description='All that is done to reported grievances'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Age Bracket'
	--SET @Description='A persons age bracket'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Illiteracy Reason'
	--SET @Description='Reasons for not attending school'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
			
	--SET @Code='Orphanhood'
	--SET @Description='Various types of orphanhood statuses'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
	
	--SET @Code='Marital Status'
	--SET @Description='Marital statuses'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;
	
	--SET @Code='Household Item'
	--SET @Description='Household Items'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Wall Material'
	--SET @Description='Wall Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Floor Material'
	--SET @Description='Floor Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Roof Material'
	--SET @Description='Roof Material'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Toilet Type'
	--SET @Description='Toilet Type'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Water Source'
	--SET @Description='Water Source'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Lighting Source'
	--SET @Description='Lighting Source'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Cooking Fuel'
	--SET @Description='Cooking Fuel'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Other SP Programme'
	SET @Description='Other Social Assistance Programme'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='SP Benefit Type'
	SET @Description='Social Protection Benefit Type'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Frequency'
	--SET @Description='Frequency'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Income'
	--SET @Description='Income'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Expenditure'
	--SET @Description='Expenditure'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Food Type'
	--SET @Description='Food Type'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Skipped Meals Range'
	--SET @Description='Skipped Meals Range'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	--SET @Code='Savings Storage'
	--SET @Description='How savings gets stored'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=1;

	SET @Code='Calendar Months'
	SET @Description='The twelve calendar months'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Change Type'
	--SET @Description='The type of household update'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	--SET @Code='Change Justification'
	--SET @Description='The reason for household update'
	--EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='File Type'
	SET @Description='The information classification for the file'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='File Creation Type'
	SET @Description='How the file was created'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Exception Type'
	SET @Description=''
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;


	---------------BENEFICIARY TYPE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Beneficiary Type'
	SET @DetailCode='HOUSEHOLD'
	SET @Description='Household Entitlement'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INDIVIDUAL'
	SET @Description='Individual Entitlement'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------SYSTEM SETTINGS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='System Settings'
	SET @DetailCode='PWDEXPIRYDAYS'	--Login Password Lifespan Days
	SET @Description='90'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ACCINACTIVEDAYS'	--The Maximum Days An A Login Can Remain Inactive Before Auto-Disabled
	SET @Description='60'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECAPTCHASECRET'	--Login Recaptcha Secret
	SET @Description='6LfFV0MUAAAAAChMtW4ShnmTxzklTuvCXgoka6Mc'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='RECAPTCHAKEY'	--Login Recaptcha Key
	SET @Description='6LfFV0MUAAAAAF9P_w1kn5NGSMW49I9jvPMs_hJO'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PWDRESETVALIDITYMINS'	--Login Password Reset Link Lifespan Minutes
	SET @Description='120'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1

	SET @DetailCode='BENEPSPRENEWALDATE'	--Beneficiary Payment Service Provider Renewal Date
	SET @Description=''
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1

	SET @DetailCode='CURFINYEAR'	--Current system financial year
	SET @Description=''
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1

	SET @DetailCode='WAITLISTVALIDITYMONTHS'	--Beneficiary waiting list validity period months
	SET @Description=''
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1

	SET @DetailCode='PSPACCDORMANCY'	--PSP Beneficiary payment account dormancy when payments NO longer needs to be submitted until investigation
	SET @Description='6'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1


	---------------HOUSEHOLD PROGRAMME STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='HHStatus'
	SET @DetailCode='REG'
	SET @Description='Registered'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGAPV'
	SET @Description='Registration Approved'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='REGREJ'
	SET @Description='Registration Rejected'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='VALPASS'
	SET @Description='Registration Validation Passed'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='VALFAIL'
	SET @Description='Registration Validation Failed'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENRL'
	SET @Description='Selected for enrolment into the programme'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENRLPSP'
	SET @Description='Account opened, biometrics captured but card NOT yet to be issued by PSP'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PSPCARDED'
	SET @Description='Account opened, biometrics captured and Card issued by PSP'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ONPAY'
	SET @Description='On Payroll'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='SUS'
	SET @Description='On Payroll Suspension'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='GRAD'
	SET @Description='Graduated'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	
	SET @DetailCode='EX'
	SET @Description='Exited'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------ACCOUNT STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Account Status'
	SET @DetailCode='1'
	SET @Description='Active bank account'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='-1'
	SET @Description='Dormant bank account'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------CARD STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Card Status'
	SET @DetailCode='1'
	SET @Description='Active'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='0'
	SET @Description='Deactivated'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='-1'
	SET @Description='Not yet carded'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='-2'
	SET @Description='Lost'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------MEMBER STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Member Status'
	SET @DetailCode='1'
	SET @Description='Active household member'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='0'
	SET @Description='Suspended household member'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='-1'
	SET @Description='Exited household member'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------ENROLMENT STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Enrolment Status'
	SET @DetailCode='PROGENROLAPV'
	SET @Description='Programme enrolment approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PROGSHAREPSP'
	SET @Description='Share with PSP'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PSPRECEIPT'
	SET @Description='Awaiting PSP receipt'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PSPENROL'
	SET @Description='Account opening by PSP'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENRLCLOSURE'
	SET @Description='Enrolment Closure'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




	---------------PAYMENT/PAYROLL STATUS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Status'
	SET @DetailCode='PAYMENTOPEN'
	SET @Description='Payment Cycle Opened'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENTCLOSED'
	SET @Description='Payment Cycle Closed'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




	---------------PAYMENT/PAYROLL STAGE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Stage'
	SET @DetailCode='PAYMENTCYCLEAPV'
	SET @Description='Payment Cycle Approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PREPAYROLLDRAFT'
	SET @Description='Pre-Payroll Audit Draft'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PREPAYROLLFINAL'
	SET @Description='Pre-Payroll Audit Final'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PREPAYROLLAPV'
	SET @Description='Pre-Payroll Audit Approval'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='FUNDSREQUEST'
	SET @Description='Funds Request'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYROLL'
	SET @Description='Payroll Generation'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYROLLVER'
	SET @Description='Payroll Verification'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYROLLAPV'
	SET @Description='Payroll Approval'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYROLLEX'
	SET @Description='Payroll Exchange'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYROLLEXCONF'
	SET @Description='Payroll Exchange Confirmation'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='POSTPAYROLL'
	SET @Description='Post-Payroll Audit'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='POSTPAYROLLAPV'
	SET @Description='Post-Payroll Audit Approval'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENTRECON'
	SET @Description='Payment Reconciliation'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENTRECONAPV'
	SET @Description='Payment Reconciliation Approval'
	SET @OrderNo=14
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CLOSED'
	SET @Description='Closed'
	SET @OrderNo=15
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------PAYMENT FREQUENCY-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Frequency'
	SET @DetailCode='BIMONTHLY'
	SET @Description='Bi-monthly'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	-------------SYSTEM RIGHTS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='System Right'
	SET @DetailCode='VIEW'
	SET @Description='Data View'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='ENTRY'
	SET @Description='Data Entry'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='MODIFIER'
	SET @Description='Data Modifier'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DELETION'
	SET @Description='Data Deletion'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='APPROVAL'
	SET @Description='Data Approval'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='EXPORT'
	SET @Description='Data Export'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DOWNLOAD'
	SET @Description='Data Download'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='UPLOAD'
	SET @Description='Data Upload'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	-------------PAYMENT/PAYROLL ADJUSTMENTS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Payment Adjustment'
	SET @DetailCode='ARREARS'
	SET @Description='Arrears/uncollected Amount'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PENALTY'
	SET @Description='Penalty Amount'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='COMPLIMENTARY'
	SET @Description='Complimentary Payment Amount'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='OFFSET'
	SET @Description='Payment Offset Amount'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-------------GEOUNITS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='GeoUnit'
	SET @DetailCode='PROVINCE'
	SET @Description='Province'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='COUNTY'
	SET @Description='County'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CONSTITUENCY'
	SET @Description='Constituency'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='WARD'
	SET @Description='Ward'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DISTRICT'
	SET @Description='District'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DIVISION'
	SET @Description='Division'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='LOCATION'
	SET @Description='Location'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUBLOCATION'
	SET @Description='Sub-Location'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------LOCALITY-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Locality'
	SET @DetailCode='0'
	SET @Description='Rural Area'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='1'
	SET @Description='Urban Area'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='2'
	SET @Description='Nairobi Area'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------SEX-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Sex'
	SET @DetailCode='M'
	SET @Description='Male'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='F'
	SET @Description='Female'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------MEMBER ROLE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Member Role'
	SET @DetailCode='MEMBER'
	SET @Description='Household member'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='CAREGIVER'
	SET @Description='Caregiver'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='BENEFICIARY'
	SET @Description='Beneficiary'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



	---------------RELATIONSHIP-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Relationship'

	SET @DetailCode='CAREGIVER'
	SET @Description='Caregiver'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='BENEFICIARY'
	SET @Description='Beneficiary'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;
	--INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.RelationshipId,T1.RelationshipName,ROW_NUMBER() OVER(ORDER BY RelationshipId),1,GETDATE()
	--FROM [DCS].dbo.Relationship T1
	--UNION
	--SELECT @Id,(SELECT COUNT(RelationshipId) FROM [DCS].dbo.Relationship )+1,'Beneficiary',(SELECT COUNT(RelationshipId) FROM [DCS].dbo.Relationship )+1,1,GETDATE()

	---------------LIVELIHOOD-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Livelihood'
	--SET @DetailCode='Farming'
	--SET @Description='Farming'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Employment'
	--SET @Description='Employment'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Livelihood'
	--SET @DetailCode='Casual'
	--SET @Description='Casual'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------GRIEVANCE LEVEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Level'
	--SET @DetailCode='DISTRICT'
	--SET @Description='District'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='COUNTY'
	--SET @Description='County'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='NATIONAL'
	--SET @Description='National'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------GRIEVANCE TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.GrievanceTypeId,T1.GrievanceTypeName,ROW_NUMBER() OVER(ORDER BY GrievanceTypeId),1,GETDATE()
	--FROM [DCS].dbo.GrievanceType T1

	---------------REPORTING CHANNEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Reporting Channel'
	--SET @DetailCode='TELEPHONE'
	--SET @Description='via Telephone'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='TEXT'
	--SET @Description='via Short Text Message'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='LETTER'
	--SET @Description='via Letter'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='EMAIL'
	--SET @Description='via Email'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='IN-PERSON'
	--SET @Description='in Person'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------COMPLAINANT TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Complainant Type'
	--SET @DetailCode='INDIVIDUAL'
	--SET @Description='Individual / Household complainant'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='COMMUNITY'
	--SET @Description='Community Complainant'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='SENATE'
	--SET @Description='Senate Complainant'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='PARLIAMENT'
	--SET @Description='Parliament Complainant'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------GRIEVANCE RESOLUTION-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Grievance Resolution'
	--SET @DetailCode='REFERRAL'
	--SET @Description='Refferal'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='ACTION'
	--SET @Description='Other Action'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='APPEAL'
	--SET @Description='Appeal'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='RESOLVE'
	--SET @Description='Resolve'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------AGE BRACKET-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Age Bracket'
	--SET @DetailCode='0-5'
	--SET @Description='0 - 5 years'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='6-18'
	--SET @Description='6 - 18 years'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='19-24'
	--SET @Description='19 - 24 years'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='25-34'
	--SET @Description='25 - 34 years'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='35-44'
	--SET @Description='35 - 44 years'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='45-54'
	--SET @Description='45 - 54 years'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='55-64'
	--SET @Description='55 - 64 years'
	--SET @OrderNo=7
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='65-74'
	--SET @Description='65 - 74 years'
	--SET @OrderNo=8
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='75-84'
	--SET @Description='75 - 84 years'
	--SET @OrderNo=9
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='>84'
	--SET @Description='Above 84 years'
	--SET @OrderNo=10
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------AILMENT TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Ailment'
	--SET @DetailCode='Tuberculosis'
	--SET @Description='TB'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------DISABILITY-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Disability'
	--SET @DetailCode='Mental Disable'
	--SET @Description='Mental Disability'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Physical Disable'
	--SET @Description='Physical Disability'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------EDUCATION LEVEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Education Level'
	--SET @DetailCode='Form 4'
	--SET @Description='Form 4'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------ILLITERACY REASON-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Illiteracy Reason'
	--SET @DetailCode='Lack of schools'
	--SET @Description='Lack of schools'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------ORPHANHOOD-------------

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan None'
	--SET @Description='None'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan Father'
	--SET @Description='Father'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SELECT @Id=Id FROM SystemCode WHERE Code='Orphanhood'
	--SET @DetailCode='Orphan Mother'
	--SET @Description='Mother'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Orphan Both'
	--SET @Description='Both'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------MARITAL STATUS-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Marital Status'
	--SET @DetailCode='Never Married'
	--SET @Description='Never Married'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Married'
	--SET @Description='Married'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Divorced'
	--SET @Description='Divorced'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------HOUSEHOLD ITEM-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Household Item'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=10

	-----------------WALL TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Wall Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------FLOOR TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Floor Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------ROOF TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Roof Material'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=0

	-----------------TOILET TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Toilet Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=1

	-----------------WATER SOURCE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Water Source'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=2

	-----------------LIGHTING SOURCE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Lighting Source'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=3

	-----------------COOKING FUEL-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Cooking Fuel'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=4

	---------------OTHER SA PROGRAMME-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Other SP Programme'
	SET @DetailCode='CT-OVC'
	SET @Description='CT-OVC'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='OPCT'
	SET @Description='OPCT'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PwSD-CT'
	SET @Description='PwSD-CT'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='HSNP'
	SET @Description='HSNP'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='WFP'
	SET @Description='WFP'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Other'
	SET @Description='Other'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;




	---------------SP BENEFIT TYPE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='SP Benefit Type'
	SET @DetailCode='INKIND'
	SET @Description='In Kind Benefit'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='MONETARY'
	SET @Description='Monetary Benefit'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	


	-----------------FREQUENCY-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Frequency'
	--SET @DetailCode='Daily'
	--SET @Description='Daily'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Weekly'
	--SET @Description='Weekly'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Fortnightly'
	--SET @Description='Fortnightly'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Monthly'
	--SET @Description='Monthly'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Bi-Monthly'
	--SET @Description='Bi-Monthly'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Quarterly'
	--SET @Description='Quarterly'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Half-Yearly'
	--SET @Description='Half-Yearly'
	--SET @OrderNo=7
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Annually'
	--SET @Description='Annually'
	--SET @OrderNo=8
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------INCOME-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Income'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=8

	-----------------EXPENDITURE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Expenditure'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=9

	-----------------FOOD TYPE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Food Type'
	--INSERT INTO SystemCodeDetail(Id,Code,[Description],OrderNo,CreatedBy,CreatedOn)
	--SELECT @Id,T1.Grade_Code,T1.Grade_Name,ROW_NUMBER() OVER(ORDER BY CONVERT(int,Grade_Code)),1,GETDATE()
	--FROM [DSD].dbo.Grades T1
	--WHERE T1.Parameter_Code=11

	-----------------SKIPPED MEALS RANGE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Skipped Meals Range'
	--SET @DetailCode='1 - 2'
	--SET @Description='1 - 2'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='3 - 4'
	--SET @Description='3 - 4'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='5 - 6'
	--SET @Description='5 - 6'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='7 And Above'
	--SET @Description='7 And Above'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------SAVINGS STORAGE-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Savings Storage'
	--SET @DetailCode='Bank'
	--SET @Description='Cash in the bank'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------CALENDAR MONTHS-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Calendar Months'
	SET @DetailCode='1'
	SET @Description='January'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='2'
	SET @Description='February'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='3'
	SET @Description='March'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='4'
	SET @Description='April'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='5'
	SET @Description='May'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='6'
	SET @Description='June'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='7'
	SET @Description='July'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='8'
	SET @Description='August'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='9'
	SET @Description='September'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='10'
	SET @Description='October'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='11'
	SET @Description='November'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='12'
	SET @Description='December'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------CHANGE TYPES-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Change Type'
	--SET @DetailCode='Dissolution'
	--SET @Description='Dissolution'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Caregiver'
	--SET @Description='Caregiver'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Exlusion'
	--SET @Description='Exlusion'
	--SET @OrderNo=3
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Correction'
	--SET @Description='Correction'
	--SET @OrderNo=4
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Graduation'
	--SET @Description='Graduation'
	--SET @OrderNo=5
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Reversal'
	--SET @Description='Reversal'
	--SET @OrderNo=6
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	-----------------CHANGE JUSTIFICATION-------------
	--SELECT @Id=Id FROM SystemCode WHERE Code='Change Justification'
	--SET @DetailCode='Death'
	--SET @Description='Deceased'
	--SET @OrderNo=1
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	--SET @DetailCode='Fraud'
	--SET @Description='Fraud'
	--SET @OrderNo=2
	--EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	---------------FILE TYPE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='File Type'
	SET @DetailCode='ENROLMENT'
	SET @Description='Beneficiary enrolment File'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='PAYMENT'
	SET @Description='Beneficiary payment file'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUPPORT'
	SET @Description='Supporting Document file'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='EXCEPTION'
	SET @Description='Exception Document file'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------FILE CREATION TYPE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='File Creation Type'
	SET @DetailCode='SYSGEN'
	SET @Description='System generated file'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='UPLOADED'
	SET @Description='User uploaded file'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	---------------EXCEPTION TYPE-------------
	SELECT @Id=Id FROM SystemCode WHERE Code='Exception Type'
	SET @DetailCode='INVALIDBENEID'
	SET @Description='Invalid National ID Number'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INVALIDCGID'
	SET @Description='Duplicate Caregiver National ID Number'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DUPLICATEBENEIDIN'
	SET @Description='In Programme Duplicate Beneficiary National ID Number'
	SET @OrderNo=3
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DUPLICATEBENEIDACC'
	SET @Description='Across Programmes Duplicate Beneficiary National ID Number'
	SET @OrderNo=4
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DUPLICATECGIDIN'
	SET @Description='In Programme Duplicate Caregiver National ID Number'
	SET @OrderNo=5
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='DUPLICATECGIDACC'
	SET @Description='Across Programmes Duplicate Caregiver National ID Number'
	SET @OrderNo=6
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INVALIDACC'
	SET @Description='Invalid Payment Account'
	SET @OrderNo=7
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INVALIDCARD'
	SET @Description='Invalid Payment Card'
	SET @OrderNo=8
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUSPICIOUSAMT'
	SET @Description='Suspicious Payment Amount'
	SET @OrderNo=9
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='SUSPICIOUSDORMANCY'
	SET @Description='Suspicious Dormant Account'
	SET @OrderNo=10
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INELIGIBLEBENE'
	SET @Description='Inelgible Beneficiary'
	SET @OrderNo=11
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INELIGIBLECG'
	SET @Description='Ineligible Caregiver'
	SET @OrderNo=12
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='INELIGIBLESUS'
	SET @Description='Household on Suspension'
	SET @OrderNo=13
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SELECT * FROM SystemCode ORDER BY Id
	SELECT * FROM SystemCodeDetail ORDER BY Id
	SELECT * FROM UserGroupProfile ORDER BY Id
END


DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @LoopVar int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

     SET @SysCode='Payroll Stage'
		SET @SysDetailCode='PAYROLLEXCONF'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		
		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId1
		FROM PaymentCycleDetail T1
		 















		 
INSERT INTO  Payment (
   PaymentCycleId
  ,ProgrammeId
  ,HhId
  ,WasTrxSuccessful
  ,TrxAmount
  ,TrxNo
  ,TrxDate
  ,TrxNarration
  ,CreatedBy
  ,CreatedOn
)

SELECT   T1.PaymentCycleId, T1.ProgrammeId,T1.HhId,0, 0,CONCAT(CONVERT(VARCHAR,T1.HhId),CONVERT(VARCHAR,T1.ProgrammeId),CONVERT(VARCHAR,T1.PaymentCycleId)), GETDATE(), 'Failed', T6.CreatedBy, GETDATE() as CreatedOn
FROM dbo.Payroll T1
INNER JOIN dbo.Household T2 ON T1.HhId = T2.Id
INNER JOIN dbo.HouseholdEnrolment T3 ON T3.HhId = T2.Id
INNER JOIN dbo.BeneficiaryAccount T4 
INNER JOIN PSPBranch T5 ON T4.PSPBranchId = T5.Id
INNER JOIN PSP T6 ON T6.Id = T5.PSPId
ON T4.HhEnrolmentId = T3.ID --ORDER BY NEWID(), T4.Id
LEFT JOIN DBO.Payment T7 ON T1.PaymentCycleId = T7.PaymentCycleId AND T7.PaymentCycleId = T1.PaymentCycleId AND T7.ProgrammeId = T1.ProgrammeId AND T7.HhId = T1.HhId
WHERE T7.PaymentCycleId IS NULL




