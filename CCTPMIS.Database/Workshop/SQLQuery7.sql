
IF NOT OBJECT_ID('GetRegistrationBatches') IS NULL	
 DROP PROC GetRegistrationBatches
 GO
CREATE PROC GetRegistrationBatches
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	SELECT DISTINCT T1.*, TG.County, TG.Constituency, TG.ConstituencyId, T2.FirstName AS AcceptBy,
		T3.FirstName AS AcceptApvBy, T4.Name AS TargetPlan
	FROM  HouseholdRegAccept T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.ConstituencyId  = T1.ConstituencyId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.TargetPlanId = @TargetPlanId
		LEFT JOIN [USER] T2 ON T1.AcceptById = T2.Id
		LEFT JOIN [USER] T3 ON T1.AcceptApvById = T3.Id
		LEFT JOIN TargetPlan T4 ON T1.TargetPlanId = T4.Id

END
GO