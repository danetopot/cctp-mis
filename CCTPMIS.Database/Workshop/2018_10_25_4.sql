USE  UAT_CCTPMIS_COPY
GO


INSERT INTO BeneAccountMonthlyActivityDetail (
   BeneAccountMonthlyActivityId
  ,BeneAccountId
  ,HadUniqueWdl
  ,UniqueWdlTrxNo
  ,UniqueWdlDate
  ,HadBeneBiosVerified
  ,IsDormant
  ,DormancyDate
  ,IsDueForClawback
  ,ClawbackAmount
  ,CreatedBy
  ,CreatedOn
  ,ModifiedBy
  ,ModifiedOn
) SELECT 
   1 AS  BeneAccountMonthlyActivityId
  ,BeneAccountId
  ,HadUniqueWdl
  ,UniqueWdlTrxNo
  ,UniqueWdlDate
  ,HadBeneBiosVerified
  ,IsDormant
  ,DormancyDate
  ,IsDueForClawback
  ,ClawbackAmount
  ,CreatedBy
  ,CreatedOn
  ,ModifiedBy
  ,ModifiedOn
FROM