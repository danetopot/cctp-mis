
IF NOT OBJECT_ID('GetHHListingClosureSummary') IS NULL	
DROP PROC GetHHListingClosureSummary
GO
CREATE PROC GetHHListingClosureSummary
	@TargetPlanId int
    AS
    BEGIN
SELECT T4.Id, T4.Name 'ProgrammeName', COUNT(T1.Id) 'BeneficiaryHousehold'   FROM ComValListingPlanHH   T1
INNER JOIN TargetPlan T2 ON T2.Id = T1.TargetPlanId
INNER JOIN  ListingPlanProgramme T3 ON T1.ProgrammeId = T3.ProgrammeId and T2.Id = T3.TargetPlanId
INNER JOIN Programme T4 ON T4.Id = T1.ProgrammeId
 INNER JOIN  SystemCodeDetail T7 ON T1.StatusId = T7.Id  AND T7.Code IN ('REGCONFIRM','REGCORRECT')
 INNER JOIN dbo.ComValListingException T8  ON T8.Id = T1.Id  
WHERE 
	(T8.BeneNationalIdNo IS NOT NULL AND (T8.Bene_SexMatch = 1 AND T8.Bene_DoBYearMatch = 1 AND ( T8.Bene_FirstNameExists =1 OR T8.Bene_MiddleNameExists=1 OR T8.Bene_SurnameExists =1)) OR  T8.BeneNationalIdNo IS NULL)
	AND (T8.CG_SexMatch = 1 AND T8.CG_DoBYearMatch = 1 AND( T8.CG_FirstNameExists =1 OR T8.CG_MiddleNameExists=1 OR  T8.CG_SurnameExists =1)) AND T8.TargetPlanId=@TargetPlanId
  
  GROUP BY T4.Id,T4.Name

  END
 
