
/****** Object:  StoredProcedure [dbo].[AddEditReconciliation]    Script Date: 25/10/2018 1:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[AddEditReconciliation]
	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
   ,@ApplicablePaymentCyclesXML XML
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentCycle TABLE(
		Id int
	   ,PaymentCylceId int
	)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO @tblPaymentCycle(Id,PaymentCylceId)
	SELECT T1.Id,T1.PaymentCycleId
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(PaymentCycleId)[1]','int') AS PaymentCycleId
		FROM @ApplicablePaymentCyclesXML.nodes('PaymentCycles/Record') AS U(R)
	) T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id

	SET @Id=ISNULL(@Id,0)

	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentCycle) AND EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE FundsRequestOn>=@StartDate)
		SET @ErrorMsg='Please specify valid ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PostPayrollApvOn>=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail
					  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 RIGHT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T1.PaymentCycleId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) in ApplicablePaymentCyclesXML parameter are invalid for the period specified'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail 
					  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 LEFT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T2.PaymentCylceId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(
					SELECT 1
					FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
										 INNER JOIN PaymentAccountActivityMonth T3 ON T1.Id=T3.PaymentCycleId
										 LEFT JOIN BeneAccountMonthlyActivity T4 ON T3.MonthId=T4.MonthId AND T3.[Year]=T4.[Year] 
					WHERE ISNULL(T4.StatusId,0)<>@SystemCodeDetailId3
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.StartDate=@StartDate
		   ,T1.EndDate=@EndDate
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Reconciliation T1
		WHERE T1.Id=@Id

		UPDATE T1
		SET T1.ReconciliationId=NULL
		FROM PaymentCycle T1
		WHERE T1.ReconciliationId=@Id

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END
	ELSE
	BEGIN
		INSERT INTO Reconciliation(StartDate,EndDate,StatusId,CreatedBy,CreatedOn)
		SELECT @StartDate,@EndDate,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @Id=Id FROM Reconciliation WHERE StartDate=@StartDate AND EndDate=@EndDate

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId
	END
END

GO