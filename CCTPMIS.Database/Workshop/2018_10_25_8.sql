USE [UAT_CCTPMIS_COPY]
GO
/****** Object:  Table [dbo].[BeneAccountMonthlyActivityDetail]    Script Date: 10/25/2018 4:52:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BeneAccountMonthlyActivityDetail](
	[BeneAccountMonthlyActivityId] [int] NOT NULL,
	[BeneAccountId] [int] NOT NULL,
	[HadUniqueWdl] [bit] NOT NULL,
	[UniqueWdlTrxNo] [nvarchar](50) NULL,
	[UniqueWdlDate] [datetime] NULL,
	[HadBeneBiosVerified] [bit] NOT NULL,
	[IsDormant] [bit] NOT NULL,
	[DormancyDate] [datetime] NULL,
	[IsDueForClawback] [bit] NOT NULL,
	[ClawbackAmount] [money] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_BeneAccountMonthlyActivityDetail] PRIMARY KEY CLUSTERED 
(
	[BeneAccountMonthlyActivityId] ASC,
	[BeneAccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail]  WITH CHECK ADD  CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneAccountMonthlyActivity_MonthlyActivityId] FOREIGN KEY([BeneAccountMonthlyActivityId])
REFERENCES [dbo].[BeneAccountMonthlyActivity] ([Id])
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] CHECK CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneAccountMonthlyActivity_MonthlyActivityId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail]  WITH CHECK ADD  CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneficiaryAccount_BeneAccountId] FOREIGN KEY([BeneAccountId])
REFERENCES [dbo].[BeneficiaryAccount] ([Id])
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] CHECK CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_BeneficiaryAccount_BeneAccountId]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail]  WITH CHECK ADD  CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] CHECK CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_CreatedBy]
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail]  WITH CHECK ADD  CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[BeneAccountMonthlyActivityDetail] CHECK CONSTRAINT [FK_BeneAccountMonthlyActivityDetail_User_ModifiedBy]
GO
