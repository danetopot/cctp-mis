
IF NOT OBJECT_ID('AddEditRegistrationHH') IS NULL	
DROP PROC AddEditRegistrationHH
GO

CREATE PROC AddEditRegistrationHH
	@HouseHoldInfoXml xml,
	@DeviceInfoXml xml
AS
BEGIN
	DECLARE @ErrorMsg varchar(256)
	DECLARE @SysCode varchar(20)
	DECLARE @EditingCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @EditingId int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @TargetPlanId int
	DECLARE @InterviewStatusId int
	DECLARE @InterviewResultId int
	DECLARE @CategoryId int
	DECLARE @Id int
	DECLARE @EnumeratorDeviceId  int = NULL
	DECLARE @DeviceId  varchar(36)

	DECLARE @AppVersion  varchar(10)
	DECLARE @AppBuild  varchar(10)

	DECLARE @StatusId INT
 

	SET @SysCode='REG_STATUS'
	SET @SysDetailCode='ACTIVE'
	SELECT @StatusId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='REG_CATEGORIES'
	SET @SysDetailCode='REGISTRATION'
	SELECT @CategoryId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	SELECT @TargetPlanId =  Id FROM dbo.TargetPlan WHERE CategoryId=@CategoryId AND StatusId =@StatusId

	IF  (ISNULL(@TargetPlanId,0)=0)
	SET @ErrorMsg ='No current Active Plan accepting data'

	SELECT @DeviceId = U.R.value('(DeviceId)[1]','VARCHAR(36)')
	FROM @DeviceInfoXml.nodes('Registrations/TabEnvironment') AS U(R)
	SELECT @AppVersion = U.R.value('(AppVersion)[1]','VARCHAR(36)')
	FROM @DeviceInfoXml.nodes('Registrations/TabEnvironment') AS U(R)
	SELECT @AppBuild = U.R.value('(AppBuild)[1]','VARCHAR(36)')
	FROM @DeviceInfoXml.nodes('Registrations/TabEnvironment') AS U(R)
	SELECT @Id = U.R.value('(Id)[1]','INT') FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm') AS U(R)

	IF  EXISTS(SELECT 1 FROM HouseholdReg WHERE Id = @Id AND HouseholdRegAcceptId IS  NOT NULL ) 
	SET @ErrorMsg ='This Household has been submitted and Accepted'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @EnumeratorDeviceId =  Id    from EnumeratorDevice where DeviceId = @DeviceId
	
	BEGIN
		IF ISNULL(@EnumeratorDeviceId,0)=0
	  INSERT INTO EnumeratorDevice ( DeviceId, DeviceModel, DeviceManufacturer, DeviceName,[Version],VersionNumber, [Platform],Idiom,IsDevice, EnumeratorId)
		SELECT T1.DeviceId, DeviceModel, DeviceManufacturer, DeviceName, [Version], VersionNumber, [Platform], Idiom, IsDevice, EnumeratorId
		FROM ( SELECT U.R.value('(DeviceId)[1]','VARCHAR(36)') AS DeviceId
				, U.R.value('(DeviceModel)[1]','VARCHAR(15)') AS DeviceModel
				, U.R.value('(DeviceManufacturer)[1]','VARCHAR(15)') AS DeviceManufacturer
				, U.R.value('(DeviceName)[1]','VARCHAR(15)') AS DeviceName
				, U.R.value('(Version)[1]','VARCHAR(15)')  AS [Version]
				, U.R.value('(VersionNumber)[1]','VARCHAR(15) ') AS VersionNumber
				, U.R.value('(Platform)[1]','VARCHAR(15)') AS [Platform]
				, U.R.value('(Idiom)[1]','VARCHAR(10)') AS Idiom
				, U.R.value('(IsDevice)[1]','BIT') AS IsDevice
				, U.R.value('(SyncEnumeratorId)[1]','INT ') AS EnumeratorId
			FROM @DeviceInfoXml.nodes('Registrations/TabEnvironment') AS U(R)
			 ) T1

		SELECT @EnumeratorDeviceId=IDENT_CURRENT('EnumeratorDevice')
		
	END
		
	DELETE FROM HouseholdRegMemberDisability WHERE HouseholdRegId = @Id
	DELETE FROM HouseholdRegMember WHERE HouseholdRegId = @Id
	DELETE FROM HouseholdRegProgramme WHERE HouseholdRegId = @Id
	DELETE FROM HouseholdRegCharacteristic WHERE Id = @Id
	DELETE FROM HouseholdReg WHERE Id = @Id

	 INSERT INTO HouseholdReg(Id,SubLocationId,UniqueId,LocationId,ProgrammeId,Village,StatusId,Years,Months,NearestReligiousBuilding,NearestSchool,PhysicalAddress,StartTime,EndTime,RegistrationDate,SyncUpDate,DownloadDate,RegDate1,RegDate2,RegDate3,Longitude,Latitude,TargetPlanId,EnumeratorId,SyncEnumeratorId,InterviewStatusId,InterviewResultId,EnumeratorDeviceId,SyncDate,AppVersion,AppBuild)
	SELECT Id, SubLocationId, UniqueId, LocationId, ProgrammeId, Village, StatusId, Years, Months, NearestReligiousBuilding, NearestSchool, PhysicalAddress, StartTime, EndTime, RegistrationDate,
		GETDATE(), DownloadDate, RegDate1, RegDate2, RegDate3, Longitude, Latitude, @TargetPlanId, EnumeratorId, SyncEnumeratorId, InterviewStatusId, InterviewResultId,
		 @EnumeratorDeviceId, GETDATE(), AppVersion, AppBuild
	FROM
		( SELECT
			U.R.value('(Id)[1]','INT') AS Id
		  , U.R.value('(SubLocationId)[1]','INT') AS SubLocationId
		  , U.R.value('(UniqueId)[1]','VARCHAR(36)') AS UniqueId
		  , U.R.value('(LocationId)[1]','INT') AS LocationId
		  , U.R.value('(ProgrammeId)[1]','TINYINT') AS ProgrammeId 
		  , U.R.value('(Village)[1]','VARCHAR(25)') AS Village
		  , U.R.value('(StatusId)[1]','INT') AS StatusId
		  , U.R.value('(Years)[1]','INT') AS Years
		  , U.R.value('(Months)[1]','INT') AS Months
		  , U.R.value('(NearestReligiousBuilding)[1]','VARCHAR(50)') AS NearestReligiousBuilding
		  , U.R.value('(NearestSchool)[1]','VARCHAR(50)') AS NearestSchool
		  , U.R.value('(PhysicalAddress)[1]','VARCHAR(50)') AS PhysicalAddress	 
		  , U.R.value('(StartTime)[1]','DATETIME ') AS StartTime
		  , U.R.value('(EndTime)[1]','DATETIME') AS EndTime
		  , U.R.value('(SyncUpDate)[1]','DATETIME') AS SyncUpDate
		  , U.R.value('(RegistrationDate)[1]','DATETIME') AS RegistrationDate
		  , U.R.value('(DownloadDate)[1]','DATETIME') AS DownloadDate
		  , U.R.value('(RegDate1)[1]','DATETIME') AS RegDate1
		  , U.R.value('(RegDate2)[1]','DATETIME') AS RegDate2
		  , U.R.value('(RegDate3)[1]','DATETIME') AS RegDate3
		  , U.R.value('(Longitude)[1]','FLOAT') AS Longitude
		  , U.R.value('(Latitude)[1]','FLOAT') AS Latitude
		  , U.R.value('(TargetPlanId)[1]','INT ') AS TargetPlanId
		  , U.R.value('(EnumeratorId)[1]','INT ') AS EnumeratorId
		  , U.R.value('(SyncEnumeratorId)[1]','INT ') AS SyncEnumeratorId	
		  , U.R.value('(HouseholdMembers)[1]','INT ') AS HouseholdMembers
		  , U.R.value('(InterviewStatusId)[1]','INT ') AS InterviewStatusId
		  , U.R.value('(InterviewResultId)[1]','INT ') AS InterviewResultId	
		  , GETDATE() AS 	CreatedOn
		  , @AppVersion AS AppVersion
		  , @AppBuild AS AppBuild
		
		FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm') AS U(R)
) T1


SET @SysCode='Interview Status'
SET @SysDetailCode='02'
SELECT @InterviewStatusId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

SET @SysCode='Interview Result'
SET @SysDetailCode='01'
SELECT @InterviewResultId = T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


IF  EXISTS(SELECT 1 FROM HouseholdReg WHERE Id = @Id AND HouseholdRegAcceptId IS NULL  AND InterviewResultId = @InterviewResultId AND InterviewStatusId = @InterviewStatusId 
) 

BEGIN


 INSERT INTO  HouseholdRegCharacteristic (Id,HouseholdMembers,HabitableRooms,IsOwnedId,TenureStatusId,RoofMaterialId,WallMaterialId,FloorMaterialId,DwellingUnitRiskId,WaterSourceId,ToiletTypeId,CookingFuelSourceId,LightingSourceId,IsTelevisionId,IsMotorcycleId,IsTukTukId,IsRefrigeratorId,IsCarId,IsMobilePhoneId,IsBicycleId,ExoticCattle,IndigenousCattle,Sheep,Goats,Camels,Donkeys,Pigs,Chicken,Deaths,LiveBirths,HouseholdConditionId,IsSkippedMealId,NsnpProgrammesId,OtherProgrammesId,OtherProgrammeNames,BenefitTypeId,LastReceiptAmount,InKindBenefit)

SELECT Id,HouseholdMembers,HabitableRooms,IsOwnedId,TenureStatusId,RoofMaterialId,WallMaterialId,
FloorMaterialId,DwellingUnitRiskId,WaterSourceId,ToiletTypeId,CookingFuelSourceId,
LightingSourceId,IsTelevisionId,IsMotorcycleId,IsTukTukId,IsRefrigeratorId,IsCarId,IsMobilePhoneId,IsBicycleId,ExoticCattle,IndigenousCattle,Sheep,Goats,Camels,Donkeys,Pigs,Chicken,Deaths,LiveBirths,HouseHoldConditionId,IsSkippedMealId,NsnpProgrammesId,OtherProgrammesId,OtherProgrammeNames,BenefitTypeId,LastReceiptAmount,InKindBenefit
FROM ( 
	SELECT
	U.R.value('(Id)[1]','INT') AS Id
	,U.R.value('(HouseholdMembers)[1]','INT') AS HouseholdMembers
	,U.R.value('(HabitableRooms)[1]','INT') AS HabitableRooms
	,U.R.value('(IsOwnedId)[1]','INT') AS IsOwnedId
	,U.R.value('(TenureStatusId)[1]','INT') AS TenureStatusId
	,U.R.value('(RoofConstructionMaterialId)[1]','INT') AS RoofMaterialId
	,U.R.value('(WallConstructionMaterialId)[1]','INT') AS WallMaterialId
	,U.R.value('(FloorConstructionMaterialId)[1]','INT') AS FloorMaterialId
	,U.R.value('(DwellingUnitRiskId)[1]','INT') AS DwellingUnitRiskId
	,U.R.value('(WaterSourceId)[1]','INT') AS WaterSourceId
	,U.R.value('(WasteDisposalModeId)[1]','INT') AS ToiletTypeId
	,U.R.value('(CookingFuelTypeId)[1]','INT') AS CookingFuelSourceId
	,U.R.value('(LightingFuelTypeId)[1]','INT') AS LightingSourceId
	,U.R.value('(IsTelevisionId)[1]','INT') AS IsTelevisionId
	,U.R.value('(IsMotorcycleId)[1]','INT') AS IsMotorcycleId
	,U.R.value('(IsTukTukId)[1]','INT') AS IsTukTukId
	,U.R.value('(IsRefrigeratorId)[1]','INT') AS IsRefrigeratorId
	,U.R.value('(IsCarId)[1]','INT') AS IsCarId
	,U.R.value('(IsMobilePhoneId)[1]','INT') AS IsMobilePhoneId
	,U.R.value('(IsBicycleId)[1]','INT') AS IsBicycleId
	,U.R.value('(ExoticCattle)[1]','INT') AS ExoticCattle
	,U.R.value('(IndigenousCattle)[1]','INT') AS IndigenousCattle
	,U.R.value('(Sheep)[1]','INT') AS Sheep
	,U.R.value('(Goats)[1]','INT') AS Goats
	,U.R.value('(Camels)[1]','INT') AS Camels
	,U.R.value('(Donkeys)[1]','INT') AS Donkeys
	,U.R.value('(Pigs)[1]','INT') AS  Pigs
	,U.R.value('(Chicken)[1]','INT') AS  Chicken
	,U.R.value('(Deaths)[1]','INT') AS Deaths
	,U.R.value('(LiveBirths)[1]','INT') AS LiveBirths
	,U.R.value('(HouseHoldConditionId)[1]','INT') AS HouseHoldConditionId
	,U.R.value('(IsSkippedMealId)[1]','INT') AS IsSkippedMealId
	,U.R.value('(NsnpProgrammesId)[1]','INT') AS NsnpProgrammesId
	,U.R.value('(OtherProgrammesId)[1]','INT') AS OtherProgrammesId
	,U.R.value('(OtherProgrammeNames)[1]','VARCHAR(50)') AS OtherProgrammeNames
	, CASE WHEN (U.R.value('(BenefitTypeId)[1]','INT')=0) THEN NULL else U.R.value('(BenefitTypeId)[1]','INT') end  AS  BenefitTypeId
	,U.R.value('(LastReceiptAmount)[1]','decimal') AS  LastReceiptAmount
	,U.R.value('(InKindBenefit)[1]','INT') AS  InKindBenefit
FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm') AS U(R)
) T2

 INSERT INTO  HouseholdRegProgramme ( HouseholdRegId ,ProgrammeId ) 
SELECT   @Id ,ProgrammeId
FROM
(  SELECT  
		  U.R.value('(ProgrammeId)[1]','INT ') AS ProgrammeId  
		   FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm/RegistrationProgrammes/RegistrationProgramme') AS U(R)
) T1
 INSERT INTO HouseholdRegMember (HouseholdRegId,MemberId,CareGiverId,SpouseId,FirstName,MiddleName,Surname,IdentificationTypeId,IdentificationNumber,PhoneNumber,RelationshipId,SpouseInHouseholdId,SexId,DateOfBirth,MaritalStatusId,ChronicIllnessStatusId,DisabilityCareStatusId,DisabilityTypeId,EducationLevelId,FatherAliveStatusId,FormalJobNgoId,LearningStatusId,WorkTypeId,MotherAliveStatusId) 
SELECT    @Id,MemberId,CareGiverId,SpouseId,FirstName,MiddleName,Surname,IdentificationTypeId,IdentificationNumber,PhoneNumber,RelationshipId,SpouseInHouseholdId,SexId,DateOfBirth,MaritalStatusId,ChronicIllnessStatusId,DisabilityCareStatusId,DisabilityTypeId,EducationLevelId,FatherAliveStatusId,FormalJobNgoId,LearningStatusId,WorkTypeId,MotherAliveStatusId
FROM
(     SELECT
	  U.R.value('(MemberId)[1]','VARCHAR(50) ') AS MemberId
   , U.R.value('(CareGiverId)[1]','VARCHAR(50) ') AS CareGiverId
   , U.R.value('(SpouseId)[1]','VARCHAR(50) ') AS SpouseId
   , U.R.value('(FirstName)[1]','VARCHAR(50) ') AS FirstName
   , U.R.value('(MiddleName)[1]','VARCHAR(50) ') AS MiddleName
   , U.R.value('(Surname)[1]','VARCHAR(50) ') AS Surname
   , U.R.value('(IdentificationDocumentTypeId)[1]','INT ') AS IdentificationTypeId
   , U.R.value('(IdentificationNumber)[1]','VARCHAR(50) ') AS IdentificationNumber
   , U.R.value('(PhoneNumber)[1]','VARCHAR(50) ') AS PhoneNumber
   , U.R.value('(RelationshipId)[1]','INT ') AS RelationshipId
   , U.R.value('(SpouseInHouseholdId)[1]','INT ') AS SpouseInHouseholdId
   , U.R.value('(SexId)[1]','INT ') AS SexId
   , U.R.value('(DateOfBirth)[1]','datetime ') AS DateOfBirth
   , U.R.value('(MaritalStatusId)[1]','INT ') AS MaritalStatusId
   , U.R.value('(ChronicIllnessStatusId)[1]','INT ') AS ChronicIllnessStatusId
   , U.R.value('(DisabilityCareStatusId)[1]','INT ') AS DisabilityCareStatusId
   , U.R.value('(DisabilityTypeId)[1]','INT ') AS DisabilityTypeId 
   , U.R.value('(EducationLevelId)[1]','INT ') AS EducationLevelId
   , U.R.value('(FatherAliveStatusId)[1]','INT ') AS FatherAliveStatusId
   , U.R.value('(FormalJobNgoId)[1]','INT ') AS FormalJobNgoId
   , U.R.value('(LearningStatusId)[1]','INT ') AS LearningStatusId
   , U.R.value('(WorkTypeId)[1]','INT ') AS WorkTypeId
   , U.R.value('(MotherAliveStatusId)[1]','INT ') AS MotherAliveStatusId
	FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm/RegistrationMembers/RegistrationMember') AS U(R)
) T1

 INSERT INTO  HouseholdRegMemberDisability ( HouseholdRegMemberId ,DisabilityId,HouseholdRegId) 
SELECT   HouseholdRegMemberId ,DisabilityId, HouseholdRegId
FROM
(  SELECT    
			U.R.value('(RegistrationId)[1]','INT ') AS HouseholdRegId  
		   ,U.R.value('(DisabilityId)[1]','INT ') AS DisabilityId  
		  , U.R.value('(RegistrationMemberId)[1]','VARCHAR(50) ') AS HouseholdRegMemberId  
		   FROM @HouseHoldInfoXml.nodes('Registrations/RegistrationHHVm/RegistrationMemberDisabilities/RegistrationMemberDisability') AS U(R)
) T1

 
 EXEC PMT_Generate @Id

END


SELECT 0 AS StatusId, 'Success' AS [Description]
END
GO



 

  EXEC PMT_Generate 21