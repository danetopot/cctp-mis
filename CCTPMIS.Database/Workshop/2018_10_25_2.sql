IF NOT OBJECT_ID('PSPFinalizePayrollTrx') IS NULL	DROP PROC PSPFinalizePayrollTrx
GO
CREATE PROC PSPFinalizePayrollTrx
	@PaymentCycleId int
   ,@BankCode nvarchar(20)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @PayrollHhs int
	DECLARE @PaymentHhs int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEXCONF'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PayrollHhs=COUNT(T1.HhId)
	FROM Payroll T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN HouseholdEnrolment T3 ON T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T2.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN PaymentCycleDetail T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.Code=@BankCode AND T7.PaymentStageId=@SystemCodeDetailId1

	SELECT @PaymentHhs=COUNT(T1.HhId)
	FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN HouseholdEnrolment T3 ON T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T2.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN PaymentCycleDetail T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.Code=@BankCode AND T7.PaymentStageId=@SystemCodeDetailId1
		 	   
	IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not in the payment stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN
	
	IF @PayrollHhs=@PaymentHhs
	BEGIN
		SET @SysDetailCode='POSTPAYROLL'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId2
		FROM PaymentCycleDetail T1 INNER JOIN (
												SELECT PaymentCycleId,ProgrammeId,COUNT(HhId) AS PayrollHhs
												FROM Payroll
												GROUP BY PaymentCycleId,ProgrammeId
												) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
								   INNER JOIN (
												SELECT PaymentCycleId,ProgrammeId,COUNT(HhId) AS PaymentHhs
												FROM Payment
												GROUP BY PaymentCycleId,ProgrammeId
												) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.PaymentStageId=@SystemCodeDetailId1 AND T2.PayrollHhs=T3.PaymentHhs
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,@PaymentHhs AS NoOfRecs,'Fatal Error' AS [Description]
	END
	ELSE
	BEGIN
		COMMIT TRAN
		IF ISNULL(@PayrollHhs,0)>ISNULL(@PaymentHhs,0)
			SELECT -1 AS StatusId,@PaymentHhs AS NoOfRecs,'Payment results has not yet been updated for the complete set of beneficiaries. '+CONVERT(varchar(10),@PayrollHhs-@PaymentHhs)+' out of '+CONVERT(varchar(10),@PayrollHhs)+' are still remaining to be updated.' AS [Description]
		ELSE
			SELECT 0 AS StatusId,@PaymentHhs AS NoOfRecs,'All your payment information has been updated successfully' AS [Description]
	END
END
GO