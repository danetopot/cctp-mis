use tempdb
   go

   dbcc shrinkfile (tempdev, 10)
   go
   -- this command shrinks the primary data file

   dbcc shrinkfile (templog, 10)
   go
   -- this command shrinks the log file, examine the last paragraph.