



select distinct sourceId from Household


IF NOT OBJECT_ID('Source') IS NULL	
DROP TABLE Source
GO

CREATE TABLE Source(
	Id tinyint NOT NULL IDENTITY(1,1)
   ,Name varchar(30) NOT NULL
   ,[Description] varchar(100) NOT NULL
   ,CONSTRAINT PK_Source PRIMARY KEY (Id)
)
GO

insert into Source( Name,[Description])
SELECT    T1.[Name],T1.[Description] FROM (
SELECT 1 Id, '70 Plus Registration' as [Name], '70 plus Inua Jamii Registration' as [Description]
UNION
SELECT 2, 'Legacy MIS Migration', '2018/2019 Legacy MIS beneficiaries Migration'
UNION
SELECT 3, 'CCTP-MIS Registration', 'CCTP-MIS Registration Modules'
) T1
LEFT JOIN Source T2 ON T1.Name = T2.[NAME] WHERE T2.ID IS NULL ORDER BY T1.Id



go



select * from Source
go


SELECT DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) 'day_prev_q',
  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0)'day_1_cur_q',
  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0)) 'day_last_c_q',
   DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) + 1, 0) 'day_first_n_q',
    DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) -1, 0)) 'day_last_n_q'
 ,  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) -1, 0))
 , DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) 'day_prev_q'
 , DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0) 'day_1_prev_year'
 , DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)) 'day_last_prev_year'
 , DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
 , DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
 , DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)
,  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) -4, 0)) 'day_q4_ago',
         DATEADD (dd, 0, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) -4, 0)) 'day_q4_ago'
        SELECT  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0)) 'day_last_c_q'