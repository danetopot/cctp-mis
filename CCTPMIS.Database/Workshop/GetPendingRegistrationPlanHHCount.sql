

IF NOT OBJECT_ID('GetPendingRegistrationPlanHHCount') IS NULL	
 DROP PROC GetPendingRegistrationPlanHHCount
 GO
CREATE PROC GetPendingRegistrationPlanHHCount
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	 
	SELECT
		 COUNT(Id) AS PendingHHs
	FROM HouseholdReg T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) AS TG ON TG.SubLocationId  = T1.SubLocationId
			AND T1.HouseholdRegAcceptId  IS NULL and T1.TargetPlanId = @TargetPlanId  
			--AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			--AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
 
	--GROUP BY TG.ConstituencyId, TG.Constituency, TG.County, TG.LocationId, TG.Location
 
END
GO



GetPendingRegistrationPlanHHCount 1009


SELECT * FROM dbo.HouseholdReg