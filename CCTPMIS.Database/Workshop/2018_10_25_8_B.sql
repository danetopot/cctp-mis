alter PROC [dbo].[GeneratePayrollFile]
	@PaymentCycleId int
   ,@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @LoopVar int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults
	SELECT @FromMonthId=FromMonthId,@ToMonthId=ToMonthId,@FinancialYearId=FinancialYearId FROM PaymentCycle WHERE Id=@PaymentCycleId

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not ready for generation'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	   

	DECLARE @PSPs TABLE(
		RowId int NOT NULL IDENTITY(1,1)
	   ,PSPId int NOT NULL
	   ,PSPCode nvarchar(20) NOT NULL
	   ,FileCreationId int NULL
	   ,UserId int NOT NULL
	)

	INSERT INTO temp_PayrollFile(PaymentCycleId,PaymentCycle,EnrolmentNo,ProgrammeNo,BankId,BankCode,BankName,BranchCode,BranchName,AccountNo,AccountName,Amount,BeneficiaryIDNo,BeneficiaryContact,CaregiverIDNo,CaregiverContact,SubLocation,Location,Division,District,County,Constituency)
	SELECT T1.PaymentCycleId,T2.[Description] AS PaymentCycle,T7.Id AS EnrolmentNo,T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T10.Id AS PSPId,T10.Code AS PSPCode,T10.Name AS PSPName,T9.Code AS PSPBranchCode,T9.Name AS PSPBranchName,T8.AccountNo,T8.AccountName,T1.PaymentAmount AS Amount,T4.NationalIDNo AS BeneficiaryIDNo,T4.MobileNo1 AS BeneficiaryContact,T5.NationalIDNo AS CaregiverIDNo,T5.MobileNo1 AS CaregiverContact,T14.SubLocation,T14.Location,T14.Division,T14.District,T14.County,T14.Constituency
	FROM Payroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN Person T4 ON T3.BenePersonId=T4.Id
					LEFT JOIN Person T5 ON T3.CGPersonId=T5.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
					INNER JOIN BeneficiaryAccount T8 ON T3.BeneAccountId=T8.Id
					INNER JOIN PSPBranch T9 ON T8.PSPBranchId=T9.Id
					INNER JOIN PSP T10 ON T9.PSPId=T10.Id
					INNER JOIN Household T11 ON T1.HhId=T11.Id
					INNER JOIN HouseholdSubLocation T12 ON T11.Id=T12.HhId
					INNER JOIN GeoMaster T13 ON T12.GeoMasterId=T13.Id AND T13.IsDefault=1
					INNER JOIN (
									SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency
									FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
														INNER JOIN Division T7 ON T2.DivisionId=T7.Id
														INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
														INNER JOIN District T9 ON T8.DistrictId=T9.Id
														INNER JOIN County T10 ON T8.CountyId=T10.Id
														INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
														INNER JOIN GeoMaster T12 ON T10.GeoMasterId=T10.GeoMasterId AND T12.IsDefault=1
								) T14 ON T12.SubLocationId=T14.SubLocationId
	WHERE T1.PaymentCycleId=@PaymentCycleId

	IF NOT EXISTS(SELECT 1 FROM temp_PayrollFile)
		SET @ErrorMsg='There are no beneficiaries valid for payment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)

		RETURN
	END
	INSERT INTO @PSPs(PSPId,PSPCode,UserId)
	SELECT DISTINCT T1.BankId,T1.BankCode,T2.UserId FROM temp_PayrollFile T1 INNER JOIN PSP T2 ON T1.BankId=T2.Id

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @LoopVar=1
	WHILE EXISTS(SELECT 1 FROM @PSPs WHERE RowId=@LoopVar)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SET @FileName='PAYROLL_'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@ToMonthId),1)+'-'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SELECT @FileName=@FileName+'_'+PSPCode FROM @PSPs WHERE RowId=@LoopVar
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName

		TRUNCATE TABLE temp_PSPPayrollFile;
		INSERT INTO temp_PSPPayrollFile(PaymentCycleId,EnrolmentNo,BankCode,BranchCode,AccountNo,AccountName,Amount,BeneficiaryIDNo,CaregiverIDNo)
		SELECT T1.PaymentCycleId,EnrolmentNo,BankCode,BranchCode,AccountNo,AccountName,Amount,BeneficiaryIDNo,CaregiverIDNo
		FROM temp_PayrollFile T1 INNER JOIN @PSPs T2 ON T1.BankCode=T2.PSPCode
		WHERE T2.RowId=@LoopVar

		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_PSPPayrollFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='PAYMENT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,TargetUserId,CreatedBy,CreatedOn)
		SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS Isshared,T1.UserID AS TargetUserID,@UserId AS CreatedBy,GETDATE() AS CreatedOn FROM @PSPS T1 WHERE T1.RowId=@LoopVar

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.FileCreationId=@FileCreationId
		FROM @PSPs T1
		WHERE RowId=@LoopVar

		SET @LoopVar=@LoopVar+1
	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='MOBILIZATION_'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@ToMonthId),1)+'-'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM vw_temp_PayrollMobilization" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	--SELECT @SQLStmt
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
	--RECORDING THE FILE
	SET @SysCode='File Type'
	SET @SysDetailCode='PAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,TargetUserId,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS Isshared,NULL AS TargetUserID,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

	TRUNCATE TABLE temp_PayrollFile;
	TRUNCATE TABLE temp_PSPPayrollFile;

	SELECT FileCreationId FROM @PSPs
	UNION
	SELECT @FileCreationId

	SET NOCOUNT OFF
END

GO