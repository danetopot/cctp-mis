

DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(30)
DECLARE @DetailCode varchar(30)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int


SELECT @Id=Id FROM SystemCode WHERE Code='Payment Stage'
SET @DetailCode='PAYMENTCYCLESKIP'
SET @Description='Payment Cycle Skipped'
SET @OrderNo=1
EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;



    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @ErrorMsg varchar(128)

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PAYMENTCYCLEAPV'
    SELECT *
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode --AND T1.Code=@SysDetailCode
