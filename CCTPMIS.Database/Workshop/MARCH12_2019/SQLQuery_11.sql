SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GenerateEnrolmentFile]
	@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS

BEGIN

	SET NOCOUNT ON



	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath
	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults
	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	DROP TABLE #FileResults
	IF OBJECT_ID('temp_EnrolmentFile') IS NOT NULL	DROP TABLE temp_EnrolmentFile;
	CREATE TABLE temp_EnrolmentFile(
		EnrolmentNo int
	   ,Programme varchar(50)
	   ,ProgrammeNo varchar(50)
	   ,BeneFirstName varchar(50)
	   ,BeneMiddleName varchar(50)
	   ,BeneSurname varchar(50)
	   ,BeneIDNo varchar(30)
	   ,BeneSex varchar(20)
	   ,BeneDoB datetime
	   ,CGFirstName varchar(50)
	   ,CGMiddleName varchar(50)
	   ,CGSurname varchar(50)
	   ,CGIDNo varchar(30)
	   ,CGSex varchar(20)
	   ,CGDoB datetime
	   ,MobileNo1 varchar(20)
	   ,MobileNo2 varchar(20)
	   ,County varchar(30)
	   ,Constituency varchar(30)
	   ,District varchar(30)
	   ,Division varchar(30)
	   ,Location varchar(30)
	   ,SubLocation varchar(30)
	   );
	INSERT INTO temp_EnrolmentFile(EnrolmentNo,Programme,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneIDNo,BeneSex,BeneDoB,CGFirstName,CGMiddleName,CGSurname,CGIDNo,CGSex,CGDoB,MobileNo1,MobileNo2,County,Constituency,District,Division,Location,SubLocation)

	SELECT T2.Id AS EnrolmentNo
		,T4.Code AS Programme
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!

	WHERE T1.StatusId=@SystemCodeDetailId1

	IF NOT EXISTS(SELECT 1 FROM temp_EnrolmentFile)
		SET @ErrorMsg='There is no beneficiary selection approved for enrolment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN

		RAISERROR(@ErrorMsg,16,1)

		RETURN

	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;



	SET @FileName='ENROLMENT_'



	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END

	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END

	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))

	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) 
	END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END

	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time

	SET @FilePathName=@FilePath+@FileName

	SET @FileExtension='.csv'

	SET @FileCompression='.rar'



	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_EnrolmentFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'

	--SELECT @SQLStmt

	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension

	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;



	DROP TABLE temp_EnrolmentFile;

	

	--RECORDING THE FILE

	SET @SysCode='File Type'

	SET @SysDetailCode='ENROLMENT'

	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	SET @SysCode='File Creation Type'

	SET @SysDetailCode='SYSGEN'

	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,CreatedBy,CreatedOn)

	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS IsShared,@UserId AS CreatedBy,GETDATE() AS CreatedOn



	SET @FileCreationId=IDENT_CURRENT('FileCreation')



	SET @SysCode='Enrolment Status'

	SET @SysDetailCode='PROGSHAREPSP'

	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	SET @SysDetailCode='PSPRECEIPT'

	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	UPDATE T1

	SET T1.StatusId=@SystemCodeDetailId2

	   ,T1.FileCreationId=@FileCreationId

	FROM HouseholdEnrolmentPlan T1

	WHERE T1.StatusId=@SystemCodeDetailId1



	SELECT @FileCreationId AS FileCreationId

	SET NOCOUNT OFF

END



 
GO
