




SELECT  County, Constituency, [1] AS MALE, [2] AS FEMALE, [1]+[2] AS Total
FROM 

(
SELECT   T1.Id,   T2.* FROM  Constituency T1
LEFT JOIN (
SELECT T1.Id 'BeneAcc',  T11.*,  T10.[Description] 'Sex', T7.SexId 
FROM   BeneficiaryAccount T1
INNER JOIN  HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id
INNER JOIN Household T3 ON T2.HhId=T3.Id
INNER JOIN HouseholdSubLocation T4 ON T3.Id=T4.HhId
INNER JOIN  Programme T5 ON T3.ProgrammeId = T5.Id
INNER JOIN HouseholdMember T6 ON T6.HhId  = T6.Id AND T6.MemberRoleId = T5.PrimaryRecipientId
INNER JOIN Person T7 ON   T6.PersonId   = T6.Id
INNER JOIN SystemCodeDetail T10 ON   T7.SexId = T10.Id

   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency, T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T11 ON T11.SubLocationId=T4.SubLocationId AND T11.GeoMasterId=T4.GeoMasterId
   WHERE T1.DateAdded IS   NULL  AND T1.StatusId=23

 ) T2 ON T1.Id = T2.ConstituencyId and t1.CountyId = T2.CountyId

) P  

PIVOT (COUNT (BeneAcc) FOR SexId IN ([1],[2]))AS PVT

ORDER BY PVT.County, PVT.Constituency
