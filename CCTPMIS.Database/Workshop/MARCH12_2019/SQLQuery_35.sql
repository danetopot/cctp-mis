SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  
ALTER PROC [dbo].[GetBeneHouseholdEnrolmentInfo]    
 @Id int    
AS    
BEGIN    
 DECLARE @SysCode varchar(20)    
 DECLARE @SysDetailCode varchar(20)    
 DECLARE @SystemCodeDetailId1 int    
 DECLARE @SystemCodeDetailId2 int    
 DECLARE @SystemCodeDetailId3 int    
 DECLARE @Len tinyint    
 DECLARE @Prefix varchar(10)    
 DECLARE @ProgNo int    
 DECLARE @HhId int    
 DECLARE @LoopVar int    
   
    
 SET @SysCode='Member Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
    
 SET @SysCode='Account Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
    
 SET @SysCode='Card Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
         

  BEGIN    
   SELECT T2.Id AS ProgrammeId,T2.Code AS ProgrammeCode,T2.Name AS ProgrammeName, T3.Id  as EnrolmentNo,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.ProgrammeNo)))+CONVERT(varchar(6),T3.ProgrammeNo) AS ProgrammeNo,
  T5.Id AS PriReciPersonId,T5.FirstName AS PriReciFirstName,T5.MiddleName AS PriReciMiddleName,T5.Surname AS PriReciSurname,T5.SexId AS PriReciSexId,T10.Code AS PriReciSex,T5.NationalIdNo AS PriReciNationalIdNo,T5.BirthCertNo AS PriReciBirthCertNo,T5.MobileNo1 AS PriReciMobileNo1,T5.MobileNo2 AS PriReciMobileNo2    
 ,T6.Id AS SecReciPersonId,T6.FirstName AS SecReciFirstName,T6.MiddleName AS SecReciMiddleName,T6.Surname AS SecReciSurname,T6.SexId AS SecReciSexId,T11.Code AS SecReciSex,T6.NationalIdNo AS SecReciNationalIdNo,T6.BirthCertNo AS SecReciBirthCertNo,T6.MobileNo1 AS SecReciMobileNo1,T6.MobileNo2 AS SecReciMobileNo2    
 ,T7.AccountNo,T7.AccountName,T7.PSPBranch,T7.PSP,T7.OpenedOn AS AccountOpenedOn    
 ,T12.PaymentCardNo AS PaymentCardNo    
 ,T9.SubLocationId,T9.SubLocationName,T9.LocationId,T9.LocationName,T9.DivisionId,T9.DivisionName,T9.DistrictId,T9.DistrictName,T9.CountyId,T9.CountyName,T9.ConstituencyId,T9.ConstituencyName    
   FROM Household T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id    
   INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId    
   INNER JOIN HouseholdMember T4  ON T1.Id=T4.HhId    
   INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1    
   LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1    
   LEFT JOIN (    
     SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn    
     FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id    
     INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id    
     INNER JOIN PSP T4 ON T3.PSPId=T4.Id    
   --  WHERE T2.HhId=@HhId AND T1.StatusId=@SystemCodeDetailId2    
    ) T7 ON T3.Id=T7.HhEnrolmentId    
   INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId    
   INNER JOIN (    
     SELECT T1.Id AS SubLocationId,T1.Name AS SubLocationName,T2.Id AS LocationId,T2.Name AS LocationName,T3.Id AS DivisionId,T3.Name AS DivisionName,T5.Id AS DistrictId,T5.Name AS DistrictName,T6.Id AS CountyId,T6.Name AS CountyName,T7.Id AS ConstituencyId,T7.Name AS ConstituencyName,T8.Id AS GeoMasterId    
     FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id    
    INNER JOIN Division T3 ON T2.DivisionId=T3.Id    
    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id    
    INNER JOIN District T5 ON T4.DistrictId=T5.Id    
    INNER JOIN County T6 ON T4.CountyId=T6.Id    
    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id    
    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1    
    ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId    
   INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id    
   LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id    
   LEFT JOIN (    
     SELECT DISTINCT T1.BeneAccountId,T1.PaymentCardNo    
     FROM BeneficiaryPaymentCard T1     
     WHERE T1.StatusId=@SystemCodeDetailId3    
    ) T12 ON T7.BeneAccountId=T12.BeneAccountId    
   WHERE T3.HhId = @Id    
    
   RETURN    
  END    
   
 END    

GO
