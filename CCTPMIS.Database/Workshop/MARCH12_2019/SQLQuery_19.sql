
-- look into the   AddEditSystemCode and AddEditSystemCodeDetail varchar parameters

update SystemCode set Code= 'Monitoring Report Status'
where code like 'Monitoring Report%'
select * from systemcode where code like 'Monitoring Report%'
 
DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(30)
DECLARE @DetailCode varchar(30)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo INT
DECLARE @Getter INT

 
SET @Code='Monitoring Report Status'
SET @Description='Monitoring Report Status'
EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

SELECT @Id = NULL
SELECT @Id=Id
FROM SystemCode
WHERE Code ='Monitoring Report Status'
IF(ISNULL(@Id,0)>0)
	BEGIN

	SET @DetailCode='Creation Approval'
	SET @Description='Creation Approval'
	SET @OrderNo=1
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Verified'
	SET @Description='Verified'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Approved'
	SET @Description='Approved'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;

	SET @DetailCode='Rejected'
	SET @Description='Rejected'
	SET @OrderNo=2
	EXEC AddEditSystemCodeDetail @SystemCodeId=@Id,@DetailCode=@DetailCode,@Description=@Description,@OrderNo=@OrderNo,@UserId=1;


	END

	GO
  
 INSERT INTO MonitoringReportCategory ([Name],CODE,[Description],CreatedOn,CreatedBy)
SELECT T1.[Name],T1.CODE,T1.[Description],T1.CreatedOn,T1.CreatedBy
FROM(
SELECT  'Internal Monitoring Report' AS [Name] , 'IMR' Code , 'Internal Monitoring Report' Description ,GETDATE() CreatedOn ,1 AS CreatedBy --FROM  MonitoringReportCategory
UNION
SELECT 'External Monitoring Report' , 'EMR', 'External Monitoring Report', GETDATE(), 1
UNION
SELECT 'Spot-check Report' , 'SCR', 'Spot-check Report', GETDATE(), 1
UNION
SELECT 'Impact Evaluation Report' , 'IER', 'Impact Evaluation Report', GETDATE(), 1
UNION
SELECT 'Beneficiary Satisfaction Survey Report' , 'PIBBSSR', 'Programme Implementation Beneficiary Satisfaction Survey Report', GETDATE(), 1
) T1 LEFT JOIN MonitoringReportCategory T2 ON T1.Code = T2.Code where T2.ID IS NULL


select * from MonitoringReportCategory


update MonitoringReportCategory set ApvBy =1, ApvOn=1