SELECT T6.Name 'PSP' , COUNT(T1.HhId)  FROM Payroll T1 
LEFT JOIN Payment T2  ON T1.PaymentCycleId = T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId AND T1.HhId = T2.HhId
inner join HouseholdEnrolment T3 ON T1.HhId = T3.HhId
INNER JOIN BeneficiaryAccount T4 ON T4.HhEnrolmentId = T3.Id AND T4.StatusId=23
INNER JOIN PSPBranch T5 ON T5.Id = T4.PSPBranchId
INNER JOIN PSP T6 ON T6.Id = T5.PSPId
WHERE T2.HhId IS NULL
GROUP BY T6.Name 




INSERT INTO Payment (PaymentCycleId, ProgrammeId, HhId, WasTrxSuccessful, TrxAmount, TrxNo,  TrxDate, TrxNarration, CreatedBy, CreatedOn )
	SELECT 
	T1.PaymentCycleId,
	 T1.ProgrammeId, 
	 T1.HhId, 1 AS WasTrxSuccessful,
	 T1.PaymentAmount AS TrxAmount, 
CONCAT( T1.PaymentCycleId,'_',T1.ProgrammeId,'_',T1.HhId) TrxNo, 
GETDATE() AS TrxDate, 
'Delayed PSP Pending List SAU Assumed Successful' AS TrxNarration,
1 AS CreatedBy, GETDATE() AS CreatedOn FROM Payroll T1 
LEFT JOIN Payment T2  ON T1.PaymentCycleId = T2.PaymentCycleId AND T1.ProgrammeId = T2.ProgrammeId AND T1.HhId = T2.HhId
WHERE T2.HhId IS NULL





