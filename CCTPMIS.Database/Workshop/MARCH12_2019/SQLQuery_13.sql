



 
SELECT  County, Constituency, [1] AS MALE, [2] AS FEMALE, [1]+[2] AS Total  FROM 
(
SELECT  T1.Name 'Constituency', T2.CountyName 'County', T2.*  FROM  Constituency T1
LEFT JOIN 
(
SELECT T1.Id 'BeneAcc', T13.County 'CountyName',  T13.Constituency 'ConstituencyName', T10.[Description] Sex, T9.SexId from   BeneficiaryAccount T1
INNER JOIN HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id
INNER JOIN Household T6 ON T5.HhId=T6.Id
INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
INNER JOIN (
        SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency FROM SubLocation T1  
        INNER JOIN Location T2 ON T1.LocationId=T2.Id
        INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
        INNER JOIN County T6 ON T7.CountyId=T6.Id
        INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
INNER JOIN HouseholdMember T7 ON T7.HhId  = T6.Id AND T7.MemberRoleId = T4.PrimaryRecipientId
INNER JOIN Person T9 ON T9.ID = T7.PersonId  
INNER JOIN SystemCodeDetail T10 ON T10.Id = T9.SexId

WHERE T1.DateAdded IS   NULL  AND T1.StatusId=23
 ) T2 ON T1.Name = T2.ConstituencyName

) P  

PIVOT (COUNT (BeneAcc) FOR SexId IN ([1],[2]))AS PVT

ORDER BY PVT.County, PVT.Constituency



SELECT * FROM SystemCodeDetail WHERE [Description] LIKE '%MALE%'