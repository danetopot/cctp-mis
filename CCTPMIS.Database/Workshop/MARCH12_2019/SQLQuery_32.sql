
IF NOT OBJECT_ID('SkipPayroll') IS NULL	
DROP PROC SkipPayroll
GO
CREATE PROC SkipPayroll
    @PaymentCycleId int
   ,
    @UserId int
AS
BEGIN

    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @ErrorMsg varchar(128)

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PAYMENTCYCLEAPV'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    IF NOT EXISTS(SELECT 1
    FROM PaymentCycleDetail
    WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1
    FROM PaymentCycleDetail T1
    WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The Payment Cycle appears not to be in Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1
    FROM [User]
    WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='CLOSED'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    UPDATE T1
	SET T1.Skippedby=@UserId
	   ,T1.SkippedOn=GETDATE()
	   ,T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

   

   SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM PaymentCycle T1
	WHERE T1.Id=@PaymentCycleId


END
    go

     


     exec GetUserGroupRights 2