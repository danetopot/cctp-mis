


IF NOT OBJECT_ID('RptGetBeneficiariesWithNoAccounts') IS NULL	
DROP PROC RptGetBeneficiariesWithNoAccounts
GO

CREATE PROC RptGetBeneficiariesWithNoAccounts
    @DateRange varchar(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @StatusXml xml,
    @RegGroupXml xml,
    @EnrolmentGroupXml xml,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type varchar(100)= ''

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE @offset int = (@Page-1)*@PageSize
    DECLARE @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    declare @Guid varchar(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			                                                             SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'STATUS' AS ReportType
                FROM @StatusXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'REGGROUP' AS ReportType
                FROM @RegGroupXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'ENROLMENT_GRP' AS ReportType
                FROM @EnrolmentGroupXml.nodes('Report/Record') AS U(R) ) T1
			) T0


    
    
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T10.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T10.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T10.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
            INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
            INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId AND T2A.CreatedOn BETWEEN @StartDate AND @EndDate
            INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id
            INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id

            INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
            INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
            INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
            INNER JOIN temp_ReportID T18 ON T18.ReportId  = T1.ProgrammeId AND T18.ReportType = 'PROGRAMME' and T18.ReportGuId = @Guid
            INNER JOIN temp_ReportID T20 ON T20.ReportId  = T6.SexId AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.StatusId AND T19.ReportType = 'STATUS' and T19.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19b ON T19b.ReportId  = T1.RegGroupId AND T19b.ReportType = 'REGGROUP' and T19B.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19c ON T19c.ReportId  = T2A.EnrolmentGroupId AND T19C.ReportType = 'ENROLMENT_GRP' and T19C.ReportGuId = @Guid
            INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid

            LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid

            LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
            INNER JOIN Person T9 ON T8.PersonId=T9.Id
            INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            LEFT JOIN temp_ReportID T23 ON T23.ReportId  = T9.SexId AND T23.ReportType = 'SEC_SEX' and T23.ReportGuId = @Guid
            LEFT JOIN BeneficiaryAccount T3 ON T3.HhEnrolmentId= T2.Id
        WHERE T3.Id IS NULL

        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
select X.*, T13.*, T4.Name, T4.code from x 
inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	ELSE  IF(@Type='DETAILS')
	BEGIN
        SELECT t2.Id, T6.FirstName AS BeneFirstName, T1.CreatedOn, T4.Name Programme
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		, COUNT(T1.Id) OVER () as  NoOfRows
        FROM Household T1
            INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
            INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId
            INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id
            INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id

            INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
            INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
            INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

            INNER JOIN temp_ReportID T18 ON T18.ReportId  = T1.ProgrammeId AND T18.ReportType = 'PROGRAMME' and T18.ReportGuId = @Guid
            INNER JOIN temp_ReportID T20 ON T20.ReportId  = T6.SexId AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.StatusId AND T19.ReportType = 'STATUS' and T19.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19b ON T19b.ReportId  = T1.RegGroupId AND T19b.ReportType = 'REGGROUP' and T19B.ReportGuId = @Guid
            INNER JOIN temp_ReportID T19c ON T19c.ReportId  = T2A.EnrolmentGroupId AND T19C.ReportType = 'ENROLMENT_GRP' and T19C.ReportGuId = @Guid
            INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid

            LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid

            LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
            INNER JOIN Person T9 ON T8.PersonId=T9.Id
            INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
            LEFT JOIN temp_ReportID T23 ON T23.ReportId  = T9.SexId AND T23.ReportType = 'SEC_SEX' and T23.ReportGuId = @Guid
            LEFT JOIN BeneficiaryAccount T3 ON T3.HhEnrolmentId= T2.Id
        WHERE T3.Id IS NULL

        ORDER BY T1.CreatedOn
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END
GO

