USE [CCTP-MIS]
GO
 

ALTER  PROC [dbo].[GetEnroledBeneficiariesByProgrammeAndGender] (@startDate datetime, @enddate datetime, @Programme varchar(10)
)

AS
BEGIN

select  County, [74] AS Male, [75] AS Female

FROM 

(
SELECT T1.Name 'County', T2.SexId, T2.BeneAcc FROM County  T1 LEFT JOIN 
( 
SELECT T1.Id 'BeneAcc',T13.County CountyName, t13.CountyId, T08.SexId, T09.[Description] 'Sex'
from   BeneficiaryAccount T1
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id  and T1.OpenedOn BETWEEN @startDate AND @startDate
  INNER JOIN Household T6 ON T5.HhId=T6.Id
  inner join Programme t06 on t06.Id = t6.ProgrammeId
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
 INNER JOIN HouseholdMember T07 ON T6.ID = T07.HhId and t07.MemberRoleId = t06.PrimaryRecipientId
 INNER JOIN  Person T08 ON T07.PersonId = T08.Id 
  INNER JOIN  SystemCodeDetail T09 ON T08.SexId = T09.Id 
 

   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE  T1.StatusId=23  AND T4.Code=@Programme  
  ) T2 ON T1.Id = T2.CountyId
  ) P
  PIVOT (COUNT (BeneAcc) FOR SexId IN ([74],[75]))AS PVT

END




exec GetEnroledBeneficiariesByProgrammeAndGender @startDate ='2018-01-01', @enddate ='2018-10-11', @Programme ='OPCT'
