 

 create proc ApproveMonitoringReport (@Id int, @UserId int, @Type VARCHAR(20)='VERIFY')
 AS

 BEGIN
 
 IF(@Type='VERIFY')
 BEGIN
 UPDATE MonitoringReport SET VerifiedOn=GETDATE(), VerifiedBy=@UserId WHERE Id = @Id
 END
 IF(@Type='APPROVE')
 BEGIN
 UPDATE MonitoringReport SET ApvOn=GETDATE(), ApvBy=@UserId WHERE Id = @Id
 END
 END
 GO


 SELECT * FROM MONITORINGREPORT