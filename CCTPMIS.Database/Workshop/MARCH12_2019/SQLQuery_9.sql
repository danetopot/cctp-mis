DECLARE @RC int
DECLARE @PaymentCycleId int
DECLARE @EnrolmentNo int
DECLARE @BankCode nvarchar(20)
DECLARE @BranchCode nvarchar(20)
DECLARE @AccountNo varchar(50)
DECLARE @AccountName varchar(100)
DECLARE @AmountTransferred money
DECLARE @WasTransferSuccessful bit
DECLARE @TrxNo nvarchar(50)
DECLARE @TrxDate datetime
DECLARE @TrxNarration nvarchar(128)
DECLARE @UserId int

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[PSPPayrollTrx] 
   @PaymentCycleId
  ,@EnrolmentNo
  ,@BankCode
  ,@BranchCode
  ,@AccountNo
  ,@AccountName
  ,@AmountTransferred
  ,@WasTransferSuccessful
  ,@TrxNo
  ,@TrxDate
  ,@TrxNarration
  ,@UserId
GO