 -- insert into [CCTP-MIS].dbo.[user] ([UserName], [PasswordHash], [SecurityStamp], [UserGroupId], [Email], [EmailConfirmed], [PasswordChangeDate], [FirstName], [MiddleName], [Surname], [Avatar], [Organization], [Department], [Position], [MobileNo], [MobileNoConfirmed], [IsActive], [DeactivateDate], [LoginDate], [ActivityDate], [AccessFailedCount], [IsLocked], [LockoutEnabled], [LockoutEndDateUTC], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn], [StaffNo], [NationalIDNo])

select T1.[UserName], T1.[PasswordHash], T1.[SecurityStamp], 3, T1.[Email], T1.[EmailConfirmed], T1.[PasswordChangeDate], T1.[FirstName], T1.[MiddleName], T1.[Surname], T1.[Avatar],T1. [Organization], T1.[Department], T1.[Position], T1.[MobileNo], T1.[MobileNoConfirmed], T1.[IsActive], T1.[DeactivateDate], T1.[LoginDate], T1.[ActivityDate], T1.[AccessFailedCount], T1.[IsLocked], T1.[LockoutEnabled],T1.[LockoutEndDateUTC], T1.[CreatedBy],T1. [CreatedOn], T1.[ModifiedBy], T1.[ModifiedOn], T1.[StaffNo], T1.[NationalIDNo]
from GeoDB.dbo.[User]  T1
LEFT JOIN [CCTP-MIS].dbo.[user] T2 ON T1.Email = T2.Email WHERE T2.ID IS NULL

GO