ALTER PROC [dbo].[AddEditPaymentCycle]
    @Id int=NULL,
    @Description varchar(128),
    @FinancialYearId int,
    @FromMonthId int,
    @ToMonthId int,
	@FromMonthActivityDate datetime,
	@ToMonthActivityDate datetime,
    @UserId int
AS
BEGIN
	DECLARE @tblPaymentMonthlyActivity TABLE(
	   MonthId int
	   ,[Year] smallint
	)
    DECLARE @ErrorMsg varchar(128)
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
	DECLARE @CalendarMonthsCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @ACActivityMonthId int
	DECLARE @ACActivityMonthCode varchar(5)
	DECLARE @ACActivityYear int
	DECLARE @LoopVar int
    DECLARE @StatusId int

	SET @CalendarMonthsCode='Calendar Months'
	SET @FinancialYearCode='Financial Year'
	SET @SysCode='System Settings'
	SET @SysDetailCode='CURFINYEAR'
	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Payment Status'
    SET @SysDetailCode='PAYMENTOPEN'
    SELECT @StatusId  =T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ACActivityMonthId=T1.MonthId,@ACActivityMonthCode=T1.Code,@ACActivityYear=T1.Year
	FROM (	
			SELECT TOP 1 T1.PaymentCycleId,T1.[Year],T2.Code,T1.MonthId
			FROM PaymentAccountActivityMonth T1 INNER JOIN SystemCodeDetail T2 ON T1.MonthId=T2.Id
			WHERE DATEDIFF(MM,CONVERT(datetime,'1 '+dbo.fn_MonthName(T2.Code,0)+' '+CONVERT(varchar(4),T1.[Year])),GETDATE())>1
			ORDER BY T1.PaymentCycleId,T1.[Year],T2.Code DESC
		) T1

	SET @LoopVar=0
	WHILE (DATEDIFF(MM,@FromMonthActivityDate,@ToMonthActivityDate)-@LoopVar>=0)
	BEGIN
		INSERT INTO @tblPaymentMonthlyActivity(MonthId,[Year])
		SELECT T1.Id,YEAR(DATEADD(MM,@LoopVar,@FromMonthActivityDate))
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
		WHERE T2.Code=@CalendarMonthsCode AND T1.Code=MONTH(DATEADD(MM,@LoopVar,@FromMonthActivityDate))

		SET @LoopVar=@LoopVar+1
	END

	
	IF @FinancialYearId<>@SystemCodeDetailId1
		SET @ErrorMsg='The FinancialYearId parameter does not match the configured current system financial year'	
	ELSE IF @FromMonthActivityDate IS NULL
		SET @ErrorMsg='Please specify valid FromMonthActivityDate parameter'
	ELSE IF @ToMonthActivityDate IS NULL
		SET @ErrorMsg='Please specify valid ToMonthActivityDate parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentMonthlyActivity)
		SET @ErrorMsg='Please specify valid FromMonthActivityDate and ToMonthActivityDate'
  --  ELSE IF EXISTS(SELECT 1
--					FROM PaymentCycle
--					WHERE StatusId=@StatusId and @Id  is NULL )
	--	SET @ErrorMsg='There is an existing payment cycle whose payment need to be completed'
	--ELSE IF NOT EXISTS(
	--					SELECT 1
	--					FROM BeneAccountMonthlyActivity T1 
	--					WHERE T1.MonthId=@ACActivityMonthId AND T1.[Year]=@ACActivityYear AND T1.StatusId=@SystemCodeDetailId2 
	--					) AND @ACActivityMonthId>0
--		SET @ErrorMsg='Account monthly activity for the period '+dbo.fn_MonthName(@ACActivityMonthCode,0)+','+CONVERT(varchar(4),@ACActivityYear)+' need to have been completely submitted'		

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END
	ELSE
    BEGIN
        BEGIN TRAN
        IF EXISTS(SELECT 1
        FROM PaymentCycle
        WHERE Id=@Id )
			UPDATE PaymentCycle SET [Description] = @Description,FinancialYearId =  @FinancialYearId ,FromMonthId =  @FromMonthId,ToMonthId = @ToMonthId , StatusId =  @StatusId,ModifiedBy = @UserId,ModifiedOn = GETDATE() WHERE Id =  @Id
		ELSE  
            INSERT INTO PaymentCycle
            ( [Description] ,FinancialYearId ,FromMonthId ,ToMonthId ,StatusId,CreatedBy,CreatedOn )
			VALUES
            ( @Description  , @FinancialYearId  , @FromMonthId  , @ToMonthId , @StatusId  , @UserId, GETDATE())

		SELECT @Id=Id FROM PaymentCycle WHERE FinancialYearId=@FinancialYearId AND FromMonthId=@FromMonthId AND ToMonthId=@ToMonthId 

		DELETE FROM PaymentAccountActivityMonth WHERE PaymentCycleId=@Id

		INSERT INTO PaymentAccountActivityMonth(PaymentCycleId,[Year],MonthId)
		SELECT @Id AS PaymentCycleId,T1.[Year],T1.MonthId
		FROM @tblPaymentMonthlyActivity T1

		IF @@ERROR>0
		BEGIN
				ROLLBACK TRAN
				SELECT 0 AS NoOfRows
			END
		ELSE
		BEGIN
            COMMIT TRAN
            SELECT @@ROWCOUNT AS NoOfRows
        END
    END
END
GO