DECLARE @RC int
DECLARE @MonthNo tinyint
DECLARE @Year int
DECLARE @BankCode nvarchar(20)
DECLARE @UserId int

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[PSPFinalizeBeneAccountMonthlyActivity] 
   @MonthNo
  ,@Year
  ,@BankCode
  ,@UserId
GO