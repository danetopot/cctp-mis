DECLARE @RC int
DECLARE @PaymentCycleId int
DECLARE @BankCode nvarchar(20)
DECLARE @UserId int

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[PSPFinalizePayrollTrx] 
   @PaymentCycleId
  ,@BankCode
  ,@UserId
GO