GetBeneHouseholdEnrolmentInfo 333

IF NOT OBJECT_ID('GetClassFromTable') IS NULL	
DROP PROCEDURE GetClassFromTable
GO
CREATE PROCEDURE  GetClassFromTable(@TableName sysname )
AS
BEGIN
DECLARE @Result VARCHAR(max) = 'public class ' + @TableName + '{'
SELECT @Result = @Result + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }'
FROM
( SELECT  REPLACE(col.name, ' ', '_') ColumnName, column_id ColumnId,
		CASE typ.name 
			WHEN 'bigint' THEN 'lONg'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'mONey' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmONey' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			else 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'mONey', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmONey', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			else '' 
		END NullableSign
	FROM sys.columns col
		join sys.types typ ON
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	where object_id = object_id(@TableName)
) t
ORDER BY ColumnId
SET @Result = @Result  + '
}'
print @Result
END
GO

exec 
 GetClassFromTable   temp_GetBeneHouseholdEnrolmentInfo

 go