
DECLARE @Id INT
DECLARE @ModuleName VARCHAR(20)
DECLARE @SysCode varchar(20)
DECLARE @ModuleCode varchar(30)
DECLARE @ModuleId int

DECLARE @SysRightCode_VIEW VARCHAR(20)
DECLARE @SysRightCode_ENTRY VARCHAR(20)
DECLARE @SysRightCode_MODIFIER VARCHAR(20)
DECLARE @SysRightCode_DELETION VARCHAR(20)
DECLARE @SysRightCode_APPROVAL VARCHAR(20)
DECLARE @SysRightCode_FINALIZE VARCHAR(20)
DECLARE @SysRightCode_VERIFY VARCHAR(20)
DECLARE @SysRightCode_EXPORT VARCHAR(20)
DECLARE @SysRightCode_DOWNLOAD VARCHAR(20)
DECLARE @SysRightCode_UPLOAD VARCHAR(20)

DECLARE @SysRightId_VIEW INT
DECLARE @SysRightId_ENTRY INT
DECLARE @SysRightId_MODIFIER INT
DECLARE @SysRightId_DELETION INT
DECLARE @SysRightId_APPROVAL INT
DECLARE @SysRightId_FINALIZE INT
DECLARE @SysRightId_VERIFY INT
DECLARE @SysRightId_EXPORT INT
DECLARE @SysRightId_DOWNLOAD INT
DECLARE @SysRightId_UPLOAD INT

SET @SysCode='System Right'
SET @SysRightCode_VIEW='VIEW'
SET @SysRightCode_ENTRY='ENTRY'
SET @SysRightCode_MODIFIER='MODIFIER'
SET @SysRightCode_DELETION='DELETION'
SET @SysRightCode_FINALIZE='FINALIZE'
SET @SysRightCode_VERIFY='VERIFY'
SET @SysRightCode_APPROVAL='APPROVAL'
SET @SysRightCode_EXPORT='EXPORT'
SET @SysRightCode_DOWNLOAD='DOWNLOAD'
SET @SysRightCode_UPLOAD='UPLOAD'

SELECT @SysRightId_VIEW=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VIEW
SELECT @SysRightId_ENTRY=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_ENTRY
SELECT @SysRightId_MODIFIER=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_MODIFIER
SELECT @SysRightId_DELETION=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DELETION
SELECT @SysRightId_APPROVAL=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_APPROVAL
SELECT @SysRightId_FINALIZE=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_FINALIZE
SELECT @SysRightId_VERIFY=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_VERIFY
SELECT @SysRightId_EXPORT=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_EXPORT
SELECT @SysRightId_DOWNLOAD=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_DOWNLOAD
SELECT @SysRightId_UPLOAD=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysRightCode_UPLOAD





SET @ModuleName = 'Monitoring & Evaluation'
SELECT @Id = Id
FROM Module
WHERE [Name] = @ModuleName

BEGIN
    INSERT INTO Module
        (Name, [Description],ParentModuleId)
    SELECT T1.Name, T1.[Description], T1.ParentModuleId
    FROM (            
            SELECT 2 AS Id, 'Payments Reports', 'Payments Reports', @Id AS ParentModuleId
        UNION
            SELECT 3 AS Id, 'Case Management Reports', 'Case Management Reports', @Id AS ParentModuleId
        UNION
            SELECT 4 AS Id, 'Enrolment Reports', 'Enrolment Reports', @Id AS ParentModuleId
        UNION
            SELECT 5 AS Id, 'Targeting Reports', 'Targeting Reports', @Id AS ParentModuleId
      

	)T1 LEFT JOIN Module T2 ON T1.Name = T2.Name
    WHERE  T2.Name IS  NULL
END



SET @ModuleName = 'Payments Reports'
SELECT @Id = Id
FROM Module
WHERE [Name] = @ModuleName

IF(ISNULL(@Id,0)>0)
BEGIN
    INSERT INTO Module
        (Name, [Description],ParentModuleId)
    SELECT T1.Name, T1.[Description], T1.ParentModuleId
    FROM (            
                                    SELECT 2 AS Id, 'PrePayroll Exceptions Reports', 'PrePayroll Exceptions Reports', @Id AS ParentModuleId
        UNION
            SELECT 3 AS Id, 'Payroll Statement Reports', 'Payroll Statement Reports', @Id AS ParentModuleId
        UNION
            SELECT 4 AS Id, 'Post-Payroll Statement Reports', 'Post-Payroll Statement Reports', @Id AS ParentModuleId
        UNION
            SELECT 5 AS Id, 'Monthly Activity Reports', 'Monthly Activity Reports', @Id AS ParentModuleId
	)T1 LEFT JOIN Module T2 ON T1.Name = T2.Name
    WHERE  T2.Name IS  NULL
END
SELECT @Id =NULL
SELECT @ModuleName  =NULL


SET @ModuleName = 'Case Management Reports'
SELECT @Id = Id
FROM Module
WHERE [Name] = @ModuleName

IF(ISNULL(@Id,0)>0)
BEGIN
    INSERT INTO Module (Name, [Description],ParentModuleId)
    SELECT T1.Name, T1.[Description], T1.ParentModuleId
    FROM (            
            SELECT 1 AS Id, 'Complaints Reports', 'Complaints Reports', @Id AS ParentModuleId
        UNION
            SELECT 2 AS Id, 'Updates Reports', 'Updates Reports', @Id AS ParentModuleId
        UNION
            SELECT 3 AS Id, 'EFC Reports', 'EFC Reports', @Id AS ParentModuleId
      UNION
            SELECT 4 AS Id, 'Beneficiary Statement', 'Beneficiary Statement', @Id AS ParentModuleId
        
	)T1 LEFT JOIN Module T2 ON T1.Name = T2.Name
    WHERE  T2.Name IS  NULL
END
SELECT @Id =NULL
SELECT @ModuleName  =NULL



SET @ModuleName = 'Enrolment Reports'
SELECT @Id = Id
FROM Module
WHERE [Name] = @ModuleName

IF(ISNULL(@Id,0)>0)
BEGIN
    INSERT INTO Module
        (Name, [Description],ParentModuleId)
    SELECT T1.Name, T1.[Description], T1.ParentModuleId
    FROM (            
            SELECT 1 AS Id, 'Enrolment Reports', 'Enrolment Reports', @Id AS ParentModuleId
        UNION
            SELECT 2 AS Id, 'Account Opening Reports', 'Account Opening Reports', @Id AS ParentModuleId
        UNION
            SELECT 3 AS Id, 'Post-Enrolment Reports', 'Post-Enrolment Reports', @Id AS ParentModuleId
        
	)T1 LEFT JOIN Module T2 ON T1.Name = T2.Name
    WHERE  T2.Name IS  NULL
END
SELECT @Id =NULL
SELECT @ModuleName  =NULL


 
SET @ModuleName = 'Targeting Reports'
SELECT @Id = Id
FROM Module
WHERE [Name] = @ModuleName

IF(ISNULL(@Id,0)>0)
BEGIN
    INSERT INTO Module
        (Name, [Description],ParentModuleId)
    SELECT T1.Name, T1.[Description], T1.ParentModuleId
    FROM (            
            SELECT 1 AS Id, 'Household Listing Reports', 'Household Listing Reports', @Id AS ParentModuleId
        UNION
            SELECT 2 AS Id, 'Registrationg Reports', 'Registrationg Reports', @Id AS ParentModuleId
        UNION
            SELECT 3 AS Id, 'Recertification Reports', 'Recertification Reports', @Id AS ParentModuleId
        
	)T1 LEFT JOIN Module T2 ON T1.Name = T2.Name
    WHERE  T2.Name IS  NULL
END
SELECT @Id =NULL
SELECT @ModuleName  =NULL
