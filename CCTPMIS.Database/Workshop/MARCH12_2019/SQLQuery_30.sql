
 	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

; WITH DuplicateChecker AS(
SELECT ROW_NUMBER() OVER(PARTITION BY T1.NationalIdNo ORDER BY T1.DoB ASC) AS RowId,T2.HhId,T1.Id AS PersonId,T1.FirstName,T1.MiddleName,T1.Surname,T1.DoB,T1.SexId,T1.NationalIdNo
									FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
												   INNER JOIN Household T3 ON T2.HhId=T3.Id
												   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
									WHERE T2.StatusId=@SystemCodeDetailId1  AND T4.Code='OPCT'
)

select * from DuplicateChecker WHERE RowId >1