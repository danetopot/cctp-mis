USE [CCTP-MIS]
GO
/****** Object:  StoredProcedure [dbo].[AcceptRegistrationHouseholds]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[AcceptRegistrationHouseholds]

 @RegPlanId INT,
 @CountyId INT = null,
 @ConstituencyId INT = null,
 @BatchName varchar(30),
 @UserId INT

 as
 BEGIN
 DECLARE @ErrorMsg varchar(128)
  DECLARE @SysCode varchar(30)
  DECLARE @SysDetailCode varchar(30)
  DECLARE @CategoryId INT
  DECLARE @StatusId INT
  DECLARE @ReceivedHHs INT
  DECLARE @RegAcceptId INT
  DECLARE @InsertDate DATE = GETDATE()
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	 
	ELSE  IF NOT EXISTS(SELECT 1 FROM RegPlan WHERE Id=@RegPlanId AND ApvBy>0)
	SET @ErrorMsg='The Registration Plan is not Approved. You cannot Accept Data into this Batch'
		IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	
BEGIN TRAN
IF(@ConstituencyId IS NOT NULL)
BEGIN
SELECT @ReceivedHHs = COUNT(Id)  FROM RegistrationHH H
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = H.SubLocationId  
																	  
																	  WHERE TG.ConstituencyId =@ConstituencyId
END
ELSE IF(@ConstituencyId IS NULL AND @CountyId IS NOT NULL )
BEGIN
SELECT @ReceivedHHs = COUNT(Id) FROM RegistrationHH H
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = H.SubLocationId  
																	 
																	  WHERE TG.CountyId =CountyId
END
ELSE IF(@ConstituencyId IS NULL AND @CountyId IS   NULL )
BEGIN
SELECT @ReceivedHHs = COUNT(Id)  FROM RegistrationHH H
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
																	  AS TG ON TG.SubLocationId  = H.SubLocationId  
																	  
END


INSERT INTO RegAccept (AccById,AccDate,ReceivedHHs,BatchName,ConstituencyId,RegPlanId) 
              SELECT @UserId,@InsertDate,@ReceivedHHs,@BatchName,@ConstituencyId,@RegPlanId  
  
select @RegAcceptId = Id from RegAccept  where BatchName = @BatchName  AND @RegPlanId=@RegPlanId  AND @ReceivedHHs = @ReceivedHHs AND ConstituencyId = @ConstituencyId 	AND AccDate = @InsertDate


IF(@ConstituencyId IS NOT NULL)
BEGIN
UPDATE T1
SET T1.RegAcceptId = @RegAcceptId
FROM RegistrationHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId
				AND  TG.ConstituencyId =@ConstituencyId
END
																	    
ELSE IF(@ConstituencyId IS NULL AND @CountyId IS NOT  NULL )
BEGIN
UPDATE T1
SET T1.RegAcceptId = @RegAcceptId
FROM RegistrationHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId
				AND  TG.CountyId =@CountyId
END
																	    
ELSE IF(@ConstituencyId IS NULL AND @CountyId IS   NULL )
BEGIN
UPDATE T1
SET T1.RegAcceptId = @RegAcceptId
FROM RegistrationHH T1   
INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
				INNER JOIN District T5 ON T4.DistrictId=T5.Id
				INNER JOIN County T6 ON T4.CountyId=T6.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
				AS TG  ON TG.SubLocationId  = T1.SubLocationId
END

					   
IF @@ERROR>0
BEGIN
	ROLLBACK TRAN
	SELECT 0 AS NoOfRows
END
ELSE
BEGIN
	COMMIT TRAN
	SELECT 1 AS NoOfRows
END
 


 
 
 
 END



GO
/****** Object:  StoredProcedure [dbo].[AddAuditTrail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AddAuditTrail]  
    @IPAddress varchar(15)  
   ,@MACAddress varchar(17)=NULL  
   ,@IMEI bigint=NULL  
   ,@ModuleRightCode varchar(64)  
   ,@Description varchar(64)   =''
   ,@TableName varchar(64)  
   ,@UserAgent varchar(64)  
   ,@Key1 int  
   ,@Key2 int=NULL  
   ,@Key3 int=NULL  
   ,@Key4 int=NULL  
   ,@Key5 int=NULL  
   ,@Record nvarchar(MAX)  
   ,@WasSuccessful bit  
   ,@UserId int = null  
AS  
BEGIN  
 DECLARE @ModuleRightId int  
 DECLARE @TableId int  
 DECLARE @SysCode varchar(20)  
 DECLARE @NoOfRows int  
 DECLARE @ErrorMsg varchar(128)  
  
  BEGIN TRY

 SELECT @ModuleRightId=T1.Id FROM ModuleRight T1 INNER JOIN Module T2 ON T1.ModuleId=T2.Id   
			 INNER JOIN SystemCodeDetail T3 ON T1.RightId=T3.Id  
			 INNER JOIN SystemCode T4 ON T3.SystemCodeId=T4.Id 
WHERE UPPER(T2.Name+':'+T1.Description)=UPPER(@ModuleRightCode)  
  

 SELECT @TableId =  Id FROM  [CCTP-MIS_AUDIT].dbo.LogTable where TableName = @TableName

 IF(ISNULL(@TableId,0)=0)
 insert into  [CCTP-MIS_AUDIT].dbo.LogTable(ModuleRightId,TableName)
 SELECT @ModuleRightId, @TableName
 
 DECLARE @FullName varchar(100)
 DECLARE @Email varchar(100)
 
 select @FullName= concat(Firstname,' ',MiddleName, '', Surname), @Email = Email from [User] where Id = @userId
 

 INSERT INTO  [CCTP-MIS_AUDIT].dbo.AuditTrail(UserId,LogTime,IPAddress,MACAddress,IMEI,ModuleRightId,Key1,Key2,Key3,Key4,Key5,Record,TableId,WasSuccessful,FullName,Module,Description,UserName)  
 SELECT @UserId,GETDATE(),@IPAddress,@MACAddress,@IMEI,@ModuleRightId,@Key1,@Key2,@Key3,@Key4,@Key5,@Record  ,@TableId,@WasSuccessful,@FullName,@ModuleRightCode, @Description,@Email
 
  SELECT  0   as StatusId
 END TRY
BEGIN CATCH
 SELECT  0   as StatusId
END CATCH
   

END  

GO
/****** Object:  StoredProcedure [dbo].[AddEditBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditBeneAccountMonthlyActivity]
	@Id int=NULL
   ,@MonthId int
   ,@Year int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @CalendarMonthCode varchar(20)
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)

	SET @CalendarMonthCode='Calendar Months'
	SET @FinancialYearCode='Financial Year'

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='CREATIONAPV'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@CalendarMonthCode AND T1.Id=@MonthId)
		SET @ErrorMsg='Please specify valid MonthId parameter'
	ELSE IF (ISNULL(@Year,0)<1900 OR ISNULL(@Year,0)>YEAR(GETDATE()))
		SET @ErrorMsg='Please specify valid Year parameter'	
	ELSE IF EXISTS(SELECT 1 FROM BeneAccountMonthlyActivity WHERE StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='Another account monthly activity is still active and needs to be concluded and closed first'
	ELSE IF EXISTS(SELECT 1 FROM BeneAccountMonthlyActivity WHERE MonthId=@MonthId AND [Year]=@Year AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='A similar account monthly activity period exists already'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.MonthId=@MonthId
		   ,T1.Year=@Year
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM BeneAccountMonthlyActivity T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO BeneAccountMonthlyActivity(MonthId,[Year],StatusId,CreatedBy,CreatedOn)
		SELECT @MonthId,@Year,@SystemCodeDetailId2,@UserId,GETDATE()
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditBulkTransferDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AddEditBulkTransferDetail]
	@Id INT
	,
	@UserId INT
AS

BEGIN

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @TransferStatusId int
	DECLARE @StatusCode varchar(20)
	DECLARE @StatusId int
	DECLARE @ErrorMsg varchar(200)
	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
	SET @ErrorMsg='Please specify valid User'

	SET @SysCode='Change Status'
	SET @SysDetailCode='Escalated'
	SELECT @StatusId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	IF NOT EXISTS (SELECT 1
	FROM CHANGE
	where   ChangeStatusId = @StatusId )
	SET @ErrorMsg='There Exists no  Update is not ready for Bulk Transfer.'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	ELSE
	BEGIN
		INSERT INTO  BulkTransferDetail
			( ChangeId ,BulkTransferId,CreatedBy,CreatedOn )
		SELECT Id, @Id, @UserId, GETDATE()
		FROM CHANGE
		where   ChangeStatusId = @StatusId
		SET @SysCode='Change Status'
		SET @SysDetailCode='Bulk Transfer'
		SELECT @TransferStatusId = T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		UPDATE CHANGE  set BulkTransferBy = @UserId, BulkTransferOn= GETDATE(),  ChangeStatusId= @TransferStatusId  WHERE   ChangeStatusId = @StatusId
		SELECT @@ROWCOUNT AS NoOfRows
	END
END



GO
/****** Object:  StoredProcedure [dbo].[AddEditCaseCategory]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[AddEditCaseCategory]
    @Id int=NULL
   ,
    @CaseTypeId int
   ,
    @Name varchar(20)
   ,
    @Code varchar(20)
   ,
    @Description varchar(128)
   ,
    @DocumentsToAttach varchar(128)
   ,
    @ReportingProcess varchar(128)
   ,
    @Timeline int
   ,
    @TypeOfDayId int
   ,
    @UserId int
AS
BEGIN
    DECLARE @ErrorMsg varchar(128)
    DECLARE @NoOfRows int

    SELECT @UserId=Id
    FROM [User]
    WHERE Id=@UserId



    IF ISNULL(@CaseTypeId,0)=0
		SET @ErrorMsg='Please specify valid Case Type'	
	ELSE IF ISNULL(@Code,'')=''
		SET @ErrorMsg='Please specify valid Code parameter'
    IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'

    IF @DocumentsToAttach IS NULL
		SET @ErrorMsg='Please specify valid DocumentsToAttach parameter'

    IF @ReportingProcess IS NULL
		SET @ErrorMsg='Please specify valid ReportingProcess parameter'

	ELSE IF EXISTS(SELECT 1
    FROM CaseCategory
    WHERE CaseTypeId=@CaseTypeId AND Code=@Code AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the  Code ('+@Code+') under the same Case Type '
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END

    BEGIN TRAN

    IF ISNULL(@Id,0)>0
	BEGIN
        UPDATE T1
		SET T1.CaseTypeId=@CaseTypeId
           ,T1.Name=@Name
		   ,T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.DocumentsToAttach=@DocumentsToAttach
		   ,T1.ReportingProcess=@ReportingProcess
           ,T1.Timeline=@Timeline
           ,T1.TypeofDayId=@TypeofDayId
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM CaseCategory T1
		WHERE T1.Id=@Id
    END
	ELSE
	BEGIN
        INSERT INTO CaseCategory
            (CaseTypeId,[Name],Code,[Description],DocumentsToAttach,ReportingProcess,Timeline, TypeofDayId, CreatedBy,CreatedOn)
        SELECT @CaseTypeId, @Name, @Code, @Description, @DocumentsToAttach, @ReportingProcess, @Timeline, @TypeofDayId, @UserId, GETDATE()

        SET @Id=IDENT_CURRENT('CaseCategory')
    END

    SET @NoOfRows=@@ROWCOUNT

    IF @@ERROR>0
	BEGIN
        ROLLBACK TRAN
        SELECT 0 AS NoOfRows
    END
	ELSE
	BEGIN
        COMMIT TRAN
        SELECT @NoOfRows AS NoOfRows

    END
END


GO
/****** Object:  StoredProcedure [dbo].[AddEditEnrolmentPlan]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditEnrolmentPlan]
	@Id int=NULL
   ,@ProgrammeId int
   ,@RegGroupId int
   ,@RegGroupHhs int
   ,@BeneHhs int
   ,@ExpPlanEqualShare int
   ,@ExpPlanPovertyPrioritized int
   ,@EnrolmentNumbers int
   ,@EnrolmentGroupId int
   ,@UserId int
AS

BEGIN

	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)

	DECLARE @NoOfRows int



	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Id=@ProgrammeId)

		SET @ErrorMsg='Please specify valid ProgrammeId parameter'

	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@RegGroupId AND T2.Code='Registration Group')

		SET @ErrorMsg='Please specify valid RegGroupId parameter'

	ELSE IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T1.Id=@EnrolmentGroupId AND T2.Code='Enrolment Group')

		SET @ErrorMsg='Please specify valid EnrolmentGroupId parameter'

	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)

		SET @ErrorMsg='Please specify valid UserId parameter'



	IF ISNULL(@ErrorMsg,'')<>''

	BEGIN

		RAISERROR(@ErrorMsg,16,1)

		RETURN

	END



	SET @SysCode='Enrolment Status'

	SET @SysDetailCode='PROGENROLAPV'

	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	BEGIN TRAN



	IF ISNULL(@Id,0)>0

	BEGIN

		UPDATE T1

		SET T1.ProgrammeId=@ProgrammeId

		   ,T1.RegGroupId=@RegGroupId

		   ,T1.RegGroupHhs=@RegGroupHhs

		   ,T1.BeneHhs=@BeneHhs

		   ,T1.ExpPlanEqualShare=@BeneHhs

		   ,T1.ExpPlanPovertyPrioritized=@BeneHhs

		   ,T1.EnrolmentNumbers=@EnrolmentNumbers

		   ,T1.EnrolmentGroupId=@EnrolmentGroupId

		   ,T1.ModifiedBy=@UserId

		   ,T1.ModifiedOn=GETDATE()

		FROM HouseholdEnrolmentPlan T1

		WHERE T1.Id=@Id

	END

	ELSE

	BEGIN

		INSERT INTO HouseholdEnrolmentPlan(ProgrammeId,RegGroupId,RegGroupHhs,BeneHhs,ExpPlanEqualShare,ExpPlanPovertyPrioritized,EnrolmentNumbers,EnrolmentGroupId,StatusId,CreatedBy,CreatedOn)

		SELECT @ProgrammeId,@RegGroupId,@RegGroupHhs,@BeneHhs,@ExpPlanEqualShare,@ExpPlanPovertyPrioritized,@EnrolmentNumbers,@EnrolmentGroupId,@SystemCodeDetailId,@UserId,GETDATE()



		SET @Id=IDENT_CURRENT('HouseholdEnrolmentPlan')

	END



	SET @NoOfRows=@@ROWCOUNT

	

	IF @@ERROR>0

	BEGIN

		ROLLBACK TRAN

		SELECT 0 AS NoOfRows

	END

	ELSE

	BEGIN

		COMMIT TRAN

		SELECT @NoOfRows AS NoOfRows

	END

END

GO
/****** Object:  StoredProcedure [dbo].[AddEditPaymentCardBiometrics]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditPaymentCardBiometrics]
	@BenePaymentCardId int
   ,@PriReciRT varbinary(max)
   ,@PriReciRI varbinary(max)
   ,@PriReciRMF varbinary(max)
   ,@PriReciRRF varbinary(max)
   ,@PriReciRP varbinary(max)
   ,@PriReciLT varbinary(max)
   ,@PriReciLI varbinary(max)
   ,@PriReciLMF varbinary(max)
   ,@PriReciLRF varbinary(max)
   ,@PriReciLP varbinary(max)
   ,@SecReciRT nvarchar(max)=NULL
   ,@SecReciRI nvarchar(max)=NULL
   ,@SecReciRMF nvarchar(max)=NULL
   ,@SecReciRRF nvarchar(max)=NULL
   ,@SecReciRP nvarchar(max)=NULL
   ,@SecReciLT nvarchar(max)=NULL
   ,@SecReciLI nvarchar(max)=NULL
   ,@SecReciLMF nvarchar(max)=NULL
   ,@SecReciLRF nvarchar(max)=NULL
   ,@SecReciLP nvarchar(max)=NULL
   ,@UserId int
AS
BEGIN
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM BeneficiaryPaymentCard WHERE Id=@BenePaymentCardId)
		SET @ErrorMsg='Please specify valid PaymentCardId'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF NOT EXISTS(SELECT 1 FROM [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics WHERE BenePaymentCardId=@BenePaymentCardId)
	--BEGIN
	--	UPDATE T1
	--	SET T1.PriReciRT=@PriReciRT
	--	   ,T1.PriReciRI=@PriReciRI
	--	   ,T1.PriReciRMF=@PriReciRMF
	--	   ,T1.PriReciRRF=@PriReciRRF
	--	   ,T1.PriReciRP=@PriReciRP
	--	   ,T1.PriReciLT=@PriReciLT
	--	   ,T1.PriReciLI=@PriReciLI
	--	   ,T1.PriReciLMF=@PriReciLMF
	--	   ,T1.PriReciLRF=@PriReciLRF
	--	   ,T1.PriReciLP=@PriReciLP
	--	   ,T1.SecReciRT=CONVERT(varbinary(max),@SecReciRT)
	--	   ,T1.SecReciRI=CONVERT(varbinary(max),@SecReciRI)
	--	   ,T1.SecReciRMF=CONVERT(varbinary(max),@SecReciRMF)
	--	   ,T1.SecReciRRF=CONVERT(varbinary(max),@SecReciRRF)
	--	   ,T1.SecReciRP=CONVERT(varbinary(max),@SecReciRP)
	--	   ,T1.SecReciLT=CONVERT(varbinary(max),@SecReciLT)
	--	   ,T1.SecReciLI=CONVERT(varbinary(max),@SecReciLI)
	--	   ,T1.SecReciLMF=CONVERT(varbinary(max),@SecReciLMF)
	--	   ,T1.SecReciLRF=CONVERT(varbinary(max),@SecReciLRF)
	--	   ,T1.SecReciLP=CONVERT(varbinary(max),@SecReciLP)
	--	   ,T1.CreatedBy=@UserId
	--	   ,T1.CreatedOn=GETDATE()
	--	FROM [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics T1
	--	WHERE T1.BenePaymentCardId=@BenePaymentCardId
	--END
	--ELSE
	BEGIN
		INSERT INTO [CCTP-MIS_Biometrics].dbo.PaymentCardBiometrics(BenePaymentCardId,PriReciRT,PriReciRI,PriReciRMF,PriReciRRF,PriReciRP,PriReciLT,PriReciLI,PriReciLMF,PriReciLRF,PriReciLP,SecReciRT,SecReciRI,SecReciRMF,SecReciRRF,SecReciRP,SecReciLT,SecReciLI,SecReciLMF,SecReciLRF,SecReciLP,CreatedBy,CreatedOn)
		SELECT @BenePaymentCardId,@PriReciRT,@PriReciRI,@PriReciRMF,@PriReciRRF,@PriReciRP,@PriReciLT,@PriReciLI,@PriReciLMF,@PriReciLRF,@PriReciLP,CONVERT(varbinary(max),@SecReciRT),CONVERT(varbinary(max),@SecReciRI),CONVERT(varbinary(max),@SecReciRMF),CONVERT(varbinary(max),@SecReciRRF),CONVERT(varbinary(max),@SecReciRP),CONVERT(varbinary(max),@SecReciLT),CONVERT(varbinary(max),@SecReciLI),CONVERT(varbinary(max),@SecReciLMF),CONVERT(varbinary(max),@SecReciLRF),CONVERT(varbinary(max),@SecReciLP),@UserId,GETDATE()
	END

	--SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditPaymentCycle]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditPaymentCycle]
    @Id int=NULL,
    @Description varchar(128),
    @FinancialYearId int,
    @FromMonthId int,
    @ToMonthId int,
	@FromMonthActivityDate datetime,
	@ToMonthActivityDate datetime,
    @UserId int
AS
BEGIN
	DECLARE @tblPaymentMonthlyActivity TABLE(
	   MonthId int
	   ,[Year] smallint
	)
    DECLARE @ErrorMsg varchar(128)
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
	DECLARE @CalendarMonthsCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @ACActivityMonthId int
	DECLARE @ACActivityMonthCode varchar(5)
	DECLARE @ACActivityYear int
	DECLARE @LoopVar int
    DECLARE @StatusId int

	SET @CalendarMonthsCode='Calendar Months'
	SET @FinancialYearCode='Financial Year'
	SET @SysCode='System Settings'
	SET @SysDetailCode='CURFINYEAR'
	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Payment Status'
    SET @SysDetailCode='PAYMENTOPEN'
    SELECT @StatusId  =T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ACActivityMonthId=T1.MonthId,@ACActivityMonthCode=T1.Code,@ACActivityYear=T1.Year
	FROM (	
			SELECT TOP 1 T1.PaymentCycleId,T1.[Year],T2.Code,T1.MonthId
			FROM PaymentAccountActivityMonth T1 INNER JOIN SystemCodeDetail T2 ON T1.MonthId=T2.Id
			WHERE DATEDIFF(MM,CONVERT(datetime,'1 '+dbo.fn_MonthName(T2.Code,0)+' '+CONVERT(varchar(4),T1.[Year])),GETDATE())>1
			ORDER BY T1.PaymentCycleId,T1.[Year],T2.Code DESC
		) T1

	SET @LoopVar=0
	WHILE (DATEDIFF(MM,@FromMonthActivityDate,@ToMonthActivityDate)-@LoopVar>=0)
	BEGIN
		INSERT INTO @tblPaymentMonthlyActivity(MonthId,[Year])
		SELECT T1.Id,YEAR(DATEADD(MM,@LoopVar,@FromMonthActivityDate))
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
		WHERE T2.Code=@CalendarMonthsCode AND T1.Code=MONTH(DATEADD(MM,@LoopVar,@FromMonthActivityDate))

		SET @LoopVar=@LoopVar+1
	END

	
	IF @FinancialYearId<>@SystemCodeDetailId1
		SET @ErrorMsg='The FinancialYearId parameter does not match the configured current system financial year'	
	ELSE IF @FromMonthActivityDate IS NULL
		SET @ErrorMsg='Please specify valid FromMonthActivityDate parameter'
	ELSE IF @ToMonthActivityDate IS NULL
		SET @ErrorMsg='Please specify valid ToMonthActivityDate parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentMonthlyActivity)
		SET @ErrorMsg='Please specify valid FromMonthActivityDate and ToMonthActivityDate'
  --  ELSE IF EXISTS(SELECT 1
--					FROM PaymentCycle
--					WHERE StatusId=@StatusId and @Id  is NULL )
	--	SET @ErrorMsg='There is an existing payment cycle whose payment need to be completed'
	--ELSE IF NOT EXISTS(
	--					SELECT 1
	--					FROM BeneAccountMonthlyActivity T1 
	--					WHERE T1.MonthId=@ACActivityMonthId AND T1.[Year]=@ACActivityYear AND T1.StatusId=@SystemCodeDetailId2 
	--					) AND @ACActivityMonthId>0
--		SET @ErrorMsg='Account monthly activity for the period '+dbo.fn_MonthName(@ACActivityMonthCode,0)+','+CONVERT(varchar(4),@ACActivityYear)+' need to have been completely submitted'		

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END
	ELSE
    BEGIN
        BEGIN TRAN
        IF EXISTS(SELECT 1
        FROM PaymentCycle
        WHERE Id=@Id )
			UPDATE PaymentCycle SET [Description] = @Description,FinancialYearId =  @FinancialYearId ,FromMonthId =  @FromMonthId,ToMonthId = @ToMonthId , StatusId =  @StatusId,ModifiedBy = @UserId,ModifiedOn = GETDATE() WHERE Id =  @Id
		ELSE  
            INSERT INTO PaymentCycle
            ( [Description] ,FinancialYearId ,FromMonthId ,ToMonthId ,StatusId,CreatedBy,CreatedOn )
			VALUES
            ( @Description  , @FinancialYearId  , @FromMonthId  , @ToMonthId , @StatusId  , @UserId, GETDATE())

		SELECT @Id=Id FROM PaymentCycle WHERE FinancialYearId=@FinancialYearId AND FromMonthId=@FromMonthId AND ToMonthId=@ToMonthId 

		DELETE FROM PaymentAccountActivityMonth WHERE PaymentCycleId=@Id

		INSERT INTO PaymentAccountActivityMonth(PaymentCycleId,[Year],MonthId)
		SELECT @Id AS PaymentCycleId,T1.[Year],T1.MonthId
		FROM @tblPaymentMonthlyActivity T1

		IF @@ERROR>0
		BEGIN
				ROLLBACK TRAN
				SELECT 0 AS NoOfRows
			END
		ELSE
		BEGIN
            COMMIT TRAN
            SELECT @@ROWCOUNT AS NoOfRows
        END
    END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditPaymentCycleDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditPaymentCycleDetail]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@EnrolmentGroupsXML XML
   ,@FilePath nvarchar(128)
   ,@HasSupportingDoc bit=0
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentEnrolmentGroup TABLE(
		Id int
	   ,EnrolmentGroupId int
	   ,PaymentAmount money
	)

	DECLARE @HHStatus_EnrolledProgCode varchar(20)
	DECLARE @HHStatus_EnrolledPSPCode varchar(20)
	DECLARE @HHStatus_PSPCardedCode varchar(20)
	DECLARE @HHStatus_OnPayrollCode varchar(20)
	DECLARE @HHStatus_OnSuspensionCode varchar(20)
	DECLARE @FinancialYearCode varchar(20)
	DECLARE @CalendarMonthCode varchar(20)
	DECLARE @EnrolmentGroupCode varchar(20)
	DECLARE @PaymentCycleStageCode varchar(20)
	DECLARE @PaymentCycleStageCode_PAYMENTCYCLEAPV varchar(20)
	DECLARE @PaymentCycleStageId int
	DECLARE @PayableHHs int
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @EnrolmentGroups1 varchar(128), @EnrolmentGroups2 varchar(128)
	DECLARE @ErrorMsg varchar(128)



	SET @HHStatus_EnrolledProgCode = 'ENRL'
	SET @HHStatus_EnrolledPSPCode = 'ENRLPSP'
	SET @HHStatus_PSPCardedCode = 'PSPCARDED'
	SET @HHStatus_OnPayrollCode = 'ONPAY'
	SET @HHStatus_OnSuspensionCode = 'SUS'
	SET @EnrolmentGroupCode = 'Enrolment Group'
	SET @PaymentCycleStageCode = 'Payment Stage'
	SET @PaymentCycleStageCode_PAYMENTCYCLEAPV = 'PAYMENTCYCLEAPV'

	INSERT INTO @tblPaymentEnrolmentGroup(Id,EnrolmentGroupId,PaymentAmount)
	SELECT T1.Id,T1.EnrolmentGroupId,PaymentAmount
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(EnrolmentGroupId)[1]','int') AS EnrolmentGroupId
			  ,U.R.value('(PaymentAmount)[1]','money') AS PaymentAmount
		FROM @EnrolmentGroupsXML.nodes('EnrolmentGroups/Record') AS U(R)
	) T1 INNER JOIN SystemCodeDetail T2 ON T1.EnrolmentGroupId=T2.Id
		 INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@EnrolmentGroupCode

	SELECT @EnrolmentGroups1=COALESCE(@EnrolmentGroups1+','+CONVERT(varchar(20),T2.EnrolmentGroupId),@EnrolmentGroups1) FROM PaymentCycleDetail T1 INNER JOIN PaymentEnrolmentGroup T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId ORDER BY T2.EnrolmentGroupId ASC
	SELECT @EnrolmentGroups2=COALESCE(@EnrolmentGroups2+','+CONVERT(varchar(20),EnrolmentGroupId),@EnrolmentGroups1) FROM @tblPaymentEnrolmentGroup ORDER BY EnrolmentGroupId ASC
	SELECT @PaymentCycleStageId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@PaymentCycleStageCode AND T1.Code=@PaymentCycleStageCode_PAYMENTCYCLEAPV
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId
	SELECT @ProgrammeId=Id FROM Programme WHERE Id=@ProgrammeId AND IsActive=1

	IF ISNULL(@ProgrammeId,0)=0
		SET @ErrorMsg='Please specify valid ProgrammeId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentEnrolmentGroup)
		SET @ErrorMsg='Please specify valid EnrolmentGroupsXML parameter'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF @EnrolmentGroups1=@EnrolmentGroups2
		SET @ErrorMsg='The Payment Cycle Detail already exists'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT @PayableHHs=COUNT(T1.Id)
	FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
	WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HHStatus_EnrolledProgCode,@HHStatus_EnrolledPSPCode,@HHStatus_PSPCardedCode,@HHStatus_OnPayrollCode)

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
	BEGIN
		UPDATE T1
		SET T1.ProgrammeId=@ProgrammeId
		   ,T1.EnrolledHHs=@PayableHHs
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
	ELSE
	BEGIN
		INSERT INTO PaymentCycleDetail(PaymentCycleId,ProgrammeId,EnrolledHHs,PaymentStageId,CreatedBy,CreatedOn)
		SELECT @PaymentCycleId,@ProgrammeId,@PayableHHs,@PaymentCycleStageId,@UserId,GETDATE()
	END
	
	DELETE T1
	FROM PaymentEnrolmentGroup T1 
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	INSERT INTO PaymentEnrolmentGroup(PaymentCycleId,ProgrammeId,EnrolmentGroupId,PaymentAmount)
	SELECT @PaymentCycleID,@ProgrammeId,EnrolmentGroupId,PaymentAmount
	FROM @tblPaymentEnrolmentGroup

	--RECORDING THE FILE
	IF @HasSupportingDoc=1 
	BEGIN
		SELECT @FromMonthId=T1.FromMonthId
			  ,@ToMonthId=T1.ToMonthId
			  ,@FinancialYearId=T1.FinancialYearId
		FROM PaymentCycle T1 INNER JOIN PaymentCycleDetail T2 ON T1.Id=T2.PaymentCycleId
		WHERE T2.ProgrammeId=@ProgrammeId

		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @FileName='SUPPORT'+'_'+'PAYMENTCYCLE'+'_'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@FromMonthId)+'-'+(SELECT [Description] FROM SystemCodeDetail WHERE Id=@ToMonthId)+'_'+(SELECT REPLACE([description],'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.FileCreationId=@FileCreationId
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows,T1.PaymentCycleId,T1.ProgrammeId,T2.Id AS PaymentCycleStageId,T2.[Description] AS PaymentCycleStage,@FileName+@FileExtension AS SupportingDoc
		FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditProgrammeOfficer]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditProgrammeOfficer]
	@Id int=NULL
   ,@ProgrammeOfficerUserId int
   ,@CountyId int
   ,@ConstituencyId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Officer Status'
	SET @SysDetailCode='ACTIVEAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@ProgrammeOfficerUserId)
		SET @ErrorMsg='Please specify valid ProgrammeOfficerUserId parameter'
	ELSE IF ISNULL(@CountyId,0)<>0 AND NOT EXISTS(SELECT 1 FROM County WHERE Id=@CountyId)
		SET @ErrorMsg='Please specify valid CountyId parameter'
	ELSE IF ISNULL(@ConstituencyId,0)<>0 AND NOT EXISTS(SELECT 1 FROM Constituency T1 INNER JOIN County T2 ON T1.CountyId=T2.Id WHERE T1.Id=@ConstituencyId AND T2.Id=@CountyId)
		SET @ErrorMsg='Please specify valid ConstituencyId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='Once the Programme Officer location has been approved it cannot be modified. Deactivate and make a new assignment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.UserId=@ProgrammeOfficerUserId
		   ,T1.CountyId=@CountyId
		   ,T1.ConstituencyId=@ConstituencyId
		   ,T1.StatusId=@SystemCodeDetailId1
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ProgrammeOfficer T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO ProgrammeOfficer(UserId,CountyId,ConstituencyId,StatusId,CreatedBy,CreatedOn)
		SELECT @ProgrammeOfficerUserId,@CountyId,@ConstituencyId,@SystemCodeDetailId1,@UserId,GETDATE()
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditReconciliation]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditReconciliation]
	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
   ,@ApplicablePaymentCyclesXML XML
   ,@UserId int
AS
BEGIN
	DECLARE @tblPaymentCycle TABLE(
		Id int
	   ,PaymentCylceId int
	)

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO @tblPaymentCycle(Id,PaymentCylceId)
	SELECT T1.Id,T1.PaymentCycleId
	FROM (
		SELECT U.R.value('(Id)[1]','int') AS Id
			  ,U.R.value('(PaymentCycleId)[1]','int') AS PaymentCycleId
		FROM @ApplicablePaymentCyclesXML.nodes('PaymentCycles/Record') AS U(R)
	) T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id

	SET @Id=ISNULL(@Id,0)

	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF NOT EXISTS(SELECT 1 FROM @tblPaymentCycle) AND EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE FundsRequestOn>=@StartDate)
		SET @ErrorMsg='Please specify valid ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PostPayrollApvOn>=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail
					  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 RIGHT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T1.PaymentCycleId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) in ApplicablePaymentCyclesXML parameter are invalid for the period specified'
	ELSE IF EXISTS(
				SELECT 1
				FROM (
					  SELECT PaymentCycleId
					  FROM PaymentCycleDetail 
					  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
					  GROUP BY PaymentCycleId
					  ) T1 LEFT JOIN @tblPaymentCycle T2 ON T1.PaymentCycleId=T2.PaymentCylceId
				WHERE T2.PaymentCylceId IS NULL
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(
					SELECT 1
					FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
										 INNER JOIN PaymentAccountActivityMonth T3 ON T1.Id=T3.PaymentCycleId
										 LEFT JOIN BeneAccountMonthlyActivity T4 ON T3.MonthId=T4.MonthId AND T3.[Year]=T4.[Year] 
					WHERE ISNULL(T4.StatusId,0)<>@SystemCodeDetailId3
			)
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is missing in the specified ApplicablePaymentCyclesXML parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.StartDate=@StartDate
		   ,T1.EndDate=@EndDate
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM Reconciliation T1
		WHERE T1.Id=@Id

		UPDATE T1
		SET T1.ReconciliationId=NULL
		FROM PaymentCycle T1
		WHERE T1.ReconciliationId=@Id

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END
	ELSE
	BEGIN
		INSERT INTO Reconciliation(StartDate,EndDate,StatusId,CreatedBy,CreatedOn)
		SELECT @StartDate,@EndDate,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @Id=Id FROM Reconciliation WHERE StartDate=@StartDate AND EndDate=@EndDate

		UPDATE T1
		SET T1.ReconciliationId=@Id
		FROM PaymentCycle T1 INNER JOIN @tblPaymentCycle T2 ON T1.Id=T2.PaymentCylceId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditReconciliationDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditReconciliationDetail]
	@ReconciliationId int=NULL
   ,@PSPId int
   ,@OpeningBalance money
   ,@CrFundsRequests money
   ,@CrFundsRequestsStatement money
   ,@CrFundsRequestsDiffNarration varchar(128)
   ,@CrClawBacks money
   ,@CrClawBacksStatement money
   ,@CrClawBacksDiffNarration varchar(128)
   ,@DrPayments money
   ,@DrPaymentsStatement money
   ,@DrPaymentsDiffNarration varchar(128)
   ,@DrCommissions money
   ,@DrCommissionsStatement money
   ,@DrCommissionsDiffNarration varchar(128)
   ,@Balance money
   ,@BalanceStatement money
   ,@BalanceDiffNarration varchar(128)
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)	
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0),@OpeningBalance=ISNULL(@OpeningBalance,0)
		,@CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrFundsRequestsStatement=ISNULL(@CrFundsRequestsStatement,0),@CrFundsRequestsDiffNarration=ISNULL(@CrFundsRequestsDiffNarration,'')
		,@CrClawBacks=ISNULL(@CrClawBacks,0),@CrClawBacksStatement=ISNULL(@CrClawBacksStatement,0),@CrClawBacksDiffNarration=ISNULL(@CrClawBacksDiffNarration,'')
		,@DrPayments=ISNULL(@DrPayments,0),@DrPaymentsStatement=ISNULL(@DrPaymentsStatement,0)	,@DrPaymentsDiffNarration=ISNULL(@DrPaymentsDiffNarration,'')
		,@DrCommissions=ISNULL(@DrCommissions,0),@DrCommissionsStatement=ISNULL(@DrCommissionsStatement,0),@DrCommissionsDiffNarration=ISNULL(@DrCommissionsDiffNarration,'')
		,@Balance=ISNULL(@Balance,0),@BalanceStatement=ISNULL(@BalanceStatement,0),@BalanceDiffNarration=ISNULL(@BalanceDiffNarration,'')

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT PaymentCycleId
								FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PostPayrollApvOn<=T2.EndDate
								GROUP BY PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF (@CrFundsRequests<>@CrFundsRequestsStatement AND @CrFundsRequestsDiffNarration='')
		SET @ErrorMsg='Please specify valid CrFundsRequestsDiffNarration parameter'
	ELSE IF (@CrClawBacks<>@CrClawBacksStatement AND @CrClawBacksDiffNarration='')
		SET @ErrorMsg='Please specify valid CrClawBacksDiffNarration parameter'
	ELSE IF (@DrPayments<>@DrPaymentsStatement AND @DrPaymentsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrPaymentsDiffNarration parameter'
	ELSE IF (@DrCommissions<>@DrCommissionsStatement AND @DrCommissionsDiffNarration='')
		SET @ErrorMsg='Please specify valid DrCommissionsDiffNarration parameter'
	ELSE IF (@Balance<>@BalanceStatement AND @BalanceDiffNarration='')
		SET @ErrorMsg='Please specify valid BalanceDiffNarration parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId)
	BEGIN
		UPDATE T1
		SET T1.OpeningBalance=@OpeningBalance
		   ,T1.CrFundsRequests=@CrFundsRequests
		   ,T1.CrFundsRequestsStatement=@CrFundsRequestsStatement
		   ,T1.CrFundsRequestsDiffNarration=@CrFundsRequestsDiffNarration
		   ,T1.CrClawBacks=@CrClawBacks
		   ,T1.CrClawBacksStatement=@CrClawBacksStatement
		   ,T1.CrClawBacksDiffNarration=@CrClawBacksDiffNarration
		   ,T1.DrPayments=@DrPayments
		   ,T1.DrPaymentsStatement=@DrPaymentsStatement
		   ,T1.DrPaymentsDiffNarration=@DrPaymentsDiffNarration
		   ,T1.DrCommissions=@DrCommissions
		   ,T1.DrCommissionsStatement=@DrCommissionsStatement
		   ,T1.DrCommissionsDiffNarration=@DrCommissionsDiffNarration
		   ,T1.Balance=@Balance
		   ,T1.BalanceStatement=@BalanceStatement
		   ,T1.BalanceDiffNarration=@BalanceDiffNarration
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM ReconciliationDetail T1
		WHERE T1.ReconciliationId=@ReconciliationId AND T1.PSPId=@PSPId

		SELECT @FileCreationId=BankStatementFileId FROM ReconciliationDetail WHERE ReconciliationId=@ReconciliationId AND PSPId=@PSPId
	END
	ELSE
	BEGIN
		SET @SysCode='File Type'
		SET @SysDetailCode='SUPPORT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='UPLOADED'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @FileName='SUPPORT'+'_'+'RECONCILIATION'+'_'+CONVERT(varchar(11),T1.StartDate,106)+'-'+CONVERT(varchar(11),T1.EndDate,106) FROM Reconciliation T1 WHERE Id=@ReconciliationId
		SELECT @FileName=@FileName+'_'+T1.Code FROM PSP T1 WHERE Id=@PSPId
		SET @FileExtension='.pdf'

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		INSERT INTO ReconciliationDetail(ReconciliationId,PSPId,OpeningBalance,CrFundsRequests,CrFundsRequestsStatement,CrFundsRequestsDiffNarration,CrClawBacks,CrClawBacksStatement,CrClawBacksDiffNarration,DrPayments,DrPaymentsStatement,DrPaymentsDiffNarration,DrCommissions,DrCommissionsStatement,DrCommissionsDiffNarration,Balance,BalanceStatement,BalanceDiffNarration,BankStatementFileId,CreatedBy,CreatedOn)
		SELECT @ReconciliationId,@PSPId,@OpeningBalance,@CrFundsRequests,@CrFundsRequestsStatement,@CrFundsRequestsDiffNarration,@CrClawBacks,@CrClawBacksStatement,@CrClawBacksDiffNarration,@DrPayments,@DrPaymentsStatement,@DrPaymentsDiffNarration,@DrCommissions,@DrCommissionsStatement,@DrCommissionsDiffNarration,@Balance,@BalanceStatement,@BalanceDiffNarration,@FileCreationId,@UserId,GETDATE()
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId,@FileCreationId AS FileId,@FileName+@FileExtension AS SupportingDoc
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditSystemCode]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditSystemCode]
	@Id int=NULL
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCode WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCode T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCode(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END

GO
/****** Object:  StoredProcedure [dbo].[AddEditSystemCodeDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AddEditSystemCodeDetail]
	@Id int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(20)=NULL
   ,@DetailCode varchar(20)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=Id
		FROM SystemCode
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SystemCodeId AND Code=@DetailCode AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.Code=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetail T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('SystemCodeDetail')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		--EXEC GetSystemCodeDetails	
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveBeneAccountMonthlyActivity]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CREATIONAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM BeneAccountMonthlyActivity WHERE Id=@Id AND StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified account monthly activity id parameter is not in the approval stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='ACTIVE'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@SystemCodeDetailId1
	FROM BeneAccountMonthlyActivity T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveChangeRequest]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveChangeRequest]
	@Id int,
	@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @NoOfRows int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @HhId int
	DECLARE @PersonId int
	DECLARE @UpdatedPersonId int
	DECLARE @RelationshipIdCode varchar(20)
	DECLARE @RelationshipCode varchar(20)
	DECLARE @Relationship_BeneficiaryCode varchar(20)
	DECLARE @Relationship_CaregiverCode varchar(20)
	DECLARE @MemberRoleCode varchar(20)
	DECLARE @MemberRole_BeneficiaryCode varchar(20)
	DECLARE @MemberRole_CaregiverCode varchar(20)
	DECLARE @MemberRole_MemberCode varchar(20)
	DECLARE @UpdatedSubLocationId int
	DECLARE @GeoMasterId int
	DECLARE @ConstituencyId int
	DECLARE @ChangeTypeId int


	DECLARE @RelationshipId int
	DECLARE @StatusCode varchar(20)

	DECLARE @StatusId int

	DECLARE @MemberStatus int

	IF NOT EXISTS(SELECT 1
	FROM [User]
	WHERE Id=@UserId)
	SET @ErrorMsg='Please specify valid User'

	SET @SysCode='Change Status'
	SET @SysDetailCode='Accepted'
	SELECT @StatusId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON  T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS (SELECT 1
	FROM CHANGE
	where   Id = @Id AND ChangeStatusId = @StatusId )
	SET @ErrorMsg='This Update is not ready for Approval.'


	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END


	SELECT @StatusId = NULL


	SET @MemberRoleCode='Member Role'
	SET @MemberRole_BeneficiaryCode='BENEFICIARY'

	SET @MemberRole_CaregiverCode='CAREGIVER'
	SET @MemberRole_MemberCode='MEMBER'

	SET @RelationshipCode='Relationship'
	SET @Relationship_BeneficiaryCode='BENEFICIARY'
	SET @Relationship_CaregiverCode='CAREGIVER'


	SELECT
		@HhId=T2.HhId,
		@UpdatedSubLocationId= T1.UpdatedSubLocationId,
		@UpdatedPersonId=T1.UpdatedPersonId
	FROM Change T1
		INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
	WHERE T1.Id=@Id

 
	SET @SysCode='UPDATE'
	SET @SysDetailCode='CAREGIVER_CHANGE'


	SELECT @ChangeTypeId=T1.Id
FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 
	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@MemberRoleCode AND T1.Code=@MemberRole_CaregiverCode

		SELECT @RelationshipIdCode=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2
			ON T1.SystemCodeId=T2.Id AND T2.Code=@RelationshipCode AND T1.Code=@Relationship_CaregiverCode

			print @SystemCodeDetailId1
		-- DEACTIVATE PREVIOUS CG
		UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId1 	FROM HouseholdMember T1 
				 WHERE T1.HhId=@HhId AND T1.RelationshipId=@RelationshipId

		IF(ISNULL(@UpdatedPersonId,0)>0)
			BEGIN
			SET @SysCode='Member Status'
			SET @SysDetailCode='1'

			SELECT @SystemCodeDetailId3=T1.Id
			FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

			UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId3, T1.RelationshipId =@SystemCodeDetailId2 	
					FROM HouseholdMember T1 WHERE T1.HhId=@HhId AND T1.PersonId = @UpdatedPersonId

		END
			ELSE
			BEGIN
			INSERT INTO Person
				(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,CreatedBy,CreatedOn)
			SELECT 'ChangeId: '+CONVERT(varchar,T1.Id), T1.UpdatedFirstName, T1.UpdatedMiddleName, T1.UpdatedSurname, T1.UpdatedSexId, T1.UpdatedDoB, T1.UpdatedBirthCertNo, T1.UpdatedNationalIdNo, T1.UpdatedMobileNo1, 0, T1.UpdatedMobileNo2, 0, @UserId, GETDATE()
			FROM Change T1
			WHERE T1.Id=@Id

			SELECT @PersonId=Id
			FROM Person
			WHERE RefId='ChangeId: '+CONVERT(varchar,@Id)

			SET @SysCode='Member Status'
			SET @SysDetailCode='1'
			SELECT @SystemCodeDetailId1=T1.Id
			FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

			INSERT INTO HouseholdMember
				(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
			SELECT @HhId, @PersonId, UpdatedRelationshipId, @SystemCodeDetailId2, @SystemCodeDetailId1, @UserId, GETDATE()
			from Change
			where Id = @Id


		END



		SET @StatusCode='Escalated'

	END






	SET @SysCode='UPDATE'
	SET @SysDetailCode='BENEFICIARY_DEATH'

	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SELECT @HhId=T2.HhId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SET @SysCode='HhStatus'
		SET @SysDetailCode='EX'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1 SET T1.StatusId=@SystemCodeDetailId1 FROM Household T1 WHERE T1.Id=@HhId

		SET @StatusCode='Escalated'
	END


	SET @SysCode='UPDATE'
	SET @SysDetailCode='HH_EXIT'

	
	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SELECT @HhId=T2.HhId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SET @SysCode='HhStatus'
		SET @SysDetailCode='EX'

		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId2=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1 SET T1.StatusId=@SystemCodeDetailId1 FROM Household T1 WHERE T1.Id=@HhId

		SET @StatusCode='Escalated'
	END

	SET @SysCode='UPDATE'
	SET @SysDetailCode='HH_MEMBER_EXIT'

	
	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
			
			BEGIN
		SELECT @HhId=T3.HhId   , @UpdatedPersonId=T1.UpdatedPersonId
		FROM Change T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.ProgrammeId=T2.ProgrammeId INNER JOIN HouseholdEnrolment T3 ON T2.Id=T3.HhEnrolmentPlanId AND T1.HhEnrolmentId=T3.Id
		WHERE T1.Id=@Id

		SET @SysCode='Member Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
				SET T1.StatusId=@SystemCodeDetailId1 FROM HouseholdMember T1 WHERE T1.HhId=@HhId AND T1.PersonId=@UpdatedPersonId


		SET @StatusCode='Approved'
	END



	SET @SysCode='UPDATE'
	SET @SysDetailCode='Relocation'

	
	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SELECT @GeoMasterId=T5.GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN County T5 ON T4.CountyId=T5.Id
		WHERE T1.Id=@UpdatedSubLocationId

		IF EXISTS(SELECT 1
		FROM HouseholdSubLocation
		WHERE Hhid=@HhId AND GeoMasterId=@GeoMasterId)
						UPDATE T1
						SET T1.SubLocationId=@UpdatedSubLocationId
						FROM HouseholdSubLocation T1
						WHERE T1.HhId=@HhId AND T1.GeoMasterId=@GeoMasterId
					ELSE
						INSERT INTO HouseholdSubLocation
			(HhId,GeoMasterId,SubLocationId)
		SELECT @HhId, @GeoMasterId, @UpdatedSubLocationId

		SET @StatusCode='Approved'
	END

	SET @SysCode='UPDATE'
	SET @SysDetailCode='NEW_HH_MEMBER'


	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		INSERT INTO Person
			(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,CreatedBy,CreatedOn)
		SELECT 'ChangeId: '+CONVERT(varchar,T1.Id), T1.UpdatedFirstName, T1.UpdatedMiddleName, T1.UpdatedSurname, T1.UpdatedSexId, T1.UpdatedDoB, T1.UpdatedBirthCertNo, T1.UpdatedNationalIdNo, T1.UpdatedMobileNo1, 0, T1.UpdatedMobileNo2, 0, @UserId, GETDATE()
		FROM Change T1
		WHERE T1.Id=@Id

		SELECT @PersonId=Id
		FROM Person
		WHERE RefId='ChangeId: '+CONVERT(varchar,@Id)

		SET @SysCode='Member Status'
		SET @SysDetailCode='1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO HouseholdMember
			(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
		SELECT @HhId, PersonId, RelationshipId, UpdatedRelationshipId, @SystemCodeDetailId1, @UserId, GETDATE()
		from Change
		where Id = @Id

		SET @StatusCode='Approved'

	END



	SET @SysCode='UPDATE'
	SET @SysDetailCode='HH_CORRECTION'


	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )

		BEGIN
		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		SELECT @GeoMasterId=T5.GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN County T5 ON T4.CountyId=T5.Id
		WHERE T1.Id=@UpdatedSubLocationId

		IF EXISTS(SELECT 1
		FROM HouseholdSubLocation
		WHERE Hhid=@HhId AND GeoMasterId=@GeoMasterId)
						UPDATE T1
						SET T1.SubLocationId=@UpdatedSubLocationId
						FROM HouseholdSubLocation T1
						WHERE T1.HhId=@HhId AND T1.GeoMasterId=@GeoMasterId
					ELSE
						INSERT INTO HouseholdSubLocation
			(HhId,GeoMasterId,SubLocationId)
		SELECT @HhId, @GeoMasterId, @UpdatedSubLocationId

		SET @StatusCode='Approved'
	END

	SET @SysCode='UPDATE'
	SET @SysDetailCode='PSP_CHANGE'

	
	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Account Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		UPDATE T2
			SET T2.StatusId=@SystemCodeDetailId1
			FROM BeneficiaryAccount T2 INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
			WHERE T3.HhId=@HhId AND T2.StatusId<>@SystemCodeDetailId1

		SET @StatusCode='Escalated'
	END

	SET @SysCode='UPDATE'
	SET @SysDetailCode='CARD_REPLACEMENT'


	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		BEGIN
		SET @SysCode='Card Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id

		UPDATE T1 	SET T1.StatusId=@SystemCodeDetailId1 FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id WHERE T3.HhId=@HhId AND T1.StatusId<>@SystemCodeDetailId1
		SET @StatusCode='Escalated'
	END


	SET @SysCode='UPDATE'
	SET @SysDetailCode='BANK_ACC_CHANGE'

	
	SELECT @ChangeTypeId=T1.Id FROM CaseCategory T1 INNER JOIN SystemCodeDetail T2 ON T1.CaseTypeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF EXISTS (SELECT 1
	FROM CHANGE
	WHERE ChangeTypeId = @ChangeTypeId AND Id = @Id )
		
		BEGIN
		SET @SysCode='Account Status'
		SET @SysDetailCode='-1'
		SELECT @SystemCodeDetailId1=T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SELECT @HhId=T2.HhId, @UpdatedSubLocationId= T1.UpdatedSubLocationId
		FROM Change T1 INNER JOIN HouseholdEnrolment T2 ON T2.Id=T1.HhEnrolmentId
		WHERE T1.Id=@Id
		UPDATE T2
			SET T2.StatusId=@SystemCodeDetailId1
			FROM BeneficiaryAccount T2 INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
			WHERE T3.HhId=@HhId AND T2.StatusId<>@SystemCodeDetailId1

		SET @StatusCode='Escalated'
	END




	SET @SysCode='Account Status'
	SET @SysDetailCode='-1'
	
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	IF(ISNULL(@StatusCode,'')<>'')
				BEGIN
		SET @SysCode='Change Status'
		SELECT @StatusId  =T1.Id
		FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@StatusCode
		UPDATE  Change SET 
					 EscalatedBy = CASE WHEN @StatusCode ='Escalated' THEN  @UserId ELSE NULL END,
					 EscalatedOn = CASE WHEN @StatusCode ='Escalated' THEN  GETDATE()ELSE NULL END,
					 ApprovedBy = CASE WHEN @StatusCode ='Approved' THEN  @UserId ELSE NULL END,
					 ApprovedOn = CASE WHEN @StatusCode ='Approved' THEN  GETDATE()ELSE NULL END, 
					 ChangeStatusId = @StatusId WHERE Id = @Id

					 
		SELECT *
		From Change
		where Id = @Id
	END
				ELSE
				BEGIN
		SET @ErrorMsg = 'The Update was not effected. '
		IF ISNULL(@ErrorMsg,'')<>''
					BEGIN
			RAISERROR(@ErrorMsg,16,1)
			RETURN
		END
	END
 
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveDeactivateProgrammeOfficer]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveDeactivateProgrammeOfficer]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Officer Status'
	SET @SysDetailCode='DEAC'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedBy>0)
		SET @ErrorMsg='The specified programme officer id is not ready for deactivation approval'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedApvBy>0)
		SET @ErrorMsg='The specified programme officer id deactivation has already been approved'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId
	   ,T1.DeactivatedApvBy=@UserId
	   ,T1.DeactivatedApvOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveEnrolmentPlan]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
/****** Object:  StoredProcedure [dbo].[ApproveFundsRequest]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveFundsRequest]
	@PaymentCycleId int
   ,@FilePath nvarchar(128)
   ,@UserId int
AS
BEGIN
	DECLARE @FileCreationId int
	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	SELECT @FromMonthId=FromMonthId,@ToMonthId=ToMonthId,@FinancialYearId=FinancialYearId FROM PaymentCycle WHERE Id=@PaymentCycleId
	SET @SysCode='Payment Stage'
	SET @SysDetailCode='FUNDSREQUESTAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The PaymentCycleId appears not to be in the Funds Request Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='File Type'
	SET @SysDetailCode='SUPPORT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='UPLOADED'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @FileName='SUPPORT'+'_'+'FUNDSREQUEST'+'_'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@ToMonthId),1)+'_'+(SELECT REPLACE([description],'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
	SET @FileExtension='.pdf'

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
		INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
		SELECT @FileName+@FileExtension AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,NULL AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

	UPDATE T1
	SET T1.FileCreationId=@FileCreationId
	FROM FundsRequest T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FundsRequestApvBy=@UserId
	   ,T1.FundsRequestApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FileId,NULL AS [FileName],0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @FileCreationId AS FileId,@FileName+@FileExtension AS [FileName],1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApprovePaymentCycleDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApprovePaymentCycleDetail]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLDRAFT'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

 	--LOGGING THE ARREARS
	SET @SysCode='Payment Adjustment'
	SET @SysDetailCode='ARREARS'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO PaymentAdjustment(PaymentCycleId,ProgrammeId,HhId,AdjustmentTypeId,AdjustmentAmount,AmountAdjusted,RefNo,Notes,CreatedBy,CreatedOn)
	SELECT @PaymentCycleId AS PaymentCycleId,T1.ProgrammeId,T1.HhId,@SystemCodeDetailId1 AS AdjustmentTypeId,T1.PaymentAmount,0 AS AmountAdjusted,NULL AS RefNo,dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=T3.FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=T3.ToMonthId),1)+'-'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=T3.FinancialYearId)+' Payment Cycle Arrears' AS Notes,@UserId AS CreatedBy,GETDATE()
	FROM Payroll T1 INNER JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN PaymentCycle T3 ON T1.PaymentCycleId=T3.Id
	WHERE T1.PaymentCycleId=(SELECT MAX(PaymentCycleId) FROM Payroll WHERE PaymentCycleId<@PaymentCycleId AND ProgrammeId=@ProgrammeId) AND ISNULL(T2.WasTrxSuccessful,0)=0
	
	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[ApprovePayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApprovePayroll]
	@PaymentCycleId int
   ,@ProgrammeId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The PaymentCycleId appears not to be in the Funds Request Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PayrollApvBy=@UserId
	   ,T1.PayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApprovePostPayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApprovePostPayroll]
	@PaymentCycleId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='POSTPAYROLLAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The PaymentCycleId appears not to be in the Post Payroll Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId
	
	SET @SysCode='Payment Stage'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM PaymentCycle T1
	WHERE T1.Id=@PaymentCycleId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApprovePrepayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[ApprovePrepayroll]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id AND T2.Code='PREPAYROLLAPV' WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The specified PaymentCycleId appears not to be in the Prepayroll Stage where Prepayroll Approval actioning can be done'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='FUNDSREQUEST'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PaymentStageId=@SystemCodeDetailId1, PrepayrollApvBy=@UserId, PrepayrollApvOn = GETDATE()
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	 UPDATE  PrepayrollDuplicateID  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1
	 UPDATE  PrepayrollIneligible  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1
	 UPDATE  PrepayrollInvalidID  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1
	 UPDATE  PrepayrollInvalidPaymentAccount  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1
	 UPDATE  PrepayrollInvalidPaymentCard  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1
	 UPDATE  PrepayrollSuspicious  SET ActionedApvBy = @UserId, ActionedApvOn = GETDATE() WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND Actioned=1

	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveProgrammeOfficer]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveProgrammeOfficer]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Officer Status'
	SET @SysDetailCode='ACTIVE'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='The specified programme officer id location has already been approved'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId
	   ,T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ApproveReconciliation]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ApproveReconciliation]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified account monthly activity id parameter is not in the approval stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONCLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ApvBy=@UserId
	   ,T1.ApvOn=GETDATE()
	   ,T1.StatusId=@SystemCodeDetailId1
	FROM Reconciliation T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[BeneEnrolFileDownloaded]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BeneEnrolFileDownloaded]
	@FileCreationId int
   ,@FileChecksum varchar(64)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @RowCount int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Id=@FileCreationId AND FileChecksum=@FileChecksum)
		SET @ErrorMsg='Please specify valid FileCreationId corresponding FileCheksum'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId AND T2.UserId=@UserId)
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM FileDownload WHERE FileCreationId=@FileCreationId AND DownloadedBy=@UserId)
			INSERT INTO FileDownload(FileCreationId,FileChecksum,DownloadedBy,DownloadedOn)
			SELECT @FileCreationId,@FileChecksum,@UserId,GETDATE() AS DownloadedOn

		SET @SysCode='Enrolment Status'
		SET @SysDetailCode='PSPENROL'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.StatusId=@SystemCodeDetailId1
		FROM HouseholdEnrolmentPlan T1
		WHERE T1.FileCreationId=@FileCreationId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FilePassword
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT FilePassword FROM FileCreation WHERE Id=@FileCreationId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[BeneEnrolmentStatus]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BeneEnrolmentStatus]
	@NationalIdNo varchar(30)
   ,@UserId int
AS
BEGIN
	DECLARE @BeneAccountId int
	DECLARE @HhEnrolId int
	DECLARE @PSPCode varchar(30)
	DECLARE @PSPUserId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@NationalIdNo,'')=''
		SET @ErrorMsg='Please specify valid NationalIdNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T6.Id,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','ENRLPSP','PSPCARDED','ONPAY'))
							   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id
							   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1 AND T5.PrimaryRecipientId=T3.MemberRoleId
							   INNER JOIN Person T4 ON T3.PersonId=T4.Id 
							   LEFT JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
							   LEFT JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
							   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
							   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	WHERE T4.NationalIdNo=@NationalIdNo 

	IF ISNULL(@BeneAccountId,'')<>''
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
	ELSE IF ISNULL(@PSPUserId,0)>0 AND @PSPUserId<>@UserId
		SET @ErrorMsg=@PSPCode
	ELSE IF ISNULL(@HhEnrolId,0)=0
		SET @ErrorMsg='The beneficiary appears not to have been selected for enrolment into the programme'
	ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@NationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	ELSE IF EXISTS(SELECT 1 FROM [70PlusRegistration] WHERE OP_CONFIRM_ID_NO=@NationalIdNo AND RefId>523449)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	ELSE
		SELECT 0 AS StatusId,'The beneficiary is available for enrolment' AS [Description]	
END	

GO
/****** Object:  StoredProcedure [dbo].[BeneEnrolmentStatus_TEST]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BeneEnrolmentStatus_TEST]
	@NationalIdNo varchar(30)
   ,@UserId int
AS
BEGIN
	DECLARE @BeneAccountId int
	DECLARE @HhEnrolId int
	DECLARE @PSPCode varchar(30)
	DECLARE @PSPUserId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)

	IF ISNULL(@NationalIdNo,'')=''
		SET @ErrorMsg='Please specify valid NationalIdNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T6.Id,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','ENRLPSP','PSPCARDED','ONPAY'))
							   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id
							   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1 AND T5.PrimaryRecipientId=T3.MemberRoleId
							   INNER JOIN Person T4 ON T3.PersonId=T4.Id 
							   LEFT JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
							   LEFT JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
							   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
							   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	WHERE T4.NationalIdNo=@NationalIdNo 

	IF ISNULL(@BeneAccountId,'')<>''
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
	ELSE IF ISNULL(@PSPUserId,0)>0 AND @PSPUserId<>@UserId
		SET @ErrorMsg=@PSPCode
	ELSE IF ISNULL(@HhEnrolId,0)=0
		SET @ErrorMsg='The beneficiary appears not to have been selected for enrolment into the programme'
	ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@NationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
		SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	ELSE
		SELECT 0 AS StatusId,'The beneficiary is available for enrolment' AS [Description]	
END

GO
/****** Object:  StoredProcedure [dbo].[CloseBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[CloseBeneAccountMonthlyActivity]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='SUBMISSIONAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM BeneAccountMonthlyActivity WHERE Id=@Id AND StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='Please specify account monthly activity id parameter is not in the closing stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='CLOSED'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.ClosedBy=@UserId
	   ,T1.ClosedOn=GETDATE()
	   ,T1.StatusId=@SystemCodeDetailId1
	FROM BeneAccountMonthlyActivity T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[DeactivateProgrammeOfficer]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeactivateProgrammeOfficer]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Officer Status'
	SET @SysDetailCode='DEACAPV'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id)
		SET @ErrorMsg='Please specify valid (programme officer) Id parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND ApvBy>0)
		SET @ErrorMsg='The specified programme officer id location is not active'
	ELSE IF EXISTS(SELECT 1 FROM ProgrammeOfficer WHERE Id=@Id AND DeactivatedBy>0)
		SET @ErrorMsg='The specified programme officer id has already been deactivated'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId
	   ,T1.DeactivatedBy=@UserId
	   ,T1.DeactivatedOn=GETDATE()
	FROM ProgrammeOfficer T1
	WHERE T1.Id=@Id
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 1 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeleteBeneAccountMonthlyActivity]
	@Id int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM BeneAccountMonthlyActivityDetail WHERE BeneAccountMonthlyActivityId=@Id)
		SET @ErrorMsg='The specified id has associated account monthly activity records and cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM BeneAccountMonthlyActivity T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteDonor]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeleteDonor]
	@DonorId smallint
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM TargetGroup WHERE DonorId=@DonorId)
		SET @ErrorMsg='The specified Donor has dependant child records'
	--CONSIDER CHECKING IN OTHER DEPENDANT TABLE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM Donor T1
	WHERE T1.DonorId=@DonorId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetDonors
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteEnrolmentPlan]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeleteEnrolmentPlan]
	@Id int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE Id=@Id AND ApvBy IS NOT NULL)
		SET @ErrorMsg='The specified enrolment batch has been approved and cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM HouseholdEnrolmentPlan T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteGrievance]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeleteGrievance]
	@GrievanceId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	DELETE T1
	FROM GrievanceAction T1
	WHERE T1.GrievanceId=@GrievanceId;

	DELETE T1
	FROM Grievance T1
	WHERE T1.GrievanceId=@GrievanceId

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetGrievances		
END

GO
/****** Object:  StoredProcedure [dbo].[DeletePaymentCycle]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeletePaymentCycle]
	@Id int
   ,@UserId int
AS
BEGIN
	DECLARE @PaymentCycleStatusCode_PREPAYROLL varchar(20)
	DECLARE @PaymentCycleStatusCode_PREPAYROLLAPV varchar(20)
	DECLARE @ErrorMsg varchar(128)

	SET @PaymentCycleStatusCode_PREPAYROLL='PREPAYROLL'
	SET @PaymentCycleStatusCode_PREPAYROLLAPV='PREPAYROLLAPV'
	SELECT @Id=Id FROM PaymentCycle WHERE Id=@Id
	SELECT @UserId=Id FROM [User] WHERE Id=@UserId

	IF ISNULL(@Id,0)=0
		SET @ErrorMsg='The specified Payment Cycle does not exist'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id WHERE T1.Id=@Id AND T2.Code IN(@PaymentCycleStatusCode_PREPAYROLL,@PaymentCycleStatusCode_PREPAYROLLAPV))
		SET @ErrorMsg='Once the Payment Cycle Funds have been requested it cannot be deleted'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	UPDATE T1
	SET T1.DeletedBy=@UserId
	   ,T1.DeletedOn=GETDATE() 
	FROM PaymentCycle T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
	
	EXEC GetPaymentCycles
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteSystemCodeDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DeleteSystemCodeDetail]
	@Id int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	--IF EXISTS(SELECT 1 FROM Household T1 WHERE T1.PSPBranchId=@PSPBranchId)
	--	SET @ErrorMsg='The specified PSPBranch has dependant child records'
	--CONSIDER CHECKING DEPENDANT TABLES BASED ON SYSTEM CODE

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE T1
	FROM SystemCodeDetail T1
	WHERE T1.Id=@Id

	SELECT @@ROWCOUNT AS NoOfRows
	
	--EXEC GetSystemCodeDetails
END

GO
/****** Object:  StoredProcedure [dbo].[ELMAH_GetErrorsXml]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[ELMAH_GetErrorsXml]
    (
    @Application NVARCHAR(60),
    @PageIndex INT = 0,
    @PageSize INT = 15,
    @TotalCount INT OUTPUT
)
AS

SET NOCOUNT ON

DECLARE @FirstTimeUTC DATETIME
DECLARE @FirstSequence INT
DECLARE @StartRow INT
DECLARE @StartRowIndex INT

SELECT
@TotalCount = COUNT(1)
FROM
[ELMAH_Error]
WHERE
[Application] = @Application

--Get the ID of the first error for the requested page

    SET @StartRowIndex = @PageIndex * @PageSize + 1

IF @StartRowIndex <= @TotalCount
BEGIN

SET ROWCOUNT @StartRowIndex

SELECT
@FirstTimeUTC =[TimeUtc],
            @FirstSequence =[Sequence]
FROM
[ELMAH_Error]
WHERE
[Application] = @Application
ORDER BY
[TimeUtc] DESC,
    [Sequence] DESC

END
ELSE
BEGIN

SET @PageSize = 0

END

--Now set the row count to the requested page size and get
--all records below it for the pertaining application.

    SET ROWCOUNT @PageSize

SELECT
errorId = [ErrorId],
    application = [Application],
    host = [Host],
    type = [Type],
    source = [Source],
    message = [Message],
    [user] = [User],
    statusCode = [StatusCode],
    time = CONVERT(VARCHAR(50), [TimeUtc], 126) + 'Z'
FROM
[ELMAH_Error] error
WHERE
[Application] = @Application
AND
[TimeUtc] <= @FirstTimeUTC
AND
[Sequence] <= @FirstSequence
ORDER BY
[TimeUtc] DESC,
    [Sequence] DESC
FOR
XML AUTO


GO
/****** Object:  StoredProcedure [dbo].[ELMAH_GetErrorXml]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[ELMAH_GetErrorXml]
    (
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

SET NOCOUNT ON

SELECT
[AllXml]
FROM
[ELMAH_Error]
WHERE
[ErrorId] = @ErrorId
AND
[Application] = @Application


GO
/****** Object:  StoredProcedure [dbo].[ELMAH_LogError]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[dbo].[ELMAH_LogError]
    (
    @ErrorId UNIQUEIDENTIFIER,
    @Application NVARCHAR(60),
    @Host NVARCHAR(30),
    @Type NVARCHAR(100),
    @Source NVARCHAR(60),
    @Message NVARCHAR(500),
    @User NVARCHAR(50),
    @AllXml NTEXT,
    @StatusCode INT,
    @TimeUtc DATETIME
)
AS

SET NOCOUNT ON

INSERT
INTO
[ELMAH_Error]
    (
    [ErrorId],
    [Application],
    [Host],
    [Type],
    [Source],
    [Message],
    [User],
    [AllXml],
    [StatusCode],
    [TimeUtc]
    )
VALUES
    (
            @ErrorId,
@Application,
@Host,
@Type,
@Source,
@Message,
@User,
@AllXml,
@StatusCode,
@TimeUtc
        )


GO
/****** Object:  StoredProcedure [dbo].[FinalizePrepayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[FinalizePrepayroll]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN SystemCodeDetail T2 ON T1.PaymentStageId=T2.Id AND T2.Code='PREPAYROLLFINAL' WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The specified PaymentCycleId appears not to be in the Prepayroll Stage where Prepayroll Exceptions actioning can be done'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PREPAYROLLAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	
	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[FinalizeReconciliationDetail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[FinalizeReconciliationDetail]
	@ReconciliationId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows finalization'
	ELSE IF EXISTS( SELECT 1
					FROM (
							SELECT PaymentCycleId
							FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PostPayrollApvOn<=T2.EndDate
							GROUP BY PaymentCycleId
						) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
								LEFT JOIN ReconciliationDetail T4 ON T4.ReconciliationId=@ReconciliationId AND T3.PSPId=T4.PSPId
					WHERE T4.PSPId IS NULL
					)
		SET @ErrorMsg='One or more PSPs Reconciliation detail are missing for the specified reconciliation'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM Reconciliation T1
	WHERE T1.Id=@ReconciliationId
	
	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[FlagFilesToShare]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[FlagFilesToShare]
	@FileTypeCode varchar(20)
AS
BEGIN
	DECLARE @FileType_PAYMENT varchar(20)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int

	UPDATE T1
	SET T1.IsShared=1
	FROM FileCreation T1 INNER JOIN SystemCodeDetail T2 ON T1.TypeId=T2.Id
	WHERE T2.Code=@FileTypeCode AND ISNULL(T1.IsShared,0)=0

	SET @FileType_PAYMENT='PAYMENT'
	BEGIN
		SET @SysCode='Payment Stage'
		SET @SysDetailCode='PAYROLLEX'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysDetailCode='PAYROLLEXCONF'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId2
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentStageId=@SystemCodeDetailId1
	END

	SELECT @@ROWCOUNT AS NoOfRows
END

GO
/****** Object:  StoredProcedure [dbo].[GenerateBulkTransferFile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GenerateBulkTransferFile]
	@FilePath nvarchar(128),
	@DBServer varchar(30),
	@DBName varchar(30),
	@DBUser varchar(30),
	@DBPassword nvarchar(30),
	@Id int,
	@UserId int
AS

BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @Len tinyint
	DECLARE @Prefix varchar(10)
	DECLARE @ProgNo int
	DECLARE @HhId int
	DECLARE @LoopVar int


	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)

	DECLARE @FileId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults
	(
		FileExists int
	   ,
		FileIsDirectory int
	   ,
		FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists, @FileIsDirectory=FileIsDirectory, @FileParentDirExists=FileParentDirExists
	FROM #FileResults

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1
	FROM [User]
	WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END



	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


	DELETE FROM  temp_BulkTransferFile

	INSERT INTO temp_BulkTransferFile
		(BulkTransferId,ChangeType,ProgrammeId,ProgrammeCode,ProgrammeName,PriReciFirstName,PriReciMiddleName,PriReciSurname,PriReciSex,PriReciNationalIdNo,PriReciMobileNo1,PriReciMobileNo2,SecReciFirstName,SecReciMiddleName,SecReciSurname,SecReciSexId,SecReciSex,SecReciNationalIdNo,SecReciMobileNo1,SecReciMobileNo2,AccountNo,AccountName,PSPBranch,PSP,AccountOpenedOn,SubLocationName,LocationName,DivisionName,DistrictName,CountyName,ConstituencyName)
	SELECT BTD.BulkTransferId, CT.[Description] AS ChangeType, T2.Id AS ProgrammeId, T2.Code AS ProgrammeCode, T2.Name AS ProgrammeName  , T5.FirstName AS PriReciFirstName, T5.MiddleName AS PriReciMiddleName, T5.Surname AS PriReciSurname,
		T10.Code AS PriReciSex, T5.NationalIdNo AS PriReciNationalIdNo,
		T5.MobileNo1 AS PriReciMobileNo1, T5.MobileNo2 AS PriReciMobileNo2  
  , T6.FirstName AS SecReciFirstName, T6.MiddleName AS SecReciMiddleName, T6.Surname AS SecReciSurname,
		T6.SexId AS SecReciSexId, T11.Code AS SecReciSex, T6.NationalIdNo AS SecReciNationalIdNo,
		T6.MobileNo1 AS SecReciMobileNo1, T6.MobileNo2 AS SecReciMobileNo2  
	, T7.AccountNo, T7.AccountName, T7.PSPBranch, T7.PSP, T7.OpenedOn AS AccountOpenedOn  
, T9.SubLocationName, T9.LocationName, T9.DivisionName, T9.DistrictName, T9.CountyName, T9.ConstituencyName
	FROM Household T1

		INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
		INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId
		INNER JOIN Change C ON T3.Id = C.HhEnrolmentId
		INNER JOIN BulkTransferDetail BTD ON C.Id = BTD.ChangeId
		INNER JOIN SystemCodeDetail CT ON C.ChangeTypeId = CT.Id
		INNER JOIN HouseholdMember T4 ON T1.Id=T4.HhId
		INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId --AND T4.StatusId=@SystemCodeDetailId1
		LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId --AND T4.StatusId=@SystemCodeDetailId1

		INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId
		INNER JOIN (  
		   SELECT T1.Id AS SubLocationId, T1.Name AS SubLocationName, T2.Id AS LocationId, T2.Name AS LocationName, T3.Id AS DivisionId, T3.Name AS DivisionName, T5.Id AS DistrictId, T5.Name AS DistrictName, T6.Id AS CountyId, T6.Name AS CountyName, T7.Id AS ConstituencyId, T7.Name AS ConstituencyName, T8.Id AS GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1  
		  ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId
		INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id
		LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id
		LEFT JOIN (  
		   SELECT DISTINCT T1.HhEnrolmentId, T1.Id AS BeneAccountId, T1.AccountNo, T1.AccountName, T3.Name AS PSPBranch, T4.Name AS PSP, T1.OpenedOn
		FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
			INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id
			INNER JOIN PSP T4 ON T3.PSPId=T4.Id
		WHERE  T1.StatusId=@SystemCodeDetailId2  
		  ) T7 ON T3.Id=T7.HhEnrolmentId
	where BTD.BulkTransferId = @Id

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SELECT @FileName='BULK_TRANSFER_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_BulkTransferFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SysCode='File Type'
	SET @SysDetailCode='ENROLMENT'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation
		(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, @FilePassword AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

	SET @FileId=IDENT_CURRENT('FileCreation')

	SET @SysCode='Change Status'
	SET @SysDetailCode='Bulk Transfer'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1 SET T1.ChangeStatusId = @SystemCodeDetailId2  FROM Change T1
		INNER JOIN BulkTransferDetail T2 ON T1.Id = T2.BulkTransferId AND T2.BulkTransferId = @Id
	UPDATE T1 SET T1.FileId = @FileId ,ApvBy = @UserId, ApvOn =GETDATE() FROM BulkTransfer T1 WHERE T1.Id=@Id
	
 
	SELECT @FileId AS FileCreationId
END

GO
/****** Object:  StoredProcedure [dbo].[GenerateEnrolmentFile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GenerateEnrolmentFile]
	@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS

BEGIN

	SET NOCOUNT ON



	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath
	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults
	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	DROP TABLE #FileResults
	IF OBJECT_ID('temp_EnrolmentFile') IS NOT NULL	DROP TABLE temp_EnrolmentFile;
	CREATE TABLE temp_EnrolmentFile(
		EnrolmentNo int
	   ,Programme varchar(50)
	   ,ProgrammeNo varchar(50)
	   ,BeneFirstName varchar(50)
	   ,BeneMiddleName varchar(50)
	   ,BeneSurname varchar(50)
	   ,BeneIDNo varchar(30)
	   ,BeneSex varchar(20)
	   ,BeneDoB datetime
	   ,CGFirstName varchar(50)
	   ,CGMiddleName varchar(50)
	   ,CGSurname varchar(50)
	   ,CGIDNo varchar(30)
	   ,CGSex varchar(20)
	   ,CGDoB datetime
	   ,MobileNo1 varchar(20)
	   ,MobileNo2 varchar(20)
	   ,County varchar(30)
	   ,Constituency varchar(30)
	   ,District varchar(30)
	   ,Division varchar(30)
	   ,Location varchar(30)
	   ,SubLocation varchar(30)
	   );
	INSERT INTO temp_EnrolmentFile(EnrolmentNo,Programme,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneIDNo,BeneSex,BeneDoB,CGFirstName,CGMiddleName,CGSurname,CGIDNo,CGSex,CGDoB,MobileNo1,MobileNo2,County,Constituency,District,Division,Location,SubLocation)

	SELECT T2.Id AS EnrolmentNo
		,T4.Code AS Programme
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!

	WHERE T1.StatusId=@SystemCodeDetailId1 or T3.TokenId>636167

	IF NOT EXISTS(SELECT 1 FROM temp_EnrolmentFile)
		SET @ErrorMsg='There is no beneficiary selection approved for enrolment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN

		RAISERROR(@ErrorMsg,16,1)

		RETURN

	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;



	SET @FileName='ENROLMENT_'



	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END

	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END

	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))

	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) 
	END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END

	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time

	SET @FilePathName=@FilePath+@FileName

	SET @FileExtension='.csv'

	SET @FileCompression='.rar'



	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_EnrolmentFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'

	--SELECT @SQLStmt

	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension

	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;



	DROP TABLE temp_EnrolmentFile;

	

	--RECORDING THE FILE

	SET @SysCode='File Type'

	SET @SysDetailCode='ENROLMENT'

	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	SET @SysCode='File Creation Type'

	SET @SysDetailCode='SYSGEN'

	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,CreatedBy,CreatedOn)

	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS IsShared,@UserId AS CreatedBy,GETDATE() AS CreatedOn



	SET @FileCreationId=IDENT_CURRENT('FileCreation')



	SET @SysCode='Enrolment Status'

	SET @SysDetailCode='PROGSHAREPSP'

	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	SET @SysDetailCode='PSPRECEIPT'

	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



	UPDATE T1

	SET T1.StatusId=@SystemCodeDetailId2

	   ,T1.FileCreationId=@FileCreationId

	FROM HouseholdEnrolmentPlan T1

	WHERE T1.StatusId=@SystemCodeDetailId1



	SELECT @FileCreationId AS FileCreationId

	SET NOCOUNT OFF

END



 

GO
/****** Object:  StoredProcedure [dbo].[GeneratePayrollFile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GeneratePayrollFile]
	@PaymentCycleId int
   ,@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @FromMonthId int
	DECLARE @ToMonthId int
	DECLARE @FinancialYearId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @LoopVar int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults
	SELECT @FromMonthId=FromMonthId,@ToMonthId=ToMonthId,@FinancialYearId=FinancialYearId FROM PaymentCycle WHERE Id=@PaymentCycleId

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEX'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not ready for generation'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END	   

	DECLARE @PSPs TABLE(
		RowId int NOT NULL IDENTITY(1,1)
	   ,PSPId int NOT NULL
	   ,PSPCode nvarchar(20) NOT NULL
	   ,FileCreationId int NULL
	   ,UserId int NOT NULL
	)

	INSERT INTO temp_PayrollFile(PaymentCycleId,PaymentCycle,EnrolmentNo,ProgrammeNo,BankId,BankCode,BankName,BranchCode,BranchName,AccountNo,AccountName,Amount,BeneficiaryIDNo,BeneficiaryContact,CaregiverIDNo,CaregiverContact,SubLocation,Location,Division,District,County,Constituency)
	SELECT T1.PaymentCycleId,T2.[Description] AS PaymentCycle,T7.Id AS EnrolmentNo,T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T10.Id AS PSPId,T10.Code AS PSPCode,T10.Name AS PSPName,T9.Code AS PSPBranchCode,T9.Name AS PSPBranchName,T8.AccountNo,T8.AccountName,T1.PaymentAmount AS Amount,T4.NationalIDNo AS BeneficiaryIDNo,T4.MobileNo1 AS BeneficiaryContact,T5.NationalIDNo AS CaregiverIDNo,T5.MobileNo1 AS CaregiverContact,T14.SubLocation,T14.Location,T14.Division,T14.District,T14.County,T14.Constituency
	FROM Payroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN Person T4 ON T3.BenePersonId=T4.Id
					LEFT JOIN Person T5 ON T3.CGPersonId=T5.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
					INNER JOIN BeneficiaryAccount T8 ON T3.BeneAccountId=T8.Id
					INNER JOIN PSPBranch T9 ON T8.PSPBranchId=T9.Id
					INNER JOIN PSP T10 ON T9.PSPId=T10.Id
					INNER JOIN Household T11 ON T1.HhId=T11.Id
					INNER JOIN HouseholdSubLocation T12 ON T11.Id=T12.HhId
					INNER JOIN GeoMaster T13 ON T12.GeoMasterId=T13.Id AND T13.IsDefault=1
					INNER JOIN (
									SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency
									FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
														INNER JOIN Division T7 ON T2.DivisionId=T7.Id
														INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
														INNER JOIN District T9 ON T8.DistrictId=T9.Id
														INNER JOIN County T10 ON T8.CountyId=T10.Id
														INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
														INNER JOIN GeoMaster T12 ON T10.GeoMasterId=T10.GeoMasterId AND T12.IsDefault=1
								) T14 ON T12.SubLocationId=T14.SubLocationId
	WHERE T1.PaymentCycleId=@PaymentCycleId

	IF NOT EXISTS(SELECT 1 FROM temp_PayrollFile)
		SET @ErrorMsg='There are no beneficiaries valid for payment'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)

		RETURN
	END
	INSERT INTO @PSPs(PSPId,PSPCode,UserId)
	SELECT DISTINCT T1.BankId,T1.BankCode,T2.UserId FROM temp_PayrollFile T1 INNER JOIN PSP T2 ON T1.BankId=T2.Id

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @LoopVar=1
	WHILE EXISTS(SELECT 1 FROM @PSPs WHERE RowId=@LoopVar)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SET @FileName='PAYROLL_'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@ToMonthId),1)+'-'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
		SELECT @FileName=@FileName+'_'+PSPCode FROM @PSPs WHERE RowId=@LoopVar
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName

		TRUNCATE TABLE temp_PSPPayrollFile;
		INSERT INTO temp_PSPPayrollFile(PaymentCycleId,EnrolmentNo,BankCode,BranchCode,AccountNo,AccountName,Amount,BeneficiaryIDNo,CaregiverIDNo)
		SELECT T1.PaymentCycleId,EnrolmentNo,BankCode,BranchCode,AccountNo,AccountName,Amount,BeneficiaryIDNo,CaregiverIDNo
		FROM temp_PayrollFile T1 INNER JOIN @PSPs T2 ON T1.BankCode=T2.PSPCode
		WHERE T2.RowId=@LoopVar

		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_PSPPayrollFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='PAYMENT'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,TargetUserId,CreatedBy,CreatedOn)
		SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS Isshared,T1.UserID AS TargetUserID,@UserId AS CreatedBy,GETDATE() AS CreatedOn FROM @PSPS T1 WHERE T1.RowId=@LoopVar

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.FileCreationId=@FileCreationId
		FROM @PSPs T1
		WHERE RowId=@LoopVar

		SET @LoopVar=@LoopVar+1
	END

	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SET @FileName='MOBILIZATION_'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code FROM SystemCodeDetail WHERE Id=@ToMonthId),1)+'-'+(SELECT REPLACE(Code,'/','') FROM SystemCodeDetail WHERE Id=@FinancialYearId)
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM vw_temp_PayrollMobilization" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	--SELECT @SQLStmt
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
	--RECORDING THE FILE
	SET @SysCode='File Type'
	SET @SysDetailCode='PAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,IsShared,TargetUserId,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,0 AS Isshared,NULL AS TargetUserID,@UserId AS CreatedBy,GETDATE() AS CreatedOn

	SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

	TRUNCATE TABLE temp_PayrollFile;
	TRUNCATE TABLE temp_PSPPayrollFile;

	SELECT FileCreationId FROM @PSPs
	UNION
	SELECT @FileCreationId

	SET NOCOUNT OFF
END

GO
/****** Object:  StoredProcedure [dbo].[GeneratePostpayrollExceptionsFile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GeneratePostpayrollExceptionsFile]
	@PaymentCycleId int
   ,@FilePath nvarchar(256)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='POSTPAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The payment cycle appears not to be in the post payroll stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
	   
	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId;

	INSERT INTO temp_Exceptions(PaymentCycleId,PaymentCycle,ProgrammeId,Programme,EnrolmentNo,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBene,IsIneligibleBene,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCG,IsIneligibleCG,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZone,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount,WasTrxSuccessful,TrxNarration)
	SELECT T1.PaymentCycleId,T2.[Description] AS PaymentCycle,T1.ProgrammeId,T3.Code AS Programme,TT4.Id AS EnrolmentNo,TT4.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),TT4.ProgrammeNo)))+CONVERT(varchar(6),TT4.ProgrammeNo) AS ProgrammeNo,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneDoB,T4.Code AS BeneSex,T1.BeneNationalIDNo,T1.PriReciCanReceivePayment,CONVERT(bit,ISNULL(T7.PersonId,0)) AS IsInvalidBene,CONVERT(bit,ISNULL(T8.PersonId,0)) AS IsDuplicateBene,CONVERT(bit,ISNULL(T9.HhId,0)) AS IsIneligibleBene,T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,T1.CGDoB,T5.Code AS CGSex,T1.CGNationalIDNo,CONVERT(bit,ISNULL(T11.PersonId,0)) AS IsInvalidCG,CONVERT(bit,ISNULL(T12.PersonId,0)) AS IsDuplicateCG,CONVERT(bit,ISNULL(T13.HhId,0)) AS IsIneligibleCG,T6.Code AS HhStatus,CONVERT(bit,ISNULL(T19.HhId,0)) AS IsIneligibleHh,TT15.AccountNo AS AccountNumber,CONVERT(bit,ISNULL(T15.HhId,0)) AS IsInvalidAccount,CONVERT(bit,ISNULL(T17.HhId,0)) AS IsDormantAccount,TT16.PaymentCardNo AS PaymentCardNumber,CONVERT(bit,ISNULL(T16.HhId,0)) AS IsInvalidPaymentCard,T21.Name AS PaymentZone,T1.PaymentZoneCommAmt,T1.ConseAccInactivity,T1.EntitlementAmount,T1.AdjustmentAmount,CONVERT(bit,ISNULL(T18.HhId,0)) AS IsSuspiciousAmount,T22.WasTrxSuccessful,T22.TrxNarration
	FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
					   INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					   INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
					   INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
					   LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
					   INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
					   LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
					   LEFT JOIN PrepayrollDuplicateID T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
					   LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId
					   LEFT JOIN PrepayrollDuplicateID T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId					   			   
					   LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
					   LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
					   LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.BenePaymentCardId=TT16.Id
					   LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId					   			   
					   LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
					   INNER JOIN Payment T22 ON T1.PaymentCycleId=T22.PaymentCycleId AND T1.ProgrammeId=T22.ProgrammeId AND T1.HhId=T22.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId

	DELETE 
	FROM temp_Exceptions 
	WHERE PaymentCycleId=@PaymentCycleId
		AND NOT (IsInvalidBene=1 OR IsDuplicateBene=1 OR IsIneligibleBene=1 OR IsInvalidCG=1 OR IsDuplicateCG=1 OR IsIneligibleCG=1	OR IsIneligibleHh=1 OR IsInvalidAccount=1 OR IsDormantAccount=1 OR IsInvalidPaymentCard=1 OR IsSuspiciousAmount=1 OR WasTrxSuccessful=1)
	
	IF EXISTS(SELECT 1 FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SET @FileName='EXCEPTIONS_POSTPAYROLL_'

		SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
		SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
		SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
		SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName
		SET @FileExtension='.csv'
		SET @FileCompression='.rar'


		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM vw_temp_PostpayrollExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='EXCEPTION'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.ExceptionsFileId=@FileCreationId
		FROM PaymentCycle T1
		WHERE T1.Id=@PaymentCycleId

		SET @SysCode='Payment Stage'
		SET @SysDetailCode='POSTPAYROLLAPV'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId1
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId
	END

	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId;

	SELECT ISNULL(@FileCreationId,0) AS FileCreationId
	SET NOCOUNT OFF
END

GO
/****** Object:  StoredProcedure [dbo].[GeneratePrepayrollExceptionsFile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GeneratePrepayrollExceptionsFile]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
   ,@FilePath nvarchar(128)
   ,@DBServer varchar(30)
   ,@DBName varchar(30)
   ,@DBUser varchar(30)
   ,@DBPassword nvarchar(30)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	IF OBJECT_ID(N'tempdb.dbo.#FileResults') IS NOT NULL	DROP TABLE #FileResults;
	CREATE TABLE #FileResults(
		FileExists int
	   ,FileIsDirectory int
	   ,FileParentDirExists int
	);

	INSERT INTO #FileResults
	EXEC Master.dbo.xp_fileexist @FilePath

	SELECT @FileExists=FileExists,@FileIsDirectory=FileIsDirectory,@FileParentDirExists=FileParentDirExists FROM #FileResults

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYMENTCYCLEAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF @FileExists=1 OR @FileParentDirExists=0
		SET @ErrorMsg='Please specify valid FilePath parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId IN(@SystemCodeDetailId1))
	   OR EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND (PrePayrollBy IS NULL OR PrePayrollOn IS NULL))
		SET @ErrorMsg='The prepayroll seems not to have been executed on the specified payment cycle'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN FileCreation T2 ON T1.ExceptionsFileId=T2.Id WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId)
		SET @ErrorMsg='The prepayroll exceptions file seem to already have been generated'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	DROP TABLE #FileResults

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
	   
	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	INSERT INTO temp_Exceptions(PaymentCycleId,PaymentCycle,ProgrammeId,Programme,EnrolmentNo,ProgrammeNo,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSex,BeneNationalIDNo,PriReciCanReceivePayment,IsInvalidBene,IsDuplicateBene,IsIneligibleBene,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSex,CGNationalIDNo,IsInvalidCG,IsDuplicateCG,IsIneligibleCG,HhStatus,IsIneligibleHh,AccountNumber,IsInvalidAccount,IsDormantAccount,PaymentCardNumber,IsInvalidPaymentCard,PaymentZone,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount,IsSuspiciousAmount,SubLocation,Location,Division,District,County,Constituency)
	SELECT T1.PaymentCycleId,T2.[Description] AS PaymentCycle,T1.ProgrammeId,T3.Name AS Programme,TT4.Id AS EnrolmentNo,TT4.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),TT4.ProgrammeNo)))+CONVERT(varchar(6),TT4.ProgrammeNo) AS ProgrammeNo
		,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneDoB,T4.Code AS BeneSex,T1.BeneNationalIDNo,T1.PriReciCanReceivePayment,CASE WHEN(ISNULL(T7.ActionedApvBy,0))>0 THEN -1 ELSE CONVERT(bit,ISNULL(T7.PersonId,0)) END AS IsInvalidBene,CASE WHEN(ISNULL(T8.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8.PersonId,0)) END AS IsDuplicateBene,CASE WHEN(ISNULL(T9.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T9.HhId,0)) END AS IsIneligibleBene
		,T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,T1.CGDoB,T5.Code AS CGSex,T1.CGNationalIDNo,CASE WHEN(ISNULL(T11.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T11.PersonId,0)) END AS IsInvalidCG,CASE WHEN(ISNULL(T12.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12.PersonId,0)) END AS IsDuplicateCG,CASE WHEN(ISNULL(T13.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T13.HhId,0)) END AS IsIneligibleCG
		,T6.[Description] AS HhStatus,CASE WHEN(ISNULL(T19.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T19.HhId,0)) END AS IsIneligibleHh,TT15.AccountNo AS AccountNumber,CASE WHEN(ISNULL(T15.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T15.HhId,0)) END AS IsInvalidAccount,CASE WHEN(ISNULL(T17.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T17.HhId,0)) END AS IsDormantAccount,TT16.PaymentCardNo AS PaymentCardNumber,CASE WHEN(ISNULL(T16.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T16.HhId,0)) END AS IsInvalidPaymentCard,T21.Name AS PaymentZone,T1.PaymentZoneCommAmt,T1.ConseAccInactivity,T1.EntitlementAmount,T1.AdjustmentAmount,CASE WHEN(ISNULL(T18.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T18.HhId,0)) END AS IsSuspiciousAmount,T25.SubLocation,T25.Location,T25.Division,T25.District,T25.County,T25.Constituency
	FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
					   INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					   INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
					   INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
					   LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
					   INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
					   LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
					   LEFT JOIN PrepayrollDuplicateID T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
					   LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId
					   LEFT JOIN PrepayrollDuplicateID T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId					   			   
					   LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
					   LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
					   LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.BenePaymentCardId=TT16.Id
					   LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId					   			   
					   LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
					   INNER JOIN Household T22 ON T1.HhId=T22.Id
					   INNER JOIN HouseholdSubLocation T23 ON T22.Id=T23.HhId
					   INNER JOIN GeoMaster T24 ON T23.GeoMasterId=T24.Id AND T24.IsDefault=1
					   INNER JOIN (
									SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency
									FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
														INNER JOIN Division T7 ON T2.DivisionId=T7.Id
														INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
														INNER JOIN District T9 ON T8.DistrictId=T9.Id
														INNER JOIN County T10 ON T8.CountyId=T10.Id
														INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
								) T25 ON T23.SubLocationId=T25.SubLocationId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	DELETE 
	FROM temp_Exceptions 
	WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
		AND NOT (IsInvalidBene=1 OR IsDuplicateBene=1 OR IsIneligibleBene=1 OR IsInvalidCG=1 OR IsDuplicateCG=1 OR IsIneligibleCG=1	OR IsIneligibleHh=1 OR IsInvalidAccount=1 OR IsDormantAccount=1 OR IsInvalidPaymentCard=1 OR IsSuspiciousAmount=1)
	
	IF EXISTS(SELECT 1 FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId)
	BEGIN
		EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

		SELECT @FileName='EXCEPTIONS_PREPAYROLL_'+Code+'_' FROM Programme WHERE Id=@ProgrammeId

		SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
		SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
		SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
		SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
		SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
		SET @FilePathName=@FilePath+@FileName
		SET @FileExtension='.csv'
		SET @FileCompression='.rar'


		SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM vw_temp_PrepayrollExceptions" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
		--SELECT @SQLStmt
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
		SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
		EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	
		--RECORDING THE FILE
		SET @SysCode='File Type'
		SET @SysDetailCode='EXCEPTION'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='File Creation Type'
		SET @SysDetailCode='SYSGEN'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
			SELECT @FileName+@FileCompression AS Name,@SystemCodeDetailId1 AS TypeId,@SystemCodeDetailId2 AS CreationTypeId,@FilePath,NULL AS Checksum,@FilePassword AS FilePassword,@UserId AS CreatedBy,GETDATE() AS CreatedOn

		SELECT @FileCreationId=Id FROM FileCreation WHERE Name=@FileName+@FileCompression AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

		UPDATE T1
		SET T1.ExceptionsFileId=@FileCreationId
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
	END
	DELETE FROM temp_Exceptions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	SELECT ISNULL(@FileCreationId,0) AS FileCreationId
	SET NOCOUNT OFF
END

GO
/****** Object:  StoredProcedure [dbo].[GetActionableExceptionTypes]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetActionableExceptionTypes]
	@Category varchar(20)
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysCodeId int
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)

	SET @SysCode='Exception Type'
	SELECT @SysCodeId=Id FROM SystemCode WHERE Code=@SysCode
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'

	IF @Category='INVALIDID'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID)
	END

	IF @Category='DUPLICATEID'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC)
	END

	--IF @Category='INVALIDACC'
	--BEGIN
	--	SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDACC)
	--END

	IF @Category='INVALIDCARD'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INVALIDCARD)
	END

	IF @Category='SUSPICIOUS'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY)
	END

	IF @Category='INELIGIBLE'
	BEGIN
		SELECT Id AS ExceptionTypeId,Code AS ExceptionCode,[Description] AS ExceptionDesc FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeId AND Code IN(@Exception_INELIGIBLESUS)
	END


END

GO
/****** Object:  StoredProcedure [dbo].[GetAllEnrolmentPlan]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetAllEnrolmentPlan]
	@EnrolmentPlanId int=NULL
AS
BEGIN
	DECLARE @SQL varchar(8000)
	DECLARE @strPSP varchar(1000)
	DECLARE @strCols varchar(1000)
 	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Card Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SELECT T1.Id,T2.Code AS Programme,T3.Code AS RegGroup,T1.RegGroupHhs,T1.BeneHhs,T1.ExpPlanEqualShare,T1.ExpPlanPovertyPrioritized,T1.EnrolmentNumbers,T4.Code AS EnrolmentGroup,T5.[Description] AS [Status]
	FROM HouseholdEnrolmentPlan T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
								   INNER JOIN SystemCodeDetail T3 ON T1.RegGroupId=T3.Id
								   INNER JOIN SystemCodeDetail T4 ON T1.EnrolmentGroupId=T4.Id
								   INNER JOIN SystemCodeDetail T5 ON T1.StatusId=T5.Id
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)

	SELECT T3.PSP,SUM(ISNULL(T3.AccountOpenedHhs,0)) AS AccountOpenedHhs,SUM(ISNULL(T3.CardedHhs,0)) AS CardedHhs
	FROM HouseholdEnrolmentPlan T1 LEFT JOIN (
												SELECT HhEnrolmentPlanId,COUNT(Id) AS EnrolledHhs
												FROM HouseholdEnrolment
												GROUP BY HhEnrolmentPlanId
											) T2 ON T1.Id=T2.HhEnrolmentPlanId
								   LEFT JOIN(
												SELECT T1.EnrolmentPlanId,T1.PSP,T1.AccountOpenedHhs,T2.CardedHhs
												FROM (
														SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.BeneAccountId) AS AccountOpenedHhs
														FROM (
																SELECT T3.Id AS EnrolmentPlanId,T1.Id AS BeneAccountId,T5.Id AS PSPId,T5.Name AS PSP
																FROM (	SELECT MIN(Id) AS Id,HhEnrolmentId,PSPBranchId 
																		FROM BeneficiaryAccount 
																		WHERE StatusId=@SystemCodeDetailId1 
																		GROUP BY HhEnrolmentId,PSPBranchId
																		) T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
																			 INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
																			 INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id
																			 INNER JOIN PSP T5 ON T4.PSPId=T5.Id
															) T1
														GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
													) T1 LEFT JOIN (
																			SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.PaymentCard) AS CardedHhs
																			FROM (
																					SELECT T4.Id AS EnrolmentPlanId,T1.PriReciId AS PaymentCard,T6.Id AS PSPId,T6.Name AS PSP
																					FROM (SELECT T1.BeneAccountId,T1.PriReciId
																						  FROM BeneficiaryPaymentCard T1 INNER JOIN (
																																		SELECT BeneAccountId,MIN(Id) AS Id
																																		FROM BeneficiaryPaymentCard
																																		WHERE StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
																																		GROUP BY BeneAccountId
																																	) T2 ON T1.Id=T2.Id
																						) T1 INNER JOIN (SELECT MIN(Id) AS Id,HhEnrolmentId,PSPBranchId 
																										FROM BeneficiaryAccount 
																										WHERE StatusId=@SystemCodeDetailId1 
																										GROUP BY HhEnrolmentId,PSPBranchId
																										) T2 ON T1.BeneAccountId=T2.Id
																							 INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
																							 INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id
																							 INNER JOIN PSPBranch T5 ON T2.PSPBranchId=T5.Id
																							 INNER JOIN PSP T6 ON T5.PSPId=T6.Id
																				) T1
																			GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
																	) T2 ON T1.EnrolmentPlanId=T2.EnrolmentPlanId AND T1.PSPId=T2.PSPId
											) T3 ON T1.Id=T3.EnrolmentPlanId
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)	
	GROUP BY T3.PSP							  
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllEnrolmentPlan_TEST]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetAllEnrolmentPlan_TEST]
	@EnrolmentPlanId int=NULL
AS
BEGIN
	SELECT T1.Id,T2.Code AS Programme,T3.Code AS RegGroup,T1.RegGroupHhs,T1.BeneHhs,T1.ExpPlanEqualShare,T1.ExpPlanPovertyPrioritized,T1.EnrolmentNumbers,T4.Code AS EnrolmentGroup,T5.[Description] AS [Status]
	FROM HouseholdEnrolmentPlan T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
								   INNER JOIN SystemCodeDetail T3 ON T1.RegGroupId=T3.Id
								   INNER JOIN SystemCodeDetail T4 ON T1.EnrolmentGroupId=T4.Id
								   INNER JOIN SystemCodeDetail T5 ON T1.StatusId=T5.Id
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)

	SELECT T3.PSP,SUM(ISNULL(T3.AccountOpenedHhs,0)) AS AccountOpenedHhs,SUM(ISNULL(T3.CardedHhs,0)) AS CardedHhs
	FROM HouseholdEnrolmentPlan T1 LEFT JOIN (
												SELECT HhEnrolmentPlanId,COUNT(Id) AS EnrolledHhs
												FROM HouseholdEnrolment
												GROUP BY HhEnrolmentPlanId
											) T2 ON T1.Id=T2.HhEnrolmentPlanId
								   LEFT JOIN(
												SELECT T1.EnrolmentPlanId,T1.PSP,T1.AccountOpenedHhs,T2.CardedHhs
												FROM (
														SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.BeneAccountId) AS AccountOpenedHhs
														FROM (
																SELECT DISTINCT T3.Id AS EnrolmentPlanId,T1.Id AS BeneAccountId,T5.Id AS PSPId,T5.Name AS PSP
																FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
																						   INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
																						   INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id
																						   INNER JOIN PSP T5 ON T4.PSPId=T5.Id
															) T1
														GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
													) T1 LEFT JOIN (
																		SELECT T1.EnrolmentPlanId,T1.PSPId,T1.PSP,COUNT(T1.PaymentCard) AS CardedHhs
																		FROM (
																				SELECT DISTINCT T4.Id AS EnrolmentPlanId,T1.PriReciId AS PaymentCard,T6.Id AS PSPId,T6.Name AS PSP
																				FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
																											   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
																											   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id
																											   INNER JOIN PSPBranch T5 ON T2.PSPBranchId=T5.Id
																											   INNER JOIN PSP T6 ON T5.PSPId=T6.Id
																				WHERE ISNULL(T1.PaymentCardNo,'')<>''
																			) T1
																		GROUP BY T1.EnrolmentPlanId,T1.PSPId,T1.PSP
																	) T2 ON T1.EnrolmentPlanId=T2.EnrolmentPlanId AND T1.PSPId=T2.PSPId
											) T3 ON T1.Id=T3.EnrolmentPlanId
	WHERE T1.Id=ISNULL(@EnrolmentPlanId,T1.Id)	
	GROUP BY T3.PSP							  
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllOpenedAccounts]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[GetAllOpenedAccounts]

AS
BEGIN

DECLARE @SysCode varchar(20)
DECLARE @SysDetailCode varchar(20)
DECLARE @SystemCodeDetailId1 int
DECLARE @SystemCodeDetailId2 int
DECLARE @SystemCodeDetailId3 int

SET @SysCode='Account Status'
SET @SysDetailCode='1'
SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SELECT T2.Id AS EnrolmentNo
,T14.AccountNo,T14.AccountName,T14.OpenedOn
	,T15.Name AS PSPBranch,T16.Name AS PSP
		,T4.Code AS Programme
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM 
	
	HouseholdEnrolmentPlan T1    INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN BeneficiaryAccount T14 ON T14.HhEnrolmentId = T2.Id
								   INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id
								   INNER JOIN PSP T16 ON T15.PSPId = T16.Id
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!

	WHERE T14.StatusId=@SystemCodeDetailId1 
--AND T14.DateAdded IS NOT NULL  AND T3.TokenId IS NOT NULL 
ORDER BY T14.HhEnrolmentId, T16.Name, T15.Name
END

GO
/****** Object:  StoredProcedure [dbo].[GetAuditTrail]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetAuditTrail]
	@Id int=NULL
   ,@UserId int=NULL
   ,@StartDate datetime=NULL
   ,@EndDate datetime=NULL
   ,@IPAddress varchar(15)=NULL
   ,@MACAddress varchar(15)=NULL
   ,@IMEI bigint=NULL
   ,@ModuleId int=NULL
   ,@ModuleRightId int=NULL
   ,@Key1 int=null
   ,@Key2 int=null
   ,@Key3 int=null
   ,@Key4 int=null
   ,@Key5 int=null
AS
BEGIN
	SELECT T1.Id,T1.UserId,T5.UserName,T1.LogTime,T1.IPAddress,T1.MACAddress,T1.IMEI,T3.Name AS Module,T1.ModuleRightId,T2.[Description] AS ModuleRightDescription,T1.Record,T1.WasSuccessful
	FROM [CCTP-MIS_AUDIT].dbo.AuditTrail T1 INNER JOIN ModuleRight T2 ON T1.ModuleRightId=T2.Id
					   INNER JOIN Module T3 ON T2. ModuleId=T3.Id
					   INNER JOIN SystemCodeDetail T4 ON T2.RightId=T4.Id
					   INNER JOIN [User] T5 ON T1.UserId=T5.Id
	WHERE T1.Id=ISNULL(@Id,T1.Id)
		AND T1.UserId=ISNULL(@UserId,T1.UserId)
		AND T1.LogTime>=ISNULL(@StartDate,T1.LogTime) AND T1.LogTime<=ISNULL(@EndDate,T1.LogTime)
		AND T1.IPAddress=ISNULL(@IPAddress,T1.IPAddress)
		AND T1.MACAddress=ISNULL(@MACAddress,T1.MACAddress)
		AND T1.IMEI=ISNULL(@IMEI,T1.IMEI)
		AND T3.Id=ISNULL(@ModuleId,T3.Id)
		AND T1.ModuleRightId=ISNULL(@ModuleRightId,T1.ModuleRightId)
		AND T1.Key1=ISNULL(@key1,T1.Key1)
		AND T1.Key2=ISNULL(@key2,T1.Key2)
		AND T1.Key3=ISNULL(@key3,T1.Key3)
		AND T1.Key4=ISNULL(@key4,T1.Key4)
		AND T1.Key5=ISNULL(@key5,T1.Key5)
END

GO
/****** Object:  StoredProcedure [dbo].[GetBeneAccountMonthlyActivitySummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetBeneAccountMonthlyActivitySummary]
	@MonthNo tinyint
   ,@Year int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int

	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T3.Name AS PSPName,COUNT(T1.Id) AS TotalAccounts
		,SUM(CASE WHEN(T4.BeneAccountId>0) THEN 1 ELSE 0 END) AS ReportedAccounts
		,COUNT(T1.Id)-SUM(CASE WHEN(T4.BeneAccountId>0) THEN 1 ELSE 0 END) AS PendingAccounts
		,SUM(CASE WHEN(T4.HadUniqueWdl<>0) THEN 1 ELSE 0 END) AS HadUniqueWdl
		,SUM(CASE WHEN(T4.HadBeneBiosVerified<>0) THEN 1 ELSE 0 END) AS HadBeneBiosVerified
		,SUM(CASE WHEN(T4.IsDormant<>0) THEN 1 ELSE 0 END) AS IsDormant
		,SUM(CASE WHEN(T4.IsDueForClawback<>0) THEN 1 ELSE 0 END) AS IsDueForClawback
		,SUM(CASE WHEN(T4.ClawbackAmount>0) THEN ClawbackAmount ELSE 0 END) AS ClawbackAmount
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
							   LEFT JOIN (
											SELECT T2.BeneAccountId,T2.HadUniqueWdl,T2.UniqueWdlTrxNo,T2.UniqueWdlDate,T2.HadBeneBiosVerified,T2.IsDormant,T2.DormancyDate,T2.IsDueForClawback,T2.ClawbackAmount
											FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
											WHERE T1.MonthId=@SystemCodeDetailId1 AND T1.[Year]=@Year
										) T4 ON T1.Id=T4.BeneAccountId
	WHERE T1.StatusId=@SystemCodeDetailId2 AND DATEDIFF(MM,T1.OpenedOn,CONVERT(datetime,'1 '+dbo.fn_MonthName(@MonthNo,0)+' '+CONVERT(varchar(4),@Year)))>=0
	GROUP BY T3.Id,T3.Name
END

GO
/****** Object:  StoredProcedure [dbo].[GetBeneficiariesByLocation]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetBeneficiariesByLocation] (@CountyName VARCHAR(50))
AS
begin
select  County, [74] AS Male, [75] AS Female

FROM 

(
SELECT T1.Name 'County', T2.SexId, T2.BeneAcc FROM [Location]  T1 LEFT JOIN 
( 
SELECT T1.Id 'BeneAcc', T13.* , T08.SexId, T09.[Description] 'Sex'
from   BeneficiaryAccount T1
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id   
  INNER JOIN HouseholdEnrolmentPlan T05 ON T5.HhEnrolmentPlanId = T05.Id  
  INNER JOIN Household T6 ON T5.HhId=T6.Id
  inner join Programme t06 on t06.Id = t6.ProgrammeId
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
 INNER JOIN HouseholdMember T07 ON T6.ID = T07.HhId and t07.MemberRoleId = t06.PrimaryRecipientId
 INNER JOIN  Person T08 ON T07.PersonId = T08.Id 
  INNER JOIN  SystemCodeDetail T09 ON T08.SexId = T09.Id 
 

   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id AND T6.Name = @CountyName
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE  T1.StatusId=23     
  ) T2 ON T1.id = T2.LocationId 
  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id AND T6.Name = @CountyName
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id ) T13 ON T1.Id=T13.LocationId 

  ) P
  PIVOT (COUNT (BeneAcc) FOR SexId IN ([74],[75]))AS PVT

  end
 



GO
/****** Object:  StoredProcedure [dbo].[GetBeneficiariesWithNoAccounts]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROC [dbo].[GetBeneficiariesWithNoAccounts]
@StartDate datetime = null,
@EndDate datetime = null
AS
BEGIN

SELECT   T2.Id AS EnrolmentNo
		,T4.Code 'Programme'
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS PriReciFirstName
		,T6.MiddleName AS PriReciMiddleName
		,T6.Surname AS PriReciSurname
		,T6.NationalIdNo AS PriReciIDNo
		,T7.Code AS PriReciSex
		,T6.DoB AS PriReciDoB
		,ISNULL(T9.FirstName,'') AS SecReciFirstName
		,ISNULL(T9.MiddleName,'') AS SecReciMiddleName
		,ISNULL(T9.Surname,'') AS SecReciSurname
		,ISNULL(T9.NationalIdNo,'') AS SecReciIDNo
		,ISNULL(T10.Code,'') AS SecReciSex
		,ISNULL(T9.DoB,'') AS SecReciDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		 
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
	LEFT JOIN BeneficiaryAccount T14 ON T2.Id = T14.HhEnrolmentId   
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id  
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

											  WHERE T14.AccountNo IS   NULL and t1.ApvOn >= isnull(@StartDate,t1.ApvOn)and t1.ApvOn <= isnull(@EndDate,t1.ApvOn)
	
ORDER BY EnrolmentNo, ProgrammeNo
END

GO
/****** Object:  StoredProcedure [dbo].[GetBeneForEnrolment]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetBeneForEnrolment]
	@ProgrammeId int
   ,@RegGroupId int

AS

BEGIN
	--TO DO LIST: 1. INCORPORATE WAITING LIST VALIDITY PERIOD!
	DECLARE @tbl_EnrolmentHhAnalysis TABLE(
		LocationId int NOT NULL
	   ,PovertyPerc float NOT NULL DEFAULT(0.00)
	   ,RegGroupHhs int NOT NULL DEFAULT(0)
	   ,BeneHhs int NOT NULL DEFAULT(0)
	   ,ExpPlanEqualShare int NOT NULL DEFAULT(0)
	   ,ExpPlanPovertyPrioritized int NOT NULL DEFAULT(0)
	)



	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode1 varchar(20)
	DECLARE @SysDetailCode2 varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @ErrorMsg varchar(128)


	IF EXISTS(SELECT 1 FROM HouseholdEnrolmentPlan WHERE ProgrammeId=@ProgrammeId AND ApvBy IS NULL)
		SET @ErrorMsg='There is an existing enrolment batch for the programme which requires to be approved first'

	IF ISNULL(@ErrorMsg,'')<>''

	BEGIN

		RAISERROR(@ErrorMsg,16,1)

		RETURN

	END



	SET @SysCode='System Settings'

	SET @SysDetailCode1='CURFINYEAR'

	SET @SysDetailCode2='WAITLISTVALIDITYMONTHS'

	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1

	SELECT @SystemCodeDetailId2=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2



	SET @SysCode='HHStatus'

	SET @SysDetailCode1='VALPASS'

	SET @SysDetailCode2='PSPCARDED'

	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode1

	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode2



	SELECT SUM(T1.RegGroupHhs) AS RegGroupHhs,SUM(T1.BeneHhs) AS BeneHhs,SUM(ScaleupEqualShare) AS ScaleupEqualShare,SUM(ScaleupPovertyPrioritized) AS ScaleupPovertyPrioritized

	FROM (

			SELECT T1.LocationId,T1.RegGroupHhs,ISNULL(T2.BeneHhs,0) AS BeneHhs,ISNULL(T3.PovertyHeadCountPerc,0.00) AS PovertyPerc,ISNULL(T3.ScaleupEqualShare,0) AS ScaleupEqualShare,ISNULL(T3.ScaleupPovertyPrioritized,0) AS ScaleupPovertyPrioritized

			FROM (

					SELECT T4.LocationId,COUNT(T1.Id) AS RegGroupHhs

					FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId

									  INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1

									  INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency

												  FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id

																	  INNER JOIN Division T3 ON T2.DivisionId=T3.Id

																	  INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																	  INNER JOIN District T5 ON T4.DistrictId=T5.Id
																	  INNER JOIN County T6 ON T4.CountyId=T6.Id
																	  INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																	  INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id --AND T6.[Name] IN ('Isiolo','Kitui','Machakos','Makueni','Marsabit','Meru','Tharaka','Embu')

												 ) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId		 								 					  
					WHERE T1.ProgrammeId=@ProgrammeId AND T1.RegGroupId=@RegGroupId AND T1.StatusId=@SystemCodeDetailId3 --EVALUATE THE WAITING LIST VALIDITY PERIOD HERE!!! 
					GROUP BY T4.LocationId
				) T1 LEFT JOIN (

									SELECT T4.LocationId,COUNT(T1.Id) AS BeneHhs
									FROM Household T1 INNER JOIN HouseholdSubLocation T2 ON T1.Id=T2.HhId
														INNER JOIN GeoMaster T3 ON T2.GeoMasterId=T3.Id AND T3.IsDefault=1
														INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																						INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																						INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																						INNER JOIN District T5 ON T4.DistrictId=T5.Id
																						INNER JOIN County T6 ON T4.CountyId=T6.Id  --AND T6.[Name] IN ('Isiolo','Kitui','Machakos','Makueni','Marsabit','Meru','Tharaka','Embu')
																						INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																						INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id 
																	) T4 ON T2.SubLocationId=T4.SubLocationId AND T3.Id=T4.GeoMasterId					  
									WHERE T1.StatusId=@SystemCodeDetailId4
									GROUP BY T4.LocationId
								) T2 ON T1.LocationId=T2.LocationId 

					 LEFT JOIN (
									SELECT T2.LocationId,T2.PovertyHeadCountPerc,T1.ScaleupEqualShare,T1.ScaleupPovertyPrioritized,(T1.ScaleupEqualShare+T1.ScaleupPovertyPrioritized) AS ExpPlanHhs
									FROM ExpansionPlanDetail T1 INNER JOIN ExpansionPlan T2 ON T1.ExpansionPlanId=T2.Id
																INNER JOIN ExpansionPlanMaster T3 ON T2.ExpansionPlanMasterId=T3.Id
																INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Id AS LocationId,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
																			FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																								INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																								INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																								INNER JOIN District T5 ON T4.DistrictId=T5.Id
																								INNER JOIN County T6 ON T4.CountyId=T6.Id  --AND T6.[Name] IN ('Isiolo','Kitui','Machakos','Makueni','Marsabit','Meru','Tharaka','Embu')
																								INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																								INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
																			WHERE T8.IsDefault=1
																			) T4 ON T2.LocationId=T4.LocationId	

									WHERE T3.ProgrammeId=@ProgrammeId AND T1.FinancialYearId=@SystemCodeDetailId1
								) T3 ON T1.LocationId=T3.LocationId
			) T1
END


GO
/****** Object:  StoredProcedure [dbo].[GetBeneHouseholdEnrolmentInfo]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  
CREATE PROC [dbo].[GetBeneHouseholdEnrolmentInfo]    
 @Id int    
AS    
BEGIN    
 DECLARE @SysCode varchar(20)    
 DECLARE @SysDetailCode varchar(20)    
 DECLARE @SystemCodeDetailId1 int    
 DECLARE @SystemCodeDetailId2 int    
 DECLARE @SystemCodeDetailId3 int    
 DECLARE @Len tinyint    
 DECLARE @Prefix varchar(10)    
 DECLARE @ProgNo int    
 DECLARE @HhId int    
 DECLARE @LoopVar int    
   
    
 SET @SysCode='Member Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
    
 SET @SysCode='Account Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
    
 SET @SysCode='Card Status'    
 SET @SysDetailCode='1'    
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode    
         

  BEGIN    
   SELECT T2.Id AS ProgrammeId,T2.Code AS ProgrammeCode,T2.Name AS ProgrammeName, T3.Id  as EnrolmentNo,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.ProgrammeNo)))+CONVERT(varchar(6),T3.ProgrammeNo) AS ProgrammeNo,
  T5.Id AS PriReciPersonId,T5.FirstName AS PriReciFirstName,T5.MiddleName AS PriReciMiddleName,T5.Surname AS PriReciSurname,T5.SexId AS PriReciSexId,T10.Code AS PriReciSex,T5.NationalIdNo AS PriReciNationalIdNo,T5.BirthCertNo AS PriReciBirthCertNo,T5.MobileNo1 AS PriReciMobileNo1,T5.MobileNo2 AS PriReciMobileNo2    
 ,T6.Id AS SecReciPersonId,T6.FirstName AS SecReciFirstName,T6.MiddleName AS SecReciMiddleName,T6.Surname AS SecReciSurname,T6.SexId AS SecReciSexId,T11.Code AS SecReciSex,T6.NationalIdNo AS SecReciNationalIdNo,T6.BirthCertNo AS SecReciBirthCertNo,T6.MobileNo1 AS SecReciMobileNo1,T6.MobileNo2 AS SecReciMobileNo2    
 ,T7.AccountNo,T7.AccountName,T7.PSPBranch,T7.PSP,T7.OpenedOn AS AccountOpenedOn    
 ,T12.PaymentCardNo AS PaymentCardNo    
 ,T9.SubLocationId,T9.SubLocationName,T9.LocationId,T9.LocationName,T9.DivisionId,T9.DivisionName,T9.DistrictId,T9.DistrictName,T9.CountyId,T9.CountyName,T9.ConstituencyId,T9.ConstituencyName    
   FROM Household T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id    
   INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId    
   INNER JOIN HouseholdMember T4  ON T1.Id=T4.HhId    
   INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1    
   LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1    
   LEFT JOIN (    
     SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn    
     FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id    
     INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id    
     INNER JOIN PSP T4 ON T3.PSPId=T4.Id    
   --  WHERE T2.HhId=@HhId AND T1.StatusId=@SystemCodeDetailId2    
    ) T7 ON T3.Id=T7.HhEnrolmentId    
   INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId    
   INNER JOIN (    
     SELECT T1.Id AS SubLocationId,T1.Name AS SubLocationName,T2.Id AS LocationId,T2.Name AS LocationName,T3.Id AS DivisionId,T3.Name AS DivisionName,T5.Id AS DistrictId,T5.Name AS DistrictName,T6.Id AS CountyId,T6.Name AS CountyName,T7.Id AS ConstituencyId,T7.Name AS ConstituencyName,T8.Id AS GeoMasterId    
     FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id    
    INNER JOIN Division T3 ON T2.DivisionId=T3.Id    
    INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id    
    INNER JOIN District T5 ON T4.DistrictId=T5.Id    
    INNER JOIN County T6 ON T4.CountyId=T6.Id    
    INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id    
    INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1    
    ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId    
   INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id    
   LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id    
   LEFT JOIN (    
     SELECT DISTINCT T1.BeneAccountId,T1.PaymentCardNo    
     FROM BeneficiaryPaymentCard T1     
     WHERE T1.StatusId=@SystemCodeDetailId3    
    ) T12 ON T7.BeneAccountId=T12.BeneAccountId    
   WHERE T3.HhId = @Id    
    
   RETURN    
  END    
   
 END    


GO
/****** Object:  StoredProcedure [dbo].[GetBeneInfo]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetBeneInfo]
	@ProgrammeNo varchar(10)=NULL
   ,@EnrolmentNo int=NULL
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @Len tinyint
	DECLARE @Prefix varchar(10)
	DECLARE @ProgNo int
	DECLARE @HhId int
	DECLARE @LoopVar int
	DECLARE @ProgTable TABLE(
		RowId int NOT NULL IDENTITY(1,1)
	   ,ProgrammeId int
	   ,BeneProgNoPrefix varchar(10) NOT NULL
	)

	INSERT INTO @ProgTable(ProgrammeId,BeneProgNoPrefix)
	SELECT Id,BeneProgNoPrefix
	FROM Programme
	
	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ProgrammeNo=ISNULL(@ProgrammeNo,BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),ProgrammeNo)))+CONVERT(varchar(6),ProgrammeNo)) FROM HouseholdEnrolment WHERE Id=@EnrolmentNo

	SET @LoopVar=1
	WHILE EXISTS(SELECT 1 FROM @ProgTable WHERE RowId=@LoopVar)
	BEGIN
		SELECT @Len=LEN(BeneProgNoPrefix),@Prefix=BeneProgNoPrefix FROM @ProgTable WHERE RowId=@LoopVar

		IF LEFT(@ProgrammeNo,@Len)=@Prefix
			SET @ProgNo=CONVERT(int,RIGHT(@ProgrammeNo,LEN(@ProgrammeNo)-@Len))
		SELECT @HhId=HhId FROM HouseholdEnrolment WHERE BeneProgNoPrefix=@Prefix AND ProgrammeNo=@ProgNo

		IF @HhId>0
		BEGIN
			SELECT T2.Id AS ProgrammeId,T2.Code AS ProgrammeCode,T2.Name AS ProgrammeName,T13.[Description] AS HhStatus
				,T5.Id AS PriReciPersonId,T5.FirstName AS PriReciFirstName,T5.MiddleName AS PriReciMiddleName,T5.Surname AS PriReciSurname,T5.SexId AS PriReciSexId,T10.Code AS PriReciSex,T5.NationalIdNo AS PriReciNationalIdNo,T5.BirthCertNo AS PriReciBirthCertNo,T5.MobileNo1 AS PriReciMobileNo1,T5.MobileNo2 AS PriReciMobileNo2
				,T6.Id AS SecReciPersonId,T6.FirstName AS SecReciFirstName,T6.MiddleName AS SecReciMiddleName,T6.Surname AS SecReciSurname,T6.SexId AS SecReciSexId,T11.Code AS SecReciSex,T6.NationalIdNo AS SecReciNationalIdNo,T6.BirthCertNo AS SecReciBirthCertNo,T6.MobileNo1 AS SecReciMobileNo1,T6.MobileNo2 AS SecReciMobileNo2
				,T7.AccountNo,
				T7.AccountName,
				T7.PSPBranch,
				T7.BANKCODE ,	T7.BranchCode ,
				T7.PSP,T7.OpenedOn AS AccountOpenedOn
				,T12.PaymentCardNo AS PaymentCardNo
				,T9.SubLocationId,T9.SubLocationName,T9.LocationId,T9.LocationName,T9.DivisionId,T9.DivisionName,T9.DistrictId,T9.DistrictName,T9.CountyId,T9.CountyName,T9.ConstituencyId,T9.ConstituencyName
			FROM Household T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
							  INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId
							  INNER JOIN HouseholdMember T4  ON T1.Id=T4.HhId
							  INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1
							  LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1
							  LEFT JOIN (
											SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn, T4.Id PSPID, T4.Code 'BANKCODE', T3.Code 'BRANCHCODE'
											FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
																	   INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id
																	   INNER JOIN PSP T4 ON T3.PSPId=T4.Id
											WHERE T2.HhId=@HhId AND T1.StatusId=@SystemCodeDetailId2
										) T7 ON T3.Id=T7.HhEnrolmentId
							  INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId
							  INNER JOIN (
											SELECT T1.Id AS SubLocationId,T1.Name AS SubLocationName,T2.Id AS LocationId,T2.Name AS LocationName,T3.Id AS DivisionId,T3.Name AS DivisionName,T5.Id AS DistrictId,T5.Name AS DistrictName,T6.Id AS CountyId,T6.Name AS CountyName,T7.Id AS ConstituencyId,T7.Name AS ConstituencyName,T8.Id AS GeoMasterId
											FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																INNER JOIN District T5 ON T4.DistrictId=T5.Id
																INNER JOIN County T6 ON T4.CountyId=T6.Id
																INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1
										) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId
							  INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id
							  LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id
							  LEFT JOIN (
											SELECT DISTINCT T1.BeneAccountId,T1.PaymentCardNo
											FROM BeneficiaryPaymentCard T1 
											WHERE T1.StatusId=@SystemCodeDetailId3
										) T12 ON T7.BeneAccountId=T12.BeneAccountId
							  INNER JOIN SystemCodeDetail T13 ON T1.StatusId=T13.Id
			WHERE T3.BeneProgNoPrefix=@Prefix AND T3.ProgrammeNo=@ProgNo

			RETURN
		END
		SET @LoopVar=@LoopVar+1
	END
	RAISERROR('The specified programme number is not valid',16,1)
END

GO
/****** Object:  StoredProcedure [dbo].[GetBenePayrollSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE proc [dbo].[GetBenePayrollSummary]
	@Id int
	as
	BEGIN
	SELECT T1.PaymentCycleId,CONCAT(TT2.Description,'-', TT3.Description,'  ', TT1.Description )AS PaymentCycle,
	T7.Id AS EnrolmentNo,T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T10.Id AS PSPId,T10.Code AS PSPCode,T10.Name AS PSPName,T9.Code AS PSPBranchCode,T9.Name AS PSPBranchName,T8.AccountNo,T8.AccountName,T1.PaymentAmount AS Amount,T4.NationalIDNo AS BeneficiaryIDNo,T4.MobileNo1 AS BeneficiaryContact,T5.NationalIDNo AS CaregiverIDNo,T5.MobileNo1 AS CaregiverContact,T14.SubLocation,T14.Location,T14.Division,T14.District,T14.County,T14.Constituency
	FROM Payroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					  INNER JOIN SystemCodeDetail TT1 ON T2.FinancialYearId=TT1.Id
INNER JOIN SystemCodeDetail TT2 ON T2.FromMonthId=TT2.Id
INNER JOIN SystemCodeDetail TT3 ON T2.ToMonthId=TT3.Id
INNER JOIN Person T4 ON T3.BenePersonId=T4.Id
					LEFT JOIN Person T5 ON T3.CGPersonId=T5.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
					INNER JOIN BeneficiaryAccount T8 ON T3.BeneAccountId=T8.Id
					INNER JOIN PSPBranch T9 ON T8.PSPBranchId=T9.Id
					INNER JOIN PSP T10 ON T9.PSPId=T10.Id
					INNER JOIN Household T11 ON T1.HhId=T11.Id
					INNER JOIN HouseholdSubLocation T12 ON T11.Id=T12.HhId
					INNER JOIN GeoMaster T13 ON T12.GeoMasterId=T13.Id AND T13.IsDefault=1
					INNER JOIN (
									SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency
									FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
														INNER JOIN Division T7 ON T2.DivisionId=T7.Id
														INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
														INNER JOIN District T9 ON T8.DistrictId=T9.Id
														INNER JOIN County T10 ON T8.CountyId=T10.Id
														INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
														INNER JOIN GeoMaster T12 ON T10.GeoMasterId=T10.GeoMasterId AND T12.IsDefault=1
								) T14 ON T12.SubLocationId=T14.SubLocationId
	 
 WHERE T1.HhId = @Id

 END 

GO
/****** Object:  StoredProcedure [dbo].[GetBenePrepayrollSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	CREATE PROC [dbo].[GetBenePrepayrollSummary]
	@Id int
	as
	BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int
	
	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
	
	SELECT T1.PaymentCycleId,
	CONCAT(TT2.Description,'-', TT3.Description,'  ', TT1.Description )AS PaymentCycle,T1.ProgrammeId,T3.Name AS Programme,TT4.Id AS EnrolmentNo,TT4.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),TT4.ProgrammeNo)))+CONVERT(varchar(6),TT4.ProgrammeNo) AS ProgrammeNo
		,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneDoB,T4.Code AS BeneSex,T1.BeneNationalIDNo,T1.PriReciCanReceivePayment,CASE WHEN(ISNULL(T7.ActionedApvBy,0))>0 THEN -1 ELSE CONVERT(bit,ISNULL(T7.PersonId,0)) END AS IsInvalidBene,CASE WHEN(ISNULL(T8.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8.PersonId,0)) END AS IsDuplicateBene,CASE WHEN(ISNULL(T9.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T9.HhId,0)) END AS IsIneligibleBene
		,T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,T1.CGDoB,T5.Code AS CGSex,T1.CGNationalIDNo,
		CASE WHEN(ISNULL(T11.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T11.PersonId,0)) END AS IsInvalidCG,CASE WHEN(ISNULL(T12.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12.PersonId,0)) END AS IsDuplicateCG,CASE WHEN(ISNULL(T13.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T13.HhId,0)) END AS IsIneligibleCG
		,T6.[Description] AS HhStatusDescription
		,T6.Code AS HhStatus
		
		,CASE WHEN(ISNULL(T19.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T19.HhId,0)) END AS IsIneligibleHh,
	TT15.AccountName AS AccountName,
		TT15.AccountNo AS AccountNumber,CASE WHEN(ISNULL(T15.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T15.HhId,0)) END AS IsInvalidAccount,CASE WHEN(ISNULL(T17.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T17.HhId,0)) END AS IsDormantAccount, ''  AS PaymentCardNumber,CASE WHEN(ISNULL(T16.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T16.HhId,0)) END AS IsInvalidPaymentCard,T21.Name AS PaymentZone,T1.PaymentZoneCommAmt,T1.ConseAccInactivity,T1.EntitlementAmount,T1.AdjustmentAmount,CASE WHEN(ISNULL(T18.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T18.HhId,0)) END AS IsSuspiciousAmount--,T25.SubLocation,T25.Location,T25.Division,T25.District,T25.County,T25.Constituency
 FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id
  INNER JOIN SystemCodeDetail TT1 ON T2.FinancialYearId=TT1.Id
INNER JOIN SystemCodeDetail TT2 ON T2.FromMonthId=TT2.Id
INNER JOIN SystemCodeDetail TT3 ON T2.ToMonthId=TT3.Id


					   INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					   INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
					   INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
					   LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
					   INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
					   LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
					   LEFT JOIN PrepayrollDuplicateID T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
					   LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId
					   LEFT JOIN PrepayrollDuplicateID T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId					   			   
					   LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
					   LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
					 --  LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.p=TT16.Id
					   LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId					   			   
					   LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
					   INNER JOIN Household T22 ON T1.HhId=T22.Id
					   INNER JOIN HouseholdSubLocation T23 ON T22.Id=T23.HhId
					   INNER JOIN GeoMaster T24 ON T23.GeoMasterId=T24.Id AND T24.IsDefault=1


					  -- exec GetClassFromTable 'TEMP_prepayrollReport'
					   --INNER JOIN (
								--	SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency
								--	FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
								--						INNER JOIN Division T7 ON T2.DivisionId=T7.Id
								--						INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
								--						INNER JOIN District T9 ON T8.DistrictId=T9.Id
								--						INNER JOIN County T10 ON T8.CountyId=T10.Id
								--						INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
								--) T25 ON T23.SubLocationId=T25.SubLocationId
 WHERE T1.HhId = @Id

 END 

GO
/****** Object:  StoredProcedure [dbo].[GetBeneStatementCreditReport]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE proc [dbo].[GetBeneStatementCreditReport] @Id int
as
begin
 SELECT T1.*, T2.PaymentAmount, 
 CONCAT(T5.Description,'-', T6.Description,'  ', T7.Description )AS PaymentCycle,
 TT3.AccountName AS AccountName , 
 TT3.AccountNo AS AccountNo ,
 
 T5.Description 'FromMonth' , T6.Description 'ToMonth', T7.Description 'FinancialYear', T8.Description 'Status', T9.Description 'PaymentStage' FROM Payment T1 
 INNER JOIN Payroll T2 ON T1.HhId = T2.HhId AND T1.ProgrammeId=T2.ProgrammeId AND T1.PaymentCycleId = T2.PaymentCycleId
 INNER JOIN PrePayroll TT2 ON T1.HhId = TT2.HhId AND T1.ProgrammeId=TT2.ProgrammeId AND T1.PaymentCycleId = TT2.PaymentCycleId
 INNER JOIN BeneficiaryAccount TT3 ON TT3.Id = TT2.BeneAccountId
 INNER JOIN PaymentCycleDetail T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId = T3.ProgrammeId
 INNER JOIN PaymentCycle T4 ON  T3.PaymentCycleId = T4.Id
 INNER JOIN SystemCodeDetail T5 ON T5.ID=T4.FromMonthId
 INNER JOIN SystemCodeDetail T6 ON T6.ID=T4.ToMonthId
 INNER JOIN SystemCodeDetail T7 ON T7.ID=T4.FinancialYearId
 INNER JOIN SystemCodeDetail T8 ON T8.Id=T4.StatusId
INNER JOIN SystemCodeDetail T9 ON T9.Id=T3.PaymentStageId

WHERE T1.HhId = @Id


end




GO
/****** Object:  StoredProcedure [dbo].[GetBeneStatementMonthlyStatement]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[GetBeneStatementMonthlyStatement] @Id int
as
begin
select T1.*,T2.Year,T7.Description 'Month',
t3.AccountNo, T3.AccountName, T3.OpenedOn, T3.DateAdded, T6.Name 'PSP', T5.Name 'BRANCH'
 from BeneAccountMonthlyActivityDetail T1
INNER JOIN BeneAccountMonthlyActivity T2 ON T1.BeneAccountMonthlyActivityId = T2.Id
INNER JOIN BeneficiaryAccount T3 ON T1.BeneAccountId = T3.Id
INNER JOIN HouseholdEnrolment T4 ON T3.HhEnrolmentId = T4.Id
INNER JOIN PSPBranch T5 ON T3.PSPBranchId = T5.Id
INNER JOIN PSP T6 ON T5.PSPId = T6.Id
INNER JOIN SystemCodeDetail T7 ON T2.MonthId = T7.Id
where T4.HhId = @Id
  end

GO
/****** Object:  StoredProcedure [dbo].[GetBeneStatementSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[GetBeneStatementSummary]
@id int
as

 begin

select 

 COUNT(DISTINCT  T2.MemberId) AS Members
,COUNT(DISTINCT  CASE WHEN(T2.Status!='EX') THEN 1 ELSE 0 END) AS ActiveMembers   
,SUM( DISTINCT t4. EntitlementAmount + t4.  AdjustmentAmount ) AS PrepayrollAmount
,count( DISTINCT t4.PaymentCycleId ) AS Prepayrolls
,sum(DISTINCT t5.PaymentAmount ) AS Payrols
,Sum( DISTINCT t5.PaymentAmount ) AS PayrolAmount
,Sum( DISTINCT t6.TrxAmount ) AS SuccessfulCredit
,Sum( DISTINCT t6.TrxAmount ) AS SuccessfulCreditAmount
,COUNT(DISTINCT  t7.HhId ) AS Complaints
,COUNT(DISTINCT  t8.HhId ) AS Changes
,COUNT( DISTINCT case when (t7.Code<>'closed') then 1 else  NULL end) AS OpenComplaints
,COUNT( DISTINCT case when (t8.Code<>'closed') then 1 else NULL end)  AS OpenChanges

, MAX(T9.ProgrammeNo )  ProgrammeNo
, MAX(T9.AccountName )  AccountName
, MAX(T9.AccountNo )  AccountNo
 , 0 ActivityMonths
 

  FROM  Household T1

LEFT JOIN (SELECT DISTINCT T1.Id MemberId,  T2.Description 'Status', HhId, T3.Description Relationship, T4.Description MemberRole  FROM HouseholdMember T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId = T2.Id INNER JOIN SystemCodeDetail T3 ON T1.RelationshipId = T3.Id INNER JOIN SystemCodeDetail T4 ON T1.MemberRoleId = T4.Id) T2 ON T1.Id = T2.HhId
LEFT JOIN (SELECT DISTINCT   HhId, T1.SubLocationId, T2.Name FROM HouseholdSubLocation T1  INNER JOIN SubLocation T2 ON T1.SubLocationId =T2.Id) T3 ON T3.HhId = T1.Id
LEFT JOIN ( SELECT  DISTINCT T1.HhId, T1.AdjustmentAmount, T1.PaymentZoneCommAmt, t1.EntitlementAmount  , PaymentCycleId FROM Prepayroll T1 WHERE T1.HhId=@id) T4 ON T4.HhId = T1.Id
LEFT JOIN ( SELECT DISTINCT T1.HhId, T1.PaymentAmount , PaymentCycleId FROM Payroll T1 WHERE T1.HhId=@id) T5 ON T5.HhId = T1.Id
LEFT JOIN ( SELECT  DISTINCT T1.HhId, T1.TrxAmount , PaymentCycleId FROM Payment T1 WHERE T1.HhId=@id) T6 ON T6.HhId = T1.Id
LEFT JOIN ( SELECT DISTINCT T2.HhId, T1.ComplaintStatusId, t3.Code  FROM Complaint T1 inner join HouseholdEnrolment t2 on t1.HhEnrolmentId = t2.Id  INNER JOIN SystemCodeDetail T3 ON T1.ComplaintStatusId = T3.Id AND T2.HhId=@id) T7 ON T7.HhId = T1.Id
LEFT JOIN ( SELECT DISTINCT T2.HhId, T1.ChangeStatusId , t3.Code FROM Change T1 inner join HouseholdEnrolment t2 on t1.HhEnrolmentId = t2.Id  INNER JOIN SystemCodeDetail T3 ON T1.ChangeStatusId = T3.Id  AND T2.HhId=@id) T8 ON T8.HhId = T1.Id
 LEFT JOIN (  
		   SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn , T2.HhId, 

		   T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		
		   FROM BeneficiaryAccount 
		   T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id  
					INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id  
					INNER JOIN PSP T4 ON T3.PSPId=T4.Id  
		  
		  ) T9 ON T1.ID =T9.HhEnrolmentId  

		 
WHERE T2.HhId=@id

		    

end

GO
/****** Object:  StoredProcedure [dbo].[GetClassFromTable]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetClassFromTable](@TableName sysname )
AS
BEGIN
DECLARE @Result VARCHAR(max) = 'public class ' + @TableName + '{'
SELECT @Result = @Result + ' public ' + ColumnType + NullableSign + ' ' + ColumnName + ' { get; set; }'
FROM
( SELECT  REPLACE(col.name, ' ', '_') ColumnName, column_id ColumnId,
		CASE typ.name 
			WHEN 'bigint' THEN 'lONg'
			WHEN 'binary' THEN 'byte[]'
			WHEN 'bit' THEN 'bool'
			WHEN 'char' THEN 'string'
			WHEN 'date' THEN 'DateTime'
			WHEN 'datetime' THEN 'DateTime'
			WHEN 'datetime2' THEN 'DateTime'
			WHEN 'datetimeoffset' THEN 'DateTimeOffset'
			WHEN 'decimal' THEN 'decimal'
			WHEN 'float' THEN 'float'
			WHEN 'image' THEN 'byte[]'
			WHEN 'int' THEN 'int'
			WHEN 'mONey' THEN 'decimal'
			WHEN 'nchar' THEN 'string'
			WHEN 'ntext' THEN 'string'
			WHEN 'numeric' THEN 'decimal'
			WHEN 'nvarchar' THEN 'string'
			WHEN 'real' THEN 'double'
			WHEN 'smalldatetime' THEN 'DateTime'
			WHEN 'smallint' THEN 'short'
			WHEN 'smallmONey' THEN 'decimal'
			WHEN 'text' THEN 'string'
			WHEN 'time' THEN 'TimeSpan'
			WHEN 'timestamp' THEN 'DateTime'
			WHEN 'tinyint' THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varbinary' THEN 'byte[]'
			WHEN 'varchar' THEN 'string'
			else 'UNKNOWN_' + typ.name
		END ColumnType,
		CASE 
			WHEN col.is_nullable = 1 and typ.name in ('bigint', 'bit', 'date', 'datetime', 'datetime2', 'datetimeoffset', 'decimal', 'float', 'int', 'mONey', 'numeric', 'real', 'smalldatetime', 'smallint', 'smallmONey', 'time', 'tinyint', 'uniqueidentifier') 
			THEN '?' 
			else '' 
		END NullableSign
	FROM sys.columns col
		join sys.types typ ON
			col.system_type_id = typ.system_type_id AND col.user_type_id = typ.user_type_id
	where object_id = object_id(@TableName)
) t
ORDER BY ColumnId
SET @Result = @Result  + '
}'
print @Result
END

GO
/****** Object:  StoredProcedure [dbo].[GetComValBatchPendingIPRS]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[GetComValBatchPendingIPRS](
	@ComValListingAcceptId INT)
AS
BEGIN




	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)

	DECLARE @ConfirmedId INT = NULL
	DECLARE @CorrectedId INT = NULL
	DECLARE @PendingPersons INT = NULL
	
	SET @SysCode='Registration Status'
	SET @SysDetailCode='REGCORRECT'

	SELECT @CorrectedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='REGCONFIRM'
 SELECT @ConfirmedId = T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
 
 delete from temp_IPRS where ComValListingAcceptId = @ComValListingAcceptId
 ;with X as (
			SELECT T1.BeneNationalIdNo as IDNumber, CONCAT(T1.BeneFirstName, ' ' ,CASE WHEN(T1.BeneSurname<>'') 
THEN T1.BeneMiddleName ELSE '' END, ' ',T1.BeneSurname) AS Names
		FROM ComValListingPlanHH T1
			LEFT OUTER JOIN (SELECT ID_Number AS IDNo
			FROM IPRSCache) AS T2 ON CONVERT(bigint,REPLACE(T1.BeneNationalIdNo,'.',''))=CONVERT(bigint,T2.IDNo)
		WHERE T2.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId AND T1.StatusId IN (@CorrectedId,@ConfirmedId)
		AND T1.BeneNationalIdNo IS NOT NULL
	UNION
		SELECT T1.CgNationalIdNo, CONCAT(T1.CgFirstName, ' ' ,CASE WHEN(T1.CgMiddleName<>'') THEN T1.CgMiddleName ELSE '' END, ' ',T1.CgSurname) AS BeneName
		FROM
			ComValListingPlanHH T1 LEFT OUTER JOIN ( SELECT ID_Number AS IDNo
			FROM IPRSCache) AS T3 ON CONVERT(bigint,REPLACE(T1.CGNationalIDNo,'.',''))=CONVERT(bigint,T3.IDNo)
		WHERE T3.IDNo is null AND T1.ComValListingAcceptId = @ComValListingAcceptId AND T1.StatusId IN (@CorrectedId,@ConfirmedId) 
		AND T1.CgNationalIdNo IS NOT NULL
		)
		insert into temp_IPRS(IdNumber,Names,ComValListingAcceptId)
		SELECT IdNumber,Names,@ComValListingAcceptId as ComValListingAcceptId  FROM X


	select @PendingPersons = count(*) from temp_IPRS where ComValListingAcceptId = @ComValListingAcceptId
	UPDATE  ComValListingAccept SET PendingValidation = @PendingPersons, IsValidated = CASE WHEN @PendingPersons =0 THEN 1 ELSE 0 END  
WHERE Id =@ComValListingAcceptId


SELECT IdNumber,Names FROM temp_IPRS where ComValListingAcceptId = ComValListingAcceptId 

delete FROM temp_IPRS --where ComValListingAcceptId = ComValListingAcceptId  
END


GO
/****** Object:  StoredProcedure [dbo].[GetComValListingBatches]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetComValListingBatches]
	@TargetPlanId INT
,
	@CountyId int = null
,
	@ConstituencyId int = null
AS
BEGIN

	SELECT DISTINCT T1.*, TG.County, TG.Constituency, TG.ConstituencyId, T2.FirstName AS AcceptBy,
		T3.FirstName AS AcceptApvBy,T3.Id AS AcceptApvById, T4.Name AS TargetPlan
	FROM ComValListingAccept T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 
AS TG ON TG.ConstituencyId  = T1.ConstituencyId
			AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
			AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
			AND T1.TargetPlanId = @TargetPlanId
		LEFT JOIN [USER] T2 ON T1.AcceptById = T2.Id
		LEFT JOIN [USER] T3 ON T1.AcceptApvById = T3.Id
		LEFT JOIN TargetPlan T4 ON T1.TargetPlanId = T4.Id

END

GO
/****** Object:  StoredProcedure [dbo].[GetComValListingSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetComValListingSummary]
	@TargetPlanId int
	,
	@CountyId int = null
	,
	@ConstituencyId int = null
AS
BEGIN

	SELECT
		MAX(T1.ExceptionsFileId) AS ExceptionsFileId
, MAX(T1.ValidationFileId) AS ValidationFileId 
, SUM(CASE WHEN(T2.Id>0) THEN 1 ELSE 0 END) AS RegisteredHouseholds
, MAX(T5.AcceptedBatches) AS AcceptedBatches
, SUM(CASE WHEN(T2.ComValListingAcceptId>0) THEN 1 ELSE 0 END) AS AcceptedHouseholds
, SUM(CASE WHEN(T2.ComValListingAcceptId IS NULL AND T2.Id>0) THEN 1 ELSE 0 END) AS PendingHouseholds
, SUM(CASE WHEN(T4.CG_IDNoExists>0) THEN 1 ELSE 0 END) AS CG_IDNoExists
, SUM(CASE WHEN(T4.CG_SexMatch>0) THEN 1 ELSE 0 END) AS CGSexMatch
, SUM(CASE WHEN(T4.CG_DoBYearMatch>0) THEN 1 ELSE 0 END) AS CG_DoBYearMatch
, SUM(CASE WHEN(T4.CG_DoBMatch>0) THEN 1 ELSE 0 END) AS CG_DoBMatch
, SUM(CASE WHEN(T4.Bene_IDNoExists>0) THEN 1 ELSE 0 END) AS BeneIDNoExists
, SUM(CASE WHEN(T4.Bene_SexMatch>0) THEN 1 ELSE 0 END) AS BeneSexMatch
, SUM(CASE WHEN(T4.Bene_DoBYearMatch>0) THEN 1 ELSE 0 END) AS BeneDoBYearMatch
, SUM(CASE WHEN(T4.Bene_DoBMatch>0) THEN 1 ELSE 0 END) AS BeneDoBMatch
, MAX(T6.TargetPlanId) AS   TargetPlanId
, MAX(T6.RegHH) AS  RegHH 
, MAX(T6.ExitedHH ) AS   ExitedHH
, MAX(T6.ConfirmedHH) AS   ConfirmedHH
, MAX(T6.AmendedHH) AS   AmendedHH
, MAX(T6.CValHH) AS  CValHH 
, MAX(T6.PendingAcceptanceHH) AS   PendingAcceptanceHH

	FROM TargetPlan  T1
		LEFT JOIN ComValListingPlanHH T2 ON T1.Id =T2.TargetPlanId
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id)  AS TG ON TG.SubLocationId  = T2.SubLocationId
		LEFT JOIN ( SELECT DISTINCT Id, TargetPlanId , ReceivedHHs, ConstituencyId FROM ComValListingAccept)  T3 ON T1.Id =  T3.TargetPlanId AND T2.ComValListingAcceptId = T3.Id
		LEFT JOIN ( SELECT DISTINCT Id, CG_IDNoExists, CG_DoBYearMatch, CG_SexMatch, Bene_DoBMatch, Bene_DoBYearMatch, Bene_SexMatch, Bene_IDNoExists, CG_DoBMatch, TargetPlanId FROM ListingException)   T4 ON T1.Id =  T4.TargetPlanId AND T2.Id = T4.Id
		LEFT JOIN ( SELECT TargetPlanId, COUNT(Id) AS AcceptedBatches FROM ComValListingAccept GROUP BY TargetPlanId ) T5 ON T3.Id=T5.TargetPlanId

		LEFT JOIN ( 

		SELECT
		MAX(T2.TargetPlanId) TargetPlanId,
		 COUNT(T1.Id) AS RegHH,
		SUM(CASE  WHEN (T3.Code ='REGEXIT' ) THEN 1 ELSE 0 END)  AS ExitedHH,
		SUM(CASE  WHEN (T3.Code ='REGCONFIRM'  ) THEN 1 ELSE 0 END)  AS ConfirmedHH,
		SUM(CASE  WHEN (T3.Code ='REGCORRECT'  ) THEN 1 ELSE 0 END)  AS AmendedHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)>0    ) then 1 else 0 end)  AS CValHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)=0    ) then 1 else 0 end)  AS PendingAcceptanceHH FROM   ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 	
  AS TG ON TG.SubLocationId  = T1.SubLocationId AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
		AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END
		LEFT OUTER JOIN ComValListingPlanHH T2 ON T1.Id =T2.Id left outer JOIN SystemCodeDetail T3 ON T2.StatusId = T3.Id
	WHERE T2.TargetPlanId = @TargetPlanId ) T6 ON T2.TargetPlanId = T6.TargetPlanId
		
	WHERE  T1.Id =@TargetPlanId
		AND TG.ConstituencyId = CASE WHEN @ConstituencyId IS NOT NULL THEN @ConstituencyId ELSE TG.ConstituencyId END
		AND TG.CountyId = CASE WHEN @CountyId IS NOT NULL THEN @CountyId ELSE TG.CountyId END

END

GO
/****** Object:  StoredProcedure [dbo].[GetComValSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROC [dbo].[GetComValSummary]
	@Id int
AS
BEGIN
	SELECT
		COUNT(T1.Id) AS RegHH,
		SUM(CASE  WHEN (T3.Code ='REGEXIT' ) THEN 1 ELSE 0 END)  AS ExitedHH,
		SUM(CASE  WHEN (T3.Code ='REGCONFIRM'  ) THEN 1 ELSE 0 END)  AS ConfirmedHH,
		SUM(CASE  WHEN (T3.Code ='REGCORRECT'  ) THEN 1 ELSE 0 END)  AS AmendedHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)>0    ) then 1 else 0 end)  AS CValHH,
		SUM( CASE WHEN(ISNULL(T2.Id,0)=0    ) then 1 else 0 end)  AS PendingAcceptanceHH
	FROM   ListingPlanHH T1
		INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Id AS LocationId, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency, T7.Id AS ConstituencyId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id) 	
  AS TG ON TG.SubLocationId  = T1.SubLocationId AND T1.RejectById IS NULL
		LEFT OUTER JOIN ComValListingPlanHH T2 ON T1.Id =T2.Id left outer JOIN SystemCodeDetail T3 ON T2.StatusId = T3.Id
	WHERE T1.TargetPlanId = @Id 

END

GO
/****** Object:  StoredProcedure [dbo].[GetDisputedAccounts]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetDisputedAccounts]
@Type varchar(10) = 'SUMMARY'
as

BEGIN
IF @Type='SUMMARY'
begin

;WITH X AS (
select * from PspEnrolmentLog
UNION
select * from PspEnrolmentLogV1 
UNION
select * from PspEnrolmentLogV2 
 )
 SELECT DISTINCT 
 T1.EnrolmentNo,
 (SELECT p.Name FROM PSP p WHERE p.Code=T1.BankCode) 'Attempting PSP',
 (SELECT p.Name FROM PSPBranch p WHERE p.Code=T1.BranchCode) 'Attempting PSP Branch',
 -- T1.BankCode 'Attempting PSP',
 -- T1.BranchCode 'Attempting PSP Branch', 
 T1.AccountName 'Attempted Account Name',
 T1.AccountNo 'Attempted Account No',
 T1.TokenId 'Attempted ToKen',
    T4.Name 'Locked By PSP ',
    T3.Name 'Locked By PSP Branch',
   T2.AccountName 'Locked Account Name',
   T2.AccountNo 'Locked Account No',
   T6.TokenId 'Locked by Token',
   T2.OpenedOn 'Opened on',
   T2.DateAdded 'Locked Date',
   -- T14.PaymentAmount 'Payment Amount',
   CASE WHEN ISNULL(T14.PaymentAmount,0)=0 THEN 'NO' ELSE 'YES' END 'On Payroll',
   CASE WHEN ISNULL(T14.PaymentAmount,0)=0 THEN 0 ELSE CAST(T14.PaymentAmount AS MONEY) END 'Payment Amount',
   T15.Code 'Programme',
   T16.PriReciNationalIdNo 'National ID Number',
	T13.County,
	T13.Constituency,
	--T13.District,
	-- T13.Division,
	T13.Location
	-- T13.SubLocation
   FROM X T1 
 LEFT JOIN BeneficiaryAccount T2 ON  CONVERT(INT,T1.EnrolmentNo ) = T2.HhEnrolmentId
 INNER JOIN PSPBranch T3 ON T2.PSPBranchId = T3.Id
 INNER JOIN PSP T4 ON T3.PSPId = T4.Id -- AND T3.PSPId=T1.BankCode
 INNER JOIN HouseholdEnrolment T5 ON T2.HhEnrolmentId = T5.Id
 INNER JOIN Household T6 ON T6.Id= T5.HhId
 INNER JOIN HouseholdSubLocation T7 ON T6.Id=T7.HhId
 LEFT JOIN Payroll T14 ON T6.Id = T14.HhId AND T14.PaymentCycleId=7
 INNER JOIN Programme T15 ON T15.Id = T6.ProgrammeId
 LEFT JOIN BeneficiaryPaymentCard T16 ON T2.Id=T16.BeneAccountId -- AND T3.StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
 INNER JOIN GeoMaster T12 ON T7.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T7.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
WHERE T3.Code<>T1.BranchCode AND T4.Code<>T1.BankCode and t6.TokenId is not null and t1.TokenId <>0
ORDER BY T15.Code,T13.County, T13.Constituency, T13.Location
END
ELSE
BEGIN

;WITH X AS (
select * from PspEnrolmentLog
UNION
select * from PspEnrolmentLogV1
UNION
select * from PspEnrolmentLogV2
 )
 SELECT DISTINCT 
 T1.EnrolmentNo ,
 (SELECT p.Name FROM PSP p WHERE p.Code=T1.BankCode) 'Attempting PSP',
 (SELECT p.Name FROM PSPBranch p WHERE p.Code=T1.BranchCode) 'Attempting PSP Branch',
 -- T1.BankCode 'Attempting PSP',
 -- T1.BranchCode 'Attempting PSP Branch', 
 T1.AccountName 'Attempted Account Name',
 T1.AccountNo 'Attempted Account No',
 T1.TokenId 'Attempted TOKen',
 T1.AccountOpenedOn 'Account Opened on',
 T1.DateAdded 'Attempted Date',
    T4.Name 'Locked By PSP',
    T3.Name 'Locked By PSP Branch',
   T2.AccountName 'Locked Account Name',
   T2.AccountNo 'Locked Account No',
   T6.TokenId 'Locked by Token',
   t2.OpenedOn 'Opened on',
   T2.DateAdded 'Locked Date',
   -- T14.PaymentAmount 'Payment Amount',
   CASE WHEN ISNULL(T14.PaymentAmount,0)=0 THEN 'NO' ELSE 'YES' END 'On Payroll',
   CASE WHEN ISNULL(T14.PaymentAmount,0)=0 THEN 0 ELSE CAST(T14.PaymentAmount AS MONEY)  END 'Payment Amount',
   T15.Code 'Programme',
   T16.PriReciNationalIdNo 'National ID Number',
	T13.County,
	T13.Constituency,
	--T13.District,
	--T13.Division,
	T13.Location
	--T13.SubLocation
   FROM X T1 
 LEFT JOIN BeneficiaryAccount T2 ON  CONVERT(INT,T1.EnrolmentNo ) = T2.HhEnrolmentId
 INNER JOIN PSPBranch T3 ON T2.PSPBranchId = T3.Id
 INNER JOIN PSP  T4 ON T3.PSPId = T4.Id
 INNER JOIN HouseholdEnrolment T5 ON T2.HhEnrolmentId = T5.Id
 INNER JOIN Household T6 ON T6.Id= T5.HhId
 INNER JOIN HouseholdSubLocation T7 ON T6.Id=T7.HhId
 LEFT JOIN Payroll T14 ON T6.Id = T14.HhId AND T14.PaymentCycleId=7
 INNER JOIN Programme T15 ON T15.Id = T6.ProgrammeId
 LEFT JOIN BeneficiaryPaymentCard T16 ON T2.Id=T16.BeneAccountId -- AND T3.StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
 INNER JOIN GeoMaster T12 ON T7.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T7.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
 WHERE T3.Code<>T1.BranchCode AND T4.Code<>T1.BankCode and t6.TokenId is not null and t1.TokenId <>0
 ORDER BY T15.Code,T13.County, T13.Constituency, T13.Location
 END
 END


GO
/****** Object:  StoredProcedure [dbo].[GetDisputedAccountsMaster]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetDisputedAccountsMaster]
@Type varchar(10) = 'SUMMARY'
as

BEGIN
IF @Type='SUMMARY'
begin

;WITH X AS (
select * from PspEnrolmentLog
UNION
select * from PspEnrolmentLogV1 
UNION
select * from PspEnrolmentLogV2 
 )
 SELECT DISTINCT 
 T1.EnrolmentNo,
 T1.BankCode 'Attempting PSP',
 T1.BranchCode 'Attempting PSP Branch', 
 T1.AccountName 'Attempted Account Name',
 T1.AccountNo 'Attempted Account No',
 T1.TokenId 'Attempted ToKen',
    T4.Code 'Locked By PSP ',
    T3.Code 'Locked By PSP Branch',
   T2.AccountName 'Locked Account No',
   T2.AccountNo 'Locked Account Name',
   T6.TokenId 'Locked by Token',
   T2.OpenedOn 'Opened on',
   T2.DateAdded 'Locked Date'
   FROM X T1 
 LEFT JOIN BeneficiaryAccount T2 ON  CONVERT(INT,T1.EnrolmentNo ) = T2.HhEnrolmentId
 INNER JOIN PSPBranch T3 ON T2.PSPBranchId = T3.Id
 INNER JOIN PSP T4 ON T3.PSPId = T4.Id -- AND T3.PSPId=T1.BankCode
 INNER JOIN HouseholdEnrolment T5 ON T2.HhEnrolmentId = T5.Id
 INNER JOIN Household T6 ON T6.Id= T5.HhId
WHERE T3.Code<>T1.BranchCode AND T4.Code<>T1.BankCode and t6.TokenId is not null and t1.TokenId <>0
END
ELSE
BEGIN

;WITH X AS (
select * from PspEnrolmentLog
UNION
select * from PspEnrolmentLogV1
UNION
select * from PspEnrolmentLogV2
 )
 SELECT DISTINCT 
 T1.EnrolmentNo ,
 T1.BankCode 'Attempting PSP',
T1.BranchCode 'Attempting PSP Branch', 
 T1.AccountName 'Attempted Account Name',
 T1.AccountNo 'Attempted Account No',
 T1.TokenId 'Attempted TOKen',
 T1.AccountOpenedOn 'Account Opened on',
 T1.DateAdded 'Attempted Date',
    T4.Code 'Locked By PSP',
    T3.Code 'Locked By PSP Branch',
   T2.AccountName 'Locked Account No',
   T2.AccountNo 'Locked Account Name',
   T6.TokenId 'Locked by Token',
   t2.OpenedOn 'Opened on',
   T2.DateAdded 'Locked Date'
   FROM X T1 
 LEFT JOIN BeneficiaryAccount T2 ON  CONVERT(INT,T1.EnrolmentNo ) = T2.HhEnrolmentId
 INNER JOIN PSPBranch T3 ON T2.PSPBranchId = T3.Id
 INNER JOIN PSP  T4 ON T3.PSPId = T4.Id
 INNER JOIN HouseholdEnrolment T5 ON T2.HhEnrolmentId = T5.Id
 INNER JOIN Household T6 ON T6.Id= T5.HhId
 WHERE T3.Code<>T1.BranchCode AND T4.Code<>T1.BankCode and t6.TokenId is not null and t1.TokenId <>0
 END
 END


GO
/****** Object:  StoredProcedure [dbo].[GetEnroledBeneficiariesByProgrammeAndGender]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROC [dbo].[GetEnroledBeneficiariesByProgrammeAndGender] (@startDate datetime, @enddate datetime, @Programme varchar(10)
)

AS
BEGIN

select  County, [74] AS Male, [75] AS Female

FROM 

(
SELECT T1.Name 'County', T2.SexId, T2.BeneAcc FROM County  T1 LEFT JOIN 
( 
SELECT T1.Id 'BeneAcc',T13.County CountyName, t13.CountyId, T08.SexId, T09.[Description] 'Sex'
from   BeneficiaryAccount T1
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id   
  INNER JOIN HouseholdEnrolmentPlan T05 ON T5.HhEnrolmentPlanId = T05.Id AND T05.ApvOn BETWEEN @startDate AND @enddate
  INNER JOIN Household T6 ON T5.HhId=T6.Id
  inner join Programme t06 on t06.Id = t6.ProgrammeId
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
 INNER JOIN HouseholdMember T07 ON T6.ID = T07.HhId and t07.MemberRoleId = t06.PrimaryRecipientId
 INNER JOIN  Person T08 ON T07.PersonId = T08.Id 
  INNER JOIN  SystemCodeDetail T09 ON T08.SexId = T09.Id 
 

   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE  T1.StatusId=23  AND T4.Code=@Programme  
  ) T2 ON T1.id = T2.CountyId
  ) P
  PIVOT (COUNT (BeneAcc) FOR SexId IN ([74],[75]))AS PVT

END

GO
/****** Object:  StoredProcedure [dbo].[GetEnrolmentProgress]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetEnrolmentProgress]
as
BEGIN
 
 DECLARE @SysCode varchar(20)
DECLARE @SysDetailCode varchar(20)
DECLARE @SystemCodeDetailId1 int
DECLARE @SystemCodeDetailId2 int
DECLARE @SystemCodeDetailId3 int

SET @SysCode='Account Status'
SET @SysDetailCode='1'
SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



 select  T3.Name 'PSP', COUNT(T1.ID) 'Accounts Opened' from BeneficiaryAccount T1
INNER JOIN PSPBranch T2 ON T1.PSPBranchId = T2.Id
INNER JOIN PSP T3 ON T2.PSPId = T3.ID AND T1.DateAdded IS NOT NULL and T1.StatusId=@SystemCodeDetailId1
GROUP BY T3.Name
ORDER BY 1 ASC

SELECT T1.Name, 
CASE WHEN T2.[CT-OVC] IS NULL THEN 0 ELSE T2.[CT-OVC] END AS [CT-OVC] ,
CASE WHEN T2.OPCT IS NULL THEN 0 ELSE T2.OPCT END AS OPCT ,
CASE WHEN T2.[PwSD-CT]  IS NULL THEN 0 ELSE T2.[PwSD-CT]  END AS [PwSD-CT]  
,CASE WHEN T2.[CT-OVC] +T2.OPCT+ T2.[PwSD-CT] IS NULL THEN 0 ELSE T2.[CT-OVC] +T2.OPCT+ T2.[PwSD-CT] END AS TOTAL
FROM PSP T1
LEFT JOIN (
select PSPId,  PSP, [1] AS 'OPCT', [2] AS 'CT-OVC', [3] AS 'PwSD-CT' 

FROM 

(SELECT T1.Id 'BeneAcc', T3.Name 'PSP', T2.PSPId, T6.ProgrammeId
from   BeneficiaryAccount T1
  inner join  PSPBranch T2 ON T1.PSPBranchId = T2.Id
  INNER JOIN PSP T3 ON T3.Id = T2.PSPId
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id
  INNER JOIN Household T6 ON T5.HhId=T6.Id
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE T1.DateAdded IS NOT NULL and T1.StatusId=@SystemCodeDetailId1

  ) P
  PIVOT (COUNT (BeneAcc) FOR ProgrammeId IN ([1],[2],[3],[4])
   )AS PVT   -- ORDER BY PVT.PSP
   )
   T2 ON T2.PSPId = T1.Id

   ORDER BY 1 ASC

END 
GO
/****** Object:  StoredProcedure [dbo].[GetEnrolmentProgressReportByConstituencyAndProgramme]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetEnrolmentProgressReportByConstituencyAndProgramme]
@Programme VARCHAR(10) =''
AS
BEGIN
SELECT 
County, Constituency, [1] AS OPCT, [2] AS CTOVC, [3] AS PWSD, [1]+[2]+[3] AS Total
FROM 

(
SELECT 
T1.Name 'Constituency', T2.CountyName 'County', T1.Id, T2.*
FROM  Constituency T1
LEFT JOIN (
SELECT T1.Id 'BeneAcc', T13.County 'CountyName',  T13.Constituency 'ConstituencyName', T6.ProgrammeId
from   BeneficiaryAccount T1
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id
  INNER JOIN Household T6 ON T5.HhId=T6.Id
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE T1.DateAdded IS NOT NULL  AND T1.StatusId=23
 ) T2 ON T1.Name = T2.ConstituencyName

) P  

PIVOT (COUNT (BeneAcc) FOR ProgrammeId IN ([1],[2],[3]))AS PVT

ORDER BY PVT.County, PVT.Constituency
END



GO
/****** Object:  StoredProcedure [dbo].[GetEnrolmentProgressReportByCountyAndProgramme]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetEnrolmentProgressReportByCountyAndProgramme]
@Programme VARCHAR(10) =''
AS
BEGIN
select  County, [1] AS CoopBank, [2] AS PostBank, [3] AS EBL, [4] AS KCB

FROM 

(
SELECT T1.Name 'County', T1.Id, T2.* FROM County  T1 LEFT JOIN ( 
SELECT T1.Id 'BeneAcc',T13.County CountyName, T2.PSPId, T4.Name 'Programme'
from   BeneficiaryAccount T1
  inner join  PSPBranch T2 ON T1.PSPBranchId = T2.Id
  INNER JOIN PSP T3 ON T3.Id = T2.PSPId
  inner join HouseholdEnrolment T5 ON T1.HhEnrolmentId = T5.Id
  INNER JOIN Household T6 ON T5.HhId=T6.Id
   INNER JOIN HouseholdSubLocation T11 ON T6.Id=T11.HhId
   INNER JOIN  Programme T4 ON T4.Id = T6.ProgrammeId
   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T6.Name AS County,T6.Id AS CountyId,T7.Name AS Constituency
				FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
				INNER JOIN County T6 ON T7.CountyId=T6.Id
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T11.GeoMasterId=T13.GeoMasterId
  WHERE T1.DateAdded IS not NULL  AND T1.StatusId=23 AND T4.Code=@Programme
  ) T2 ON T1.Name = T2.CountyName
  ) P
  PIVOT (COUNT (BeneAcc) FOR PSPId IN ([1],[2],[3],[4]))AS PVT


   ORDER BY PVT.County
   END
   



GO
/****** Object:  StoredProcedure [dbo].[GetEnrolmentPspFiles]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetEnrolmentPspFiles]

     @UserId int

 AS

 BEGIN

     select 
	 distinct
     fc.Id, fc.[Name], fc.FilePath, fc.FileChecksum, fc.CreatedOn, fc.CreatedBy, fc.CreationTypeId, fc.TypeId,

     fd.Id as FileDownloadId, fd.FileCreationId, fd.FileChecksum as FileChecksum2 , fd.DownloadedBy, fd.DownloadedOn

     from FileCreation fc left outer  join FileDownload fd on fc.Id = fd.FileCreationId and fd.DownloadedBy = @UserId

						  INNER JOIN HouseholdEnrolmentPlan T3 ON fc.Id=T3.FileCreationId

	 WHERE fc.IsShared=1 and fc.id not in (27,28,29)


     order by fc.Id desc


	 

     END



GO
/****** Object:  StoredProcedure [dbo].[GetFailedPayrollTrx]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetFailedPayrollTrx]
	@PaymentCycleId int
AS
BEGIN
	SELECT T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T7.Id AS EnrolmentNo,T6.Name AS Bank,T5.Name AS Branch
		,T4.AccountNo,T4.AccountName,T1.PaymentAmount,T2.TrxNarration AS FailedReason
	FROM Payroll T1 INNER JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T3.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentcycleId AND T2.WasTrxSuccessful=0
END

GO
/****** Object:  StoredProcedure [dbo].[GetHouseholdInfo]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetHouseholdInfo]  
 @Id int  
AS  
BEGIN  
 DECLARE @SysCode varchar(20)  
 DECLARE @SysDetailCode varchar(20)  
 DECLARE @SystemCodeDetailId1 int  
 DECLARE @SystemCodeDetailId2 int  
 DECLARE @SystemCodeDetailId3 int  
 DECLARE @Len tinyint  
 DECLARE @Prefix varchar(10)  
 DECLARE @ProgNo int  
 DECLARE @HhId int  
 DECLARE @LoopVar int  
	
  
 SET @SysCode='Member Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Account Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Card Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
   
  SELECT @HhId=HhId FROM HouseholdEnrolment WHERE  id =@Id    
  
  IF @HhId>0  
  BEGIN  
   SELECT T2.Id AS ProgrammeId,T2.Code AS ProgrammeCode,T2.Name AS ProgrammeName  
	,T5.Id AS PriReciPersonId,T5.FirstName AS PriReciFirstName,T5.MiddleName AS PriReciMiddleName,T5.Surname AS PriReciSurname,T5.SexId AS PriReciSexId,T10.Code AS PriReciSex,T5.NationalIdNo AS PriReciNationalIdNo,T5.BirthCertNo AS PriReciBirthCertNo,T5.MobileNo1 AS PriReciMobileNo1,T5.MobileNo2 AS PriReciMobileNo2  
	,T6.Id AS SecReciPersonId,T6.FirstName AS SecReciFirstName,T6.MiddleName AS SecReciMiddleName,T6.Surname AS SecReciSurname,T6.SexId AS SecReciSexId,T11.Code AS SecReciSex,T6.NationalIdNo AS SecReciNationalIdNo,T6.BirthCertNo AS SecReciBirthCertNo,T6.MobileNo1 AS SecReciMobileNo1,T6.MobileNo2 AS SecReciMobileNo2  
	,T7.AccountNo,T7.AccountName,T7.PSPBranch,T7.PSP,T7.OpenedOn AS AccountOpenedOn  
	,T12.PaymentCardNo AS PaymentCardNo  
	,T9.SubLocationId,T9.SubLocationName,T9.LocationId,T9.LocationName,T9.DivisionId,T9.DivisionName,T9.DistrictId,T9.DistrictName,T9.CountyId,T9.CountyName,T9.ConstituencyId,T9.ConstituencyName  
   FROM Household T1 INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id  
		 INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId  
		 INNER JOIN HouseholdMember T4  ON T1.Id=T4.HhId  
		 INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1  
		 LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1  
		 LEFT JOIN (  
		   SELECT DISTINCT T1.HhEnrolmentId,T1.Id AS BeneAccountId,T1.AccountNo,T1.AccountName,T3.Name AS PSPBranch,T4.Name AS PSP,T1.OpenedOn  
		   FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id  
					INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id  
					INNER JOIN PSP T4 ON T3.PSPId=T4.Id  
		   WHERE T2.HhId=@HhId AND T1.StatusId=@SystemCodeDetailId2  
		  ) T7 ON T3.Id=T7.HhEnrolmentId  
		 INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId  
		 INNER JOIN (  
		   SELECT T1.Id AS SubLocationId,T1.Name AS SubLocationName,T2.Id AS LocationId,T2.Name AS LocationName,T3.Id AS DivisionId,T3.Name AS DivisionName,T5.Id AS DistrictId,T5.Name AS DistrictName,T6.Id AS CountyId,T6.Name AS CountyName,T7.Id AS ConstituencyId,T7.Name AS ConstituencyName,T8.Id AS GeoMasterId  
		   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id  
				INNER JOIN Division T3 ON T2.DivisionId=T3.Id  
				INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id  
				INNER JOIN District T5 ON T4.DistrictId=T5.Id  
				INNER JOIN County T6 ON T4.CountyId=T6.Id  
				INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id  
				INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1  
		  ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId  
		 INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id  
		 LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id  
		 LEFT JOIN (  
		   SELECT DISTINCT T1.BeneAccountId,T1.PaymentCardNo  
		   FROM BeneficiaryPaymentCard T1   
		   WHERE T1.StatusId=@SystemCodeDetailId3  
		  ) T12 ON T7.BeneAccountId=T12.BeneAccountId  
   WHERE T3.Id = @Id  
  
   RETURN  
  END  
 
 END  

GO
/****** Object:  StoredProcedure [dbo].[GetIndividualHHEnrolmentDetails]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[GetIndividualHHEnrolmentDetails]
    @HhEnrolmentPlanId   int  = null
     
AS
   BEGIN 

 	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    SET @SysCode='Card Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T2.Id AS EnrolmentNo
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,BPC.PriReciFirstName AS BeneFirstName
		,BPC.PriReciMiddleName AS BeneMiddleName
		,BPC.PriReciSurname AS BeneSurname
		,BPC.PriReciNationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,BPC.PriReciDoB AS BeneDoB
		,ISNULL(BPC.SecReciFirstName,'') AS CGFirstName
		,ISNULL(BPC.SecReciMiddleName,'') AS CGMiddleName
		,ISNULL(BPC.SecReciSurname,'') AS CGSurname
		,ISNULL(BPC.SecReciNationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(BPC.SecReciDoB,'') AS CGDoB
		,ISNULL(BPC.MobileNo1,'') AS MobileNo1
		,ISNULL(BPC.MobileNo2,'') AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,P.[Name] AS 'BANK'
		,PB.[NAME] AS 'BankBranch'
		,CASE WHEN BPC.PaymentCardNo IS NULL THEN 'Account Opened' ELSE 'Account Opened,Payment Card Issued' END  AS 'Status'
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
                                     INNER JOIN (SELECT MIN(Id) AS Id,HhEnrolmentId,PSPBranchId FROM BeneficiaryAccount WHERE StatusId=@SystemCodeDetailId1 GROUP BY HhEnrolmentId,PSPBranchId) BA ON BA.HhEnrolmentId = T2.Id
                                     INNER JOIN PSPBranch PB ON BA.PSPBranchId = PB.Id
									 INNER JOIN PSP P ON PB.PSPId = P.ID
									 LEFT JOIN (SELECT T1.BeneAccountId,T1.PaymentCardNo,T1.PriReciId,T1.PriReciFirstName,T1.PriReciMiddleName,T1.PriReciSurname,T1.PriReciSexId,T1.PriReciDoB,T1.PriReciNationalIdNo,T1.SecReciId,T1.SecReciFirstName,T1.SecReciMiddleName,T1.SecReciSurname,T1.SecReciSexId,T1.SecReciDoB,T1.SecReciNationalIdNo,T1.MobileNo1,T1.MobileNo2
												 FROM BeneficiaryPaymentCard T1 INNER JOIN (
																							   SELECT BeneAccountId,MIN(Id) AS Id 
																							   FROM BeneficiaryPaymentCard 
																							   WHERE StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3) 
																							   GROUP BY BeneAccountId
																							) T2 ON T1.Id=T2.Id
												) BPC ON BPC.BeneAccountId = BA.Id
								     LEFT JOIN SystemCodeDetail T7 ON BPC.PriReciSexId=T7.Id
								     LEFT JOIN SystemCodeDetail T10 ON BPC.SecReciSexId=T10.Id
  WHERE T1.Id=ISNULL(@HHenrolmentPlanId,T1.Id)
END

GO
/****** Object:  StoredProcedure [dbo].[GetOpenedAccountsPerPSP]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[GetOpenedAccountsPerPSP]

AS
BEGIN

DECLARE @SysCode varchar(20)
DECLARE @SysDetailCode varchar(20)
DECLARE @SystemCodeDetailId1 int
DECLARE @SystemCodeDetailId2 int
DECLARE @SystemCodeDetailId3 int

SET @SysCode='Account Status'
SET @SysDetailCode='1'
SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


SELECT T2.Id AS EnrolmentNo
,T14.AccountNo,T14.AccountName,T14.OpenedOn
	,T15.Name AS PSPBranch,T16.Name AS PSP
		,T4.Code AS Programme
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM 
	
	HouseholdEnrolmentPlan T1    INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN BeneficiaryAccount T14 ON T14.HhEnrolmentId = T2.Id
								   INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id
								   INNER JOIN PSP T16 ON T15.PSPId = T16.Id
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
	--CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!

	WHERE T14.StatusId=@SystemCodeDetailId1 
AND T14.DateAdded IS NOT NULL  AND T3.TokenId IS NOT NULL 
ORDER BY T14.HhEnrolmentId, T16.Name, T15.Name
END

GO
/****** Object:  StoredProcedure [dbo].[GetPaymentCycleDetailOptions]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPaymentCycleDetailOptions]
	@PaymentCycleId int
   ,@ProgrammeId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @EntitlementAmount money


	IF EXISTS(	SELECT 1
				FROM PaymentCycleDetail
				WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
					AND PaymentStageId NOT IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='Payment Stage' AND T1.Code='CLOSED')
			)
	BEGIN
		RAISERROR('The specified programme seems to be having a running payment cycle which needs to be conluded first',16,1)
		RETURN
	END

	
	SELECT T1.EntitlementAmount
	FROM Programme T1
	WHERE T1.Id=@ProgrammeId

	SET @SysCode='Enrolment Group'
	SELECT @EntitlementAmount=EntitlementAmount FROM Programme WHERE Id=@ProgrammeId

	SELECT DISTINCT T1.Id AS EnrolmentGroupId,T1.Code AS EnrolmentGroup,@EntitlementAmount AS EntitlementAmount
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
							 INNER JOIN HouseholdEnrolmentPlan T3 ON T1.Id=T3.EnrolmentGroupId AND T3.ApvBy IS NOT NULL
	WHERE T2.Code=@SysCode AND T3.ProgrammeId=@ProgrammeId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPaymentCycleOptions]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPaymentCycleOptions]
AS
BEGIN
    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)

    DECLARE @FromMonthId int
    DECLARE @ToMonthId int
    DECLARE @CurFromMonthId int
    DECLARE @CurToMonthId int

    DECLARE @FinancialYearId int
    DECLARE @FinancialYear varchar(30)
    DECLARE @FromMonth  varchar(30)
    DECLARE @ToMonth  varchar(30)

    IF EXISTS(	SELECT 1
    FROM PaymentCycle
    where StatusId
				    IN(SELECT T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id
    WHERE T2.Code='Payment Status' AND T1.Code='PAYMENTOPEN')
			)
	BEGIN
        RAISERROR('There seems  to be having a running payment cycle which needs to be concluded first',16,1)
        RETURN
    END
	ELSE
	BEGIN




        SELECT @FromMonthId=T2.Id, @CurFromMonthId = CAST( T2.Code AS INT)
			  , @ToMonthId=T3.Id, @CurToMonthId = CAST( T3.Code AS INT)
              , @FinancialYearId = T1.FinancialYearId



        FROM PaymentCycle T1
            INNER JOIN SystemCodeDetail T2 ON T1.FromMonthId=T2.Id
            INNER JOIN SystemCodeDetail T3 ON T1.ToMonthId=T3.Id
            INNER JOIN SystemCodeDetail T4 ON T1.StatusId=T4.Id
        WHERE T1.Id=(SELECT MAX(Id)
        FROM PaymentCycle)


        PRINT @CurFromMonthId

        PRINT @CurToMonthId

        --IF(ISNULL(@FinancialYearId,'' )='')

        SELECT @FinancialYearId =  T1.Id
        FROM SystemCodeDetail T1
        WHERE T1.Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

		IF EXISTS(SELECT 1 FROM PaymentCycle)
		BEGIN
			SELECT @FromMonthId=T3.Code+1
				  ,@ToMonthId=(T4.Code)+2 --BY DEFAULT THE PAYMENT CYCLES ARE BI-MONTHLY. ADDITIONAL LOGIC SHOULD TAKE CARE OF PROGRAMMES WITH VARYING FREQUENCIES
			FROM PaymentCycle T1 INNER JOIN SystemCodeDetail T3 ON T1.ToMonthId=T3.Id
								 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
			WHERE T1.Id=(SELECT MAX(Id) FROM PaymentCycle)
		
			SELECT @FromMonthId=T1.Id,@FromMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CONVERT(varchar(30),CASE WHEN(@FromMonthId>12) THEN @FromMonthId-12 ELSE @FromMonthId END)
			SELECT @ToMonthId=T1.Id, @ToMonth=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code='Calendar Months' AND T1.Code=CONVERT(varchar(30),CASE WHEN(@ToMonthId>12) THEN @ToMonthId-12 ELSE @ToMonthId END)
		END
        SELECT @FinancialYearId =  Id, @FinancialYear = [Description]
        FROM SystemCodeDetail
        WHERE Id = (SELECT [Description]
        FROM SystemCodeDetail
        WHERE Code='CURFINYEAR')

        SELECT @FromMonthId AS 'FromMonthId', @ToMonthId as 'ToMonthId', @FromMonth as 'FromMonth' , @ToMonth as 'ToMonth', @FinancialYearId as 'FinancialYearId', @FinancialYear as 'FinancialYear'


    END


END

GO
/****** Object:  StoredProcedure [dbo].[GetPaymentCyclesDueForFundsRequest]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[GetPaymentCyclesDueForFundsRequest]
	 
AS
BEGIN

	DECLARE @SysCode2 varchar(30)
	DECLARE @SysDetailCode2 varchar(30)
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @SystemCodeDetailId2 int



	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode


    SET @SysCode2='Payment Stage'
	SET @SysDetailCode2='FUNDSREQUEST'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode2 AND T1.Code=@SysDetailCode2



	SELECT  PC.ID, FY.[Description] +' ' + FM.[Description] +' ' + TM.[Description] AS  [Name]  FROM PaymentCycle PC
	LEFT JOIN SystemCodeDetail FM ON FM.Id = PC.FromMonthId
	LEFT JOIN SystemCodeDetail TM ON TM.Id = PC.ToMonthId
	LEFT JOIN SystemCodeDetail FY ON PC.FinancialYearId = FY.Id
	 WHERE PC.StatusId = @SystemCodeDetailId1 AND 
	PC.Id NOT IN (SELECT Id FROM FundsRequest) AND 
    PC.Id   IN (SELECT  PaymentCycleId FROM  PaymentCycleDetail WHERE PaymentStageId =@SystemCodeDetailId2  ) 
    	 
END

GO
/****** Object:  StoredProcedure [dbo].[GetPayrollSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPayrollSummary]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
AS
BEGIN
	SELECT COUNT(T1.HhId) AS TotalPayrollHhs
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollExceptionsHhs
		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidIDHhs
		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PayrollDuplicateIDHhs
		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentCardHhs
		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PayrollIneligibleHhs
		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollSuspiciousHhs
		,SUM(T1.PaymentAmount) AS TotalPayrollAmount
	FROM Payroll T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPayrollTrxSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPayrollTrxSummary]
	@PaymentCycleId int
AS
BEGIN
	SELECT T6.Name AS PSP,COUNT(T1.HhId) AS OnPayroll,COUNT(T2.HhId) AS ProcessedPayment,SUM(CASE WHEN(ISNULL(T2.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments,SUM(CASE WHEN(T2.WasTrxSuccessful=0) THEN 1 ELSE 0 END) AS FailedPayments,COUNT(T1.HhId)-COUNT(T2.HhId) AS PendingPayments
	FROM Payroll T1 LEFT JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T3.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
	WHERE T1.PaymentCycleId=@PaymentcycleId
	GROUP BY T6.Code,T6.Name
END

GO
/****** Object:  StoredProcedure [dbo].[GetPendingAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPendingAccountMonthlyActivity]
	@MonthNo tinyint
   ,@Year int
   ,@PSPId int = null
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int

	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T5.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T5.ProgrammeNo)))+CONVERT(varchar(6),T5.ProgrammeNo) AS ProgrammeNo,T5.Id AS EnrolmentNo,T3.Name AS Bank,T2.Name AS Branch,T1.AccountNo,T1.AccountName, T1.OpenedOn
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
							   LEFT JOIN (
											SELECT T2.BeneAccountId,T2.HadUniqueWdl,T2.UniqueWdlTrxNo,T2.UniqueWdlDate,T2.HadBeneBiosVerified,T2.IsDormant,T2.DormancyDate,T2.IsDueForClawback,T2.ClawbackAmount
											FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
											WHERE T1.MonthId=@SystemCodeDetailId1 AND T1.[Year]=@Year
										) T4 ON T1.Id=T4.BeneAccountId
							   INNER JOIN HouseholdEnrolment T5 ON T1.HhEnrolmentId=T5.Id
	WHERE T1.StatusId=@SystemCodeDetailId2 AND DATEDIFF(MM,T1.OpenedOn,CONVERT(datetime,'1 '+dbo.fn_MonthName(@MonthNo,0)+' '+CONVERT(varchar(4),@Year)))>=0 AND T4.BeneAccountId IS NULL AND T3.Id=ISNULL(@PSPId,T3.Id)
END

GO
/****** Object:  StoredProcedure [dbo].[GetPendingBeneForPSPEnrolment]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPendingBeneForPSPEnrolment]
	@HhEnrolmentPlanId int=NULL
AS
BEGIN 

 	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId int

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T2.Id AS EnrolmentNo
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId AND T8.StatusId=@SystemCodeDetailId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
								   LEFT JOIN (SELECT DISTINCT HhEnrolmentId FROM BeneficiaryAccount) T14 ON T2.Id=T14.HhEnrolmentId
  WHERE T1.Id=ISNULL(@HHenrolmentPlanId,T1.Id) AND T14.HhEnrolmentId IS NULL
END

GO
/****** Object:  StoredProcedure [dbo].[GetPendingMonthlyActivityAccounts]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[GetPendingMonthlyActivityAccounts]

@MonthNo tinyint
   ,@Year int
   as
   begin
DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int

	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T3.Name AS PSPName, T2.Name AS PSPBranch,	T1.AccountNo, T1.AccountName, T1.OpenedOn, T1.HhEnrolmentId AS EnrolmentNo
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
							   LEFT JOIN (
											SELECT T2.BeneAccountId,T2.HadUniqueWdl,T2.UniqueWdlTrxNo,T2.UniqueWdlDate,T2.HadBeneBiosVerified,T2.IsDormant,T2.DormancyDate,T2.IsDueForClawback,T2.ClawbackAmount
											FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
											WHERE T1.MonthId=@SystemCodeDetailId1 AND T1.[Year]=@Year
										) T4 ON T1.Id=T4.BeneAccountId
	WHERE T1.StatusId=@SystemCodeDetailId2
	 AND DATEDIFF(MM,T1.OpenedOn,CONVERT(datetime,'1 '+dbo.fn_MonthName(@MonthNo,0)+' '+CONVERT(varchar(4),@Year)))>=0
	    AND T4.BeneAccountId IS NULL 
		--AND ( T1.DateAdded IS NULL OR T1.DateAdded<=CONVERT(datetime,'1 '+dbo.fn_MonthName(@MonthNo,0)+' '+CONVERT(varchar(4),@Year)))

	 end


	  
GO
/****** Object:  StoredProcedure [dbo].[GetPendingPayrollTrx]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPendingPayrollTrx]
	@PaymentCycleId int
AS
BEGIN
	SELECT T7.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T7.ProgrammeNo)))+CONVERT(varchar(6),T7.ProgrammeNo) AS ProgrammeNo,T7.Id AS EnrolmentNo,T6.Name AS Bank,T5.Name AS Branch
		,T4.AccountNo,T4.AccountName,T1.PaymentAmount,T2.TrxNarration AS FailedReason
	FROM Payroll T1 LEFT JOIN Payment T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T3.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN HouseholdEnrolment T7 ON T1.HhId=T7.HhId
	WHERE T1.PaymentCycleId=@PaymentcycleId AND T2.HhId IS NULL
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonByEnrolmentNo]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
CREATE  PROC [dbo].[GetPersonByEnrolmentNo]

	@Id int, @TYPE INT =0

AS

BEGIN

IF(@TYPE<>0)
BEGIN
	SELECT  

		T4.*

	from HouseholdEnrolment T1

		inner JOIN Household T2 ON T1.HhId = T2.ID

		INNER JOIN HouseholdMember T3 ON T2.Id = T3.HhId

		INNER JOIN Person T4 ON T3.PersonId = T4.ID

		INNER JOIN SystemCodeDetail T5 ON T3.RelationshipId = T5.Id

		INNER JOIN SystemCodeDetail T6 ON T3.MemberRoleId = T6.Id

			--AND T5.Code!='BENEFICIARY'

			--AND T5.Code!='CAREGIVER'

			AND T1.Id = @Id

END
ELSE
BEGIN
	SELECT  

		T4.*

	from HouseholdEnrolment T1

		inner JOIN Household T2 ON T1.HhId = T2.ID

		INNER JOIN HouseholdMember T3 ON T2.Id = T3.HhId

		INNER JOIN Person T4 ON T3.PersonId = T4.ID

		INNER JOIN SystemCodeDetail T5 ON T3.RelationshipId = T5.Id

		INNER JOIN SystemCodeDetail T6 ON T3.MemberRoleId = T6.Id

		AND T5.Code!='BENEFICIARY'

		 AND T5.Code!='CAREGIVER'

			AND T1.Id = @Id




END

END

GO
/****** Object:  StoredProcedure [dbo].[GetPostPayrollSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPostPayrollSummary]
	@PaymentCycleId int
AS
BEGIN
	SELECT COUNT(T8.HhId) AS TotalPayrollHhs
		,SUM(T8.PaymentAmount) AS TotalPayrollAmount
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 0 ELSE 1 END) AS SuccessfulPayments
		,SUM(T1.TrxAmount) AS SuccessfulPaymentsAmount
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN 1 ELSE 0 END) AS FailedPayments
		,SUM(CASE WHEN(ISNULL(T1.WasTrxSuccessful,0)=0) THEN T8.PaymentAmount ELSE 0 END) AS FailedPaymentsAmount
		,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentExceptionsHhs

		,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs
		,SUM(CASE WHEN(T2.HhId>1 AND T2.ExceptionTypeCode='INVALIDBENEID') THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs_BENE
		,SUM(CASE WHEN(T2.HhId>1 AND T2.ExceptionTypeCode='INVALIDCGID') THEN 1 ELSE 0 END) AS PaymentInvalidIDHhs_CG

		,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_BENEIN
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_BENEACC
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATECGIDIN') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_CGIN
		,SUM(CASE WHEN(T3.HhId>1 AND T2.ExceptionTypeCode='DUPLICATECGIDACC') THEN 1 ELSE 0 END) AS PaymentDuplicateIDHhs_CGAC

		,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentAccountHhs
		,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PaymentInvalidPaymentCardHhs

		,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PaymentIneligibleHhs
		,SUM(CASE WHEN(T6.HhId>1 AND T2.ExceptionTypeCode='INELIGIBLEBENE') THEN 1 ELSE 0 END) AS PaymentIneligibleHhs_BENE
		,SUM(CASE WHEN(T6.HhId>1 AND T2.ExceptionTypeCode='INELIGIBLECG') THEN 1 ELSE 0 END) AS PaymentIneligibleHhs_CG

		,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PaymentSuspiciousHhs
	FROM Payment T1 LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
					LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
					INNER JOIN Payroll T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.HhId=T8.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPrepayrollPaymentCycles]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetPrepayrollPaymentCycles] (@Code VARCHAR(50) = NULL)

as
BEGIN
 
select CAST(PD.PaymentCycleId AS VARCHAR(5)) + '-'+CAST(PD.ProgrammeId AS VARCHAR(5))  AS "ID", 
-- P.*,F.Code, F.Code as 'FinancialYear',FM.Code 'FromMonth', TM.Code 'ToMonth', PR.Name 'Programme', PR.EntitlementAmount
F.Code +' '+ FM.[Description] +' '+ TM.[Description] + ' ' + PR.Name as 'Name'
 

 from PaymentCycleDetail PD    
INNER JOIN PaymentCycle P ON P.Id = PD.PaymentCycleId
INNER JOIN SystemCodeDetail F ON P.FinancialYearId = F.Id
INNER JOIN SystemCodeDetail FM ON P.FromMonthId = FM.Id
INNER JOIN SystemCodeDetail TM ON P.ToMonthId = TM.Id
INNER JOIN SystemCodeDetail PS ON PD.PaymentStageId = PS.Id AND PS.Code = @Code
INNER JOIN Programme PR ON PD.ProgrammeId = PR.Id




END


GO
/****** Object:  StoredProcedure [dbo].[GetPrepayrollSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPrepayrollSummary]
	@PaymentCycleId int
   ,@ProgrammeId tinyint
AS
BEGIN
	SELECT MAX(T2.ExceptionsFileId) AS ExceptionsFileId,MAX(T2.EnrolledHHs) AS EnroledHhs,COUNT(DISTINCT T1.HhId) AS EnrolmentGroupHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN 1 ELSE 0 END) AS PayrollHhs
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.EntitlementAmount,0) ELSE 0 END) AS PayrollEntitlementAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN ISNULL(T1.AdjustmentAmount,0) ELSE 0 END) AS PayrollAdjustmentAmount
		,SUM(CASE WHEN(T3.PaymentCycleId IS NULL AND T4.PaymentCycleId IS NULL AND T5.PaymentCycleId IS NULL AND T6.PaymentCycleId IS NULL AND T7.PaymentCycleId IS NULL AND T8.PaymentCycleId IS NULL) THEN (ISNULL(T1.EntitlementAmount,0)+ISNULL(T1.AdjustmentAmount,0)) ELSE 0 END) AS PayrollAmount
		--EXCEPTION:INVALID NATIONAL ID
		,COUNT(DISTINCT CASE WHEN(TT3.PaymentCycleId>0) THEN TT3.PersonId ELSE NULL END) AS InvalidNationalIDHhs
		,COUNT(DISTINCT CASE WHEN(TT3.PaymentCycleId>0 AND TT3.ExceptionTypeCode='INVALIDBENEID') THEN TT3.PersonId ELSE NULL END) AS InvalidNationalIDHhs_BENE
		,COUNT(DISTINCT CASE WHEN(TT3.PaymentCycleId>0 AND TT3.ExceptionTypeCode='INVALIDCGID') THEN TT3.PersonId ELSE NULL END) AS InvalidNationalIDHhs_CG
		,COUNT(DISTINCT CASE WHEN(TA3.PaymentCycleId>0) THEN TA3.PersonId ELSE NULL END) AS ActionedInvalidNationalIDHhs
		,COUNT(DISTINCT CASE WHEN(TA3.PaymentCycleId>0 AND TA3.ExceptionTypeCode='INVALIDBENEID') THEN TA3.PersonId ELSE NULL END) AS ActionedInvalidNationalIDHhs_BENE
		,COUNT(DISTINCT CASE WHEN(TA3.PaymentCycleId>0 AND TA3.ExceptionTypeCode='INVALIDBENEID') THEN TA3.PersonId ELSE NULL END) AS ActionedInvalidNationalIDHhs_CG
		----EXCEPTION:DUPLICATE NATIONAL ID
		,COUNT(DISTINCT CASE WHEN(TT4.PaymentCycleId>0) THEN TT4.PersonId ELSE NULL END) AS DuplicateNationalIDHhs
		,COUNT(DISTINCT CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN TT4.PersonId ELSE NULL END) AS DuplicateNationalIDHhs_BENEIN
		,COUNT(DISTINCT CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN TT4.PersonId ELSE NULL END) AS DuplicateNationalIDHhs_BENEACC
		,COUNT(DISTINCT CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATECGIDIN') THEN TT4.PersonId ELSE NULL END) AS DuplicateNationalIDHhs_CGIN
		,COUNT(DISTINCT CASE WHEN(TT4.PaymentCycleId>0 AND TT4.ExceptionTypeCode='DUPLICATECGIDACC') THEN TT4.PersonId ELSE NULL END) AS DuplicateNationalIDHhs_CGACC
		,COUNT(DISTINCT CASE WHEN(TA4.PaymentCycleId>0) THEN TA4.PersonId ELSE NULL END) AS ActionedDuplicateNationalIDHhs
		,COUNT(DISTINCT CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATEBENEIDIN') THEN TA4.PersonId ELSE NULL END) AS ActionedDuplicateNationalIDHhs_BENEIN
		,COUNT(DISTINCT CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATEBENEIDACC') THEN TA4.PersonId ELSE NULL END) AS ActionedDuplicateNationalIDHhs_BENEACC
		,COUNT(DISTINCT CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATECGIDIN') THEN TA4.PersonId ELSE NULL END) AS ActionedDuplicateNationalIDHhs_CGIN
		,COUNT(DISTINCT CASE WHEN(TA4.PaymentCycleId>0 AND TA4.ExceptionTypeCode='DUPLICATECGIDACC') THEN TA4.PersonId ELSE NULL END) AS ActionedDuplicateNationalIDHhs_CGACC
		--EXCEPTION:INVALID PAYMENT ACCOUNT
		,COUNT(DISTINCT CASE WHEN(TT5.PaymentCycleId>0) THEN TT5.HhId ELSE NULL END) AS InvalidPaymentAccountHhs
		,COUNT(DISTINCT CASE WHEN(TA5.PaymentCycleId>0) THEN TA5.HhId ELSE NULL END) AS ActionedInvalidPaymentAccountHhs
		--EXCEPTION:INVALID PAYMENT CARD
		,COUNT(DISTINCT CASE WHEN(TT6.PaymentCycleId>0) THEN TT6.HhId ELSE NULL END) AS InvalidPaymentCardHhs
		,COUNT(DISTINCT CASE WHEN(TA6.PaymentCycleId>0) THEN TA6.HhId ELSE NULL END) AS ActionedInvalidPaymentCardHhs
		--EXCEPTION:INELIGIBLE
		,COUNT(DISTINCT CASE WHEN(TT7.PaymentCycleId>0) THEN TT7.HhId ELSE NULL END) AS IneligibleHhs
		,COUNT(DISTINCT CASE WHEN(TT7.PaymentCycleId>0 AND TT7.ExceptionTypeCode='INELIGIBLEBENE') THEN TT7.HhId ELSE NULL END) AS IneligibleHhs_BENE
		,COUNT(DISTINCT CASE WHEN(TT7.PaymentCycleId>0 AND TT7.ExceptionTypeCode='INELIGIBLECG') THEN TT7.HhId ELSE NULL END) AS IneligibleHhs_CG
		,COUNT(DISTINCT CASE WHEN(TA7.PaymentCycleId>0) THEN TA7.HhId ELSE NULL END) AS ActionedIneligibleHhs
		,COUNT(DISTINCT CASE WHEN(TA7.PaymentCycleId>0 AND TA7.ExceptionTypeCode='INELIGIBLEBENE') THEN TA7.HhId ELSE NULL END) AS ActionedIneligibleHhs_BENE
		,COUNT(DISTINCT CASE WHEN(TA7.PaymentCycleId>0 AND TA7.ExceptionTypeCode='INELIGIBLECG') THEN TA7.HhId ELSE NULL END) AS ActionedIneligibleHhs_CG
		--EXCEPTION:SUSPICIOUS PAYMENT
		,COUNT(DISTINCT CASE WHEN(TT8.PaymentCycleId>0) THEN TT8.HhId ELSE NULL END) AS SuspiciousPaymentHhs
		,COUNT(DISTINCT CASE WHEN(TA8.PaymentCycleId>0) THEN TA8.HhId ELSE NULL END) AS ActionedSuspiciousPaymentHhs
		,MAX(CASE WHEN(TA3.PaymentCycleId>0
					   OR TA4.PaymentCycleId>0
					   OR TA5.PaymentCycleId>0
					   OR TA6.PaymentCycleId>0
					   OR TA7.PaymentCycleId>0
					   OR TA8.PaymentCycleId>0) THEN 1 ELSE 0 END) AS PayrollHasBeenActioned
	FROM Prepayroll T1 INNER JOIN PaymentCycleDetail T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy IS NULL) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HHId
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT3 ON T1.PaymentCycleId=TT3.PaymentCycleId AND T1.ProgrammeId=TT3.ProgrammeId AND T1.HhId=TT3.HHId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollInvalidID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) TA3 ON T1.PaymentCycleId=TA3.PaymentCycleId AND T1.ProgrammeId=TA3.ProgrammeId AND T1.HhId=TA3.HHId
					   
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy IS NULL) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HHId
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT4 ON T1.PaymentCycleId=TT4.PaymentCycleId AND T1.ProgrammeId=TT4.ProgrammeId AND T1.HhId=TT4.HHId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.PersonId FROM PrepayrollDuplicateID T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id WHERE T1.ActionedBy>0) TA4 ON T1.PaymentCycleId=TA4.PaymentCycleId AND T1.ProgrammeId=TA4.ProgrammeId AND T1.HhId=TA4.HHId
					   
					   LEFT JOIN PrepayrollInvalidPaymentAccount T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId AND T5.ActionedBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentAccount TT5 ON T1.PaymentCycleId=TT5.PaymentCycleId AND T1.ProgrammeId=TT5.ProgrammeId AND T1.HhId=TT5.HhId
								LEFT JOIN PrepayrollInvalidPaymentAccount TA5 ON T1.PaymentCycleId=TA5.PaymentCycleId AND T1.ProgrammeId=TA5.ProgrammeId AND T1.HhId=TA5.HhId AND TA5.ActionedBy>0
					   
					   LEFT JOIN PrepayrollInvalidPaymentCard T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId AND T6.ActionedBy IS NULL
							LEFT JOIN PrepayrollInvalidPaymentCard TT6 ON T1.PaymentCycleId=TT6.PaymentCycleId AND T1.ProgrammeId=TT6.ProgrammeId AND T1.HhId=TT6.HhId
								LEFT JOIN PrepayrollInvalidPaymentCard TA6 ON T1.PaymentCycleId=TA6.PaymentCycleId AND T1.ProgrammeId=TA6.ProgrammeId AND T1.HhId=TA6.HhId AND TA6.ActionedBy>0
					   
					   LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedBy FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId AND T7.ActionedBy IS NULL
							LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TT7 ON T1.PaymentCycleId=TT7.PaymentCycleId AND T1.ProgrammeId=TT7.ProgrammeId AND T1.HhId=TT7.HhId
								LEFT JOIN (SELECT DISTINCT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T2.Code AS ExceptionTypeCode,T1.ActionedBy FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id) TA7 ON T1.PaymentCycleId=TA7.PaymentCycleId AND T1.ProgrammeId=TA7.ProgrammeId AND T1.HhId=TA7.HhId AND TA7.ActionedBy>0
					   
					   LEFT JOIN PrepayrollSuspicious T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.HhId=T8.HhId AND T8.ActionedBy IS NULL
							LEFT JOIN PrepayrollSuspicious TT8 ON T1.PaymentCycleId=TT8.PaymentCycleId AND T1.ProgrammeId=TT8.ProgrammeId AND T1.HhId=TT8.HhId
								LEFT JOIN PrepayrollSuspicious TA8 ON T1.PaymentCycleId=TA8.PaymentCycleId AND T1.ProgrammeId=TA8.ProgrammeId AND T1.HhId=TA8.HhId AND TA8.ActionedBy>0
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
END

GO
/****** Object:  StoredProcedure [dbo].[GetProgOfficerLocations]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[GetProgOfficerLocations]
	@CountyId int = NULL,
	@ConstituencyId int = NULL
AS
BEGIN

	IF(@CountyId IS NOT NULL AND @ConstituencyId IS NOT NULL)
 BEGIN
		SELECT DISTINCT T2.*
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		WHERE T7.ID = @ConstituencyId
	END

	IF(@CountyId IS NOT NULL AND @ConstituencyId IS   NULL)
 BEGIN
		SELECT DISTINCT T2.*
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
		WHERE T6.Id = @CountyId
	END

 ELSE 
 BEGIN
		SELECT TOP 0
			*
		FROM [Location]
	END

END


GO
/****** Object:  StoredProcedure [dbo].[GetProgOfficerSummary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE PROC [dbo].[GetProgOfficerSummary] @UserId int  
 AS  
 BEGIN  
  
 DECLARE @Exists INT =0  
  SELECT @Exists = COUNT(Id)   FROM   ProgrammeOfficer WHERE UserId = @UserId    
   IF(@Exists>0)  
   BEGIN
   SELECT TOP 1 CountyId, ConstituencyId, UserId FROM ProgrammeOfficer WHERE UserId = @UserId  ORDER BY 1 DESC  
   END
   ELSE   
   BEGIN
   SELECT NULL AS CountyId,NULL AS ConstituencyId, @UserId AS UserId  
   END
 END  




GO
/****** Object:  StoredProcedure [dbo].[GetPSPBeneEnrolment]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetPSPBeneEnrolment]
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='CARD Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT T1.HhEnrolmentId AS EnrolmentNumber,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T1.AccountNo,T1.AccountName,T1.OpenedOn
		,T4.Name AS PSPBranch,T5.Name AS PSP
		,T3.PaymentCardNo
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.PriReciFirstName,'') ELSE '' END AS BeneficiaryFirstName
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.PriReciMiddleName,'') ELSE '' END AS BeneficiaryMiddleName
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.PriReciSurname,'') ELSE '' END AS BeneficiarySurname
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.PriReciNationalIdNo,'') ELSE '' END AS BeneficiaryNationalIdNo
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.SecReciFirstName,'') ELSE ISNULL(T3.PriReciFirstName,'') END AS CaregiverFirstName
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.SecReciMiddleName,'') ELSE ISNULL(T3.PriReciMiddleName,'') END AS CaregiverMiddleName
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.SecReciSurname,'') ELSE ISNULL(T3.PriReciSurname,'') END AS CaregiverSurname
		,CASE WHEN(T7.PriReciCanReceivePayment=1) THEN ISNULL(T3.SecReciNationalIdNo,'') ELSE ISNULL(T3.PriReciNationalIdNo,'') END AS CaregiverNationalIdNo
	FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
							   LEFT JOIN BeneficiaryPaymentCard T3 ON T1.Id=T3.BeneAccountId AND T3.StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
							   INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id
							   INNER JOIN PSP T5 ON T4.PSPId=T5.Id
							   INNER JOIN HouseholdEnrolmentPlan T6 ON T2.HhEnrolmentPlanId=T6.Id
							   INNER JOIN Programme T7 ON T6.ProgrammeId=T7.Id
	WHERE T1.StatusId=@SystemCodeDetailId1
	ORDER BY T1.HhEnrolmentId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPSPEnrolmentByCounty]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPSPEnrolmentByCounty]
	@EnrolmentPlanId int=NULL
AS
BEGIN	  	
	IF NOT OBJECT_ID('tempdb..##tblEnrolmentAnalysis') IS NULL	DROP TABLE ##tblEnrolmentAnalysis;
	CREATE TABLE ##tblEnrolmentAnalysis(RowId bigint NOT NULL IDENTITY(1,1)
								      ,PSPId int
								      ,PSPName varchar(30)
								      ,CountyId int
								      ,County varchar(128)
								      ,AccountsOpened int
								      ,PaymentCardsIssued int
								      )
								   
	IF NOT OBJECT_ID('tempdb..##tblEnrolmentSummary') IS NULL	DROP TABLE ##tblEnrolmentSummary;
	CREATE TABLE ##tblEnrolmentSummary(RowId bigint NOT NULL IDENTITY(1,1)
								    ,CountyId int
								    ,TotalAccounts int
								    ,TotalPaymentCardsIssued int
								    )
								    
	DECLARE @tblPaymentCycles TABLE(PSPId int
								   ,PSPName varchar(48)
								   )
								   
	DECLARE @SQL varchar(8000)
	DECLARE @strPSP varchar(1000)
	DECLARE @strCols varchar(1000)
 	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Card Status'
	SET @SysDetailCode='-1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	--1. BUILDING THE PAYMENT NUMBERS STRING
	SELECT @strPSP=COALESCE(@strPSP+',['+T1.Name+']','['+T1.Name+']')
	FROM PSP T1
	
	--SELECT @strCols=COALESCE(@strCols+',ISNULL(T2.['+T1.Name+'],0) ['+T1.Name+'-Accs],ISNULL(T3.['+T1.Name+'],0) ['+T1.Name+'-Cards]','ISNULL(T2.['+Name+'],0) ['+T1.Name+'-Accs],ISNULL(T3.['+T1.Name+'],0) ['+T1.Name+'-Cards]')
	SELECT @strCols=COALESCE(@strCols+',ISNULL(T2.['+T1.Name+'],0) ['+T1.Name+'-Accs]','ISNULL(T2.['+Name+'],0) ['+T1.Name+'-Accs]')
	FROM PSP T1

	--3. ANALYZING Enrolment (BENEFICIARY AND PaymentCardsIssued NUMBERS) BY CYCLE AND BY COUNTY
	INSERT INTO ##tblEnrolmentAnalysis(PSPId,PSPName,CountyId,County,AccountsOpened,PaymentCardsIssued)
	SELECT T1.Code,T1.Name,T8.CountyId,T8.CountyName,COUNT(T3.Id) AS AccountsOpened,SUM(CASE WHEN(ISNULL(T4.PaymentCardNo,'')<>'') THEN 1 ELSE 0 END) AS PaymentCardsIssued
	FROM PSP T1 INNER JOIN PSPBranch T2 ON T1.Id=T2.PSPId
				INNER JOIN (SELECT MIN(Id) AS Id,HhEnrolmentId,PSPBranchId FROM BeneficiaryAccount WHERE StatusId=@SystemCodeDetailId1 GROUP BY HhEnrolmentId,PSPBranchId) T3 ON T2.Id=T3.PSPBranchId
				LEFT JOIN (SELECT T1.BeneAccountId,T1.PriReciId,T1.PaymentCardNo 
							FROM BeneficiaryPaymentCard T1 INNER JOIN (
																		SELECT BeneAccountId,MIN(Id) AS Id
																		FROM BeneficiaryPaymentCard
																		WHERE StatusId IN(@SystemCodeDetailId2,@SystemCodeDetailId3)
																		GROUP BY BeneAccountId
																		) T2 ON T1.Id=T2.Id
							) T4 ON T3.Id=T4.BeneAccountId
				INNER JOIN HouseholdEnrolment T5 ON T3.HhEnrolmentId=T5.Id
				INNER JOIN Household T6 ON T5.HhId=T6.Id
				INNER JOIN HouseholdSubLocation T7 ON T6.Id=T7.HhId
				INNER JOIN (
								SELECT T1.Id AS SubLocationId,T6.Id AS CountyId,T6.Code AS CountyCode,T6.Name AS CountyName,T6.GeoMasterId
								FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
													INNER JOIN Division T3 ON T2.DivisionId=T3.Id
													INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
													INNER JOIN District T5 ON T4.DistrictId=T5.Id
													INNER JOIN County T6 ON T4.CountyId=T6.Id
													INNER JOIN GeoMaster T7 ON T6.GeoMasterId=T7.Id AND T7.IsDefault=1
							) T8 ON T7.SubLocationId=T8.SubLocationId AND T7.GeoMasterId=T8.GeoMasterId
	WHERE T5.HhEnrolmentPlanId=ISNULL(@EnrolmentPlanId,T5.HhEnrolmentPlanId)
	GROUP BY T1.Code,T1.Name,T8.CountyId,T8.CountyName
	


	--4. Enrolment SUMMARY
	INSERT INTO ##tblEnrolmentSummary(CountyId,TotalAccounts)--,TotalPaymentCardsIssued)
	SELECT T1.CountyId,SUM(T1.AccountsOpened) AS TotalAccounts--,SUM(T1.PaymentCardsIssued) AS TotalPaymentCardsIssued
	FROM ##tblEnrolmentAnalysis T1
	GROUP BY T1.CountyId
	
	---->QUICK CONFIRMATION
	--SELECT @strPSP
	--SELECT @strCols

	--SELECT T1.*
	--FROM ##tblEnrolmentAnalysis T1
	--ORDER BY T1.County
	----COMPUTE SUM(T1.AccountsOpened),SUM(T1.PaymentCardsIssued)
	
	--BUILDING THE QUERY STRING
	SET @SQL =' SELECT ROW_NUMBER() OVER(ORDER BY T1.Name) AS No,T1.Name AS County,'+@strCols+',ISNULL(T4.TotalAccounts,0) TotalAccs'
			 +' FROM County T1 LEFT JOIN ('
										 +' SELECT *'
										 +' FROM ('
												 +'	SELECT CountyId,County,AccountsOpened,PSPName'
												 +'	FROM ##tblEnrolmentAnalysis'
											  +' ) AS T1'
										 +' PIVOT'
											  +' ('
												 +'	SUM(AccountsOpened)'
												 +'	FOR PSPName IN('+@strPSP+')'
											  +' ) AS T2'
									   +' ) T2 ON T1.Id=T2.CountyId'
							+' LEFT JOIN ('
										 +' SELECT *'
										 +' FROM ('
												 +'	SELECT CountyId,County,PaymentCardsIssued,PSPName'
												 +'	FROM ##tblEnrolmentAnalysis'
											  +' ) AS T1'
										 +' PIVOT'
											  +' ('
												 +'	SUM(PaymentCardsIssued)'
												 +'	FOR PSPName IN('+@strPSP+')'
											  +' ) AS T2'
									   +' ) T3 ON T1.Id=T3.CountyId'
							+' LEFT JOIN ##tblEnrolmentSummary T4 ON T1.Id=T4.CountyId'
			 +' ORDER BY T1.Name'

	--PRINT @SQL
	--PRINT @strCols
	
	EXEC(@SQL)
END

GO
/****** Object:  StoredProcedure [dbo].[GetReconciliationDetailOptions]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetReconciliationDetailOptions]
	@ReconciliationId int
   ,@PSPId int
   ,@UserId int
AS
BEGIN
	DECLARE @tbl_PaymentCycles TABLE(
		PaymentCycleId int
	);
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @CrFundsRequests money
	DECLARE @CrClawBacks money
	DECLARE @DrPayments money
	DECLARE @DrCommissions money
	DECLARE @Balance money
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @ReconciliationId=ISNULL(@ReconciliationId,0),@PSPId=ISNULL(@PSPId,0)

	IF NOT EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId)
		SET @ErrorMsg='Please specify valid ReconciliationId parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@ReconciliationId AND StatusId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified reconciliation is not in a stage that allows details to be updated'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Id=@PSPId)
		SET @ErrorMsg='Please specify valid PSPId parameter'
	ELSE IF NOT EXISTS( SELECT 1
						FROM (
								SELECT PaymentCycleId
								FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T2.Id=@ReconciliationId AND T1.FundsRequestOn>=T2.StartDate AND T1.PostPayrollApvOn<=T2.EndDate
								GROUP BY PaymentCycleId
							) T1 INNER JOIN FundsRequest T2 ON T1.PaymentCycleId=T2.PaymentCycleId
								 INNER JOIN FundsRequestDetail T3 ON T2.Id=T3.FundsRequestId
						WHERE T3.PSPId=@PSPId
						)
		SET @ErrorMsg='The specified PSPId parameter is not associated with respective payment cycles in reconciliation period'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	INSERT INTO @tbl_PaymentCycles(PaymentCycleId)
	SELECT T1.PaymentCycleId
	FROM PaymentCycleDetail T1 INNER JOIN Reconciliation T2 ON T1.FundsRequestOn>=T2.StartDate AND t1.PostPayrollApvOn<=T2.EndDate
	WHERE T2.Id=@ReconciliationId
	GROUP BY PaymentCycleId

	SELECT @CrFundsRequests=SUM(T2.FundsRequestAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T2.PaymentCycleId,SUM(T1.EntitlementAmount+T1.OtherAmount) AS FundsRequestAmount
											FROM FundsRequestDetail T1 INNER JOIN FundsRequest T2 ON T1.FundsRequestId=T2.Id
											WHERE T1.PSPId=@PSPId
											GROUP BY T2.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId 

	SELECT @CrClawBacks=SUM(T1.ClawbackAmount)
	FROM (
			SELECT DISTINCT T1.PaymentCycleId,T2.MonthId,T2.[Year],T3.ClawbackAmount
			FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
									   INNER JOIN (
													SELECT T1.Id,T1.MonthId,T1.[Year],SUM(T2.ClawbackAmount) AS ClawbackAmount
													FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
																					   INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
																					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
													WHERE T4.PSPId=@PSPId
													GROUP BY T1.Id,T1.MonthId,T1.[Year]
												 ) T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
		) T1

	SELECT @DrPayments=SUM(T2.TrxAmount)
	FROM @tbl_PaymentCycles T1 INNER JOIN (
											SELECT T1.PaymentCycleId,SUM(T1.TrxAmount) AS TrxAmount
											FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
															INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
															INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
											WHERE T4.PSPId=@PSPId
											GROUP BY T1.PaymentCycleId
										) T2 ON T1.PaymentCycleId=T2.PaymentCycleId
	
	SELECT @DrCommissions=SUM(T1.PaymentZoneCommAmt)
	FROM  (
			SELECT T1.PaymentCycleId,T1.HHId,T2.PaymentZoneCommAmt AS PaymentZoneCommAmt
			FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
							INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
							INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
			WHERE T1.WasTrxSuccessful=1 AND T4.PSPId=@PSPId
		) T1 INNER JOIN (
							SELECT T1.PaymentCycleId,T6.HhId,MAX(CONVERT(int,T4.HadUniqueWdl)) AS HadUniqueWdl
							FROM @tbl_PaymentCycles T1 INNER JOIN PaymentAccountActivityMonth T2 ON T1.PaymentCycleId=T2.PaymentCycleId
													   INNER JOIN BeneAccountMonthlyActivity T3 ON T2.MonthId=T3.MonthId AND T2.[Year]=T3.[Year]
													   INNER JOIN BeneAccountMonthlyActivityDetail T4 ON T3.Id=T4.BeneAccountMonthlyActivityId
													   INNER JOIN BeneficiaryAccount T5 ON T4.BeneAccountId=T5.Id
													   INNER JOIN HouseholdEnrolment T6 ON T5.HhEnrolmentId=T6.Id
													   INNER JOIN PSPBranch T7 ON T5.PSPBranchId=T7.Id
							WHERE T7.PSPId=@PSPId
							GROUP BY T1.PaymentCycleId,T6.HhId
							HAVING MAX(CONVERT(int,T4.HadUniqueWdl))<>0
						) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.HhId=T2.HhId

	SELECT @CrFundsRequests=ISNULL(@CrFundsRequests,0),@CrClawBacks=ISNULL(@CrClawBacks,0),@DrPayments=ISNULL(@DrPayments,0),@DrCommissions=ISNULL(@DrCommissions,0)

	SELECT @Balance=(@CrFundsRequests+@CrClawBacks)-(@DrPayments-@DrCommissions)			 

	SELECT @CrFundsRequests AS CrFundsRequests,@CrClawBacks AS CrClawBacks,@DrPayments AS DrPayments,@DrCommissions AS DrCommissions,@Balance AS Balance
END

GO
/****** Object:  StoredProcedure [dbo].[GetReconciliationOptions]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetReconciliationOptions]
	@Id int=NULL
   ,@StartDate datetime
   ,@EndDate datetime
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @ErrorMsg varchar(256)


	SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Reconciliation Status'
	SET @SysDetailCode='RECONOPEN'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @Id=ISNULL(@Id,0)
	IF ISNULL(@StartDate,GETDATE())>=GETDATE() 
		SET @ErrorMsg='The StartDate parameter cannot be equal to or greater than today'
	ELSE IF @EndDate IS NULL
		SET @ErrorMsg='Please specify valid EndDate parameter'
	ELSE IF @EndDate>GETDATE()
		SET @ErrorMsg='The EndDate parameter cannot be greater than today'
	ELSE IF (@StartDate>@EndDate)
		SET @ErrorMsg='The StartDate parameter cannot be greater than the EndDate parameter'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE StartDate>=@StartDate AND StartDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE EndDate>=@EndDate AND EndDate<=@EndDate AND Id<>@Id)
		SET @ErrorMsg='There''s already another reconciliation covering the period specified'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id WHERE T1.FundsRequestOn>=@StartDate AND (T1.PostPayrollApvOn>=@EndDate OR T2.StatusId<>@SystemCodeDetailId1))
		SET @ErrorMsg='One or more applicable payment cycle(s) for the period specified is yet to be ready for reconciliation'
	ELSE IF EXISTS(SELECT 1 FROM Reconciliation WHERE Id=@Id AND StatusId<>@SystemCodeDetailId2)
		SET @ErrorMsg='The specified reconciliation cannot be edited once it is in approval stage'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	SELECT T1.Id,T3.[Description] AS FromMonth,T4.[Description] AS ToMonth,T5.[Description] AS FinancialYear
	FROM PaymentCycle T1 INNER JOIN (
									  SELECT PaymentCycleId
									  FROM PaymentCycleDetail
									  WHERE FundsRequestOn>=@StartDate AND PostPayrollApvOn<=@EndDate
									  GROUP BY PaymentCycleId
									) T2 ON T1.Id=T2.PaymentCycleId
						 INNER JOIN SystemCodeDetail T3 ON T1.FromMonthId=T3.Id
						 INNER JOIN SystemCodeDetail T4 ON T1.ToMonthId=T4.Id
						 INNER JOIN SystemCodeDetail T5 ON T1.FinancialYearId=T5.Id
END

GO
/****** Object:  StoredProcedure [dbo].[GetRequestForFunds]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetRequestForFunds]
	@PaymentCycleId int
AS
BEGIN
	SELECT CONVERT(int,ROW_NUMBER() OVER(ORDER BY T3.Code)) AS [No],T3.Code AS Programme,T4.Name AS PSP,SUM(T2.PayrollHhs) AS NoOfHouseholds,SUM(T2.EntitlementAmount) AS Amount,SUM(T2.CommissionAmount) AS Commission,SUM(T2.EntitlementAmount+T2.CommissionAmount) AS Total
	FROM FundsRequest T1 INNER JOIN FundsRequestDetail T2 ON T1.Id=T2.FundsRequestId
						 INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id
						 INNER JOIN PSP T4 ON T2.PSPId=T4.Id

	WHERE T1.PaymentCycleId=@PaymentCycleId
	GROUP BY T3.Code,T4.Name
END

GO
/****** Object:  StoredProcedure [dbo].[GetUserGroupRights]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserGroupRights]
    @UserId  int
 AS
   BEGIN  

   
     select UPPER( concat(m.Name,':', mr.[DESCRIPTION]) ) from Groupright r 
     inner join  ModuleRight mr on mr.Id = r.ModuleRightId
     inner join SystemCodeDetail scd on scd.Id = mr.RightId
     inner join UserGroup ug on ug.Id = r.UserGroupId
     inner join Module m on m.Id = mr.ModuleId
     inner join   dbo.[User]  u on u.UserGroupId = ug.Id
     and u.Id = @UserId

     END



GO
/****** Object:  StoredProcedure [dbo].[GetUserProfile]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserProfile]
    @UserId  int
 AS
   BEGIN  

      select u.FirstName, 
      u.MiddleName, 
      u.Surname, 
      u.Avatar,
       u.Email,
        u.MobileNo,
        u.UserGroupId
       from    [User] u 
        where u.Id = @UserId

     END


GO
/****** Object:  StoredProcedure [dbo].[IPRSCheckmember]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[IPRSCheckmember]
	@IdNumber VARCHAR(10)
AS
BEGIN

 DECLARE @SysCode varchar(20)  
 DECLARE @SysDetailCode varchar(20) 
 DECLARE @Male int  
 DECLARE @Female INT
 
 SET @SysCode='Sex'  
 SET @SysDetailCode='M'  
 SELECT @Male=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
 SET @SysDetailCode='F'  
 SELECT @Female=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode   
  


 SELECT  First_Name, Surname, Middle_Name, ID_Number, CASE WHEN Gender ='M' THEN CONVERT(VARCHAR,@Male) ELSE CONVERT(VARCHAR,@FeMale) END AS Gender, Date_of_Birth, Date_of_Issue, Place_of_Birth, Serial_Number, Address, Status, DateCached, Gender AS Sex FROM  IPRSCache

 WHERE ID_Number =@IdNumber
END

GO
/****** Object:  StoredProcedure [dbo].[PayrollFileDownloaded]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PayrollFileDownloaded]
	@FileCreationId int
   ,@FileChecksum varchar(64)
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @RowCount int
	DECLARE @ErrorMsg varchar(128)

	IF NOT EXISTS(SELECT 1 FROM FileCreation WHERE Id=@FileCreationId AND FileChecksum=@FileChecksum)
		SET @ErrorMsg='Please specify valid FileCreationId corresponding FileCheksum'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId AND T2.UserId=@UserId)
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM FileDownload WHERE FileCreationId=@FileCreationId AND DownloadedBy=@UserId)
			INSERT INTO FileDownload(FileCreationId,FileChecksum,DownloadedBy,DownloadedOn)
			SELECT @FileCreationId,@FileChecksum,@UserId,GETDATE() AS DownloadedOn

		SET @SysCode='Payroll Stage'
		SET @SysDetailCode='PAYROLLEXCONF'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PaymentStageId=@SystemCodeDetailId1
		FROM PaymentCycleDetail T1
		WHERE T1.FileCreationId=@FileCreationId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS FilePassword
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT FilePassword FROM FileCreation WHERE Id=@FileCreationId
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ProcessFundsRequest]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ProcessFundsRequest]
	@PaymentCycleId int
   ,@UserId int
AS
BEGIN
	DECLARE @FundsRequestId int
	DECLARE @SysCode varchar(30)
	DECLARE @SysDetailCode varchar(30)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @ErrorMsg varchar(128)

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='FUNDSREQUEST'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1 FROM PaymentCycleDetail T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='One or more programmes in the PaymentCycleId appears not to be in the Funds Request Stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF NOT EXISTS(SELECT 1 FROM FundsRequest WHERE PaymentCycleId=@PaymentCycleId)
		INSERT INTO FundsRequest(PaymentCycleId)
		SELECT @PaymentCycleId AS PaymentCycleId

	SELECT @FundsRequestId=Id FROM FundsRequest WHERE PaymentCycleId=@PaymentCycleId

	INSERT INTO FundsRequestDetail(FundsRequestId,PSPId,ProgrammeId,PayrollHhs,EntitlementAmount,OtherAmount,CommissionAmount)
	SELECT @FundsRequestId AS FundsRequestId,T5.Id AS PSPId,T1.ProgrammeId AS ProgrammeId,COUNT(T1.HhId) AS PayrollHhs,SUM(T31.PaymentAmount) AS EntitlementAmount,SUM(T1.AdjustmentAmount) AS OtherAmount,SUM(T1.PaymentZoneCommAmt) AS CommissionAmount
	FROM Prepayroll T1 LEFT JOIN (
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedApvBy IS NULL
								) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
					   INNER JOIN BeneficiaryAccount T3 ON T1.BeneAccountId=T3.Id
					   INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
					   INNER JOIN PSP T5 ON T4.PSPId=T5.Id

					   INNER JOIN (
		SELECT  T1.*, T5.HhId FROM PaymentEnrolmentGroup T1  
			INNER JOIN Programme T2 ON T1.ProgrammeId = T2.Id
		INNER JOIN PaymentCycleDetail T3 ON T1.PaymentCycleId = T3.PaymentCycleId AND T3.ProgrammeId = T1.ProgrammeId
		INNER JOIN HouseholdEnrolmentPlan T4 ON T4.EnrolmentGroupId = T1.EnrolmentGroupId  AND T4.ProgrammeId = T2.Id 
		INNER JOIN HouseholdEnrolment T5 ON T5.HhEnrolmentPlanId = T4.Id AND T1.ProgrammeId = T3.ProgrammeId AND T1.PaymentCycleId = T3.PaymentCycleId
		inner JOIN Household T6 ON T6.Id = T5.HhId AND T1.PaymentCycleId = @PaymentCycleId
		)T31 ON T1.HhId = T31.HhId AND T1.ProgrammeId = T31.ProgrammeId  AND T31.PaymentCycleId = @PaymentCycleId and T1.PaymentCycleId=@PaymentCycleId 

	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.PaymentCycleId IS NULL 
	GROUP BY T5.Id,T1.ProgrammeId

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='FUNDSREQUESTAPV'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.FundsRequestBy=@UserId
		,T1.FundsRequestOn=GETDATE()
		,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		SELECT 1 AS NoRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ProcessPayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ProcessPayroll]
	@PaymentCycleId int
   ,@ProgrammeId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='PAYROLLVER'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId IN(@SystemCodeDetailId1,@SystemCodeDetailId2))
		SET @ErrorMsg='The specified payment cycle is not in the payroll generation stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN
	
	DELETE FROM Payroll WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	INSERT INTO Payroll(PaymentCycleId,ProgrammeId,HhId,PaymentAmount)
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,(T1.EntitlementAmount+T1.AdjustmentAmount) AS PaymentAmount
	FROM Prepayroll T1 LEFT JOIN (
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedApvBy IS NULL
									UNION
									SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedApvBy IS NULL
								) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T6.PaymentCycleId IS NULL AND (T1.EntitlementAmount+T1.AdjustmentAmount) <=16000
	
	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLVER'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.PayrollBy=@UserId
	   ,T1.PayrollOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		SELECT 1 AS NoRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[ProcessPrepayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ProcessPrepayroll]

    @PaymentCycleId int
   ,
    @ProgrammeId int
   ,
    @UserId int
AS
BEGIN
    DECLARE @ActiveHhs int
    DECLARE @TargetHhs int
    DECLARE @HhStatus_EnrolledProgCode varchar(20)
    DECLARE @HhStatus_EnrolledPSPCode varchar(20)
    DECLARE @HhStatus_PSPCardedCode varchar(20)
    DECLARE @HhStatus_OnPayrollCode varchar(20)
    DECLARE @HhStatus_OnSuspensionCode varchar(20)
    DECLARE @SysSetting_PSPACCDORMANCY int
    DECLARE @DormancyFromDate datetime
    DECLARE @DormancyToDate datetime
    DECLARE @BeneficiaryTypeId int
    DECLARE @PriReciCanReceivePayment bit
    DECLARE @SecondaryRecipientMandatory bit
    DECLARE @BeneficiaryType_INDIVIDUAL int
    DECLARE @ExceptionType_INVALIDBENEID int
    DECLARE @ExceptionType_INVALIDCGID int
    DECLARE @ExceptionType_DUPLICATEBENEIDIN int
    DECLARE @ExceptionType_DUPLICATEBENEIDACC int
    DECLARE @ExceptionType_DUPLICATECGIDIN int
    DECLARE @ExceptionType_DUPLICATECGIDACC int
    DECLARE @ExceptionType_INVALIDACC int
    DECLARE @ExceptionType_INVALIDCARD int
    DECLARE @ExceptionType_SUSPICIOUSAMT int
    DECLARE @ExceptionType_SUSPICIOUSDORMANCY int
    DECLARE @ExceptionType_INELIGIBLEBENE int
    DECLARE @ExceptionType_INELIGIBLECG int
    DECLARE @ExceptionType_INELIGIBLESUS int
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @SystemCodeDetailId3 int
    DECLARE @ErrorMsg varchar(128)

    DECLARE @ACActivityMonthId int
    DECLARE @ACActivityMonthCode varchar(5)
    DECLARE @ACActivityYear int

    DECLARE @FileName nvarchar(128)
    DECLARE @FileType nvarchar(10)
    DECLARE @NoOfRows int

    SET @HhStatus_EnrolledProgCode = 'ENRL'
    SET @HhStatus_EnrolledPSPCode = 'ENRLPSP'
    SET @HhStatus_PSPCardedCode = 'PSPCARDED'
    SET @HhStatus_OnPayrollCode = 'ONPAY'
    SET @HhStatus_OnSuspensionCode = 'SUS'

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PREPAYROLLDRAFT'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='PREPAYROLLFINAL'
    SELECT @SystemCodeDetailId2=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SELECT @ActiveHhs=COUNT(T1.Id), @TargetHhs=SUM(CASE WHEN(T6.EnrolmentGroupId>0) THEN 1 ELSE 0 END), @SystemCodeDetailId3=MAX(T3.PaymentStageId)
    FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
        INNER JOIN PaymentCycleDetail T3 ON T1.ProgrammeId=T3.ProgrammeId
        INNER JOIN HouseholdEnrolment T4 ON T1.Id=T4.HhId
        INNER JOIN HouseholdEnrolmentPlan T5 ON T4.HhEnrolmentPlanId=T5.Id
        LEFT JOIN PaymentEnrolmentGroup T6 ON T3.PaymentCycleId=T6.PaymentCycleId AND T3.ProgrammeId=T6.ProgrammeId AND T5.EnrolmentGroupId=T6.EnrolmentGroupId
    WHERE T3.PaymentCycleId=@PaymentCycleId AND T3.ProgrammeId=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode,@HhStatus_OnSuspensionCode)

    IF NOT EXISTS(SELECT 1
    FROM PaymentCycle
    WHERE Id=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF	@SystemCodeDetailId3 NOT IN(@SystemCodeDetailId1,@SystemCodeDetailId2)
		SET @ErrorMsg='Once the Prepayroll has been APPROVED one CANNOT run the Prepayroll again'	
	ELSE IF ISNULL(@TargetHhs,0)=0
		SET @ErrorMsg='No target households'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END

    SET @SysCode='Member Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Account Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId2=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Card Status'
    SET @SysDetailCode='1'
    SELECT @SystemCodeDetailId3=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='System Settings'
    SET @SysDetailCode='PSPACCDORMANCY'
    SELECT @SysSetting_PSPACCDORMANCY=T1.[Description]
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Beneficiary Type'
    SET @SysDetailCode='INDIVIDUAL'
    SELECT @BeneficiaryType_INDIVIDUAL=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysCode='Exception Type'
    SET @SysDetailCode='INVALIDBENEID'
    SELECT @ExceptionType_INVALIDBENEID=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='INVALIDCGID'
    SELECT @ExceptionType_INVALIDCGID=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='DUPLICATEBENEIDIN'
    SELECT @ExceptionType_DUPLICATEBENEIDIN=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='DUPLICATEBENEIDACC'
    SELECT @ExceptionType_DUPLICATEBENEIDACC=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='DUPLICATECGIDIN'
    SELECT @ExceptionType_DUPLICATECGIDIN=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='DUPLICATECGIDACC'
    SELECT @ExceptionType_DUPLICATECGIDACC=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='INVALIDACC'
    SELECT @ExceptionType_INVALIDACC=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='INVALIDCARD'
    SELECT @ExceptionType_INVALIDCARD=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='SUSPICIOUSAMT'
    SELECT @ExceptionType_SUSPICIOUSAMT=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='SUSPICIOUSDORMANCY'
    SELECT @ExceptionType_SUSPICIOUSDORMANCY=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='INELIGIBLEBENE'
    SELECT @ExceptionType_INELIGIBLEBENE=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='INELIGIBLECG'
    SELECT @ExceptionType_INELIGIBLECG=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
    SET @SysDetailCode='INELIGIBLESUS'
    SELECT @ExceptionType_INELIGIBLESUS=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SELECT @ACActivityMonthId=T1.MonthId, @ACActivityMonthCode=T1.Code, @ACActivityYear=T1.Year
    FROM (	
			SELECT TOP 1
            T1.PaymentCycleId, T1.[Year], T2.Code, T1.MonthId
        FROM PaymentAccountActivityMonth T1 INNER JOIN SystemCodeDetail T2 ON T1.MonthId=T2.Id
        WHERE DATEDIFF(MM,CONVERT(datetime,'1 '+dbo.fn_MonthName(T2.Code,0)+' '+CONVERT(varchar(4),T1.[Year])),(SELECT ISNULL(ModifiedOn,CreatedOn)
        FROM PaymentCycle
        WHERE Id=@PaymentCycleId))>1
        ORDER BY T1.PaymentCycleId,T1.[Year],T2.Code DESC
		) T1

    SET @DormancyToDate=CONVERT(datetime,'1 '+dbo.fn_MonthName(@ACActivityMonthCode,0)+' '+CONVERT(varchar(4),@ACActivityYear))
    SET @DormancyFromDate=DATEADD(MM,-@SysSetting_PSPACCDORMANCY,@DormancyToDate)

    SELECT @BeneficiaryTypeId=T1.BeneficiaryTypeId
		  , @PriReciCanReceivePayment=T1.PriReciCanReceivePayment
		  , @SecondaryRecipientMandatory=T1.SecondaryRecipientMandatory
    FROM Programme T1
    WHERE T1.Id=@ProgrammeId

    PRINT 'DELETE Prepayroll'

    DELETE T1 FROM Prepayroll T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollInvalidID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollDuplicateID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollInvalidPaymentAccount T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollInvalidPaymentCard T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollIneligible T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;
    DELETE T1 FROM PrepayrollSuspicious T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId;

    PRINT 'Prepayroll'
    INSERT INTO Prepayroll
        (PaymentCycleId,ProgrammeId,HhId,BenePersonId,BeneFirstName,BeneMiddleName,BeneSurname,BeneDoB,BeneSexId,BeneNationalIDNo,PriReciCanReceivePayment,CGPersonId,CGFirstName,CGMiddleName,CGSurname,CGDoB,CGSexId,CGNationalIDNo,TotalHhMembers,HhStatusId,BeneAccountId,BenePaymentCardId,SubLocationId,PaymentZoneId,PaymentZoneCommAmt,ConseAccInactivity,EntitlementAmount,AdjustmentAmount)
    SELECT DISTINCT @PaymentCycleId as PaymentCycleId, T3.Id AS ProgrammeId, T1.Id AS HhId, T4.PersonId AS BenePersonId, T4.FirstName AS BeneFirstName, T4.MiddleName AS BeneMiddleName, T4.Surname AS BeneSurname, T4.DoB AS BeneDoB, T4.SexId AS BeneSexId, T4.NationalIdNo AS BeneNationalIDNo, T3.PriReciCanReceivePayment, T5.PersonId AS CGPersonId, T5.FirstName AS CGFirstName, T5.MiddleName AS CGMiddleName, T5.Surname AS CGSurname, T5.DoB AS CGDoB, T5.SexId AS CGSexId, T5.NationalIdNo AS CGNationalIDNo, T6.TotalHhMembers, T1.StatusId AS HhStatusId, T14.Id AS BeneAccountId, T15.Id AS BenePaymentCardId, T7.SubLocationId, T8.Id AS PaymentZoneId, CASE WHEN(T8.IsPerc=1) THEN ((T31.PaymentAmount+T10.AdjustmentAmount)*T8.Commission)/100 ELSE T8.Commission END AS PaymentZoneCommAmt, 0 AS ConseAccInactivity, T31.PaymentAmount, ISNULL(T10.AdjustmentAmount,0) AS AdjustmentAmount
    FROM Household T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
        INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
		INNER JOIN (
		SELECT  T1.*, T5.HhId FROM PaymentEnrolmentGroup T1  
			INNER JOIN Programme T2 ON T1.ProgrammeId = T2.ID
		INNER JOIN PaymentCycleDetail T3 ON T1.PaymentCycleId = T3.PaymentCycleId AND T3.ProgrammeId = T1.ProgrammeId
		INNER JOIN HouseholdEnrolmentPlan T4 ON T4.EnrolmentGroupId = T1.EnrolmentGroupId  AND T4.ProgrammeId = T2.Id
		INNER JOIN HouseholdEnrolment T5 ON T5.HhEnrolmentPlanId = T4.Id AND T1.ProgrammeId = @ProgrammeId AND T1.PaymentCycleId = @PaymentCycleId
		)T31 ON T1.Id = T31.HhId AND T3.ID = T31.ProgrammeId AND T31.ProgrammeId = @ProgrammeId AND T31.PaymentCycleId = @PaymentCycleId
        INNER JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId, T2.HhId, T1.Id AS PersonId, T1.FirstName, T1.MiddleName, T1.Surname, T1.DoB, T1.SexId, T1.NationalIdNo
        FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
            INNER JOIN Household T3 ON T2.HhId=T3.Id
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.PrimaryRecipientId
        WHERE T2.StatusId=@SystemCodeDetailId1
								) T4 ON T1.Id=T4.HhId AND T4.RowId=1
        LEFT JOIN (
									SELECT ROW_NUMBER() OVER(PARTITION BY T2.HhId ORDER BY T1.DoB ASC) AS RowId, T2.HhId, T1.Id AS PersonId, T1.FirstName, T1.MiddleName, T1.Surname, T1.DoB, T1.SexId, T1.NationalIdNo
        FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId
            INNER JOIN Household T3 ON T2.HhId=T3.Id
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T2.MemberRoleId=T4.SecondaryRecipientId
        WHERE T2.StatusId=@SystemCodeDetailId1
								) T5 ON T1.Id=T5.HhId AND T5.RowId=1
        LEFT JOIN(
									SELECT HhId, COUNT(PersonId) AS TotalHhMembers
        FROM HouseholdMember
        WHERE StatusId=@SystemCodeDetailId1
        GROUP BY HhId
								) T6 ON T1.Id=T6.HhId
        LEFT JOIN(
									SELECT T1.HhId, T1.SubLocationId, T2.LocationId, T4.PaymentZoneId
        FROM HouseholdSubLocation T1 INNER JOIN SubLocation T2 ON T1.SubLocationId=T2.Id
            INNER JOIN GeoMaster T3 ON T1.GeoMasterId=T3.Id
            INNER JOIN Location T4 ON T2.LocationId=T4.Id
        WHERE T3.IsDefault=1
								) T7 ON T1.Id=T7.HhId
        LEFT JOIN PaymentZone T8 ON T7.PaymentZoneId=T8.Id
        --LEFT JOIN (
        --		SELECT T4.HhId,SUM(CASE(T2.HadUniqueWdl) WHEN 1 THEN 1 ELSE 0 END) AS BeneAccDormancy
        --		FROM BeneAccountMonthlyActivity T1 INNER JOIN BeneAccountMonthlyActivityDetail T2 ON T1.Id=T2.BeneAccountMonthlyActivityId
        --										   INNER JOIN BeneficiaryAccount T3 ON T2.BeneAccountId=T3.Id
        --										   INNER JOIN HouseholdEnrolment T4 ON T3.HhEnrolmentId=T4.Id
        --										   INNER JOIN SystemCodeDetail T5 ON T1.MonthId=T5.Id
        --		WHERE CONVERT(datetime,'1 '+dbo.fn_MonthName(T5.Code,0)+' '+CONVERT(varchar(4),T1.[Year]))>@DormancyFromDate
        --			AND CONVERT(datetime,'1 '+dbo.fn_MonthName(T5.Code,0)+' '+CONVERT(varchar(4),T1.[Year]))<=@DormancyToDate
        --		GROUP BY T4.HhId
        --	) T9 ON T1.Id=T9.HhId
        LEFT JOIN (
									SELECT PaymentCycleId, HhId, SUM(AdjustmentAmount) AS AdjustmentAmount
        FROM PaymentAdjustment
        WHERE PaymentCycleId =@PaymentCycleId
        GROUP BY PaymentCycleId,HhId
								) T10 ON T10.PaymentCycleId=@PaymentCycleId AND T1.Id=T10.HHId
        INNER JOIN HouseholdEnrolment T11 ON T1.Id=T11.HhId
        INNER JOIN HouseholdEnrolmentPlan T12 ON T11.HhEnrolmentPlanId=T12.Id
        INNER JOIN PaymentEnrolmentGroup T13 ON T13.PaymentCycleId=@PaymentCycleId AND T12.EnrolmentGroupId=T13.EnrolmentGroupId
        LEFT JOIN BeneficiaryAccount T14 ON T11.Id=T14.HhEnrolmentId AND T14.StatusId=@SystemCodeDetailId2 --AND T14.ExpiryDate>GETDATE() --THIS AUTOMATION NEEDS TO BE AGREED TO
        LEFT JOIN BeneficiaryPaymentCard T15 ON T14.Id=T15.BeneAccountId AND T15.StatusId=@SystemCodeDetailId3
    --AND T15.RowId=1
    --(
    --SELECT ROW_NUMBER() OVER(PARTITION BY T3.HhId ORDER BY T1.iD ASC) AS RowId,T3.HhId,T1.BeneAccountId,T1.PaymentCardNo, T1.Id, T1.StatusId
    --		FROM BeneficiaryPaymentCard T1 
    --		INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId = T2.Id
    --		INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId = T3.Id 
    --		INNER JOIN Household T4 ON T3.HhId=T4.Id
    --		WHERE T2.StatusId=@SystemCodeDetailId3

    --)  T15 ON T14.Id=T15.BeneAccountId AND T15.StatusId=@SystemCodeDetailId3 AND T15.RowId=1
    WHERE T3.Id=@ProgrammeId
    order by HHId asc


    PRINT 'PrepayrollInvalidID'
    INSERT INTO PrepayrollInvalidID
        (PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)

            SELECT PaymentCycleId, ProgrammeId, HhId, BenePersonId, @ExceptionType_INVALIDBENEID
        FROM Prepayroll
        WHERE PaymentCycleId=@PaymentCycleId AND @PriReciCanReceivePayment=1 AND ISNUMERIC(BeneNationalIDNo)<>1 AND ProgrammeId = @ProgrammeId
    UNION
        SELECT PaymentCycleId, ProgrammeId, HhId, CGPersonId, @ExceptionType_INVALIDCGID
        FROM Prepayroll
        WHERE PaymentCycleId=@PaymentCycleId AND CGPersonId>1 AND ISNUMERIC(CGNationalIDNo)<>1 AND ProgrammeId = @ProgrammeId

    PRINT 'PrepayrollInvalidPaymentAccount'
    INSERT INTO PrepayrollInvalidPaymentAccount
        (PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
    SELECT PaymentCycleId, ProgrammeId, HhId, @ExceptionType_INVALIDACC
    FROM Prepayroll
    WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BeneAccountId,0)<=0 AND ProgrammeId = @ProgrammeId

    PRINT 'PrepayrollInvalidPaymentCard'
    INSERT INTO PrepayrollInvalidPaymentCard
        (PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
    SELECT PaymentCycleId, ProgrammeId, HhId, @ExceptionType_INVALIDCARD
    FROM Prepayroll
    WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePaymentCardId,0)<=0 AND ProgrammeId = @ProgrammeId

    PRINT 'PrepayrollIneligible'
    INSERT INTO PrepayrollIneligible
        (PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
                SELECT PaymentCycleId, ProgrammeId, HhId, @ExceptionType_INELIGIBLEBENE
        FROM Prepayroll
        WHERE PaymentCycleId=@PaymentCycleId AND ISNULL(BenePersonId,0)<=0 AND ProgrammeId = @ProgrammeId
    UNION
        SELECT PaymentCycleId, ProgrammeId, HhId, @ExceptionType_INELIGIBLECG
        FROM Prepayroll
        WHERE PaymentCycleId=@PaymentCycleId AND @SecondaryRecipientMandatory=1 AND ISNULL(CGPersonId,0)<=0 AND ProgrammeId = @ProgrammeId
    UNION
        SELECT T1.PaymentCycleId, ProgrammeId, T1.HhId, @ExceptionType_INELIGIBLESUS
        FROM Prepayroll T1 INNER JOIN SystemCodeDetail T2 ON T1.HhStatusId=T2.Id
        WHERE PaymentCycleId=@PaymentCycleId AND T2.Code=@HhStatus_OnSuspensionCode AND ProgrammeId = @ProgrammeId



    PRINT 'PrepayrollSuspicious'
    INSERT INTO PrepayrollSuspicious
        (PaymentCycleId,ProgrammeId,HhId,ExceptionTypeId)
    --SELECT PaymentCycleId,ProgrammeId,HhId,@ExceptionType_SUSPICIOUSDORMANCY
    --FROM Prepayroll
    --WHERE PaymentCycleId=@PaymentCycleId AND ConseAccInactivity>=@SysSetting_PSPACCDORMANCY
    --UNION
    SELECT PaymentCycleId, ProgrammeId, HhId, @ExceptionType_SUSPICIOUSAMT
    FROM Prepayroll
    WHERE PaymentCycleId=@PaymentCycleId AND (AdjustmentAmount<>0 or EntitlementAmount <> 4000) AND ProgrammeId = @ProgrammeId


    PRINT 'PrepayrollDuplicateID'
    INSERT INTO PrepayrollDuplicateID
        (PaymentCycleId,ProgrammeId,HhId,PersonId,ExceptionTypeId)


    SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.PersonId, T1.ExceptionTypeId
    FROM
        (
		                            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.BenePersonId PersonId , @ExceptionType_DUPLICATEBENEIDIN ExceptionTypeId
            FROM (
			SELECT PaymentCycleId, ProgrammeId, HhId, BenePersonId, BeneNationalIDNo
                FROM Prepayroll
                WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo, COUNT(T1.HhId) NoOfHhs
                FROM (
									SELECT T1.NationalIdNo, T1.HhId
                    FROM VwPersonMember T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
                        INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId
                            AND T1.PrimaryRecipientId=T1.MemberRoleId AND T1.MemberStatusId=@SystemCodeDetailId1
                    WHERE T1.ProgrammeId=@ProgrammeId AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
                        AND ISNUMERIC(T1.NationalIDNo)=1
								) T1
                GROUP BY try_CONVERT(bigint,T1.NationalIdNo)
                HAVING COUNT(T1.HhId)>1
						) T2 ON try_CONVERT(bigint,T1.BeneNationalIDNo)=T2.ResolvedIDNo
        UNION
            SELECT T1.PaymentCycleId, ProgrammeId, T1.HhId, T1.BenePersonId, @ExceptionType_DUPLICATEBENEIDACC
            FROM (
			SELECT PaymentCycleId, ProgrammeId, HhId, BenePersonId, BeneNationalIDNo
                FROM Prepayroll
                WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(BeneNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T1.NationalIdNo
                FROM VwPersonMember T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
                    INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
                        AND T1.PrimaryRecipientId=T1.MemberRoleId AND T1.MemberStatusId=@SystemCodeDetailId1
                WHERE NOT(T1.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)
                    AND ISNUMERIC(T1.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
                GROUP BY T1.NationalIdNo
						) T2 ON try_CONVERT(bigint,T1.BeneNationalIDNo)=try_CONVERT(bigint,T2.NationalIdNo)
            WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)
        UNION

            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.CGPersonId, @ExceptionType_DUPLICATECGIDIN
            FROM (
			SELECT PaymentCycleId, ProgrammeId, HhId, CGPersonId, CGNationalIDNo
                FROM Prepayroll
                WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT try_CONVERT(bigint,T1.NationalIdNo) AS ResolvedIDNo, COUNT(T1.HhId) NoOfHhs
                FROM (

								SELECT T1.NationalIdNo, T1.HhId
                    FROM VwPersonMember T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
                        INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId=T4.ProgrammeId
                            AND T1.SecondaryRecipientId=T1.MemberRoleId AND T1.ProgrammeId=@ProgrammeId AND T1.MemberStatusId=@SystemCodeDetailId1
                    WHERE T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
                        AND ISNUMERIC(T1.NationalIDNo)=1
							) T1
                GROUP BY try_CONVERT(bigint,T1.NationalIdNo)
                HAVING COUNT(T1.HhId)>1
						) T2 ON try_CONVERT(bigint,T1.CGNationalIDNo)=T2.ResolvedIDNo
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.CGPersonId, @ExceptionType_DUPLICATECGIDACC
            FROM (
			SELECT PaymentCycleId, ProgrammeId, HhId, CGPersonId, CGNationalIDNo
                FROM Prepayroll
                WHERE PaymentCycleId=@PaymentCycleId AND ISNUMERIC(CGNationalIDNo)=1
		)T1 INNER JOIN (
							SELECT T1.NationalIdNo
                FROM VwPersonMember T1 INNER JOIN SystemCodeDetail T2 ON T1.StatusId=T2.Id
                    INNER JOIN PaymentCycleDetail T4 ON T4.PaymentCycleId=@PaymentCycleId AND T4.ProgrammeId=@ProgrammeId AND T1.ProgrammeId<>T4.ProgrammeId
                        AND T1.SecondaryRecipientId=T1.MemberRoleId AND T1.MemberStatusId=@SystemCodeDetailId1
                WHERE NOT(T1.BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL) AND ISNUMERIC(T1.NationalIDNo)=1 AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
                GROUP BY T1.NationalIdNo
						) T2 ON try_CONVERT(bigint,T1.CGNationalIDNo)=try_CONVERT(bigint,T2.NationalIdNo)
            WHERE T1.PaymentCycleId=@PaymentCycleId AND NOT(@BeneficiaryTypeId=@BeneficiaryType_INDIVIDUAL)
) T1
    WHERE T1.ProgrammeId = @ProgrammeId AND PaymentCycleId = @PaymentCycleId


    BEGIN TRAN
    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PREPAYROLLFINAL'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    UPDATE T1
	SET T1.EnrolledHhs=@ActiveHHs
	   ,T1.PrePayrollBy=@UserId
	   ,T1.PrePayrollOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

    IF @@ERROR>0
	BEGIN
        ROLLBACK TRAN
        SELECT 0 AS NoOfRows
    END
	ELSE
	BEGIN
        COMMIT TRAN;
        SELECT 1 AS NoRows
    END
--EXEC GetPrepayrollAudit @PaymentCycleId=@PaymentCycleId;
END

GO
/****** Object:  StoredProcedure [dbo].[ProcessPrepayrollExceptionsAction]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[ProcessPrepayrollExceptionsAction]


    @PaymentCycleId int
   ,
    @ProgrammeId tinyint
   ,
    @ExceptionTypeIdsXML XML
   ,
    @Notes varchar(128)
   ,
    @EnrolmentGroupIdsXML XML=NULL
   ,
    @PrepayrollExceptionsXML XML=NULL
   ,
    @ReplacePreviousActioning bit=0
   ,
    @FilePath nvarchar(128)
   ,
    @HasSupportingDoc bit=0
   ,
    @RemovePreviousSupportingDoc bit=0
   ,
    @UserId int
AS
BEGIN
    DECLARE @tblNotActionable int
    DECLARE @SysCode varchar(20)
    DECLARE @SysDetailCode varchar(20)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @Category varchar(20)
    DECLARE @Exception_INVALIDBENEID varchar(20)
    DECLARE @Exception_INVALIDCGID varchar(20)
    DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
    DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
    DECLARE @Exception_DUPLICATECGIDIN varchar(20)
    DECLARE @Exception_DUPLICATECGIDACC varchar(20)
    DECLARE @Exception_INVALIDACC varchar(20)
    DECLARE @Exception_INVALIDCARD varchar(20)
    DECLARE @Exception_SUSPICIOUSAMT varchar(20)
    DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
    DECLARE @Exception_INELIGIBLEBENE varchar(20)
    DECLARE @Exception_INELIGIBLECG varchar(20)
    DECLARE @Exception_INELIGIBLESUS varchar(20)
    DECLARE @FromMonthId int
    DECLARE @ToMonthId int
    DECLARE @FinancialYearId int
    DECLARE @FileCreationId int
    DECLARE @FileName varchar(128)
    DECLARE @FileExtension varchar(5)
    DECLARE @ErrorMsg varchar(128)

    SET @SysCode='Exception Type'
    SET @Exception_INVALIDBENEID='INVALIDBENEID'
    SET @Exception_INVALIDCGID='INVALIDCGID'
    SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
    SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
    SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
    SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
    SET @Exception_INVALIDACC='INVALIDACC'
    SET @Exception_INVALIDCARD='INVALIDCARD'
    SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
    SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
    SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
    SET @Exception_INELIGIBLECG='INELIGIBLECG'
    SET @Exception_INELIGIBLESUS='INELIGIBLESUS'
    SELECT @FromMonthId=FromMonthId, @ToMonthId=ToMonthId, @FinancialYearId=FinancialYearId
    FROM PaymentCycle
    WHERE Id=@PaymentCycleId


    BEGIN TRAN

    DELETE FROM temp_ExceptionTypes WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;
    DELETE FROM temp_EnrolmentGroups WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;
    DELETE FROM temp_ExceptionActions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

    SET @SysCode='Exception Type'
    IF @ExceptionTypeIdsXML IS NOT NULL
		INSERT INTO temp_ExceptionTypes
        (PaymentCycleId,ProgrammeId,ExceptionTypeId,ExceptionTypeCode)
    SELECT @PaymentCycleId AS PaymentCycleId, @ProgrammeId AS ProgrammeId, T1.ExceptionTypeId, T2.Code AS ExceptionTypeCode
    FROM (
			SELECT U.R.value('(ExceptionTypeId)[1]','int') AS ExceptionTypeId
        FROM @ExceptionTypeIdsXML.nodes('ExceptionTypes/Record') AS U(R)
		) T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id
        INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@SysCode

    SET @SysCode='Enrolment Group'
    IF @EnrolmentGroupIdsXML IS NOT NULL
		INSERT INTO temp_EnrolmentGroups
        (PaymentCycleId,ProgrammeId,EnrolmentGroupId)
    SELECT @PaymentCycleId AS PaymentCycleId, @ProgrammeId AS ProgrammeId, T1.EnrolmentGroupId
    FROM (
			SELECT U.R.value('(EnrolmentGroupId)[1]','int') AS EnrolmentGroupId
        FROM @EnrolmentGroupIdsXML.nodes('EnrolmentGroups/Record') AS U(R)
		) T1 INNER JOIN SystemCodeDetail T2 ON T1.EnrolmentGroupId=T2.Id
        INNER JOIN SystemCode T3 ON T2.SystemCodeId=T3.Id AND T3.Code=@SysCode

    IF EXISTS(SELECT 1
    FROM temp_EnrolmentGroups) 
	BEGIN
        INSERT INTO temp_ExceptionActions
            (PaymentCycleId,ProgrammeId,HhId,PersonId)
                                                    SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.PersonId
            FROM PrepayrollInvalidID T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, T1.PersonId
            FROM PrepayrollDuplicateID T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, NULL
            FROM PrepayrollInvalidPaymentAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, NULL
            FROM PrepayrollInvalidPaymentCard T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, NULL
            FROM PrepayrollSuspicious T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        UNION
            SELECT T1.PaymentCycleId, T1.ProgrammeId, T1.HhId, NULL
            FROM PrepayrollIneligible T1 INNER JOIN HouseholdEnrolment T2 ON T1.HHId=T2.HhId
                INNER JOIN HouseholdEnrolmentPlan T3 ON T2.HhEnrolmentPlanId=T3.Id
                INNER JOIN temp_ExceptionTypes T4 ON T1.ExceptionTypeId=T4.ExceptionTypeId
                INNER JOIN temp_EnrolmentGroups T5 ON T3.EnrolmentGroupId=T5.EnrolmentGroupId
            WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
    END
	ELSE
		IF @PrepayrollExceptionsXML IS NOT NULL
		BEGIN
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID,@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC))
				INSERT INTO temp_ExceptionActions
            (PaymentCycleId,ProgrammeId,HhId,PersonId)
        SELECT @PaymentCycleId AS PaymentCycleId, @ProgrammeId AS ProgrammeId, T1.HhId, T1.PersonId
        FROM (
					SELECT U.R.value('(HhId)[1]','int') AS HhId
						  , U.R.value('(PersonId)[1]','int') AS PersonId
            FROM @PrepayrollExceptionsXML.nodes('PrepayrollExceptions/Record') AS U(R)
				) T1 
			ELSE
				INSERT INTO temp_ExceptionActions
            (PaymentCycleId,ProgrammeId,HhId)
        SELECT @PaymentCycleId AS PaymentCycleId, @ProgrammeId AS ProgrammeId, T1.HhId
        FROM (
					SELECT U.R.value('(HhId)[1]','int') AS HhId
            FROM @PrepayrollExceptionsXML.nodes('PrepayrollExceptions/Record') AS U(R)
				) T1
    END

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PREPAYROLLFINAL'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    IF NOT EXISTS(SELECT 1
    FROM PaymentCycle
    WHERE Id=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF NOT EXISTS(SELECT 1
    FROM PaymentCycleDetail T1
    WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T1.PaymentStageId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified PaymentCycleId appears not to be in the Prepayroll Stage where Prepayroll Exceptions actioning can be done'
	ELSE IF NOT EXISTS(SELECT 1
    FROM temp_ExceptionTypes)
		SET @ErrorMsg='Please specify valid ExceptionTypeIdsXML parameter'
	ELSE IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_INVALIDACC))
		SET @ErrorMsg='The exception type(s) specified cannot be actioned into the payroll'
	ELSE IF ISNULL(@Notes,'')=''
		SET @ErrorMsg='Please specify valid Notes parameter'
	ELSE IF NOT EXISTS(SELECT 1
    FROM temp_ExceptionActions)
		SET @ErrorMsg='There''re no matches for the specified EnrolmentGroupXML or PrepayrollExceptionsXML parameter'
	ELSE IF NOT EXISTS(SELECT 1
    FROM [User]
    WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        ROLLBACK TRAN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END

    IF @ReplacePreviousActioning=1
	BEGIN
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID))
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC))
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollDuplicateID T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDACC)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidPaymentAccount T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDCARD)
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollInvalidPaymentCard T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY))
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollSuspicious T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS))
			UPDATE T1 SET T1.Actioned=0,T1.Notes='',T1.ActionedBy=NULL,T1.ActionedOn=NULL,T1.ActionedApvBy=NULL,T1.ActionedApvOn=NULL FROM PrepayrollIneligible T1 WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
    END

    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID))
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidID T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId AND T1.PersonId=T2.PersonId
        SET @Category='INVALIDID'
    END
    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC))
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollDuplicateID T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId AND T1.PersonId=T2.PersonId
        SET @Category='DUPLICATEID'
    END
    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode=@Exception_INVALIDACC)
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidPaymentAccount T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId
        SET @Category='INVALIDACC'
    END
    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode=@Exception_INVALIDCARD)
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollInvalidPaymentCard T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId
        SET @Category='INVALIDCARD'
    END
    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY))
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollSuspicious T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId
        SET @Category='SUSPICIOUS'
    END
    IF EXISTS(SELECT 1
    FROM temp_ExceptionTypes
    WHERE ExceptionTypeCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS))
	BEGIN
        UPDATE T1 SET T1.Actioned=1,T1.Notes=@Notes,T1.ActionedBy=@UserId,T1.ActionedOn=GETDATE() FROM PrepayrollIneligible T1 INNER JOIN temp_ExceptionActions T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HHId=T2.HhId
        SET @Category='INELIGIBLE'
    END

    IF @RemovePreviousSupportingDoc=1
	BEGIN
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID))
			UPDATE T1 SET T1.InvalidIDActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC))
			UPDATE T1 SET T1.DuplicateIDActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDACC)
			UPDATE T1 SET T1.InvalidPaymentAccountActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDCARD)
			UPDATE T1 SET T1.InvalidPaymentCardActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY))
			UPDATE T1 SET T1.IneligibleBeneficiaryActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS))
			UPDATE T1 SET T1.SuspiciousPaymentActionsFileId=NULL FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND ProgrammeId=@ProgrammeId
    END

    --RECORDING THE FILE
    IF @HasSupportingDoc=1 
	BEGIN
        SET @SysCode='File Type'
        SET @SysDetailCode='SUPPORT'
        SELECT @SystemCodeDetailId1=T1.Id
        FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

        SET @SysCode='File Creation Type'
        SET @SysDetailCode='UPLOADED'
        SELECT @SystemCodeDetailId2=T1.Id
        FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

        SET @FileName='SUPPORT'+'_'+'EXCEPTIONACTION'+'_'+@Category+'_'+dbo.fn_MonthName((SELECT Code
        FROM SystemCodeDetail
        WHERE Id=@FromMonthId),1)+'-'+dbo.fn_MonthName((SELECT Code
        FROM SystemCodeDetail
        WHERE Id=@ToMonthId),1)+'_'+(SELECT REPLACE(Code,'/','')
        FROM SystemCodeDetail
        WHERE Id=@FinancialYearId)
        SET @FileExtension='.pdf'

        IF NOT EXISTS(SELECT 1
        FROM FileCreation
        WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2)
			INSERT INTO FileCreation
            (Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
        SELECT @FileName+@FileExtension AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, NULL AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

        SELECT @FileCreationId=Id
        FROM FileCreation
        WHERE Name=@FileName+@FileExtension AND TypeId=@SystemCodeDetailId1 AND CreationTypeId=@SystemCodeDetailId2

        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INVALIDBENEID,@Exception_INVALIDCGID))
			UPDATE T1 SET T1.InvalidIDActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_DUPLICATEBENEIDIN,@Exception_DUPLICATEBENEIDACC,@Exception_DUPLICATECGIDIN,@Exception_DUPLICATECGIDACC))
			UPDATE T1 SET T1.DuplicateIDActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDACC)
			UPDATE T1 SET T1.InvalidPaymentAccountActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode=@Exception_INVALIDCARD)
			UPDATE T1 SET T1.InvalidPaymentCardActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_SUSPICIOUSAMT,@Exception_SUSPICIOUSDORMANCY))
			UPDATE T1 SET T1.IneligibleBeneficiaryActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
        IF EXISTS(SELECT 1
        FROM temp_ExceptionTypes
        WHERE ExceptionTypeCode IN(@Exception_INELIGIBLEBENE,@Exception_INELIGIBLECG,@Exception_INELIGIBLESUS))
			UPDATE T1 SET T1.SuspiciousPaymentActionsFileId=@FileCreationId FROM PaymentCycleDetail T1 WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId
    END

    DELETE FROM temp_ExceptionTypes WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;
    DELETE FROM temp_EnrolmentGroups WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;
    DELETE FROM temp_ExceptionActions WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

    IF @@ERROR>0
	BEGIN
        ROLLBACK TRAN
        SELECT NULL AS FileId, 0 AS NoOfRows
    END
	ELSE
	BEGIN
        COMMIT TRAN
        SELECT @FileCreationId AS FileId, 1 AS NoOfRows
    END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPBeneAccountMonthlyActivity]
	@MonthNo tinyint
   ,@Year int
   ,@BankCode nvarchar(20)
   ,@BranchCode nvarchar(20)
   ,@EnrolmentNo int
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AccountOpenedOn datetime
   ,@HadUniqueWdl bit
   ,@UniqueWdlTrxNo nvarchar(50)=NULL
   ,@UniqueWdlDate datetime=NULL
   ,@HadBeneBiosVerified bit
   ,@IsDormant bit
   ,@DormancyDate datetime=NULL
   ,@IsDueForClawback bit
   ,@ClawbackAmount money=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @AccountId int
	DECLARE @MonthId int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @AccountId=T1.Id
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id AND T2.IsActive=1
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id AND T3.IsActive=1
	WHERE T3.Code=@BankCode AND T2.Code=@BranchCode AND LTRIM(RTRIM(T1.AccountNo))=LTRIM(RTRIM(@AccountNo)) AND LTRIM(RTRIM(T1.AccountName))=LTRIM(RTRIM(@AccountName))
			 	   
	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @MonthId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='System Settings'
	SET @SysDetailCode='CURFINYEAR'
	SELECT @SystemCodeDetailId1=T1.[Description] FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='ACTIVE'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF ISNULL(@AccountId,0)=0
		SET @ErrorMsg='Please specify valid beneficiary account details'
	ELSE IF (ISNULL(@MonthId,0)<=0)
		SET @ErrorMsg='Please specify valid MonthNo parameter'	
	ELSE IF (ISNULL(@Year,0)<1900 OR ISNULL(@Year,0)>YEAR(GETDATE()))
		SET @ErrorMsg='Please specify valid Year parameter'	
	ELSE IF (@HadUniqueWdl<>0 AND ISNULL(@UniqueWdlTrxNo,'')='')
		SET @ErrorMsg='Please specify valid UniqueWdlTrxNo parameter'	
	ELSE IF (@HadUniqueWdl<>0 AND @UniqueWdlDate IS NULL)
		SET @ErrorMsg='Please specify valid UniqueWdlDate parameter'	
	ELSE IF (@HadUniqueWdl<>0 AND @UniqueWdlDate>GETDATE())
		SET @ErrorMsg='Please specify valid UniqueWdlDate parameter'	
	ELSE IF (@IsDormant<>0 AND @DormancyDate IS NULL)
		SET @ErrorMsg='Please specify valid DormancyDate parameter'	
	ELSE IF (@IsDormant<>0 AND @DormancyDate>GETDATE())
		SET @ErrorMsg='Please specify valid DormancyDate parameter'	
	--NEED TO CONFIRM THIS VALIDATION. SHOULD THERE BE AMOUNTS FOR CLAWBACK
	ELSE IF (@IsDueForClawback<>0 AND ISNULL(@ClawbackAmount,0)<=0)
		SET @ErrorMsg='Please specify valid ClawbackAmount parameter'	
	ELSE IF NOT EXISTS(SELECT 1 FROM BeneAccountMonthlyActivity T1 WHERE T1.MonthId=@MonthId AND T1.[Year]=@Year AND T1.StatusId=@SystemCodeDetailId2)
		SET @ErrorMsg='The specified monthly activity period specified is not active'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	SELECT @SystemCodeDetailId2=Id FROM BeneAccountMonthlyActivity WHERE MonthId=@MonthId AND [Year]=@Year AND StatusId=@SystemCodeDetailId2

	BEGIN TRAN

	IF EXISTS(SELECT 1 
			  FROM BeneAccountMonthlyActivityDetail T1
			  WHERE T1.BeneAccountMonthlyActivityId=@SystemCodeDetailId2 AND T1.BeneAccountId=@AccountId
			  )
	BEGIN
		UPDATE T1
		SET T1.HadUniqueWdl=@HadUniqueWdl
		   ,T1.UniqueWdlTrxNo=@UniqueWdlTrxNo
		   ,T1.UniqueWdlDate=@UniqueWdlDate
		   ,T1.HadBeneBiosVerified=@HadBeneBiosVerified
		   ,T1.IsDormant=@IsDormant
		   ,T1.DormancyDate=@DormancyDate
		   ,T1.IsDueForClawback=@IsDueForClawback
		   ,T1.ClawbackAmount=@ClawbackAmount
		   ,T1.ModifiedBy=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM BeneAccountMonthlyActivityDetail T1
		WHERE T1.BeneAccountMonthlyActivityId=@SystemCodeDetailId2 AND T1.BeneAccountId=@AccountId
	END
	ELSE
	BEGIN
		INSERT INTO BeneAccountMonthlyActivityDetail(BeneAccountMonthlyActivityId,BeneAccountId,HadUniqueWdl,UniqueWdlTrxNo,UniqueWdlDate,HadBeneBiosVerified,IsDormant,DormancyDate,IsDueForClawback,ClawbackAmount,CreatedBy,CreatedOn)
		SELECT @SystemCodeDetailId2,@AccountId,@HadUniqueWdl,@UniqueWdlTrxNo,@UniqueWdlDate,@HadBeneBiosVerified,@IsDormant,@DormancyDate,@IsDueForClawback,@ClawbackAmount,@UserId,GETDATE()
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,0 AS NoOfRecs,'An error has occurred while attempting to store the monthly account activity in the database' AS [Description]
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId,@NoOfRows AS NoOfRecs,'The monthly account activity has been saved successfully' AS [Description]
	END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPCardBeneficiary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPCardBeneficiary]
	@EnrolmentNo int
   ,@AccountNo varchar(50)
   ,@PaymentCardNo varchar(50)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode			 
			 	   
	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRLPSP'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode	

	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo AND T2.StatusId=@SystemCodeDetailId1)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id INNER JOIN Household T3 ON T2.HhId=T3.Id WHERE T1.AccountNo=@AccountNo AND T3.StatusId=@SystemCodeDetailId2)
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id 
													  INNER JOIN Household T3 ON T2.HhId=T3.Id
													  INNER JOIN PSPBranch T4 ON T1.PSPBranchId=T4.Id AND T4.IsActive=1 
													  INNER JOIN PSP T5 ON T4.PSPId=T5.Id AND T5.IsActive=1 
				   WHERE T1.AccountNo=@AccountNo AND T2.Id=@EnrolmentNo AND T3.StatusId=@SystemCodeDetailId2 AND T5.UserId=@UserId)
		SET @ErrorMsg='Please specify valid EnrolmentNo, corresponding AccountNo and PSP UserId parameter'
	ELSE IF ISNULL(@PaymentCardNo,'')=''
		SET @ErrorMsg='Please specify valid PaymentCardNo parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SELECT @AccountId=Id FROM BeneficiaryAccount WHERE AccountNo=@AccountNo

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	UPDATE T1
	SET T1.PaymentCardNo=@PaymentCardNo
	   ,T1.StatusId=@SystemCodeDetailId2
	FROM BeneficiaryPaymentCard T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
								   INNER JOIN HouseholdEnrolment T3 ON T2.HhEnrolmentId=T3.Id
	WHERE T3.Id=@EnrolmentNo AND T2.AccountNo=@AccountNo 

	SET @SysCode='HHStatus'
	SET @SysDetailCode='PSPCARDED'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPEnrolBeneficiary]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
CREATE PROC [dbo].[PSPEnrolBeneficiary] 
	@EnrolmentNo int
   ,@TokenId int
   ,@BankCode nvarchar(20)
   ,@BranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AccountOpenedOn datetime
   ,@PriReciFirstName varchar(50)
   ,@PriReciMiddleName varchar(50)=NULL
   ,@PriReciSurname varchar(50)
   ,@PriReciNationalIdNo varchar(30)
   ,@PriReciSex char(1)
   ,@PriReciDoB datetime=NULL
   ,@SecReciFirstName varchar(50)=NULL
   ,@SecReciMiddleName varchar(50)=NULL
   ,@SecReciSurname varchar(50)=NULL
   ,@SecReciNationalIdNo varchar(30)=NULL
   ,@SecReciSex char(1)=NULL
   ,@SecReciDoB datetime=NULL
   ,@PaymentCardNo varchar(50)=	NULL
   ,@MobileNo1 nvarchar(20)=NULL
   ,@MobileNo2 nvarchar(20)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @HhEnrolmentId int
	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @PriReciId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @SystemCodeDetailId5 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @EnrolmentLimit int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int
    
	DECLARE @SecondaryRecMandatoryIfSupplied bit = 0

		insert into   PspEnrolmentLog(EnrolmentNo,PriReciFirstName,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSex,PriReciDoB,SecReciFirstName,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSex,SecReciDoB,MobileNo1,MobileNo2,AccountName,AccountNo,TokenId,BankCode,BranchCode,AccountOpenedOn,PaymentCardNo)  
select  @EnrolmentNo,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@PriReciSex,@PriReciDoB,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SecReciSex,@SecReciDoB,@MobileNo1,@MobileNo2,@AccountName,@AccountNo,@TokenId,@BankCode,@BranchCode,@AccountOpenedOn,@PaymentCardNo




	SET @EnrolmentLimit=523129
	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId5=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@PriReciSex
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PriReciId=T1.Id,@PriReciDoB=ISNULL(@PriReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.PrimaryRecipientId
	WHERE T1.FirstName=@PriReciFirstName AND T1.MiddleName=ISNULL(@PriReciMiddleName,'') AND T1.Surname=@PriReciSurname AND T1.NationalIdNo=@PriReciNationalIdNo AND T1.SexId=@SystemCodeDetailId3 --AND T1.DoB=@PriReciDoB 
		
	SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id,@SecReciDoB=ISNULL(@SecReciDoB,T1.DoB)
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname AND   ISNULL(T1.NationalIdNo,'')= ISNULL(@SecReciNationalIdNo,'') AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB 
						
	SELECT @SecondaryRecMandatoryIfSupplied =1
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T3.Id = @EnrolmentNo and T6.Code='OPCT' 
		

	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
	 
     IF(ISNULL(@TokenId,0)=0)
     SET @ErrorMsg='Please specify valid Token ID parameter'
    ELSE IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id INNER JOIN Household T3 ON T1.HhId=T3.Id WHERE T1.Id=@EnrolmentNo AND T3.TokenId = @TokenId AND T2.StatusId=@SystemCodeDetailId1)  
		SET @ErrorMsg='Please specify valid Corresponding EnrolmentNo and TokenID parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF @AccountOpenedOn IS NULL OR NOT (@AccountOpenedOn<=GETDATE())
		SET @ErrorMsg='Please specify valid AccountOpenedOn parameter'
	ELSE IF ISNULL(@PriReciId,0)=0
		SET @ErrorMsg='Please specify valid Primary Recipient details'
	
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	
	ELSE IF((@SecondaryRecMandatoryIfSupplied=1) AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'


	ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)
		SET @ErrorMsg='The EnrolmentNo and Secondary Recipient details do not match'
	ELSE IF ISNULL(@MobileNo1,'')<>'' AND (CASE WHEN(@MobileNo1 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo1 parameter'
	ELSE IF ISNULL(@MobileNo2,'')<>'' AND (CASE WHEN(@MobileNo2 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0
		SET @ErrorMsg='Please specify valid MobileNo2 parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 
				   FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
											  INNER JOIN PSP T3 ON T2.PSPId=T3.Id
				   WHERE HhEnrolmentId=@EnrolmentNo AND ExpiryDate>GETDATE() 
						AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)
				   )
		SET @ErrorMsg='The household appears to have an existing account with a PSP'
    

	--ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@PriReciNationalIdNo)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
	--	SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	--ELSE IF EXISTS(SELECT 1 FROM [70PlusRegistration] WHERE OP_CONFIRM_ID_NO=@PriReciNationalIdNo AND RefId>523449)	--THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED
	--	SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'
	--ELSE IF (SELECT COUNT(DISTINCT T1.HhEnrolmentId) FROM BeneficiaryAccount T1 WHERE T1.StatusId=@SystemCodeDetailId5)>=@EnrolmentLimit	
	--	SET @ErrorMsg='The beneficiary enrolment numbers limit has exceeded. Currently, no more beneficiary enrolment will be allowed'
	ELSE IF EXISTS(	  SELECT 1 
					  FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
												 INNER JOIN PSP T3 ON T2.PSPId=T3.Id
					  WHERE T1.HhEnrolmentId=@EnrolmentNo
				)
		SET @ErrorMsg='Once a beneficiary has been enrolled with a valid account number it cannot be changed'			  
	--ELSE IF EXISTS(	SELECT @HhEnrolId=T1.Id,@BeneAccountId=T7.BeneAccountId,@PSPCode='The household can only be carded by '+T9.Code,@PSPUserId=T9.UserId
	--				FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id AND T2.StatusId IN(SELECT T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id WHERE T2.Code='HHStatus' AND T1.Code IN('ENRL','PSPCARDED','ONPAY'))
	--										   INNER JOIN HouseholdMember T3 ON T2.Id=T3.HhId AND T3.StatusId=@SystemCodeDetailId1
	--										   INNER JOIN Person T4 ON T3.PersonId=T4.Id
	--										   INNER JOIN Programme T5 ON T2.ProgrammeId=T5.Id AND T3.MemberRoleId=T5.PrimaryRecipientId
	--										   INNER JOIN BeneficiaryAccount T6 ON T1.Id=T6.HhEnrolmentId
	--										   INNER JOIN BeneficiaryPaymentCard T7 ON T6.Id=T7.BeneAccountId AND T7.PriReciNationalIdNo=@NationalIdNo AND T7.StatusId=@SystemCodeDetailId2
	--										   LEFT JOIN PSPBranch T8 ON T6.PSPBranchId=T8.Id
	--										   LEFT JOIN PSP T9 ON T8.PSPId=T9.Id
	--				WHERE T4.NationalIdNo=@NationalIdNo 
	--				)

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	SET @SysCode='System Settings'
	SET @SysDetailCode='BENEPSPRENEWALDATE'
	SELECT @AccountExpiryDate=CASE WHEN(ISNULL(T1.[Description],'')='' OR CONVERT(datetime,T1.[Description])<GETDATE()) THEN CONVERT(datetime,'30 June '+CONVERT(varchar(4),CASE WHEN(GETDATE()>CONVERT(datetime,'30 June '+CONVERT(varchar(4),YEAR(GETDATE())))) THEN YEAR(GETDATE())+1 ELSE YEAR(GETDATE()) END))
																													    ELSE CONVERT(datetime,T1.[Description])
							  END
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	--IF EXISTS(SELECT 1 
	--		  FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--									 INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--		  WHERE T1.HhEnrolmentId=@EnrolmentNo --AND T1.AccountNo=@AccountNo
	--		  )
	--BEGIN
	--	UPDATE T1
	--	SET T1.PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) 
	--	   ,T1.AccountNo=@AccountNo
	--	   ,T1.AccountName=@AccountName
	--	   ,T1.OpenedOn=@AccountOpenedOn
	--	   ,T1.StatusId=@SystemCodeDetailId1
	--	   ,T1.ExpiryDate=@AccountExpiryDate
	--	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo

	--	SELECT @AccountId=T1.Id
	--	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
	--							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	--	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.AccountNo=@AccountNo
	--END
	--ELSE
	--BEGIN
		INSERT INTO BeneficiaryAccount(HhEnrolmentId,PSPBranchId,AccountNo,AccountName,OpenedOn,StatusId,ExpiryDate)
		SELECT @EnrolmentNo AS HhEnrolmentId
			,(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AS PSPBranchId
			,@AccountNo AS AccountNo
			,@AccountName AS AccountName
			,@AccountOpenedOn AS OpenedOn
			,@SystemCodeDetailId1 AS StatusId
			,@AccountExpiryDate AS ExpiryDate

		--SET @AccountId=IDENT_CURRENT('BeneficiaryAccount')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
		SELECT @AccountId=Id FROM BeneficiaryAccount WHERE PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AND AccountNo=@AccountNo
	--END

	SET @SysCode='Card Status'
	SET @SysDetailCode= CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN '-1' ELSE '1' END
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId

	--IF ISNULL(@PaymentCardId,0)>0
	--BEGIN
	--	UPDATE T1
	--	SET T1.BeneAccountId=@AccountId
	--	   ,T1.PriReciId=@PriReciId
	--	   ,T1.PriReciFirstname=@PriReciFirstName
	--	   ,T1.PriReciMiddleName=@PriReciMiddleName
	--	   ,T1.PriReciSurname=@PriReciSurname
	--	   ,T1.PriReciNationalIdNo=@PriReciNationalIdNo
	--	   ,T1.PriReciSexId=@SystemCodeDetailId3
	--	   ,T1.PriReciDoB=@PriReciDoB
	--	   ,T1.SecReciId=@SecReciId
	--	   ,T1.SecReciFirstname=@SecReciFirstName
	--	   ,T1.SecReciMiddleName=@SecReciMiddleName
	--	   ,T1.SecReciSurname=@SecReciSurname
	--	   ,T1.SecReciNationalIdNo=@SecReciNationalIdNo
	--	   ,T1.SecReciSexId=@SystemCodeDetailId4
	--	   ,T1.SecReciDoB=@SecReciDoB
	--	   ,T1.PaymentCardNo=@PaymentCardNo
	--	   ,T1.MobileNo1=@MobileNo1
	--	   ,T1.MobileNo2=@MobileNo2
	--	   ,T1.StatusId=@SystemCodeDetailId2
	--	   ,T1.ModifiedBy=@UserId
	--	   ,T1.ModifiedOn=GETDATE()
	--	FROM BeneficiaryPaymentCard T1
	--	WHERE T1.Id=@PaymentCardId
	--END
	--ELSE
	--BEGIN
		INSERT INTO BeneficiaryPaymentCard(BeneAccountId,PriReciId,PriReciFirstname,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSexId,PriReciDoB,SecReciId,SecReciFirstname,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSexId,SecReciDoB,PaymentCardNo,MobileNo1,MobileNo2,StatusId,CreatedBy,CreatedOn)
		SELECT @AccountId,@PriReciId,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@SystemCodeDetailId3,@PriReciDoB,@SecReciId,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SystemCodeDetailId4,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@SystemCodeDetailId2,@UserId,GETDATE()
	
		SELECT @PaymentCardId=Id FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId AND CreatedBy=@UserId AND StatusId=@SystemCodeDetailId2
		--SET @PaymentCardId=IDENT_CURRENT('BeneficiaryPaymentCard')	--SEEMS TO INTRODUCE A LOGICAL BUG WHEN EXECUTED IN MULTIPLE THREADS
	--END


	SET @SysCode='HHStatus'
	SET @SysDetailCode='ENRL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode=CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN 'ENRLPSP' ELSE 'PSPCARDED' END
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId3
	   ,T1.ModifiedBy=@UserId
	   ,T1.ModifiedOn=GETDATE()
	FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
	WHERE T2.Id=@EnrolmentNo AND T1.StatusId=@SystemCodeDetailId1

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT NULL AS BeneAccountId,NULL AS PaymentCardId,NULL AS PriReciId,NULL AS SecReciId
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @AccountId AS BeneAccountId,@PaymentCardId AS PaymentCardId,@PriReciId AS PriReciId,@SecReciId AS SecReciId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[PSPEnrolCargiver]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPEnrolCargiver]
	@EnrolmentNo int
   ,@BankCode varchar(20)
   ,@BranchCode varchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@SecReciFirstName varchar(50)
   ,@SecReciMiddleName varchar(50)=NULL
   ,@SecReciSurname varchar(50)
   ,@SecReciNationalIdNo varchar(30)=NULL
   ,@SecReciSex char(1)
   ,@SecReciDoB datetime=NULL

   ,@SecReciRT nvarchar(max)=NULL
   ,@SecReciRI nvarchar(max)=NULL
   ,@SecReciRMF nvarchar(max)=NULL
   ,@SecReciRRF nvarchar(max)=NULL
   ,@SecReciRP nvarchar(max)=NULL
   ,@SecReciLT nvarchar(max)=NULL
   ,@SecReciLI nvarchar(max)=NULL
   ,@SecReciLMF nvarchar(max)=NULL
   ,@SecReciLRF nvarchar(max)=NULL
   ,@SecReciLP nvarchar(max)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @HhEnrolmentId int
	DECLARE @AccountId int
	DECLARE @PaymentCardId int
	DECLARE @SecReciId int
	DECLARE @SecReciMandatory bit
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @SystemCodeDetailId4 int
	DECLARE @AccountExpiryDate datetime
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Sex'
	SET @SysDetailCode=@SecReciSex
	SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		
	SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id 
	FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2
				   INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId
				   INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1
				   INNER JOIN Household T5 ON T3.HhId=T5.Id
				   INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId
	WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB 
						
	SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory
	FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id 
	WHERE T1.Id=@EnrolmentNo
			 
	SELECT @AccountId=T1.Id
	FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
							   INNER JOIN PSP T3 ON T2.PSPId=T3.Id
	WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.ExpiryDate>GETDATE() AND T1.AccountNo=@AccountNo AND T1.AccountName=@AccountName AND T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId
			 	   
	IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo)
		SET @ErrorMsg='Please specify valid EnrolmentNo parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)
		SET @ErrorMsg='Please specify valid BankCode parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid BranchCode parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''
		SET @ErrorMsg='Please specify valid AccountNo parameter'
	ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''
		SET @ErrorMsg='Please specify valid AccountName parameter'
	ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)
		SET @ErrorMsg='Please specify valid Secondary Recipient details'
	ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)
		SET @ErrorMsg='The EnrolmentNo and caregiver details do not match'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode)
		SET @ErrorMsg='Please specify valid UserId parameter'
	ELSE IF EXISTS(SELECT 1 
				   FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id 
											  INNER JOIN PSP T3 ON T2.PSPId=T3.Id
				   WHERE T1.HhEnrolmentId=@EnrolmentNo AND T1.ExpiryDate>GETDATE() 
						AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)
				   )
		SET @ErrorMsg='The household appears to have an existing account with a different PSP'
	ELSE IF(ISNULL(@AccountId,0)=0)
		SET @ErrorMsg='The specified account details do not return a match with an existing beneficiary account'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN
	
	SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId

	IF ISNULL(@PaymentCardId,0)>0
	BEGIN
		UPDATE T1
		SET T1.SecReciId=@SecReciId
		   ,T1.SecReciFirstname=@SecReciFirstName
		   ,T1.SecReciMiddleName=@SecReciMiddleName
		   ,T1.SecReciSurname=@SecReciSurname
		   ,T1.SecReciNationalIdNo=@SecReciNationalIdNo
		   ,T1.SecReciSexId=@SystemCodeDetailId4
		   ,T1.SecReciDoB=@SecReciDoB
		FROM BeneficiaryPaymentCard T1
		WHERE T1.Id=@PaymentCardId

		UPDATE T1
		SET T1.SecReciRT=CONVERT(varbinary(max),@SecReciRT)
		   ,T1.SecReciRI=CONVERT(varbinary(max),@SecReciRI)
		   ,T1.SecReciRMF=CONVERT(varbinary(max),@SecReciRMF)
		   ,T1.SecReciRRF=CONVERT(varbinary(max),@SecReciRRF)
		   ,T1.SecReciRP=CONVERT(varbinary(max),@SecReciRP)
		   ,T1.SecReciLT=CONVERT(varbinary(max),@SecReciLT)
		   ,T1.SecReciLI=CONVERT(varbinary(max),@SecReciLI)
		   ,T1.SecReciLMF=CONVERT(varbinary(max),@SecReciLMF)
		   ,T1.SecReciLRF=CONVERT(varbinary(max),@SecReciLRF)
		   ,T1.SecReciLP=CONVERT(varbinary(max),@SecReciLP)
		FROM PaymentCardBiometrics T1
		WHERE T1.BenePaymentCardId=@PaymentCardId
	END
	
	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows
	END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPFinalizeBeneAccountMonthlyActivity]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPFinalizeBeneAccountMonthlyActivity]
	@MonthNo tinyint
   ,@Year int
   ,@BankCode nvarchar(20)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @PaymentCycleId int
	DECLARE @MonthlyActivityId int
	DECLARE @MonthId int
	DECLARE @YearId int
	DECLARE @ActiveHhs int
	DECLARE @AccountActivityHhs int
	DECLARE @PSPActiveHhs int
	DECLARE @PSPAccountActivityHhs int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Calendar Months'
	SET @SysDetailCode=@MonthNo
	SELECT @MonthId=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='A/C Activity Status'
	SET @SysDetailCode='ACTIVE'
	SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @MonthlyActivityId=Id FROM BeneAccountMonthlyActivity WHERE MonthId=@MonthId AND [Year]=@Year AND StatusId=@SystemCodeDetailId2

	IF (ISNULL(@MonthId,0)<=0)
		SET @ErrorMsg='Please specify valid MonthNo parameter'	
	ELSE IF (ISNULL(@Year,0)<1900 OR ISNULL(@Year,0)>YEAR(GETDATE()))
		SET @ErrorMsg='Please specify valid Year parameter'	
	ELSE IF ISNULL(@MonthlyActivityId,0)<=0
		SET @ErrorMsg='The specified monthly activity period specified is not active'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	SELECT @ActiveHhs=COUNT(T1.BeneAccountId),@PSPActiveHhs=SUM(CASE WHEN(T1.BankCode=@BankCode) THEN 1 ELSE 0 END),@AccountActivityHhs=SUM(CASE WHEN(T2.BeneAccountId>0) THEN 1 ELSE 0 END),@PSPAccountActivityHhs=SUM(CASE WHEN(T2.BankCode=@BankCode) THEN 1 ELSE 0 END)
	FROM (
			SELECT T3.Id AS BeneAccountId,T5.Code AS BankCode
			FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId
							  INNER JOIN BeneficiaryAccount T3 ON T2.Id=T3.HhEnrolmentId AND T3.StatusId=@SystemCodeDetailId3
							  INNER JOIN PSPBranch T4 ON T3.PSPBranchId=T4.Id
							  INNER JOIN PSP T5 ON T4.PSPId=T5.Id
			WHERE DATEDIFF(MM,T3.OpenedOn,CONVERT(datetime,'1 '+dbo.fn_MonthName(@MonthNo,0)+' '+CONVERT(varchar(4),@Year)))>=0
		 ) T1 LEFT JOIN (
							SELECT T1.BeneAccountId,T4.Code AS BankCode
							FROM BeneAccountMonthlyActivityDetail T1 INNER JOIN BeneficiaryAccount T2 ON T1.BeneAccountId=T2.Id
																	 INNER JOIN PSPBranch T3 ON T2.PSPBranchId=T3.Id
																	 INNER JOIN PSP T4 ON T3.PSPId=T4.Id
							WHERE T1.BeneAccountMonthlyActivityId=@MonthlyActivityId
						) T2 ON T1.BeneAccountId=T2.BeneAccountId

	BEGIN TRAN
	
	IF @ActiveHhs=@AccountActivityHhs
	BEGIN
		SET @SysCode='A/C Activity Status'
		SET @SysDetailCode='SUBMISSIONAPV'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.StatusId=@SystemCodeDetailId1
		FROM BeneAccountMonthlyActivity T1
		WHERE t1.Id=@MonthlyActivityId
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,@AccountActivityHhs AS NoOfRecs,'Fatal Error' AS [Description]
	END
	ELSE
	BEGIN
		COMMIT TRAN
		IF ISNULL(@PSPActiveHhs,0)>ISNULL(@PSPAccountActivityHhs,0)
			SELECT -1 AS StatusId,@PSPAccountActivityHhs AS NoOfRecs,'Payment results has not yet been updated for the complete set of beneficiaries. '+CONVERT(varchar(10),@PSPActiveHhs-@PSPAccountActivityHhs)+' out of '+CONVERT(varchar(10),@PSPActiveHhs)+' are still remaining to be updated.' AS [Description]
		ELSE
			SELECT 0 AS StatusId,@PSPAccountActivityHhs AS NoOfRecs,'All your payment information has been updated successfully' AS [Description]
	END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPFinalizePayrollTrx]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPFinalizePayrollTrx]
	@PaymentCycleId int
   ,@BankCode nvarchar(20)
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @PayrollHhs int
	DECLARE @PaymentHhs int
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEXCONF'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @PayrollHhs=COUNT(T1.HhId)
	FROM Payroll T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN HouseholdEnrolment T3 ON T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T2.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN PaymentCycleDetail T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.Code=@BankCode AND T7.PaymentStageId=@SystemCodeDetailId1

	SELECT @PaymentHhs=COUNT(T1.HhId)
	FROM Payment T1 INNER JOIN Prepayroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
					INNER JOIN HouseholdEnrolment T3 ON T1.HhId=T3.HhId
					INNER JOIN BeneficiaryAccount T4 ON T2.BeneAccountId=T4.Id
					INNER JOIN PSPBranch T5 ON T4.PSPBranchId=T5.Id
					INNER JOIN PSP T6 ON T5.PSPId=T6.Id
					INNER JOIN PaymentCycleDetail T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T6.Code=@BankCode AND T7.PaymentStageId=@SystemCodeDetailId1
		 	   
	IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not in the payment stage. Contact SAU'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN
	
	IF @PayrollHhs=@PaymentHhs
	BEGIN
		SET @SysDetailCode='POSTPAYROLL'
		SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PostPayrollBy=@UserId
		   ,T1.PostPayrollOn=GETDATE()
		   ,T1.PaymentStageId=@SystemCodeDetailId2
		FROM PaymentCycleDetail T1 INNER JOIN (
												SELECT PaymentCycleId,ProgrammeId,COUNT(HhId) AS PayrollHhs
												FROM Payroll
												GROUP BY PaymentCycleId,ProgrammeId
												) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId
								   INNER JOIN (
												SELECT PaymentCycleId,ProgrammeId,COUNT(HhId) AS PaymentHhs
												FROM Payment
												GROUP BY PaymentCycleId,ProgrammeId
												) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.PaymentStageId=@SystemCodeDetailId1 AND T2.PayrollHhs=T3.PaymentHhs
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,@PaymentHhs AS NoOfRecs,'Fatal Error' AS [Description]
	END
	ELSE
	BEGIN
		COMMIT TRAN
		IF ISNULL(@PayrollHhs,0)>ISNULL(@PaymentHhs,0)
			SELECT -1 AS StatusId,@PaymentHhs AS NoOfRecs,'Payment results has not yet been updated for the complete set of beneficiaries. '+CONVERT(varchar(10),@PayrollHhs-@PaymentHhs)+' out of '+CONVERT(varchar(10),@PayrollHhs)+' are still remaining to be updated.' AS [Description]
		ELSE
			SELECT 0 AS StatusId,@PaymentHhs AS NoOfRecs,'All your payment information has been updated successfully' AS [Description]
	END
END

GO
/****** Object:  StoredProcedure [dbo].[PSPPayrollTrx]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSPPayrollTrx]
	@PaymentCycleId int
   ,@EnrolmentNo int
   ,@BankCode nvarchar(20)
   ,@BranchCode nvarchar(20)
   ,@AccountNo varchar(50)
   ,@AccountName varchar(100)
   ,@AmountTransferred money
   ,@WasTransferSuccessful bit
   ,@TrxNo nvarchar(50)=NULL
   ,@TrxDate datetime=NULL
   ,@TrxNarration nvarchar(128)=NULL
   ,@UserId int
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @BeneHhId int
	DECLARE @BeneProgrammeId tinyint
	DECLARE @BeneBankCode nvarchar(20)
	DECLARE @BeneBranchCode nvarchar(20)
	DECLARE @BeneAccountNo varchar(50)
	DECLARE @BeneAccountName varchar(100)
	DECLARE @PaymentAmount money
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int


	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLEXCONF'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @BeneHhId=T1.HhId,@BeneProgrammeId=T3.ProgrammeId,@BeneBankCode=T7.Code,@BeneBranchCode=T6.Code,@BeneAccountNo=T5.AccountNo,@BeneAccountName=T5.AccountName,@PaymentAmount=T1.PaymentAmount
	FROM Payroll T1 INNER JOIN Household T2 ON T1.PaymentCycleId=@PaymentCycleId AND T1.HhId=T2.Id
					INNER JOIN Prepayroll T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T2.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
					INNER JOIN HouseholdEnrolment T4 ON T1.HhId=T4.HhId
					INNER JOIN BeneficiaryAccount T5 ON T3.BeneAccountId=T5.Id
					INNER JOIN PSPBranch T6 ON T5.PSPBranchId=T6.Id
					INNER JOIN PSP T7 ON T6.PSPId=T7.Id
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T4.Id=@EnrolmentNo

	IF EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not in the payment stage. Contact SAU'
	ELSE IF ISNULL(@BeneHhId,0)=0
		SET @ErrorMsg='Please specify valid combination of PaymentCycleId, ProgrammeId and EnrolmentNo parameters'
	ELSE IF @WasTransferSuccessful IS NULL
		SET @ErrorMsg='Please specify valid WasTrxSuccessful parameters'
	ELSE IF @BankCode<>@BeneBankCode
		SET @ErrorMsg='Please specify valid BankCode parameter for the beneficiary'
	ELSE IF @BranchCode<>@BeneBranchCode
		SET @ErrorMsg='Please specify valid BranchCode parameter for the beneficiary'
	ELSE IF @AccountNo<>@BeneAccountNo
		SET @ErrorMsg='Please specify valid AccountNo parameter for the beneficiary'
	ELSE IF @AccountName<>@BeneAccountName
		SET @ErrorMsg='Please specify valid AccountName parameter for the beneficiary'
	ELSE IF @PaymentAmount<>@AmountTransferred AND @AmountTransferred>0
		SET @ErrorMsg='Please specify valid AmountTranferred parameter for the beneficiary'
	ELSE IF @AmountTransferred>0 AND @WasTransferSuccessful=0
		SET @ErrorMsg='Please specify valid corresponding AmountTranferred and WasTransferSuccessful parameters for the beneficiary'
	ELSE IF ISNULL(@TrxNo,'')='' AND (@AmountTransferred>0 OR @WasTransferSuccessful=1)
		SET @ErrorMsg='Please specify valid TrxNo parameter'
	ELSE IF @TrxDate>GETDATE() AND (@AmountTransferred>0 OR @WasTransferSuccessful=1)
		SET @ErrorMsg='Please specify valid TrxDate parameter'
	ELSE IF ISNULL(@TrxNarration,'')='' AND (ISNULL(@AmountTransferred,0)=0 OR @WasTransferSuccessful=0)
		SET @ErrorMsg='Please specify TrxNarration TrxDate parameter'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	BEGIN TRAN

	IF EXISTS(SELECT 1 
			  FROM Payment T1
			  WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@BeneProgrammeId AND T1.HhId=@BeneHhId
			  )
	BEGIN
		UPDATE T1
		SET T1.WasTrxSuccessful=@WasTransferSuccessful
		   ,T1.TrxAmount=@AmountTransferred
		   ,T1.TrxNo=@TrxNo
		   ,T1.TrxDate=@TrxDate
		   ,T1.TrxNarration=@TrxNarration
		   ,T1.CreatedBy=@UserId
		   ,T1.CreatedOn=GETDATE()
		FROM Payment T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@BeneProgrammeId AND T1.HhId=@BeneHhId
	END
	ELSE
	BEGIN
		INSERT INTO Payment(PaymentCycleId,ProgrammeId,HhId,WasTrxSuccessful,TrxAmount,TrxNo,TrxDate,TrxNarration,CreatedBy,CreatedOn)
		SELECT @PaymentCycleId,@BeneProgrammeId,@BeneHhId,@WasTransferSuccessful,@AmountTransferred,@TrxNo,@TrxDate,@TrxNarration,@UserId,GETDATE()
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,'An error has occurred while attempting to update beneficiary payment information' AS [Description]
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT 0 AS StatusId,'Beneficiary payment information successfully updated' AS [Description]
	END
END

GO
/****** Object:  StoredProcedure [dbo].[RptGetBeneficiariesAccounts]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROC [dbo].[RptGetBeneficiariesAccounts]
 
	  @DateRange varchar(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @PspXml xml,
    
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type varchar(100)= ''

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE @offset int = (@Page-1)*@PageSize
    DECLARE @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    declare @Guid varchar(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			 SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
               
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'PSP' AS ReportType
                FROM @PspXml.nodes('Report/Record') AS U(R) ) T1 
			 
			) T0
      
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T100.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T100.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T100.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
         		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
     LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
      
        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
	select X.*, T13.*, T4.Name, T4.code from x 
	inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	
	
	ELSE  IF(@Type='DETAILS')
	BEGIN
      SELECT   
           @NoOfRows  =  Count_BIG(T1.Id) 
           
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
     INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
 
    
    SELECT  T2.Id
    ,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		
    , T6.FirstName AS BeneFirstName 
    ,T1.CreatedOn
    , T4.Code Programme
          , T6.MiddleName AS BeneMiddleName
          , T6.Surname AS BeneSurname
          , T6.NationalIdNo AS BeneIDNo
          , T7.Code AS BeneSex
          , T6.DoB AS BeneDoB
          , ISNULL(T90.FirstName,'') AS CGFirstName
          , ISNULL(T90.MiddleName,'') AS CGMiddleName
          , ISNULL(T90.Surname,'') AS CGSurname
          , ISNULL(T90.NationalIdNo,'') AS CGIDNo
          , ISNULL(T100.Code,'') AS CGSex
          , ISNULL(T90.DoB,'') AS CGDoB
          , ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
          , ISNULL(T90.MobileNo1,T90.MobileNo2) AS MobileNo2
          , T13.County
          , T13.Constituency
          , T13.District
          , T13.Division
          , T13.Location
          , T13.SubLocation
          ,T15.[Name] AS Psp
          ,T16.[Name] AS PspBranch
          ,T14.OpenedOn AccountOpenedOn
         ,T14.DateAdded AccountAddedOn
          ,T14.AccountNo
          ,T14.AccountName
           
          ,T14B.[Description] AccountStatus
          ,T1B.[Description] HouseholdStatus
          ,@NoOfRows NoOfRows            
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
               		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
    ORDER BY T1.Id
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END

GO
/****** Object:  StoredProcedure [dbo].[RptGetBeneficiariesPaymentCards]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RptGetBeneficiariesPaymentCards]
 
	  @DateRange varchar(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @PspXml xml,
    @CardStatusXml xml,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type varchar(100)= ''

AS
BEGIN
    IF(@Page=0) set @Page =1
    DECLARE @offset int = (@Page-1)*@PageSize
    DECLARE @NoOfRows int=0
    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')


    declare @Guid varchar(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			 SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
               
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'PSP' AS ReportType
                FROM @PspXml.nodes('Report/Record') AS U(R) ) T1 
				UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CARD_STATUS' AS ReportType
                FROM @CardStatusXml.nodes('Report/Record') AS U(R) ) T1
			) T0
      
    IF(@Type='SUMMARY')
 	
	BEGIN
 
 ;with x as (
        SELECT T13.SubLocationId, T4.Id ProgrammeId, COUNT(T2.Id) as Total
		, COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci, COUNT(T2.Id) AS TotalPriReci
		, COUNT(CASE WHEN T100.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T100.Code = 'F' THEN 1 END ) AS FemaleSecReci, COUNT(case when T100.Id is not null then 1 end) AS TotalSecReci
        ,COUNT(*) OVER ( ) as  NoOfRows
        FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN BeneficiaryPaymentCard T17 ON T14.Id = T17.BeneAccountId
    INNER JOIN SystemCodeDetail T18 ON T17.StatusId=T18.Id                 		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    INNER JOIN temp_ReportID T24 ON T24.ReportId  = T17.StatusId AND T24.ReportType = 'CARD_STATUS' and T24.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
      
        GROUP BY  T13.SubLocationId, T4.Id
        ORDER by  T13.SubLocationId, T4.Id
				OFFSET     @offset ROWS       
				FETCH NEXT @PageSize ROWS ONLY
) 
	select X.*, T13.*, T4.Name, T4.code from x 
	inner join (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
                T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId
            FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
                INNER JOIN Division T3 ON T2.DivisionId=T3.Id
                INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
                INNER JOIN District T5 ON T4.DistrictId=T5.Id
                INNER JOIN County T6 ON T4.CountyId=T6.Id
                INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
                INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON x.SubLocationId=T13.SubLocationId
             INNER JOIN Programme T4 ON x.ProgrammeId=T4.Id
    END
	
	
	ELSE  IF(@Type='DETAILS')
	BEGIN
      SELECT   
           @NoOfRows  =  Count_BIG(T1.Id) 
           
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN BeneficiaryPaymentCard T17 ON T14.Id = T17.BeneAccountId
    INNER JOIN SystemCodeDetail T18 ON T17.StatusId=T18.Id                 		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    INNER JOIN temp_ReportID T24 ON T24.ReportId  = T17.StatusId AND T24.ReportType = 'CARD_STATUS' and T24.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
 
    
    SELECT  T2.Id
    ,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		
    , T6.FirstName AS BeneFirstName 
    ,T1.CreatedOn
    , T4.Code Programme
          , T6.MiddleName AS BeneMiddleName
          , T6.Surname AS BeneSurname
          , T6.NationalIdNo AS BeneIDNo
          , T7.Code AS BeneSex
          , T6.DoB AS BeneDoB
          , ISNULL(T90.FirstName,'') AS CGFirstName
          , ISNULL(T90.MiddleName,'') AS CGMiddleName
          , ISNULL(T90.Surname,'') AS CGSurname
          , ISNULL(T90.NationalIdNo,'') AS CGIDNo
          , ISNULL(T100.Code,'') AS CGSex
          , ISNULL(T90.DoB,'') AS CGDoB
          , ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
          , ISNULL(T90.MobileNo1,T90.MobileNo2) AS MobileNo2
          , T13.County
          , T13.Constituency
          , T13.District
          , T13.Division
          , T13.Location
          , T13.SubLocation
          ,T15.[Name] AS Psp
          ,T16.[Name] AS PspBranch
          ,T14.OpenedOn AccountOpenedOn
         ,T14.DateAdded AccountAddedOn
          ,T14.AccountNo
          ,T14.AccountName
          ,T17.PaymentCardNo
          ,T18.[Description] CardingStatus
          ,T14B.[Description] AccountStatus
          ,T1B.[Description] HouseholdStatus
          ,@NoOfRows NoOfRows            
          FROM Household T1
    INNER JOIN SystemCodeDetail T1B ON T1.StatusId = T1B.Id
    INNER JOIN HouseholdEnrolment T2 ON T2.HhId = T1.Id
    INNER JOIN HouseholdEnrolmentPlan  T2A ON T2A.Id = T2.HhEnrolmentPlanId  
    INNER JOIN Programme T4 ON T1.ProgrammeId=T4.Id  
    INNER JOIN temp_ReportID T19 ON T19.ReportId  = T1.ProgrammeId AND T19.ReportType = 'PROGRAMME' and T19.ReportGuId = @Guid   
    INNER JOIN HouseholdMember T5 ON T1.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id 
    INNER JOIN temp_ReportID T20 ON T20.ReportId  = T7.Id AND T20.ReportType = 'PRI_SEX' and T20.ReportGuId = @Guid   
    INNER JOIN HouseholdSubLocation T11 ON T1.Id=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (
              SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County,
              T6.Id AS CountyId, T7.Name AS Constituency, T7.Id AS ConstituencyId FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
              INNER JOIN Division T3 ON T2.DivisionId=T3.Id
              INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
              INNER JOIN District T5 ON T4.DistrictId=T5.Id
              INNER JOIN County T6 ON T4.CountyId=T6.Id
              INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
              INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
    ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId		
    INNER JOIN temp_ReportID T21 ON T21.ReportId  = T13.CountyId AND T21.ReportType = 'COUNTY' and T21.ReportGuId = @Guid
    INNER JOIN BeneficiaryAccount T14 ON   T2.Id=T14.HhEnrolmentId AND T14.OpenedOn BETWEEN @StartDate AND @EndDate
    INNER JOIN SystemCodeDetail T14B ON T14.StatusId=T14B.Id                 		
    INNER JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id 
    INNER JOIN PSP T16 ON T15.PSPId = T16.Id 
    INNER JOIN BeneficiaryPaymentCard T17 ON T14.Id = T17.BeneAccountId
    INNER JOIN SystemCodeDetail T18 ON T17.StatusId=T18.Id                 		            
    INNER JOIN temp_ReportID T23 ON T23.ReportId  = T16.ID AND T23.ReportType = 'PSP' and T23.ReportGuId = @Guid
    INNER JOIN temp_ReportID T24 ON T24.ReportId  = T17.StatusId AND T24.ReportType = 'CARD_STATUS' and T24.ReportGuId = @Guid
    LEFT JOIN temp_ReportID  T22 ON T22.ReportId  = T13.ConstituencyId AND T22.ReportType = 'CONST' and T22.ReportGuId = @Guid		 
    LEFT JOIN HouseholdMember T80 ON T2.HhId=T80.HhId AND T4.SecondaryRecipientId=T80.MemberRoleId
    LEFT JOIN Person T90 ON T80.PersonId=T90.Id
    LEFT JOIN SystemCodeDetail T100 ON T90.SexId=T100.Id
    INNER JOIN temp_ReportID T25 ON T25.ReportId  = T90.SexId AND T25.ReportType = 'SEC_SEX' and T25.ReportGuId = @Guid
    ORDER BY T1.Id
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;

        DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
    END
END

GO
/****** Object:  StoredProcedure [dbo].[RptGetPendingEnrolment]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[RptGetPendingEnrolment]
	@DateRange varchar(20),
	@StartDate datetime,
	@EndDate datetime,
	@ProgrammeXml xml,
	@PrimarySexXml xml,
	@SecondarySexXml xml,
	@StatusXml xml,
	@RegGroupXml xml,
	@CountyXml xml,
	@ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
	@Type varchar(100)= ''
AS 
BEGIN 
	IF(@Page=0) set @Page =1
	DECLARE @offset int = (@Page-1)*@PageSize
	DECLARE @NoOfRows int=0
	IF(@DateRange='CUSTOM')
	SELECT  @StartDate = @StartDate, @EndDate = @EndDate
	IF(@DateRange='ALL_TIME')
	SELECT  @StartDate = '2000-01-01', @EndDate = GETDATE()
	IF(@DateRange='LAST_YEAR')
	SELECT  @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
	IF(@DateRange='THIS_YEAR')
	SELECT  @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_QUARTER')
	SELECT  @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
	IF(@DateRange='THIS_QUARTER')
	SELECT  @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
	IF(@DateRange='LAST_MONTH')
	SELECT  @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
	IF(@DateRange='THIS_MONTH')
	SELECT  @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
	IF(@DateRange='LAST_WEEK')
	SELECT  @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
	IF(@DateRange='THIS_WEEK')
	SELECT  @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
	IF(@DateRange='LAST_30')
	SELECT  @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd') 
	IF(@DateRange='LAST_7')
	SELECT  @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='YESTERDAY')
	SELECT  @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
	IF(@DateRange='TODAY')
	SELECT  @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')
	


	DECLARE @tblTableId TABLE(
	   Id int,
	   IDType varchar(20)
	)

	INSERT INTO @tblTableId(Id,IDType)
	SELECT T1.Id,T1.IDType FROM ( SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS IDType FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS IDType FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM  (SELECT U.R.value('(Id)[1]','int') AS Id, 'STATUS' AS IDType FROM @StatusXml.nodes('Report/Record') AS U(R)) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'REGGROUP' AS IDType FROM @RegGroupXml.nodes('Report/Record') AS U(R) ) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM  (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS IDType FROM @CountyXml.nodes('Report/Record') AS U(R)) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS IDType FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1  
	UNION
 	SELECT T1.Id,T1.IDType FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS IDType FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1  
	 
	IF(@Type='SUMMARY')
	BEGIN
	
	select  @NoOfRows = count(T2.Id) FROM   Household T2 
								   INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id  AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
								   INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   INNER JOIN Person T9 ON T8.PersonId=T9.Id
								   INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,
								   T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
									

		--INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
		--INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
		--INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
		--INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
		--INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
		--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
		-- LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
		LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE T14.HhId IS NULL


		SELECT  
		 T13.Constituency
		 , T13.County
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T4.Name Programme
		,@NoOfRows NoOfRows 
		,COUNT(T2.Id) as Total
		,COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MalePriReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemalePriReci,  COUNT(T2.Id) AS TotalPriReci
		,COUNT(CASE WHEN T7.Code = 'M' THEN 1 END ) AS MaleSecReci, COUNT(CASE WHEN T7.Code = 'F' THEN 1 END ) AS FemaleSecReci,  COUNT(T2.Id) AS TotalSecReci

	FROM   Household T2 
								   INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
								   INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   INNER JOIN Person T9 ON T8.PersonId=T9.Id
								   INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,
								   T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
									
	--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
	--	INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
	----	INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
	--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
	 INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
	--	LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
	--	 LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
		 	LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id
		WHERE  T14.HhId IS NULL
		GROUP BY  T13.Constituency
		 , T13.County
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T4.Name 
		ORDER by  T13.Constituency
		 , T13.County
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T4.Name 
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;  

 END

 ELSE  IF(@Type='DETAILS')
	BEGIN
	
	select  @NoOfRows = count(T2.Id) FROM   Household T2 
								   INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id --AND T2.CreatedOn BETWEEN @StartDate AND @EndDate 
								   INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   INNER JOIN Person T9 ON T8.PersonId=T9.Id
								   INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,
								   T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
									
			--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
						--			INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
							--		INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
								--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
				 		  INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
									--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
									--LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
									LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id 
		WHERE  T14.HhId IS NULL


		SELECT t2.Id,  T6.FirstName AS BeneFirstName, t2.CreatedOn, T4.Name Programme
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,@NoOfRows NoOfRows 
	FROM   Household T2 
								   INNER JOIN Programme T4 ON T2.ProgrammeId=T4.Id --AND T2.CreatedOn BETWEEN @StartDate AND @EndDate
								   INNER JOIN HouseholdMember T5 ON T2.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   INNER JOIN Person T9 ON T8.PersonId=T9.Id
								   INNER JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,
								   T6.Id AS CountyId,T7.Name AS Constituency,T7.Id AS ConstituencyId
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
								
								--	INNER JOIN  @tblTableId T18 ON T18.ID  = T2.ProgrammeId AND T18.IDType = 'PROGRAMME'
								--	INNER JOIN @tblTableId T20 ON T20.Id  = T6.SexId	AND T20.IDType = 'PRI_SEX'	
							--		INNER JOIN @tblTableId T19 ON T19.ID  = T2.StatusId AND T19.IDType = 'STATUS'	
								--	INNER JOIN @tblTableId T19b ON T19b.ID  = T2.RegGroupId AND T19b.IDType = 'REGGROUP'	
									--  INNER JOIN @tblTableId T21 ON T21.ID  = T13.CountyId AND T21.IDType = 'COUNTY'	
									--LEFT JOIN @tblTableId  T22 ON T22.ID  = T13.ConstituencyId AND T22.IDType = 'CONST'	 
									--LEFT JOIN @tblTableId T23 ON T23.Id  = T9.SexId AND T23.IDType = 'SEC_SEX'	
									LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T2.Id WHERE    T14.Id IS NULL
		
		ORDER by  T13.CountyId, T13.ConstituencyId,T13.SubLocationId
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY;  
 END

 END

GO
/****** Object:  StoredProcedure [dbo].[RptGetPrepayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [dbo].[RptGetPrepayroll]
 
	@DateRange VARCHAR(20),
    @StartDate datetime,
    @EndDate datetime,
    @ProgrammeXml xml,
    @PrimarySexXml xml,
    @SecondarySexXml xml,
    @ExceptionTypeId int,
    @CountyXml xml,
    @ConstituencyXml xml,
    @Page int =1,
    @PageSize int=10,
    @Type VARCHAR(100)= '',
	@DobDateRangeId VARCHAR(20),
	@DobStartDate datetime,
	@DobEndDate datetime
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @Exception_INVALIDBENEID varchar(20)
	DECLARE @Exception_INVALIDCGID varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDIN varchar(20)
	DECLARE @Exception_DUPLICATEBENEIDACC varchar(20)
	DECLARE @Exception_DUPLICATECGIDIN varchar(20)
	DECLARE @Exception_DUPLICATECGIDACC varchar(20)
	DECLARE @Exception_INVALIDACC varchar(20)
	DECLARE @Exception_INVALIDCARD varchar(20)
	DECLARE @Exception_SUSPICIOUSAMT varchar(20)
	DECLARE @Exception_SUSPICIOUSDORMANCY varchar(20)
	DECLARE @Exception_INELIGIBLEBENE varchar(20)
	DECLARE @Exception_INELIGIBLECG varchar(20)
	DECLARE @Exception_INELIGIBLESUS varchar(20)

	SET @SysCode='Exception Type'
	SET @Exception_INVALIDBENEID='INVALIDBENEID'
	SET @Exception_INVALIDCGID='INVALIDCGID'
	SET @Exception_DUPLICATEBENEIDIN='DUPLICATEBENEIDIN'
	SET @Exception_DUPLICATEBENEIDACC='DUPLICATEBENEIDACC'
	SET @Exception_DUPLICATECGIDIN='DUPLICATECGIDIN'
	SET @Exception_DUPLICATECGIDACC='DUPLICATECGIDACC'
	SET @Exception_INVALIDACC='INVALIDACC'
	SET @Exception_INVALIDCARD='INVALIDCARD'
	SET @Exception_SUSPICIOUSAMT='SUSPICIOUSAMT'
	SET @Exception_SUSPICIOUSDORMANCY='SUSPICIOUSDORMANCY'
	SET @Exception_INELIGIBLEBENE='INELIGIBLEBENE'
	SET @Exception_INELIGIBLECG='INELIGIBLECG'
	SET @Exception_INELIGIBLESUS='INELIGIBLESUS' 


	
	 IF(@Page=0) set @Page =1
    DECLARE  @offset int = (@Page-1)*@PageSize
    DECLARE  @NoOfRows int=0

    IF(@DateRange='CUSTOM')
	SELECT @StartDate = @StartDate, @EndDate = @EndDate
    IF(@DateRange='ALL_TIME')
	SELECT @StartDate = '2000-01-01', @EndDate = GETDATE()
    IF(@DateRange='LAST_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 1, 0), @EndDate = DATEADD(dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0))
    IF(@DateRange='THIS_YEAR')
	SELECT @StartDate = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) , @EndDate = DATEADD (dd, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_QUARTER')
	SELECT @StartDate = DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) - 1, 0) , @EndDate = DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) , 0))
    IF(@DateRange='THIS_QUARTER')
	SELECT @StartDate =  DATEADD(qq, DATEDIFF(qq, 0, GETDATE()), 0) , @EndDate =  DATEADD (dd, -1, DATEADD(qq, DATEDIFF(qq, 0, GETDATE()) +1, 0))
    IF(@DateRange='LAST_MONTH')
	SELECT @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) , @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1)
    IF(@DateRange='THIS_MONTH')
	SELECT @StartDate = DATEADD(month, DATEDIFF(MONTH, 0, GETDATE()), 0) , @EndDate = GETDATE()
    IF(@DateRange='LAST_WEEK')
	SELECT @StartDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0), @EndDate =  DATEADD(wk,DATEDIFF(wk,7,GETDATE()),4)
    IF(@DateRange='THIS_WEEK')
	SELECT @StartDate = dateadd(day, 1-datepart(dw, getdate()), getdate()) , @EndDate = GETDATE()
    IF(@DateRange='LAST_30')
	SELECT @StartDate = FORMAT( DATEADD(day ,-30, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='LAST_7')
	SELECT @StartDate = FORMAT( DATEADD(day ,-7, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='YESTERDAY')
	SELECT @StartDate = FORMAT( DATEADD(day ,-1, GETDATE()), 'yyyy-MM-dd') , @EndDate = FORMAT( GETDATE(), 'yyyy-MM-dd')
    IF(@DateRange='TODAY')
	SELECT @StartDate = FORMAT( GETDATE(), 'yyyy-MM-dd') , @EndDate = FORMAT( DATEADD(day ,1, GETDATE()), 'yyyy-MM-dd')

	DECLARE @SQL varchar(8000)	
	DECLARE @SQLWhere varchar(8000)
    DECLARE  @Guid VARCHAR(36)
    select @Guid = NEWID()

    INSERT INTO temp_ReportID
        (ReportId,ReportType, ReportGuId)

    SELECT T0.ReportId, T0.ReportType, @Guid as ReportGuId
    FROM (
			 SELECT T1.Id AS ReportId, T1.ReportType
            FROM ( 
		SELECT U.R.value('(Id)[1]','int') AS Id, 'PROGRAMME' AS ReportType
                FROM @ProgrammeXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM ( SELECT U.R.value('(Id)[1]','int') AS Id , 'PRI_SEX' AS ReportType
                FROM @PrimarySexXml.nodes('Report/Record') AS U(R)) T1
               
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'COUNTY' AS ReportType
                FROM @CountyXml.nodes('Report/Record') AS U(R)) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'CONST' AS ReportType
                FROM @ConstituencyXml.nodes('Report/Record') AS U(R) ) T1
        UNION
            SELECT T1.Id, T1.ReportType
            FROM (SELECT U.R.value('(Id)[1]','int') AS Id, 'SEC_SEX' AS ReportType
                FROM @SecondarySexXml.nodes('Report/Record') AS U(R) ) T1
        
			) T0

	 IF(@TYPE='DETAILS')
	 BEGIN
 ;WITH X AS (
	 SELECT T1.PaymentCycleId,T2.[Description] AS PaymentCycle,T1.ProgrammeId,T3.Name AS Programme
	 ,TT4.Id AS EnrolmentNo
	 ,TT4.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),TT4.ProgrammeNo)))+CONVERT(varchar(6),TT4.ProgrammeNo) AS ProgrammeNo
		,T1.BeneFirstName,T1.BeneMiddleName,T1.BeneSurname,T1.BeneDoB,T4.Code AS BeneSex
		,T1.BeneNationalIDNo
		,T1.PriReciCanReceivePayment
		,CASE WHEN(ISNULL(T7.ActionedApvBy,0))>0 THEN -1 ELSE CONVERT(bit,ISNULL(T7.PersonId,0)) END AS IsInvalidBene
		,CASE WHEN(ISNULL(T8.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T8.PersonId,0)) END AS IsDuplicateBene
		,CASE WHEN(ISNULL(T9.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T9.HhId,0)) END AS IsIneligibleBene
		,T1.CGFirstName
		,T1.CGMiddleName
		,T1.CGSurname
		,T1.CGDoB
		,T5.Code AS CGSex
		,T1.CGNationalIDNo
		,CASE WHEN(ISNULL(T11.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T11.PersonId,0)) END AS IsInvalidCG
		,CASE WHEN(ISNULL(T12.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T12.PersonId,0)) END AS IsDuplicateCG
		,CASE WHEN(ISNULL(T13.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T13.HhId,0)) END AS IsIneligibleCG
		,T6.[Description] AS HhStatus
		,CASE WHEN(ISNULL(T19.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T19.HhId,0)) END AS IsIneligibleHh
		,TT15.AccountNo AS AccountNumber,CASE WHEN(ISNULL(T15.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T15.HhId,0)) END AS IsInvalidAccount
		,CASE WHEN(ISNULL(T17.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T17.HhId,0)) END AS IsDormantAccount
		,TT16.PaymentCardNo AS PaymentCardNumber,CASE WHEN(ISNULL(T16.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T16.HhId,0)) END AS IsInvalidPaymentCard,
		T21.Name AS PaymentZone
		,T1.PaymentZoneCommAmt
		,T1.ConseAccInactivity
		,T1.EntitlementAmount
		,T1.AdjustmentAmount
		,CASE WHEN(ISNULL(T18.ActionedApvBy,0)>0) THEN -1 ELSE CONVERT(bit,ISNULL(T18.HhId,0)) END AS IsSuspiciousAmount
		,T25.SubLocation,T25.Location,T25.Division,T25.District,T25.County,T25.Constituency
		--, COUNT(T1.PaymentCycleId) OVER () as  NoOfRows
	FROM Prepayroll T1 INNER JOIN PaymentCycle T2 ON T1.PaymentCycleId=T2.Id and T2.CreatedOn BETWEEN @StartDate AND @EndDate
					   INNER JOIN Programme T3 ON T1.ProgrammeId=T3.Id
					    INNER JOIN temp_ReportID T3A ON T3A.ReportId  = T3.ID AND T3A.ReportType = 'PROGRAMME' and T3A.ReportGuId = @Guid
  
					   INNER JOIN HouseholdEnrolment TT4 ON T1.HhId=TT4.HhId
					   INNER JOIN SystemCodeDetail T4 ON T1.BeneSexId=T4.Id
					   INNER JOIN temp_ReportID T4A ON T4A.ReportId  = T4.ID AND T4A.ReportType = 'PRI_SEX' and T4A.ReportGuId = @Guid  
					   LEFT JOIN SystemCodeDetail T5 ON T1.CGSexId=T5.Id
					   LEFT JOIN temp_ReportID T5A ON T5A.ReportId  = T5.ID AND T5A.ReportType = 'SEC_SEX' and T5A.ReportGuId = @Guid  
					   INNER JOIN SystemCodeDetail T6 ON T1.HhStatusId=T6.Id
					   LEFT JOIN PrepayrollInvalidID T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.BenePersonId=T7.PersonId
					   LEFT JOIN PrepayrollDuplicateID T8 ON T1.PaymentCycleId=T8.PaymentCycleId AND T1.ProgrammeId=T8.ProgrammeId AND T1.BenePersonId=T8.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,T1.ActionedApvBy, T1.ExceptionTypeId 
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLEBENE
								  )  T9 ON T1.PaymentCycleId=T9.PaymentCycleId AND T1.ProgrammeId=T9.ProgrammeId AND T1.HhId=T9.HhId
					   LEFT JOIN PrepayrollInvalidID T11 ON T1.PaymentCycleId=T11.PaymentCycleId AND T1.ProgrammeId=T11.ProgrammeId AND T1.CGPersonId=T11.PersonId
					   LEFT JOIN PrepayrollDuplicateID T12 ON T1.PaymentCycleId=T12.PaymentCycleId AND T1.ProgrammeId=T12.ProgrammeId AND T1.CGPersonId=T12.PersonId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy, T1.ExceptionTypeId  
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLECG
								  )  T13 ON T1.PaymentCycleId=T13.PaymentCycleId AND T1.ProgrammeId=T13.ProgrammeId AND T1.HhId=T13.HhId					   			   
					   LEFT JOIN BeneficiaryAccount TT15 ON T1.BeneAccountId=TT15.Id
					   LEFT JOIN PrepayrollInvalidPaymentAccount T15 ON T1.PaymentCycleId=T15.PaymentCycleId AND T1.ProgrammeId=T15.ProgrammeId AND T1.HhId=T15.HhId
					   LEFT JOIN BeneficiaryPaymentCard TT16 ON T1.BenePaymentCardId=TT16.Id
					   LEFT JOIN PrepayrollInvalidPaymentCard T16 ON T1.PaymentCycleId=T16.PaymentCycleId AND T1.ProgrammeId=T16.ProgrammeId AND T1.HhId=T16.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy, t1.ExceptionTypeId  
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSDORMANCY
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T17 ON T1.PaymentCycleId=T17.PaymentCycleId AND T1.ProgrammeId=T17.ProgrammeId AND T1.HhId=T17.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy,t1.ExceptionTypeId  
								  FROM PrepayrollSuspicious T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_SUSPICIOUSAMT
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T18 ON T1.PaymentCycleId=T18.PaymentCycleId AND T1.ProgrammeId=T18.ProgrammeId AND T1.HhId=T18.HhId
					   LEFT JOIN (SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,ActionedApvBy,t1.ExceptionTypeId  
								  FROM PrepayrollIneligible T1 INNER JOIN SystemCodeDetail T2 ON T1.ExceptionTypeId=T2.Id AND T2.Code=@Exception_INELIGIBLESUS
								  WHERE ISNULL(T1.ActionedApvBy,0)<=0
								  )  T19 ON T1.PaymentCycleId=T19.PaymentCycleId AND T1.ProgrammeId=T19.ProgrammeId AND T1.HhId=T19.HhId					   			   
					   LEFT JOIN PaymentZone T21 ON T1.PaymentZoneId=T21.Id
					   INNER JOIN Household T22 ON T1.HhId=T22.Id
					   INNER JOIN HouseholdSubLocation T23 ON T22.Id=T23.HhId
					   INNER JOIN GeoMaster T24 ON T23.GeoMasterId=T24.Id AND T24.IsDefault=1
					   INNER JOIN (
									SELECT T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T7.Name AS Division,T9.Name AS District,T10.Name AS County,T11.Name AS Constituency, T10.Id CountyId
									FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
														INNER JOIN Division T7 ON T2.DivisionId=T7.Id
														INNER JOIN CountyDistrict T8 ON T7.CountyDistrictId=T8.Id
														INNER JOIN District T9 ON T8.DistrictId=T9.Id
														INNER JOIN County T10 ON T8.CountyId=T10.Id
														INNER JOIN Constituency T11 ON T1.ConstituencyId=T11.Id
								) T25 ON T23.SubLocationId=T25.SubLocationId
					  INNER JOIN temp_ReportID T26 ON T26.ReportId  = T25.CountyId AND T26.ReportType = 'COUNTY' and T26.ReportGuId = @Guid					    
						LEFT JOIN BeneficiaryAccount T27 ON   T1.BeneAccountId=T27.Id    
						LEFT JOIN SystemCodeDetail T28 ON T27.StatusId=T28.Id                 		
						LEFT JOIN PSPBranch T29 ON T27.PSPBranchId = T29.Id 
						LEFT JOIN PSP T30 ON T29.PSPId = T30.Id  	
						where T7.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T7.ExceptionTypeId ELSE 	T7.ExceptionTypeId 		END	 	 
						OR  T8.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T8.ExceptionTypeId ELSE 	T8.ExceptionTypeId 		END	 	 
						OR  T9.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T9.ExceptionTypeId ELSE 	T9.ExceptionTypeId 		END	 	 
						
						OR  T11.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T11.ExceptionTypeId ELSE 	T11.ExceptionTypeId 		END	 	 
						OR  T12.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T12.ExceptionTypeId ELSE 	T12.ExceptionTypeId 		END	 	 
						
						OR  T13.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T13.ExceptionTypeId ELSE 	T13.ExceptionTypeId 		END	 	 
						OR  T17.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T17.ExceptionTypeId ELSE 	T17.ExceptionTypeId 		END	 	 
						
						OR  T18.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T18.ExceptionTypeId ELSE 	T18.ExceptionTypeId 		END	 	 
						OR  T19.ExceptionTypeId = CASE WHEN @ExceptionTypeId IS NULL THEN  T19.ExceptionTypeId ELSE 	T19.ExceptionTypeId 		END	 	 

						ORDER BY T1.PaymentCycleId
		OFFSET     @offset ROWS       
		FETCH NEXT @PageSize ROWS ONLY
						) 

 
						SELECT * FROM X 

						 DELETE FROM temp_ReportID WHERE ReportGuId = @Guid
						 
 END
	
End

GO
/****** Object:  StoredProcedure [dbo].[SkipPayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SkipPayroll]
    @PaymentCycleId int
   ,
    @UserId int
AS
BEGIN

    DECLARE @SysCode varchar(30)
    DECLARE @SysDetailCode varchar(30)
    DECLARE @SystemCodeDetailId1 int
    DECLARE @SystemCodeDetailId2 int
    DECLARE @ErrorMsg varchar(128)

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='PREPAYROLLDRAFT'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    IF NOT EXISTS(SELECT 1
    FROM PaymentCycleDetail
    WHERE PaymentCycleId=@PaymentCycleId)
		SET @ErrorMsg='Please specify valid PaymentCycleId parameter'
	ELSE IF EXISTS(SELECT 1
    FROM PaymentCycleDetail T1
    WHERE T1.PaymentCycleId=@PaymentCycleId AND PaymentStageId<>@SystemCodeDetailId1)
		SET @ErrorMsg='The Payment Cycle appears not to be in Approval Stage'
	ELSE IF NOT EXISTS(SELECT 1
    FROM [User]
    WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

    IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
        RAISERROR(@ErrorMsg,16,1)
        RETURN
    END

    SET @SysCode='Payment Stage'
    SET @SysDetailCode='CLOSED'
    SELECT @SystemCodeDetailId1=T1.Id
    FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    UPDATE T1
	SET T1.Skippedby=@UserId
	   ,T1.SkippedOn=GETDATE()
	   ,T1.PostPayrollApvby=@UserId
	   ,T1.PostPayrollApvOn=GETDATE()
	   ,T1.PaymentStageId=@SystemCodeDetailId1
	FROM PaymentCycleDetail T1
	WHERE T1.PaymentCycleId=@PaymentCycleId

   

   SET @SysCode='Payment Status'
	SET @SysDetailCode='PAYMENTRECON'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1
	SET T1.StatusId=@SystemCodeDetailId1
	FROM PaymentCycle T1
	WHERE T1.Id=@PaymentCycleId

	SELECT 1 AS NoOfRows 
END

GO
/****** Object:  StoredProcedure [dbo].[UTILITY_SP_PWDGEN]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UTILITY_SP_PWDGEN]
    @len int = 8,
    --@Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz.,#$%^&*-+_/=',
    @Charset nvarchar(256) = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
    @output nvarchar(64) OUTPUT
AS
    SET NOCOUNT ON;
    SET @output = '';
    SELECT  @output += SUBSTRING(@Charset, FLOOR(ABS(CAST(CRYPT_GEN_RANDOM(8) AS BIGINT) / 9223372036854775808.5) * LEN(@Charset)) + 1, 1)
    FROM master.dbo.spt_values
    WHERE type = 'P' AND number < @len;

GO
/****** Object:  StoredProcedure [dbo].[VerifyPayroll]    Script Date: 16/04/2019 2:36:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[VerifyPayroll]
	@PaymentCycleId int
   ,@ProgrammeId int
   ,@UserId int
AS
BEGIN
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @PayrollMatches bit
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SET @SysCode='Payment Stage'
	SET @SysDetailCode='PAYROLLVER'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	IF NOT EXISTS(SELECT 1 FROM PaymentCycleDetail WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId AND PaymentStageId=@SystemCodeDetailId1)
		SET @ErrorMsg='The specified payment cycle is not in the payroll verification stage'
	ELSE IF NOT EXISTS(SELECT 1 FROM [User] WHERE Id=@UserId)
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	DELETE FROM temp_Payroll WHERE PaymentCycleId=@PaymentCycleId AND ProgrammeId=@ProgrammeId;

	INSERT INTO temp_Payroll(PaymentCycleId,ProgrammeId,HhId,PaymentAmount)
	SELECT T1.PaymentCycleId,T1.ProgrammeId,T1.HhId,(T1.EntitlementAmount+T1.AdjustmentAmount) AS PaymentAmount
	FROM Prepayroll T1 LEFT JOIN (
									SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId
									FROM (
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedApvBy IS NULL
											UNION
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedApvBy IS NULL
											UNION
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedApvBy IS NULL
											UNION
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedApvBy IS NULL
											UNION
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedApvBy IS NULL
											UNION
											SELECT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedApvBy IS NULL
										) T1
								) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
	WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId AND T6.PaymentCycleId IS NULL and (T1.EntitlementAmount+T1.AdjustmentAmount)<=16000

	BEGIN TRAN

	SET @PayrollMatches=0
	IF NOT EXISTS(
					SELECT 1
					FROM temp_Payroll T1 LEFT JOIN Payroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId AND T1.PaymentAmount=T2.PaymentAmount
					WHERE T2.PaymentCycleId IS NULL AND T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
					UNION
					SELECT 1
					FROM Payroll T1 LEFT JOIN temp_Payroll T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId AND T1.PaymentAmount=T2.PaymentAmount
					WHERE T2.PaymentCycleId IS NULL AND T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId
				)
	BEGIN	
		SET @SysCode='Payment Stage'
		SET @SysDetailCode='PAYROLLAPV'
		SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		UPDATE T1
		SET T1.PayrollVerBy=@UserId
		   ,T1.PayrollVerOn=GETDATE()
		   ,T1.PaymentStageId=@SystemCodeDetailId1
		FROM PaymentCycleDetail T1
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId

		SET @PayrollMatches=1
	END

	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT -1 AS StatusId,@PayrollMatches AS Verified

		SELECT NULL AS TotalPayrollHhs
			,NULL AS PayrollExceptionsHhs
			,NULL AS PayrollInvalidIDHhs
			,NULL AS PayrollDuplicateIDHhs
			,NULL AS PayrollInvalidPaymentAccountHhs
			,NULL AS PayrollInvalidPaymentCardHhs
			,NULL AS PayrollIneligibleHhs
			,NULL AS PayrollSuspiciousHhs
			,NULL AS TotalPayrollAmount

		SELECT NULL AS TotalPayrollHhs
			,NULL AS PayrollExceptionsHhs
			,NULL AS PayrollInvalidIDHhs
			,NULL AS PayrollDuplicateIDHhs
			,NULL AS PayrollInvalidPaymentAccountHhs
			,NULL AS PayrollInvalidPaymentCardHhs
			,NULL AS PayrollIneligibleHhs
			,NULL AS PayrollSuspiciousHhs
			,NULL AS TotalPayrollAmount
	END
	ELSE
	BEGIN
		COMMIT TRAN;
		SELECT 0 AS StatusId,@PayrollMatches AS Verified

		SELECT COUNT(T1.HhId) AS TotalPayrollHhs
			,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollExceptionsHhs
			,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidIDHhs
			,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PayrollDuplicateIDHhs
			,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentAccountHhs
			,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentCardHhs
			,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PayrollIneligibleHhs
			,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollSuspiciousHhs
			,SUM(T1.PaymentAmount) AS TotalPayrollAmount
		FROM temp_Payroll T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
						     LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
							 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
							 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
							 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
							 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId  and T1.PaymentAmount<=16000

		SELECT COUNT(T1.HhId) AS TotalPayrollHhs
			,SUM(CASE WHEN(T2.HhId>1 OR T3.HhId>1 OR T4.HhId>1 OR T5.HhId>1 OR T6.HhId>1 OR T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollExceptionsHhs
			,SUM(CASE WHEN(T2.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidIDHhs
			,SUM(CASE WHEN(T3.HhId>1) THEN 1 ELSE 0 END) AS PayrollDuplicateIDHhs
			,SUM(CASE WHEN(T4.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentAccountHhs
			,SUM(CASE WHEN(T5.HhId>1) THEN 1 ELSE 0 END) AS PayrollInvalidPaymentCardHhs
			,SUM(CASE WHEN(T6.HhId>1) THEN 1 ELSE 0 END) AS PayrollIneligibleHhs
			,SUM(CASE WHEN(T7.HhId>1) THEN 1 ELSE 0 END) AS PayrollSuspiciousHhs
			,SUM(T1.PaymentAmount) AS TotalPayrollAmount
		FROM Payroll T1 LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidID WHERE ActionedBy>0) T2 ON T1.PaymentCycleId=T2.PaymentCycleId AND T1.ProgrammeId=T2.ProgrammeId AND T1.HhId=T2.HhId
						LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollDuplicateID WHERE ActionedBy>0) T3 ON T1.PaymentCycleId=T3.PaymentCycleId AND T1.ProgrammeId=T3.ProgrammeId AND T1.HhId=T3.HhId
						LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentAccount WHERE ActionedBy>0) T4 ON T1.PaymentCycleId=T4.PaymentCycleId AND T1.ProgrammeId=T4.ProgrammeId AND T1.HhId=T4.HhId
						LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollInvalidPaymentCard WHERE ActionedBy>0) T5 ON T1.PaymentCycleId=T5.PaymentCycleId AND T1.ProgrammeId=T5.ProgrammeId AND T1.HhId=T5.HhId
						LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollIneligible WHERE ActionedBy>0) T6 ON T1.PaymentCycleId=T6.PaymentCycleId AND T1.ProgrammeId=T6.ProgrammeId AND T1.HhId=T6.HhId
						LEFT JOIN (SELECT DISTINCT PaymentCycleId,ProgrammeId,HhId FROM PrepayrollSuspicious WHERE ActionedBy>0) T7 ON T1.PaymentCycleId=T7.PaymentCycleId AND T1.ProgrammeId=T7.ProgrammeId AND T1.HhId=T7.HhId
		WHERE T1.PaymentCycleId=@PaymentCycleId AND T1.ProgrammeId=@ProgrammeId and T1.PaymentAmount<=16000
	END
END

GO
