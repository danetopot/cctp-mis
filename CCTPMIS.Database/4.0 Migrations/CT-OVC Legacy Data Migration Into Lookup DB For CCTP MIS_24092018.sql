


USE [LegacyDB]
GO

DECLARE @ProgrammeId int
DECLARE @Programme varchar(100)
DECLARE @MIG_STAGE tinyint

SELECT @ProgrammeId=T1.Id,@Programme=T1.Name FROM [UAT_CCTPMIS].dbo.Programme T1 WHERE T1.Code='CT-OVC'
SET @MIG_STAGE=0

-->>1. HOUSEHOLD REGISRATION
IF @MIG_STAGE=1
BEGIN
	DECLARE @HousePartCode_WALL int
	DECLARE @HousePartCode_FLOOR int
	DECLARE @HousePartCode_ROOF int
	DECLARE @ToiletCode int				   
	DECLARE @WaterSourceCode int				   
	DECLARE @LightingSourceCode int				   
	DECLARE @FuelSourceCode int	
	DECLARE @BenefitTypeCode int			

	SET @HousePartCode_WALL=74
	SET @HousePartCode_FLOOR=76
	SET @HousePartCode_ROOF=75
	SET @ToiletCode=73
	SET @WaterSourceCode=70
	SET @LightingSourceCode=71
	SET @FuelSourceCode=72
	SET @BenefitTypeCode=5

	INSERT INTO HhRegistration(HhId,ProgrammeId,Programme,ProgrammeNumber,County,Constituency,District,Division,Location,SubLocation,Village,NearestLandmark,HasRealEstate,IsLivingInOwnHouse
		,RentAmount,ZebuCattle,HybridCattle,Sheep,Goats,Pigs,Camels,Poultry,NoMealPerWeek,MealsPerDay,ReasonForNoMeal,NoMealPerMonth,WallMaterial,FloorMaterial,RoofMaterial,ToiletType,WaterSource
		,LightingSource,FuelSource,LandSize,OtherProg,OtherProgBenefitType,OtherProgBenefitFrequency,OtherProgBenefitAmount,OtherProgOtherBenefits,EnrolmentStatus,EnrolmentDate,EnrolmentReason
		,ApprovalDate,PSPBranch,PSP,Inactive,CapturedBy,ApprovedBy,EnrolledBy,PovertyScore)

	SELECT T1.TargetingId,@ProgrammeId,@Programme,T1.ProgrammeNumber,T2.CountyName,T2.ConstituencyName,T2.DistrictName,T2.DivisionName,T2.LocationName,T2.SubLocationName,T3.VillageName
		,ISNULL(T1.NearestSchool,T1.NearestWorshipPlace),Case(ISNULL(T1.RealEstate,2)) WHEN 1 THEN 'Yes' ELSE 'No' END AS RealEstate,NULL AS IsLivingInOwnHouse,NULL AS RentAmount
		,T1.ZebuCattle,T1.HybridCattle,T1.Sheep,T1.Goats,T1.Pigs,T1.Camels,NULL AS Poultry,NULL AS NoMealPerWeek,NULL AS MealsPerDay,NULL AS ReasonForNoMeal,NULL AS NoMealPerMonth
		,LTRIM(RIGHT(T4.ParameterName,LEN(T4.ParameterName)-CHARINDEX('.',T4.ParameterName,0))) AS Wall
		,LTRIM(RIGHT(T5.ParameterName,LEN(T5.ParameterName)-CHARINDEX('.',T5.ParameterName,0))) AS [Floor]
		,LTRIM(RIGHT(T6.ParameterName,LEN(T6.ParameterName)-CHARINDEX('.',T6.ParameterName,0))) AS Roof
		,LTRIM(RIGHT(T7.ParameterName,LEN(T7.ParameterName)-CHARINDEX('.',T7.ParameterName,0))) AS Toilet
		,LTRIM(RIGHT(T8.ParameterName,LEN(T8.ParameterName)-CHARINDEX('.',T8.ParameterName,0))) AS WaterSource
		,LTRIM(RIGHT(T9.ParameterName,LEN(T9.ParameterName)-CHARINDEX('.',T9.ParameterName,0))) AS Lighting
		,LTRIM(RIGHT(T10.ParameterName,LEN(T10.ParameterName)-CHARINDEX('.',T10.ParameterName,0))) AS FuelSource
		,T1.LandSize,T12.ProgrammeName AS OtherProg,CASE WHEN(ISNULL(T1.OtherProgrammes,0)=0 AND ISNULL(T1.OtherProgrammeId,0)<=0) THEN NULL ELSE T11.ParameterName END AS BenefitType,NULL AS Frequency,CASE WHEN(ISNULL(T1.OtherProgrammes,0)=0 AND ISNULL(T1.OtherProgrammeId,0)<=0) THEN NULL ELSE T1.BenefitAmount END AS BenefitAmount,NULL AS OtherBenefits,T16.StatusName AS EnrolmentStatus,T1.EnrollmentDate,NULL AS EnrollmentReason
		,T1.DateSelected AS ApprovalDate,T13.AgencyBranchName,T14.AgencyName,NULL AS InActive,ISNULL(T15.FirstName,'')+ISNULL(' '+T15.MiddleName,'')+ISNULL(' '+T15.Surname,'') AS CreatedBy,T1.EnrolledBy AS ApprovedBy,T1.SelectedBy AS EnrolledBy,T1.PMTScore AS PovertyScore
	FROM [DCS].dbo.TargetingHeader T1 LEFT JOIN (	SELECT T1.SubLocationId,T1.SubLocationCode,T1.SubLocationName,T2.LocationCode,T2.LocationName,T3.DivisionCode,T3.DivisionName,T4.DistrictCode,T4.DistrictName,T5.CountyCode,T5.CountyName,T6.ConstituencyCode,T6.ConstituencyName
													FROM [DCS].dbo.SubLocation T1 INNER JOIN [DCS].dbo.Location T2 ON T1.LocationId=T2.LocationId	
																		INNER JOIN [DCS].dbo.Division T3 ON T2.DivisionId=T3.DivisionId
																		INNER JOIN [DCS].dbo.District T4 ON T3.DistrictId=T4.DistrictId
																		INNER JOIN [DCS].dbo.County T5 ON T4.CountyId=T5.CountyId
																		INNER JOIN [DCS].dbo.Constituency T6 ON T1.ConstituencyId=T6.ConstituencyId
													) T2 ON T1.SubLocationId=T2.SubLocationId
							LEFT JOIN [DCS].dbo.Village T3 ON T1.VillageId=T3.VillageId
							LEFT JOIN [DCS].dbo.Parameter T4 ON T1.Wall=T4.ParameterId AND T4.ParameterCode=@HousePartCode_WALL
							LEFT JOIN [DCS].dbo.Parameter T5 ON T1.[Floor]=T5.ParameterId AND T5.ParameterCode=@HousePartCode_FLOOR
							LEFT JOIN [DCS].dbo.Parameter T6 ON T1.[Roof]=T6.ParameterId AND T6.ParameterCode=@HousePartCode_ROOF
							LEFT JOIN [DCS].dbo.Parameter T7 ON T1.Toilet=T7.ParameterId AND T7.ParameterCode=@ToiletCode
							LEFT JOIN [DCS].dbo.Parameter T8 ON T1.WaterSource=T8.ParameterId AND T8.ParameterCode=@WaterSourceCode
							LEFT JOIN [DCS].dbo.Parameter T9 ON T1.LightingSource=T9.ParameterId AND T9.ParameterCode=@LightingSourceCode
							LEFT JOIN [DCS].dbo.Parameter T10 ON T1.FuelSource=T10.ParameterId AND T10.ParameterCode=@FuelSourceCode
							LEFT JOIN [DCS].dbo.Parameter T11 ON T1.BenefitType=T11.ParameterId AND T11.ParameterCode=@BenefitTypeCode
							LEFT JOIN [DCS].dbo.Programme T12 ON T1.OtherProgrammeId=T12.ProgrammeId
							LEFT JOIN [DCS].dbo.AgencyBranch T13 ON T1.AgencyBranchId=T13.AgencyBranchId
							LEFT JOIN [DCS].dbo.Agency T14 ON T13.AgencyId=T14.AgencyId
							LEFT JOIN [DCS].dbo.Employee T15 ON T1.Enumerator=T15.EmployeeId
							LEFT JOIN [DCS].dbo.HouseholdStatus T16 ON T1.[Status]=T16.StatusId
							LEFT JOIN HhRegistration T17 ON T1.TargetingId=T17.HhId AND T17.ProgrammeId=@ProgrammeId
WHERE T1.[Status] IN(18,19,20,21,33) AND T17.Id IS NULL
END



-->>2. HOUSEHOLD MEMBERSHIP
IF @MIG_STAGE=2
BEGIN
	DECLARE @OccupationCode int			

	SET @OccupationCode=15

	INSERT INTO HhMembers(MemberId,HhId,HhRegistrationId,FirstName,MiddleName,Surname,Sex,DateOfBirth,BirthCertNo,NationalIdNo,MarritalStatus,EducationLevel,AttendanceSchool,IllnessType,DisabilityType,NCPWDNo,AttendanceHeathCentre,Relationship,OccupationType,IsExited)
	SELECT T1.TargetingDetailId,T1.TargetingId,T3.Id,T1.FirstName,T1.MiddleName,T1.Surname,T1.Sex,T1.DateOfBirth,NULL AS BirthCertNo,T1.IdNumber,NULL AS MarritalStatus,LTRIM(RIGHT(T4.EducationName,LEN(T4.EducationName)-CHARINDEX('.',T4.EducationName,0))) AS EducationName,T5.CentreName,T6.IllnessName,NULL AS DisabilityType,NULL AS NCPWDNo,T7.CentreName,LTRIM(RIGHT(T8.RelationshipName,LEN(T8.RelationshipName)-CHARINDEX('.',T8.RelationshipName,0))) AS RelationshipName,LTRIM(RIGHT(T9.ParameterName,LEN(T9.ParameterName)-CHARINDEX(']',T9.ParameterName,0))) AS Occupation,ISNULL(T1.Exited,0)
	FROM [DCS].dbo.TargetingDetail T1 INNER JOIN [DCS].dbo.TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
									  INNER JOIN HhRegistration T3 ON T1.TargetingId=T3.HhId
									  LEFT JOIN [DCS].dbo.Education T4 ON T1.EducationId=T4.EducationId
									  LEFT JOIN [DCS].dbo.Centre T5 ON T1.SchoolId=T5.CentreId
									  LEFT JOIN [DCS].dbo.Illness T6 ON T1.IllnessId=T6.IllnessId
									  LEFT JOIN [DCS].dbo.Centre T7 ON T1.HealthCentreId=T7.CentreId
									  LEFT JOIN [DCS].dbo.Relationship T8 ON T1.RelationshipId=T8.RelationshipId
									  LEFT JOIN [DCS].dbo.Parameter T9 ON T1.OccupationId=T9.ParameterId AND T9.ParameterCode=@OccupationCode
									  LEFT JOIN HhMembers T10 ON T1.TargetingDetailId=T10.MemberId AND T1.TargetingId=T10.HhId
	WHERE T2.[Status] IN(18,19,20,21,33) AND T10.Id IS NULL
END



-->>3. BENEFICIARY PAYMENT
IF @MIG_STAGE=3
BEGIN
	INSERT INTO Payment(PaymentId,HhId,HhRegistrationId,CycleId,CycleName,CycleStartDate,CycleEndDate,PaymentDate,Approved,Notes,PSPBranch,PSP,PrimaryRecipientName,PrimaryRecipientNationalIdNo,SecondaryRecipientName,SecondaryRecipientNationalIdNo,Amount,PaidAmount,PaidTrxNo,GeneratedBy)
	SELECT T1.PaymentDetailId,T1.TargetingId,T4.Id,T2.CycleId,T5.CycleName,T5.StartDate,T5.EndDate,T1.DatePaid,'Yes' AS Approved,'' AS Notes,T6.AgencyBranchName,T7.AgencyName,T1.CaregiverName,T1.CaregiverIdNumber,T1.AlternateCaregiverName,T1.AlternateCaregiverIdNumber,T1.Amount,ISNULL(T1.AmountPaid,0),T1.TransactionNumber,CASE WHEN(T8.EmployeeId IS NULL) THEN T2.CreatedBy ELSE ISNULL(T8.FirstName,'')+ISNULL(' '+T8.MiddleName,'')+ISNULL(' '+T8.Surname,'') END AS GeneratedBy
	FROM [DCS].dbo.PaymentDetail T1 INNER JOIN [DCS].dbo.PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
									INNER JOIN [DCS].dbo.TargetingHeader T3 ON T1.TargetingId=T3.TargetingId
									INNER JOIN HhRegistration T4 ON T1.TargetingId=T4.HhId
									INNER JOIN [DCS].dbo.PaymentCycle T5 ON T2.CycleId=T5.CycleId
									INNER JOIN [DCS].dbo.AgencyBranch T6 ON T1.AgencyBranchId=T6.AgencyBranchId
									INNER JOIN [DCS].dbo.Agency T7 ON T6.AgencyId=T7.AgencyId
									LEFT JOIN [DCS].dbo.Employee T8 ON T2.CreatedBy=CONVERT(varchar(10),T8.EmployeeId)
									LEFT JOIN Payment T9 ON T1.PaymentDetailId=T9.PaymentId AND T1.TargetingId=T9.HhId
	WHERE T9.Id IS NULL
END



-->>4. COMPLAINTS
IF @MIG_STAGE=4
BEGIN
	INSERT INTO CaseGrievance(CaseId,SourceProgrammeId,SourceProgramme,SerialNo,ProgrammeId,Programme,GrievanceTypeId,GrievanceTypeName,GrievanceDescription,CaseHistory,Constituency
	,SubLocation,Village,Community,HhId,HhRegistrationId,IsBeneficiary,ComplainantType,ComplainantName,ComplainantSex,ComplainantAge,ComplainantTel,ReceiptOn,ReceivedBy,ReceiptLevel
	,CreatedOn,CreatedBy,VerifiedOn,VerifiedBy,ActionNotes,CaseStatus,FeedbackOn,Resolution,ResolvedOn,ResolvedBy,ClosedOn,ClosedBy
	)	
	
	SELECT T1.GrievanceId,@ProgrammeId AS SourceProgrammeId,@Programme AS SourceProgramme,T1.SerialNumber,T3.Id,T3.Name,T1.GrievanceTypeId,T4.GrievanceTypeName,T1.GrievanceDescription,T1.CaseHistory,T5.ConstituencyName
	,T6.SubLocationName,T1.Village,T1.CommunityName,T1.RegisterId,T8.Id,CASE WHEN(T1.Beneficiary=1) THEN 'Yes' ELSE 'No' END AS IsBeneficiary,CASE(T1.ComplainantType) WHEN 0 THEN 'Individual/Household' WHEN 1 THEN 'Community' WHEN 2 THEN ISNULL(T1.OtherComplainantType,'Other') ELSE NULL END AS ComplainantType,T1.ComplainantName,CONVERT(varchar(1),T1.Sex),ISNULL(CONVERT(varchar(10),T1.Age),CONVERT(varchar(10),T9.RangeStart)+' - '+CONVERT(varchar(10),T9.RangeEnd)) AS ComplainantAge,T1.TelephoneNumber,ISNULL(ISNULL(T1.ReceiptDate,T1.GrievanceDate),ISNULL(T1.FeedbackDate,T1.DateClosed)),T1.StaffName,CASE(T1.StaffLevel) WHEN 0 THEN 'National' WHEN 1 THEN 'County' WHEN 2 THEN 'Sub-County' ELSE NULL END AS ReceiptLevel
	,T1.GrievanceDate,T1.EnteredBy,T1.DateVerified,T1.VerifiedBy,T1.ActionTaken,CASE(T1.ResolutionStatus) WHEN 0 THEN 'Open' WHEN 1 THEN 'Verified' WHEN 2 THEN 'Resolved' WHEN 3 THEN 'Closed' ELSE NULL END AS CaseStatus,ISNULL(T1.FeedbackDate,T1.ExpectedResolutionDate) AS FeedbackDate,T1.ResolutionDescription,T1.DateResolved,T1.ResolvedBy,T1.DateClosed,T1.ClosedBy
	FROM [DCS].dbo.Grievance T1 LEFT JOIN [DCS].dbo.Programme T2 ON T1.ProgrammeId=T2.ProgrammeId
								LEFT JOIN [UAT_CCTPMIS].dbo.Programme T3 ON (CASE(T2.ProgrammeCode) WHEN 'OVC' THEN 'CT-OVC'
																							WHEN 'OPP' THEN 'OPCT'
																							WHEN 'PWSD' THEN 'PwSD-CT'
																							ELSE T2.ProgrammeCode
																	END) COLLATE DATABASE_DEFAULT=T3.Code COLLATE DATABASE_DEFAULT
								LEFT JOIN [DCS].dbo.GrievanceType T4 ON T1.GrievanceTypeId=T4.GrievanceTypeId
								LEFT JOIN [DCS].dbo.Constituency T5 ON T1.ConstituencyId=T5.ConstituencyId
								LEFT JOIN [DCS].dbo.SubLocation T6 ON T1.SubLocationId=T6.SubLocationId
								LEFT JOIN [DCS].dbo.TargetingHeader T7 ON T1.RegisterId=T7.TargetingId
								LEFT JOIN HhRegistration T8 ON T7.TargetingId=T8.HhId
								LEFT JOIN [DCS].dbo.AgeBracket T9 ON T1.AgeBracketId=T9.AgeBracketId
								LEFT JOIN CaseGrievance T10 ON T1.GrievanceId=T10.CaseId AND T10.SourceProgrammeId=@ProgrammeId
	WHERE T10.Id IS NULL
END



-->>5. UPDATES
IF @MIG_STAGE=5
BEGIN
	INSERT INTO CaseUpdate(CaseId,SourceProgrammeId,SourceProgramme,UpdateType,UpdateName,UpdateReason,UpdateDate,HhId,HhRegistrationId,OldCGMemberId,NewCGMemberId,OldCGHhMemberId,NewCGHhMemberId
		,FirstName,MiddleName,Surname,Sex,DateOfBirth,IDNumber,MarritalStatus,EducationLevel,AttendanceSchool,EducationChangeReason,IsChronicallyIll,IsDisabled,AttendanceHeathCentre,HealthChangeReason
		,Relationship,OldSubLocation,NewSubLocation,PSPBranch,PSP,ReplacePaymentCard,PSPChangeReason,AlternativeRecipientName,AlternativeRecipientIDNumber,CreatedOn,CreatedBy,ApprovedOn,ApprovedBy,ActionNotes
		)	
	
	SELECT T1.ChangeDetailId,@ProgrammeId AS SourceProgrammeId,@Programme AS SourceProgramme,T2.ChangeTypeId,T3.ChangeTypeName,T4.ReasonName,T2.DateCreated,T2.TargetingId,T5.Id,T1.OldCaregiver,T1.NewCaregiver,T6.Id,T7.Id
		,CONVERT(varchar(50),T1.FirstName),CONVERT(varchar(50),T1.MiddleName),CONVERT(varchar(50),T1.Surname),NULL AS Sex,T1.DateOfBirth,CONVERT(varchar(30),T1.IdNumber),NULL AS MarritalStatus,CONVERT(varchar(50),T8.EducationName),CONVERT(varchar(100),T1.SchoolName),CASE(T1.SchoolReason) WHEN 1 THEN 'New School' WHEN 2 THEN 'Change Of Class' WHEN 3 THEN 'Exempted' ELSE NULL END AS EducationChangeReason,CASE WHEN(T1.Illness=1) THEN 'Yes' ELSE 'No' END AS ChronicallyIll,CASE WHEN(T1.[Disabled]=1) THEN 'Yes' ELSE 'No' END AS IsDisabled,CONVERT(varchar(100),T1.HealthCentreName),CASE(T1.HealthReason) WHEN 1 THEN 'Change Of Health Facility' WHEN 2 THEN 'Exempted' ELSE NULL END AS HealthChangeReason
		,LTRIM(RIGHT(T9.RelationshipName,LEN(T9.RelationshipName)-CHARINDEX('.',T9.RelationshipName,0))) AS RelationshipName,T10.SubLocationName,T11.SubLocationName,T12.AgencyBranchName,T13.AgencyName,CASE WHEN(T1.ReplaceCard=1) THEN 'Yes' ELSE 'No' END AS ReplacePaymentCard,CASE(T1.PSPReason) WHEN 1 THEN 'Nearest Payment Facility' WHEN 2 THEN 'Quality Assurance and Security Issues' ELSE NULL END AS PSPChangeReason,CONVERT(varchar(100),T1.AlternateRepName),T1.AlternateRepIdNumber,T2.DateCreated,T2.CreatedBy,T2.ApprovalDate,T2.ApprovedBy,T2.Comments
	FROM [DCS].dbo.ChangeDetail T1 INNER JOIN [DCS].dbo.ChangeHeader T2 ON T1.ChangeId=T2.ChangeId
								   INNER JOIN [DCS].dbo.ChangeType T3 ON T2.ChangeTypeId=T3.ChangeTypeId
								   LEFT JOIN [DCS].dbo.Reason T4 ON T2.ReasonId=T4.ReasonId
								   LEFT JOIN HhRegistration T5 ON T2.TargetingId=T5.HhId
								   LEFT JOIN HhMembers T6 ON T1.OldCaregiver=T6.MemberId AND T6.HhId=T2.TargetingId
								   LEFT JOIN HhMembers T7 ON T1.NewCaregiver=T7.MemberId AND T7.HhId=T2.TargetingId
								   LEFT JOIN [DCS].dbo.Education T8 ON T1.EducationId=T8.EducationId
								   LEFT JOIN [DCS].dbo.Relationship T9 ON T1.RelationshipId=T9.RelationshipId
								   LEFT JOIN [DCS].dbo.SubLocation T10 ON T1.OldSubLocationId=T10.SubLocationId
								   LEFT JOIN [DCS].dbo.SubLocation T11 ON T1.NewSubLocationId=T11.SubLocationId
								   LEFT JOIN [DCS].dbo.AgencyBranch T12 ON T1.AgencyBranchId=T12.AgencyBranchId
								   LEFT JOIN [DCS].dbo.Agency T13 ON T12.AgencyId=T13.AgencyId
								   LEFT JOIN CaseUpdate T14 ON T1.ChangeDetailId=T14.CaseId AND T14.SourceProgrammeId=@ProgrammeId
	WHERE T14.Id IS NULL
END

GO



