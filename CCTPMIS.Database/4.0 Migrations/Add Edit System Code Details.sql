

IF NOT OBJECT_ID('AddEditSystemCode') IS NULL	
DROP PROC AddEditSystemCode
GO
CREATE PROC AddEditSystemCode
	@Id int=NULL
   ,@Code varchar(20)
   ,@Description varchar(128)
   ,@IsUserMaintained bit=NULL
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)

	SET @IsUserMaintained=ISNULL(@IsUserMaintained,0)

	IF ISNULL(@Code,'')=''
	BEGIN
		SET @ErrorMsg='Please specify valid Code parameter'
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF EXISTS(SELECT 1 FROM SystemCodes WHERE Code=@Code)
	BEGIN
		SET @ErrorMsg='Possible duplicate entry for the System Code ('+@Code+') '
		
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END
	
	IF @Id>0
	BEGIN
		UPDATE T1
		SET T1.Code=@Code
		   ,T1.[Description]=@Description
		   ,T1.IsUserMaintained=@IsUserMaintained
		FROM SystemCodes T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodes(Code,[Description],IsUserMaintained)
		SELECT @Code,@Description,@IsUserMaintained
	END
END
GO



IF NOT OBJECT_ID('AddEditSystemCodeDetail') IS NULL	DROP PROC AddEditSystemCodeDetail
GO
CREATE PROC AddEditSystemCodeDetail
	@Id int=NULL
   ,@SystemCodeId int=NULL
   ,@Code varchar(20)=NULL
   ,@DetailCode varchar(20)
   ,@Description varchar(128)
   ,@OrderNo int
   ,@UserId int
AS
BEGIN
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	SELECT @UserId=Id FROM [AspNetUsers] WHERE Id=@UserId

	IF ISNULL(@SystemCodeId,0)=0
		SELECT @SystemCodeId=Id
		FROM SystemCodes
		WHERE Code=@Code
		
	IF ISNULL(@SystemCodeId,0)=0
		SET @ErrorMsg='Please specify valid SystemCodeId or Code parameter'	
	ELSE IF ISNULL(@DetailCode,'')=''
		SET @ErrorMsg='Please specify valid DetailCode parameter'
	IF @Description IS NULL
		SET @ErrorMsg='Please specify valid Description parameter'
	ELSE IF EXISTS(SELECT 1 FROM SystemCodeDetails WHERE SystemCodeId=@SystemCodeId AND Code=@DetailCode AND Id<>ISNULL(@Id,0))
		SET @ErrorMsg='Possible duplicate entry for the Detail Code ('+@DetailCode+') under the same System Code'
	ELSE IF ISNULL(@UserId,0)=0
		SET @ErrorMsg='Please specify valid UserId parameter'

	IF ISNULL(@ErrorMsg,'')<>''
	BEGIN
		RAISERROR(@ErrorMsg,16,1)
		RETURN
	END

	BEGIN TRAN

	IF ISNULL(@Id,0)>0
	BEGIN
		UPDATE T1
		SET T1.SystemCodeId=@SystemCodeId
		   ,T1.Code=@DetailCode
		   ,T1.[Description]=@Description
		   ,T1.OrderNo=@OrderNo
		   ,T1.ModifiedByUserId=@UserId
		   ,T1.ModifiedOn=GETDATE()
		FROM SystemCodeDetails T1
		WHERE T1.Id=@Id
	END
	ELSE
	BEGIN
		INSERT INTO SystemCodeDetails(SystemCodeId,Code,[Description],OrderNo,CreatedByUserId,CreatedOn)
		SELECT @SystemCodeId,@DetailCode,@Description,@OrderNo,@UserId,GETDATE()

		SET @Id=IDENT_CURRENT('SystemCodeDetails')
	END

	SET @NoOfRows=@@ROWCOUNT
	
	IF @@ERROR>0
	BEGIN
		ROLLBACK TRAN
		SELECT 0 AS NoOfRows
	END
	ELSE
	BEGIN
		COMMIT TRAN
		SELECT @NoOfRows AS NoOfRows

		--EXEC GetSystemCodeDetails	
	END
END
GO



DECLARE @MIG_STAGE tinyint
DECLARE @Year int
DECLARE @Id int
DECLARE @Code varchar(20)
DECLARE @DetailCode varchar(20)
DECLARE @Name varchar(30)
DECLARE @Description varchar(128)
DECLARE @OrderNo int


 SET @MIG_STAGE = 1



IF @MIG_STAGE = 1	--1. GENERAL SETUP
BEGIN	
	SET @Code='System Settings'
	SET @Description='The System Settings'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Tablet Type'
	SET @Description='Various Types / Models of tablets'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Ownership Mode'
	SET @Description='Paid or free '
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Household Status'
	SET @Description='The recepient ownserhip status'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

	SET @Code='Sex'
	SET @Description='The sex of a person'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;
	

    SET @Code='Title'
	SET @Description='The Title of an Individual'
	EXEC AddEditSystemCode @Code=@Code,@Description=@Description,@IsUserMaintained=0;

END
