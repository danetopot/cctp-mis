 DECLARE 
	 @FilePath varchar(128)
	,@DBServer varchar(30)
	,@DBName varchar(30)
	,@DBUser varchar(30)
	,@DBPassword varchar(30)

		SELECT @FilePath = 'E:\\LIVE_CCTP_MIS\\SHAREDFILES\\', @DBName='CCTP-MIS',@DBPassword='SAU@70+', @DBUser='CCTP-MIS',@DBServer='localhost'

	DECLARE @Id int = 1,
	@UserId int = 4

	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
	DECLARE @SystemCodeDetailId3 int
	DECLARE @Len tinyint
	DECLARE @Prefix varchar(10)
	DECLARE @ProgNo int
	DECLARE @HhId int
	DECLARE @LoopVar int


	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)

	DECLARE @FileId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int

	 


	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Account Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Card Status'
	SET @SysDetailCode='1'
	SELECT @SystemCodeDetailId3=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	 
	 
	 delete from temp_BulkTransferFile
 

	 INSERT INTO temp_BulkTransferFile (EnrolmentNo,ProgrammeNo, BulkTransferId,BulkTransfer,ChangeType,ProgrammeId,ProgrammeCode,ProgrammeName,PriReciFirstName,PriReciMiddleName,PriReciSurname,PriReciSex,PriReciNationalIdNo,PriReciDob,PriReciMobileNo1,PriReciMobileNo2,SecReciFirstName,SecReciMiddleName,SecReciSurname,SecReciSexId,SecReciSex,SecReciNationalIdNo,SecReciMobileNo1,SecReciMobileNo2,AccountNo,AccountName,PSPBranch,PSP,AccountOpenedOn,SubLocationName,LocationName,DivisionName,DistrictName,CountyName,ConstituencyName)
	SELECT T3.Id as EnrolmentNo
	,T3.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.ProgrammeNo)))+CONVERT(varchar(6),T3.ProgrammeNo) AS ProgrammeNo
	, BTD.BulkTransferId
	, BT.Name BulkTransfer
	, CT.Name AS ChangeType
	, T2.Id AS ProgrammeId
	, T2.Code AS ProgrammeCode
	, T2.Name AS ProgrammeName  
	, T5.FirstName AS PriReciFirstName
	, T5.MiddleName AS PriReciMiddleName
	, T5.Surname AS PriReciSurname
	, T10.Code AS PriReciSex
	, T5.NationalIdNo AS PriReciNationalIdNo
	, T5.DoB  AS PriReciDob
	, T5.MobileNo1 AS PriReciMobileNo1
	, T5.MobileNo2 AS PriReciMobileNo2  
    , T6.FirstName AS SecReciFirstName
	, T6.MiddleName AS SecReciMiddleName
	, T6.Surname AS SecReciSurname
	, T6.SexId AS SecReciSexId
	, T11.Code AS SecReciSex
	, T6.NationalIdNo AS SecReciNationalIdNo
	, T6.MobileNo1 AS SecReciMobileNo1, T6.MobileNo2 AS SecReciMobileNo2  
	, T7.AccountNo
	, T7.AccountName
	, T7.PSPBranch
	, T7.PSP, T7.OpenedOn AS AccountOpenedOn  
	, T9.SubLocationName
	, T9.LocationName
	, T9.DivisionName
	, T9.DistrictName
	, T9.CountyName
	, T9.ConstituencyName
	
	FROM Household T1
		INNER JOIN Programme T2 ON T1.ProgrammeId=T2.Id
		INNER JOIN HouseholdEnrolment T3 ON T1.Id=T3.HhId
		INNER JOIN Change C ON T3.Id = C.HhEnrolmentId 
		INNER JOIN BulkTransferDetail BTD ON C.Id = BTD.ChangeId
		INNER JOIN BulkTransfer BT ON BT.ID = BTD.BulkTransferId
		INNER JOIN CaseCategory CT ON C.ChangeTypeId = CT.Id
		INNER JOIN HouseholdMember T4 ON T1.Id=T4.HhId
		INNER JOIN Person T5 ON T4.PersonId=T5.Id AND T2.PrimaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1
		LEFT JOIN Person T6 ON T4.PersonId=T6.Id AND T2.SecondaryRecipientId=T4.MemberRoleId AND T4.StatusId=@SystemCodeDetailId1

		INNER JOIN HouseholdSubLocation T8 ON T1.Id=T8.HhId
		INNER JOIN (  
		   SELECT T1.Id AS SubLocationId, T1.Name AS SubLocationName, T2.Id AS LocationId, T2.Name AS LocationName, T3.Id AS DivisionId, T3.Name AS DivisionName, T5.Id AS DistrictId, T5.Name AS DistrictName, T6.Id AS CountyId, T6.Name AS CountyName, T7.Id AS ConstituencyId, T7.Name AS ConstituencyName, T8.Id AS GeoMasterId
		FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
			INNER JOIN Division T3 ON T2.DivisionId=T3.Id
			INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
			INNER JOIN District T5 ON T4.DistrictId=T5.Id
			INNER JOIN County T6 ON T4.CountyId=T6.Id
			INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
			INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id AND T8.IsDefault=1  
		  ) T9 ON T8.SubLocationId=T9.SubLocationId AND T8.GeoMasterId=T9.GeoMasterId
		INNER JOIN SystemCodeDetail T10 ON T5.SexId=T10.Id
		LEFT JOIN SystemCodeDetail T11 ON T6.SexId=T11.Id
		LEFT JOIN (  
		   SELECT DISTINCT T1.HhEnrolmentId, T1.Id AS BeneAccountId, T1.AccountNo, T1.AccountName, T3.Name AS PSPBranch, T4.Name AS PSP, T1.OpenedOn
		FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId=T2.Id
			INNER JOIN PSPBranch T3 ON T1.PSPBranchId=T3.Id
			INNER JOIN PSP T4 ON T3.PSPId=T4.Id
		WHERE  T1.StatusId=@SystemCodeDetailId2  
		  ) T7 ON T3.Id=T7.HhEnrolmentId
	where BTD.BulkTransferId = @Id 
	ORDER BY EnrolmentNo



 
	EXEC UTILITY_SP_PWDGEN @Output=@FilePassword OUTPUT;

	SELECT @FileName='BULK_TRANSFER_'

	SET @DatePart_Day=CASE WHEN(DATEPART(D,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(D,GETDATE())) ELSE CONVERT(char(2),DATEPART(D,GETDATE())) END
	SET @DatePart_Month=CASE WHEN(DATEPART(M,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(M,GETDATE())) ELSE CONVERT(char(2),DATEPART(M,GETDATE())) END
	SET @DatePart_Year=CONVERT(char(4),DATEPART(YY,GETDATE()))
	SET @DatePart_Time=CASE WHEN(DATEPART(hour,GETDATE())<10) THEN '0'+CONVERT(char(1),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END ELSE CONVERT(char(2),DATEPART(hour,GETDATE()))+CASE WHEN(DATEPART(minute,GETDATE()))<10 THEN '0'+CONVERT(char(1),DATEPART(minute,GETDATE())) ELSE CONVERT(char(2),DATEPART(minute,GETDATE())) END END
	SET @FileName=@FileName+'_'+@DatePart_Day+@DatePart_Month+@DatePart_Year+'_'+@DatePart_Time
	SET @FilePathName=@FilePath+@FileName
	SET @FileExtension='.csv'
	SET @FileCompression='.rar'

	SET @SQLStmt='SQLCMD -S '+@DBServer +' -d ' + @DBName + ' -U ' + @DBUser + ' -P ' + @DBPassword  + ' -s , -W -Q ' + '"SET NOCOUNT ON; SELECT * FROM temp_BulkTransferFile" | findstr /V /C:"-" /B> "'+ @FilePathName + @FileExtension +'"'
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;
	SET @SQLStmt='rar.exe a -m5 -hp' + @FilePassword + ' -ep -df ' + @FilePathName + @FileCompression + ' ' + @FilePathName + @FileExtension
	EXEC xp_cmdshell @SQLStmt, NO_OUTPUT;

/*
	SET @SysCode='File Type'
	SET @SysDetailCode='ENROLMENT'
	SELECT @SystemCodeDetailId1=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='File Creation Type'
	SET @SysDetailCode='SYSGEN'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	INSERT INTO FileCreation 	(Name,TypeId,CreationTypeId,FilePath,FileChecksum,FilePassword,CreatedBy,CreatedOn)
	SELECT @FileName+@FileCompression AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, @FilePassword AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

	SET @FileId=IDENT_CURRENT('FileCreation')

	SET @SysCode='Change Status'
	SET @SysDetailCode='Bulk Transfer'
	SELECT @SystemCodeDetailId2=T1.Id
	FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	UPDATE T1 SET T1.ChangeStatusId = @SystemCodeDetailId2  FROM Change T1
		INNER JOIN BulkTransferDetail T2 ON T1.Id = T2.BulkTransferId AND T2.BulkTransferId = @Id
	UPDATE T1 SET T1.FileId = @FileId ,ApvBy = @UserId, ApvOn =GETDATE() FROM BulkTransfer T1 WHERE T1.Id=@Id
	*/
 SELECT @FileName+@FileCompression AS Name, @SystemCodeDetailId1 AS TypeId, @SystemCodeDetailId2 AS CreationTypeId, @FilePath, NULL AS Checksum, @FilePassword AS FilePassword, @UserId AS CreatedBy, GETDATE() AS CreatedOn

	SELECT @FileId AS FileCreationId

 

 --select * from FileCreation

 --update FileCreation set FileChecksum='2F71D5DB7C752A1C207866F5773B372709F85630BA9930808C5324368A1AB53B' where id =66

 update FileCreation  set  Name='BULK_TRANSFER__14052019_1507.rar', FilePassword='6zqzWT8Q'  , FileChecksum='B06B643CB8634B2C1518B744FA73FB5D7FE81AE7FF6EC8B7753455DD186C425F'
where id =66