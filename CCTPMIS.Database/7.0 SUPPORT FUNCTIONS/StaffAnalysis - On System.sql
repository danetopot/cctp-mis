USE  [CCTP-MIS]
GO

;WITH CsvReader AS 
(
SELECT * FROM OPENROWSET
(
	BULK N'D:\\StaffAnalysis\\staff_analysis_201190429.csv', 
	FORMATFILE = N'D:\\StaffAnalysis\\fmt.fmt'
) AS T1
  
  )
SELECT  T2.Id AS EnrolmentNo,
T3.CreatedOn HouseholdAddedOn,
t1.ApvOn HouseholdEnrolledByProgrammeOn,
		T4.Code AS Programme
		,CASE WHEN T70.NationalIdNo IS NOT NULL THEN 'PRIMARY RECIPIENT' ELSE 'SECONDARY RECIPIENT' END 
		, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		, T30.Description as Status
		, T6.FirstName AS BeneFirstName
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
		,T14.AccountName
		,T14.AccountNo
		,T15.Name BankBranch
		,T16.Name Bank
		,T14.OpenedOn 'AccountOpenedOn'
		,T17.Payroll
		,T18.TrxSuccess
		,T18.TrxFailed
		,T70.*
		,T90.*
    
    FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
        INNER JOIN Household T3 ON T2.HhId=T3.Id
		INNER JOIN SYSTEMCODEDETAIL T30 ON T3.StatusId = T30.Id
        INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
        INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
        INNER JOIN Person T6 ON T5.PersonId=T6.Id
        INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
		LEFT JOIN CsvReader T70 ON TRY_CONVERT(bigint,T70.NationalIdNo) = TRY_CONVERT(bigint,T6.NationalIdNo)
        LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
        LEFT JOIN Person T9 ON T8.PersonId=T9.Id
        LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
		LEFT JOIN CsvReader T90 ON TRY_CONVERT(bigint,T90.NationalIdNo) =  TRY_CONVERT(bigint,T9.NationalIdNo)
        INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
        INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
        INNER JOIN (
		SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency FROM SubLocation T1 
			INNER JOIN Location T2 ON T1.LocationId=T2.Id
            INNER JOIN Division T3 ON T2.DivisionId=T3.Id
            INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
            INNER JOIN District T5 ON T4.DistrictId=T5.Id
            INNER JOIN County T6 ON T4.CountyId=T6.Id
            INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
            INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
			) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
        LEFT JOIN BeneficiaryAccount T14 ON T2.ID= T14.HhEnrolmentId
		LEFT JOIN PSPBranch T15 ON T14.PSPBranchId = T15.Id
		LEFT JOIN PSP T16 ON T15.PSPId = T16.Id
		LEFT JOIN ( 
			SELECT COUNT(PaymentCycleId) 'Payroll', HhId from Payroll  group by HhId
		) T17 on T17.HhId = t3.Id
		LEFT JOIN ( 
			SELECT Sum(case when WasTrxSuccessful= 1 then 1 else 0 end) 'TrxSuccess', Sum(case when WasTrxSuccessful= 0 then 1 else 0 end) 'TrxFailed', HhId from Payment  group by HhId
		) T18 on T18.HhId = t3.Id 

		WHERE T70.NationalIdNo IS NOT NULL OR T90.NationalIdNo IS NOT NULL

