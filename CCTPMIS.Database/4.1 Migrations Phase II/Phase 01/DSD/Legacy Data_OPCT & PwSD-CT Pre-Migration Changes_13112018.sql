

/*
alter table ActiveOPCTBeneficiaries
	add MemberCode bigint
   ,CGMemberCode bigint
   ,Exited bit DEFAULT(0)

update t1 set t1.exited=0 from ActiveOPCTBeneficiaries t1

alter table ActivePwSDBeneficiaries
	add MemberCode bigint
   ,CGMemberCode bigint
   ,Exited bit DEFAULT(0)
   
update t1 set t1.exited=0 from ActivePwSDBeneficiaries t1

*/

--OPCT--
select count(t1.rowid) tbl_BeneCGDuplicates 
--update t2 set t2.inactive=1
from tbl_BeneCGDuplicates t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where t1.cyclecode=105 and t1.ProgramCode=2 and isnull(t2.Inactive,0)=0

select count(t1.rowid) as totalBeneexceptions,sum(case when(t1.isduplicate=1) then 1 else 0 end) as duplicated,sum(case when(t1.isinvalid=1) then 1 else 0 end) as isinvalid 
--update t2 set t2.inactive=1
from tbl_BeneficiaryIDNoExceptions t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where CycleCode=105 and t2.Company_Code=2 and isnull(t2.Inactive,0)=0

select count(t1.rowid) as totalCGexceptions,sum(case when(t1.isduplicate=1) then 1 else 0 end) as duplicated,sum(case when(t1.isinvalid=1) then 1 else 0 end) as isinvalid  
--update t2 set t2.inactive=1
from tbl_CaregiverIDNoExceptions t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where CycleCode=105 and t2.Company_Code=2 and isnull(t2.Inactive,0)=0


--PwSD-CT--
select count(t1.rowid) tbl_BeneCGDuplicates 
--update t2 set t2.inactive=1
from tbl_BeneCGDuplicates t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where t1.cyclecode=104 and t1.ProgramCode=12 and isnull(t2.Inactive,0)=0

select count(t1.rowid) as totalBeneexceptions,sum(case when(t1.isduplicate=1) then 1 else 0 end) as duplicated,sum(case when(t1.isinvalid=1) then 1 else 0 end) as isinvalid 
--update t2 set t2.inactive=1
from tbl_BeneficiaryIDNoExceptions t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where CycleCode=104 and t2.Company_Code=12 and isnull(t2.Inactive,0)=0

select count(t1.rowid) as totalCGexceptions,sum(case when(t1.isduplicate=1) then 1 else 0 end) as duplicated,sum(case when(t1.isinvalid=1) then 1 else 0 end) as isinvalid  
--update t2 set t2.inactive=1
from tbl_CaregiverIDNoExceptions t1 inner join register t2 on t1.Register_Id=t2.Register_Id
where CycleCode=104 and t2.Company_Code=12 and isnull(t2.Inactive,0)=0


-->>5. PwSD BENEFICIARIES AS CAREGIVERS HOUSEHOLDS
SELECT T1.Register_Id,T1.Register_No,T1.Programme,T1.BeneficiaryNames,T1.BeneficiaryIDNo,T1.CaregiverNames,T1.CaregiverIDNo,T1.Enrolled,T1.Inactive,T2.Sublocation_Name,T2.Location_Name,T2.Division_Name,T2.District_Name,T2.County_Name,T2.Constituency_Name,CASE WHEN(T3.Register_Id>0) THEN 1 ELSE 0 END AS EverBeenPaid
--UPDATE T5 SET T5.Inactive=1
FROM (
		SELECT Register_Id,REPLACE(REPLACE(REPLACE(T1.Register_No,CHAR(9),''),CHAR(10),''),CHAR(13),'') AS Register_No,T1.Company_Code AS ProgrammeCode,CASE(T1.Company_Code) WHEN 2 THEN 'OPCT' WHEN 12 THEN 'PwSD-CT' END AS Programme,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.Beneficiary_Head,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),'  ',' '))) AS BeneficiaryNames,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.Beneficiary_IDNo,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ',''))) AS BeneficiaryIDNo,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.Caregiver_Names,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),'  ',' '))) AS CaregiverNames,LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.Caregiver_IDNo,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ',''))) AS CaregiverIDNo,SubLocation_Id,Constituency_Id,T1.Enrolled,ISNULL(T1.Inactive,0) AS Inactive 
		FROM Register T1
		WHERE T1.Enrolled IN (2,4) AND ISNULL(T1.Inactive,0)=0
	) T1 LEFT JOIN (
						SELECT T1.SubLocation_Id,T1.SubLocation_Name,T2.Location_Name,T3.Division_Name,T4.District_Name,T5.County_Name,T6.Constituency_Name
						FROM SubLocations T1 INNER JOIN Locations T2 ON T1.Location_Id=T2.Location_Id
											 INNER JOIN Divisions T3 ON T2.Division_Id=T3.Division_Id
											 INNER JOIN Districts T4 ON T3.District_Id=T4.District_Id
											 INNER JOIN Counties T5 ON T4.County_Id=T5.County_Id
											 INNER JOIN Constituencies T6 ON T1.Constituency_Id=T6.Constituency_Id
				) T2 ON T1.Sublocation_Id=T2.Sublocation_Id
		 LEFT JOIN (SELECT T2.Register_Id
					FROM Payments T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
					WHERE T1.Payment_Amount>0
					GROUP BY T2.Register_Id
					) T3 ON T1.Register_Id=T3.Register_Id
		 INNER JOIN (
						SELECT DISTINCT T2.Register_Id
						FROM Payments T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
						WHERE T1.Cycle_Code IN(104,105)
					) T4 ON T1.Register_Id=T4.Register_Id
		 INNER JOIN Register T5 ON T1.Register_Id=T5.Register_Id
WHERE ((T1.BeneficiaryNames=T1.CaregiverNames) OR (T1.BeneficiaryIDNo=T1.CaregiverIDNo))  AND LEN(T1.CaregiverIDNo)>0 AND T1.ProgrammeCode=12


--TOTAL NAME MISMATCH
SELECT T1.*
--UPDATE T2 SET T2.Inactive=1
FROM (
		SELECT T1.ProgrammeNumber
		FROM ActiveOPCTBeneficiaries T1 
		WHERE T1.IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	
		UNION
		SELECT T1.ProgrammeNumber
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1
	) T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.ProgrammeNumber=T2.Register_Id

		SELECT T1.ProgrammeNumber
		--UPDATE T1 SET T1.Exited=1
		FROM ActiveOPCTBeneficiaries T1 
		WHERE T1.IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	
		
		SELECT T1.ProgrammeNumber
		--UPDATE T1 SET T1.Exited=1
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1

SELECT T1.*
--UPDATE T2 SET T2.Inactive=1
FROM (
		SELECT T1.ProgrammeNumber
		FROM ActivePwSDBeneficiaries T1 
		WHERE T1.IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	
		UNION
		SELECT T1.ProgrammeNumber
		FROM ActivePwSDBeneficiaries T1
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1
	) T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.ProgrammeNumber=T2.Register_Id

		SELECT T1.ProgrammeNumber
		--UPDATE T1 SET T1.Exited=1
		FROM ActivePwSDBeneficiaries T1 
		WHERE T1.IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	
		
		SELECT T1.ProgrammeNumber
		--UPDATE T1 SET T1.Exited=1
		FROM ActivePwSDBeneficiaries T1
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1


--UPDATING IPRS DETAIL
SELECT COUNT(DISTINCT T1.ProgrammeNumber)
--UPDATE T1 SET T1.MemberCode=T3.Member_Code
FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 LEFT JOIN Register T2 ON T1.ProgrammeNumber=T2.Register_Id
											   LEFT JOIN (
															SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
															FROM Members T1
															GROUP BY T1.Register_No,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
														) T3 ON T2.Register_No=T3.Register_No AND T1.FirstName=T3.FirstName AND T1.MiddleName=T3.MiddleName AND T1.Surname=T3.Surname AND T1.DateOfBirth=T3.DateofBirth

SELECT COUNT(DISTINCT T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGMemberCode=T3.Member_Code
FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 LEFT JOIN Register T2 ON T1.ProgrammeNumber=T2.Register_Id
											   LEFT JOIN (
															SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
															FROM Members T1
															GROUP BY T1.Register_No,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
														) T3 ON T2.Register_No=T3.Register_No AND T1.CGFirstName=T3.FirstName AND T1.CGMiddleName=T3.MiddleName AND T1.CGSurname=T3.Surname AND T1.CGDateOfBirth=T3.DateofBirth


SELECT COUNT(DISTINCT T1.ProgrammeNumber)
--UPDATE T1 SET T1.MemberCode=T3.Member_Code
FROM [LegacyDB].dbo.ActivePwSDBeneficiaries T1 LEFT JOIN Register T2 ON T1.ProgrammeNumber=T2.Register_Id
											   LEFT JOIN (
															SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
															FROM Members T1
															GROUP BY T1.Register_No,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
														) T3 ON T2.Register_No=T3.Register_No AND T1.FirstName=T3.FirstName AND T1.MiddleName=T3.MiddleName AND T1.Surname=T3.Surname AND T1.DateOfBirth=T3.DateofBirth

SELECT COUNT(DISTINCT T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGMemberCode=T3.Member_Code
FROM [LegacyDB].dbo.ActivePwSDBeneficiaries T1 LEFT JOIN Register T2 ON T1.ProgrammeNumber=T2.Register_Id
											   LEFT JOIN (
															SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
															FROM Members T1
															GROUP BY T1.Register_No,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
														) T3 ON T2.Register_No=T3.Register_No AND T1.CGFirstName=T3.FirstName AND T1.CGMiddleName=T3.MiddleName AND T1.CGSurname=T3.Surname AND T1.CGDateOfBirth=T3.DateofBirth



SELECT COUNT(T1.Register_No)
--UPDATE T1 SET T1.IDNo=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Gender=CASE(T3.Sex) WHEN 'M' THEN 0 ELSE 1 END,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
FROM Members T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
				INNER JOIN (
								SELECT T1.ProgrammeNumber,T1.MemberCode,T2.FirstName,T2.MiddleName,T2.Surname,T2.Sex,T2.DoB,T2.IDNumber
								FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								WHERE T1.IPRS_IDNoExists=1 AND T1.Exited=0
							)T3 ON T2.Register_Id=T3.ProgrammeNumber AND T1.Member_Code=T3.MemberCode

SELECT COUNT(T1.Register_No)
--UPDATE T1 SET T1.IDNo=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Gender=CASE(T3.Sex) WHEN 'M' THEN 0 ELSE 1 END,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
FROM Members T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
				INNER JOIN (
								SELECT T1.ProgrammeNumber,T1.CGMemberCode,T2.FirstName,T2.MiddleName,T2.Surname,T2.Sex,T2.DoB,T2.IDNumber
								FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
								WHERE T1.IPRS_CGIDNoExists=1 AND T1.Exited=0
							)T3 ON T2.Register_Id=T3.ProgrammeNumber AND T1.Member_Code=T3.CGMemberCode

SELECT COUNT(T1.Register_No)
--UPDATE T1 SET T1.IDNo=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Gender=CASE(T3.Sex) WHEN 'M' THEN 0 ELSE 1 END,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
FROM Members T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
				INNER JOIN (
								SELECT T1.ProgrammeNumber,T1.MemberCode,T2.FirstName,T2.MiddleName,T2.Surname,T2.Sex,T2.DoB,T2.IDNumber
								FROM [LegacyDB].dbo.ActivePwSDBeneficiaries T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								WHERE T1.IPRS_IDNoExists=1 AND T1.Exited=0
							)T3 ON T2.Register_Id=T3.ProgrammeNumber AND T1.Member_Code=T3.MemberCode

SELECT COUNT(T1.Register_No)
--UPDATE T1 SET T1.IDNo=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Gender=CASE(T3.Sex) WHEN 'M' THEN 0 ELSE 1 END,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
FROM Members T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
				INNER JOIN (
								SELECT T1.ProgrammeNumber,T1.CGMemberCode,T2.FirstName,T2.MiddleName,T2.Surname,T2.Sex,T2.DoB,T2.IDNumber
								FROM [LegacyDB].dbo.ActivePwSDBeneficiaries T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
								WHERE T1.IPRS_CGIDNoExists=1 AND T1.Exited=0
							)T3 ON T2.Register_Id=T3.ProgrammeNumber AND T1.Member_Code=T3.CGMemberCode

--DROP TABLE ActiveOPCTBeneficiariesUPDATED
--DROP TABLE ActivePwSDBeneficiariesUPDATED
--SELECT * INTO ActiveOPCTBeneficiariesUPDATED FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries;
--SELECT * INTO ActivePwSDBeneficiariesUPDATED FROM [LegacyDB].dbo.ActivePwSDBeneficiaries


SELECT COUNT(T1.ProgrammeNumber)
--UPDATE T1 SET T1.IDNumber=T1.ResolvedIDNo,T1.FirstName=T2.FirstName,T1.MiddleName=T2.MiddleName,T1.Surname=T2.Surname,T1.Sex=ISNULL(T2.Sex,T1.Sex),T1.DateOfBirth=ISNULL(T2.DoB,T1.DateOfBirth)
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1.IPRS_IDNoExists=1 AND T1.Exited=0

SELECT COUNT(T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGIDNumber=T1.ResolvedCGIDNo,T1.CGFirstName=T2.FirstName,T1.CGMiddleName=T2.MiddleName,T1.CGSurname=T2.Surname,T1.CGSex=ISNULL(T1.Sex,T1.CGSex),T1.CGDateOfBirth=ISNULL(T2.DoB,T1.DateOfBirth)
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
WHERE T1.IPRS_CGIDNoExists=1 AND T1.Exited=0

SELECT COUNT(T1.ProgrammeNumber)
--UPDATE T1 SET T1.IDNumber=T1.ResolvedIDNo,T1.FirstName=T2.FirstName,T1.MiddleName=T2.MiddleName,T1.Surname=T2.Surname,T1.Sex=ISNULL(T2.Sex,T1.Sex),T1.DateOfBirth=ISNULL(T2.DoB,T1.DateOfBirth)
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1.IPRS_IDNoExists=1 AND T1.Exited=0

SELECT COUNT(T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGIDNumber=T1.ResolvedCGIDNo,T1.CGFirstName=T2.FirstName,T1.CGMiddleName=T2.MiddleName,T1.CGSurname=T2.Surname,T1.CGSex=ISNULL(T1.Sex,T1.CGSex),T1.CGDateOfBirth=ISNULL(T2.DoB,T1.DateOfBirth)
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
WHERE T1.IPRS_CGIDNoExists=1 AND T1.Exited=0



SELECT T1.*
--UPDATE T1 SET T1.FirstName=REPLACE(T1.FirstName,'  ',' ')
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1
WHERE T1.Exited=0 AND ISNULL(T1.Surname,'')='' AND T1.FirstName<>'' AND T1.FirstName LIKE '%  %'

SELECT REPLACE(T1.CGFirstName,'  ',' '),T1.*
--UPDATE T1 SET T1.CGFirstName=REPLACE(T1.CGFirstName,'  ',' ')
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1
WHERE T1.Exited=0 AND ISNULL(T1.CGSurname,'')='' AND T1.CGFirstName<>'' AND T1.CGFirstName LIKE '%  %'


SELECT T1.*
--UPDATE T1 SET T1.FirstName=REPLACE(T1.FirstName,'  ',' ')
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1
WHERE T1.Exited=0 AND ISNULL(T1.Surname,'')='' AND T1.FirstName<>'' AND T1.FirstName LIKE '%  %'

SELECT REPLACE(T1.CGFirstName,'  ',' '),T1.*
--UPDATE T1 SET T1.CGFirstName=REPLACE(T1.CGFirstName,'  ',' ')
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1
WHERE T1.Exited=0 AND ISNULL(T1.CGSurname,'')='' AND T1.CGFirstName<>'' AND T1.CGFirstName LIKE '%  %'



SELECT CASE WHEN(T2.FirstName='') THEN T1.FirstName ELSE T2.FirstName END AS FirstName
	,CASE WHEN(RTRIM(LTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN '' ELSE T2.MiddleName END AS MiddleName
	,CASE WHEN(LTRIM(RTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN T1.MiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.FirstName,'')))='') THEN T1.Surname ELSE T2.Surname END END AS Surname
	,'||',T1.FirstName,T1.MiddleName,T1.Surname,'||'
--UPDATE T1 SET T1.FirstName=CASE WHEN(T2.FirstName='') THEN T1.FirstName ELSE T2.FirstName END,T1.MiddleName=CASE WHEN(RTRIM(LTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN '' ELSE T2.MiddleName END,T1.Surname=CASE WHEN(LTRIM(RTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN T1.MiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.FirstName,'')))='') THEN T1.Surname ELSE T2.Surname END END
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN (
																	SELECT T1.ProgrammeNumber
																		,RTRIM(LEFT(T1.FirstName,CHARINDEX(' ',T1.FirstName))) AS FirstName
																		,CASE WHEN(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))=0) THEN ''
																			  WHEN((CHARINDEX(' ',T1.FirstName))+1 = (CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1)))) THEN '' 
																			  ELSE SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1),CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))-(CHARINDEX(' ',T1.FirstName)+1)) 
																		 END AS MiddleName
																		,CASE WHEN(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))=0) THEN SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1),LEN(T1.FirstName) - (CHARINDEX(' ',T1.FirstName)))
																		 ELSE SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))+1),LEN(T1.FirstName) - (CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName))))) 
																		 END AS Surname
																	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1
																	WHERE T1.Exited=0 AND ISNULL(T1.Surname,'')='' AND T1.FirstName<>''
																) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber


SELECT CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END AS FirstName
	,CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END AS MiddleName
	,CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END AS Surname
	,'||',T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,'||'
--UPDATE T1 SET T1.CGFirstName=CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END,T1.CGMiddleName=CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END,T1.CGSurname=CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END
FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN (
																	SELECT T1.ProgrammeNumber
																		,RTRIM(LEFT(T1.CGFirstName,CHARINDEX(' ',T1.CGFirstName))) AS CGFirstName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN ''
																			  WHEN((CHARINDEX(' ',T1.CGFirstName))+1 = (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1)))) THEN '' 
																			  ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))-(CHARINDEX(' ',T1.CGFirstName)+1)) 
																		 END AS CGMiddleName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName)))
																		 ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName))))) 
																		 END AS CGSurname
																	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1
																	WHERE T1.Exited=0 AND ISNULL(T1.CGSurname,'')='' AND T1.CGFirstName<>''
																) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber


SELECT CASE WHEN(T2.FirstName='') THEN T1.FirstName ELSE T2.FirstName END AS FirstName
	,CASE WHEN(RTRIM(LTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN '' ELSE T2.MiddleName END AS MiddleName
	,CASE WHEN(LTRIM(RTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN T1.MiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.FirstName,'')))='') THEN T1.Surname ELSE T2.Surname END END AS Surname
	,'||',T1.FirstName,T1.MiddleName,T1.Surname,'||'
--UPDATE T1 SET T1.FirstName=CASE WHEN(T2.FirstName='') THEN T1.FirstName ELSE T2.FirstName END,T1.MiddleName=CASE WHEN(RTRIM(LTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN '' ELSE T2.MiddleName END,T1.Surname=CASE WHEN(LTRIM(RTRIM(ISNULL(T1.MiddleName,'')))<>'') THEN T1.MiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.FirstName,'')))='') THEN T1.Surname ELSE T2.Surname END END
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN (
																	SELECT T1.ProgrammeNumber
																		,RTRIM(LEFT(T1.FirstName,CHARINDEX(' ',T1.FirstName))) AS FirstName
																		,CASE WHEN(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))=0) THEN ''
																			  WHEN((CHARINDEX(' ',T1.FirstName))+1 = (CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1)))) THEN '' 
																			  ELSE SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1),CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))-(CHARINDEX(' ',T1.FirstName)+1)) 
																		 END AS MiddleName
																		,CASE WHEN(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))=0) THEN SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1),LEN(T1.FirstName) - (CHARINDEX(' ',T1.FirstName)))
																		 ELSE SUBSTRING(T1.FirstName,(CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName)+1))+1),LEN(T1.FirstName) - (CHARINDEX(' ',T1.FirstName,(CHARINDEX(' ',T1.FirstName))))) 
																		 END AS Surname
																	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1
																	WHERE T1.Exited=0 AND ISNULL(T1.Surname,'')='' AND T1.FirstName<>''
																) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber




SELECT CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END AS FirstName
	,CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END AS MiddleName
	,CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END AS Surname
	,'||',T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,'||'
--UPDATE T1 SET T1.CGFirstName=CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END,T1.CGMiddleName=CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END,T1.CGSurname=CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN (
																	SELECT T1.ProgrammeNumber
																		,RTRIM(LEFT(T1.CGFirstName,CHARINDEX(' ',T1.CGFirstName))) AS CGFirstName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN ''
																			  WHEN((CHARINDEX(' ',T1.CGFirstName))+1 = (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1)))) THEN '' 
																			  ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))-(CHARINDEX(' ',T1.CGFirstName)+1)) 
																		 END AS CGMiddleName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName)))
																		 ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName))))) 
																		 END AS CGSurname
																	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1
																	WHERE T1.Exited=0 AND ISNULL(T1.CGSurname,'')='' AND T1.CGFirstName<>''
																) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber


UPDATE T1
SET T1.SubLocationCode=428040202
--SELECT T1.*
FROM ActivePwSDBeneficiariesUPDATED T1
WHERE T1.ProgrammeNumber=139802



SELECT T2.Beneficiary_Head,T2.Beneficiary_IDNo,T2.Caregiver_Names,T2.Caregiver_IDNo,T1.* --COUNT(T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGFirstName=T2.Caregiver_Names,T1.CGMiddleName='',T1.CGSurname='',T1.CGIDNumber=T2.Caregiver_IDNo,T1.IPRS_CGIDNoExists=0,T1.IPRS_CGFirstNameExists=0,T1.IPRS_CGMiddleNameExists=0,T1.IPRS_CGSurnameExists=0,T1.IPRS_CGDoBMatches=0,T1.IPRS_CGDoBYearMatches=0,T1.IPRS_CGSexMatches=0,T1.CGMemberCode=NULL,T1.Exited=0
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN Register T2 ON T1.ProgrammeNumber=T2.Register_Id
WHERE ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.Caregiver_IDNo,'')


SELECT T1.*
--UPDATE T1 SET T1.ResolvedCGIDNo=REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[CGIDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ',''),T1.HasInvalid2=CASE WHEN(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[CGIDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') LIKE '%[^0-9]%') THEN 1 ELSE 0 END
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
WHERE ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.CGIDNumber,'')


UPDATE T1
SET T1.IPRS_CGIDNoExists=CONVERT(bit,ISNULL(T2.IDNumber,0))
   ,T1.IPRS_CGFirstNameExists=CASE WHEN(UPPER(ISNULL(T2.FirstName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.FirstName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGMiddleNameExists=CASE WHEN(UPPER(ISNULL(T2.MiddleName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.MiddleName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGSurnameExists=CASE WHEN(UPPER(ISNULL(T2.Surname,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.Surname),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGDoBMatches=CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(T1.CGDateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_CGDoBYearMatches=CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(YEAR(T1.CGDateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_CGSexMatches=CASE WHEN(UPPER(T1.CGSex)=UPPER(T2.Sex)) THEN 1 ELSE 0 END
--SELECT T1.*
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
													  INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T3.CGIDNumber,'')
WHERE T1.HasInvalid2=0





SELECT T1.ProgrammeNumber
--UPDATE T3 SET T3.Inactive=0
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.CGIDNumber,'')
													  INNER JOIN Register T3 ON T1.ProgrammeNumber=T3.Register_Id


SELECT T1.*
--UPDATE T2 SET T2.Inactive=1
FROM (
		SELECT T1.ProgrammeNumber
		FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.CGIDNumber,'')
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1
	) T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.ProgrammeNumber=T2.Register_Id


		SELECT T1.ProgrammeNumber
		--UPDATE T1 SET T1.Exited=1
		FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.CGIDNumber,'')
		WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1



SELECT COUNT(DISTINCT T1.ProgrammeNumber)
--UPDATE T1 SET T1.CGMemberCode=T4.Member_Code
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T2.CGIDNumber,'')
													  INNER JOIN Register T3 ON T1.ProgrammeNumber=T3.Register_Id
													  INNER JOIN (
																	SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
																	FROM Members T1
																	GROUP BY T1.Register_No,T1.FirstName,T1.MiddleName,T1.Surname,T1.DateOfBirth
																) T4 ON T3.Register_No=T4.Register_No AND T1.CGFirstName=T4.FirstName AND T1.CGMiddleName=T4.MiddleName AND T1.CGSurname=T4.Surname AND T1.CGDateOfBirth=T4.DateofBirth


SELECT COUNT(T1.Register_No)
--UPDATE T1 SET T1.IDNo=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Gender=CASE(T3.Sex) WHEN 'M' THEN 0 ELSE 1 END,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
FROM Members T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
				INNER JOIN (
								SELECT T1.ProgrammeNumber,T1.CGMemberCode,T2.FirstName,T2.MiddleName,T2.Surname,T2.Sex,T2.DoB,T2.IDNumber
								FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.ActivePwSDBeneficiaries T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber AND ISNULL(T1.CGIDNumber,'')<>ISNULL(T3.CGIDNumber,'')
																					  INNER JOIN [LegacyDB].dbo.IPRS_Legacy T2 ON CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
								WHERE T1.IPRS_CGIDNoExists=1 AND T1.Exited=0
							)T3 ON T2.Register_Id=T3.ProgrammeNumber AND T1.Member_Code=T3.CGMemberCode



SELECT CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END AS FirstName
	,CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END AS MiddleName
	,CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END AS Surname
	,'||',T1.CGFirstName,T1.CGMiddleName,T1.CGSurname,'||'
--UPDATE T1 SET T1.CGFirstName=CASE WHEN(T2.CGFirstName='') THEN T1.CGFirstName ELSE T2.CGFirstName END,T1.CGMiddleName=CASE WHEN(RTRIM(LTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN '' ELSE T2.CGMiddleName END,T1.CGSurname=CASE WHEN(LTRIM(RTRIM(ISNULL(T1.CGMiddleName,'')))<>'') THEN T1.CGMiddleName ELSE CASE WHEN(LTRIM(RTRIM(ISNULL(T2.CGFirstName,'')))='') THEN T1.CGSurname ELSE T2.CGSurname END END
FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN (
																	SELECT T1.ProgrammeNumber
																		,RTRIM(LEFT(T1.CGFirstName,CHARINDEX(' ',T1.CGFirstName))) AS CGFirstName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN ''
																			  WHEN((CHARINDEX(' ',T1.CGFirstName))+1 = (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1)))) THEN '' 
																			  ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))-(CHARINDEX(' ',T1.CGFirstName)+1)) 
																		 END AS CGMiddleName
																		,CASE WHEN(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))=0) THEN SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName)))
																		 ELSE SUBSTRING(T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName)+1))+1),LEN(T1.CGFirstName) - (CHARINDEX(' ',T1.CGFirstName,(CHARINDEX(' ',T1.CGFirstName))))) 
																		 END AS CGSurname
																	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1
																	WHERE T1.Exited=0 AND ISNULL(T1.CGSurname,'')='' AND T1.CGFirstName<>''
																) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber

