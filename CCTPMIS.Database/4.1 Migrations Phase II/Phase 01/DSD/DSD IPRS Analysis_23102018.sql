


DROP TABLE ActiveOPCTBeneficiaries

DECLARE @Prog varchar(10)

SET @Prog=12


	SELECT T1.* INTO ActiveOPCTBeneficiaries
	FROM (
		SELECT T1.*,REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[IDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') AS ResolvedIDNo,CASE WHEN(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[IDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') LIKE '%[^0-9]%') THEN 1 ELSE 0 END AS HasInvalid1
			,REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[CGIDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') AS ResolvedCGIDNo,CASE WHEN(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.[CGIDNumber],''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') LIKE '%[^0-9]%') THEN 1 ELSE 0 END AS HasInvalid2
		FROM (
				SELECT T1.Register_Id AS ProgrammeNumber
					,CASE WHEN(T2.Register_No IS NULL) THEN T1.BeneficiaryNames ELSE T2.FirstName END AS [FirstName]
					,ISNULL(T2.MiddleName,'') AS [MiddleName]
					,ISNULL(T2.Surname,'') AS Surname
					,CASE WHEN(T2.Register_No IS NULL) THEN T1.BeneficiaryIDNo ELSE T2.IDNo END AS [IDNumber]
					,CASE WHEN(T2.Register_No IS NULL) THEN '' ELSE CASE (ISNULL(T2.Gender,0)) WHEN 0 THEN 'M' ELSE 'F' END END AS Sex
					,CASE WHEN(T2.Register_No IS NULL) THEN NULL ELSE T2.DateofBirth END AS DateOfBirth
					,CASE WHEN(T3.Register_No IS NULL) THEN T1.CaregiverNames ELSE ISNULL(T3.FirstName,'') END AS [CGFirstName]
					,ISNULL(T3.MiddleName,'') AS [CGMiddleName]
					,ISNULL(T3.Surname,'') AS [CGSurname]
					,CASE WHEN(T3.Register_No IS NULL) THEN T1.CaregiverIDNo ELSE ISNULL(T3.IDNo,'') END AS [CGIDNumber]
					,CASE WHEN(T3.Register_No IS NULL) THEN '' ELSE CASE (ISNULL(T3.Gender,0)) WHEN 0 THEN 'M' ELSE 'F' END END AS [CGSex]
					,CASE WHEN(T3.Register_No IS NULL) THEN NULL ELSE T3.DateofBirth END AS CGDateOfBirth
					,T4.County_Name,ISNULL(T5.Constituency_Name,T4.Constituency_Name) AS Constituency_Name,T4.District_Name,T4.Division_Name,T4.Location_Name,T4.SubLocation_Name
				FROM (	SELECT Register_Id,Register_No,T1.SerialNo,T1.Beneficiary_Head,REPLACE(REPLACE(REPLACE(T1.Register_No,CHAR(9),' '),CHAR(10),' '),CHAR(13),' ') AS ProgrammeNumber,T1.Company_Code,CASE(T1.Company_Code) WHEN 2 THEN 'OPCT' WHEN 12 THEN 'PwSD-CT' END AS Programme,REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.Beneficiary_Head,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' ') AS BeneficiaryNames,REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.Beneficiary_IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' ') AS BeneficiaryIDNo,REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.Caregiver_Names,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' ') AS CaregiverNames,REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.Caregiver_IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' ') AS CaregiverIDNo,SubLocation_Id,Constituency_Id,T1.Village,T1.Enrolled,ISNULL(T1.Inactive,0) AS Inactive
						FROM [DGSD].dbo.Register T1
						WHERE T1.Company_Code=@Prog AND ISNULL(T1.Enrolled,0) IN(2,4) AND ISNULL(T1.Inactive,0)=0
					) T1 LEFT JOIN (
									SELECT T1.Register_No,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','') AS IDNo,T1.Gender,T1.DateofBirth,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.FirstName,LEN(T1.FirstName)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS FirstName,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.MiddleName,LEN(T1.MiddleName)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS MiddleName,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.Surname,LEN(T1.Surname)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS Surname
									FROM [DGSD].dbo.Members T1 INNER JOIN (SELECT Register_No,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','') AS IDNo,MIN(Member_Code) AS Member_Code
																			FROM [DGSD].dbo.Members
																			WHERE REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','')<>''
																			GROUP BY Register_No,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','')
																			) T2 ON T1.Register_No=T2.Register_No AND T1.Member_Code=T2.Member_Code AND REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','')=T2.IDNo
							) T2 ON T1.Register_No=T2.Register_No AND T1.BeneficiaryIDNo=T2.IDNo 
						 LEFT JOIN (
									SELECT T1.Register_No,REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(T1.IDNo,''))),CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),' ','') AS IDNo,T1.Gender,T1.DateofBirth,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.FirstName,LEN(T1.FirstName)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS FirstName,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.MiddleName,LEN(T1.MiddleName)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS MiddleName,REPLACE(REPLACE(REPLACE(ISNULL(LEFT(T1.Surname,LEN(T1.Surname)),''),CHAR(10),' '),CHAR(9),' '),CHAR(13),' ') AS Surname
									FROM [DGSD].dbo.Members T1 INNER JOIN (SELECT T1.Register_No,MIN(T1.Member_Code) AS Member_Code
																FROM [DGSD].dbo.Members T1 INNER JOIN [DGSD].dbo.MemberRelationships T2 ON T1.Member_Code = T2.Member_Code    
																				INNER JOIN [DGSD].dbo.Relationships T3 ON T2.Relationship_Code = T3.Relationship_Code  
																WHERE T3.Group_Code = 2 
																GROUP BY Register_No
																) T2 ON T1.Register_No=T2.Register_No AND T1.Member_Code=T2.Member_Code
							) T3 ON T1.Register_No=T3.Register_No
						 LEFT JOIN (
									SELECT T1.SubLocation_Id,T1.SubLocation_Name,T2.Location_Name,T3.Division_Name,T4.District_Name,T5.County_Name,T6.Constituency_Name
									FROM [DGSD].dbo.SubLocations T1 INNER JOIN [DGSD].dbo.Locations T2 ON T1.Location_Id=T2.Location_Id
														 INNER JOIN [DGSD].dbo.Divisions T3 ON T2.Division_Id=T3.Division_Id
														 INNER JOIN [DGSD].dbo.Districts T4 ON T3.District_Id=T4.District_Id
														 INNER JOIN [DGSD].dbo.Counties T5 ON T4.County_Id=T5.County_Id
														 INNER JOIN [DGSD].dbo.Constituencies T6 ON T1.Constituency_Id=T6.Constituency_Id
							) T4 ON T1.SubLocation_Id=T4.SubLocation_Id
						LEFT JOIN [DGSD].dbo.Constituencies T5 ON T1.Constituency_Id=T5.Constituency_Id
						--LEFT JOIN (
						--			SELECT T2.Register_Id
						--			FROM Payments T1 INNER JOIN Register T2 ON T1.Register_No=T2.Register_No
						--			WHERE T1.Cycle_Code IN(102,103)
						--		) T6 ON T1.Register_Id=T6.Register_Id
						INNER JOIN (
									SELECT T2.Register_Id
									FROM [DGSD].dbo.Payments T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.Register_No=T2.Register_No
									WHERE T1.Cycle_Code IN(104,105)
								) T7 ON T1.Register_Id=T7.Register_Id
				WHERE T1.Company_Code=@Prog AND ISNULL(T1.Inactive,0)=0 AND T1.Enrolled IN(2,4) --AND T6.Register_Id IS NULL
		) T1
	) T1
GO


SELECT T2.Register_Id
FROM [DGSD].dbo.Payments T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.Register_No=T2.Register_No
WHERE T1.Cycle_Code IN(104)


alter table ActiveOPCTBeneficiaries
	add IPRS_IDNoExists bit
	,IPRS_FirstNameExists bit
	,IPRS_MiddleNameExists bit
	,IPRS_SurnameExists bit
	,IPRS_DoBMatches bit
	,IPRS_DoBYearMatches bit
	,IPRS_SexMatches bit
	,IPRS_CGIDNoExists bit
	,IPRS_CGFirstNameExists bit
	,IPRS_CGMiddleNameExists bit
	,IPRS_CGSurnameExists bit
	,IPRS_CGDoBMatches bit
	,IPRS_CGDoBYearMatches bit
	,IPRS_CGSexMatches bit
go

UPDATE T1
SET T1.IPRS_IDNoExists=0
   ,T1.IPRS_FirstNameExists=0
   ,T1.IPRS_MiddleNameExists=0
   ,T1.IPRS_SurnameExists=0
   ,T1.IPRS_DoBMatches=0
   ,T1.IPRS_DoBYearMatches=0
   ,T1.IPRS_SexMatches=0
   ,T1.IPRS_CGIDNoExists=0
   ,T1.IPRS_CGFirstNameExists=0
   ,T1.IPRS_CGMiddleNameExists=0
   ,T1.IPRS_CGSurnameExists=0
   ,T1.IPRS_CGDoBMatches=0
   ,T1.IPRS_CGDoBYearMatches=0
   ,T1.IPRS_CGSexMatches=0
FROM ActiveOPCTBeneficiaries T1


SELECT TOP 10 * FROM ActiveOPCTBeneficiaries
SELECT COUNT(ProgrammeNumber) FROM ActiveOPCTBeneficiaries


SELECT 
	--t1.[DateOfBirth],CONVERT(bit,ISNULL(T2.IDNumber,0)) IPRS_IDNoExists
	--,t1.FirstName,t2.firstname
	--,CASE WHEN(UPPER(ISNULL(T2.FirstName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.FirstName),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END IPRS_FirstNameExists
	--,CASE WHEN(UPPER(ISNULL(T2.MiddleName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.MiddleName),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END IPRS_MiddleNameExists
	--,CASE WHEN(UPPER(ISNULL(T2.Surname,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.Surname),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END IPRS_SurnameExists
	--,CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(T1.DateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_DoBMatches
	--,CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(YEAR(T1.DateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_DoBYearMatches
	--,CASE WHEN(T1.Sex=T2.Sex) THEN 1 ELSE 0 END AS IPRS_SexMatches

UPDATE T1
SET T1.IPRS_IDNoExists=CONVERT(bit,ISNULL(T2.IDNumber,0))
   ,T1.IPRS_FirstNameExists=CASE WHEN(UPPER(ISNULL(T2.FirstName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.FirstName),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_MiddleNameExists=CASE WHEN(UPPER(ISNULL(T2.MiddleName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.MiddleName),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_SurnameExists=CASE WHEN(UPPER(ISNULL(T2.Surname,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.FirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.MiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.Surname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.Surname),UPPER(T1.FirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_DoBMatches=CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(T1.DateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_DoBYearMatches=CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(YEAR(T1.DateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_SexMatches=CASE WHEN(UPPER(T1.Sex)=UPPER(T2.Sex)) THEN 1 ELSE 0 END
FROM ActiveOPCTBeneficiaries T1 INNER JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE HasInvalid1=0



SELECT 
	--t1.[CGDateOfBirth],CONVERT(bit,ISNULL(T2.IDNumber,0)) IPRS_CGIDNoExists
	--,t1.CGFirstName,t2.firstname
	--,CASE WHEN(UPPER(ISNULL(T2.FirstName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.FirstName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END IPRS_CGFirstNameExists
	--,CASE WHEN(UPPER(ISNULL(T2.MiddleName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.MiddleName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END IPRS_CGMiddleNameExists
	--,CASE WHEN(UPPER(ISNULL(T2.Surname,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.Surname),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END IPRS_CGSurnameExists
	--,CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(T1.CGDateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_CGDoBMatches
	--,CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(YEAR(T1.CGDateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_CGDoBYearMatches
	--,CASE WHEN(T1.CGSex=T2.Sex) THEN 1 ELSE 0 END AS IPRS_CGSexMatches

UPDATE T1
SET T1.IPRS_CGIDNoExists=CONVERT(bit,ISNULL(T2.IDNumber,0))
   ,T1.IPRS_CGFirstNameExists=CASE WHEN(UPPER(ISNULL(T2.FirstName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.FirstName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGMiddleNameExists=CASE WHEN(UPPER(ISNULL(T2.MiddleName,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.MiddleName),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGSurnameExists=CASE WHEN(UPPER(ISNULL(T2.Surname,'')) COLLATE DATABASE_DEFAULT IN(UPPER(T1.CGFirstName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGMiddleName) COLLATE DATABASE_DEFAULT,UPPER(T1.CGSurname) COLLATE DATABASE_DEFAULT) OR CHARINDEX(UPPER(T2.Surname),UPPER(T1.CGFirstName))>0) THEN 1 ELSE 0 END
   ,T1.IPRS_CGDoBMatches=CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(T1.CGDateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_CGDoBYearMatches=CASE WHEN(ISDATE(T1.CGDateOfBirth)=1) THEN CASE WHEN(YEAR(T1.CGDateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_CGSexMatches=CASE WHEN(UPPER(T1.CGSex)=UPPER(T2.Sex)) THEN 1 ELSE 0 END
FROM   T1 INNER JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
WHERE HasInvalid2=0



IF NOT OBJECT_ID('IPRS_Legacy') IS NULL	DROP VIEW IPRS_Legacy
GO
CREATE VIEW IPRS_Legacy
AS
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_CTOVC_CG
	WHERE ISNULL(IDNumber,'')<>'' AND IDNumber NOT IN(508804,50713,4914508,20358752,134659)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
	UNION
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_OPCT_CG
	WHERE ISNULL(IDNumber,'')<>'' AND IDNumber NOT IN(157814,585132,82607,20762327,672798,503037,30737)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
	UNION
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_OPCT_Bene
	WHERE ISNULL(IDNumber,'')<>'' AND IDNumber NOT IN(157814,585132,82607,20762327,672798,503037,30737)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
	UNION
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_PwSD_CG
	WHERE ISNULL(IDNumber,'')<>'' AND IDNumber NOT IN(107163,268344,282268,22109235)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
	UNION
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_PwSD_Bene
	WHERE ISNULL(IDNumber,'')<>'' AND IDNumber NOT IN(107163,268344,282268,22109235)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
GO


select * from IPRS_PwSD_CG where idnumber IN(107163,268344,282268,22109235)
UNION
select * from IPRS_OPCT_Bene where idnumber IN(157814,585132,82607,20762327,672798,503037,30737)



select idnumber from IPRS_Legacy group by idnumber having count(firstname)>1

select count(programmenumber) from ActiveOPCTBeneficiaries




--ENTIRE LIST
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE T1. IPRS_IDNoExists=1 --AND T1.ProgrammeNumber>307000 AND T1.ProgrammeNumber<=430000
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,NULL AS IPRS_IDNo
	,NULL AS IPRS_Name
	,NULL AS IPRS_Sex,NULL AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
WHERE T1. IPRS_IDNoExists=0 --AND T1.ProgrammeNumber>307000 AND T1.ProgrammeNumber<=430000
ORDER BY T1.ProgrammeNumber




--MISSING IN IPRS
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,NULL AS IPRS_IDNo
	,NULL AS IPRS_Name
	,NULL AS IPRS_Sex,NULL AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
WHERE T1. IPRS_IDNoExists=0 --AND (T1.IDNumber<>'')
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,NULL AS IPRS_CGIDNo
							,NULL AS IPRS_CGName
							,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1
						WHERE T1. IPRS_CGIDNoExists=0 --AND (T1.CGFirstName<>'' OR T1.CGSurname<>'' OR T1.CGIDNumber<>'')
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber

--181000, 307000, 430000



--TOTAL NAME MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE T1.IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,T2.IDNumber AS IPRS_CGIDNo
							,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
							,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
						WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)<=1
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber



--TOTAL DoB MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE T1. IPRS_IDNoExists=1 AND T1.IPRS_DoBMatches=0 AND T1.IPRS_DoBYearMatches=0
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,T2.IDNumber AS IPRS_CGIDNo
							,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
							,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
						WHERE T1. IPRS_CGIDNoExists=1 AND T1.IPRS_CGDoBMatches=0 AND T1.IPRS_CGDoBYearMatches=0
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber



--PARTIAL NAME MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE T1. IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)=2
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,T2.IDNumber AS IPRS_CGIDNo
							,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
							,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
						WHERE T1. IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)=2
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber



--PARTIAL DoB MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE IPRS_IDNoExists=1 AND T1.IPRS_DoBMatches=0 AND T1.IPRS_DoBYearMatches=1
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,T2.IDNumber AS IPRS_CGIDNo
							,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
							,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
						WHERE IPRS_CGIDNoExists=1 AND T1.IPRS_CGDoBMatches=0 AND T1.IPRS_CGDoBYearMatches=1
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber



--SEX MISMATCH 
SELECT T1.ProgrammeNumber,T1.IDNumber
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T3.CGIDNumber
	,T3.CGName
	,T3.CGSex,T3.CGDateOfBirth
	,T3.IPRS_CGIDNo
	,T3.IPRS_CGName
	,T3.IPRS_CGSex,T3.IPRS_CGDoB
	,T3.IPRS_CGIDNoExists
	,T3.IPRS_CGFirstNameExists
	,T3.IPRS_CGMiddleNameExists
	,T3.IPRS_CGSurnameExists
	,T3.IPRS_CGDoBMatches
	,T3.IPRS_CGDoBYearMatches
	,T3.IPRS_CGSexMatches
FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
								LEFT JOIN (
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,T2.IDNumber AS IPRS_CGIDNo
												,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
												,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
											WHERE T1. IPRS_CGIDNoExists=1
											UNION
											SELECT T1.ProgrammeNumber,T1.CGIDNumber
												,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
												,T1.CGSex,T1.CGDateOfBirth
												,NULL AS IPRS_CGIDNo
												,NULL AS IPRS_CGName
												,NULL AS IPRS_CGSex,NULL AS IPRS_CGDoB
												,T1.IPRS_CGIDNoExists
												,T1.IPRS_CGFirstNameExists
												,T1.IPRS_CGMiddleNameExists
												,T1.IPRS_CGSurnameExists
												,T1.IPRS_CGDoBMatches
												,T1.IPRS_CGDoBYearMatches
												,T1.IPRS_CGSexMatches
											FROM ActiveOPCTBeneficiaries T1
											WHERE T1. IPRS_CGIDNoExists=0
											) T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
WHERE IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)=2 AND IPRS_SexMatches=0
UNION
SELECT T1.ProgrammeNumber,T1.IDNumber
	,T1.Name
	,T1.Sex,T1.DateOfBirth
	,T1.IPRS_IDNo
	,T1.IPRS_Name
	,T1.IPRS_Sex,T1.IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
	,T2.CGIDNumber
	,T2.CGName
	,T2.CGSex,T2.CGDateOfBirth
	,T2.IPRS_CGIDNo
	,T2.IPRS_CGName
	,T2.IPRS_CGSex,T2.IPRS_CGDoB
	,T2.IPRS_CGIDNoExists
	,T2.IPRS_CGFirstNameExists
	,T2.IPRS_CGMiddleNameExists
	,T2.IPRS_CGSurnameExists
	,T2.IPRS_CGDoBMatches
	,T2.IPRS_CGDoBYearMatches
	,T2.IPRS_CGSexMatches
FROM (
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,T2.IDNumber AS IPRS_IDNo
			,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
			,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
		WHERE T1. IPRS_IDNoExists=1
		UNION
		SELECT T1.ProgrammeNumber,T1.IDNumber
			,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
			,T1.Sex,T1.DateOfBirth
			,NULL AS IPRS_IDNo
			,NULL AS IPRS_Name
			,NULL AS IPRS_Sex,NULL AS IPRS_DoB
			,T1.IPRS_IDNoExists
			,T1.IPRS_FirstNameExists
			,T1.IPRS_MiddleNameExists
			,T1.IPRS_SurnameExists
			,T1.IPRS_DoBMatches
			,T1.IPRS_DoBYearMatches
			,T1.IPRS_SexMatches
		FROM ActiveOPCTBeneficiaries T1
		WHERE T1. IPRS_IDNoExists=0
	) T1 INNER JOIN (
						SELECT T1.ProgrammeNumber,T1.CGIDNumber
							,ISNULL(T1.CGFirstName,'')+CASE WHEN(ISNULL(T1.CGMiddleName,'')='') THEN '' ELSE ' '+T1.CGMiddleName END+CASE WHEN(ISNULL(T1.CGSurname,'')='') THEN '' ELSE ' '+T1.CGSurname END AS CGName
							,T1.CGSex,T1.CGDateOfBirth
							,T2.IDNumber AS IPRS_CGIDNo
							,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_CGName
							,T2.Sex AS IPRS_CGSex,T2.DoB AS IPRS_CGDoB
							,T1.IPRS_CGIDNoExists
							,T1.IPRS_CGFirstNameExists
							,T1.IPRS_CGMiddleNameExists
							,T1.IPRS_CGSurnameExists
							,T1.IPRS_CGDoBMatches
							,T1.IPRS_CGDoBYearMatches
							,T1.IPRS_CGSexMatches
						FROM ActiveOPCTBeneficiaries T1 LEFT JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedCGIDNo)=T2.IDNumber
						WHERE  IPRS_CGIDNoExists=1 AND CONVERT(tinyint,T1.IPRS_CGFirstNameExists)+CONVERT(tinyint,T1.IPRS_CGMiddleNameExists)+CONVERT(tinyint,T1.IPRS_CGSurnameExists)=2 AND IPRS_CGSexMatches=0
					) T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
ORDER BY T1.ProgrammeNumber
