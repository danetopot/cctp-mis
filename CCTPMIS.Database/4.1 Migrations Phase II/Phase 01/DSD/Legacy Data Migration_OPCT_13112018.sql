/*


ALTER TABLE Household
	ALTER COLUMN Village varchar(100)
GO

ALTER TABLE Household
	ADD SourceId tinyint
GO

ALTER TABLE Person
	ADD SourceId tinyint
GO


UPDATE T1
SET T1.SourceId=1
FROM Household T1
GO


UPDATE T1
SET T1.SourceId=2
FROM Household T1
WHERE RegGroupId=195
GO


UPDATE T1
SET T1.SourceId=1
FROM Person T1


INSERT INTO SystemCodeDetail(SystemCodeId,Code,[Description],OrderNo,CreatedBy,CreatedOn)
SELECT 17,LTRIM(RIGHT(T1.RelationshipName,LEN(T1.RelationshipName)-CHARINDEX('.',T1.RelationshipName,0))) AS Code,LTRIM(RIGHT(T1.RelationshipName,LEN(T1.RelationshipName)-CHARINDEX('.',T1.RelationshipName,0))) AS RelationshipName,ROW_NUMBER() OVER(ORDER BY RelationshipId)+2,1,GETDATE()
FROM [DCS].dbo.Relationship T1
WHERE T1.RelationshipName NOT LIKE '%Caregiver'


alter table ActiveCTOVCBeneficiaries
	add TargetingId int
   ,TargetingDetailId int
   ,Exited bit DEFAULT(0)
 


  SELECT T2.TargetingId,T2.[Status],CASE WHEN(T2.[Status] IN (18,33)) THEN 0 ELSE 1 END,T1.*
  --UPDATE T1 SET T1.TargetingId=T2.TargetingId,T1.Exited=CASE WHEN(T2.[Status] IN (18,33)) THEN 0 ELSE 1 END
  FROM ActiveCTOVCBeneficiaries T1 INNER JOIN [DCS].dbo.TargetingHeader T2 ON T1.ProgrammeNumber=T2.ProgrammeNumber
  ----WHERE T2.[Status]=91

UPDATE T5 SET T5.TargetingDetailId=ISNULL(T2.TargetingDetailId,T3.TargetingDetailId)
FROM TargetingHeader T1 LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
									FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																		FROM TargetingDetail
																		WHERE RelationshipId=1 AND ISNULL(Exited,0)=0
																		GROUP BY TargetingId
																		) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
										) T2 ON T1.TargetingId=T2.TargetingId
						LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
									FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																		FROM TargetingDetail
																		WHERE (RelationshipId<>1 AND CanReceivePayment=1 AND ISNULL(Exited,0)=0)
																		GROUP BY TargetingId
																		) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId									
									) T3 ON T1.TargetingId=T3.TargetingId
						INNER JOIN (
								SELECT T1.TargetingId
								FROM PaymentDetail T1 INNER JOIN PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
								WHERE T2.CycleId=73
							) T4 ON T1.TargetingId=T4.TargetingId
						INNER JOIN [LegacyDB].dbo.ActiveCTOVCBeneficiaries T5 ON T1.TargetingId=T5.TargetingId



update t1 set t1.programmeid=2 from [LegacyDB].dbo.HhRegistration t1
update t1 set t1.sourceprogrammeid=2 from [LegacyDB].dbo.CaseGrievance t1
update t1 set t1.programmeid=2 from [LegacyDB].dbo.CaseGrievance t1 where t1.Programme='CT-OVC'
update t1 set t1.sourceprogrammeid=2 from [LegacyDB].dbo.CaseUpdate t1

*/



DECLARE @SysCode varchar(30)
	   ,@SysDetailCode varchar(30)
DECLARE @SysCodeDetailId1 int
	   ,@SysCodeDetailId2 int
	   ,@SysCodeDetailId3 int
	   ,@SysCodeDetailId4 int
	   ,@SysCodeDetailId5 int
	   ,@SysCodeDetailId6 int
	   ,@SysCodeDetailId7 int
DECLARE @MIG_STAGE tinyint


SET @MIG_STAGE = 8




--=====================================	HOUSEHOLD	====================================
IF @MIG_STAGE=1
BEGIN
	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Code='OPCT')
	BEGIN
		SET @SysCode='Beneficiary Type'
		SET @SysDetailCode='INDIVIDUAL'
		SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='BENEFICIARY'
		SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		SET @SysDetailCode='CAREGIVER'
		SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Payment Frequency'
		SET @SysDetailCode='BIMONTHLY'
		SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO Programme(Code,Name,BeneficiaryTypeId,IsActive,PrimaryRecipientId,SecondaryRecipientId,SecondaryRecipientMandatory,BeneProgNoPrefix,PaymentFrequencyId,CreatedBy,CreatedOn)
		SELECT 'OPCT' AS Code,'Older Persons Cash Transfer Programme' AS Name,@SysCodeDetailId1 AS BeneficiaryTypeId,1 AS IsActive,@SysCodeDetailId2 AS PrimaryRecipientId,@SysCodeDetailId3 AS SecondaryRecipientId,0 AS SecondaryRecipientMandatory,1 AS BeneProgNoPrefix,@SysCodeDetailId4 AS PaymentFrequencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	END

	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy OPCT MIS'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
		EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries',@OrderNo=2,@UserId=1;

	SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='HHStatus'
	SET @SysDetailCode='VALPASS'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='VALFAIL'
	SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId3=Id FROM Programme WHERE Code='OPCT'
	
	--INSERT INTO Household(ProgrammeId,RegGroupId,RefId,Village,StatusId,SourceId,CreatedBy,CreatedOn)
	SELECT @SysCodeDetailId3 AS ProgrammeId,@SysCodeDetailId2,T2.Id AS RefId,T2.Village,@SysCodeDetailId1,2 AS SourceId,1,GETDATE()
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId3
												   LEFT JOIN Household T3 ON T3.ProgrammeId=@SysCodeDetailId3 AND T3.RegGroupId=@SysCodeDetailId2 AND T3.SourceId=2 AND T3.RefId=T2.Id
	WHERE T1.Exited=0 AND T1.IPRS_IDNoExists=1 AND (T1.IPRS_CGIDNoExists=1 OR ISNULL(T1.CGIDNumber,'')='') AND T3.Id IS NULL
	UNION
	SELECT @SysCodeDetailId3 AS ProgrammeId,@SysCodeDetailId2,T2.Id,T2.Village,@SysCodeDetailId4,2 AS SourceId,1,GETDATE()
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId3
													LEFT JOIN Household T3 ON T3.ProgrammeId=@SysCodeDetailId3 AND T3.RegGroupId=@SysCodeDetailId2 AND T3.SourceId=2 AND T3.RefId=T2.Id
	WHERE T1.Exited=0 AND (T1.IPRS_IDNoExists=0 OR (T1.IPRS_CGIDNoExists=0 AND ISNULL(T1.CGIDNumber,'')<>''))AND T3.Id IS NULL
	ORDER BY RefId
END



--=====================================	HOUSEHOLD SUBLOCATION	====================================
IF @MIG_STAGE=2
BEGIN
	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy OPCT MIS'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId2=Id FROM Programme WHERE Code='OPCT'

	--INSERT INTO HouseholdSubLocation(HhId,GeoMasterId,SubLocationId)
	SELECT T3.Id AS HhId,1 AS GeoMasterId,T4.Id AS SubLocationId
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiaries T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
												   INNER JOIN Household T3 ON T3.ProgrammeId=@SysCodeDetailId2 AND T3.RegGroupId=@SysCodeDetailId1 AND T3.SourceId=2 AND T2.Id=T3.RefId
												   INNER JOIN Sublocation T4 ON T1.SubLocationCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
												   LEFT JOIN HouseholdSubLocation T5 ON T3.Id=T5.HhId AND T5.GeoMasterId=1 AND T4.Id=T5.SubLocationId
	WHERE T5.HhId IS NULL
END



--=====================================	HOUSEHOLD ENROLMENT	====================================
IF @MIG_STAGE=4
BEGIN
	SET @SysCode='Enrolment Group'
	SET @SysDetailCode='Legacy MIS'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
		EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy MIS Beneficiaries',@OrderNo=2,@UserId=1;

	--SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	--INSERT INTO HouseholdEnrolment(HhId,ProgrammeNo,EnrolmentGroupId,CreatedBy,CreatedOn,ApvBy,ApvOn)
	--SELECT T2.Id AS HhId,T2.Id AS ProgrammeNo,@SysCodeDetailId2 AS TargetGroupId,1 AS CreatedBy,GETDATE() AS CreatedOn,1 AS ApvBy,GETDATE() AS ApvOn
	--FROM vw_70PlusRegistration T1 INNER JOIN Household T2 ON T1.RefId=T2.RefId
END



--=====================================	PERSON	====================================
IF @MIG_STAGE=5
BEGIN
	DECLARE @DEFAULT_DoB datetime
	SET @DEFAULT_DoB='15 Sep 1989'	--DEFAULTING ANY MISSING Date Of Birth
	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy OPCT MIS'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId2=Id FROM Programme WHERE Code='OPCT'

	--INSERT INTO Person(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,SourceId,CreatedBy,CreatedOn)
	--MAPPED BENE
	SELECT T3.Id AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),T3.BirthCertNo,T1.ResolvedIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
														  INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN SystemCodeDetail T5 ON ISNULL(T1.Sex,T3.Sex)=T5.Code
														  INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
														  LEFT JOIN Person T7 ON T7.SourceId=2 AND T3.Id=T7.RefId
	WHERE ISNULL(T3.IsExited,0)=0 AND T7.Id IS NULL
	--MISSING BENE
	UNION
	SELECT NULL AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),NULL AS BirthCertNo,T1.ResolvedIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
												   LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
												   INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
												   INNER JOIN SystemCodeDetail T5 ON CASE WHEN(ISNULL(T1.Sex,'M')='F') THEN 'F' ELSE 'M' END=T5.Code
												   INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
												   LEFT JOIN Person T7 ON T7.SourceId=2 AND T1.ResolvedIDNo=T7.NationalIdNo AND T7.RefId IS NULL 
	WHERE ISNULL(T3.IsExited,0)=0 AND ISNULL(T1.MemberCode,0)<=0 AND T7.Id IS NULL
	--MAPPED CG
	UNION
	SELECT T3.Id AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),T3.BirthCertNo,T1.ResolvedCGIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
													      INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN SystemCodeDetail T5 ON ISNULL(T1.Sex,T3.Sex)=T5.Code
													      INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
													      LEFT JOIN Person T7 ON T7.SourceId=2 AND T3.Id=T7.RefId
	WHERE ISNULL(T3.IsExited,0)=0 AND T1.ResolvedCGIDNo<>'' AND ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND T7.Id IS NULL
	--MISSING CG
	UNION
	SELECT NULL AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),NULL AS BirthCertNo,T1.ResolvedCGIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
														  LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN SystemCodeDetail T5 ON CASE WHEN(ISNULL(T1.CGSex,'M')='F') THEN 'F' ELSE 'M' END=T5.Code
														  INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
														  LEFT JOIN Person T7 ON T7.SourceId=2 AND ISNULL(T1.ResolvedCGIDNo,'')=ISNULL(T7.NationalIdNo,'') AND T7.RefId IS NULL
	WHERE ISNULL(T3.IsExited,0)=0 AND ISNULL(T1.CGMemberCode,0)<=0 AND T1.ResolvedCGIDNo<>'' AND ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND T7.Id IS NULL
	ORDER BY RefId
END




--=====================================	MEMBER RELATIONSHIP	====================================
IF @MIG_STAGE=8
BEGIN
	SET @SysCode='Relationship'
	SET @SysDetailCode='BENEFICIARY'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='CAREGIVER'
	SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Role'
	SET @SysDetailCode='BENEFICIARY'
	SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='CAREGIVER'
	SELECT @SysCodeDetailId5=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy OPCT MIS'
	SELECT @SysCodeDetailId6=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId7=Id FROM Programme WHERE Code='OPCT'

	--INSERT INTO HouseholdMember(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId1 AS RelationshipId,@SysCodeDetailId4 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
												   INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
												   INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
												   INNER JOIN Person T5 ON T5.SourceId=2 AND T3.Id=T5.RefId
												   LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE ISNULL(T3.IsExited,0)=0 AND T6.Id IS NULL
	UNION
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId1 AS RelationshipId,@SysCodeDetailId4 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
														  LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN Person T5 ON T5.SourceId=2 AND T1.ResolvedIDNo=T5.NationalIdNo AND T5.RefId IS NULL AND T1.FirstName=T5.FirstName
														  LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE ISNULL(T3.IsExited,0)=0 AND ISNULL(T1.MemberCode,0)<=0 AND T6.Id IS NULL
	UNION
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId2 AS RelationshipId,@SysCodeDetailId5 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
														  INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN Person T5 ON T5.SourceId=2 AND T3.Id=T5.RefId
														  LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE ISNULL(T3.IsExited,0)=0 AND ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND  T6.Id IS NULL
	UNION
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId2 AS RelationshipId,@SysCodeDetailId5 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActiveOPCTBeneficiariesUPDATED T1 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
													      LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN Person T5 ON T5.SourceId=2 AND ISNULL(T1.ResolvedCGIDNo,'')=ISNULL(T5.NationalIdNo,'') AND T5.RefId IS NULL AND T1.FirstName=T5.FirstName AND T1.MiddleName=T5.MiddleName AND T1.Surname=T5.Surname
													      LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE ISNULL(T3.IsExited,0)=0 AND ISNULL(T1.CGMemberCode,0)<=0AND ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND T6.Id IS NULL

	----SET @SysCodeDetailId1=1
	----WHILE(@SysCodeDetailId1<@MIG_STAGE)
	----BEGIN
	----	UPDATE T1
	----	SET T1.FirstName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.FirstName,'u','a'),'o','e'),'i','u'),'e','a'),'a','i')
	----	   ,T1.MIddleName=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.MIddleName,'u','a'),'o','e'),'i','u'),'e','a'),'a','i')
	----	   ,T1.Surname=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.Surname,'u','a'),'o','e'),'i','u'),'e','a'),'a','i')
	----	   ,T1.BirthCertNo=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.BirthCertNo,'1','6'),'2','8'),'3','4'),'4','1'),'5','3'),'6','2'),'7','1'),'8','4'),'9','0')
	----	   ,T1.NationalIdNo=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.NationalIdNo,'1','6'),'2','8'),'3','4'),'4','1'),'5','3'),'6','2'),'7','1'),'8','4'),'9','0')
	----	   ,T1.MobileNo1=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.MobileNo1,'1','6'),'2','8'),'3','4'),'4','1'),'5','3'),'6','2'),'7','1'),'8','4'),'9','0')
	----	   ,T1.MobileNo2=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(T1.MobileNo2,'1','6'),'2','8'),'3','4'),'4','1'),'5','3'),'6','2'),'7','1'),'8','4'),'9','0')
	----	FROM Person T1

	----	SET @SysCodeDetailId1=@SysCodeDetailId1+1
	----END
END





