	

DECLARE @AnalysisStep tinyint 
DECLARE @CycleId int

SET @AnalysisStep=8
SET @CycleId=73

DECLARE @MIN_OVC_DOB datetime

SET @MIN_OVC_DOB=DATEADD(MM,-((SELECT TOP 1 MaximumOVCAge FROM [DCS].dbo.Project)*12),GETDATE())	--FOR A MIN OF OVC YEARS AND BELOW

	SELECT T1.* INTO ActiveCTOVCBeneficiaries
	FROM (
		SELECT T1.*,REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.IDNo,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') AS ResolvedIDNo,CASE WHEN(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(T1.IDNo,''),CHAR(9),''),CHAR(10),''),CHAR(13),''),' ','') LIKE '%[^0-9]%') THEN 1 ELSE 0 END AS HasInvalid
		FROM (
				SELECT T1.ProgrammeNumber,ISNULL(T2.FirstName,T3.FirstName) AS FirstName,ISNULL(T2.MiddleName,T3.MiddleName) AS MiddleName,ISNULL(T2.Surname,T3.Surname) AS Surname,ISNULL(T2.Sex,T3.Sex) AS Sex,ISNULL(T2.DateOfBirth,T3.DateOfBirth) AS DateOfBirth,ISNULL(T2.Age,T3.Age) AS Age,ISNULL(T2.IDNumber,T3.IDNumber) AS IDNo,ISNULL(T4.NoOfHouseholdMembers,0) AS NoOfHouseholdMembers,ISNULL(T4.NoOfMaleOVC,0) AS NoOfMaleOVC,ISNULL(T4.NoOfFemaleOVC,0) AS NoOfFemaleOVC
					,T5.CountyName,T5.DistrictName,T5.DivisionName,T5.LocationName,T5.SubLocationName,T5.ConstituencyName--,CONVERT(bit,ISNULL(T6.TargetingId,0)) AS EverBeenPaid
				FROM [DCS].dbo.TargetingHeader T1 LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,[DCS].dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
												   FROM [DCS].dbo.TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																						FROM [DCS].dbo.TargetingDetail
																						WHERE RelationshipId=1 AND ISNULL(Exited,0)=0
																						GROUP BY TargetingId
																						) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
														) T2 ON T1.TargetingId=T2.TargetingId
											LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,[DCS].dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
													   FROM [DCS].dbo.TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																							FROM [DCS].dbo.TargetingDetail
																							WHERE (RelationshipId<>1 AND CanReceivePayment=1 AND ISNULL(Exited,0)=0)
																							GROUP BY TargetingId
																							) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
														) T3 ON T1.TargetingId=T3.TargetingId
											LEFT JOIN (SELECT TT1.TargetingId,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex='F') THEN 1 ELSE 0 END) AS NoOfFemaleOVC,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex<>'F') THEN 1 ELSE 0 END) AS NoOfMaleOVC,COUNT(TT1.TargetingDetailId) AS NoOfHouseholdMembers
													   FROM [DCS].dbo.TargetingDetail TT1 LEFT JOIN (SELECT TargetingDetailId
																						   FROM [DCS].dbo.TargetingDetail T1
																						   WHERE T1.DateOfBirth>@MIN_OVC_DOB AND ISNULL(T1.Exited,0)=0
																						   ) TT2 ON TT1.TargetingDetailId=TT2.TargetingDetailId
													   WHERE ISNULL(TT1.Exited,0)=0
													   GROUP BY TT1.TargetingId
													   ) T4 ON T1.TargetingId=T4.TargetingId
											LEFT JOIN (
														SELECT T1.SubLocationId,T1.SubLocationName,T2.LocationName,T3.DivisionName,T4.DistrictName,T5.CountyName,T6.ConstituencyName
														FROM [DCS].dbo.SubLocation T1 INNER JOIN [DCS].dbo.Location T2 ON T1.LocationId=T2.LocationId
																			INNER JOIN [DCS].dbo.Division T3 ON T2.DivisionId=T3.DivisionId
																			INNER JOIN [DCS].dbo.District T4 ON T3.DistrictId=T4.DistrictId
																			INNER JOIN [DCS].dbo.County T5 ON T4.CountyId=T5.CountyId
																			INNER JOIN [DCS].dbo.Constituency T6 ON T1.ConstituencyId=T6.ConstituencyId
													) T5 ON T1.SubLocationId=T5.SubLocationId
											INNER JOIN (
														SELECT T1.TargetingId
														FROM [DCS].dbo.PaymentDetail T1 INNER JOIN [DCS].dbo.PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
														WHERE T2.CycleId=@CycleId
													) T6 ON T1.TargetingId=T6.TargetingId
											--INNER JOIN (
											--			SELECT T1.TargetingId
											--			FROM PaymentDetail T1 INNER JOIN PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
											--			WHERE T2.CycleId=73
											--		) T7 ON T1.TargetingId=T7.TargetingId
					WHERE T1.[Status] IN(18,33) --AND T6.TargetingId IS NULL
				) T1
			) T1
			




select * 
from paymentdetail t1 inner join paymentheader t2 on t1.paymentid=t2.paymentid

select * from ActiveCTOVCBeneficiaries where hasinvalid=1

select * from activectovcbeneficiaries where resolvedidno='2152.9697'

select * from IPRS_CTOVC_CG where idnumber IN(508804,50713,4914508,20358752,134659)

SELECT COUNT(T1.ProgrammeNumber)
FROM ActiveCTOVCBeneficiaries T1 INNER JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE hasinvalid=0 --T2.ProgrammeNumber IS NULL
--ORDER BY T1.ProgrammeNumber
--238046

SELECT COUNT(T1.ProgrammeNumber)
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE hasinvalid=0 AND T2.IDNumber IS NULL
--ORDER BY T1.ProgrammeNumber
--102368



SELECT *
FROM IPRS_CTOVC_CG T1
WHERE T1.IDNumber IN(select idnumber from IPRS_CTOVC_CG group by idnumber having count(ProgrammeNumber)>1)

select ResolvedIDNo from ActiveCTOVCBeneficiaries group by ResolvedIDNo having count(ProgrammeNumber)>1

select * from IPRS_CTOVC_CG where isnull(idnumber,'')<>''

select * from IPRS_CTOVC

IF NOT OBJECT_ID('IPRS_CTOVC') IS NULL	DROP VIEW IPRS_CTOVC
GO
CREATE VIEW IPRS_CTOVC
AS
	SELECT IDNumber,ISNULL(FirstName,'') AS FirstName,ISNULL(MiddleName,'') AS MiddleName,ISNULL(Surname,'') AS Surname,ISNULL(Gender,'') AS Sex,MIN(DateOfBirth) AS DoB
	FROM [LegacyDB].dbo.IPRS_CTOVC_CG
	WHERE ISNULL(IDNumber,'')<>'' --AND IDNumber NOT IN(508804,50713,4914508,20358752,134659)
	GROUP BY IDNumber,ISNULL(FirstName,''),ISNULL(MiddleName,''),ISNULL(Surname,''),ISNULL(Gender,'')
GO






SELECT t1.*
FROM ActiveCTOVCBeneficiaries T1 INNER JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE hasinvalid=0 --T2.ProgrammeNumber IS NULL
--ORDER BY T1.ProgrammeNumber
--238046

--MISSING ON IPRS
SELECT T1.*
FROM ActiveCTOVCBeneficiaries T1
WHERE ISNUMERIC(T1.ResolvedIDNo)=0
UNION
SELECT T1.*
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE ISNUMERIC(T1.ResolvedIDNo)=1 AND T2.IDNumber IS NULL
ORDER BY T1.ProgrammeNumber


alter table ActiveCTOVCBeneficiaries
	add IPRS_IDNoExists bit
	,IPRS_FirstNameExists bit
	,IPRS_MiddleNameExists bit
	,IPRS_SurnameExists bit
	,IPRS_DoBMatches bit
	,IPRS_DoBYearMatches bit
	,IPRS_SexMatches bit

,CONVERT(bit,ISNULL(T2.IDNo,0)) AS IPRS_IDNoExists
		,CASE WHEN(T1.BeneFirstName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_FirstNameExists
		,CASE WHEN(T1.BeneMiddleName IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_MiddleNameExists
		,CASE WHEN(T1.BeneSurname IN(T2.FirstName,T2.MiddleName,T2.Surname)) THEN 1 ELSE 0 END AS IPRS_BeneSurnameExists
		,CASE WHEN(T1.BeneDoB=T2.DoB) THEN 1 ELSE 0 END AS IPRS_DoBMatches
		,CASE WHEN(YEAR(T1.BeneDoB)=YEAR(T2.DoB)) THEN 1 ELSE 0 END AS IPRS_DoBYearMatches
		,CASE WHEN(T1.BeneSex=T2.Sex) THEN 1 ELSE 0 END AS IPRS_SexMatches

UPDATE T1
SET T1.IPRS_IDNoExists=0
   ,T1.IPRS_FirstNameExists=0
   ,T1.IPRS_MiddleNameExists=0
   ,T1.IPRS_SurnameExists=0
   ,T1.IPRS_DoBMatches=0
   ,T1.IPRS_DoBYearMatches=0
   ,T1.IPRS_SexMatches=0
FROM ActiveCTOVCBeneficiaries T1


SELECT t1.DateOfBirth,CONVERT(bit,ISNULL(T2.IDNumber,0)) IPRS_IDNoExists
	,CASE WHEN(ISNULL(T1.FirstName,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END IPRS_FirstNameExists
	,CASE WHEN(ISNULL(T1.MiddleName,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END IPRS_MiddleNameExists
	,CASE WHEN(ISNULL(T1.Surname,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END IPRS_SurnameExists
	,CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(T1.DateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_DoBMatches
	,CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(YEAR(T1.DateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END AS IPRS_DoBYearMatches
	,CASE WHEN(T1.Sex=T2.Sex) THEN 1 ELSE 0 END AS IPRS_SexMatches
UPDATE T1
SET T1.IPRS_IDNoExists=CONVERT(bit,ISNULL(T2.IDNumber,0))
   ,T1.IPRS_FirstNameExists=CASE WHEN(ISNULL(T1.FirstName,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END
   ,T1.IPRS_MiddleNameExists=CASE WHEN(ISNULL(T1.MiddleName,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END
   ,T1.IPRS_SurnameExists=CASE WHEN(ISNULL(T1.Surname,'') COLLATE DATABASE_DEFAULT IN(T2.FirstName COLLATE DATABASE_DEFAULT,T2.MiddleName COLLATE DATABASE_DEFAULT,T2.Surname COLLATE DATABASE_DEFAULT)) THEN 1 ELSE 0 END
   ,T1.IPRS_DoBMatches=CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(T1.DateOfBirth=T2.DoB) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_DoBYearMatches=CASE WHEN(ISDATE(T1.DateOfBirth)=1) THEN CASE WHEN(YEAR(T1.DateOfBirth)=YEAR(T2.DoB)) THEN 1 ELSE 0 END ELSE 0 END
   ,T1.IPRS_SexMatches=CASE WHEN(T1.Sex=T2.Sex) THEN 1 ELSE 0 END
FROM ActiveCTOVCBeneficiaries T1 INNER JOIN IPRS_Legacy T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE HasInvalid=0


set dateformat dmy


SELECT ISNUMERIC('2152.9697')
select convert(datetime,'31/12/1979')




select count(programmenumber) from ActiveCTOVCBeneficiaries where hasinvalid=0
select count(programmenumber) from ActiveCTOVCBeneficiaries where hasinvalid=1



SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1. IPRS_IDNoExists=1
UNION
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,NULL AS IPRS_IDNo
	,NULL AS IPRS_Name
	,NULL AS IPRS_Sex,NULL AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1
WHERE T1. IPRS_IDNoExists=0
ORDER BY T1.ProgrammeNumber

--TOTAL NAME MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1. IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1	

--TOTAL DoB MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1. IPRS_IDNoExists=1 AND T1.IPRS_DoBMatches=0 AND T1.IPRS_DoBYearMatches=0


--PARTIAL NAME MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE T1. IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)=2
	

--PARTIAL DoB MISMATCH
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE IPRS_IDNoExists=1 AND T1.IPRS_DoBMatches=0 AND T1.IPRS_DoBYearMatches=1


--SEX MISMATCH 
SELECT T1.ProgrammeNumber,T1.IDNo
	,ISNULL(T1.FirstName,'')+CASE WHEN(ISNULL(T1.MiddleName,'')='') THEN '' ELSE ' '+T1.MiddleName END+CASE WHEN(ISNULL(T1.Surname,'')='') THEN '' ELSE ' '+T1.Surname END AS Name
	,T1.Sex,T1.DateOfBirth
	,T2.IDNumber AS IPRS_IDNo
	,ISNULL(T2.FirstName,'')+CASE WHEN(ISNULL(T2.MiddleName,'')='') THEN '' ELSE ' '+T2.MiddleName END+CASE WHEN(ISNULL(T2.Surname,'')='') THEN '' ELSE ' '+T2.Surname END AS IPRS_Name
	,T2.Sex AS IPRS_Sex,T2.DoB AS IPRS_DoB
	,T1.IPRS_IDNoExists
	,T1.IPRS_FirstNameExists
	,T1.IPRS_MiddleNameExists
	,T1.IPRS_SurnameExists
	,T1.IPRS_DoBMatches
	,T1.IPRS_DoBYearMatches
	,T1.IPRS_SexMatches
FROM ActiveCTOVCBeneficiaries T1 LEFT JOIN IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
WHERE IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)=2 AND IPRS_SexMatches=0




select IPRS_IDNoExists,count(programmenumber) from ActiveCTOVCBeneficiaries group by IPRS_IDNoExists
0	102380
1	238036

select * from IPRS_CTOVC where idnumber='508804'


select idnumber from IPRS_CTOVC group by idnumber having count(firstname)>1

