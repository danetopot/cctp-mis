

DECLARE @AnalysisStep tinyint 
DECLARE @CycleId int

SET @AnalysisStep=5
SET @CycleId=73

DECLARE @MIN_OVC_DOB datetime

SET @MIN_OVC_DOB=DATEADD(MM,-((SELECT TOP 1 MaximumOVCAge FROM Project)*12),'15 Sep 2018')	--FOR A MIN OF OVC YEARS AND BELOW


--INSERT INTO HouseholdStatus(StatusId,StatusName,[Description])
--SELECT 99 AS StatusId,'Temp ID Exceptions' AS StatusName,'Temp ID Exceptions' AS [Description]

--SELECT 90 AS StatusId,'Exited Jul-Aug 2018 Prepayroll Exceptions' AS StatusName,'Exited Jul-Aug 2018 prepayroll exceptions prior to data migration into the CCTP MIS' AS [Description]
--UNION
--SELECT 91 AS StatusId,'Exited Total IPRS Mismatch Exception' AS StatusName,'Exited total IPRS mismatch exception prior to data migration into the CCTP MIS' AS [Description]


--3. HOUSEHOLDS WITHOUT OVCs
IF @AnalysisStep=3
BEGIN
	--UPDATE T1 SET T1.[Status]=90
	SELECT T1.ProgrammeNumber,ISNULL(T2.FirstName,'') AS FirstName,ISNULL(T2.MiddleName,'') AS MiddleName,ISNULL(T2.Surname,'') AS Surname,ISNULL(T2.Sex,'') AS Sex,T2.DateOfBirth AS DateOfBirth,T2.Age AS Age,T2.IDNumber AS IDNo,ISNULL(T4.NoOfHouseholdMembers,0) AS NoOfHouseholdMembers,ISNULL(T4.NoOfMaleOVC,0) AS NoOfMaleOVC,ISNULL(T4.NoOfFemaleOVC,0) AS NoOfFemaleOVC
		,T5.CountyName,T5.DistrictName,T5.DivisionName,T5.LocationName,T5.SubLocationName,T5.ConstituencyName--,CONVERT(bit,ISNULL(T6.TargetingId,0)) AS EverBeenPaid
	FROM TargetingHeader T1 LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
									   FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																			FROM TargetingDetail
																			WHERE RelationshipId=1 AND ISNULL(Exited,0)=0
																			GROUP BY TargetingId
																			) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
											) T2 ON T1.TargetingId=T2.TargetingId
								LEFT JOIN (SELECT TT1.TargetingId,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex='F') THEN 1 ELSE 0 END) AS NoOfFemaleOVC,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex<>'F') THEN 1 ELSE 0 END) AS NoOfMaleOVC,COUNT(TT1.TargetingDetailId) AS NoOfHouseholdMembers
										   FROM TargetingDetail TT1 LEFT JOIN (SELECT TargetingDetailId
																			   FROM TargetingDetail T1
																			   WHERE T1.DateOfBirth>@MIN_OVC_DOB --AND ISNULL(T1.Exited,0)=0
																			   ) TT2 ON TT1.TargetingDetailId=TT2.TargetingDetailId
										   --WHERE ISNULL(TT1.Exited,0)=0
										   GROUP BY TT1.TargetingId
										   ) T4 ON T1.TargetingId=T4.TargetingId
								LEFT JOIN (
											SELECT T1.SubLocationId,T1.SubLocationName,T2.LocationName,T3.DivisionName,T4.DistrictName,T5.CountyName,T6.ConstituencyName
											FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.LocationId
																INNER JOIN Division T3 ON T2.DivisionId=T3.DivisionId
																INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
																INNER JOIN County T5 ON T4.CountyId=T5.CountyId
																INNER JOIN Constituency T6 ON T1.ConstituencyId=T6.ConstituencyId
										) T5 ON T1.SubLocationId=T5.SubLocationId
								LEFT JOIN (
											SELECT DISTINCT T1.TargetingId
											FROM PaymentDetail T1 INNER JOIN PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
											WHERE T2.CycleId IN(@CycleId)
										) T6 ON T1.TargetingId=T6.TargetingId
		WHERE T1.[Status] IN(18,33) AND ISNULL(T4.NoOfMaleOVC,0)<=0 AND ISNULL(T4.NoOfFemaleOVC,0)<=0 AND T6.TargetingId IS NULL
		--GROUP BY T5.CountyName
		ORDER BY T5.CountyName,T5.DistrictName,T5.DivisionName,T5.LocationName,T5.SubLocationName,T5.ConstituencyName,T1.ProgrammeNumber
END



--4. HOUSEHOLDS WITHOUT CGs
IF @AnalysisStep=4
BEGIN
	--UPDATE T1 SET T1.[Status]=90
	SELECT T1.ProgrammeNumber,ISNULL(T2.FirstName,'') AS FirstName,ISNULL(T2.MiddleName,'') AS MiddleName,ISNULL(T2.Surname,'') AS Surname,ISNULL(T2.Sex,'') AS Sex,T2.DateOfBirth AS DateOfBirth,T2.Age AS Age,T2.IDNumber AS IDNo,ISNULL(T4.NoOfHouseholdMembers,0) AS NoOfHouseholdMembers,ISNULL(T4.NoOfMaleOVC,0) AS NoOfMaleOVC,ISNULL(T4.NoOfFemaleOVC,0) AS NoOfFemaleOVC
		,T5.CountyName,T5.DistrictName,T5.DivisionName,T5.LocationName,T5.SubLocationName,T5.ConstituencyName--,CONVERT(bit,ISNULL(T6.TargetingId,0)) AS EverBeenPaid
	FROM TargetingHeader T1 LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
									   FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																			FROM TargetingDetail
																			WHERE RelationshipId=1 AND ISNULL(Exited,0)=0
																			GROUP BY TargetingId
																			) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
											) T2 ON T1.TargetingId=T2.TargetingId
								LEFT JOIN (SELECT TT1.TargetingId,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex='F') THEN 1 ELSE 0 END) AS NoOfFemaleOVC,SUM(CASE WHEN(TT2.TargetingDetailId IS NOT NULL AND TT1.Sex<>'F') THEN 1 ELSE 0 END) AS NoOfMaleOVC,COUNT(TT1.TargetingDetailId) AS NoOfHouseholdMembers
										   FROM TargetingDetail TT1 LEFT JOIN (SELECT TargetingDetailId
																			   FROM TargetingDetail T1
																			   WHERE T1.DateOfBirth>@MIN_OVC_DOB AND ISNULL(T1.Exited,0)=0
																			   ) TT2 ON TT1.TargetingDetailId=TT2.TargetingDetailId
										   WHERE ISNULL(TT1.Exited,0)=0
										   GROUP BY TT1.TargetingId
										   ) T4 ON T1.TargetingId=T4.TargetingId
								LEFT JOIN (
											SELECT T1.SubLocationId,T1.SubLocationName,T2.LocationName,T3.DivisionName,T4.DistrictName,T5.CountyName,T6.ConstituencyName
											FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.LocationId
																INNER JOIN Division T3 ON T2.DivisionId=T3.DivisionId
																INNER JOIN District T4 ON T3.DistrictId=T4.DistrictId
																INNER JOIN County T5 ON T4.CountyId=T5.CountyId
																INNER JOIN Constituency T6 ON T1.ConstituencyId=T6.ConstituencyId
										) T5 ON T1.SubLocationId=T5.SubLocationId
								LEFT JOIN (
											SELECT DISTINCT T1.TargetingId
											FROM PaymentDetail T1 INNER JOIN PaymentHeader T2 ON T1.PaymentId=T2.PaymentId
											WHERE T2.CycleId IN(@CycleId)
										) T6 ON T1.TargetingId=T6.TargetingId
		WHERE T1.[Status] IN(18,33,90) AND T2.TargetingDetailId IS NULL AND T6.TargetingId IS NULL
		--GROUP BY T5.CountyName
		--ORDER BY T5.CountyName,T5.DistrictName,T5.DivisionName,T5.LocationName,T5.SubLocationName,T5.ConstituencyName,T1.ProgrammeNumber
END



--1. T-SERIES
IF @AnalysisStep=1
BEGIN
	--UPDATE T1 SET T1.[Status]=90
	SELECT T1.ProgrammeNumber,ISNULL(T2.FirstName,T3.FirstName) AS FirstName,ISNULL(T2.MiddleName,T3.MiddleName) AS MiddleName,ISNULL(T2.Surname,T3.Surname) AS Surname,ISNULL(T2.Sex,T3.Sex) AS Sex,ISNULL(T2.DateOfBirth,T3.DateOfBirth) AS DateOfBirth,ISNULL(T2.Age,T3.Age) AS Age,ISNULL(T2.IDNumber,T3.IDNumber) AS IDNo
	FROM TargetingHeader T1 LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
									   FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																			FROM TargetingDetail
																			WHERE RelationshipId=1 AND ISNULL(Exited,0)=0
																			GROUP BY TargetingId
																			) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId
											) T2 ON T1.TargetingId=T2.TargetingId
								LEFT JOIN (SELECT TT1.TargetingDetailId,TT1.TargetingId,TT1.FirstName,TT1.MiddleName,TT1.Surname,CASE(TT1.Sex) WHEN 'M' THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar,TT1.DateOfBirth,105) AS DateOfBirth,dbo.GetAge(TT1.DateOfBirth,GETDATE()) AS Age,TT1.IdNumber AS IDNumber
										   FROM TargetingDetail TT1 INNER JOIN (SELECT TargetingId,MIN(TargetingDetailId) AS TargetingDetailId
																				FROM TargetingDetail
																				WHERE (RelationshipId<>1 AND CanReceivePayment=1 AND ISNULL(Exited,0)=0)
																				GROUP BY TargetingId
																				) TT2 ON TT1.TargetingId=TT2.TargetingId AND TT1.TargetingDetailId=TT2.TargetingDetailId									
											) T3 ON T1.TargetingId=T3.TargetingId
		WHERE T1.[Status] IN(18,33,90,99) AND ISNULL(T2.IDNumber,T3.IDNumber) LIKE 'T%'
END



--2. TOTAL NAME MISMATCH
IF @AnalysisStep=2
BEGIN
	--UPDATE T3 SET T3.[Status]=91
	SELECT T3.*
	FROM LegacyDB.dbo.ActiveCTOVCBeneficiaries T1 LEFT JOIN LegacyDB.dbo.IPRS_CTOVC T2 ON  CONVERT(float,T1.ResolvedIDNo)=T2.IDNumber
												  INNER JOIN TargetingHeader T3 ON T1.ProgrammeNumber=T3.ProgrammeNumber
	WHERE T1. IPRS_IDNoExists=1 AND CONVERT(tinyint,T1.IPRS_FirstNameExists)+CONVERT(tinyint,T1.IPRS_MiddleNameExists)+CONVERT(tinyint,T1.IPRS_SurnameExists)<=1 AND T3.[Status] IN(18,33)
END



--7. HOUSEHOLDS WITH ONE OR MORE OVCs DUPLICATED
IF @AnalysisStep=7
BEGIN

--6419
--13581
--7162

	--SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId,T1.FirstName,ISNULL(T1.MiddleName,'') AS MiddleName,T1.Surname,T1.DateOfBirth,COUNT(T1.TargetingDetailId) AS NoOfDuplicates
	--FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
	--WHERE T2.[Status] IN(18,33,90,91) AND T1.RelationshipId<>1 AND T1.DateOfBirth>@MIN_OVC_DOB AND ISNULL(T1.Exited,0)=0 AND ISNULL(T1.FirstName,'')<>''-- and t1.TargetingId=358084
	--GROUP BY T1.TargetingId,T1.FirstName,ISNULL(T1.MiddleName,''),T1.Surname,T1.DateOfBirth
	--HAVING COUNT(T1.TargetingDetailId)>1

	--UPDATE T1 SET T1.Exited=1
	SELECT T1.*
	FROM TargetingDetail T1 INNER JOIN (
											SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId,T1.FirstName,ISNULL(T1.MiddleName,'') AS MiddleName,T1.Surname,T1.DateOfBirth,COUNT(T1.TargetingDetailId) AS NoOfDuplicates
											FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
											WHERE T2.[Status] IN(18,33,90,91) AND T1.RelationshipId<>1 AND T1.DateOfBirth>@MIN_OVC_DOB AND ISNULL(T1.Exited,0)=0 AND ISNULL(T1.FirstName,'')<>''
											GROUP BY T1.TargetingId,T1.FirstName,ISNULL(T1.MiddleName,''),T1.Surname,T1.DateOfBirth
											HAVING COUNT(T1.TargetingDetailId)>1
										) T2 ON T1.TargetingId=T2.TargetingId AND T1.RelationshipId<>1 AND T1.FirstName=T2.FirstName AND ISNULL(T1.MiddleName,'')=T2.MiddleName AND T1.Surname=T2.Surname AND T1.DateOfBirth=T2.DateOfBirth AND T1.TargetingDetailId<>T2.TargetingDetailId

END



--12. HOUSEHOLDS MISSING ENROLMENT DATE INFO
IF @AnalysisStep=12
BEGIN
	--UPDATE T1 SET T1.EnrollmentDate=ISNULL(T1.EnrollmentDate,ISNULL(T1.DateSelected,ISNULL(T1.TargetingDate,T1.DateInterviewed)))
	SELECT T1.ProgrammeNumber,T1.EnrollmentDate,T1.DateSelected,T1.TargetingDate,T1.DateInterviewed,ISNULL(T1.EnrollmentDate,ISNULL(T1.DateSelected,ISNULL(T1.TargetingDate,T1.DateInterviewed)))
	FROM TargetingHeader T1 
	WHERE T1.[Status] IN(18,33,90,91) AND ISDATE(T1.EnrollmentDate)=0
END



--5. HOUSEHOLDS WITH MULTIPLE CGs
IF @AnalysisStep=5
BEGIN
	--SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId
	--FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
	--WHERE T2.[Status] IN(18,33,90,91,99) AND T1.RelationshipId=1 AND ISNULL(T1.Exited,0)=0
	--GROUP BY T1.TargetingId
	--HAVING COUNT(T1.TargetingDetailId)>1

	--76
	--157
	--81

	--UPDATE T1 SET T1.Exited=1
	SELECT T1.*
	FROM TargetingDetail T1 INNER JOIN (
											SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId
											FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
											WHERE T2.[Status] IN(18,33,90,91,99) AND T1.RelationshipId=1 AND ISNULL(T1.Exited,0)=0
											GROUP BY T1.TargetingId
											HAVING COUNT(T1.TargetingDetailId)>1
										) T2 ON T1.TargetingId=T2.TargetingId AND T1.RelationshipId=1 AND T1.TargetingDetailId<>T2.TargetingDetailId

--1767
--3751
--1984

	--SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId,T1.FirstName,ISNULL(T1.MiddleName,'') AS MiddleName,T1.Surname,T1.DateOfBirth,COUNT(T1.TargetingDetailId) AS NoOfDuplicates
	--FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
	--WHERE T2.[Status] IN(18,33,90,91) AND ISNULL(T1.Exited,0)=0 AND ISNULL(T1.FirstName,'')<>'' AND T1.RelationshipId<>1 
	--GROUP BY T1.TargetingId,T1.FirstName,ISNULL(T1.MiddleName,''),T1.Surname,T1.DateOfBirth
	--HAVING COUNT(T1.TargetingDetailId)>1

	--UPDATE T1 SET T1.Exited=1
	SELECT T1.*
	FROM TargetingDetail T1 INNER JOIN (
											SELECT T1.TargetingId,MIN(T1.TargetingDetailId) AS TargetingDetailId,T1.FirstName,ISNULL(T1.MiddleName,'') AS MiddleName,T1.Surname,T1.DateOfBirth,COUNT(T1.TargetingDetailId) AS NoOfDuplicates
											FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
											WHERE T2.[Status] IN(18,33,90,91) AND ISNULL(T1.Exited,0)=0 AND ISNULL(T1.FirstName,'')<>'' AND T1.RelationshipId<>1
											GROUP BY T1.TargetingId,T1.FirstName,ISNULL(T1.MiddleName,''),T1.Surname,T1.DateOfBirth
											HAVING COUNT(T1.TargetingDetailId)>1
										) T2 ON T1.TargetingId=T2.TargetingId AND T1.RelationshipId<>1 AND T1.FirstName=T2.FirstName AND ISNULL(T1.MiddleName,'')=T2.MiddleName AND T1.Surname=T2.Surname AND T1.DateOfBirth=T2.DateOfBirth AND T1.TargetingDetailId<>T2.TargetingDetailId


	--UPDATE T1 SET T1.Exited=1
	SELECT T1.*
	FROM TargetingDetail T1 INNER JOIN (
											SELECT T1.TargetingId,MIN(CASE WHEN(T1.RelationshipId=1) THEN T1.TargetingDetailId ELSE NULL END) AS TargetingDetailId,T1.FirstName,ISNULL(T1.MiddleName,'') AS MiddleName,T1.Surname,T1.DateOfBirth,COUNT(T1.TargetingDetailId) AS NoOfDuplicates
											FROM TargetingDetail T1 INNER JOIN TargetingHeader T2 ON T1.TargetingId=T2.TargetingId
											WHERE T2.[Status] IN(18,33,90,91) AND ISNULL(T1.Exited,0)=0 AND ISNULL(T1.FirstName,'')<>''
											GROUP BY T1.TargetingId,T1.FirstName,ISNULL(T1.MiddleName,''),T1.Surname,T1.DateOfBirth
											HAVING COUNT(T1.TargetingDetailId)>1
										) T2 ON T1.TargetingId=T2.TargetingId AND T1.RelationshipId<>1 AND T1.FirstName=T2.FirstName AND ISNULL(T1.MiddleName,'')=T2.MiddleName AND T1.Surname=T2.Surname AND T1.DateOfBirth=T2.DateOfBirth AND T1.TargetingDetailId<>T2.TargetingDetailId AND ISNULL(T1.Exited,0)=0



END



--7. UPDATING IPRS DETAIL
IF @AnalysisStep=7
BEGIN
	--UPDATE T1 SET T1.IDNumber=T3.IDNumber,T1.FirstName=T3.FirstName,T1.MiddleName=T3.MiddleName,T1.Surname=T3.Surname,T1.Sex=T3.Sex,T1.DateOfBirth=ISNULL(T3.DoB,T1.DateOfBirth)
	SELECT COUNT(T1.TargetingDetailId)
	--SELECT T1.IDNumber,T1.FirstName,T1.MiddleName,T1.Surname,T1.Sex,T1.DateOfBirth,T3.IDNumber,T3.FirstName,T3.MiddleName,T3.Surname,T3.Sex,T3.DoB
	FROM TargetingDetail T1 INNER JOIN [LegacyDB].dbo.ActiveCTOVCBeneficiaries T2 ON T1.TargetingDetailId=T2.TargetingDetailId AND T2.Exited=0 AND T2.IPRS_IDNoExists=1 
							INNER JOIN [LegacyDB].dbo.IPRS_Legacy T3 ON CONVERT(float,T2.ResolvedIDNo)=T3.IDNumber
	--WHERE T1.FirstName COLLATE DATABASE_DEFAULT<>T3.FirstName COLLATE DATABASE_DEFAULT 
	--	OR T1.MiddleName COLLATE DATABASE_DEFAULT<>T3.MiddleName COLLATE DATABASE_DEFAULT OR T1.Surname COLLATE DATABASE_DEFAULT<>T3.Surname COLLATE DATABASE_DEFAULT 
	--	OR T1.Sex COLLATE DATABASE_DEFAULT<>T3.Sex COLLATE DATABASE_DEFAULT OR T1.DateOfBirth<>T3.DoB
END


