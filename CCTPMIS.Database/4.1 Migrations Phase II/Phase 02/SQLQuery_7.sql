	DECLARE @FileName varchar(128)
	DECLARE @FileExtension varchar(5)
	DECLARE @FileCompression varchar(5)
	DECLARE @FilePathName varchar(128)
	DECLARE @SQLStmt varchar(8000)
	DECLARE @FileExists bit
	DECLARE @FileIsDirectory bit
	DECLARE @FileParentDirExists bit
	DECLARE @DatePart_Day char(2)
	DECLARE @DatePart_Month char(2)
	DECLARE @DatePart_Year char(4)
	DECLARE @DatePart_Time char(4)
	DECLARE @SysCode varchar(20)
	DECLARE @SysDetailCode varchar(20)
	DECLARE @SystemCodeDetailId1 int
	DECLARE @SystemCodeDetailId2 int
    DECLARE @SystemCodeDetailId3 int
	DECLARE @FileCreationId int
	DECLARE @FilePassword nvarchar(64)
	DECLARE @ErrorMsg varchar(128)
	DECLARE @NoOfRows int
    
    SET @SysCode='Enrolment Status'
	SET @SysDetailCode='PROGSHAREPSP'
	SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

    SET @SysDetailCode='PSPENROL'
	SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
    
    SELECT T2.Id AS EnrolmentNo
		,T4.Code AS Programme
		,T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T7.Code AS BeneSex
		,T6.DoB AS BeneDoB
		,ISNULL(T9.FirstName,'') AS CGFirstName
		,ISNULL(T9.MiddleName,'') AS CGMiddleName
		,ISNULL(T9.Surname,'') AS CGSurname
		,ISNULL(T9.NationalIdNo,'') AS CGIDNo
		,ISNULL(T10.Code,'') AS CGSex
		,ISNULL(T9.DoB,'') AS CGDoB
		,ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		,ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
	FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
								   INNER JOIN Household T3 ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id -- AND T6.[Name] IN ('Bomet','Kajiado','Kericho','Laikipia','Nakuru','Narok','Baringo') 
															   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
															   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

                                              LEFT JOIN BeneficiaryAccount T14 ON T2.ID= T14.HhEnrolmentId
	
    
    --CONSIDER THE BATCH FOR BEFICIARY REPLACEMENTS!

	WHERE T1.StatusId=@SystemCodeDetailId1  or ( t1.StatusId= @SystemCodeDetailId3 AND T1.CreatedOn<='2018-06-30' AND T14.HhEnrolmentId IS NULL )
