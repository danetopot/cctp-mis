
DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @SystemCodeDetailId int

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @SystemCodeDetailId=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



SELECT T2.Id AS EnrolmentNo
		, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		, T6.FirstName AS BeneFirstName
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, DATEDIFF(YEAR,T6.DoB,GETDATE()) 'BeneAge'
		
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
			, DATEDIFF(YEAR,T9.DoB,GETDATE()) 'CGAge'
	
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
    INNER JOIN Household T3 ON T2.HhId=T3.Id
    INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
    LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId AND T8.StatusId=@SystemCodeDetailId
    LEFT JOIN Person T9 ON T8.PersonId=T9.Id
    LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
    INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
    FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
        INNER JOIN Division T3 ON T2.DivisionId=T3.Id
        INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
        INNER JOIN District T5 ON T4.DistrictId=T5.Id
        INNER JOIN County T6 ON T4.CountyId=T6.Id
        INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
        INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
   
    INNER JOIN (
        SELECT try_CONVERT(DECIMAL,T4.NationalIdNo) NationalIdNo
        FROM Household T1
        INNER JOIN Programme T2 ON T1.ProgrammeId = T2.Id
        INNER JOIN HouseholdMember T3 ON T2.PrimaryRecipientId = T3.MemberRoleId AND T1.ID = T3.HhId
        INNER JOIN Person T4 ON T3.PersonId = T4.Id AND T2.Code='OPCT'
        AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
        GROUP BY try_CONVERT(DECIMAL,T4.NationalIdNo)
        HAVING COUNT_BIG(T3.HhId)>1
            ) T15 ON try_CONVERT(DECIMAL,T6.NationalIdNo) = T15.NationalIdNo
             LEFT JOIN (SELECT DISTINCT HhEnrolmentId
    FROM BeneficiaryAccount) T14 ON T2.Id=T14.HhEnrolmentId

WHERE T14.HhEnrolmentId IS NULL and t1.ApvOn<'2018-09-01' AND DATEDIFF(YEAR,T6.DoB,GETDATE())<70

---=========================================================----ELSE

DECLARE @SysCode varchar(30)
DECLARE @SysDetailCode varchar(30)
DECLARE @SystemCodeDetailId int

SET @SysCode='Member Status'
SET @SysDetailCode='1'
SELECT @SystemCodeDetailId=T1.Id
FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode



SELECT T2.Id AS EnrolmentNo
		, T2.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T2.ProgrammeNo)))+CONVERT(varchar(6),T2.ProgrammeNo) AS ProgrammeNo
		, T6.FirstName AS BeneFirstName
		, T6.MiddleName AS BeneMiddleName
		, T6.Surname AS BeneSurname
		, T6.NationalIdNo AS BeneIDNo
		, T7.Code AS BeneSex
		, T6.DoB AS BeneDoB
		, DATEDIFF(YEAR,T6.DoB,GETDATE()) 'BeneAge'
		
		, ISNULL(T9.FirstName,'') AS CGFirstName
		, ISNULL(T9.MiddleName,'') AS CGMiddleName
		, ISNULL(T9.Surname,'') AS CGSurname
		, ISNULL(T9.NationalIdNo,'') AS CGIDNo
		, ISNULL(T10.Code,'') AS CGSex
		, ISNULL(T9.DoB,'') AS CGDoB
			, DATEDIFF(YEAR,T9.DoB,GETDATE()) 'CGAge'
	
		, ISNULL(T6.MobileNo1,T6.MobileNo2) AS MobileNo1
		, ISNULL(T9.MobileNo1,T9.MobileNo2) AS MobileNo2
		, T13.County
		, T13.Constituency
		, T13.District
		, T13.Division
		, T13.Location
		, T13.SubLocation
FROM HouseholdEnrolmentPlan T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhEnrolmentPlanId
    INNER JOIN Household T3 ON T2.HhId=T3.Id
    INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5 ON T2.HhId=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId AND T5.StatusId=@SystemCodeDetailId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
    LEFT JOIN HouseholdMember T8 ON T2.HhId=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId AND T8.StatusId=@SystemCodeDetailId
    LEFT JOIN Person T9 ON T8.PersonId=T9.Id
    LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
    INNER JOIN HouseholdSubLocation T11 ON T2.HhId=T11.HhId
    INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
    INNER JOIN (SELECT T8.Id AS GeoMasterId, T1.Id AS SubLocationId, T1.Name AS SubLocation, T2.Name AS Location, T3.Name AS Division, T5.Name AS District, T6.Name AS County, T7.Name AS Constituency
    FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
        INNER JOIN Division T3 ON T2.DivisionId=T3.Id
        INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
        INNER JOIN District T5 ON T4.DistrictId=T5.Id
        INNER JOIN County T6 ON T4.CountyId=T6.Id
        INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
        INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
											  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
   
    INNER JOIN (
        SELECT try_CONVERT(DECIMAL,T4.NationalIdNo) NationalIdNo
        FROM Household T1
        INNER JOIN Programme T2 ON T1.ProgrammeId = T2.Id
        INNER JOIN HouseholdMember T3 ON T2.PrimaryRecipientId = T3.MemberRoleId AND T1.ID = T3.HhId
        INNER JOIN Person T4 ON T3.PersonId = T4.Id AND T2.Code='OPCT'
        AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
        GROUP BY try_CONVERT(DECIMAL,T4.NationalIdNo)
        HAVING COUNT_BIG(T3.HhId)>1
            ) T15 ON try_CONVERT(DECIMAL,T6.NationalIdNo) = T15.NationalIdNo
             LEFT JOIN (SELECT DISTINCT HhEnrolmentId
    FROM BeneficiaryAccount) T14 ON T2.Id=T14.HhEnrolmentId

WHERE T14.HhEnrolmentId IS NULL and t1.ApvOn<'2018-09-01' --AND DATEDIFF(YEAR,T6.DoB,GETDATE())<70

---=======================================================================----- 
