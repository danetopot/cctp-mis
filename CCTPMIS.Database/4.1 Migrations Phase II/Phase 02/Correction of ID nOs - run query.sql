

--SELECT * INTO LEGACYDB.DBO.PersonWrongID FROM Person  WHERE NationalIdNo LIKE'%E+%'
-- select * from LEGACYDB.DBO.PersonWrongID

 
--==============OPCT BENE ================


--SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('OPCTBene', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_01 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


--SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('OPCTBene', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_02 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('OPCTBene_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_01 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'

SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)

--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('OPCTBene_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_02 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'



--================================== OPCT CG =======================================--



SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('OPCTCG', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_01 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('OPCTCG', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id 
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_02 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('OPCTCG_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_01 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'

SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('OPCTCG_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_02 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'



--==================================PWSD CG =======================================--



SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('PwSDCG', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('PwSDcg', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id 
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_04 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('PwSDCG_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'

SELECT T1.NationalIdNo , CAST(T1B.IPRS_CGIDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_CGIDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('PwSDCG_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_04 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


--==================================PWSD bene =======================================--



SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('PwSDBene', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('PwSDBene', CONVERT(VARCHAR,T2.Id))
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id 
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_04 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'


SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('PwSDBene_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'

SELECT T1.NationalIdNo , CAST(T1B.IPRS_IDNo AS VARCHAR)
--update T1  set T1.NationalIdNo = CAST(T1B.IPRS_IDNo AS VARCHAR)
from Person T1
INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T1.RefId = CONCAT('PwSDBene_', CONVERT(VARCHAR,T3.Id))
INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T2.HhRegistrationId = T3.Id
INNER JOIN    (SELECT  DISTINCT * FROM [LegacyDB].dbo.EXCEPTION_04 )  T1B ON  T1B.ProgrammeNo = T2.HhId  
AND T1.CreatedOn>'2019-04-01'  AND T1.NationalIdNo LIKE '%e+%'

