
DECLARE @Migrations2StartDate DATETIME = '2019-04-01'
DECLARE @SeventyPlusEndDate DATETIME = '2018-09-01'

;
WITH
    MigrationsTokenGenerator
    as
    (
        SELECT T3.Id AS HhId--, ROW_NUMBER() over (order by T6.DoB) As TokenNo
        from Household T3
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn>@Migrations2StartDate AND ISNULL(TokenId,0)>0
    ),
    SeventyPlusTokenGenerator
    AS
    (
        SELECT T3.Id AS HhId
        from Household T3
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn<@SeventyPlusEndDate AND ISNULL(TokenId,0)=0
            INNER JOIN HouseholdEnrolment T7 ON T7.HhId = T3.Id AND T4.Code='OPCT'
            LEFT JOIN (SELECT DISTINCT HhEnrolmentId
            FROM BeneficiaryAccount) T16 on T7.Id = T16.HhEnrolmentId
        WHERE T16.HhEnrolmentId IS NULL AND T6.NationalIdNo not IN  ( SELECT NationalIdNo
            FROM ( SELECT ROW_NUMBER() OVER(PARTITION BY   T6.NationalIdNo ORDER BY  T3.ID DESC) As RowNo, T5.HhId, T6.NationalIdNo , T3.CreatedOn
                FROM Household T3
                    INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T4.Code ='OPCT'
                    INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
                    INNER JOIN Person T6 ON T5.PersonId=T6.Id   ) T1
            WHERE RowNo>1 )
    )

select T1.HhId, T6.DoB
from(
                    SELECT *
        FROM MigrationsTokenGenerator
    UNION
        SELECT *
        FROM SeventyPlusTokenGenerator
    ) T1 INNER JOIN  Household T3  ON T1.HhId = T3.Id
INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
INNER JOIN Person T6 ON T5.PersonId=T6.Id

















 DECLARE @HhStatus_EnrolledProgCode varchar(20)
    DECLARE @HhStatus_EnrolledPSPCode varchar(20)
    DECLARE @HhStatus_PSPCardedCode varchar(20)
    DECLARE @HhStatus_OnPayrollCode varchar(20)
    DECLARE @HhStatus_OnSuspensionCode varchar(20)
     SET @HhStatus_EnrolledProgCode = 'ENRL'
    SET @HhStatus_EnrolledPSPCode = 'ENRLPSP'
    SET @HhStatus_PSPCardedCode = 'PSPCARDED'
    SET @HhStatus_OnPayrollCode = 'ONPAY'
    SET @HhStatus_OnSuspensionCode = 'SUS'



    AND T2.Code IN(@HhStatus_EnrolledProgCode,@HhStatus_EnrolledPSPCode,@HhStatus_PSPCardedCode,@HhStatus_OnPayrollCode,@HHStatus_OnSuspensionCode)
    