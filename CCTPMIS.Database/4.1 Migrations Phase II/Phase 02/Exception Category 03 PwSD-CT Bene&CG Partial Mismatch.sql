 --=====================REMOVE DUPLICATED 124 HOUSEHOLDS IN THIS MIGRATION. ;WITH X AS 
    (SELECT T3.Id AS HouseholdId,
         T4.Code Programme,
         T3.CreatedOn --,
        REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.TokenId)))+CONVERT(varchar(6),T3.TokenId) AS TokenId --,T6.FirstName AS BeneFirstName --,T6.MiddleName AS BeneMiddleName --,T6.Surname AS BeneSurname --,T6.NationalIdNo AS BeneIDNo --,T13.County --,T13.Constituency --,T13.District --,T13.Division --,T13.Location --,T13.SubLocation
    FROM Household T3 --
        ON T2.HhId=T3.Id
    INNER JOIN Programme T4
        ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5
        ON T3.Id=T5.HhId
            AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6
        ON T5.PersonId=T6.Id
    INNER JOIN SystemCodeDetail T7
        ON T6.SexId=T7.Id
    LEFT JOIN HouseholdMember T8
        ON T3.Id=T8.HhId
            AND T4.SecondaryRecipientId=T8.MemberRoleId
    LEFT JOIN Person T9
        ON T8.PersonId=T9.Id
    LEFT JOIN SystemCodeDetail T10
        ON T9.SexId=T10.Id
    INNER JOIN HouseholdSubLocation T11
        ON T3.Id=T11.HhId
    INNER JOIN GeoMaster T12
        ON T11.GeoMasterId=T12.Id
            AND T12.IsDefault=1
    INNER JOIN 
        (SELECT T8.Id AS GeoMasterId,
        T1.Id AS SubLocationId,
        T1.Name AS SubLocation,
        T2.Name AS Location,
        T3.Name AS Division,
        T5.Name AS District,
        T6.Name AS County,
        T7.Name AS Constituency
        FROM SubLocation T1
        INNER JOIN Location T2
            ON T1.LocationId=T2.Id
        INNER JOIN Division T3
            ON T2.DivisionId=T3.Id
        INNER JOIN CountyDistrict T4
            ON T3.CountyDistrictId=T4.Id
        INNER JOIN District T5
            ON T4.DistrictId=T5.Id
        INNER JOIN County T6
            ON T4.CountyId=T6.Id
        INNER JOIN Constituency T7
            ON T1.ConstituencyId=T7.Id
        INNER JOIN GeoMaster T8
            ON T6.GeoMasterId=T8.Id ) T13
            ON T11.SubLocationId=T13.SubLocationId
                AND T12.Id=T13.GeoMasterId
                AND T3.CreatedOn>'2019-04-01'
                AND T4.Code='OPCT'
                AND try_CONVERT(DECIMAL,T6.NationalIdNo) IN 
            (SELECT try_CONVERT(DECIMAL,
        T4.NationalIdNo)
            FROM Household T1
            INNER JOIN Programme T2
                ON T1.ProgrammeId = T2.Id
            INNER JOIN HouseholdMember T3
                ON T2.PrimaryRecipientId = T3.MemberRoleId
                    AND T1.ID = T3.HhId
            INNER JOIN Person T4
                ON T3.PersonId = T4.Id
                    AND T2.Code='OPCT'
                    AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
            GROUP BY  try_CONVERT(DECIMAL,T4.NationalIdNo)
            HAVING COUNT_BIG(T3.HhId)>1 ) )UPDATE Household SET StatusId = 22,
         ModifiedOn=GETDATE(),
         ModifiedBy=1
        FROM X
    LEFT JOIN Household H
    ON X.HouseholdId = H.Id --SELECT *
FROM HouseholdMember
WHERE HhId = 866933 ;
WITH MigrationTokenGenerator AS 
    (SELECT T3.Id AS HhId,
         ROW_NUMBER()
        OVER (order by T6.DoB) AS TokenNo
    FROM Household T3
    INNER JOIN Programme T4
        ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5
        ON T3.Id=T5.HhId
            AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6
        ON T5.PersonId=T6.Id
            AND T3.CreatedOn>'2019-04-01' ), y AS 
    (SELECT T3.Id AS HhId,
         ROW_NUMBER()
        OVER (order by T6.DoB) AS TokenNo
    FROM Household T3
    INNER JOIN Programme T4
        ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5
        ON T3.Id=T5.HhId
            AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6
        ON T5.PersonId=T6.Id
            AND T3.CreatedOn<'2018-09-01'
            AND T4.Code = 'OPCT'
    INNER JOIN HouseholdEnrolment T7
        ON T3.ID = T7.HhId
            AND try_CONVERT(DECIMAL,T6.NationalIdNo) IN 
        (SELECT try_CONVERT(DECIMAL,
        T4.NationalIdNo)
        FROM Household T1
        INNER JOIN Programme T2
            ON T1.ProgrammeId = T2.Id
        INNER JOIN HouseholdMember T3
            ON T2.PrimaryRecipientId = T3.MemberRoleId
                AND T1.ID = T3.HhId
        INNER JOIN Person T4
            ON T3.PersonId = T4.Id
                AND T2.Code='OPCT'
                AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
        GROUP BY  try_CONVERT(DECIMAL,T4.NationalIdNo)
        HAVING COUNT_BIG(T3.HhId)>1 )
        LEFT JOIN BeneficiaryAccount T8
            ON T7.Id = T8.HhEnrolmentId
        WHERE T8.Id IS NULL )
    SELECT *
FROM Y ;WITH M AS 
    (SELECT T3.Id AS HhId,
         T6.NationalIdNo,
        DATEDIFF(YEAR,
        T6.DoB,
        GETDATE()) 'Age'
    FROM Household T3
    INNER JOIN Programme T4
        ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5
        ON T3.Id=T5.HhId
            AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6
        ON T5.PersonId=T6.Id
            AND T3.CreatedOn<'2018-09-01'
            AND T4.Code = 'OPCT'
    INNER JOIN HouseholdEnrolment T7
        ON T3.ID = T7.HhId
            AND DATEDIFF(YEAR,T6.DoB,GETDATE())>=70
    LEFT JOIN BeneficiaryAccount T8
        ON T7.Id = T8.HhEnrolmentId
    WHERE T8.Id IS NULL )
SELECT *
FROM M
WHERE try_CONVERT(DECIMAL,M.NationalIdNo) NOT IN 
    (SELECT try_CONVERT(DECIMAL,
        T4.NationalIdNo)
    FROM Household T1
    INNER JOIN Programme T2
        ON T1.ProgrammeId = T2.Id
    INNER JOIN HouseholdMember T3
        ON T2.PrimaryRecipientId = T3.MemberRoleId
            AND T1.ID = T3.HhId
    INNER JOIN Person T4
        ON T3.PersonId = T4.Id
            AND T2.Code='OPCT'
            AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
            AND DATEDIFF(YEAR,T4.DoB,GETDATE())>=70
    GROUP BY  try_CONVERT(DECIMAL,T4.NationalIdNo)
    HAVING COUNT_BIG(T3.HhId)>1 ) --SELECT *
FROM Person
WHERE NationalIdNo ='25325887'