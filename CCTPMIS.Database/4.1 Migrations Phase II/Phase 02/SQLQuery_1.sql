

DECLARE @MigrationsStartDate DATETIME = '2019-04-01'
DECLARE @SeventyPlusEndDate DATETIME = '2018-09-01'

/* 
;
WITH
    MigrationsTokenGenerator
    as
    (
        SELECT T3.Id AS HhId--, ROW_NUMBER() over (order by T6.DoB) As TokenNo
        from Household T3
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn>@MigrationsStartDate AND ISNULL(TokenId,0)>0
    ),
    SeventyPlusTokenGenerator
    AS
    (
        SELECT T3.Id AS HhId
        from Household T3
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
            INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn<@SeventyPlusEndDate AND ISNULL(TokenId,0)=0
            LEFT JOIN HouseholdEnrolment T7 ON T7.HhId = T3.Id AND T4.Code='OPCT'
            LEFT JOIN ( SELECT TRY_CONVERT(DECIMAL,T4.NationalIdNo) NationalIdNo
            FROM Household T1
                INNER JOIN Programme T2 ON T1.ProgrammeId = T2.Id
                INNER JOIN HouseholdMember T3 ON T2.PrimaryRecipientId = T3.MemberRoleId AND T1.ID = T3.HhId
                INNER JOIN Person T4 ON T3.PersonId = T4.Id AND T2.Code='OPCT'
                    AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1
            GROUP BY TRY_CONVERT(DECIMAL,T4.NationalIdNo)
            HAVING COUNT_BIG(T3.HhId)>1
            ) T15 ON TRY_CONVERT(DECIMAL,T6.NationalIdNo) = T15.NationalIdNo
            LEFT JOIN (SELECT DISTINCT HhEnrolmentId
            FROM BeneficiaryAccount) T16 on T7.Id = T16.HhEnrolmentId 
            WHERE T16.HhEnrolmentId IS NULL
    )

select 1
*/
/*
SELECT * FROM MigrationsTokenGenerator
        UNION
            SELECT *
            FROM SeventyPlusTokenGenerator
    
    
    




update Household set TokenId = TokenNo from TokenGenerator
left join Household h on h.Id = TokenGenerator.HhId
GO
*/


SELECT RowNo, NationalIdNo, HhId
FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY   T6.NationalIdNo ORDER BY  T6.NationalIDNo ASC) As RowNo, T5.HhId, T6.NationalIdNo
    FROM Household T3
        INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T4.Code ='OPCT'
        INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
        INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn>@MigrationsStartDate
            AND ISNULL(TokenId,0)>0
) T1



SELECT T3.Id AS HhId
from Household T3
    INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
    INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
    INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn<@SeventyPlusEndDate AND ISNULL(TokenId,0)=0
    LEFT JOIN HouseholdEnrolment T7 ON T7.HhId = T3.Id AND T4.Code='OPCT'
    LEFT JOIN
    (
SELECT NationalIdNo
    FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY   T6.NationalIdNo ORDER BY  T3.ID DESC) As RowNo, T5.HhId, T6.NationalIdNo , T3.CreatedOn
        FROM Household T3
            INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id AND T4.Code ='OPCT'
            INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
            INNER JOIN Person T6 ON T5.PersonId=T6.Id  
) T1
    WHERE RowNo>1
) T8 ON TRY_CONVERT(VARCHAR,T8.NationalIdNo) = TRY_CONVERT(VARCHAR,T6.NationalIdNo)
