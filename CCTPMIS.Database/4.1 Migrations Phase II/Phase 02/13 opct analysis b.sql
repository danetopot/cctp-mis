
--AND 
--X.ResolvedNationalNo IN (

-- SELECT distinct 

-- X.ResolvedNationalNo
 
 /*,
 -- x.*,
 Y.Programme
,y.BeneFirstName
,y.BeneMiddleName
,y.BeneSurname
,Y.BeneSex 
,y.BeneIDNo
,X.ProgrammeNumber 'Legacy Programme No'
,y.ProgrammeNo
,isnull(y.ProgrammeNo,'') ProgrammeNo
,case when isnull(y.TokenId,0)>0 then 'Yes' else 'No' end  as 'Migrated to CCTP'
,case when isnull(y.ProgrammeNo,0)>0 then 'Yes' else 'No' end  as 'Enrolled By Programme'
,case when isnull(y.AccountName,'')<>'' then 'Yes' else 'No' end as 'Enrolled By PSP'
,isnull(y.AccountName,'') AccountName
,isnull(y.AccountNo,'') AccountNo
,y.County
,y.Constituency
,y.District
,y.Division
,y.Location
,y.SubLocation

*/
 -- FROM X LEFT JOIN Y ON CONVERT(VARCHAR, X.ResolvedNationalNo) =  Y.BeneIDNo COLLATE DATABASE_DEFAULT
 --WHERE Y.BeneIDNo IS not    NULL

 --GROUP BY  X.ResolvedNationalNo
 --HAVING COUNT( X.ResolvedNationalNo)>1

 --)
--AND   isnull(y.ProgrammeNo,0)<>0
-- AND  isnull(y.AccountName,'')=''


use [CCTP-MIS]
go

 

;with x as 
(
SELECT distinct  T1.BeneficiaryName, T1.IdNumber IdNo, T1.ProgrammeNumber ProgrammeNo, T2.*, T1.ResolvedNationalNo ResolvedNationalNo2
FROM LegacyDB.dbo.CONSOLIDATED_CLEAN T1
    LEFT JOIN LegacyDB.dbo.ActiveOPCTBeneficiariesUPDATED T2 ON   t1.ResolvedNationalNo = t2.ResolvedNationalNo
	WHERE T1.Programme = 'OPCT' AND T2.IDNumber IS NOT   NULL
)
,Y AS (
SELECT  
		T4.Code Programme
		,REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.TokenId)))+CONVERT(varchar(6),T3.TokenId) AS TokenId
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		 ,convert(varchar,T6B.NationalIdNo) AS BeneIDNo
		,T7.Code AS BeneSex
		,T9.ID
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
		,T14.BeneProgNoPrefix+REPLICATE('0',6-LEN(CONVERT(varchar(6),T14.ProgrammeNo)))+CONVERT(varchar(6),T14.ProgrammeNo) AS ProgrammeNo
		,T15.AccountName
		,T15.AccountNo
		 FROM  Household T3  -- ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id and T4.Code ='OPCT'
								   INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id

								   INNER JOIN (
                SELECT t1.RefId, SUBSTRING( T2.NationalIdNo, PATINDEX('%[^0]%',  T2.NationalIdNo+'.'), LEN(  T2.NationalIdNo)) NationalIdNo
                FROM PERSON T1
                    INNER JOIN [LegacyDB].dbo.HhMembers T2 ON T1.RefId = CONCAT('OPCTBene', CONVERT(VARCHAR,T2.Id))
                    INNER JOIN LegacyDB.DBO.HhRegistration T3 ON T2.HhRegistrationId = T3.Id

                      
            UNION

                SELECT T1.RefId, SUBSTRING( T3.NationalIdNo, PATINDEX('%[^0]%',  T3.NationalIdNo+'.'), LEN(  T3.NationalIdNo)) NationalIdNo
                FROM PERSON T1
                    INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.RefId = CONCAT('OPCTBene_', CONVERT(VARCHAR,T2.Id))
                    INNER JOIN LegacyDB.DBO.HhMembers  T3 ON T2.ID = T3.HhRegistrationId
                        
) T6B ON T6.RefId = T6B.RefId

								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T3.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id

								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T3.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId

										  LEFT JOIN HouseholdEnrolment T14 ON T14.HhId = T3.Id
										  LEFT JOIN BeneficiaryAccount T15 ON T14.Id = T15.HhEnrolmentId
 )



 SELECT  Y.Programme
,y.BeneFirstName
,y.BeneMiddleName
,y.BeneSurname
,Y.BeneSex 
,y.BeneIDNo
,X.ProgrammeNumber 'Legacy Programme No'
,y.ProgrammeNo
,isnull(y.ProgrammeNo,'') ProgrammeNo
,case when isnull(y.TokenId,0)>0 then 'Yes' else 'No' end  as 'Migrated to CCTP'
,case when isnull(y.ProgrammeNo,0)>0 then 'Yes' else 'No' end  as 'Enrolled By Programme'
,case when isnull(y.AccountName,'')<>'' then 'Yes' else 'No' end as 'Enrolled By PSP'
,isnull(y.AccountName,'') AccountName
,isnull(y.AccountNo,'') AccountNo
,y.County
,y.Constituency
,y.District
,y.Division
,y.Location
,y.SubLocation 



 FROM X LEFT JOIN Y ON CONVERT(VARCHAR, X.ResolvedNationalNo) =  Y.BeneIDNo COLLATE DATABASE_DEFAULT
 WHERE Y.BeneIDNo IS not    NULL

AND 
X.ResolvedNationalNo IN (

 SELECT distinct 

 X.ResolvedNationalNo
 
 /*,
 -- x.*,
 Y.Programme
,y.BeneFirstName
,y.BeneMiddleName
,y.BeneSurname
,Y.BeneSex 
,y.BeneIDNo
,X.ProgrammeNumber 'Legacy Programme No'
,y.ProgrammeNo
,isnull(y.ProgrammeNo,'') ProgrammeNo
,case when isnull(y.TokenId,0)>0 then 'Yes' else 'No' end  as 'Migrated to CCTP'
,case when isnull(y.ProgrammeNo,0)>0 then 'Yes' else 'No' end  as 'Enrolled By Programme'
,case when isnull(y.AccountName,'')<>'' then 'Yes' else 'No' end as 'Enrolled By PSP'
,isnull(y.AccountName,'') AccountName
,isnull(y.AccountNo,'') AccountNo
,y.County
,y.Constituency
,y.District
,y.Division
,y.Location
,y.SubLocation

*/
  FROM X LEFT JOIN Y ON CONVERT(VARCHAR, X.ResolvedNationalNo) =  Y.BeneIDNo COLLATE DATABASE_DEFAULT
 WHERE Y.BeneIDNo IS not    NULL

 GROUP BY  X.ResolvedNationalNo
 HAVING COUNT( X.ResolvedNationalNo)>1

 )
--AND   isnull(y.ProgrammeNo,0)<>0
-- AND  isnull(y.AccountName,'')=''
