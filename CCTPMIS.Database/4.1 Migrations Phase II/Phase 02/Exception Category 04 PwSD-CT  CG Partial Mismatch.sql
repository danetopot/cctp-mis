
;WITH M AS (
select 
T3.Id AS HhId, T6.NationalIdNo,DATEDIFF(YEAR,T6.DoB,GETDATE()) 'Age'
from    Household T3  
INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn<'2018-09-01'  AND T4.Code = 'OPCT' 
INNER JOIN HouseholdEnrolment T7 ON T3.ID = T7.HhId
AND DATEDIFF(YEAR,T6.DoB,GETDATE())>=70
LEFT JOIN BeneficiaryAccount T8 ON T7.Id = T8.HhEnrolmentId WHERE T8.Id IS NULL
)

SELECT * FROM M
WHERE     try_CONVERT(DECIMAL,M.NationalIdNo) not IN (

SELECT   try_CONVERT(DECIMAL,T4.NationalIdNo)   
	 FROM Household T1
					INNER JOIN Programme T2 ON T1.ProgrammeId = T2.Id
					INNER JOIN HouseholdMember T3 ON T2.PrimaryRecipientId = T3.MemberRoleId AND T1.ID = T3.HhId
					INNER JOIN Person T4 ON T3.PersonId = T4.Id AND T2.Code='OPCT' 
					AND ISNUMERIC(ISNULL(T4.NationalIDNo,0))=1   AND DATEDIFF(YEAR,T4.DoB,GETDATE())>=70
                GROUP BY try_CONVERT(DECIMAL,T4.NationalIdNo)
                HAVING COUNT_BIG(T3.HhId)>1
)
