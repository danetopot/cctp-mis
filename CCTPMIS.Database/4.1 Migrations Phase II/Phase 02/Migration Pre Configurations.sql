

-- Upload all the exceptions 01 to 05  file

use LegacyDB
go

ALTER TABLE  dbo.HhRegistration add    DateCreated  datetime null default GETDATE()
ALTER TABLE  dbo.HhMembers add    DateCreated  datetime null default GETDATE()
ALTER TABLE  dbo.Payment add    DateCreated  datetime null default GETDATE()
ALTER TABLE  dbo.CaseGrievance add    DateCreated  datetime null default GETDATE()
ALTER TABLE  dbo.CaseUpdate add    DateCreated  datetime null default GETDATE()

go


UPDATE Exception_05 SET ProgrammeNo= '40609.3' WHERE IDNo = '20836548' 

DECLARE @ProgrammeId int
DECLARE @Programme varchar(100)
DECLARE @MIG_STAGE tinyint

SELECT @ProgrammeId=T1.Id,@Programme=T1.Name FROM [CCTP-MIS].dbo.Programme T1 WHERE T1.Code='CT-OVC'
 
 SELECT ProgrammeNo   FROM Exception_05 T1
LEFT JOIN HhRegistration T2 ON T1.ProgrammeNo = T2.ProgrammeNumber AND T2.ProgrammeId = @ProgrammeId
WHERE T2.Id IS NULL 



