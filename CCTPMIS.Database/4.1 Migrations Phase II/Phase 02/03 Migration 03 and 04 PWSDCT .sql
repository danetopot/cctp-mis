 USE [LegacyDB]
GO

DECLARE @ProgrammeId int
DECLARE @Programme varchar(100)
DECLARE @MIG_STAGE tinyint

SELECT @ProgrammeId=T1.Id,@Programme=T1.Name FROM [CCTP-MIS].dbo.Programme T1 WHERE T1.Code='PwSD-CT'

 
SET @MIG_STAGE=0

-->>1. HOUSEHOLD REGISRATION
IF @MIG_STAGE=1
BEGIN
	DECLARE @HousePartCode_WALL int
	DECLARE @HousePartCode_FLOOR int
	DECLARE @HousePartCode_ROOF int
	DECLARE @ToiletCode int				   
	DECLARE @WaterSourceCode int				   
	DECLARE @LightingSourceCode int				   
	DECLARE @FuelSourceCode int	
	DECLARE @BenefitTypeCode int			

	SET @HousePartCode_WALL=0
	SET @HousePartCode_FLOOR=0
	SET @HousePartCode_ROOF=0
	SET @ToiletCode=1
	SET @WaterSourceCode=2
	SET @LightingSourceCode=3
	SET @FuelSourceCode=4
	SET @BenefitTypeCode=5

	INSERT INTO HhRegistration(HhId,ProgrammeId,Programme,ProgrammeNumber,County,Constituency,District,Division,Location,SubLocation,Village,NearestLandmark,HasRealEstate,IsLivingInOwnHouse
		,RentAmount,ZebuCattle,HybridCattle,Sheep,Goats,Pigs,Camels,Poultry,NoMealPerWeek,MealsPerDay,ReasonForNoMeal,NoMealPerMonth,WallMaterial,FloorMaterial,RoofMaterial,ToiletType,WaterSource
		,LightingSource,FuelSource,LandSize,OtherProg,OtherProgBenefitType,OtherProgBenefitFrequency,OtherProgBenefitAmount,OtherProgOtherBenefits,EnrolmentStatus,EnrolmentDate,EnrolmentReason
		,ApprovalDate,PSPBranch,PSP,Inactive,CapturedBy,ApprovedBy,EnrolledBy,PovertyScore)

	SELECT T1.Register_Id,@ProgrammeId,@Programme,T1.Register_Id,CONVERT(varchar(100),T2.County_Name),CONVERT(varchar(100),T3.Constituency_Name),CONVERT(varchar(100),T2.District_Name),CONVERT(varchar(100),T2.Division_Name),CONVERT(varchar(100),T2.Location_Name),CONVERT(varchar(100),T2.SubLocation_Name),CONVERT(varchar(100),T1.Village) AS Village_Name
		,CONVERT(varchar(100),CASE WHEN(ISNULL(T1.Landmark,'')='') THEN T1.No_School ELSE T1.Landmark END),CASE(ISNULL(T1.Real_Estate,2)) WHEN 1 THEN 'Yes' ELSE 'No' END AS RealEstate,CASE WHEN(T1.Ownership=1) THEN 'Yes' ELSE 'No' END AS IsLivingInOwnHouse,NULL AS RentAmount
		,ISNULL(T1.Zebu_Cattle,0),ISNULL(T1.Hybrid_Cattle,0),ISNULL(T1.Sheep,0),ISNULL(T1.Goats,0),ISNULL(T1.Pigs,0),ISNULL(T1.Camels,0),ISNULL(T1.Poultry,0),T1.Meal_Days AS NoMealPerWeek,T1.Meal_Times AS MealsPerDay,CONVERT(varchar(128),T1.Meal_Reasons) AS ReasonForNoMeal,NULL AS NoMealPerMonth
		,CONVERT(varchar(100),LTRIM(RIGHT(T4.Grade_Name,LEN(T4.Grade_Name)-CHARINDEX('.',T4.Grade_Name,0)))) AS Wall
		,CONVERT(varchar(100),LTRIM(RIGHT(T5.Grade_Name,LEN(T5.Grade_Name)-CHARINDEX('.',T5.Grade_Name,0)))) AS [Floor]
		,CONVERT(varchar(100),LTRIM(RIGHT(T6.Grade_Name,LEN(T6.Grade_Name)-CHARINDEX('.',T6.Grade_Name,0)))) AS Roof
		,CONVERT(varchar(100),LTRIM(RIGHT(T7.Grade_Name,LEN(T7.Grade_Name)-CHARINDEX('.',T7.Grade_Name,0)))) AS Toilet
		,CONVERT(varchar(100),LTRIM(RIGHT(T8.Grade_Name,LEN(T8.Grade_Name)-CHARINDEX('.',T8.Grade_Name,0)))) AS WaterSource
		,CONVERT(varchar(100),LTRIM(RIGHT(T9.Grade_Name,LEN(T9.Grade_Name)-CHARINDEX('.',T9.Grade_Name,0)))) AS Lighting
		,CONVERT(varchar(100),LTRIM(RIGHT(T10.Grade_Name,LEN(T10.Grade_Name)-CHARINDEX('.',T10.Grade_Name,0)))) AS FuelSource
		,T1.Land AS LandSize,T12.Name AS OtherProg,CASE WHEN(ISNULL(T1.Program_Code,0)=0) THEN NULL ELSE T11.Grade_Name END AS BenefitType,NULL AS Frequency,CASE WHEN(ISNULL(T1.Program_Code,0)=0) THEN NULL ELSE T1.Amount END AS BenefitAmount,T1.Other_Benefits AS OtherBenefits,CASE WHEN(T1.Enrolled IN(2,3)) THEN CASE WHEN(ISNULL(T1.Inactive,0)=0 AND T1.Enrolled IN(2)) THEN 'Active' WHEN (ISNULL(T1.Inactive,0)=0 AND T1.Enrolled=3) THEN 'Suspended' ELSE 'Inactive' END ELSE 'Registered' END  AS EnrolmentStatus,T1.Entry_Date AS EnrollmentDate,T1.Enrollment_Reasons AS EnrollmentReason
		,T1.Entry_Date AS ApprovalDate,T13.Paypoint_Name AS AgencyBranchName,T14.Agency_Name,T1.InActive,CONVERT(varchar(100),T1.Loc_Names) AS CreatedBy,NULL AS ApprovedBy,CONVERT(varchar(100),ISNULL(T15.Other_Names,'')+ISNULL(' '+T15.Surname,'')) AS EnrolledBy,T1.[Weight] AS PovertyScore
	FROM [DGSD].dbo.Register T1 
   INNER JOIN  (select * from ( SELECT ROW_NUMBER() OVER(PARTITION BY  T0.ProgrammeNo ORDER BY  T0.ProgrammeNo ASC) AS RowNo, * from (SELECT  * FROM EXCEPTION_04 UNION SELECT  * FROM EXCEPTION_03) T0 ) T1 ) T1B ON CONVERT(INT,T1B.ProgrammeNo) = T1.Register_Id --AND T1B.RowNo=1
    LEFT JOIN (	SELECT T1.SubLocation_Id,T1.SubLocation_Code,T1.SubLocation_Name,T2.Location_Code,T2.Location_Name,T3.Division_Code,T3.Division_Name,T4.District_Code,T4.District_Name,T5.County_Code,T5.County_Name
											FROM [DGSD].dbo.SubLocations T1 INNER JOIN [DGSD].dbo.Locations T2 ON T1.Location_Id=T2.Location_Id	
																		    INNER JOIN [DGSD].dbo.Divisions T3 ON T2.Division_Id=T3.Division_Id
																		    INNER JOIN [DGSD].dbo.Districts T4 ON T3.District_Id=T4.District_Id
																		    INNER JOIN [DGSD].dbo.Counties T5 ON T4.County_Id=T5.County_Id
											) T2 ON T1.SubLocation_Id=T2.SubLocation_Id
							LEFT JOIN [DGSD].dbo.Constituencies T3 ON T1.Constituency_Id=T3.Constituency_Id
							LEFT JOIN [DGSD].dbo.Grades T4 ON T1.Walls=T4.Grade_Code AND T4.Parameter_Code=@HousePartCode_WALL
							LEFT JOIN [DGSD].dbo.Grades T5 ON T1.[Floor]=T5.Grade_Code AND T5.Parameter_Code=@HousePartCode_FLOOR
							LEFT JOIN [DGSD].dbo.Grades T6 ON T1.[Roof]=T6.Grade_Code AND T6.Parameter_Code=@HousePartCode_ROOF
							LEFT JOIN [DGSD].dbo.Grades T7 ON T1.Toilet=T7.Grade_Code AND T7.Parameter_Code=@ToiletCode
							LEFT JOIN [DGSD].dbo.Grades T8 ON T1.Water=T8.Grade_Code AND T8.Parameter_Code=@WaterSourceCode
							LEFT JOIN [DGSD].dbo.Grades T9 ON T1.Lighting=T9.Grade_Code AND T9.Parameter_Code=@LightingSourceCode
							LEFT JOIN [DGSD].dbo.Grades T10 ON T1.Fuel=T10.Grade_Code AND T10.Parameter_Code=@FuelSourceCode
							LEFT JOIN [DGSD].dbo.Grades T11 ON T1.Benefit_Code=T11.Grade_Code AND T11.Parameter_Code=@BenefitTypeCode
							LEFT JOIN [DGSD].dbo.Company T12 ON T1.Program_Code=T12.Code
							LEFT JOIN [DGSD].dbo.Paypoints T13 ON '1016'=T13.Paypoint_Code
							LEFT JOIN [DGSD].dbo.Agencies T14 ON T13.Agency_Code=T14.Agency_Code
							LEFT JOIN [DGSD].dbo.Logins T15 ON T1.DEC_Code=T15.Login_Code
							LEFT JOIN HhRegistration T17 ON T1.Register_Id=T17.HhId AND T17.ProgrammeId=@ProgrammeId
WHERE   T17.Id IS NULL 
END



-->>2. HOUSEHOLD MEMBERSHIP
IF @MIG_STAGE=2
BEGIN
	DECLARE @MarritalCode int			

	SET @MarritalCode=7

	 INSERT INTO HhMembers(MemberId,HhId,HhRegistrationId,FirstName,MiddleName,Surname,Sex,DateOfBirth,BirthCertNo,NationalIdNo,MarritalStatus,EducationLevel,AttendanceSchool,IllnessType,DisabilityType,NCPWDNo,AttendanceHeathCentre,Relationship,OccupationType,IsExited)
	SELECT T1.Member_Code,T2.Register_Id,T3.Id,T1.FirstName,T1.MiddleName,T1.Surname,CASE (ISNULL(T1.Gender,0)) WHEN 0 THEN 'M' ELSE 'F' END AS Sex,T1.DateOfBirth,T1.BirthCertNo,T1.IdNo AS IdNumber,T5.Grade_Name AS MarritalStatus,LTRIM(RIGHT(T4.Education_Name,LEN(T4.Education_Name)-CHARINDEX('.',T4.Education_Name,0))) AS EducationName,NULL AS EducationCentreName,T6.Illness_Name,T1.Disability_Code AS DisabilityType,T1.Disability_No AS NCPWDNo,NULL AS HealthCentreName,LTRIM(RIGHT(T8.Relationship_Name,LEN(T8.Relationship_Name)-CHARINDEX('.',T8.Relationship_Name,0))) AS RelationshipName,LTRIM(RIGHT(T9.Livelihood_Name,LEN(T9.Livelihood_Name)-CHARINDEX(']',T9.Livelihood_Name,0))) AS Occupation,0 AS IsExited
	FROM [DGSD].dbo.Members T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.Register_No=T2.Register_No
							   INNER JOIN HhRegistration T3 ON T2.Register_Id=T3.HhId AND T3.ProgrammeId=@ProgrammeId
							 INNER JOIN  (select * from ( SELECT ROW_NUMBER() OVER(PARTITION BY  T0.ProgrammeNo ORDER BY  T0.ProgrammeNo ASC) AS RowNo, * from (SELECT  * FROM EXCEPTION_04 UNION SELECT  * FROM EXCEPTION_03) T0 ) T1 ) T1B ON T1B.ProgrammeNo = T2.Register_Id AND T1B.RowNo=1
							   LEFT JOIN [DGSD].dbo.Education T4 ON T1.Education_Code=T4.Education_Code
							   LEFT JOIN [DGSD].dbo.Grades T5 ON T1.Marital_Code=T5.Grade_Code AND T5.Parameter_Code=@MarritalCode
							   LEFT JOIN [DGSD].dbo.Illnesses T6 ON T1.Illness_Code=T6.Illness_Code
							   LEFT JOIN (
											SELECT Member_Code,MIN(Relationship_Code) AS Relationship_Code 
										    FROM [DGSD].dbo.MemberRelationships 
										    GROUP BY Member_Code
										   ) T7 ON T1.Member_Code=T7.Member_Code
							   LEFT JOIN [DGSD].dbo.Relationships T8 ON T7.Relationship_Code=T8.Relationship_Code
							   LEFT JOIN [DGSD].dbo.Livelihoods T9 ON T1.Occupation_Code=T9.Livelihood_Code
							   LEFT JOIN HhMembers T10 ON T1.Member_Code=T10.MemberId AND T2.Register_Id=T10.HhId
	 WHERE  T10.Id IS NULL
END



-->>3. BENEFICIARY PAYMENT
IF @MIG_STAGE=3
BEGIN
 	INSERT INTO Payment(PaymentId,HhId,HhRegistrationId,CycleId,CycleName,CycleStartDate,CycleEndDate,PaymentDate,Approved,Notes,PSPBranch,PSP,PrimaryRecipientName,PrimaryRecipientNationalIdNo,SecondaryRecipientName,SecondaryRecipientNationalIdNo,Amount,PaidAmount,PaidTrxNo,GeneratedBy)
	SELECT T1.Payment_Code,T3.Register_Id,T4.Id,T2.Cycle_Code,CONVERT(varchar(50),T2.Cycle_Name),T2.[Start_Date],T2.End_Date,NULL AS DatePaid,T1.Payment_Approved,T1.Payment_Notes,CONVERT(varchar(50),CASE WHEN(T1.Cycle_Code<54) THEN 'PCK' ELSE 'KCB' END),CONVERT(varchar(50),CASE WHEN(T1.Cycle_Code<54) THEN 'Postal Corporation of Kenya' ELSE 'Kenya Commercial Bank' END),CONVERT(varchar(100),T1.Primary_Recipient),T1.HHH_NationalID,CONVERT(varchar(100),T1.Secondary_Recipient),T1.HHS_NationalID,T1.Payment_Amount,ISNULL(T1.Amount_Paid,0),T1.Transaction_No,'' AS GeneratedBy
	FROM [DGSD].dbo.Payments T1 INNER JOIN [DGSD].dbo.PaymentCycles T2 ON T1.Cycle_Code=T2.Cycle_Code
								INNER JOIN [DGSD].dbo.Register T3 ON T1.Register_No=T3.Register_No
								INNER JOIN HhRegistration T4 ON T3.Register_Id=T4.HhId AND T4.ProgrammeId=@ProgrammeId
								 INNER JOIN  (select * from ( SELECT ROW_NUMBER() OVER(PARTITION BY  T0.ProgrammeNo ORDER BY  T0.ProgrammeNo ASC) AS RowNo, * from (SELECT  * FROM EXCEPTION_04 UNION SELECT  * FROM EXCEPTION_03) T0 ) T1 ) T1B ON T1B.ProgrammeNo = T3.Register_Id AND T1B.RowNo=1
								LEFT JOIN [DGSD].dbo.Paypoints T6 ON T3.Branch_Code=T6.Paypoint_Code
								LEFT JOIN [DGSD].dbo.Agencies T7 ON T6.Agency_Code=T7.Agency_Code
								LEFT JOIN Payment T9 ON T1.Payment_Code=T9.PaymentId AND T3.Register_Id=T9.HhId
	WHERE T9.Id IS NULL
END



-->>4. COMPLAINTS
IF @MIG_STAGE=4
BEGIN
	INSERT INTO CaseGrievance(CaseId,SourceProgrammeId,SourceProgramme,SerialNo,ProgrammeId,Programme,GrievanceTypeId,GrievanceTypeName,GrievanceDescription,CaseHistory,Constituency
	,SubLocation,Village,Community,HhId,HhRegistrationId,IsBeneficiary,ComplainantType,ComplainantName,ComplainantSex,ComplainantAge,ComplainantTel,ReceiptOn,ReceivedBy,ReceiptLevel
	,CreatedOn,CreatedBy,VerifiedOn,VerifiedBy,ActionNotes,CaseStatus,FeedbackOn,Resolution,ResolvedOn,ResolvedBy,ClosedOn,ClosedBy
	)	
	
	SELECT T1.Grievance_Code,@ProgrammeId AS SourceProgrammeId,@Programme AS SourceProgramme,T1.Serial_No,T3.Id,T3.Name,T1.Type_Code,T4.[Type_Name],T1.Grievance_Description,T1.Case_History,T5.Constituency_Name
	,T6.SubLocation_Name,T1.Village,T1.Community_Name,T7.Register_Id,T8.Id,CASE WHEN(T1.Beneficiary='YES') THEN 'Yes' ELSE 'No' END AS IsBeneficiary,CASE(T1.Complaint_Type) WHEN 0 THEN 'Individual/Household' WHEN 1 THEN 'Community' WHEN 2 THEN ISNULL(T1.Other_Complaint,'Other') ELSE NULL END AS ComplainantType,T1.Complainant AS ComplainantName,CASE (ISNULL(T1.Gender,0)) WHEN 0 THEN 'M' ELSE 'F' END AS Sex,CONVERT(varchar(10),T1.Age) AS ComplainantAge,T1.Phone,ISNULL(ISNULL(T1.SPR_ReceiptDate,T1.Grievance_Date),ISNULL(T1.Referral_Date,T1.Resolution_Date)),T1.Staff_Names,NULL AS ReceiptLevel
	,T1.SPR_ReceiptDate AS GrievanceDate,T1.Staff_Names AS EnteredBy,NULL AS DateVerified,NULL AS VerifiedBy,T1.Resolution_Description AS ActionTaken,CASE WHEN(T1.Resolution_Date<=GETDATE() AND T1.Resolved=1) THEN 'Resolved' WHEN T1.Resolved=1 THEN 'Resolved' WHEN T1.Verified=1 THEN 'Verified' ELSE 'Opened' END AS CaseStatus,T1.Referral_Date AS FeedbackDate,T1.Resolution_Description,T1.Resolution_Date AS ResolvedOn,NULL AS ResolvedBy,T1.ActualResolution_Date AS DateClosed,NULL AS ClosedBy
	FROM [DGSD].dbo.Grievances T1 LEFT JOIN [UAT_CCTPMIS].dbo.Programme T3 ON T3.Id=@ProgrammeId
								  LEFT JOIN [DGSD].dbo.GrievanceTypes T4 ON T1.Type_Code=T4.Type_Code
								  LEFT JOIN [DGSD].dbo.Constituencies T5 ON T1.Constituency_Id=T5.Constituency_Id
								  LEFT JOIN [DGSD].dbo.SubLocations T6 ON T1.SubLocation_Id=T6.SubLocation_Id
								  LEFT JOIN [DGSD].dbo.Register T7 ON T1.Register_No=T7.Register_No AND T7.Company_Code=12
								  LEFT JOIN HhRegistration T8 ON T7.Register_Id=T8.HhId AND T8.ProgrammeId=@ProgrammeId
								  LEFT JOIN CaseGrievance T10 ON T1.Grievance_Code=T10.CaseId AND T10.SourceProgrammeId=@ProgrammeId
	WHERE T10.Id IS NULL AND (T7.Register_Id>0 OR ISNULL(T1.Register_No,'')='')
END



-->>5. UPDATES
IF @MIG_STAGE=5
BEGIN
	INSERT INTO CaseUpdate(CaseId,SourceProgrammeId,SourceProgramme,UpdateType,UpdateName,UpdateReason,UpdateDate,HhId,HhRegistrationId,OldCGMemberId,NewCGMemberId,OldCGHhMemberId,NewCGHhMemberId
		,FirstName,MiddleName,Surname,Sex,DateOfBirth,IDNumber,MarritalStatus,EducationLevel,AttendanceSchool,EducationChangeReason,IsChronicallyIll,IsDisabled,AttendanceHeathCentre,HealthChangeReason
		,Relationship,OldSubLocation,NewSubLocation,PSPBranch,PSP,ReplacePaymentCard,PSPChangeReason,AlternativeRecipientName,AlternativeRecipientIDNumber,CreatedOn,CreatedBy,ApprovedOn,ApprovedBy,ActionNotes
		)
	
	SELECT T1.Case_Code,@ProgrammeId AS SourceProgrammeId,@Programme AS SourceProgramme,CONVERT(varchar(50),T1.Case_Type) AS UpdateType,CONVERT(varchar(50),T1.Case_Type) AS UpdateName,CONVERT(varchar(100),T1.Case_Name) AS ReasonName,T1.Case_Date,T2.Register_Id,T4.Id,T5.Member_Code,NULL AS NewCaregiver,T6.Id,NULL AS Id
		,CONVERT(varchar(50),T1.Rep_FirstName),CONVERT(varchar(50),T1.Rep_MiddleName),CONVERT(varchar(50),T1.Rep_Surname),CASE (ISNULL(T1.Rep_Gender,0)) WHEN 0 THEN 'M' ELSE 'F' END AS Sex,T1.Rep_DateOfBirth,CONVERT(varchar(30),T1.Rep_IdNo) AS IdNumber,NULL AS MarritalStatus,NULL AS EducationName,NULL AS SchoolName,NULL AS EducationChangeReason,NULL AS ChronicallyIll,NULL AS IsDisabled,NULL AS HealthCentreName,NULL AS HealthChangeReason
		,NULL AS RelationshipName,NULL AS OldSubLocation,NULL AS NewSubLocation,NULL AS AgencyBranchName,NULL AS AgencyName,NULL AS ReplacePaymentCard,NULL AS PSPChangeReason,NULL AS AlternateRepName,NULL AS AlternateRepIdNumber,T1.Case_Date AS DateCreated,ISNULL(T8.Other_Names,'')+ISNULL(' '+T8.Surname,'') AS CreatedBy,NULL AS ApprovalDate,NULL AS ApprovedBy,NULL AS Comments
	FROM [DGSD].dbo.Cases T1 INNER JOIN [DGSD].dbo.Register T2 ON T1.Register_No=T2.Register_No AND T2.Company_Code=12
							 LEFT JOIN HhRegistration T4 ON T2.Register_Id=T4.HhId AND T4.ProgrammeId=@ProgrammeId
							 LEFT JOIN [DGSD].dbo.Members T5 ON T1.Member_Code=T5.Member_Code
							 LEFT JOIN HhMembers T6 ON T2.Register_Id=T6.HhId AND T5.Member_Code=T6.MemberId
							 LEFT JOIN [DGSD].dbo.Logins T8 ON T1.Created_By=T8.Login_Code
							 LEFT JOIN CaseUpdate T14 ON T1.Case_Code=T14.CaseId AND T14.SourceProgrammeId=@ProgrammeId
	WHERE T14.Id IS NULL
END

GO







