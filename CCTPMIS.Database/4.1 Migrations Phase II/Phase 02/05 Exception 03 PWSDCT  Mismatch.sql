

DECLARE @SysCode varchar(30)
	   ,@SysDetailCode varchar(30)
DECLARE @SysCodeDetailId1 int
	   ,@SysCodeDetailId2 int
	   ,@SysCodeDetailId3 int
	   ,@SysCodeDetailId4 int
	   ,@SysCodeDetailId5 int
	   ,@SysCodeDetailId6 int
	   ,@SysCodeDetailId7 int
DECLARE @MIG_STAGE tinyint


SET @MIG_STAGE = 5

--=====================================	HOUSEHOLD	====================================
IF @MIG_STAGE=1
BEGIN
	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Code='PwSD-CT')
	BEGIN
		SET @SysCode='Beneficiary Type'
		SET @SysDetailCode='HOUSEHOLD'
		SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='BENEFICIARY'
		SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		SET @SysDetailCode='CAREGIVER'
		SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Payment Frequency'
		SET @SysDetailCode='BIMONTHLY'
		SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO Programme(Code,Name,BeneficiaryTypeId,IsActive,PrimaryRecipientId,SecondaryRecipientId,SecondaryRecipientMandatory,BeneProgNoPrefix,PaymentFrequencyId,CreatedBy,CreatedOn)
		SELECT 'PwSD-CT' AS Code,'Persons with Severe Disability Cash Transfer Programme' AS Name,@SysCodeDetailId1 AS BeneficiaryTypeId,1 AS IsActive,@SysCodeDetailId3 AS PrimaryRecipientId,NULL AS SecondaryRecipientId,0 AS SecondaryRecipientMandatory,3 AS BeneProgNoPrefix,@SysCodeDetailId4 AS PaymentFrequencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	END

	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy PwSD MIS 2'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
		EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries Phase 2',@OrderNo=2,@UserId=1;

	SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	SET @SysCode='HHStatus'
	SET @SysDetailCode='VALPASS'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='VALFAIL'
	SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId3=Id FROM Programme WHERE Code='PwSD-CT'
	
	INSERT INTO Household(ProgrammeId,RegGroupId,RefId,Village,StatusId,SourceId,CreatedBy,CreatedOn)
	SELECT @SysCodeDetailId3 AS ProgrammeId,@SysCodeDetailId2,T2.Id AS RefId,T2.Village,@SysCodeDetailId1,2 AS SourceId,1,GETDATE()
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 
	INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
	INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId3 or t2.ProgrammeId=5
	LEFT JOIN Household T3 ON T3.ProgrammeId=@SysCodeDetailId3 AND T3.RegGroupId=@SysCodeDetailId2 AND T3.SourceId=2  AND T3.RefId=T2.Id
	WHERE  T3.Id IS NULL
	 
END

--=====================================	HOUSEHOLD SUBLOCATION	====================================
IF @MIG_STAGE=2
BEGIN
	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy PwSD MIS 2'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId2=Id FROM Programme WHERE Code='PwSD-CT'

	INSERT INTO HouseholdSubLocation(HhId,GeoMasterId,SubLocationId)
	SELECT T3.Id AS HhId,1 AS GeoMasterId,T4.Id AS SubLocationId
	FROM [LegacyDB].dbo.ActivePwSDBeneficiaries T1
	INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
	 INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
												   INNER JOIN Household T3 ON T3.ProgrammeId=@SysCodeDetailId2 AND T3.RegGroupId=@SysCodeDetailId1 AND T3.SourceId=2 AND T2.Id=T3.RefId
												   INNER JOIN Sublocation T4 ON T1.SubLocationCode COLLATE DATABASE_DEFAULT=T4.Code COLLATE DATABASE_DEFAULT
												   LEFT JOIN HouseholdSubLocation T5 ON T3.Id=T5.HhId AND T5.GeoMasterId=1 AND T4.Id=T5.SubLocationId
	WHERE T5.HhId IS NULL
END



--=====================================	HOUSEHOLD ENROLMENT	====================================
IF @MIG_STAGE=3
BEGIN
	SET @SysCode='Enrolment Group'
	SET @SysDetailCode='Legacy MIS 2 '
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
		EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy MIS Beneficiaries Phase 2',@OrderNo=2,@UserId=1;

	--SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	
	--INSERT INTO HouseholdEnrolment(HhId,ProgrammeNo,EnrolmentGroupId,CreatedBy,CreatedOn,ApvBy,ApvOn)
	--SELECT T2.Id AS HhId,T2.Id AS ProgrammeNo,@SysCodeDetailId2 AS TargetGroupId,1 AS CreatedBy,GETDATE() AS CreatedOn,1 AS ApvBy,GETDATE() AS ApvOn
	--FROM vw_70PlusRegistration T1 INNER JOIN Household T2 ON T1.RefId=T2.RefId
END



--=====================================	PERSON	====================================
IF @MIG_STAGE=4
BEGIN
	DECLARE @DEFAULT_DoB datetime
	SET @DEFAULT_DoB='15 Sep 1989'	--DEFAULTING ANY MISSING Date Of Birth
	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy PwSD MIS 2'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId2=Id FROM Programme WHERE Code='PwSD-CT'

	  INSERT INTO Person(RefId,FirstName,MiddleName,Surname,SexId,DoB,BirthCertNo,NationalIdNo,MobileNo1,MobileNo1Confirmed,MobileNo2,MobileNo2Confirmed,SourceId,CreatedBy,CreatedOn)
	  
	
	SELECT   CONCAT('PwSDBene', CONVERT(VARCHAR,T3.Id)) AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),T3.BirthCertNo,T1.ResolvedIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
														  INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN SystemCodeDetail T5 ON ISNULL(T1.Sex,T3.Sex)=T5.Code
														  INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
														  LEFT JOIN Person T7 ON T7.SourceId=2 AND  CONCAT('PwSDBene', CONVERT(VARCHAR,T3.Id))=T7.RefId
	--WHERE   T7.Id IS NULL
	--MISSING BENE
	UNION
	SELECT  CONCAT('PwSDBene_', CONVERT(VARCHAR,T2.Id)) AS RefId,T1.FirstName,T1.MiddleName,T1.Surname,T5.Id AS SexId,ISNULL(T1.DateOfBirth,@DEFAULT_DoB),NULL AS BirthCertNo,T1.ResolvedIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
													      LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN SystemCodeDetail T5 ON CASE WHEN(ISNULL(T1.Sex,'M')='F') THEN 'F' ELSE 'M' END=T5.Code
													      INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
													      LEFT JOIN Person T7 ON T7.SourceId=2 AND T1.ResolvedIDNo=T7.NationalIdNo AND T7.RefId IS NULL -- AND T7.Id>4292582 
														  AND T1.FirstName=T7.FirstName AND T1.MiddleName=T7.MiddleName AND T1.Surname=T7.Surname
	WHERE   ISNULL(T1.MemberCode,0)<=0 AND  T7.Id IS NULL
 
	 
--	MAPPED CG
	 UNION
 
	SELECT   CONCAT('PwSDCG', CONVERT(VARCHAR,T3.Id)) AS RefId,T3.FirstName,T3.MiddleName,T3.Surname,T5.Id AS SexId,ISNULL(T3.DateOfBirth,@DEFAULT_DoB),T3.BirthCertNo,T1B.IPRS_CGIDNo AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														INNER JOIN (SELECT* FROM [LegacyDB].[dbo].[IPRS_Legacy] )T1C ON T1C.IDNumber = T1b.IPRS_CGIDNo
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
													      INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN SystemCodeDetail T5 ON CASE WHEN(ISNULL(T1.CGSex,'M')='F') THEN 'F' ELSE 'M' END=T5.Code
													      INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
													      LEFT JOIN Person T7 ON T7.SourceId=2   AND  CONCAT('PwSDCG', CONVERT(VARCHAR,T3.Id)) = T7.RefId
	 
	----MISSING CG
	UNION
	SELECT   CONCAT('PwSDCG_', CONVERT(VARCHAR,T2.Id)) AS RefId,T1C.FirstName,T1C.MiddleName,T1C.Surname,T5.Id AS SexId,ISNULL(T1C.DoB,@DEFAULT_DoB),NULL AS BirthCertNo,CONVERT(VARCHAR,T1C.IDNumber) AS NationalIdNo,NULL AS MobileNo1,0 AS MobileNo1Confirmed,NULL AS MobileNo2,0 AS MobileNo2Confirmed,2 AS SourceId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
	INNER JOIN (SELECT* FROM [LegacyDB].[dbo].[IPRS_Legacy] )T1C ON T1C.IDNumber = T1b.IPRS_CGIDNo
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId2
														  LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId2 AND T4.RegGroupId=@SysCodeDetailId1 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN SystemCodeDetail T5 ON CASE WHEN(ISNULL(T1.CGSex,'M')='F') THEN 'F' ELSE 'M' END=T5.Code
														  INNER JOIN SystemCode T6 ON T5.SystemCodeId=T6.Id AND T6.Code='Sex'
														  LEFT JOIN Person T7 ON T7.SourceId=2 AND T1.ResolvedCGIDNo=T7.NationalIdNo AND T7.RefId IS NULL  
	WHERE   ISNULL(T1.CGMemberCode,0)<=0 AND T1.ResolvedCGIDNo<>'' AND T7.Id IS NULL
	 ORDER BY RefId
END




--=====================================	MEMBER RELATIONSHIP	====================================
IF @MIG_STAGE=5
BEGIN
	SET @SysCode='Relationship'
	SET @SysDetailCode='BENEFICIARY'
	SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='CAREGIVER'
	SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Status'
	SET @SysDetailCode='1'
	SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Member Role'
	SET @SysDetailCode='BENEFICIARY'
	SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
	SET @SysDetailCode='CAREGIVER'
	SELECT @SysCodeDetailId5=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SET @SysCode='Registration Group'
	SET @SysDetailCode='Legacy PwSD MIS 2'
	SELECT @SysCodeDetailId6=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

	SELECT @SysCodeDetailId7=Id FROM Programme WHERE Code='PwSD-CT'
	/*	*/
	--INSERT INTO HouseholdMember(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
	SELECT T1.ProgrammeNumber --,T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId1 AS RelationshipId,@SysCodeDetailId4 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
													      INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN Person T5 ON T5.SourceId=2 AND    CONCAT('PwSDBene', CONVERT(VARCHAR,T3.Id))=T5.RefId
													      LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE   T6.Id IS NULL
	UNION
	SELECT T1.ProgrammeNumber--, T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId1 AS RelationshipId,@SysCodeDetailId4 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
														  LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.MemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN (
																		SELECT MIN(Id) AS Id,ISNULL(NationalIDNo,'') AS NationalIDNo,LTRIM(RTRIM(FirstName+' '+MiddleName+' '+Surname)) AS Name
																		FROM Person
																		WHERE    RefId  like 'PwSDBene_%'
																		GROUP BY NationalIdNo,LTRIM(RTRIM(FirstName+' '+MiddleName+' '+Surname))
																	) T5 ON T1.ResolvedIDNo=T5.NationalIdNo AND T1.FirstName+' '+T1.MiddleName+' '+T1.Surname=T5.Name
														  LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId AND T6.RelationshipId=@SysCodeDetailId1
	WHERE    T6.Id IS NULL

	/* */
	--INSERT INTO HouseholdMember(HhId,PersonId,RelationshipId,MemberRoleId,StatusId,CreatedBy,CreatedOn)
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId2 AS RelationshipId,@SysCodeDetailId5 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber  
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
														  INNER JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
														  INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
														  INNER JOIN Person T5 ON T5.SourceId=2 AND  CONCAT('PwSDCG', CONVERT(VARCHAR,T3.Id))=T5.RefId
														  LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId
	WHERE  --  ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND 
	T6.Id IS NULL
	UNION
	SELECT T4.Id AS HhId,T5.Id AS PersonId,@SysCodeDetailId2 AS RelationshipId,@SysCodeDetailId5 AS MemberRoleId,@SysCodeDetailId3 AS StatusId,1 AS CreatedBy,GETDATE() AS CreatedOn
	FROM [LegacyDB].dbo.ActivePwSDBeneficiariesUPDATED T1 INNER JOIN    (SELECT  * FROM [LegacyDB].dbo.EXCEPTION_03 )  T1B ON  T1B.ProgrammeNo = T1.ProgrammeNumber 
	INNER JOIN (SELECT* FROM [LegacyDB].[dbo].[IPRS_Legacy] )T1C ON T1C.IDNumber = T1b.IPRS_CGIDNo 
														  INNER JOIN [LegacyDB].dbo.HhRegistration T2 ON T1.ProgrammeNumber=T2.HhId AND T2.ProgrammeId=@SysCodeDetailId7
													      LEFT JOIN [LegacyDB].dbo.HhMembers T3 ON T2.Id=T3.HhRegistrationId AND T1.CGMemberCode=T3.MemberId
													      INNER JOIN Household T4 ON T4.ProgrammeId=@SysCodeDetailId7 AND T4.RegGroupId=@SysCodeDetailId6 AND T4.SourceId=2 AND T2.Id=T4.RefId
													      INNER JOIN (
																		SELECT MIN(Id) AS Id,NationalIDNo
																		FROM Person
																		WHERE    RefId  like 'PwSDCG_%'
																		GROUP BY NationalIdNo
																	) T5 ON CONVERT(VARCHAR,T1C.IDNumber)=T5.NationalIdNo
													      LEFT JOIN HouseholdMember T6 ON T4.Id=T6.HhId AND T5.Id=T6.PersonId
	WHERE  ISNULL(T1.CGMemberCode,0)<=0 AND ISNULL(T1.MemberCode,0)<>ISNULL(T1.CGMemberCode,-1) AND T6.Id IS NULL

END