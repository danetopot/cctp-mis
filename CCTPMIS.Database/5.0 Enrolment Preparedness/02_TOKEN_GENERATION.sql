﻿ALTER TABLE HOUSEHOLD ADD TokenId INT NULL
GO


; WITH TokenGenerator
as
(
select 
T3.Id AS HhId, ROW_NUMBER() over (order by T6.DoB) As TokenNo
from    Household T3  
INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
INNER JOIN Person T6 ON T5.PersonId=T6.Id and T3.CreatedOn>'2018-11-01'
--order by T3.Id asc
)
update Household set TokenId = TokenNo from TokenGenerator
left join Household h on h.Id = TokenGenerator.HhId
GO







SELECT 
		T4.Code Programme
		,REPLICATE('0',6-LEN(CONVERT(varchar(6),T3.TokenId)))+CONVERT(varchar(6),T3.TokenId) AS TokenId
		,T6.FirstName AS BeneFirstName
		,T6.MiddleName AS BeneMiddleName
		,T6.Surname AS BeneSurname
		,T6.NationalIdNo AS BeneIDNo
		,T13.County
		,T13.Constituency
		,T13.District
		,T13.Division
		,T13.Location
		,T13.SubLocation
								  FROM  Household T3  -- ON T2.HhId=T3.Id
								   INNER JOIN Programme T4 ON T3.ProgrammeId=T4.Id
								   INNER JOIN HouseholdMember T5 ON T3.Id=T5.HhId AND T4.PrimaryRecipientId=T5.MemberRoleId
								   INNER JOIN Person T6 ON T5.PersonId=T6.Id
								   INNER JOIN SystemCodeDetail T7 ON T6.SexId=T7.Id
								   LEFT JOIN HouseholdMember T8 ON T3.Id=T8.HhId AND T4.SecondaryRecipientId=T8.MemberRoleId
								   LEFT JOIN Person T9 ON T8.PersonId=T9.Id
								   LEFT JOIN SystemCodeDetail T10 ON T9.SexId=T10.Id
								   INNER JOIN HouseholdSubLocation T11 ON T3.Id=T11.HhId
								   INNER JOIN GeoMaster T12 ON T11.GeoMasterId=T12.Id AND T12.IsDefault=1
								   INNER JOIN (SELECT T8.Id AS GeoMasterId,T1.Id AS SubLocationId,T1.Name AS SubLocation,T2.Name AS Location,T3.Name AS Division,T5.Name AS District,T6.Name AS County,T7.Name AS Constituency
											   FROM SubLocation T1 INNER JOIN Location T2 ON T1.LocationId=T2.Id
																   INNER JOIN Division T3 ON T2.DivisionId=T3.Id
																   INNER JOIN CountyDistrict T4 ON T3.CountyDistrictId=T4.Id
																   INNER JOIN District T5 ON T4.DistrictId=T5.Id
																   INNER JOIN County T6 ON T4.CountyId=T6.Id
																   INNER JOIN Constituency T7 ON T1.ConstituencyId=T7.Id
																   INNER JOIN GeoMaster T8 ON T6.GeoMasterId=T8.Id
										  ) T13 ON T11.SubLocationId=T13.SubLocationId AND T12.Id=T13.GeoMasterId
 WHERE T3.TokenId IS NOT NULL
	 order by T13.County, T13.Constituency, T13.Location, T13.SubLocation

