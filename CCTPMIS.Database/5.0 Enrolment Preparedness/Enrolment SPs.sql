﻿


IF NOT OBJECT_ID('PSPEnrolBeneficiary') IS NULL	
   DROP PROC PSPEnrolBeneficiary
GO
CREATE PROC PSPEnrolBeneficiary  
    @EnrolmentNo INT  
   ,@TokenId INT  

   ,@BankCode NVARCHAR(20)  
   ,@BranchCode NVARCHAR(20)  

   ,@AccountNo VARCHAR(50)  
   ,@AccountName VARCHAR(100)  
   ,@AccountOpenedOn DATETIME  

   ,@PriReciFirstName VARCHAR(50)  
   ,@PriReciMiddleName VARCHAR(50)=NULL  
   ,@PriReciSurname VARCHAR(50)  
   ,@PriReciNationalIdNo VARCHAR(30)  
   ,@PriReciSex CHAR(1)  
   ,@PriReciDoB DATETIME=NULL  

   ,@SecReciFirstName VARCHAR(50)=NULL  
   ,@SecReciMiddleName VARCHAR(50)=NULL  
   ,@SecReciSurname VARCHAR(50)=NULL  
   ,@SecReciNationalIdNo VARCHAR(30)=NULL  
   ,@SecReciSex CHAR(1)=NULL  
   ,@SecReciDoB DATETIME=NULL  

   ,@PaymentCardNo VARCHAR(50)= NULL  
   ,@MobileNo1 NVARCHAR(20)=NULL  
   ,@MobileNo2 NVARCHAR(20)=NULL  
   ,@UserId INT  
AS  
BEGIN  
 SET NOCOUNT ON  
  
 DECLARE @HhEnrolmentId int  
 DECLARE @AccountId int  
 DECLARE @PaymentCardId int  
 DECLARE @PriReciId int  
 DECLARE @SecReciId int  
 DECLARE @SecReciMandatory bit  
 DECLARE @SysCode varchar(20)  
 DECLARE @SysDetailCode varchar(20)  
 DECLARE @SystemCodeDetailId1 int  
 DECLARE @SystemCodeDetailId2 int  
 DECLARE @SystemCodeDetailId3 int  
 DECLARE @SystemCodeDetailId4 int  
 DECLARE @SystemCodeDetailId5 int  
 DECLARE @AccountExpiryDate datetime  
 DECLARE @EnrolmentLimit int  
 DECLARE @ErrorMsg varchar(128)  
 DECLARE @NoOfRows int  
  DECLARE @ProgrammeCode varchar(10)
    
 SET @EnrolmentLimit=523129  

 SET @SysCode='Enrolment Status'  
 SET @SysDetailCode='PSPENROL'  
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Member Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Account Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId5=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Sex'  
 SET @SysDetailCode=@PriReciSex  
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  

 SET @SysDetailCode=@SecReciSex  
 SELECT @SystemCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SELECT @PriReciId=T1.Id,@PriReciDoB=ISNULL(@PriReciDoB,T1.DoB)  
 FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2  
       INNER JOIN HouseholdEnrolment T3 ON T3.Id=@EnrolmentNo AND T2.HhId=T3.HhId  
       INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id -- AND T4.StatusId=@SystemCodeDetailId1  
       INNER JOIN Household T5 ON T3.HhId=T5.Id  
       INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.PrimaryRecipientId  
 WHERE T1.FirstName=@PriReciFirstName AND T1.MiddleName=ISNULL(@PriReciMiddleName,'') 
 AND T1.Surname=@PriReciSurname 
 --AND T1.NationalIdNo=@PriReciNationalIdNo 
 AND T1.SexId=@SystemCodeDetailId3 --AND T1.DoB=@PriReciDoB   
    
 SELECT @HhEnrolmentId=T3.Id,@SecReciId=T1.Id,@SecReciDoB=ISNULL(@SecReciDoB,T1.DoB) 
 FROM Person T1 INNER JOIN HouseholdMember T2 ON T1.Id=T2.PersonId AND T2.StatusId=@SystemCodeDetailId2  
       INNER JOIN HouseholdEnrolment T3 ON T2.HhId=T3.HhId  
       INNER JOIN HouseholdEnrolmentPlan T4 ON T3.HhEnrolmentPlanId=T4.Id AND T4.StatusId=@SystemCodeDetailId1  
       INNER JOIN Household T5 ON T3.HhId=T5.Id  
       INNER JOIN Programme T6 ON T5.ProgrammeId=T6.Id AND T2.MemberRoleId=T6.SecondaryRecipientId  
 WHERE T1.FirstName=@SecReciFirstName AND T1.MiddleName=ISNULL(@SecReciMiddleName,'') AND T1.Surname=@SecReciSurname 
 AND T1.NationalIdNo=@SecReciNationalIdNo AND T1.SexId=@SystemCodeDetailId4 --AND T1.DoB=@SecReciDoB   
        
 SELECT @SecReciMandatory=T3.SecondaryRecipientMandatory , @ProgrammeCode = T3.Code
 FROM HouseholdEnrolment T1 INNER JOIN Household T2 ON T1.HhId=T2.Id INNER JOIN Programme T3 ON T2.ProgrammeId=T3.Id   
 WHERE T1.Id=@EnrolmentNo  
 
  IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id INNER JOIN Household T3 ON T1.HhId=T3.Id WHERE T1.Id=@EnrolmentNo AND T3.TokenId = @TokenId AND T2.StatusId=@SystemCodeDetailId1)  
  SET @ErrorMsg='Please specify valid Corresponding EnrolmentNo and TokenID parameter'  

 ELSE IF NOT EXISTS(SELECT 1 FROM HouseholdEnrolment T1 INNER JOIN HouseholdEnrolmentPlan T2 ON T1.HhEnrolmentPlanId=T2.Id WHERE T1.Id=@EnrolmentNo AND T2.StatusId=@SystemCodeDetailId1)  
  SET @ErrorMsg='Please specify valid EnrolmentNo parameter'  

 ELSE IF NOT EXISTS(SELECT 1 FROM PSP WHERE Code=@BankCode AND IsActive=1)  
  SET @ErrorMsg='Please specify valid BankCode parameter'  
 ELSE IF NOT EXISTS(SELECT 1 FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode)  
  SET @ErrorMsg='Please specify valid BranchCode parameter'  
 ELSE IF RTRIM(LTRIM(ISNULL(@AccountNo,'')))=''  
  SET @ErrorMsg='Please specify valid AccountNo parameter'  
 ELSE IF RTRIM(LTRIM(ISNULL(@AccountName,'')))=''  
  SET @ErrorMsg='Please specify valid AccountName parameter'  
 ELSE IF @AccountOpenedOn IS NULL OR NOT (@AccountOpenedOn<=GETDATE())  
  SET @ErrorMsg='Please specify valid AccountOpenedOn parameter'  
 ELSE IF ISNULL(@PriReciId,0)=0  
  SET @ErrorMsg='Please specify valid Primary Recipient details'  
 ELSE IF((@SecReciMandatory=1 OR ISNULL(@SecReciDoB,@SecReciFirstName)<>'') AND ISNULL(@SecReciId,0)=0)  
  SET @ErrorMsg='Please specify valid Secondary Recipient details'  
 ELSE IF NOT(@HhEnrolmentId=@EnrolmentNo)  
  SET @ErrorMsg='The EnrolmentNo and caregiver details do not match'  
 ELSE IF ISNULL(@MobileNo1,'')<>'' AND (CASE WHEN(@MobileNo1 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0  
  SET @ErrorMsg='Please specify valid MobileNo1 parameter'  
 ELSE IF ISNULL(@MobileNo2,'')<>'' AND (CASE WHEN(@MobileNo2 LIKE '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') THEN 1 ELSE 0 END)=0  
  SET @ErrorMsg='Please specify valid MobileNo2 parameter'  
 ELSE IF NOT EXISTS(SELECT 1 FROM [User] T1 INNER JOIN PSP T2 ON T1.Id=T2.UserId WHERE T1.Id=@UserId AND T2.IsActive=1 AND T2.Code=@BankCode )  
  SET @ErrorMsg='Please specify valid UserId parameter'  
 ELSE IF EXISTS(SELECT 1   
       FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id   
             INNER JOIN PSP T3 ON T2.PSPId=T3.Id  
       WHERE HhEnrolmentId=@EnrolmentNo AND ExpiryDate>GETDATE()   
      AND NOT(T3.Code=@BankCode AND T2.Code=@BranchCode AND T3.UserId=@UserId)  
       )  
  SET @ErrorMsg='The household appears to have an existing account with a PSP'  
 --ELSE IF EXISTS(SELECT 1 FROM VAL_NAME_MISMATCH WHERE OP_IDNO=@PriReciNationalIdNo) --THIS IS A TEMP STOP GAP AND SHOULD BE REMOVED ONCE THE DATA HAS BEEN ADDRESSED  
--  SET @ErrorMsg='The beneficiary appears to be in the secluded list for field validation and cannot be enroled at the moment'  

 ELSE IF (SELECT COUNT(DISTINCT T1.HhEnrolmentId) FROM BeneficiaryAccount T1 INNER JOIN HouseholdEnrolment T2 ON T1.HhEnrolmentId = T2.Id INNER JOIN Household  T3 ON T2.HhId = T3.Id AND T1.StatusId=@SystemCodeDetailId5 AND T3.TokenId is null)>=@EnrolmentLimit   
  SET @ErrorMsg='The beneficiary enrolment numbers limit has exceeded. Currently, no more beneficiary enrolment will be allowed for this Plan'  
 ELSE IF EXISTS(   SELECT 1   
       FROM BeneficiaryAccount T1 INNER JOIN PSPBranch T2 ON T1.PSPBranchId=T2.Id   
             INNER JOIN PSP T3 ON T2.PSPId=T3.Id  
       WHERE T1.HhEnrolmentId=@EnrolmentNo  
    )  
  SET @ErrorMsg='Once a beneficiary has been enrolled with a valid account number it cannot be changed'

 IF ISNULL(@ErrorMsg,'')<>''  
 BEGIN  
  RAISERROR(@ErrorMsg,16,1)  
  RETURN  
 END  
   
 BEGIN TRAN  
  
 SET @SysCode='System Settings'  
 SET @SysDetailCode='BENEPSPRENEWALDATE'  
 SELECT @AccountExpiryDate=CASE WHEN(ISNULL(T1.[Description],'')='' OR CONVERT(datetime,T1.[Description])<GETDATE()) THEN CONVERT(datetime,'30 June '+CONVERT(varchar(4),CASE WHEN(GETDATE()>CONVERT(datetime,'30 June '+CONVERT(varchar(4),YEAR(GETDATE())))) 
THEN YEAR(GETDATE())+1 ELSE YEAR(GETDATE()) END))  
                                 ELSE CONVERT(datetime,T1.[Description])  
         END  
 FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 SET @SysCode='Account Status'  
 SET @SysDetailCode='1'  
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
    
  INSERT INTO BeneficiaryAccount(HhEnrolmentId,PSPBranchId,AccountNo,AccountName,OpenedOn,StatusId,ExpiryDate)  
  SELECT @EnrolmentNo AS HhEnrolmentId  
   ,(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AS PSPBranchId  
   ,@AccountNo AS AccountNo  
   ,@AccountName AS AccountName  
   ,@AccountOpenedOn AS OpenedOn  
   ,@SystemCodeDetailId1 AS StatusId  
   ,@AccountExpiryDate AS ExpiryDate  
  
  SELECT @AccountId=Id FROM BeneficiaryAccount WHERE PSPBranchId=(SELECT T1.Id FROM PSPBranch T1 INNER JOIN PSP T2 ON T1.PSPId=T2.Id WHERE T1.Code=@BranchCode AND T2.Code=@BankCode) AND AccountNo=@AccountNo  

  
 SET @SysCode='Card Status'  
 SET @SysDetailCode= CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN '-1' ELSE '1' END  
 SELECT @SystemCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
   
 SELECT @PaymentCardId=MAX(Id) FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId  
   
  INSERT INTO BeneficiaryPaymentCard(BeneAccountId,PriReciId,PriReciFirstname,PriReciMiddleName,PriReciSurname,PriReciNationalIdNo,PriReciSexId,PriReciDoB,SecReciId,SecReciFirstname,SecReciMiddleName,SecReciSurname,SecReciNationalIdNo,SecReciSexId,SecReciDoB,PaymentCardNo,MobileNo1,MobileNo2,StatusId,CreatedBy,CreatedOn)  
  SELECT @AccountId,@PriReciId,@PriReciFirstName,@PriReciMiddleName,@PriReciSurname,@PriReciNationalIdNo,@SystemCodeDetailId3,@PriReciDoB,@SecReciId,@SecReciFirstName,@SecReciMiddleName,@SecReciSurname,@SecReciNationalIdNo,@SystemCodeDetailId4,@SecReciDoB,@PaymentCardNo,@MobileNo1,@MobileNo2,@SystemCodeDetailId2,@UserId,GETDATE()  
   
  SELECT @PaymentCardId=Id FROM BeneficiaryPaymentCard WHERE BeneAccountId=@AccountId AND CreatedBy=@UserId AND StatusId=@SystemCodeDetailId2  
  
  
  
 SET @SysCode='HHStatus'  
 SET @SysDetailCode='ENRL'  
 SELECT @SystemCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
 SET @SysDetailCode=CASE WHEN(ISNULL(@PaymentCardNo,'')='') THEN 'ENRLPSP' ELSE 'PSPCARDED' END  
 SELECT @SystemCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode  
  
 UPDATE T1  
 SET T1.StatusId=@SystemCodeDetailId3  
    ,T1.ModifiedBy=@UserId  
    ,T1.ModifiedOn=GETDATE()  
 FROM Household T1 INNER JOIN HouseholdEnrolment T2 ON T1.Id=T2.HhId  
 WHERE T2.Id=@EnrolmentNo AND T1.StatusId=@SystemCodeDetailId1  
  
 SET @NoOfRows=@@ROWCOUNT  
   
 IF @@ERROR>0  
 BEGIN  
  ROLLBACK TRAN  
  SELECT NULL AS BeneAccountId,NULL AS PaymentCardId,NULL AS PriReciId,NULL AS SecReciId  
 END  
 ELSE  
 BEGIN  
  COMMIT TRAN  
  SELECT @AccountId AS BeneAccountId,@PaymentCardId AS PaymentCardId,@PriReciId AS PriReciId,@SecReciId AS SecReciId  
 END  
END 
GO 


 