-- CHANGE THE USERS TABLE TO ENSURE THAT THE USERS CAN LOGIN
UPDATE [USER] SET EmailConfirmed=1, PasswordChangeDate = DATEADD(DAY,-10, GETDATE())
---STEP 01 - CREATE PWSDCT AND CTOVC PROGRAMMES

ALTER TABLE  Household add TokenId int null 

DECLARE @SysCode varchar(30)
	   ,@SysDetailCode varchar(30)
DECLARE @SysCodeDetailId1 int
	   ,@SysCodeDetailId2 int
	   ,@SysCodeDetailId3 int
	   ,@SysCodeDetailId4 int
	   ,@SysCodeDetailId5 int
	   ,@SysCodeDetailId6 int
DECLARE @MIG_STAGE tinyint

SET @MIG_STAGE =1

IF @MIG_STAGE=1
BEGIN
	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Code='CT-OVC')
	BEGIN
		SET @SysCode='Beneficiary Type'
		SET @SysDetailCode='HOUSEHOLD'
		SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='BENEFICIARY'
		SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='CAREGIVER'
		SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Payment Frequency'
		SET @SysDetailCode='BIMONTHLY'
		SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO Programme(Code,Name,BeneficiaryTypeId,IsActive,PrimaryRecipientId,SecondaryRecipientId,SecondaryRecipientMandatory,BeneProgNoPrefix,PaymentFrequencyId,CreatedBy,CreatedOn)
		SELECT 'CT-OVC' AS Code,'Orphans & Vulnerable Children Cash Transfer Programme' AS Name,@SysCodeDetailId1 AS BeneficiaryTypeId,1 AS IsActive,@SysCodeDetailId3 AS PrimaryRecipientId,NULL AS SecondaryRecipientId,1 AS BeneProgNoPrefix,0 AS SecondaryRecipientMandatory,@SysCodeDetailId4 AS PaymentFrequencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	END

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Code='PwSD-CT')
	BEGIN
		SET @SysCode='Beneficiary Type'
		SET @SysDetailCode='HOUSEHOLD'
		SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='BENEFICIARY'
		SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		SET @SysDetailCode='CAREGIVER'
		SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Payment Frequency'
		SET @SysDetailCode='BIMONTHLY'
		SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO Programme(Code,Name,BeneficiaryTypeId,IsActive,PrimaryRecipientId,SecondaryRecipientId,SecondaryRecipientMandatory,BeneProgNoPrefix,PaymentFrequencyId,CreatedBy,CreatedOn)
		SELECT 'PwSD-CT' AS Code,'Persons with Severe Disability Cash Transfer Programme' AS Name,@SysCodeDetailId1 AS BeneficiaryTypeId,1 AS IsActive,@SysCodeDetailId3 AS PrimaryRecipientId,NULL AS SecondaryRecipientId,0 AS SecondaryRecipientMandatory,3 AS BeneProgNoPrefix,@SysCodeDetailId4 AS PaymentFrequencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	END

	IF NOT EXISTS(SELECT 1 FROM Programme WHERE Code='OPCT')
	BEGIN
		SET @SysCode='Beneficiary Type'
		SET @SysDetailCode='INDIVIDUAL'
		SELECT @SysCodeDetailId1=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Member Role'
		SET @SysDetailCode='BENEFICIARY'
		SELECT @SysCodeDetailId2=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode
		SET @SysDetailCode='CAREGIVER'
		SELECT @SysCodeDetailId3=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		SET @SysCode='Payment Frequency'
		SET @SysDetailCode='BIMONTHLY'
		SELECT @SysCodeDetailId4=T1.Id FROM SystemCodeDetail T1 INNER JOIN SystemCode T2 ON T1.SystemCodeId=T2.Id AND T2.Code=@SysCode AND T1.Code=@SysDetailCode

		INSERT INTO Programme(Code,Name,BeneficiaryTypeId,IsActive,PrimaryRecipientId,SecondaryRecipientId,SecondaryRecipientMandatory,BeneProgNoPrefix,PaymentFrequencyId,CreatedBy,CreatedOn)
		SELECT 'OPCT' AS Code,'Older Persons Cash Transfer Programme' AS Name,@SysCodeDetailId1 AS BeneficiaryTypeId,1 AS IsActive,@SysCodeDetailId2 AS PrimaryRecipientId,@SysCodeDetailId3 AS SecondaryRecipientId,0 AS SecondaryRecipientMandatory,1 AS BeneProgNoPrefix,@SysCodeDetailId4 AS PaymentFrequencyId,1 AS CreatedBy,GETDATE() AS CreatedOn
	END
END
	-- STEP 03  POPULATE THE 

	IF @MIG_STAGE=2
BEGIN


	SET @SysCode='Registration Group'
	SET @SysDetailCode='TEST CT-OVC GROUP'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
	EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries',@OrderNo=2,@UserId=1;

	SET @SysCode='Registration Group'
	SET @SysDetailCode='TEST PwSD-CT GROUP'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
	EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries',@OrderNo=2,@UserId=1;



	SET @SysCode='Registration Group'
	SET @SysDetailCode='TEST OPCT GROUP'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
	EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries',@OrderNo=2,@UserId=1;


	SET @SysCode='Enrolment Group'
	SET @SysDetailCode='Legacy Migration Test'
	SELECT @SysCodeDetailId1=Id FROM SystemCode WHERE Code=@SysCode
	IF NOT EXISTS(SELECT 1 FROM SystemCodeDetail WHERE SystemCodeId=@SysCodeDetailId1 AND Code=@SysDetailCode)
	EXEC AddEditSystemCodeDetail @SystemCodeId=@SysCodeDetailId1,@DetailCode=@SysDetailCode,@Description='Legacy DSD MIS Beneficiaries',@OrderNo=2,@UserId=1;
	 
END
